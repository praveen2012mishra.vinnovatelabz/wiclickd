import asyncio
import click
import sys
import os
import logging

logging.getLogger().setLevel(logging.INFO)

sys.path.insert(
    0,
    os.path.join(
        os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "service"
    ),
)

from weclikd.service import sql
from weclikd.service import es
from athena.db.topic_db import topic_db
from artemis.db.external_feed_db import feed_db
from eros.db.user_db import user_db
from hestia.db.clik_db import clik_db


topic_index = {
    "settings": {
        "number_of_shards": 3,
        "number_of_replicas": 1,
        "analysis": {
            "normalizer": {
                "keyword_lowercase": {"type": "custom", "filter": ["lowercase"]}
            }
        },
    },
    "mappings": {
        "dynamic": False,
        "properties": {
            "name": {
                "type": "text",
                "analyzer": "simple",
                "fields": {
                    "completion": {"type": "search_as_you_type"},
                    "keyword": {
                        "type": "keyword",
                        "normalizer": "keyword_lowercase",
                    },
                },
            },
            "description": {"type": "text"},
        },
    },
}


clik_index = {
    "settings": {
        "number_of_shards": 3,
        "number_of_replicas": 1,
        "analysis": {
            "normalizer": {
                "keyword_lowercase": {"type": "custom", "filter": ["lowercase"]}
            }
        },
    },
    "mappings": {
        "dynamic": False,
        "properties": {
            "name": {
                "type": "text",
                "analyzer": "standard",
                "fields": {
                    "completion": {
                        "type": "search_as_you_type",
                        "analyzer": "standard",
                    },
                    "keyword": {
                        "type": "keyword",
                        "normalizer": "keyword_lowercase",
                    },
                },
            },
            "description": {"type": "text"},
            "qualifications": {"type": "text"},
        },
    },
}


user_index = {
    "settings": {
        "number_of_shards": 3,
        "number_of_replicas": 1,
        "analysis": {
            "normalizer": {
                "keyword_lowercase": {"type": "custom", "filter": ["lowercase"]}
            }
        },
    },
    "mappings": {
        "dynamic": False,
        "properties": {
            "username": {
                "type": "text",
                "analyzer": "standard",
                "fields": {
                    "completion": {
                        "type": "search_as_you_type",
                        "analyzer": "standard",
                    },
                    "keyword": {
                        "type": "keyword",
                        "normalizer": "keyword_lowercase",
                    },
                },
            }
        },
    },
}


post_index = {
    "settings": {
        "number_of_shards": 3,
        "number_of_replicas": 1,
        "analysis": {
            "filter": {
                "stopwords_url": {
                    "type": "stop",
                    "stopwords": ["https", "http", "www", "", ".com"],
                }
            },
            "analyzer": {
                "url_analyzer": {
                    "type": "custom",
                    "tokenizer": "lowercase",
                    "filter": ["stopwords_url"],
                }
            },
        },
    },
    "mappings": {
        "dynamic": False,
        "properties": {
            "title": {"type": "text"},
            "summary": {"type": "text"},
            "topics": {"type": "text", "analyzer": "simple"},
            "cliks": {"type": "text", "analyzer": "simple"},
            "url": {"type": "text", "analyzer": "url_analyzer"},
            "content_info": {
                "properties": {
                    "created": {"type": "date", "format": "date_time_no_millis"}
                }
            },
        },
    },
}


feed_index = {
    "settings": {
        "number_of_shards": 3,
        "number_of_replicas": 1,
        "analysis": {
            "analyzer": {
                "url_analyzer": {
                    "type": "custom",
                    "tokenizer": "lowercase",
                    "filter": ["url_stopwords"]
                }
            },
            "filter": {
                "url_stopwords": {
                    "type": "stop",
                    "stopwords": ["http", "https", "www", "com", "org"]
                }
            },
            "normalizer": {
                "keyword_lowercase": {"type": "custom", "filter": ["lowercase"]}
            }
        },
    },
    "mappings": {
        "dynamic": False,
        "properties": {
            "name": {
                "type": "text",
                "analyzer": "simple",
                "fields": {
                    "completion": {"type": "search_as_you_type"},
                    "keyword": {
                        "type": "keyword",
                        "normalizer": "keyword_lowercase",
                    },
                },
            },
            "base_topic": {"type": "text"},
            "website": {
                "type": "text",
                "analyzer": "url_analyzer",
                "fields": {
                    "completion": {"type": "search_as_you_type"},
                },
            }
        },
    },
}


async def main(func, *args):
    await sql.start()
    await es.start()

    await func(*args)

    await sql.stop()
    await es.stop()


@click.group()
def cli():
    pass


@click.command("init")
@click.argument(
    "type",
    type=click.Choice(["clik", "topic", "user", "post", "feed", "all"]),
    required=True,
)
def init(type):
    print("initializing es indexes")
    if type == "all":
        asyncio.run(main(es.create_index, "topic", topic_index))
        asyncio.run(main(es.create_index, "clik", clik_index))
        asyncio.run(main(es.create_index, "user", user_index))
        asyncio.run(main(es.create_index, "post", post_index))
        asyncio.run(main(es.create_index, "feed", feed_index))
    if type == "topic":
        asyncio.run(main(es.create_index, "topic", topic_index))
    if type == "clik":
        asyncio.run(main(es.create_index, "clik", clik_index))
    if type == "user":
        asyncio.run(main(es.create_index, "user", user_index))
    if type == "post":
        asyncio.run(main(es.create_index, "post", post_index))
    if type == "feed":
        asyncio.run(main(es.create_index, "feed", feed_index))


@click.command("refresh")
@click.argument(
    "type", type=click.Choice(["clik", "topic", "user", "feed", "all"]), required=True
)
def refresh(type):
    if type == "all":
        asyncio.run(main(es.refresh_index, "topic", topic_db))
        asyncio.run(main(es.refresh_index, "clik", clik_db))
        asyncio.run(main(es.refresh_index, "user", user_db))
        asyncio.run(main(es.refresh_index, "feed", feed_db))
    if type == "topic":
        asyncio.run(main(es.refresh_index, "topic", topic_db))
    if type == "clik":
        asyncio.run(main(es.refresh_index, "clik", clik_db))
    if type == "user":
        asyncio.run(main(es.refresh_index, "user", user_db))
    if type == "feed":
        asyncio.run(main(es.refresh_index, "feed", feed_db))


cli.add_command(init)
cli.add_command(refresh)

if __name__ == "__main__":
    cli()
