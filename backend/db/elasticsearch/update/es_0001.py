import asyncio
import sys
import os

sys.path.insert(
    0,
    os.path.join(
        os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(__file__)))),
        "service",
    ),
)
from weclikd.service import es


async def main():
    await es.start()
    await es.update_mapping(
        index="user",
        mapping={
            "properties": {
                "username": {
                    "type": "text",
                    "analyzer": "standard",
                    "fields": {
                        "completion": {"type": "completion"},
                        "keyword": {
                            "type": "keyword",
                            "normalizer": "keyword_lowercase",
                        },
                    },
                }
            }
        },
    )
    await es.stop()


if __name__ == "__main__":
    asyncio.run(main())
