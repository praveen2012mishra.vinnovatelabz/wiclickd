How to do DB Migration.

1) Open up model.mwb using MySQL Workbench
2) Modify / Add Tables adn then Synchronize the model
3) Copy SQL diff into weclikd_update_xxxx.sql
4) If existing data has to be migrated, create a weclikd_update_xxxx.py.
The update function will be invoked during the migration after the sql file.
5) Re-build docker container and start docker-compose
6) Run system tests locally, make sure everything passes
7) Check-in db changes before code changes.

What happens in GCP.

1) Google Cloud Build starts
2) Starts cloud proxy
3) Calls update_db.py which will runs sql file and accompanied python file if necessary
4) Re-builds sql docker container
