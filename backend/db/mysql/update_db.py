from databases import Database
import os
import importlib
import argparse
import asyncio
import glob
import sqlalchemy
import time
import logging
from sqlalchemy import (
    Table,
    Column,
    Integer,
    CHAR,
    TIMESTAMP,
    text as sqltext,
    JSON,
    MetaData,
    SMALLINT,
)

schema_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "updates"))

logging.getLogger().setLevel(logging.INFO)

metadata = MetaData()

DbUpdateTable = Table(
    "db_updates",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column("sql_file", CHAR(23), primary_key=True),
    Column("additional_update", SMALLINT, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)


def parse_sql(filename):
    data = open(filename, "r").readlines()
    stmts = []
    DELIMITER = ";"
    stmt = ""

    for lineno, line in enumerate(data):
        if not line.strip():
            continue

        if line.startswith("--"):
            continue

        if "DELIMITER" in line:
            DELIMITER = line.split()[1]
            continue

        if DELIMITER not in line:
            stmt += line.replace(DELIMITER, ";")
            continue

        line = line.replace(DELIMITER, ";")
        if stmt:
            stmt += line
            stmts.append(stmt.strip())
            stmt = ""
        else:
            stmts.append(line.strip())
    return stmts


async def update_table(username, password, host, port, debug):
    os.chdir(os.path.abspath(os.path.dirname(__file__)))
    db_url = f"mysql://{username}:{password}@{host}/weclikd?charset=utf8mb4&binary_prefix=true"
    db = Database(db_url)

    logging.info(f"Connecting to {host}...")
    for i in range(12):
        try:
            await db.connect()
            logging.info("Connected to database")
            break
        except:
            logging.exception(f"Could not connect to database: retry {i}")
            time.sleep(5)

    try:
        row = await db.fetch_one(
            sqlalchemy.select([DbUpdateTable.c.sql_file])
            .order_by(sqlalchemy.desc(DbUpdateTable.c.id))
            .limit(1)
        )
    except:
        row = None

    all_sql_files = glob.glob(os.path.join(schema_dir, "*.sql"))
    all_sql_files.sort()
    if not row:
        sql_files_to_execute = all_sql_files
    else:
        last_inserted = row["sql_file"]
        index = all_sql_files.index(os.path.join(schema_dir, last_inserted))
        sql_files_to_execute = all_sql_files[index + 1 :]

    for sql_file in sql_files_to_execute:
        logging.info(f"Executing {sql_file}")
        stmts = parse_sql(sql_file)
        for stmt in stmts:
            logging.info("    " + stmt)
            if stmt == ";" or not stmt:
                continue
            await db.execute(stmt)

    for sql_file in all_sql_files:
        async with db.transaction():
            python_update = sql_file[:-4] + ".py"
            if not os.path.exists(python_update):
                continue

            row = await db.fetch_one(
                sqlalchemy.select(
                    [DbUpdateTable.c.additional_update],
                    whereclause=DbUpdateTable.c.sql_file == os.path.basename(sql_file),
                )
            )
            if row.additional_update:
                continue

            logging.info(f"Running Additional Update: {sql_file}")
            update_module = importlib.import_module(
                "updates" + "." + os.path.basename(sql_file)[:-4]
            )
            await update_module.update(db)
            await db.execute(
                sqlalchemy.update(
                    DbUpdateTable,
                    whereclause=DbUpdateTable.c.sql_file == os.path.basename(sql_file),
                    values={"additional_update": 1},
                )
            )
            logging.info(f"Additional Update Completed: {sql_file}")

    await db.disconnect()


if __name__ == "__main__":
    logging.info("Staring DB Update Script")
    parser = argparse.ArgumentParser()
    parser.add_argument("-H", "--host", default=os.getenv("SQL_HOST", "localhost"))
    parser.add_argument("-u", "--username", default=os.getenv("SQL_USER", "root"))
    parser.add_argument(
        "-p", "--password", default=os.getenv("SQL_PASSWORD", "abc12345")
    )
    parser.add_argument(
        "-P", "--port", type=int, default=int(os.getenv("SQL_PORT", "3306"))
    )
    parser.add_argument("-d", "--debug", action="store_true")
    args = parser.parse_args()
    asyncio.run(
        update_table(args.username, args.password, args.host, args.port, args.debug)
    )
    logging.info("Completed DB Update Script")
