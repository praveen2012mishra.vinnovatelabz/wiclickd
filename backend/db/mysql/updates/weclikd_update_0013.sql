-- MySQL Workbench Synchronization
-- Generated: 2020-05-30 14:03
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `weclikd`.`user`
ADD COLUMN `followers` INT(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `username`;

ALTER TABLE `weclikd`.`clik_member`
DROP COLUMN `type`,
ADD COLUMN `type` SMALLINT(5) UNSIGNED NOT NULL AFTER `seniority`;

CREATE TABLE IF NOT EXISTS `weclikd`.`user_new_post` (
  `user_id` BIGINT(20) UNSIGNED NOT NULL,
  `content_id` BIGINT(20) UNSIGNED NOT NULL,
  `data` JSON NOT NULL,
  PRIMARY KEY (`user_id`, `content_id`),
  INDEX `fk_user_new_post_content_id_idx` (`content_id` ASC),
  CONSTRAINT `fk_user_new_post_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `weclikd`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_new_post_content_id`
    FOREIGN KEY (`content_id`)
    REFERENCES `weclikd`.`content` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


CREATE TABLE IF NOT EXISTS `weclikd`.`user_new_post_metadata` (
  `user_id` BIGINT(20) UNSIGNED NOT NULL,
  `last_content_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `data` JSON NOT NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0013.sql');


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
