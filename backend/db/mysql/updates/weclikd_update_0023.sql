-- MySQL Workbench Synchronization
-- Generated: 2020-11-27 16:11
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `weclikd`.`likes_received` 
ADD COLUMN `pay_period` TIMESTAMP NOT NULL AFTER `account_id`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`account_id`, `pay_period`);
;

DROP TABLE IF EXISTS `weclikd`.`likes_used` ;

DROP TABLE IF EXISTS `weclikd`.`content_to_account` ;

INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0023.sql');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
