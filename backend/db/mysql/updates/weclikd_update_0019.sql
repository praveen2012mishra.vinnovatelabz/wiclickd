-- MySQL Workbench Synchronization
-- Generated: 2020-08-01 15:50
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `weclikd`.`clik`
ADD INDEX `clik_follower_idx` (`followers` ASC);
;

ALTER TABLE `weclikd`.`topic`
ADD INDEX `topic_follower_idx` (`followers` ASC);
;

ALTER TABLE `weclikd`.`user`
ADD INDEX `user_follower_idx` (`username` ASC);
;

ALTER TABLE `weclikd`.`external_feed`
ADD INDEX `external_feed_follower_idx` (`followers` ASC);
;

ALTER TABLE `weclikd`.`feed_follow`
ADD INDEX `fk_feed_follow_feed_id_idx` (`feed_id` ASC);
;

ALTER TABLE `weclikd`.`feed_follow`
ADD CONSTRAINT `fk_feed_follow_feed_id`
  FOREIGN KEY (`feed_id`)
  REFERENCES `weclikd`.`external_feed` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0019.sql');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
