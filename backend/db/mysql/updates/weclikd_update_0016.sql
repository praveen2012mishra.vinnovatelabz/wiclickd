-- MySQL Workbench Synchronization
-- Generated: 2020-06-25 00:57
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `weclikd`.`user_new_post`
ADD COLUMN `priority` BIGINT(20) UNSIGNED NOT NULL AFTER `content_id`,
ADD INDEX `user_new_post_priority` (`user_id` ASC, `priority` DESC);
;

CREATE TABLE IF NOT EXISTS `weclikd`.`post_read` (
  `user_id` BIGINT(20) UNSIGNED NOT NULL,
  `content_id` BIGINT(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `content_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

DROP TABLE IF EXISTS `weclikd`.`user_new_post_metadata` ;


INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0016.sql');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
