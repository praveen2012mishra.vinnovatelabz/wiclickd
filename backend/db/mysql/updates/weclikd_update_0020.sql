-- MySQL Workbench Synchronization
-- Generated: 2020-08-16 18:20
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `weclikd`.`comment`
ADD COLUMN `discussion_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0 AFTER `clik_id`,
ADD COLUMN `user_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0 AFTER `discussion_id`,
ADD COLUMN `level` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0 AFTER `user_id`,
ADD INDEX `fk_comment_discussion_id_idx` (`discussion_id` ASC);
;

ALTER TABLE `weclikd`.`comment`
ADD CONSTRAINT `fk_comment_discussion_id`
  FOREIGN KEY (`discussion_id`)
  REFERENCES `weclikd`.`content` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0020.sql');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;