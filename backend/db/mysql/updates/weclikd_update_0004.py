import sqlalchemy
from sqlalchemy import Table, Column, JSON, MetaData
from sqlalchemy.dialects.mysql import BIGINT


class DBScanner:
    def __init__(self, db, model, table):
        self.db = db
        self.model = model
        self.table = table

    async def __aiter__(self):
        cursor = None
        while True:
            sql_query = (
                sqlalchemy.select([self.table]).order_by(self.table.c.id).limit(100)
            )
            if cursor:
                sql_query = sql_query.where(self.table.c.id > cursor)

            rows = await self.db.fetch_all(sql_query)
            for row in rows:
                if self.model:
                    yield self.model.row_to_proto(row)
                else:
                    yield row

            if len(rows) == 100:
                cursor = rows[-1].id
            else:
                break


UnsignedBigInt = BIGINT(unsigned=True)

metadata = MetaData()

ContentTable = Table(
    "content",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True),
    Column("data", JSON, nullable=False),
)

TopicTable = Table(
    "topic",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True),
    Column("data", JSON, nullable=False),
)


async def update(db):
    topics = []
    async for row in DBScanner(db, model=None, table=TopicTable):
        topics.append(row.data["name"])
    print(topics)
