-- MySQL Workbench Synchronization
-- Generated: 2020-04-18 21:23
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

DELIMITER $$

USE `weclikd`$$
DROP TRIGGER IF EXISTS `weclikd`.`shard_insert_account` $$

USE `weclikd`$$
DROP TRIGGER IF EXISTS `weclikd`.`shard_insert_user` $$

DELIMITER ;

ALTER TABLE `weclikd`.`account`
ADD COLUMN `hashed_access_key` BINARY(32) NULL DEFAULT NULL AFTER `data`,
ADD INDEX `hashed_access_key_idx` (`hashed_access_key` ASC);
;


INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0010.sql');


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
