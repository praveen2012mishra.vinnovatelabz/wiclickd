import sqlalchemy
import binascii
import time
from sqlalchemy import (
    Table,
    Column,
    TIMESTAMP,
    text as sqltext,
    JSON,
    MetaData,
    SMALLINT,
)
from sqlalchemy.dialects.mysql import BIGINT

UnsignedBigInt = BIGINT(unsigned=True)

metadata = MetaData()

ClikFollowTable = Table(
    "clik_follow",
    metadata,
    Column("user_id", UnsignedBigInt, primary_key=True),
    Column("clik_id", UnsignedBigInt, primary_key=True),
    Column("member_type", SMALLINT, nullable=False),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

ClikMemberTable = Table(
    "clik_member",
    metadata,
    Column("user_id", UnsignedBigInt, primary_key=True),
    Column("clik_id", UnsignedBigInt, primary_key=True),
    Column("seniority", UnsignedBigInt, nullable=False),
    Column("member_type", SMALLINT, nullable=False),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)


async def update(db):
    rows = await db.fetch_all(sqlalchemy.select([ClikFollowTable]))
    for row in rows:
        row = {**row}
        member_type = row["member_type"]
        if member_type > 3:
            seniority = int(time.time() * 1000) + binascii.crc32(
                str(row["user_id"]).encode()
            )
            if member_type == 5:
                seniority += 1 << 62
            elif member_type == 4:
                seniority += 1 << 60
            row["seniority"] = seniority

            await db.execute(sqlalchemy.insert(ClikMemberTable, values=row))
