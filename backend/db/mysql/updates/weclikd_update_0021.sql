-- MySQL Workbench Synchronization
-- Generated: 2020-10-24 18:50
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE TABLE IF NOT EXISTS `weclikd`.`stripe_customer` (
  `account_id` BIGINT(20) UNSIGNED NOT NULL,
  `customer_id` TEXT NOT NULL,
  `current_order_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_id`),
  INDEX `fk_stripe_customer_order_idx_idx` (`current_order_id` ASC),
  CONSTRAINT `fk_stripe_customer_account_id`
    FOREIGN KEY (`account_id`)
    REFERENCES `weclikd`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_stripe_customer_order_idx`
    FOREIGN KEY (`current_order_id`)
    REFERENCES `weclikd`.`stripe_order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `weclikd`.`stripe_order` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` BIGINT(20) UNSIGNED NOT NULL,
  `subscription_id` TEXT NOT NULL,
  `subscription_type` SMALLINT(6) NOT NULL,
  `cancel` TINYINT(4) NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `stripe_order_account_id_fk_idx` (`account_id` ASC),
  CONSTRAINT `stripe_order_account_id_fk`
    FOREIGN KEY (`account_id`)
    REFERENCES `weclikd`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `weclikd`.`stripe_payout` (
  `account_id` BIGINT(20) UNSIGNED NOT NULL,
  `stripe_state` VARCHAR(32) NULL DEFAULT NULL,
  `stripe_user_id` TEXT NULL DEFAULT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_id`),
  CONSTRAINT `fk_stripe_payout_fk`
    FOREIGN KEY (`account_id`)
    REFERENCES `weclikd`.`account` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


DELIMITER $$

USE `weclikd`$$
CREATE
DEFINER=CURRENT_USER
TRIGGER `weclikd`.`shard_insert_stripe_order`
BEFORE INSERT ON `weclikd`.`stripe_order`
FOR EACH ROW
BEGIN
  DECLARE `seq_id`, `now_millis` BIGINT UNSIGNED;
  DECLARE `our_epoch` BIGINT UNSIGNED DEFAULT 1569888000;
  DECLARE `shard_id` INT UNSIGNED DEFAULT 1;
  SET `now_millis` := UNIX_TIMESTAMP();
  SET `seq_id` := `nextval`();
  SET NEW.`id` := (SELECT (`now_millis` - `our_epoch`) << 23 |
                           `shard_id` << 10 |
                           MOD(`seq_id`, 1024)
                  );
END$$


DELIMITER ;


INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0021.sql');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
