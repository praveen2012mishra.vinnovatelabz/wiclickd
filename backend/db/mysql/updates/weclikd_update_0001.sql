-- MySQL Workbench Synchronization
-- Generated: 2019-11-24 20:58
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `weclikd`.`clik_follow`
CHANGE COLUMN `member_type` `member_type` SMALLINT(1) UNSIGNED NOT NULL DEFAULT 0 ;

ALTER TABLE `weclikd`.`content`
ADD COLUMN `state` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0 AFTER `data`;

ALTER TABLE `weclikd`.`db_updates`
ADD COLUMN `additional_update` SMALLINT(6) NOT NULL DEFAULT 0 AFTER `sql_file`;

CREATE TABLE IF NOT EXISTS `weclikd`.`clik_member` (
  `user_id` BIGINT(20) UNSIGNED NOT NULL,
  `clik_id` BIGINT(20) UNSIGNED NOT NULL,
  `seniority` BIGINT(20) UNSIGNED NOT NULL,
  `member_type` SMALLINT(5) UNSIGNED NOT NULL,
  `data` JSON NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`, `clik_id`),
  INDEX `fk_clik_member_clik_id_idx` (`clik_id` ASC),
  INDEX `clik_member_seniority_idx` (`seniority` DESC),
  CONSTRAINT `fk_clik_member_clik_id`
    FOREIGN KEY (`clik_id`)
    REFERENCES `weclikd`.`clik` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `weclikd`.`clik_invite` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `clik_id` BIGINT(20) UNSIGNED NOT NULL,
  `user_id` BIGINT(20) UNSIGNED NOT NULL,
  `invitor_user_id` BIGINT(20) UNSIGNED NOT NULL,
  `member_type` SMALLINT(5) UNSIGNED NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `clik_invite_clik_user_idx` (`clik_id` ASC, `user_id` ASC),
  CONSTRAINT `clik_invite_clik_id_idx`
    FOREIGN KEY (`clik_id`)
    REFERENCES `weclikd`.`clik` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `weclikd`.`clik_join_request` (
  `id` BIGINT(20) UNSIGNED NOT NULL,
  `clik_id` BIGINT(20) UNSIGNED NOT NULL,
  `user_id` BIGINT(20) UNSIGNED NOT NULL,
  `data` JSON NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `clik_join_clik_user_idx` (`clik_id` ASC, `user_id` ASC),
  CONSTRAINT `fk_clik_join_request_idx`
    FOREIGN KEY (`clik_id`)
    REFERENCES `weclikd`.`clik` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `weclikd`.`new_clik` (
  `id` BIGINT(20) UNSIGNED NOT NULL,
  `data` JSON NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `weclikd`.`new_topic` (
  `id` BIGINT(20) UNSIGNED NOT NULL,
  `name` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `weclikd`.`banned_topic` (
  `id` BIGINT(20) UNSIGNED NOT NULL,
  `name` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


DELIMITER $$

USE `weclikd`$$
CREATE
DEFINER=CURRENT_USER
TRIGGER `weclikd`.`shard_insert_clik_invite`
BEFORE INSERT ON `weclikd`.`clik_invite`
FOR EACH ROW
BEGIN
  DECLARE `seq_id`, `now_millis` BIGINT UNSIGNED;
  DECLARE `our_epoch` BIGINT UNSIGNED DEFAULT 1569888000;
  DECLARE `shard_id` INT UNSIGNED DEFAULT 1;
  SET `now_millis` := UNIX_TIMESTAMP();
  SET `seq_id` := `nextval`();
  SET NEW.`id` := (SELECT (`now_millis` - `our_epoch`) << 23 |
                           `shard_id` << 10 |
                           MOD(`seq_id`, 1024)
                  );
END$$

USE `weclikd`$$
CREATE
DEFINER=CURRENT_USER
TRIGGER `weclikd`.`shard_insert_clik_join_request`
BEFORE INSERT ON `weclikd`.`clik_join_request`
FOR EACH ROW
BEGIN
  DECLARE `seq_id`, `now_millis` BIGINT UNSIGNED;
  DECLARE `our_epoch` BIGINT UNSIGNED DEFAULT 1569888000;
  DECLARE `shard_id` INT UNSIGNED DEFAULT 1;
  SET `now_millis` := UNIX_TIMESTAMP();
  SET `seq_id` := `nextval`();
  SET NEW.`id` := (SELECT (`now_millis` - `our_epoch`) << 23 |
                           `shard_id` << 10 |
                           MOD(`seq_id`, 1024)
                  );
END$$


DELIMITER ;

INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0001.sql');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
