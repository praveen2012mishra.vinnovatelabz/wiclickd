-- MySQL Workbench Synchronization
-- Generated: 2020-01-18 22:03
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE TABLE IF NOT EXISTS `weclikd`.`topic_relationship` (
  `parent_topic_id` BIGINT(20) UNSIGNED NOT NULL,
  `child_topic_id` BIGINT(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`parent_topic_id`, `child_topic_id`),
  INDEX `fk_child_topic_id_idx` (`child_topic_id` ASC),
  CONSTRAINT `fk_child_topic_id`
    FOREIGN KEY (`child_topic_id`)
    REFERENCES `weclikd`.`topic` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_parent_topic_id`
    FOREIGN KEY (`parent_topic_id`)
    REFERENCES `weclikd`.`topic` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0005.sql');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
