-- MySQL Workbench Synchronization
-- Generated: 2019-12-24 17:05
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE TABLE IF NOT EXISTS `weclikd`.`content_like` (
  `user_id` BIGINT(20) UNSIGNED NOT NULL,
  `content_id` BIGINT(20) UNSIGNED NOT NULL,
  `like_type` SMALLINT(5) UNSIGNED NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`, `content_id`),
  INDEX `fk_content_like_content_id_idx` (`content_id` ASC),
  CONSTRAINT `fk_content_like_content_id`
    FOREIGN KEY (`content_id`)
    REFERENCES `weclikd`.`content` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

ALTER TABLE `weclikd`.`content_to_account`
ADD COLUMN `monetized` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1 AFTER `currency`;

INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0004.sql');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
