-- MySQL Workbench Synchronization
-- Generated: 2020-02-17 00:53
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `weclikd`.`clik_post`
DROP COLUMN `id`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`clik_id`, `content_id`);
;

ALTER TABLE `weclikd`.`topic_post`
DROP COLUMN `id`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`topic_id`, `content_id`);
;

ALTER TABLE `weclikd`.`user_post`
DROP COLUMN `id`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`user_id`, `content_id`);
;


DELIMITER $$

USE `weclikd`$$
DROP TRIGGER IF EXISTS `weclikd`.`shard_insert_clik_post` $$

USE `weclikd`$$
DROP TRIGGER IF EXISTS `weclikd`.`shard_insert_topic_post` $$

USE `weclikd`$$
DROP TRIGGER IF EXISTS `weclikd`.`shard_insert_user_post` $$


DELIMITER ;

INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0006.sql');


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
