-- MySQL Workbench Synchronization
-- Generated: 2020-07-12 15:10
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `weclikd`.`account`
ADD COLUMN `num_unread_notifications` INT(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `firebase_id`;

CREATE TABLE IF NOT EXISTS `weclikd`.`notification` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(20) NOT NULL,
  `data` JSON NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


DELIMITER $$

USE `weclikd`$$
CREATE
DEFINER=CURRENT_USER
TRIGGER `weclikd`.`shard_insert_notification`
BEFORE INSERT ON `weclikd`.`notification`
FOR EACH ROW
BEGIN
  DECLARE `seq_id`, `now_millis` BIGINT UNSIGNED;
  DECLARE `our_epoch` BIGINT UNSIGNED DEFAULT 1569888000;
  DECLARE `shard_id` INT UNSIGNED DEFAULT 1;
  SET `now_millis` := UNIX_TIMESTAMP();
  SET `seq_id` := `nextval`();
  SET NEW.`id` := (SELECT (`now_millis` - `our_epoch`) << 23 |
                           `shard_id` << 10 |
                           MOD(`seq_id`, 1024)
                  );
END$$


DELIMITER ;

INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0018.sql');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
