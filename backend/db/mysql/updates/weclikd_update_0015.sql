-- MySQL Workbench Synchronization
-- Generated: 2020-06-16 00:01
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `weclikd`.`topic` 
ADD COLUMN `parent_topic_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL AFTER `id`,
ADD INDEX `parent_topic_id_fk_idx` (`parent_topic_id` ASC);
;

CREATE TABLE IF NOT EXISTS `weclikd`.`topic_alias` (
  `name` VARCHAR(32) NOT NULL,
  `topic_id` BIGINT(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`name`),
  INDEX `topic_alias_topic_id_fk_idx` (`topic_id` ASC),
  CONSTRAINT `topic_alias_topic_id_fk`
    FOREIGN KEY (`topic_id`)
    REFERENCES `weclikd`.`topic` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

DROP TABLE IF EXISTS `weclikd`.`topic_relationship` ;

ALTER TABLE `weclikd`.`topic` 
ADD CONSTRAINT `parent_topic_id_fk`
  FOREIGN KEY (`parent_topic_id`)
  REFERENCES `weclikd`.`topic` (`id`)
  ON DELETE SET NULL
  ON UPDATE CASCADE;


INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0015.sql');


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
