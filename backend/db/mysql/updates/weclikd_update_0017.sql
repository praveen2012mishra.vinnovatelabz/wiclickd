-- MySQL Workbench Synchronization
-- Generated: 2020-06-26 02:55
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Yoshinori Osone

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `weclikd`.`user_new_post`
DROP INDEX `user_new_post_priority` ,
ADD INDEX `user_new_post_priority` (`user_id` ASC, `priority` DESC);
;

ALTER TABLE `weclikd`.`post_read`
ADD INDEX `fk_post_read_content_id_idx` (`content_id` ASC);
;

ALTER TABLE `weclikd`.`post_read`
ADD CONSTRAINT `fk_post_read_content_id`
  FOREIGN KEY (`content_id`)
  REFERENCES `weclikd`.`content` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

INSERT INTO weclikd.db_updates(sql_file) VALUES ('weclikd_update_0017.sql');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
