# Redis at Weclikd

## List of Keys and Expiration


| Key              | Type | Info                               | Expiration  |
|------------------|------|------------------------------------|-------------|
| H-trend          | set  | Home feed for users not logged in  | Never       | 
| H\<userid>-trend  | set  | Home feed for users                | 1 hour      |
| T\<topicid>-trend | set  | Feed for topics                    | Never       |
| C\<clikid>-trend  | set  | Feed for cliks                     | 1 day       |
| F\<feedid>-trend  | set  | Feed for external feed             | 1 day       |
| U\<userid>-trend  | set  | Feed when viewing other users      | 1 hour      |
| H-new            | set  | Home feed for users not logged in  | Never       | 
| H\<userid>-new    | set  | Home feed for users                | 1 hour      |
| T\<topicid>-new   | set  | Feed for topics                    | Never       |
| C\<clikid>-new    | set  | Feed for cliks                     | 1 day       |
| F\<feedid>-new    | set  | Feed for external feed             | 1 day       |
| U\<userid>-new    | set  | Feed when viewing other users      | 1 hour      |
| T\<topicid>-prof  | set  | Profile for topics                 | Never       |
| C\<clikid>-prof   | set  | Profile for cliks                  | 1 day       |
| F\<feedid>-prof   | set  | Profile for external feed          | 1 day       |
| U\<userid>-prof   | set  | Profile for users                  | 1 hour      |
| trending-users   | set  | Trending Users                     | Never       |
| trending-cliks   | set  | Trending Cliks                     | Never       |
| trending-topics  | set   | Trending Topics                    | Never       |
| trending-feeds   | set   | Trending Feeds                     | Never       |
| trending-feeds   | set   | Trending Feeds                     | Never       |
| U\<userid>-session| hash | Session Info(home-update)          | 1 hour     |