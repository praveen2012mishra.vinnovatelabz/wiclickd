# Cliks

## Introduction

A clik is an exclusive group of people who share a similar background 
in an intellectual field. Members of a clik can have their own
high quality discussion.

## Clik Creation Process

First, a user will have to fill out an application form. We need basic 
information such as the description, the name, the website, and the
qualifications to join the clik. They may invite users during this process 
using the user's username or email.

Then, an admin of Weclikd will decide to approve or reject the clik application. 


## Clik Member Hierarchy

Once the clik is approved, the clik creator will automatically be a super-admin
of the clik. The other roles are admin and member.

Every user who is part of the clik has permissions to share posts to the clik and start an
exclusive discussion of the post. While the discussion is read-able by anyone,
only members of the clik can participate in this discussion. We hope to foster
higher quality content in this way.

Here's the permissions that each role has

| Action                 | Super Admin | Admin | Member |
|------------------------|-------------|-------|--------|
| Share Post             | [x]         | [x]   | [x]    |
| Comment                | [x]         | [x]   | [x]    |
| Report                 | [x]         | [x]   | [x]    |
| View Member List       | [x]         | [x]   | [x]    |
| Remove Comment or Post | [x]         | [x]   |        |
| Invite Members         | [x]         | [x]   |        |
| Invite Admins          | [x]         |       |        |
| Invite Super Admins    | [x]         |       |        |
| Edit Clik Profile      | [x]         |       |        |
 