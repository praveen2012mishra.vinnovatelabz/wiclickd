import argparse
from weclikd.service import pubsub
from weclikd.utils import environment


def create_topics_and_subscriptions(janus_host=environment.JANUS_HOST, overwrite=False):
    # other topics
    topic_names = set()
    for topic in pubsub.topics:
        topic_names.add(topic.name)

        print(f"Creating Topic: {topic.name}")
        topic.create()

    for subscriptions in pubsub.subscriptions.values():
        for subscription in subscriptions:
            if subscription.topic_name not in topic_names:
                raise Exception(f"{subscription.topic_name} is not a valid topic")
            print(f"Subscriptions: {subscription.subscription_name}")
            if overwrite:
                subscription.delete()
            print(f"Creating Subscription: {subscription.push_url}")
            subscription.create()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--janus_host", default=environment.JANUS_HOST)
    parser.add_argument("--overwrite", action="store_true")
    args = parser.parse_args()
    create_topics_and_subscriptions(args.janus_host, args.overwrite)
