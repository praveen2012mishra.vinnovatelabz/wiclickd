import os
from . import gen_protobuf
from . import get_graphql_schema


if __name__ == "__main__":
    gen_protobuf.clean()
    gen_protobuf.gen_protobuf()
    get_graphql_schema.refresh_schemas()
    os.system("black . --exclude .*pb2.*py")
