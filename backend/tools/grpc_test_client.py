import logging
import grpc
from google.protobuf import json_format


def send(port, stub_class, func_name, session, request):
    logging.info(f"Request to {func_name}\n{json_format.MessageToJson(request)}")
    with grpc.insecure_channel(f"localhost:{port}") as channel:
        stub = stub_class(channel)
        func = getattr(stub, func_name)
        response = func(request, metadata=session._session.items())
        logging.info(f"Response:\n{json_format.MessageToJson(response)}")
        return response
