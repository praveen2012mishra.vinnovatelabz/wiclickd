"""
Script to add dn lots of posts, comments, likes
Created on 04.06.19
Created By Weclikd Team
"""

from sanic import Blueprint, response
import twitter as tw
import json

scriptsfetch = Blueprint("scriptsfetch", url_prefix="/fetchscript")


@scriptsfetch.route("/", methods=["GET"])
async def scripts_fetch(request):
    return response.json({"status": "success"})
