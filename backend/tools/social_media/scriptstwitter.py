"""
Script to fetch twitter account details
Created on 22.05.19
Created By Weclikd Team
"""
from sanic import Blueprint, response
import twitter as tw
import json

scriptstwitter = Blueprint("scriptstwitter", url_prefix="/twitter")


@scriptstwitter.route("/", methods=["GET"])
async def twitter(request):
    arrayResult = {}
    api = tw.Api(
        consumer_key="87NkcIeJMQ1akspJmcIciFSIr",
        consumer_secret="qRYQXIGI8MYbmSgwRB3ZDbFjE1E4jtewjhHKzQwGXRiIWKbTtD",
        access_token_key="1130371016406867968-q0UrifgNWgVlSAoS1t0XLCww6PoyHP",
        access_token_secret="ptIlk8FqlK5T6PMSwru6tkSXIM5C6VOca0k1FloXQ3YRn",
    )
    arrayResult = api.GetUser(screen_name="BarackObama", return_json=True)
    timeline = api.GetUserTimeline(screen_name="BarackObama", count=3)
    arraySet = {}
    makeArray = {}

    postArray = {}
    postResult = {}
    count = 0
    makeArray["id"] = arrayResult["id"]
    makeArray["followers_count"] = arrayResult["followers_count"]
    makeArray["description"] = arrayResult["description"]

    for val in timeline:
        if val.retweeted_status:
            arraySet[count] = val.retweeted_status.urls[0].expanded_url

            postArray["user_name"] = val.retweeted_status.user.name
            postArray["text"] = val.retweeted_status.text

            postArray["pictures"] = [
                val.retweeted_status.user.profile_background_image_url_https,
                val.retweeted_status.user.profile_banner_url,
                val.retweeted_status.user.profile_image_url,
            ]
            postArray["posted_time"] = val.retweeted_status.created_at

            postResult[count] = postArray
        else:
            arraySet[count] = val.urls[0].expanded_url
            postArray["user_name"] = val.user.name
            postArray["text"] = val.text

            postArray["pictures"] = [
                val.user.profile_background_image_url_https,
                val.user.profile_banner_url,
                val.user.profile_image_url,
            ]
            postArray["posted_time"] = val.created_at

            postResult[count] = postArray

        count = count + 1

    makeArray["post"] = arraySet
    makeArray["link"] = arrayResult["url"]
    makeArray["created_time"] = arrayResult["created_at"]

    return response.json(
        {"status": "success", "arrayResult": makeArray, "postArray": postResult}
    )


@scriptstwitter.route("/fetch", methods=["GET"])
async def twitter_fetch(request):
    arrayResult = {}
    api = tw.Api(
        consumer_key="87NkcIeJMQ1akspJmcIciFSIr",
        consumer_secret="qRYQXIGI8MYbmSgwRB3ZDbFjE1E4jtewjhHKzQwGXRiIWKbTtD",
        access_token_key="1130371016406867968-q0UrifgNWgVlSAoS1t0XLCww6PoyHP",
        access_token_secret="ptIlk8FqlK5T6PMSwru6tkSXIM5C6VOca0k1FloXQ3YRn",
    )
    arrayResult = api.GetUser(screen_name="BarackObama", return_json=True)
    timeline = api.GetUserTimeline(screen_name="BarackObama", count=22)
    arraySet = {}
    makeArray = {}

    postArray = {}
    postResult = {}
    count = 0
    makeArray["id"] = arrayResult["id"]
    makeArray["followers_count"] = arrayResult["followers_count"]
    makeArray["description"] = arrayResult["description"]

    for val in timeline:
        if val.retweeted_status:
            arraySet[count] = val.retweeted_status.urls[0].expanded_url

            postArray["user_name"] = val.retweeted_status.user.name
            postArray["text"] = val.retweeted_status.text

            postArray["pictures"] = [
                val.retweeted_status.user.profile_background_image_url_https,
                val.retweeted_status.user.profile_banner_url,
                val.retweeted_status.user.profile_image_url,
            ]
            postArray["posted_time"] = val.retweeted_status.created_at

            postResult[count] = postArray
        else:
            arraySet[count] = val.urls[0].expanded_url
            postArray["user_name"] = val.user.name
            postArray["text"] = val.text

            postArray["pictures"] = [
                val.user.profile_background_image_url_https,
                val.user.profile_banner_url,
                val.user.profile_image_url,
            ]
            postArray["posted_time"] = val.created_at

            postResult[count] = postArray

        count = count + 1

    makeArray["post"] = arraySet
    makeArray["link"] = arrayResult["url"]
    makeArray["created_time"] = arrayResult["created_at"]

    return response.json(
        {"status": "success", "arrayResult": makeArray, "postArray": postResult}
    )
