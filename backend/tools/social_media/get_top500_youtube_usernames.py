"""
Script to fetch top 500 youtube channel details
Created on 24.05.19
Created By Weclikd Team
"""
from sanic import Blueprint, response
import json
from bs4 import BeautifulSoup
import re
import os
import requests


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
UPLOAD_DIR = os.path.join(BASE_DIR, "static", "download")

scriptstopyoutube = Blueprint("scriptstopyoutube", url_prefix="/topyoutube")


@scriptstopyoutube.route("/fetch", methods=["GET"])
async def topyoutube(request):
    import csv

    response_got = requests.get("https://socialblade.com/youtube/top/500")

    soup = BeautifulSoup(response_got.text, "html.parser")

    # Pull html from all instances of <div> tag having class content-module-wide
    artist_name_list = soup.findAll("div", style="float: right; width: 900px;")
    key = "AIzaSyBdGz2XZGbICW4mh39P4Hrl0NyqnEwosnA"
    # artist_name_list_items = artist_name_list.find_all('a')
    for text in artist_name_list:
        download = text.find_all("a")
        for text in download:
            hrefText = text["href"]
            # print('--||--')
            splitArray = hrefText.split("/youtube/c/")
            # print()
            for key, sa in enumerate(splitArray):

                # print(sa)
                name = text.contents[0]
                userName = name
                print(sa)
                # print()
                # print(text)
                if sa:
                    url3 = (
                        "https://www.googleapis.com/youtube/v3/channels?part=snippet&forUsername="
                        + str(userName)
                        + "&key="
                        + str(key)
                    )
                    headers3 = {"Accept": "application/json"}
                    req3 = requests.get(url3, data="", headers=headers3, verify=True)
                    res3 = req3.json()
                    if res3:
                        print(res3)
                        row = [name, sa, hrefText]
                        with open(UPLOAD_DIR + "/top500youtube.csv", "a") as csvFile:
                            writer = csv.writer(csvFile)
                            writer.writerow(row)
                        csvFile.close()
    return response.json({"status": "success"})
    userName = "1veritasium"
    key = "AIzaSyBdGz2XZGbICW4mh39P4Hrl0NyqnEwosnA"
    url3 = (
        "https://www.googleapis.com/youtube/v3/channels?part=snippet&forUsername="
        + userName
        + "&key="
        + key
    )
    headers3 = {"Accept": "application/json"}
    req3 = requests.get(url3, data="", headers=headers3, verify=True)
    res3 = req3.json()

    return response.json({"status": "success", "response_channel": res3})
