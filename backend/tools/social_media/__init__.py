from sanic import Blueprint

from .scriptsfb import scriptsfb
from .scriptstwitter import scriptstwitter
from .scriptslinkedin import scriptslinkedin
from .scriptsyoutube import scriptsyoutube
from .scriptstraffic import scriptstraffic
from .get_top500_twitter_usernames import scriptstoptwitter
from .get_top500_facebook_usernames import scriptstopfacebook
from .get_top500_youtube_usernames import scriptstopyoutube
from .get_top500_merge import scriptsmerge
from .scriptsfetch import scriptsfetch

v1_script = Blueprint.group(
    scriptsfb,
    scriptstwitter,
    scriptslinkedin,
    scriptsyoutube,
    scriptstraffic,
    scriptstoptwitter,
    scriptstopfacebook,
    scriptstopyoutube,
    scriptsmerge,
    scriptsfetch,
    url_prefix="/v1",
)
