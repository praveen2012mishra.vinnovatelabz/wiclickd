"""
Script to fetch top 500 websites
Created on 20.05.19
Created By Weclikd Team
"""
from sanic import Blueprint, response
from bs4 import BeautifulSoup
import requests
import sys
import json
from math import ceil
import os


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
UPLOAD_DIR = os.path.join(BASE_DIR, "static", "download")
scriptstraffic = Blueprint("scriptstraffic", url_prefix="/topwebsite")

BASE_URL = "http://www.alexa.com/topsites/countries;%d/%s"
# BASE_URL='http://www.alexa.com/topsites'


@scriptstraffic.route("/fetchfacebook", methods=["GET"])
async def fetchtopsitesfb(request):
    import csv

    response_got = requests.get("https://socialblade.com/facebook/top/500")

    soup = BeautifulSoup(response_got.text, "html.parser")
    # Pull html from all instances of <div> tag having class content-module-wide
    artist_name_list = soup.find(class_="content-module-wide")

    # Pull text from all instances of <a> tag within content-module-wide div
    artist_name_list_items = artist_name_list.find_all("a")
    count = 0

    anotherArray = {}
    for artist_name in artist_name_list_items:
        resultArray = {}
        # print(artist_name.prettify())
        resultArray["link"] = artist_name.prettify()
        resultArray["username"] = artist_name.contents[0]
        anotherArray[count] = resultArray
        count = count + 1

        with open(UPLOAD_DIR + "/twitter.csv", "w", newline="") as f:
            writer = csv.writer(f)
            # writer.writecols("username","link")
            writer.writerows(artist_name.contents[0] + " " + artist_name.prettify())

    return response.json({"status": "success", "result": anotherArray})


@scriptstraffic.route("/fetch", methods=["GET"])
async def fetchtopsites(request):
    import csv

    response_got = requests.get("https://socialblade.com/twitter/top/10/followers")

    soup = BeautifulSoup(response_got.text, "html.parser")
    # Pull html from all instances of <div> tag having class content-module-wide
    artist_name_list = soup.find(class_="content-module-wide")

    # Pull text from all instances of <a> tag within content-module-wide div
    artist_name_list_items = artist_name_list.find_all("a")
    count = 0

    anotherArray = {}
    for artist_name in artist_name_list_items:
        resultArray = {}
        # print(artist_name.prettify())
        resultArray["link"] = artist_name.prettify()
        resultArray["username"] = artist_name.contents[0]
        anotherArray[count] = resultArray
        count = count + 1
    # print('maitrayee')
    # print([anotherArray])
    # print('----------')
    with open(UPLOAD_DIR + "/twitter.csv", "w", newline="\n") as output:
        writer = csv.writer(output)
        for val in anotherArray:
            # print('maitrayee')
            # print(key)
            print(anotherArray)
            # print('-------------')
            writer.writerows([anotherArray])

    return response.json({"status": "success", "result": anotherArray})


@scriptstraffic.route("/", methods=["GET"])
async def topsites(request):

    country_code = ""
    number = 1
    delimiter = " "
    page_num = 1

    response_got = requests.get(BASE_DIR % (page_num, country_code))
    # print('maitrayee')
    print(response_got)
    return response.json({"status": "success", "response_got": response_got})
