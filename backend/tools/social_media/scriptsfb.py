"""
Script to fetch Facebook account details
Created on 22.05.19
Created By Weclikd Team
"""

from sanic import Blueprint, response
import facebook as fb
import json

scriptsfb = Blueprint("scriptsfb", url_prefix="/facebook")


@scriptsfb.route("/", methods=["GET"])
async def facebook(request):
    args = {"fields": "id,name,email"}
    access_token = fb.GraphAPI().get_app_access_token(
        "339139683411520", "9eb518d652c6bd0b070b6c80b4c57afb", True
    )
    graph = fb.GraphAPI(
        "EAADZAQVZA4sZCYBAI3d31nnokBXoipRwssED29SYmtVAZBK8eBQcD1L18vjLdDOwSZCwSPYAOPlS74y0SeFO4tJMqbPIoUFyTbWS9PIj4LKFrZAbxKzBfZAMKm8jHkdkurICUPFZCa1pYY98QPtJEczxD9eRNiPEPSGpM0eodDl2AzPC2SzAbFRd4k7ulTcTczrebJOrida0ZCQZDZD"
    )
    user = "BillGates"  # "BillGates"
    profile = graph.get_object(
        "me", fields="id,name,first_name,last_name,website,about"
    )

    posts = graph.get_connections(profile["id"], "posts")
    return response.json({"status": "success", "profile": profile})
