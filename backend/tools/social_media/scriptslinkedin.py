"""
Script to fetch Linkedin account details
Created on 22.05.19
Created By Weclikd Team
"""

from sanic import Blueprint, response
from linkedin_api import Linkedin
import json


scriptslinkedin = Blueprint("scriptslinkedin", url_prefix="/linkedin")

ACCESS_TOKEN = ""
# authentication = {}


@scriptslinkedin.route("/", methods=["GET"])
async def linkedinfetch(request):
    # Authenticate using any Linkedin account credentials
    api = Linkedin("kazi@natitsolved.com", "Natit@123")

    # GET a profile
    profile = api.get_profile("kazi-hasan-ali-0a1a97178")
    # profile = api.get_profile('maitrayee-bhaumik-39125396')

    # GET a profiles contact info
    contact_info = api.get_profile_contact_info("kazi-hasan-ali-0a1a97178")

    return response.json(
        {"status": "success", "contact_info": contact_info, "profile": profile}
    )

    # GET all connected profiles (1st, 2nd and 3rd degree) of a given profile
    connections = api.get_profile_connections("1234asc12304", max_connections=200)
