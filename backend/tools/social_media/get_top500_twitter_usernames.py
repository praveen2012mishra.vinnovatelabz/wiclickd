"""
Script to fetch top 500 twitter account details
Created on 29.05.19
Created By Weclikd Team
"""


from sanic import Blueprint, response
from bs4 import BeautifulSoup
import requests
import sys
import json
import os
import twitter as tw


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
UPLOAD_DIR = os.path.join(BASE_DIR, "static", "download")

scriptstoptwitter = Blueprint("scriptstoptwitter", url_prefix="/toptwitter")


@scriptstoptwitter.route("/fetch", methods=["GET"])
async def fetchtoptwitter(request):
    import csv

    response_got = requests.get("https://socialblade.com/twitter/top/100/followers")

    soup = BeautifulSoup(response_got.text, "html.parser")
    # Pull html from all instances of <div> tag having class content-module-wide
    artist_name_list = soup.find(class_="content-module-wide")

    # Pull text from all instances of <a> tag within content-module-wide div
    artist_name_list_items = artist_name_list.find_all("a")
    count = 0

    anotherArray = {}

    api = tw.Api(
        consumer_key="87NkcIeJMQ1akspJmcIciFSIr",
        consumer_secret="qRYQXIGI8MYbmSgwRB3ZDbFjE1E4jtewjhHKzQwGXRiIWKbTtD",
        access_token_key="1130371016406867968-q0UrifgNWgVlSAoS1t0XLCww6PoyHP",
        access_token_secret="ptIlk8FqlK5T6PMSwru6tkSXIM5C6VOca0k1FloXQ3YRn",
    )
    for artist_name in artist_name_list_items:
        userDetails = {}
        resultArray = {}
        url = ""
        if artist_name.contents[0] == "ddlovato":
            print("not")
        elif artist_name.contents[0] == "TweetRAMALAN":
            print("not")
        else:
            userDetails = api.GetUser(
                screen_name=artist_name.contents[0], return_json=True
            )
            # print(userDetails)
            # return response.json({ 'userDetails': userDetails})
            if userDetails:
                if userDetails["url"]:
                    url = userDetails["url"]
                else:
                    url = ""
                # print(userDetails)

                row = [
                    userDetails["name"],
                    userDetails["screen_name"],
                    userDetails["description"],
                    url,
                ]

                with open(UPLOAD_DIR + "/top500twitter.csv", "a") as csvFile:
                    writer = csv.writer(csvFile)
                    writer.writerow(row)

                csvFile.close()

    return response.json({"status": "success"})
