"""
Script to fetch top 500 facebook account details
Created on 29.05.19
Created By Weclikd Team
"""
from sanic import Blueprint, response
import facebook as fb
import json
from bs4 import BeautifulSoup
import requests
import os


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
UPLOAD_DIR = os.path.join(BASE_DIR, "static", "download")

scriptstopfacebook = Blueprint("scriptstopfacebook", url_prefix="/topfacebook")


@scriptstopfacebook.route("/fetch", methods=["GET"])
async def fetchtopfacebook(request):

    import csv

    response_got = requests.get("https://socialblade.com/facebook/top/500")

    soup = BeautifulSoup(response_got.text, "html.parser")

    # Pull html from all instances of <div> tag having class content-module-wide
    artist_name_list = soup.findAll("div", style="float: right; width: 900px;")
    # artist_name_list_items = artist_name_list.find_all('a')
    for text in artist_name_list:
        download = text.find_all("a")
        for text in download:
            hrefText = text["href"]
            # print('--||--')
            splitArray = hrefText.split("/facebook/page/")
            # print()
            for key, sa in enumerate(splitArray):

                # print(sa)
                name = text.contents[0]
                # print(splitArray[0])
                # print()
                # print(text)
                if sa:
                    row = [name, sa, hrefText]
                    with open(UPLOAD_DIR + "/top500fb.csv", "a") as csvFile:
                        writer = csv.writer(csvFile)
                        writer.writerow(row)
                    csvFile.close()
        # writer = csv.writer(csvFile)
        # writer.writerow(row)
        # csvFile.close()
    # print('maitrayee')
    # print(artist_name_list_items)
    # print('---------------------')

    # Pull text from all instances of <a> tag within content-module-wide div
    # artist_name_list_items = artist_name_list.find_all('a')

    return response.json({"message": "success"})

    # with open('top500fb.html', 'r') as f:
    #    	i = 0
    #    	for line in f:
    #        	if '/facebook/page' in line:
    #            username_start = line.find('/facebook/page') + 15
    #            username_end = line.find('"', username_start)
    #            name_start = line.find('>', username_end) + 1
    #            name_end = line.find('<', name_start)
    #            print("{} | {}".format(line[username_start:username_end], line[name_start:name_end]))
    #            i += 1
    #    	print("Found {}".format(i))

    # args = {'fields' : 'id,name,email'}
    # access_token = fb.GraphAPI().get_app_access_token(
    #            '339139683411520', '9eb518d652c6bd0b070b6c80b4c57afb', True
    #        )
    # graph = fb.GraphAPI("EAADZAQVZA4sZCYBAI3d31nnokBXoipRwssED29SYmtVAZBK8eBQcD1L18vjLdDOwSZCwSPYAOPlS74y0SeFO4tJMqbPIoUFyTbWS9PIj4LKFrZAbxKzBfZAMKm8jHkdkurICUPFZCa1pYY98QPtJEczxD9eRNiPEPSGpM0eodDl2AzPC2SzAbFRd4k7ulTcTczrebJOrida0ZCQZDZD")
    # user ="BillGates" # "BillGates"
    # profile = graph.get_object('me',fields='id,name,first_name,last_name,website,about')

    # posts = graph.get_connections(profile["id"], "posts")
    return response.json({"status": "success"})
