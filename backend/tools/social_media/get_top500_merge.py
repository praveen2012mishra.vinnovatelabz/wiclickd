"""
Script to fetch top 500 merge details
Created on 24.05.19
Created By Weclikd Team
"""
from sanic import Blueprint, response
import json
from bs4 import BeautifulSoup
import re
import os
import requests
import csv


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
UPLOAD_DIR = os.path.join(BASE_DIR, "static", "download")

scriptsmerge = Blueprint("scriptsmerge", url_prefix="/merge")


@scriptsmerge.route("/fetch", methods=["GET"])
async def topmerge(request):
    csvs = [
        UPLOAD_DIR + "/top500fb.csv",
        UPLOAD_DIR + "/top500twitter.csv",
        UPLOAD_DIR + "/top500youtube.csv",
    ]

    # importing csv module
    import csv

    # csv file name
    # filename = UPLOAD_DIR+'/top500fb.csv'
    filename = UPLOAD_DIR + "/top500youtube.csv"

    # initializing the titles and rows list
    fields = []
    rows = []

    with open(filename, "r") as redFile:
        csvreader = csv.reader(redFile)
        with open(UPLOAD_DIR + "/top500merge.csv", "a") as csvfile:
            for row in csvreader:
                print(row)
                writer = csv.writer(csvfile)
                writer.writerow(row)

    # reading csv file

    # creating a csv reader object
    # with open(filename,'r') as redFile:
    # 	csvreader = csv.reader(redFile)
    # 	for row in csvreader:
    #  	print(row)
    #  	writer = csv.writer(csvfile)
    #  	writer.writerow(row)

    # extracting field names through first row
    # fields = csvreader.next()

    # extracting each data row one by one
    # for row in csvreader:
    # 	print(row)
    # 	writer = csv.writer(csvfile)
    # 	writer.writerow(row)
    # rows.append(row)
    # writer = csv.writer(csvfile)
    # writer.writerow(row)
    # writer.writerow(row)

    # get total number of rows
    # print("Total no. of rows: %d"%(csvreader.line_num))

    return response.json({"message": "success"})
