"""
Script to fetch youtube channel and video details
Created on 24.05.19
Created By Weclikd Team
"""
from sanic import Blueprint, response
from collections import OrderedDict
from pytube import YouTube
import json
import re
import logger
import pprint

import os
import requests
import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors
import googleapiclient.errors

scopes = ["https://www.googleapis.com/auth/youtube.readonly"]


# from urllib2 import quote

logger = logger.getLogger("weclik")
scriptsyoutube = Blueprint("scriptsyoutube", url_prefix="/youtube")

ACCESS_TOKEN = ""
# authentication = {}


@scriptsyoutube.route("/", methods=["GET"])
async def youtube(request):
    userName = "1veritasium"
    key = "AIzaSyBdGz2XZGbICW4mh39P4Hrl0NyqnEwosnA"
    url3 = (
        "https://www.googleapis.com/youtube/v3/channels?part=snippet&forUsername="
        + userName
        + "&key="
        + key
    )
    headers3 = {"Accept": "application/json"}
    req3 = requests.get(url3, data="", headers=headers3, verify=True)
    res3 = req3.json()

    id_val = "fHsa9DqmId8"
    # id_val = 'FuXNumBwDOM'
    url4 = (
        "https://www.googleapis.com/youtube/v3/videos?part=snippet&id="
        + id_val
        + "&key="
        + key
    )
    headers4 = {"Accept": "application/json"}
    req4 = requests.get(url4, data="", headers=headers4, verify=True)
    res4 = req4.json()

    """get channnels old """

    client_id = (
        "762651667388-r6dtvmfqhlorbvvrjil0icinig3r3c9h.apps.googleusercontent.com"
    )
    client_secret = "xrMUv1dV0xc_eh0ck9SNqHnZ"

    # userName = '1veritasium'
    # url3='https://www.googleapis.com/youtube/v3/channels?part=contentDetails%2CcontentOwnerDetails%2Csnippet&forUsername='+userName+'&key='+key
    # headers3 = {'Accept':'application/json', 'Authorization':'Bearer ya29.GlwWB_NcHbz3UfPFnZhl9yIjFFY5hrs_Ou28me0LgGsb2TEI4puQZed8x_N5fPdPl6PqvFloYwh2lCeylfsS3GBEdRS-DgSkxQcjrlqMV9Uk4IjOdLp1QqwnQvex2Q','Client-ID': client_id}
    # req3=requests.get(url3,data='',headers=headers3,verify=True)
    # res3=req3.json()
    # jsonData=res3["streams"]

    # id_val = 'fHsa9DqmId8'
    # # id_val = 'FuXNumBwDOM'
    # url4='https://www.googleapis.com/youtube/v3/videos?part=contentDetails%2Csnippet&id='+id_val+'&key='+key
    # headers4 = {'Accept':'application/json', 'Authorization':'Bearer ya29.GlwWB_NcHbz3UfPFnZhl9yIjFFY5hrs_Ou28me0LgGsb2TEI4puQZed8x_N5fPdPl6PqvFloYwh2lCeylfsS3GBEdRS-DgSkxQcjrlqMV9Uk4IjOdLp1QqwnQvex2Q','Client-ID': client_id}
    # req4=requests.get(url4,data='',headers=headers4,verify=True)
    # res4=req4.json()

    return response.json(
        {"status": "success", "response_channel": res3, "response_videos": res4}
    )


def eurl(video_id):
    return "https://youtube.googleapis.com/v/{}".format(video_id)


def video_info_url(
    video_id,
    watch_url,
    watch_html,
    embed_html,
    age_restricted,
):
    """Construct the video_info url.
    :param str video_id:
        A YouTube video identifier.
    :param str watch_url:
        A YouTube watch url.
    :param str watch_html:
        The html contents of the watch page.
    :param str embed_html:
        The html contents of the embed page (for age restricted videos).
    :param bool age_restricted:
        Is video age restricted.
    :rtype: str
    :returns:
        :samp:`https://youtube.com/get_video_info` with necessary GET
        parameters.
    """
    if age_restricted:
        sts = regex_search(r'"sts"\s*:\s*(\d+)', embed_html, group=1)
        # Here we use ``OrderedDict`` so that the output is consistent between
        # Python 2.7+.
        params = OrderedDict(
            [
                ("video_id", video_id),
                ("eurl", eurl(video_id)),
                ("sts", sts),
            ]
        )
    else:
        params = OrderedDict(
            [
                ("video_id", video_id),
                ("el", "$el"),
                ("ps", "default"),
                ("eurl", '"' + watch_url + '"'),
                ("hl", "en_US"),
            ]
        )
    return json.dumps(params)
    # return 'https://youtube.com/get_video_info?' + urlencode(params)


def regex_search(pattern, string, groups=False, group=None, flags=0):
    """Shortcut method to search a string for a given pattern.
    :param str pattern:
        A regular expression pattern.
    :param str string:
        A target string to search.
    :param bool groups:
        Should the return value be ``.groups()``.
    :param int group:
        Index of group to return.
    :param int flags:
        Expression behavior modifiers.
    :rtype:
        str or tuple
    :returns:
        Substring pattern matches.
    """
    if type(pattern) == list:
        for p in pattern:
            regex = re.compile(p, flags)
            results = regex.search(string)
            if not results:
                raise RegexMatchError(
                    "regex pattern ({pattern}) had zero matches".format(pattern=p),
                )
            else:
                logger.debug(
                    "finished regex search: %s",
                    pprint.pformat(
                        {
                            "pattern": p,
                            "results": results.group(0),
                        },
                        indent=2,
                    ),
                )
                if groups:
                    return results.groups()
                elif group is not None:
                    return results.group(group)
                else:
                    return results
    else:
        regex = re.compile(pattern, flags)
        results = regex.search(string)
        if not results:
            raise RegexMatchError(
                "regex pattern ({pattern}) had zero matches".format(pattern=pattern),
            )
        else:
            logger.debug(
                "finished regex search: %s",
                pprint.pformat(
                    {
                        "pattern": pattern,
                        "results": results.group(0),
                    },
                    indent=2,
                ),
            )
            if groups:
                return results.groups()
            elif group is not None:
                return results.group(group)
            else:
                return results
