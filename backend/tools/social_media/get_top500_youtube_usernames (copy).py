"""
Script to fetch top 500 youtube channel details
Created on 24.05.19
Created By Weclikd Team
"""
from sanic import Blueprint, response
import json
from bs4 import BeautifulSoup
import re
import os
import requests


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
UPLOAD_DIR = os.path.join(BASE_DIR, "static", "download")

scriptstopyoutube = Blueprint("scriptstopyoutube", url_prefix="/topyoutube")


@scriptstopyoutube.route("/fetch", methods=["GET"])
async def topyoutube(request):
    import csv

    response_got = requests.get("https://socialblade.com/youtube/top/50")

    soup = BeautifulSoup(response_got.text, "html.parser")

    # Pull html from all instances of <div> tag having class content-module-wide
    artist_name_list = soup.findAll("div", style="float: right; width: 900px;")
    # artist_name_list_items = artist_name_list.find_all('a')
    resultArray = {}
    count = 0
    for text in artist_name_list:
        download = text.find_all("a")
        for text in download:
            hrefText = text["href"]
            # print('--||--')
            splitArray = hrefText.split("/youtube/c/")
            # print()

            for key, sa in enumerate(splitArray):
                res3 = ""
                # print(sa)
                name = text.contents[0]
                userName = name
                # userName = '1veritasium'
                key = "AIzaSyBdGz2XZGbICW4mh39P4Hrl0NyqnEwosnA"
                url3 = (
                    "https://www.googleapis.com/youtube/v3/channels?part=snippet&forUsername="
                    + userName
                    + "&key="
                    + key
                )
                headers3 = {"Accept": "application/json"}
                req3 = requests.get(url3, data="", headers=headers3, verify=True)
                res3 = req3.json()
                row = []
                if res3["items"]:
                    print("if")
                    row = [name, sa, hrefText]
                    # resultArray[count] = res3
                    # count= count+1
                    # with open(UPLOAD_DIR+'/top500youtube.csv', 'a') as csvFile:
                    # 	writer = csv.writer(csvFile)
                    # 	writer.writerow(row)
                    # csvFile.close()
                    # else:
                    # 	print("else")
                    # 	row = [name, sa, '', hrefText]

                    print(row)
                    resultArray[count] = res3
                    count = count + 1
                    with open(UPLOAD_DIR + "/top500youtube.csv", "a") as csvFile:
                        writer = csv.writer(csvFile)
                        writer.writerow(row)
                    csvFile.close()
                    # id_val = 'fHsa9DqmId8'
                    # url4='https://www.googleapis.com/youtube/v3/videos?part=snippet&id='+id_val+'&key='+key
                    # headers4 = {'Accept':'application/json'}
                    # req4=requests.get(url4,data='',headers=headers4,verify=True)
                    # res4=req4.json()

                    # row = [name, sa, '', hrefText]

                    # print(res4)
    return response.json({"status": "success", "response_channel": res3})
