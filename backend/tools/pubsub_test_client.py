import os
import logging
from google.cloud import pubsub_v1
from google.protobuf import json_format


def publish(topic, session, proto):
    project_id = os.environ.get("PROJECT_ID", "project-test")
    publisher = pubsub_v1.PublisherClient()

    logging.info(f"Publishing to {topic}\n{json_format.MessageToJson(proto)}")
    future = publisher.publish(
        publisher.topic_path(project_id, topic),
        data=proto.SerializeToString(),
        **session._session,
    )
    result = future.result()
    logging.info(f"Result: {result}")
    return result
