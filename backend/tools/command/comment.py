import click
import json
from typing import Optional, Dict, List
from command import command_utils


def create(
    parent_content_id: str, text: str = "This is a comment", clik: Optional[str] = None
) -> Dict:
    return command_utils.run_graphql_query(
        "comment_create",
        parent_content_id=command_utils.to_global_id("Comment", parent_content_id),
        text=text,
        clik=clik,
    )


def delete(content_id: str) -> Dict:
    return command_utils.run_graphql_query(
        "content_delete",
        content_id=command_utils.to_global_id("Post", content_id),
    )


def edit(comment_id: str, text: str) -> Dict:
    return command_utils.run_graphql_query(
        "comment_edit",
        content_id=command_utils.to_global_id("Comment", comment_id),
        text=text,
    )


def comments(
    content_id: str,
    first: int = 3,
    after: Optional[str] = None,
    clik_id: Optional[str] = None,
    clik_ids: List[str] = None,
    sort: str = None,
) -> Dict:
    clik_ids = clik_ids or []
    return command_utils.run_graphql_query(
        "comments",
        id=command_utils.to_global_id("Post", content_id),
        first=first,
        after=after,
        clik_ids=[command_utils.to_global_id("Clik", clik_id)] if clik_id else clik_ids,
        sort=sort,
    )


def info(comment_id: Optional[str] = None) -> Dict:
    return command_utils.run_graphql_query(
        "comment",
        id=command_utils.to_global_id("Comment", comment_id),
        first=0,
        after=None,
        clik_ids=[],
    )


def report(comment_id: str, reports: List[Dict]) -> Dict:
    return command_utils.run_graphql_query(
        "content_report",
        content_id=command_utils.to_global_id("Comment", comment_id),
        reports=reports,
    )


def like(comment_id: int, like_type: str) -> Dict:
    return command_utils.run_graphql_query(
        "content_like",
        content_id=command_utils.to_global_id("Comment", comment_id),
        like_type=like_type,
    )


@click.group()
def comment():
    pass


@comment.command("delete")
@click.option("--comment_id", "-i", required=True)
def delete_command(comment_id):
    response = delete(content_id=comment_id)
    print(json.dumps(response, indent=2))
