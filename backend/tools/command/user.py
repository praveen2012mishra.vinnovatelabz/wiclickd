import click
import json
from typing import Dict, Optional, Union
from command import command_utils
from command.upload_picture import upload_picture


def profile(user_id: Union[str, int]) -> Dict:
    return command_utils.run_graphql_query(
        "user - profile", id=command_utils.to_global_id("User", user_id)
    )


def edit(
    username: Optional[str] = None,
    profile_pic: Union[int, str, None] = None,
    banner_pic: Union[int, str, None] = None,
    full_name: Optional[str] = None,
    description: Optional[str] = None,
) -> Dict:
    return command_utils.run_graphql_query(
        "user_edit",
        username=username,
        profile_pic=str(profile_pic) if profile_pic else None,
        banner_pic=str(banner_pic) if profile_pic else None,
        full_name=full_name if full_name else username,
        description=description,
    )


def follow(user_id: str, follow_type: str = "FOLLOW") -> Dict:
    return command_utils.run_graphql_query(
        "user_follow",
        user_id=command_utils.to_global_id("User", user_id),
        follow_type=follow_type,
    )


def unfollow(user_id: str) -> Dict:
    return command_utils.run_graphql_query(
        "user_unfollow",
        user_id=command_utils.to_global_id("User", user_id),
    )


def feed(
    user_id: str, first: int = 10, sort: str = "DEFAULT", after: Optional[str] = None
) -> Dict:
    return command_utils.run_graphql_query(
        "user - feed",
        id=command_utils.to_global_id("User", user_id),
        first=first,
        sort=sort,
        after=after,
    )


def list(first: int = 10, after: str = None, sort: str = "TRENDING"):
    return command_utils.run_graphql_query(
        "user_list", first=first, after=after, sort=sort
    )


def search(query: str) -> Dict:
    return command_utils.run_graphql_query("search - everything", users=[query])


@click.group()
def user():
    """Commands related to a user"""
    pass


@user.command("edit")
@click.option("--username", "-u")
@click.option("--profile_pic", "-p")
@click.option("--banner_pic", "-b")
@click.option("--full_name", "-n")
@click.option("--description", "-d")
def edit_profile_command(username, profile_pic, banner_pic, full_name, description):
    """Get user profile info"""
    response = edit(
        username=username,
        profile_pic=upload_picture(profile_pic),
        banner_pic=upload_picture(banner_pic),
        full_name=full_name,
        description=description,
    )
    print(json.dumps(response, indent=2))


@user.command("profile")
@click.option("--user_id", "-i", required=True)
def get_profile_command(user_id):
    """Get user profile info"""
    response = profile(user_id)
    print(json.dumps(response, indent=2))


@user.command("feed")
@click.option("--user_id", "-i")
@click.option("--sort", "-s", default="NEW")
@click.option("--first", "-f", type=int, default=10)
def get_feed_command(user_id, sort, first):
    if not user_id:
        user_id = command_utils.get_current_user()
    response = feed(user_id, first=first, sort=sort)
    print(json.dumps(response, indent=2))
