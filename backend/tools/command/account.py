import click
import jwt
import json
from typing import Dict, Optional
from command import command_utils, account_settings
from eros import firebase
import base64
import os


def create(
    firebase_id: str = "id1",
    first_name: str = "John",
    last_name: str = "Doe",
    email: Optional[str] = None,
    username: Optional[str] = "username",
    admin: bool = True,
    inviter_username: str = None,
    clik_invite_key: str = None,
) -> Dict:
    if not email:
        email = f"test@example.com"
    if firebase_id:
        try:
            base64.b64decode(firebase_id)
        except:
            firebase_id = base64.b64encode(firebase_id.encode()).decode()
        command_utils.clear_auth()
        command_utils.set_auth(
            firebase_auth=create_firebase_jwt(firebase_id, admin=admin)
        )
    response = command_utils.run_graphql_query(
        "account_create",
        first_name=first_name,
        last_name=last_name,
        email=email,
        username=username,
        inviter_username=inviter_username,
        clik_invite_key=clik_invite_key,
    )
    return response


def info() -> Dict:
    return command_utils.run_graphql_query(
        "account",
    )


def balance() -> Dict:
    return command_utils.run_graphql_query(
        "account - balance",
    )


def payment_info() -> Dict:
    return command_utils.run_graphql_query(
        "account - payment_info",
    )


def my_user() -> Dict:
    return command_utils.run_graphql_query(
        "my_user", id=f"MyUser:{command_utils.current_auth.account_id}"
    )


def login(firebase_id: str = "id1", admin=True) -> Dict:
    try:
        base64.b64decode(firebase_id)
    except:
        firebase_id = base64.b64encode(firebase_id.encode()).decode()
    command_utils.clear_auth()
    command_utils.set_auth(firebase_auth=create_firebase_jwt(firebase_id, admin=admin))
    response = command_utils.run_graphql_query(
        "login",
    )
    return response


def logout():
    command_utils.clear_auth()


def change_subscription(type: str, payment_id: str = None) -> Dict:
    return command_utils.run_graphql_query(
        "account_change_subscription", type=type, payment_id=payment_id
    )


def change_payment_info(payment_id: str = None, email: str = None) -> Dict:
    return command_utils.run_graphql_query(
        "account_change_payment_info", payment_id=payment_id, email=email
    )


def change_access_key() -> Dict:
    return command_utils.run_graphql_query("account_change_access_key")


def check_username(username: str) -> Dict:
    return command_utils.run_graphql_query("username_check", username=username)


def promote(firebase_id: str, admin: bool) -> Dict:
    # import firebase_admin
    # import os
    # creds = firebase_admin.credentials.Certificate(os.environ.get('GOOGLE_APPLICATION_CREDENTIALS'))
    # default_app = firebase_admin.initialize_app(creds)
    if not os.environ.get("TESTING"):
        claims = firebase.modify_claim_sync(
            firebase_id=firebase_id, name="admin", value=admin
        )
    else:
        claims = {"admin": True}

    async def modify_db():
        from weclikd.service import sql
        from weclikd.utils import authorization
        from eros.db.user_db import account_db

        await sql.start()
        account = await account_db.get(
            id=authorization.get_account_id_from_firebase_id(firebase_id)
        )
        account.custom_claims.admin = admin
        await account_db.upsert(account, id=account.id)
        await sql.stop()

    import asyncio

    asyncio.run(modify_db())
    return claims


def notifications(first: int = 3, after: str = None):
    return command_utils.run_graphql_query(
        "account - notifications", first=first, after=after
    )


def num_notifications():
    return command_utils.run_graphql_query("account - num_unread_notifications")


def mark_notifications_as_read():
    return command_utils.run_graphql_query("account_mark_notifications_as_read")


def delete_notification(notification_id):
    return command_utils.run_graphql_query(
        "account_notification_delete",
        notification_id=command_utils.to_global_id("Notification", notification_id),
    )


@click.group()
def account():
    """Commands related to an account"""
    pass


@account.command("create")
@click.option("--firebase_id", "-F")
@click.option("--first_name", "-f")
@click.option("--last_name", "-l")
@click.option("--email", "-e", required=True)
@click.option("--username", "-u")
@click.option("--admin", "-a", is_flag=True)
def create_command(firebase_id, first_name, last_name, email, username, admin):
    """Add a new account. Optionally create a new user."""
    response = create(
        firebase_id=firebase_id,
        first_name=first_name,
        last_name=last_name,
        email=email,
        username=username,
        admin=admin,
    )
    print(json.dumps(response, indent=2))


@account.command("info")
def info_command():
    """Get account info"""
    response = info()
    print(json.dumps(response, indent=2))


@account.command("my_user")
def my_user_command():
    """Get my user info"""
    response = my_user()
    print(json.dumps(response, indent=2))


@account.command("promote")
@click.option("--firebase_id", "-F")
@click.option("--admin", "-a", is_flag=True)
def promote_command(firebase_id, admin):
    response = promote(firebase_id, admin)
    print(json.dumps(response, indent=2))


@account.command("change_access_key")
def access_key_command():
    response = change_access_key()
    print(json.dumps(response, indent=2))


def create_firebase_jwt(user_id=None, admin=False):
    if not user_id:
        user_id = "firebase-uid"
    firebase_auth = {"user_id": user_id, "uid": user_id, "email_verified": True}
    if admin:
        firebase_auth["admin"] = True
    return jwt.encode(firebase_auth, "secret", algorithm="HS256")


account.add_command(account_settings.account_settings)
