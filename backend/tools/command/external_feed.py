import json
import click
import html
import extraction
import requests
import asyncio
from bs4 import BeautifulSoup
from typing import Dict, Optional
from command import command_utils
from weclikd.service import pubsub
from artemis.event.external_feed_event_pb2 import (
    PollAllExternalFeedsEvent,
    ProcessExternalFeedItemEvent,
)

from artemis.db.external_feed_db import feed_db
from artemis.model.rss import RSSFeed
from artemis.extract_url import unfurl
from weclikd.service import sql


def create(
    feed_name: str,
    url: str,
    topic: str,
    website: str,
    icon_url=None,
    summary_source: str = None,
) -> Dict:
    return command_utils.run_graphql_query(
        "external_feed_create",
        name=feed_name,
        url=url,
        website=website,
        base_topic=topic,
        icon_url=icon_url,
        summary_source="UNFURL" if summary_source == "unfurl" else "RSS_DESCRIPTION",
    )


def edit(
    feed_id: int,
    feed_name: str = None,
    topic: str = None,
    website: str = None,
    icon_url=None,
    summary_source: str = None,
) -> Dict:
    return command_utils.run_graphql_query(
        "external_feed_edit",
        feed_id=command_utils.to_global_id("ExternalFeed", feed_id),
        name=feed_name,
        website=website,
        base_topic=topic,
        icon_url=icon_url,
        summary_source="UNFURL" if summary_source == "unfurl" else "RSS_DESCRIPTION",
    )


def profile(feed_id: int):
    return command_utils.run_graphql_query(
        "external_feed - profile",
        id=command_utils.to_global_id("ExternalFeed", feed_id),
    )


def feed(
    feed_id: int, first: int = 10, sort: str = "DEFAULT", after: Optional[str] = None
):
    return command_utils.run_graphql_query(
        "external_feed - feed",
        id=command_utils.to_global_id("ExternalFeed", feed_id),
        first=first,
        sort=sort,
        after=after,
    )


def delete(feed_id: str) -> Dict:
    return command_utils.run_graphql_query(
        "external_feed_delete",
        feed_id=feed_id,
    )


def list(sort: str = "TRENDING", first=3, after: str = None) -> Dict:
    return command_utils.run_graphql_query(
        "external_feeds", first=first, sort=sort, after=after
    )


def poll():
    return pubsub.Subscription.push(
        url=f"http://{command_utils.JANUS_HOST}/artemis/external-feed-poll",
        message=PollAllExternalFeedsEvent(),
    )


def process(feed_name: str):
    pubsub.Subscription.push(
        url=f"http://{command_utils.JANUS_HOST}/artemis/process_external_feed",
        message=ProcessExternalFeedItemEvent(feed_name=feed_name),
    )


def normalize_text(text):
    soup = BeautifulSoup(text, "html.parser")
    whitelist = ["p", "[document]", "em", "a"]
    text_elements = [t for t in soup.find_all(text=True) if t.parent.name in whitelist]
    return "".join(
        [html.unescape(str(each)) for each in text_elements if str(each).strip()]
    )


def test(url: str) -> Dict:
    return command_utils.run_graphql_query("external_feed_test", url=url)


def follow(feed_id: str, follow_type: str = "FOLLOW") -> Dict:
    return command_utils.run_graphql_query(
        "external_feed_follow",
        feed_id=command_utils.to_global_id("ExternalFeed", feed_id),
        follow_type=follow_type,
    )


def unfollow(feed_id: str) -> Dict:
    return command_utils.run_graphql_query(
        "external_feed_unfollow",
        feed_id=command_utils.to_global_id("ExternalFeed", feed_id),
    )


def list(first: int = 10, sort: str = "TRENDING", after: str = None):
    return command_utils.run_graphql_query(
        "external_feed_list", first=first, sort=sort, after=after
    )


def search(query: str) -> Dict:
    return command_utils.run_graphql_query("search - everything", feeds=[query])


@click.group("external_feed")
def external_feed():
    pass


@external_feed.command("create")
@click.option("--name", "-n", required=True)
@click.option("--url", "-u", required=True)
@click.option("--website", "-w", required=True)
@click.option("--topic", "-t", required=True)
@click.option("--icon_url", "-i", required=True)
@click.option(
    "--summary_source", "-s", type=click.Choice(["rss", "unfurl"]), required=True
)
def create_command(name, url, website, topic, icon_url, summary_source):
    response = create(
        feed_name=name,
        url=url,
        website=website,
        topic=topic,
        icon_url=icon_url,
        summary_source=summary_source,
    )
    print(json.dumps(response, indent=2))


@external_feed.command("delete")
@click.option("--id", "-i")
def delete_command(id):
    response = delete(feed_id=id)
    print(json.dumps(response, indent=2))


@external_feed.command("list")
@click.option("--first", "-f")
@click.option("--after", "-a")
def list_command(first, after):
    response = list(first=first, after=after)
    print(json.dumps(response, indent=2))


@external_feed.command("test")
@click.option("--url", "-u", required=True)
def test_command(url):
    response = test(url)
    print(json.dumps(response, indent=2))

    async def _parse_rss(url):
        rss, error = await RSSFeed.parse(url)
        if error:
            print(error)
            return

        headers = {
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36"
        }
        print(json.dumps(rss._rss.feed, indent=2))
        print(f"Link: {rss.link}")
        print(f"Last Update: {rss.last_update}")
        print(f"Icon Url: {await rss.icon_url}")
        print()
        for entry in rss.entries:
            print(json.dumps(entry, indent=2))
            print(entry.link)
            response = requests.get(entry.link, headers=headers, timeout=10)
            unfurled = extraction.Extractor().extract(
                response.text, source_url=entry.get("link")
            )

            print(f"Original URL: {entry.link}")
            print(f"Unfurled URL: {response.url}")
            print(f"Title - RSS: {normalize_text(entry.title)}")
            print(f"Description - RSS: {normalize_text(entry.description)}")
            print(f"Title - Unfurl: {unfurled.title}")
            print(f"Description - Unfurl: {unfurled.description}")
            print(f"Banner: {unfurled.image}")
            print()
            break

    asyncio.run(_parse_rss(url))


@external_feed.command("poll")
def poll_command():
    poll()


@external_feed.command("follow")
@click.option("--id", "-i")
@click.option(
    "--follow_type", "-s", type=click.Choice(["FAVORITE", "FOLLOW"]), default="FOLLOW"
)
def follow_command(id, follow_type):
    follow(feed_id=id, follow_type=follow_type)


@external_feed.command("unfollow")
@click.option("--id", "-i")
def unfollow_command(id):
    unfollow(feed_id=id)


@external_feed.command("feed")
@click.option("--feed_id", "-i", required=True)
@click.option("--sort", "-s", default="NEW")
@click.option("--after", "-a")
@click.option("--first", "-f", type=int, default=10)
def get_feed_command(feed_id, sort, after, first):
    response = feed(feed_id, first=first, sort=sort, after=after)
    print(json.dumps(response, indent=2))


@external_feed.command("test_all")
def test_all():
    async def test_all_async():
        results_errors = {}
        results = {}
        await sql.start()
        async for each in feed_db.scan():
            print(f"Testing {each.url}")
            rss, error = await RSSFeed.parse(each.url)
            if error:
                print(error)
                results_errors[each.url] = error
                continue
            else:
                for i, entry in enumerate(rss.entries):
                    if i == 2:
                        break
                    print(f"Unfurling {entry.link}")
                    response = await unfurl(entry.link, get_image=False)
                    results[each.url] = []
                    results[each.url].append(
                        {
                            "status": response.status,
                            "url": entry.link,
                            "title": response.title,
                            "summary": response.summary,
                            "thumbnail_url": response.thumbnail_url,
                        }
                    )

        print(json.dumps(results_errors, indent=2))
        print(json.dumps(results, indent=2))
        await sql.stop()

    import asyncio

    asyncio.run(test_all_async())
