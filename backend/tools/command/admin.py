import click
import json
from typing import Dict
from firebase_admin import auth


def add(
    firebase_id: str,
) -> Dict:
    user = auth.get_user(firebase_id)
    claims = user.custom_claims
    if not claims:
        claims = {}
    claims["admin"] = True
    auth.set_custom_user_claims(firebase_id, claims)
    return {"success": True}


@click.group()
def admin():
    """Commands related to being an admin on Weclikd"""
    pass


@admin.command("add")
@click.option("--firebase_id", "-f")
def add_command(firebase_id):
    """Add Weclikd Admin."""
    response = add(
        firebase_id=firebase_id,
    )
    print(json.dumps(response, indent=2))


@admin.command("delete")
@click.option("--firebase_id", "-f")
def add_command(firebase_id):
    """Remove admin."""
    response = add(
        firebase_id=firebase_id,
    )
    print(json.dumps(response, indent=2))
