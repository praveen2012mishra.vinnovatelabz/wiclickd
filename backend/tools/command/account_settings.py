import click
import json
from typing import Dict
from command import command_utils


def edit_subscription(
    subscription: str,
    payment_id: str = None,
    email: str = None,
    pricing: str = "MONTHLY",
) -> Dict:
    return command_utils.run_graphql_query(
        "account_change_subscription",
        type=subscription,
        payment_id=payment_id,
        email=email,
        pricing=pricing,
    )


@click.group("settings")
def account_settings():
    """Commands related to account settings"""
    pass


@account_settings.command("edit_subscription")
@click.option("--subscription", "-s", required=True)
@click.option("--payment_id", "-p")
@click.option("--email", "-e")
def edit_subscription_command(subscription, payment_id: str, email: str):
    response = edit_subscription(
        subscription=subscription, payment_id=payment_id, email=email
    )
    print(json.dumps(response, indent=2))
