import click
import json
from typing import Dict, Optional, List
from command import command_utils


def create(
    name: str,
    description: str = "description",
    banner_pic: Optional[str] = None,
    website: Optional[str] = "https://www.example.com",
    qualifications: Optional[List[str]] = None,
    my_qualification: Optional[str] = "my qualification",
    invited_users: Optional[List[Dict]] = None,
    max_members: Optional[int] = 10,
    auto_approve: bool = True,
    invite_only: bool = True,
) -> Dict:
    if auto_approve:
        my_qualification = "auto approve"
    if not qualifications:
        qualifications = ["qualification1", "qualification2"]
    return command_utils.run_graphql_query(
        "clik_create",
        name=name,
        description=description,
        banner_pic=banner_pic,
        website=website,
        qualifications=qualifications if qualifications else [],
        my_qualification=my_qualification,
        invited_users=invited_users if invited_users else [],
        max_members=max_members,
        invite_only=invite_only,
    )


def edit(
    clik_id: str,
    description: Optional[str] = None,
    banner_pic: Optional[str] = None,
    website: Optional[str] = None,
    qualifications: Optional[List[str]] = None,
    max_members: Optional[int] = None,
    invite_only: bool = True,
) -> Dict:
    return command_utils.run_graphql_query(
        "clik_edit",
        clik_id=command_utils.to_global_id("Clik", clik_id),
        description=description,
        banner_pic=banner_pic,
        website=website,
        qualifications=qualifications if qualifications else [],
        max_members=max_members,
        invite_only=invite_only,
    )


def delete(clik_id: str):
    return command_utils.run_graphql_query(
        "clik_delete", clik_id=command_utils.to_global_id("Clik", clik_id)
    )


def join(
    clik_id: str = None,
    known_members: Optional[List[str]] = None,
    qualification: str = "qualification",
    invite_key: str = None,
) -> Dict:
    return command_utils.run_graphql_query(
        "clik_join",
        clik_id=command_utils.to_global_id("Clik", clik_id),
        known_members=known_members if known_members else [],
        qualification=qualification,
        invite_key=invite_key,
    )


def invite(clik_id: str, invited_users: List[Dict]) -> Dict:
    return command_utils.run_graphql_query(
        "clik_invite",
        clik_id=command_utils.to_global_id("Clik", clik_id),
        invited_users=invited_users,
    )


def reject_member(clik_id: str, user_id: str) -> Dict:
    return command_utils.run_graphql_query(
        "clik_member_reject",
        clik_id=command_utils.to_global_id("Clik", clik_id),
        user_id=command_utils.to_global_id("User", user_id),
    )


def kick_member(clik_id: str, user_id: str) -> Dict:
    return command_utils.run_graphql_query(
        "clik_member_kick",
        clik_id=command_utils.to_global_id("Clik", clik_id),
        user_id=command_utils.to_global_id("User", user_id),
    )


def promote_member(clik_id: str, user_id: str, member_type: str) -> Dict:
    return command_utils.run_graphql_query(
        "clik_member_promote",
        clik_id=command_utils.to_global_id("Clik", clik_id),
        user_id=command_utils.to_global_id("User", user_id),
        member_type=member_type,
    )


def follow(clik_id: str, follow_type: str = "FOLLOW") -> Dict:
    return command_utils.run_graphql_query(
        "clik_follow",
        clik_id=command_utils.to_global_id("Clik", clik_id),
        follow_type=follow_type,
    )


def unfollow(clik_id: str) -> Dict:
    return command_utils.run_graphql_query(
        "clik_unfollow",
        clik_id=command_utils.to_global_id("Clik", clik_id),
    )


def profile(clik_id: str) -> Dict:
    return command_utils.run_graphql_query(
        "clik - profile",
        id=command_utils.to_global_id("Clik", clik_id),
    )


def feed(clik_id: str, first=10, after: Optional[str] = None, sort="NEW") -> Dict:
    return command_utils.run_graphql_query(
        "clik - feed",
        id=command_utils.to_global_id("Clik", clik_id),
        first=first,
        after=after,
        sort=sort,
    )


def members(clik_id: str, first: int = 10, after: Optional[str] = None) -> Dict:
    return command_utils.run_graphql_query(
        "clik - members",
        clik_id=command_utils.to_global_id("Clik", clik_id),
        first=first,
        after=after,
    )


def user_applications(
    clik_id: str, first: int = 10, after: Optional[str] = None
) -> Dict:
    return command_utils.run_graphql_query(
        "clik - applications",
        clik_id=command_utils.to_global_id("Clik", clik_id),
        first=first,
        after=after,
    )


def applications(first: int = 10, after: Optional[str] = None) -> Dict:
    return command_utils.run_graphql_query(
        "clik_applications", first=first, after=after
    )


def approve(
    clik_id: str,
    approved: bool,
    message: Optional[str] = None,
    clik_name: Optional[str] = None,
) -> Dict:
    return command_utils.run_graphql_query(
        "clik_approve",
        clik_id=command_utils.to_global_id("Clik", clik_id),
        approved=approved,
        clik_name=clik_name,
        message=message,
    )


def list(first: int = 10, after: str = None, sort: str = "TRENDING"):
    return command_utils.run_graphql_query(
        "clik_list", first=first, after=after, sort=sort
    )


def search(query: str) -> Dict:
    return command_utils.run_graphql_query("search - everything", cliks=[query])


def create_invite_key(clik_id: str, member_type: str) -> Dict:
    return command_utils.run_graphql_query(
        "clik_create_invite_key",
        clik_id=command_utils.to_global_id("Clik", clik_id),
        member_type=member_type,
    )


def get_invite_key(invite_key: str) -> Dict:
    return command_utils.run_graphql_query(
        "clik_invite_key",
        invite_key=invite_key,
    )


@click.group()
def clik():
    """Commands related to a clik"""
    pass


@clik.command("applications")
@click.option("--after", "-a")
def applications_command(after):
    response = applications(after=after)
    print(json.dumps(response, indent=2))


@clik.command("approve")
@click.option("--clik_id", "-i")
@click.option("--approved", "-a", is_flag=True)
@click.option("--clik_name", "-n")
@click.option("--message", "-m")
def approve_command(clik_id, approved, clik_name, message):
    response = approve(
        clik_id=clik_id, approved=approved, clik_name=clik_name, message=message
    )
    print(json.dumps(response, indent=2))
