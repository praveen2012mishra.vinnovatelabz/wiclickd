import click
import json
from typing import Optional, Dict
from command import command_utils
from command.upload_picture import upload_picture
from weclikd.utils import weclikd_id


def create(
    name: str,
    description: Optional[str] = None,
    banner_pic: Optional[str] = None,
    parent: Optional[str] = None,
    aliases: Optional[str] = None,
) -> Dict:
    return command_utils.run_graphql_query(
        "topic_create",
        name=name,
        description=description,
        banner_pic=banner_pic,
        parent=parent,
        aliases=aliases,
    )


def profile(topic_id: str) -> Dict:
    return command_utils.run_graphql_query(
        "topic - profile",
        id=command_utils.to_global_id("Topic", topic_id),
    )


def edit(
    topic_id: str,
    name: Optional[str] = None,
    description: Optional[str] = None,
    banner_pic: Optional[str] = None,
    parent: Optional[str] = None,
    aliases: Optional[str] = None,
) -> Dict:
    return command_utils.run_graphql_query(
        "topic_edit",
        topic_id=command_utils.to_global_id("Topic", topic_id),
        name=name,
        description=description,
        banner_pic=banner_pic,
        parent=parent,
        aliases=aliases,
    )


def children(topic_id: str) -> Dict:
    return command_utils.run_graphql_query(
        "topic - children", id=command_utils.to_global_id("Topic", topic_id)
    )


def feed(
    topic_id: str, first: int = 10, sort: str = "DEFAULT", after: Optional[str] = None
) -> Dict:
    return command_utils.run_graphql_query(
        "topic - feed",
        id=command_utils.to_global_id("Topic", topic_id),
        first=first,
        sort=sort,
        after=after,
    )


def delete(topic_id: str) -> Dict:
    return command_utils.run_graphql_query(
        "topic_delete", topic_id=command_utils.to_global_id("Topic", topic_id)
    )


def trending():
    return command_utils.run_graphql_query("trending_topics")


def follow(topic_id: str, follow_type: str = "FOLLOW") -> Dict:
    return command_utils.run_graphql_query(
        "topic_follow",
        topic_id=command_utils.to_global_id("Topic", topic_id),
        follow_type=follow_type,
    )


def unfollow(topic_id: str) -> Dict:
    return command_utils.run_graphql_query(
        "topic_unfollow",
        topic_id=command_utils.to_global_id("Topic", topic_id),
    )


def list(first: int = 10, sort: str = "TRENDING", after: str = None):
    return command_utils.run_graphql_query(
        "topic_list", first=first, after=after, sort=sort
    )


def search(query: str) -> Dict:
    return command_utils.run_graphql_query("search - everything", topics=[query])


@click.group()
def topic():
    pass


@topic.command("profile")
@click.option("--topic_id", "-i", required=True)
def profile_command(topic_id):
    response = profile(topic_id)
    print(json.dumps(response, indent=2))


@topic.command("children")
@click.option("--topic_id", "-i", required=True)
def children_command(topic_id):
    response = children(topic_id)
    print(json.dumps(response, indent=2))


@topic.command("create")
@click.option("--name", "-n", required=True)
@click.option("--description", "-d", required=True)
@click.option("--picture", "-p")
@click.option("--parent", "-P")
def create_command(name, description, picture, parent):
    response = create(
        name=name,
        description=description,
        banner_pic=upload_picture(picture),
        parent=parent,
    )
    print(json.dumps(response, indent=2))


@topic.command("edit")
@click.option("--topic_id", "-i", required=True)
@click.option("--name", "-n")
@click.option("--description", "-d")
@click.option("--picture", "-p")
@click.option("--parent", "-P")
def add_command(topic_id, name, description, picture, parent):
    response = edit(
        topic_id=topic_id,
        name=name,
        description=description,
        banner_pic=upload_picture(picture),
        parent=parent,
    )
    print(json.dumps(response, indent=2))


@topic.command("delete")
@click.option("--topic_id", "-i", required=True)
def delete_command(topic_id):
    if not topic_id.startswith("Topic") and not topic_id.isnumeric():
        topic_id = weclikd_id.string_to_id(topic_id)
    response = delete(
        topic_id=topic_id,
    )
    print(json.dumps(response, indent=2))


@topic.command("feed")
@click.option("--topic_name", "-n", required=True)
@click.option("--first", "-f", default=10)
@click.option("--after", "-a")
@click.option("--sort", "-s", default="NEW")
def delete_command(topic_name, first, after, sort):
    response = feed(topic_id=topic_name, first=first, after=after, sort=sort)
    print(json.dumps(response, indent=2))
