#!/usr/bin/env python

import click
from command.account import account
from command.user import user
from command.clik import clik
from command.topic import topic
from command.feed import feed
from command.post import post
from command.comment import comment
from command.external_feed import external_feed
from command.admin import admin


@click.group()
def cli():
    pass


cli.add_command(account)
cli.add_command(user)
cli.add_command(clik)
cli.add_command(topic)
cli.add_command(feed)
cli.add_command(post)
cli.add_command(comment)
cli.add_command(external_feed)
cli.add_command(admin)
