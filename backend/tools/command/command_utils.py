import json
import hashlib
import os
import requests
import jwt
from typing import Optional, Union
from weclikd.utils import environment
from weclikd.utils.authorization import get_account_id_from_firebase_id

graphql_queries = {}
project_id = os.environ.get("PROJECT_ID")
DATASTORE_EMULATOR_HOST = os.environ.get("DATASTORE_EMULATOR_HOST", "localhost:8082")
JANUS_HOST = os.environ.get("JANUS_HOST", "localhost:8080")


class AuthInfo:
    def __init__(self):
        self.authorization: str = ""
        self.access_key: str = ""

    def to_dict(self):
        auth_info = {"content-type": "application/json"}
        if self.access_key:
            auth_info["X-Weclikd-Access-Key"] = self.access_key
        elif self.authorization:
            auth_info["authorization"] = f"{self.authorization}"
        return auth_info

    @property
    def account_id(self):
        return get_account_id_from_firebase_id(get_firebase_id())


current_auth = AuthInfo()
jwt_path = os.path.join(
    os.path.dirname(__file__), os.pardir, os.pardir, ".keys", "jwt.json"
)
if os.path.exists(jwt_path):
    with open(jwt_path, "r") as f:
        jwt_data = json.load(f)[environment.ENV_TYPE.value]
        current_auth.access_key = jwt_data.get("X-Weclikd-Access-Key")
        current_auth.authorization = jwt_data.get("authorization")


def set_auth(
    firebase_auth: Optional[str] = None,
    access_key: Optional[str] = None,
):
    if firebase_auth:
        if firebase_auth.startswith("Bearer"):
            current_auth.authorization = firebase_auth
        else:
            current_auth.authorization = f"Bearer {firebase_auth}"

    if os.path.exists(jwt_path) and not environment.is_test_env():
        with open(jwt_path, "r") as f:
            jwt_data = json.load(f)
        with open(jwt_path, "w") as f:
            jwt_data[environment.ENV_TYPE.value] = current_auth.to_dict()
            json.dump(jwt_data, f, indent=4)


def clear_auth():
    current_auth.authorization = ""
    current_auth.access_key = ""


def get_auth() -> AuthInfo:
    return current_auth


def get_firebase_id() -> str:
    firebase_json = jwt.decode(
        current_auth.authorization[7:], "secret", verify=False, algorithms=["HS256"]
    )
    return firebase_json["uid"]


def get_janus_url():
    if "localhost" in JANUS_HOST or "janus:" in JANUS_HOST:
        return f"http://{JANUS_HOST}"
    else:
        return f"https://{JANUS_HOST}"


def string_to_id(data, normalize=True) -> Optional[int]:
    if not data:
        return None

    if normalize:
        data = data.lower().replace("-", "")

    return int(
        int.from_bytes(
            hashlib.sha256(str.encode(data)).digest()[0:8],
            byteorder="big",
            signed=False,
        )
        / 2
    )


def to_global_id(object_type: str, object_id: Union[int, str]) -> str:
    if object_id is None:
        return None

    if isinstance(object_id, str):
        if ":" in object_id:
            return object_id
        else:
            return f"{object_type}:{object_id}"
    else:
        return f"{object_type}:{object_id}"


def rest(path):
    return requests.post(
        f"{get_janus_url()}{path}", headers=get_auth().to_dict(), allow_redirects=False
    )


def rest_get(path):
    return requests.get(
        f"{get_janus_url()}{path}", headers=get_auth().to_dict(), allow_redirects=False
    )


_ignore_errors = False


def ignore_errors():
    global _ignore_errors
    _ignore_errors = True


def run_graphql_query(graphql_query_name, **kwargs):
    if graphql_query_name not in graphql_queries:
        raise ValueError(f"Invalid graphql query: {graphql_query_name}")

    if not graphql_queries[graphql_query_name].get("variables"):
        json_request = {"query": graphql_queries[graphql_query_name]["query"]}
    else:
        json_request = {
            "query": graphql_queries[graphql_query_name]["query"],
            "variables": {**kwargs},
        }

    response = requests.post(
        f"{get_janus_url()}/graphql",  # /{graphql_query_name}",
        json=json_request,
        headers=get_auth().to_dict(),
    )
    if response.status_code >= 300:
        print(get_janus_url())
        raise Exception(f"Command {graphql_query_name} Failed: {response.text}")

    response = response.json()
    if response.get("errors") and expected_error_string is None and not _ignore_errors:
        print(f"Unexpected errors when running graphql query {graphql_query_name}")
        print(json.dumps(response, indent=2))
        assert False

    if expected_error_string is not None:
        if "errors" not in response:
            assert False, f"Expected error is not returned: {expected_error_string}"

    if not response.get("errors"):
        return response.get("data", {}).get(graphql_query_name.split()[0])
    return response


expected_error_string = None


class CheckGraphQLError:
    def __init__(self, error_string=""):
        self.error_string = error_string

    def __enter__(self):
        global expected_error_string
        expected_error_string = self.error_string

    def __exit__(self, *_args, **_kwargs):
        global expected_error_string
        expected_error_string = None


def _load_graphql_query():
    with open(
        os.path.join(
            os.path.dirname(__file__),
            os.pardir,
            os.pardir,
            "graphql_schemas",
            "insomnia.json",
        ),
        "r",
    ) as f:
        insomnia = json.load(f)
        for resource in insomnia["resources"]:
            if resource["_type"] != "request" or "text" not in resource["body"]:
                continue
            name = resource["name"]
            body = json.loads(resource["body"]["text"])
            if "query" not in body:
                continue
            graphql_queries[name] = {
                "query": body["query"],
                "variables": body.get("variables"),
            }


_load_graphql_query()
