import requests
import shutil
import tempfile
from command import command_utils


def upload_picture(unknown: str):
    if not unknown:
        return None

    if unknown.isnumeric():
        # already an id, no need to upload
        return unknown
    elif unknown.startswith("http"):
        return upload_picture_url(unknown)
    elif unknown.startswith("C:"):
        return upload_picture_path(unknown)
    else:
        raise RuntimeError(f"Unknown Picture: {unknown}")


def upload_picture_url(url: str):
    if not url:
        return 0
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36"
    }
    r = requests.get(url, headers=headers, stream=True)
    if r.status_code < 300:
        with tempfile.TemporaryFile() as f:
            r.raw.decode_content = True
            shutil.copyfileobj(r.raw, f)
            f.seek(0)
            response = requests.post(
                f"{command_utils.get_janus_url()}/v1/media/contents", files={"file": f}
            )

            if response.status_code <= 300:
                return response.json()["id"]
            else:
                print(response.status_code)
                print(response.text)
                raise Exception(f"Unable to upload picture {url}")
    else:
        raise Exception(f"Could not download picture {url}")


def upload_picture_path(path: str):
    with open(path, "rb") as f:
        response = requests.post(
            f"https://janus-dot-{command_utils.project_id}.appspot.com/v1/media/contents",
            file={"file": f},
        )

        if response.status_code == 200:
            return response["id"]
        else:
            raise Exception(f"Unable to upload picture {path}")
