import click
from typing import Optional, Dict
from command import command_utils
from weclikd.service import pubsub


def refresh_trending():
    pubsub.Subscription.push(
        url=f"http://{command_utils.JANUS_HOST}/atlas/refresh-trending",
    )


def home(first=10, sort: str = "NEW", after: Optional[str] = None) -> Dict:
    return command_utils.run_graphql_query(
        "home_feed", first=first, sort=sort, after=after
    )


def admin(first=10, sort: str = "NEW", after: Optional[str] = None) -> Dict:
    return command_utils.run_graphql_query(
        "home_feed - Admin", first=first, sort=sort, after=after
    )


def clik(
    clik_id: str, first=10, sort: str = "NEW", after: Optional[str] = None
) -> Dict:
    return command_utils.run_graphql_query(
        "clik - feed",
        id=command_utils.to_global_id("Clik", clik_id),
        first=first,
        sort=sort,
        after=after,
    )


def topic(
    topic_id: str, first=10, sort: str = "NEW", after: Optional[str] = None
) -> Dict:
    return command_utils.run_graphql_query(
        "topic - feed",
        id=command_utils.to_global_id("Topic", topic_id),
        first=first,
        sort=sort,
        after=after,
    )


def user(
    user_id: str, first=10, sort: str = "NEW", after: Optional[str] = None
) -> Dict:
    return command_utils.run_graphql_query(
        "user - feed",
        id=command_utils.to_global_id("User", user_id),
        first=first,
        sort=sort,
        after=after,
    )


@click.group("feed")
def feed():
    pass
