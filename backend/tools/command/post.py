import click
import json
from typing import Optional, Dict, List
from command import command_utils
from command.upload_picture import upload_picture


def create(
    url: str = "https://www.example.com",
    topics: List[str] = None,
    cliks: List[str] = None,
    title: str = "title",
    summary: str = "summary",
    thumbnail_pic: Optional[str] = None,
) -> Dict:
    if not topics:
        topics = ["topic1", "topic2"]
    return command_utils.run_graphql_query(
        "post_create",
        link=url,
        topics=topics,
        cliks=cliks,
        title=title,
        summary=summary,
        thumbnail_pic=thumbnail_pic,
    )


def delete(content_id: str) -> Dict:
    return command_utils.run_graphql_query(
        "content_delete",
        content_id=command_utils.to_global_id("Post", content_id),
    )


def edit(
    post_id: str,
    topics: Optional[List[str]] = None,
    title: Optional[str] = None,
    url: Optional[str] = None,
    summary: Optional[str] = None,
    thumbnail_pic: Optional[str] = None,
) -> Dict:
    return command_utils.run_graphql_query(
        "post_edit",
        content_id=command_utils.to_global_id("Post", post_id),
        topics=topics,
        title=title,
        link=url,
        summary=summary,
        thumbnail_pic=thumbnail_pic,
    )


def share(
    post_id: str, topics: Optional[List[str]] = None, cliks: Optional[List[str]] = None
) -> Dict:
    return command_utils.run_graphql_query(
        "post_share",
        content_id=command_utils.to_global_id("Post", post_id),
        topics=topics,
        cliks=cliks,
    )


def unfurl(url) -> Dict:
    return command_utils.run_graphql_query("url_info", url=url)


def comments(
    post_id: str,
    first: int = 3,
    after: Optional[str] = None,
    clik_id: Optional[str] = None,
) -> Dict:
    return command_utils.run_graphql_query(
        "post",
        id=command_utils.to_global_id("Post", post_id),
        first=first,
        after=after,
        clik_ids=[command_utils.to_global_id("Clik", clik_id)] if clik_id else None,
    )


def info(post_id: Optional[str] = None, url: Optional[str] = None) -> Dict:
    return command_utils.run_graphql_query(
        "post",
        id=command_utils.to_global_id("Post", post_id) or "",
        url=url,
        first=0,
        after=None,
        clik_id=None,
    )


def report(post_id: str, reports: List[Dict]) -> Dict:
    return command_utils.run_graphql_query(
        "content_report",
        content_id=command_utils.to_global_id("Post", post_id),
        reports=reports,
    )


def like(post_id: int, like_type: str) -> Dict:
    return command_utils.run_graphql_query(
        "content_like",
        content_id=command_utils.to_global_id("Post", post_id),
        like_type=like_type,
    )


@click.group()
def post():
    pass


@post.command("delete")
@click.option("--post_id", "-i", required=True)
def delete_command(post_id):
    response = delete(content_id=post_id)
    print(json.dumps(response, indent=2))


@post.command("create")
@click.option("--title", "-t")
@click.option("--summary", "-s")
@click.option("--topic", "-T", multiple=True)
@click.option("--picture", "-p")
@click.option("--url", "-u")
def create_command(title, summary, topic, picture, url):
    response = create(
        title=title,
        summary=summary,
        topics=topic,
        thumbnail_pic=upload_picture(picture),
        url=url,
    )
    print(json.dumps(response, indent=2))


@post.command("edit")
@click.option("--post_id", "-i", required=True)
@click.option("--title", "-t")
@click.option("--summary", "-s")
@click.option("--topic", "-T", multiple=True)
@click.option("--picture", "-p")
@click.option("--url", "-u")
def edit_command(post_id, title, summary, topic, picture):
    response = edit(
        post_id=post_id,
        title=title,
        summary=summary,
        topics=topic,
        thumbnail_pic=upload_picture(picture),
    )
    print(json.dumps(response, indent=2))


@post.command("unfurl")
@click.option("--url", "-u", required=True)
def unfurl_command(url):
    response = unfurl(url)
    print(json.dumps(response, indent=2))
