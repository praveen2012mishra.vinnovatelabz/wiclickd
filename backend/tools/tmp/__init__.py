import os
import sys

root_dir = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir)
sys.path.append(os.path.join(root_dir, "service"))
sys.path.append(os.path.join(root_dir, "tools"))
