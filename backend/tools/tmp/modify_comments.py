from copy import deepcopy
import os
import json
import sys
import requests
from command import topic, upload_picture
from weclikd.utils import weclikd_id
from weclikd.service import sql
from athena.api.topic_pb2 import TopicFamily
from athena.db.topic_db import topic_db, topic_aliases_db
from atlas.db.tables import ContentTable, CommentTable
from weclikd.service.sql import SQLDb
from weclikd.utils.sql_db import DBScanAndProcess, SqlDatabase


class Topic:
    def __init__(self):
        self.name = ""
        self.aliases = []
        self.parents = []
        self.description = ""
        self.banner_picture = ""

    @staticmethod
    def create(line: str, previous_topic: "Topic"):
        asterisk_index = line.find("*")
        if asterisk_index == -1:
            level = 0
        else:
            level = int(asterisk_index / 4) + 1

        if level > len(previous_topic.parents):
            parents = deepcopy(previous_topic.parents) + [previous_topic.name]
        else:
            parents = previous_topic.parents[:level]

        names = line[asterisk_index + 1 :].strip().split("/")
        name = names[0]

        response = requests.get(
            "https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=2&titles="
            + name.replace("-", "%20")
        )
        for page in response.json()["query"]["pages"].values():
            description = (
                page["extract"][0 : page["extract"].find(".") + 1]
                if page.get("extract")
                else ""
            )
            break
        else:
            description = ""

        response = requests.get(
            "https://pixabay.com/api/?key=17031937-a2d7970274c18f5f61bca4f6c&per_page=3&q="
            + name.replace("-", "%20")
        )
        hits = response.json()["hits"]
        banner_picture = hits[0]["webformatURL"] if hits else ""

        new_topic = Topic()
        new_topic.name = name
        new_topic.aliases = names[1:]
        new_topic.parents = parents
        new_topic.description = description
        new_topic.banner_picture = banner_picture
        return new_topic

    def __str__(self):
        topic_string = "/" + "/".join(self.parents + [self.name]) + "\n"
        topic_string += "    " + str(self.aliases) + "\n"
        topic_string += "    " + self.description + "\n"
        topic_string += "    " + self.banner_picture + "\n"
        return topic_string

    def to_dict(self):
        return {
            "name": self.name,
            "description": self.description,
            "banner_pic": self.banner_picture,
            "parent": self.parents[-1] if self.parents else None,
            "aliases": self.aliases,
        }


def main():
    with open(os.path.join(os.path.dirname(__file__), "topics.txt")) as f, open(
        os.path.join(os.path.dirname(__file__), "topics.json"), "w"
    ) as out:
        current_topic = Topic()
        topic_list = []
        for i, line in enumerate(f):
            current_topic = Topic.create(line, current_topic)
            print(current_topic)
            topic_list.append(current_topic.to_dict())
        json.dump(topic_list, out, indent=2)


def insert():
    with open(os.path.join(os.path.dirname(__file__), "topics.json"), "r") as out:
        topics = json.load(out)
        for i, each in enumerate(topics[167:]):
            print(f"========= {i + 167 } ===========")
            print(each)

            for name in [each["name"]] + each["aliases"]:
                response = topic.profile(f"Topic:{weclikd_id.string_to_id(name)}")
                if response.get("name"):
                    picture_id = upload_picture.upload_picture_url(
                        each.get("banner_picture")
                    )
                    response = topic.edit(
                        topic_id=f"Topic:{weclikd_id.string_to_id(name)}",
                        banner_pic=picture_id,
                        name=each["name"].lower(),
                        parent=each["parent"].lower() if each["parent"] else "NONE",
                        description=each["description"],
                        aliases=each["aliases"],
                    )
                    print(response)
                    if not response["status"]["success"]:
                        print("Attempting to edit topic")
                        sys.exit(1)
                    break
            else:
                picture_id = upload_picture.upload_picture_url(
                    each.get("banner_picture")
                )
                response = topic.add(
                    banner_pic=picture_id,
                    name=each["name"],
                    parent=each["parent"].lower() if each["parent"] else None,
                    description=each["description"],
                    aliases=each["aliases"],
                )
                print(response)
                if not response["status"]["success"]:
                    print("Attempting to add topic")
                    sys.exit(1)


async def fix_topics():
    await sql.start()

    async for each in topic_db.scan():
        print(each.name)

        try:
            await topic_aliases_db.insert(
                name=each.name.lower(),
                topic_id=each.id,
            )
        except:
            print("Duplicate")
        finally:
            pass

    await sql.stop()


async def modify_comments():
    db = SqlDatabase()
    await db.start()

    async def asdf(row):
        if row.data.get("content_info", {}).get("type") == "COMMENT":
            print(row.id, row.data["content_info"]["user_id"], row.data["thread_ids"])
            content_info = row.data["content_info"]
            data = row.data

            discussion_id = (
                data["thread_ids"][1] if len(data["thread_ids"]) >= 2 else row.id
            )
            sql = f"""
                UPDATE comment set level = {len(data['thread_ids'])- 1}, user_id = {content_info['user_id']}, discussion_id={discussion_id}, post_id={data['thread_ids'][0]}
                WHERE content_id = {row.id} 
            """
            print(sql)
            await db.execute(sql)

    scanner = DBScanAndProcess(db=db, table=ContentTable, callback=asdf, max_items=0)
    await scanner.execute()
    await db.stop()


import asyncio

asyncio.run(modify_comments())

# import asyncio
# asyncio.run(fix_topics())

"""
deleted topics
Topic:2935313806558957056   # neuroscience
Topic:2773052241531875328 # molecular biology
Topic:4344347574594325504 # botany
electrical-engineering
mechanical-engineering
materials-engineering
civil-engineering
ethics
"""
