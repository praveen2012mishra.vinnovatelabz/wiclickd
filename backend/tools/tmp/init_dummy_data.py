from command import account, topic, external_feed, command_utils
import json
import os

if __name__ == "__main__":
    command_utils.ignore_errors()
    response = account.create(firebase_id="id1", username="test_user")
    print(json.dumps(response, indent=2))
    response = topic.create(name="science", description="description")
    print(json.dumps(response, indent=2))
    response = topic.create(name="technology", description="description")
    print(json.dumps(response, indent=2))
    response = topic.create(name="humanities", description="description")
    print(json.dumps(response, indent=2))
    response = topic.create(name="weclikd", description="description")
    print(json.dumps(response, indent=2))
    response = topic.create(name="business", description="description")
    print(json.dumps(response, indent=2))
    response = external_feed.create(
        feed_name="Scientific American - Science",
        url="http://rss.sciam.com/basic-science",
        icon_url="https://www.scientificamerican.com/favicon.ico",
        website="https://www.scientificamerican.com",
        summary_source="rss",
        topic="science",
    )
    print(json.dumps(response, indent=2))

    response = account.change_access_key()
    print(json.dumps(response, indent=2))

    jwt_file = os.path.join(
        os.path.dirname(__file__), os.pardir, os.pardir, ".keys", "jwt.json"
    )
    with open(jwt_file, "r") as f:
        jwt_json = json.load(f)

    with open(jwt_file, "w") as f:
        jwt_json["test"]["X-Weclikd-Access-Key"] = response["access_key"]
        json.dump(jwt_json, f, indent=2)

    response = account.promote(firebase_id="aWQx", admin=True)
    print(json.dumps(response, indent=2))
