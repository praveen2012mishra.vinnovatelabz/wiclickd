import json
import os
import sys
from graphql.utils import schema_printer
from janus.graphql.schema import schema
from app import create_app

root_dir = os.path.join(os.path.dirname(__file__), os.pardir)


def get_fragment_types(directory):
    my_app = create_app()
    query = {
        "query": """query GetSchema{  
            __schema { 
                types {
                    kind
                    name
                    possibleTypes {
                        name
                    }
                }
            }
        }"""
    }
    request, response = my_app.test_client.post(
        "/graphql", data=json.dumps(query), headers={"Content-Type": "application/json"}
    )
    filtered_types = [
        value
        for value in response.json["data"]["__schema"]["types"]
        if value.get("possibleTypes")
    ]
    fragment_types = {"__schema": {"types": filtered_types}}
    path = os.path.join(directory, "fragmentTypes.json")
    with open(path, "w") as f:
        f.write(json.dumps(fragment_types, separators=(",", ":")))
    print(f"Created {path}")


def get_graphql_schema(directory):
    path = os.path.join(directory, "janus.graphql")
    my_schema_str = schema_printer.print_schema(schema)
    with open(path, "w") as f:
        f.write(my_schema_str)

    print(f"Created {path}")


def refresh_schemas(out_directory: str = None):
    out_directory = (
        out_directory if out_directory else os.path.join(root_dir, "graphql_schemas")
    )
    if not os.path.exists(out_directory):
        os.makedirs(out_directory)
    get_fragment_types(out_directory)
    get_graphql_schema(out_directory)


if __name__ == "__main__":
    out_directory = sys.argv[1] if len(sys.argv) > 1 else None
    refresh_schemas(out_directory)
