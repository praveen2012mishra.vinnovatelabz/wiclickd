import glob
import sys
import os
import subprocess

ROOT_DIR = os.path.abspath(
    os.path.join(os.path.dirname(__file__), os.pardir, "service")
)


def clean():
    for each in glob.glob(os.path.join(ROOT_DIR, "**/*_p2.py"), recursive=True):
        os.remove(each)


def gen_protobuf():
    for each in glob.glob(os.path.join(ROOT_DIR, "**/*.proto"), recursive=True):
        print(each)
        relative_path = os.path.relpath(each, ROOT_DIR)
        args = [
            "python",
            "-m",
            "grpc.tools.protoc",
            "-I=.",
            "--python_out=.",
            relative_path,
        ]
        if "service" in relative_path:
            args.append("--grpc_python_out=.")
        exit_code = subprocess.call(args, cwd=ROOT_DIR)
        if exit_code != 0:
            print("protoc returned an error")
            sys.exit(1)
    print("Generated protobuf successfully")


if __name__ == "__main__":
    clean()
    gen_protobuf()
