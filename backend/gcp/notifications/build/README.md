# Slack Notification from Google Cloud Function
Prepare a new Slack app for notification via webhooks:

1. Choose the app’s name and your Slack team. Click Create.
2. Click Incoming Webhooks.
3. Activate incoming webhooks.
4. Click Add New Webhook to Workspace. An authorization page opens.
5. From the drop-down menu, select the channel to which you would like notifications sent.
6. Click Authorize.
7. A webhook for your Slack application has been created. Copy the webhook URL and save it for later use.


Prepare Google Cloud Function

1. We need to create a cloud storage bucket (bucket name should maintain a nomain clature - staging_bucket_name-unique_project_id-cloudbuilds)
2. Run "gsutil mb gs://staging_bucket_name-unique_project_id-cloudbuilds"
3. Create a directory in your home named as gcp-slack
4. Go into gcp-slack directory
5. create 2 files named package.json and index.js as mentioned in files
6. Put the webhook url from slack channel into index.js files
7. Then need to publish the configuration file into cloud pub/sub
8. use command to deploy functions - "gcloud functions deploy subscribe --stage-bucket [staging_bucket_name-unique_project_id-cloudbuilds] --trigger-topic cloud-builds"

If above steps are follow correctly, then when you push to build app into gcp you will get the notification into respected slack channel.


# Custom alert message from google cloud function to slack from local system

1. Prepare a new slack app and enable the webhook (copy that webhook for future)
2. Configure main.py & requirments.txt as in repo (paste the webhook in main.py file)
3. Install google cloud sdk in local system (https://cloud.google.com/sdk/docs/quickstart-debian-ubuntu)
4. Select the project
5. Make a directory named gcb_slack 
6. copy main.py & requirments.txt file into there
7. then from that directory run command: "gcloud functions deploy slack_build_notification --trigger-topic cloud-builds --runtime python37 --project weclikd-prod"