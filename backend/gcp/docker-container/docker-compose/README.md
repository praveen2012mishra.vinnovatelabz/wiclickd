# Install & configure "gcloud" into local system (used ubuntu)
1. Create an environment variable for the correct distribution using > export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)" 
2. Add the Cloud SDK distribution URI as a package source > echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
3. Import the Google Cloud Platform public key > curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
4. Update the package list and install the Cloud SDK > sudo apt-get update && sudo apt-get install google-cloud-sdk
5. To get started run gcloud init with console-only option > gcloud init --console-only
6. Set your user name & password, and slect your project where you want to work right now 

# Install docker-compose into local system (used ubuntu)
1. to download latest ver of docker-compose check url > https://github.com/docker/compose/releases
2. download exact version using > sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
3. give permission to compose downloaded file > sudo chmod +x /usr/local/bin/docker-compose
4. can check the docker compose ver using > docker-compose --version

# Create & run docker-compose file from gcloud container registry
1. rename all images using "tag" flag for upload file into conatiner registry > docker tag Image_Name us.gcr.io/project_id/directory_name/XXX.app
2. push command for container registry > gcloud docker -- push us.gcr.io/project_id/directory_name/XXX.app
3. Check it from google cloud 
4. configure Gcloud creds into local system > gcloud auth configure-docker (optional if require)
5. prepare docker-compose.yaml file as given into repo 
6. Run > docker-compose up
7. If Require Run again "docker-compose up" because sometime mysql socket can't be run into 3306 port, while when try to run again compose into another terminal mysql container will up and running into 3306 port (known issue while start the mysql container via docker compose as per mysql forum)
8. Then check with "netstat -tlpn" for open port status
9. Then manually upload weclikd.sql file into docker conatiner using > docker exec -i container_name mysql -uroot -ppassword1 --database=new_database < dump.sql
10. If all mentioned steps are follow then app should be running on 19006 port