# Create janus docker container from janus git 
1. Create and copy the .json file for google cloud service account creds into docker working directory
2. If you want to communicate with local mysql or want to communicate with remote sql instance from google cloud change accordingly in service/janus/environment.py file
3. create Dockerfile accordingly
4. Build the image from dockerfile using "docker build --tag Iamge_Name ."
5. Run the image with following command "docker run -it --net=host -p 8443:8443 --name Container_Name Image_Name"