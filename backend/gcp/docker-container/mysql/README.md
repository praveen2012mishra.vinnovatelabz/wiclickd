# Deploying Custome MySql docker container  
1. Need to install docker-ce & docker-compose in docker host machine
2. then create and go into directory using command "mkdir -p ~/my-mysql/sql-scripts && cd ~/my-mysql/sql-scripts"
3. create 2 sql file as meintioned in https://medium.com/better-programming/customize-your-mysql-database-in-docker-723ffd59d8fb
4. create Dockerfile regarding mysql inastane
5. build the image using "docker build --tag Iamge_Name ."
6. start mysql container into 3306 port using "docker run -d -p 3306:3306 --name Container_Name -e MYSQL_ROOT_PASSWORD=XXX Image_Name"
7. to go into container and execute some command use "docker exec -it Container_Name bash"
8. try to check and create any database use "mysql -u root -p"
9. To import a sql file from local system after created the database into container use "docker exec -i container_name mysql -uroot -ppassword1 --database=new_database < dump.sql"