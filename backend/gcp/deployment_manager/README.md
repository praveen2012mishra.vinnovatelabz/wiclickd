# Deploy sql-instance from deployment manager
1. In Cloud Shell, fetch the deployment manager template that will deploy SQL instance: "gsutil cp -r gs://spls/gsp148/create-sql-instance.jinja ."
2. Set the instance_name, database_name, region, tier by updating the variables in the template
3. Deploy the sql-instance using command: "gcloud deployment-manager deployments create [deployment_name] --template create-sql-instance.jinja"
4. Check your deployment status using command: "gcloud deployment-manager deployments list"

# Deploy storage bucket from deployment manager
1. In Cloud Shell, create a file with extentaion .jinja using template (storagebucket.jinja)
2. From that template, create a service account who will deploy a bukcet from same file
3. Deploy the storage bucket using command: "gcloud deployment-manager deployments create --config=storagebucket.jinja dav-test-bucket"
4. Check your deployment status using command: "gcloud deployment-manager deployments list"

# Deploy app-engine from deployment manager
1. From local project directory, create a Dockerfile using docker command to build the image from application
2. Use following command to build the image: "docker build -t us.gcr.io/project_id/appengine/default.app ."
3. Push image to google cloud container using command: "gcloud docker -- push us.gcr.io/project_id/appengine/default.app"
4. Configure the template as mentaion on app-engine.jinja file
5. Deploy app using command: "gcloud deployment-manager deployments create flex --template app-engine.jinja"
6. Check your deployment status using command: "gcloud deployment-manager deployments list"

# Deploy app-engine, storagebucket, sql-instance from single yaml file
1. We can deploy 3 or more service from single yaml file using deployment manager
2. We need to listing all the template in single template as in sql-app-engine.jinja
3. Then we need to create another final yaml file who will execute all 3 services as in merge.yaml
4. Then deploy the function using command: "gcloud deployment-manager deployments create deployment-with-many-templates --config merge.yaml"
5. Check and describe your deployment using command: "gcloud deployment-manager deployments describe deployment-with-many-templates" 
6. For more details can follow the url: "https://cloud.google.com/deployment-manager/docs/step-by-step-guide/using-multiple-templates"

# Delete abandoned deployment scripts via google cloud sdk
1. Run "gcloud deployment-manager deployments delete deployment_name --delete-policy=ABANDON"