from .test_user import add_user_and_account
from command import topic, account


def add_default_topics(additional_topics=None):
    topic.create("root-topic")
    topic.create("child-topic", parent="root-topic")
    topic.create("topic1", parent="child-topic")
    topic.create("topic2", parent="child-topic")

    for each in additional_topics or []:
        topic.create(each, parent="child-topic")


def verify_topic_info(response, topic_id):
    assert response["name"].lower() == topic_id[topic_id.find(":") + 1 :].lower()
    assert "description" in response


"""
def test_topic_approve(test_cli):
    add_user_and_account(test_cli, admin=True)

    from .test_post import create_post, get_post
    post_response = create_post(test_cli, topics=['topic1', 'topic2'])

    response = get_topic_info(test_cli, 'Topic:topic1')
    verify_topic_info(response, 'Topic:ToPiC1')
    response = get_topic_info(test_cli, response['id'])
    verify_topic_info(response, 'Topic:ToPiC1')
    response = get_topic_info(test_cli, 'Topic:topic2')
    verify_topic_info(response, 'Topic:topic2')

    response = approve_topic(test_cli, topic_id='Topic:topic1', approved=True, topic_name='Topic1')
    assert response['success']
    response = approve_topic(test_cli, topic_id='Topic:topic2', approved=False)
    assert response['success']

    # check that topic2 was removed from post
    post_response = get_post(test_cli, post_response['id'])
    assert post_response['topics'] == ['topic1']

    # check that topic2 cannot be tagged anymore
    post_response = create_post(test_cli, topics=['topic1', 'topic2'])
    assert post_response['topics'] == ['topic1']

    # check that topic1 name was changed to Topic1
    response = get_topic_info(test_cli, 'Topic:topic1')
    response['name'] == 'Topic1'

    # check that topic2 was removed
    response = get_topic_info(test_cli, 'Topic:topic2')
    assert response['id'] == 'Topic:None'
"""


def test_topic_follow_root_topics():
    add_user_and_account(admin=True)

    topic.create(name="weclikd")
    topic.create(name="science")

    add_user_and_account(admin=True, username="user2", firebase_id="id2")
    response = account.login(firebase_id="id2")
    assert len(response["account"]["my_users"][0]["topics_followed"]) == 2
    assert response["account"]["my_users"][0]["topics_followed"][0]["topic"][
        "name"
    ] in ["weclikd", "science"]
    assert response["account"]["my_users"][0]["topics_followed"][1]["topic"][
        "name"
    ] in ["weclikd", "science"]


def test_topic_create_several():
    add_user_and_account(admin=True)

    topic.create(name="root-topic")
    topic.create(name="child-topic1", parent="root-topic")

    topic.create(name="CHILD-TOPIC2", parent="root-topic")
    topic.create(name="grandchild-topic1", parent="child-topic1")

    response = topic.profile(topic_id="Topic:root-topic")
    assert response["name"] == "root-topic"
    assert not response["parents"]
    assert response["banner_pic"]
    assert response["description"]

    response = topic.children(topic_id="Topic:root-topic")
    assert len(response["children"]) == 2
    assert response["children"][0]["name"] == "child-topic1"
    assert response["children"][1]["name"] == "child-topic2"

    response = topic.profile(topic_id="Topic:child-topic1")
    assert response["name"] == "child-topic1"
    assert response["parents"] == ["root-topic"]

    response = topic.children(topic_id="Topic:child-topic1")
    assert len(response["children"]) == 1
    assert response["children"][0]["name"] == "grandchild-topic1"

    response = topic.profile(topic_id="Topic:child-TOPIC2")
    assert response["name"] == "child-topic2"
    assert response["parents"] == ["root-topic"]

    response = topic.children(topic_id="Topic:child-topic2")
    assert len(response["children"]) == 0

    response = topic.profile(topic_id="Topic:grandchild-topic1")
    assert response["name"] == "grandchild-topic1"
    assert response["parents"] == ["root-topic", "child-topic1"]

    response = topic.children(topic_id="Topic:grandchild-topic1")
    assert len(response["children"]) == 0


def test_topic_edit_grandchild_to_orphan():
    add_user_and_account(admin=True)

    root_topic = topic.create(name="root-topic")
    topic.create(name="child-topic1", parent="root-topic")
    grandchild_topic = topic.create(name="grandchild-topic1", parent="child-topic1")

    # modify all fields
    response = topic.edit(
        topic_id=grandchild_topic["topic"]["id"],
        name="grand-child-topic1",
        description="new description",
        banner_pic="12345",
        parent="",
    )
    assert response["status"]["success"]
    assert response["topic"]["id"]
    assert not response["topic"]["parents"]

    response = topic.profile("Topic:grand-child-topic1")
    assert response["name"] == "grand-child-topic1"  # now a root topic
    assert response["description"] == "new description"
    assert not response["parents"]
    assert "12345" in response["banner_pic"]

    # parent should no longer have children :(
    response = topic.children(topic_id="Topic:child-topic1")
    assert len(response["children"]) == 0

    # modify one field
    response = topic.edit(
        topic_id=root_topic["topic"]["id"], description="new description"
    )
    assert response["status"]["success"]

    response = topic.profile("Topic:root-topic")
    assert response["name"] == "root-topic"
    assert response["description"] == "new description"
    assert response.get("banner_pic")


def test_topic_edit():
    add_user_and_account(admin=True)

    # root-topic => child-topic1 => grandchild-topic1 => greatgranchild-topic1
    root_topic = topic.create(name="root-topic")
    child_topic = topic.create(name="child-topic1", parent="root-topic")
    grandchild_topic = topic.create(name="grandchild-topic1", parent="child-topic1")
    greatgrandchild_topic = topic.create(
        name="greatgrandchild-topic1", parent="grandchild-topic1"
    )
    # unlink root-topic and child-topic
    # root-topic
    # child-topic1 => grandchild-topic1 => greatgrandchild-topic1
    response = topic.edit(topic_id=child_topic["topic"]["id"], parent="NONE")
    assert response["status"]["success"]
    assert not response["topic"]["parents"]

    # check parents of grandchild and greatgrandchild as well
    response = topic.profile(topic_id="Topic:grandchild-topic1")
    assert response["parents"] == ["child-topic1"]
    response = topic.profile(topic_id="Topic:greatgrandchild-topic1")
    assert response["parents"] == ["child-topic1", "grandchild-topic1"]

    # root topic no longer has children :(
    response = topic.children("Topic:root-topic")
    assert not response["children"]

    # relink root-topic and child-topic
    # root-topic => child-topic1 => grandchild-topic1 => greatgranchild-topic1
    response = topic.edit(topic_id=child_topic["topic"]["id"], parent="root-topic")
    assert response["status"]["success"]

    # check parents of other topics
    response = topic.profile(topic_id="Topic:child-topic1")
    assert response["parents"] == ["root-topic"]
    response = topic.profile(topic_id="Topic:grandchild-topic1")
    assert response["parents"] == ["root-topic", "child-topic1"]
    response = topic.profile(topic_id="Topic:greatgrandchild-topic1")
    assert response["parents"] == ["root-topic", "child-topic1", "grandchild-topic1"]

    # root topic has children again
    response = topic.children("Topic:root-topic")
    assert len(response["children"]) == 1
    assert response["children"][0]["name"] == "child-topic1"


def test_topic_edit_failures():
    add_user_and_account(admin=True)

    topic.create(name="root-topic")
    child_topic = topic.create(name="child-topic1", parent="root-topic")

    # topic does not exist
    response = topic.edit(topic_id="Topic:12345")
    assert not response["status"]["success"]
    assert response["status"]["status"] == "INVALID_ARGUMENT"

    # parent does not exist
    response = topic.edit(topic_id=child_topic["topic"]["id"], parent="invalid-topic")
    assert not response["status"]["success"]
    assert response["status"]["status"] == "NOT_FOUND"


def test_delete_topic():
    add_user_and_account(admin=True)

    root_topic = topic.create(name="root-topic")
    child_topic = topic.create(name="child-topic1", parent="root-topic")
    child_topic2 = topic.create(name="child-topic2", parent="root-topic")

    # cannot delete root topic
    response = topic.delete(topic_id=root_topic["topic"]["id"])
    assert response["status"]["status"] == "INVALID_ARGUMENT"

    # topic is deleted
    response = topic.delete(topic_id=child_topic["topic"]["id"])
    assert response["status"]["success"]

    # check that root topic one children
    response = topic.children(topic_id="Topic:root-topic")
    assert len(response["children"]) == 1
    assert response["children"][0]["name"] == "child-topic2"


def test_change_name_and_aliases():
    add_user_and_account(admin=True)

    root_topic = topic.create(name="root-topic", aliases=["alias1", "alias2"])
    child_topic = topic.create(
        name="child-topic", parent="root-topic", aliases=["childalias1", "childalias2"]
    )

    root_topic = topic.profile(topic_id="Topic:alias1")
    assert root_topic["name"] == "root-topic"
    assert root_topic["aliases"] == ["alias1", "alias2"]

    root_topic = topic.profile(topic_id="Topic:alias2")
    assert root_topic["name"] == "root-topic"
    assert root_topic["aliases"] == ["alias1", "alias2"]

    child_topic = topic.profile(topic_id="Topic:childalias1")
    assert child_topic["name"] == "child-topic"
    assert child_topic["parents"] == ["root-topic"]
    assert child_topic["aliases"] == ["childalias1", "childalias2"]

    child_topic = topic.profile(topic_id="Topic:childalias2")
    assert child_topic["name"] == "child-topic"
    assert child_topic["parents"] == ["root-topic"]
    assert child_topic["aliases"] == ["childalias1", "childalias2"]

    # edit root topic
    topic.edit(
        topic_id=root_topic["id"], name="roottopic", aliases=["alias2", "alias3"]
    )

    root_topic = topic.profile(topic_id="Topic:root-topic")
    assert root_topic["id"] == "Topic:None"

    root_topic = topic.profile(topic_id="Topic:alias1")
    assert root_topic["id"] == "Topic:None"

    root_topic = topic.profile(topic_id="Topic:alias2")
    assert root_topic["name"] == "roottopic"
    assert root_topic["aliases"] == ["alias2", "alias3"]

    root_topic = topic.profile(topic_id="Topic:alias3")
    assert root_topic["name"] == "roottopic"
    assert root_topic["aliases"] == ["alias2", "alias3"]

    # edit child topic
    response = topic.edit(
        topic_id=child_topic["id"], name="childalias1", aliases=["childalias2"]
    )
    assert response["status"]["success"]
    child_topic = topic.profile(topic_id="Topic:child-topic")
    assert child_topic["id"] == "Topic:None"

    child_topic = topic.profile(topic_id="Topic:childalias1")
    assert child_topic["name"] == "childalias1"
    assert child_topic["parents"] == ["roottopic"]
    assert child_topic["aliases"] == ["childalias2"]

    child_topic = topic.profile(topic_id="Topic:childalias2")
    assert child_topic["name"] == "childalias1"
    assert child_topic["parents"] == ["roottopic"]
    assert child_topic["aliases"] == ["childalias2"]

    # check root topic again, make sure it has updated child info
    root_topic = topic.profile(topic_id="Topic:roottopic")
    assert root_topic["name"] == "roottopic"

    root_topic = topic.children(topic_id="Topic:roottopic")
    assert root_topic["children"][0]["name"] == "childalias1"


"""
def test_new_topics(test_cli):
    add_user_and_account(test_cli, admin=True)

    from .test_post import create_post
    post = create_post(test_cli, topics=['topic1', 'topic2'])

    response = run_graphql_query(
        test_cli,
        'new_topics',
    )
    assert len(response['edges']) == 2
    assert response['edges'][0]['node']['post']['title'] ==post['title']
    assert response['edges'][0]['node']['topic']['name'] == 'topic1'
    assert response['edges'][1]['node']['post']['id'] == post['id']
    assert response['edges'][1]['node']['topic']['name'] == 'topic2'
"""
