from .utils import *
from .test_user import add_user_and_account
from .test_topic import add_default_topics
from .test_post import verify_post
from command import post, clik, comment


def verify_comment(response, clik=None):
    from .test_user import verify_user

    if "comment" in response:
        response = response["comment"]

    verify_node_id("Comment", response["id"])

    verify_user(response["author"])
    assert response["text"] == "This is a comment"
    assert response["clik"] == clik
    assert response["created"]
    return response["id"]
    return response["id"]


def verify_comment_ids(comments, ids):
    assert len(comments["edges"]) == len(ids)
    for comment, id in zip(comments["edges"], ids):
        assert comment["node"]["id"] == id


def test_add_comment_not_nested():
    add_user_and_account(admin=True)
    add_default_topics()
    response = post.create()
    parent_content_id = verify_post(response)
    response = comment.create(parent_content_id)
    comment1 = verify_comment(response)
    response = comment.create(parent_content_id)
    comment2 = verify_comment(response)
    response = comment.create(parent_content_id)
    comment3 = verify_comment(response)

    response = post.comments(parent_content_id, first=3)
    verify_comment_ids(response["comments"], [comment1, comment2, comment3])
    assert not response["comments"]["pageInfo"]["hasNextPage"]

    # getting comments only
    response = comment.comments(parent_content_id, first=3)
    verify_comment_ids(response, [comment1, comment2, comment3])
    assert not response["pageInfo"]["hasNextPage"]


def test_add_comment_nested():
    add_user_and_account()
    add_default_topics()
    response = post.create()
    parent_content_id = verify_post(response)
    response = comment.create(parent_content_id)
    comment1 = verify_comment(response)
    response = comment.create(comment1)
    comment2 = verify_comment(response)

    response = post.comments(parent_content_id, first=1)
    verify_comment_ids(response["comments"], [comment1])
    assert not response["comments"]["pageInfo"]["hasNextPage"]
    verify_comment_ids(response["comments"]["edges"][0]["node"]["comments"], [comment2])
    assert not response["comments"]["edges"][0]["node"]["comments"]["pageInfo"][
        "hasNextPage"
    ]

    # verify comments score
    assert (
        response["comments_score"]
        > response["comments"]["edges"][0]["node"]["comments_score"]
    )
    assert (
        response["comments"]["edges"][0]["node"]["comments_score"]
        > response["comments"]["edges"][0]["node"]["comments"]["edges"][0]["node"][
            "comments_score"
        ]
    )

    # getting comments only
    response = comment.comments(parent_content_id, first=1)
    verify_comment_ids(response, [comment1])
    assert not response["pageInfo"]["hasNextPage"]
    verify_comment_ids(response["edges"][0]["node"]["comments"], [comment2])
    assert not response["edges"][0]["node"]["comments"]["pageInfo"]["hasNextPage"]


def test_add_comment_nested_pagination():
    add_user_and_account()
    add_default_topics()
    response = post.create()["post"]
    verify_post(response)
    parent_content_id = response["id"]
    response = comment.create(parent_content_id)
    comment1 = verify_comment(response)
    response = comment.create(parent_content_id)
    comment2 = verify_comment(response)
    response = comment.create(parent_content_id)
    comment3 = verify_comment(response)
    response = comment.create(comment2)
    comment4 = verify_comment(response)
    response = comment.create(comment2)
    comment5 = verify_comment(response)
    response = comment.create(comment2)
    comment6 = verify_comment(response)

    response = post.comments(parent_content_id, first=2)
    verify_comment_ids(response["comments"], [comment1, comment2])
    assert response["comments"]["pageInfo"]["hasNextPage"]
    verify_comment_ids(
        response["comments"]["edges"][1]["node"]["comments"], [comment4, comment5]
    )
    assert response["comments"]["edges"][1]["node"]["comments"]["pageInfo"][
        "hasNextPage"
    ]
    endCursor = response["comments"]["edges"][1]["node"]["comments"]["pageInfo"][
        "endCursor"
    ]

    response = post.comments(
        parent_content_id, first=2, after=response["comments"]["pageInfo"]["endCursor"]
    )
    verify_comment_ids(response["comments"], [comment3])
    assert not response["comments"]["pageInfo"]["hasNextPage"]

    response = comment.comments(comment2, first=2, after=endCursor)
    verify_comment_ids(response, [comment6])
    assert not response["pageInfo"]["hasNextPage"]

    # getting comments only
    response = comment.comments(parent_content_id, first=2)
    verify_comment_ids(response, [comment1, comment2])
    assert response["pageInfo"]["hasNextPage"]
    verify_comment_ids(response["edges"][1]["node"]["comments"], [comment4, comment5])
    assert response["edges"][1]["node"]["comments"]["pageInfo"]["hasNextPage"]

    response = comment.comments(
        parent_content_id, first=2, after=response["pageInfo"]["endCursor"]
    )
    verify_comment_ids(response, [comment3])
    assert not response["pageInfo"]["hasNextPage"]

    response = comment.comments(comment2, first=2, after=endCursor)
    verify_comment_ids(response, [comment6])
    assert not response["pageInfo"]["hasNextPage"]


def test_add_comment_error_parent_comment_not_in_same_clik():
    add_user_and_account()
    add_default_topics()
    clik.create("Clik1", auto_approve=True)

    response = post.create()
    parent_content_id = verify_post(response)
    response = comment.create(parent_content_id)
    comment1 = verify_comment(response)
    response = comment.create(parent_content_id, clik="Clik1")
    comment2 = verify_comment(response, clik="Clik1")

    response = comment.create(comment1, clik="Clik1")
    assert response["status"]["status"] == "INVALID_ARGUMENT"
    assert "not posted to the same clik" in response["status"]["user_msg"]

    response = comment.create(comment2)
    assert response["status"]["status"] == "INVALID_ARGUMENT"
    assert "not posted to the same clik" in response["status"]["user_msg"]


def test_add_comment_error_parent_comment_not_member_of_clik():
    add_user_and_account()
    add_default_topics()

    response = post.create()
    parent_content_id = verify_post(response)

    response = comment.create(parent_content_id, clik="Clik1")
    assert response["status"]["status"] == "PERMISSION_DENIED"
    assert response["status"]["user_msg"] == "Not a member of clik Clik1"


def test_add_comment_to_post_in_clik():
    add_user_and_account()
    add_default_topics()
    clik.create("Clik1", auto_approve=True)

    response = post.create()
    parent_content_id = verify_post(response)

    response = comment.create(parent_content_id, clik="Clik1")
    comment_id = verify_comment(response, clik="Clik1")

    # verify shared to clik
    response = post.info(post_id=parent_content_id)
    verify_post(response)
    assert response["cliks"] == ["Clik1"]

    response = clik.feed(clik_id="Clik:Clik1")
    assert response["posts"]["edges"][0]["node"]["id"] == parent_content_id


def test_add_comment_to_comment_in_clik():
    add_user_and_account()
    add_default_topics()
    clik.create("Clik1", auto_approve=True)
    clik.create("Clik2", auto_approve=True)

    response = post.create()
    parent_content_id = verify_post(response)

    response = comment.create(parent_content_id, clik="Clik2")
    comment0 = verify_comment(response, clik="Clik2")
    response = comment.create(parent_content_id, clik="Clik1")
    comment1 = verify_comment(response, clik="Clik1")
    response = comment.create(comment1, clik="Clik1")
    comment2 = verify_comment(response, clik="Clik1")

    add_user_and_account(firebase_id="id2", username="user2")
    response = comment.create(parent_content_id)
    comment3 = response["comment"]["id"]

    # GetPosts & GetComments without cliks
    response = post.comments(parent_content_id, first=3)
    verify_comment_ids(response["comments"], [comment0, comment1, comment3])
    verify_comment_ids(response["comments"]["edges"][1]["node"]["comments"], [comment2])

    response = comment.comments(parent_content_id, first=3)
    verify_comment_ids(response, [comment0, comment1, comment3])
    verify_comment_ids(response["edges"][1]["node"]["comments"], [comment2])

    # GetPosts & GetComments clik 1
    response = post.comments(parent_content_id, first=2, clik_id="Clik:Clik1")
    verify_comment_ids(response["comments"], [comment1])
    verify_comment_ids(response["comments"]["edges"][0]["node"]["comments"], [comment2])

    response = comment.comments(parent_content_id, first=2, clik_id="Clik:Clik1")
    verify_comment_ids(response, [comment1])
    verify_comment_ids(response["edges"][0]["node"]["comments"], [comment2])

    # GetPosts & GetComments clik 1 & 2
    response = comment.comments(
        parent_content_id, first=3, clik_ids=["Clik:Clik1", "Clik:Clik2"]
    )
    assert len(response["edges"]) == 2

    # Discussing
    # response = comment.comments(parent_content_id, first=2, sort="DISCUSSING")
    # verify_comment_ids(response, [comment1])


def test_remove_comment():
    add_user_and_account()
    add_default_topics()
    response = post.create()
    parent_content_id = verify_post(response)
    response = comment.create(parent_content_id)
    comment1 = verify_comment(response)
    response = comment.create(parent_content_id)
    comment2 = verify_comment(response)
    response = comment.create(parent_content_id)
    comment3 = verify_comment(response)
    response = comment.create(comment2)
    comment4 = verify_comment(response)
    response = comment.create(comment2)
    comment5 = verify_comment(response)
    response = comment.create(comment3)
    comment6 = verify_comment(response)

    response = comment.delete(comment2)
    assert response["status"]["success"]
    response = comment.delete(comment6)
    assert response["status"]["success"]

    response = comment.comments(parent_content_id, first=2)
    verify_comment_ids(response, [comment1, comment3])
    assert not response["pageInfo"]["hasNextPage"]
    verify_comment_ids(response["edges"][0]["node"]["comments"], [])
    assert response["edges"][0]["node"]["comments_score"] == 0
    verify_comment_ids(response["edges"][1]["node"]["comments"], [])
    assert response["edges"][1]["node"]["comments_score"] == 0


def test_comment_success():
    add_user_and_account()
    add_default_topics()
    response = post.create()["post"]
    parent_content_id = response["id"]
    response = comment.create(parent_content_id)["comment"]
    comment_id = response["id"]
    response = comment.edit(comment_id, "modified text")
    assert response["status"]["success"]

    response = comment.info(comment_id)
    assert response["text"] == "modified text"

    # set text to empty, should delete comment
    response = comment.edit(comment_id, "")
    assert response["status"]["success"]

    response = comment.comments(parent_content_id, first=2)
    assert not response["edges"]


def test_comment_edit_failure():
    add_user_and_account()
    add_default_topics()
    response = post.create()["post"]
    parent_content_id = response["id"]
    response = comment.create(parent_content_id)["comment"]
    comment_id = response["id"]

    # random id
    response = comment.edit(1234, "modified text")
    assert not response["status"]["success"]
    assert response["status"]["status"] == "NOT_FOUND"

    # cannot edit post
    response = comment.edit(parent_content_id, "modified text")
    assert not response["status"]["success"]
    assert response["status"]["status"] == "NOT_FOUND"

    # different user should not be able to modify comments
    add_user_and_account(firebase_id="firebase2", username="user2")
    response = comment.edit(comment_id, "modified text")
    assert not response["status"]["success"]
    assert response["status"]["status"] == "PERMISSION_DENIED"
