from .test_user import add_user_and_account
from .test_topic import add_default_topics
from command import (
    command_utils,
    account,
    clik,
    post,
    topic,
    user,
    comment,
    feed as home_feed,
)


def check_content_order(feed, ids):
    assert len(feed["posts"]["edges"]) == len(ids)
    for edge, id in zip(feed["posts"]["edges"], ids):
        assert (
            edge["node"]["id"] == id
        ), f"order was {[each['node']['id'] for each in feed['posts']['edges']]}"


def test_get_feed():
    username = "john_smith"
    add_user_and_account(firebase_id="id1", username=username)
    add_default_topics()
    clik.create("Engineers")
    clik.create("Gophers")

    response = post.create(
        cliks=["Engineers"], topics=["topic2"], url="https://www.cnn.com"
    )["post"]
    content1 = response["id"]
    response = post.create(
        cliks=["Gophers"], topics=["topic1"], url="https://www.google.com"
    )["post"]
    content2 = response["id"]
    response = post.create(
        cliks=["Engineers", "Gophers"],
        topics=["topic2", "topic3"],
        url="https://www.asdf.com",
    )["post"]
    content3 = response["id"]

    add_user_and_account(firebase_id="firebaseid2", username="john_doe")
    comment.create(parent_content_id=content1)
    comment.create(parent_content_id=content2)
    comment.create(parent_content_id=content3)

    feed = clik.feed("Clik:Gophers")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content2])

    feed = clik.feed("Clik:Engineers")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content1])

    feed = topic.feed("Topic:topic1")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content2])

    feed = topic.feed("Topic:topic2")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content1])

    feed = topic.feed("Topic:child-topic", first=3)
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content2, content1])

    feed = topic.feed("Topic:ROOT-TOPIC", first=3)
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content2, content1])

    # topic 3 is not created by default
    feed = topic.feed("Topic:topic3")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    # john doe commented, so the posts that he commented on appears in his feed
    feed = user.feed("User:john_doe", first=3)
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content2, content1])

    # john smith shared the posts
    feed = user.feed("User:john_smith", first=3)
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content2, content1])

    # not following anything, home feed is empty
    feed = home_feed.home()
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    # login to first account, which follows some cliks
    account.login(firebase_id="id1")
    feed = home_feed.home()
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content2, content1])

    # check again, simulate checking from another device
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content2, content1])

    feed = home_feed.home(after=feed["posts"]["pageInfo"]["endCursor"])
    assert not feed["posts"]["edges"]

    # no new homes after archiving them
    feed = home_feed.home()
    assert not feed["posts"]["edges"]


def test_get_feed_pagination_first_equals_num_posts():
    username = "john_smith"
    add_user_and_account(username=username)
    add_default_topics()
    clik.create("Engineers")
    clik.create("Gophers")

    response = post.create(
        cliks=["Engineers"], topics=["topic2"], url="https://www.cnn.com"
    )["post"]
    content1 = response["id"]
    response = post.create(
        cliks=["Gophers"], topics=["topic1"], url="https://www.google.com"
    )["post"]
    content2 = response["id"]
    response = post.create(
        cliks=["Engineers", "Gophers"],
        topics=["topic2", "topic3"],
        url="https://www.asdf.com",
    )["post"]
    content3 = response["id"]

    feed = clik.feed("Clik:Gophers", first=3)
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content2])

    feed = clik.feed("Clik:Engineers", first=3)
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content1])

    feed = home_feed.home(first=3)
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content2, content1])


def test_get_feed_pagination_first_less_than_num_posts():
    username = "john_smith"
    add_user_and_account(username=username)
    add_default_topics()
    clik.create("Engineers")
    clik.create("Gophers")

    response = post.create(
        cliks=["Engineers", "Gophers"],
        topics=["topic2", "topic3"],
        url="https://www.cnn.com",
    )["post"]
    content0 = response["id"]
    response = post.create(
        cliks=["Engineers"], topics=["topic2"], url="https://www.tofu.com"
    )["post"]
    content1 = response["id"]
    response = post.create(
        cliks=["Gophers"], topics=["topic1"], url="https://www.google.com"
    )["post"]
    content2 = response["id"]
    response = post.create(
        cliks=["Engineers", "Gophers"],
        topics=["topic2", "topic3"],
        url="https://wwww.msnbc.com",
    )["post"]
    content3 = response["id"]

    feed = home_feed.home(first=3)
    assert feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content2, content1])
    feed = home_feed.home(first=3, after=feed["posts"]["pageInfo"]["endCursor"])
    check_content_order(feed, [content0])

    # log out so posts won't be marked as read
    account.logout()

    feed = clik.feed("Clik:Gophers", first=2)
    assert feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content2])
    feed = clik.feed(
        "Clik:Gophers", first=3, after=feed["posts"]["pageInfo"]["endCursor"]
    )
    check_content_order(feed, [content0])

    feed = clik.feed("Clik:Engineers", first=2)
    assert feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content1])
    feed = clik.feed(
        "Clik:Engineers", first=3, after=feed["posts"]["pageInfo"]["endCursor"]
    )
    check_content_order(feed, [content0])

    account.login()

    # we marked the first page of the home feed as read
    feed = home_feed.home(first=3)
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content0])


def test_remove_posts():
    add_user_and_account(username="user1")
    add_default_topics()
    clik.create("Engineers")
    clik.create("Gophers")

    response = post.create(
        cliks=["Engineers", "Gophers"],
        topics=["topic2", "topic3"],
        url="https://www.cnn.com",
    )["post"]
    content0 = response["id"]
    response = post.create(
        cliks=["Engineers"], topics=["topic2"], url="https://www.tofu.com"
    )["post"]
    content1 = response["id"]
    response = post.create(
        cliks=["Gophers"], topics=["topic1"], url="https://www.google.com"
    )["post"]
    content2 = response["id"]
    response = post.create(
        cliks=["Engineers", "Gophers"],
        topics=["topic2", "topic3"],
        url="https://www.msnbc.com",
    )["post"]
    content3 = response["id"]

    feed = clik.feed("Clik:Engineers", first=2)

    response = post.delete(content0)
    assert response["status"]["success"]
    response = post.delete(content3)
    assert response["status"]["success"]

    account.logout()

    feed = clik.feed("Clik:Engineers", first=2)
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content1])

    feed = home_feed.home(first=2)
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content2, content1])

    feed = topic.feed("Topic:topic2", first=2)
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content1])

    feed = topic.feed("Topic:topic1", first=2)
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content2])


def test_admin_feed_trending():
    user_id = add_user_and_account(username="user1", admin=True)
    add_default_topics()

    response = post.create(url="https://www.google.com", topics=["topic1"])["post"]
    post.like(response["id"], "RED")
    feed = home_feed.admin(sort="DISCUSSING")
    one_post = feed["posts"]["edges"][0]["node"]
    assert one_post["admin_stats"]["sharer"]["id"] == user_id
    assert one_post["admin_stats"]["likes"] == {
        "red": 1,
        "silver": 0,
        "gold": 0,
        "diamond": 0,
    }


def test_get_feed_trending():
    username = "john_smith"
    add_user_and_account(username=username)
    add_default_topics()
    clik.create("Engineers")
    clik.create("Gophers")

    response = post.create(
        cliks=["Engineers"], topics=["topic1"], url="https://www.cnn.com"
    )["post"]
    content1 = response["id"]
    response = post.create(
        cliks=["Gophers"], topics=["topic2", "topic1"], url="https://www.google.com"
    )["post"]
    content2 = response["id"]
    response = post.create(
        cliks=["Engineers", "Gophers"],
        topics=["topic2", "topic1"],
        url="https://www.yahoo.com",
    )["post"]
    content3 = response["id"]

    # content1 should have highest score, then content 3, then content 2
    comment.create(parent_content_id=content1)
    comment.create(parent_content_id=content1)
    comment.create(parent_content_id=content3)

    # manually cause a refresh of the posts score
    home_feed.refresh_trending()

    feed = clik.feed("Clik:Gophers", first=1, sort="TRENDING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3])

    feed = clik.feed(
        "Clik:Gophers",
        first=1,
        sort="TRENDING",
        after=feed["posts"]["pageInfo"]["endCursor"],
    )
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    feed = clik.feed("Clik:Engineers", first=2, sort="TRENDING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content1, content3])
    feed = topic.feed("Topic:topic1", first=1, sort="TRENDING")
    assert feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content1])

    feed = topic.feed(
        "Topic:topic1",
        first=2,
        sort="TRENDING",
        after=feed["posts"]["pageInfo"]["endCursor"],
    )
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3])

    feed = topic.feed("Topic:topic2", first=2, sort="TRENDING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3])

    feed = home_feed.home(first=2, sort="TRENDING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3, content1])

    feed = home_feed.home(
        first=2, after=feed["posts"]["pageInfo"]["endCursor"], sort="TRENDING"
    )
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    feed = user.feed(user_id="User:john_smith", first=3, sort="TRENDING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(
        feed, [content1, content3]
    )  # Trending is the same as new for users

    # login to new user, nothing followed in the beginning
    username = "john_smith2"
    add_user_and_account(firebase_id="id2", username=username)
    feed = home_feed.home(first=2, after=None, sort="TRENDING")
    assert not feed["posts"]["edges"]

    # follow something and trending should populate
    topic.follow(topic_id="Topic:topic2")
    feed = home_feed.home(first=2, after=None, sort="TRENDING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content3])

    # unfollow and feed should go back to empty
    topic.unfollow(topic_id="Topic:topic2")
    feed = home_feed.home(first=2, after=None, sort="TRENDING")
    assert not feed["posts"]["edges"]


def test_get_feed_discussing():
    username = "john_smith"
    add_user_and_account(username=username)
    add_default_topics()
    clik.create("Engineers")
    clik.create("Gophers")

    response = post.create(
        cliks=["Engineers"], topics=["topic2"], url="https://www.cnn.com"
    )["post"]
    content1 = response["id"]
    response = post.create(
        cliks=["Gophers"], topics=["topic1"], url="https://www.google.com"
    )["post"]
    content2 = response["id"]
    response = post.create(
        cliks=["Engineers", "Gophers"],
        topics=["topic2", "topic3"],
        url="https://www.asdf.com",
    )["post"]
    content3 = response["id"]
    comment.create(parent_content_id=content1)

    add_user_and_account(firebase_id="firebaseid2", username="john_doe")
    comment.create(parent_content_id=content1)
    comment.create(parent_content_id=content2)

    feed = home_feed.home(first=3, sort="DISCUSSING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content2, content1])

    feed = user.feed(user_id="User:john_doe", first=3, sort="DISCUSSING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content2, content1])

    feed = clik.feed(clik_id="Clik:Engineers", first=3, sort="DISCUSSING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content1])

    feed = topic.feed(topic_id="Topic:topic1", first=3, sort="DISCUSSING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content2])

    feed = user.feed(user_id="User:john_smith", first=3, sort="DISCUSSING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content2, content1])


def test_get_feed_trending_after_following_user():
    username = "john_smith"
    user1 = add_user_and_account(username=username)
    add_default_topics()

    response = post.create(topics=["topic2"], url="https://www.cnn.com")["post"]
    content1 = response["id"]
    comment.create(parent_content_id=content1)

    add_user_and_account(firebase_id="firebaseid2", username="john_doe")
    user.follow(user_id=user1)

    feed = home_feed.home(first=3, sort="NEW")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [content1])
    assert (
        feed["posts"]["edges"][0]["node"]["reasons_shared"]["users"][0]["id"] == user1
    )
    assert (
        feed["posts"]["edges"][0]["node"]["reasons_shared"]["users"][0]["username"]
        == "john_smith"
    )


def test_get_feed_empty():
    username = "john_smith"
    add_user_and_account(username=username)
    add_default_topics()
    clik.create("Engineers")

    feed = home_feed.home(first=3, sort="DISCUSSING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    feed = user.feed(user_id="User:john_smith", first=3, sort="DISCUSSING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    feed = clik.feed(clik_id="Clik:Engineers", first=3, sort="DISCUSSING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    feed = topic.feed(topic_id="Topic:topic1", first=3, sort="DISCUSSING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    feed = home_feed.home(first=3, sort="NEW")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    feed = user.feed(user_id="User:john_smith", first=3, sort="NEW")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    feed = clik.feed(clik_id="Clik:Engineers", first=3, sort="NEW")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    feed = topic.feed(topic_id="Topic:topic1", first=3, sort="NEW")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    feed = home_feed.home(first=3, sort="TRENDING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    feed = user.feed(user_id="User:john_smith", first=3, sort="TRENDING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    feed = clik.feed(clik_id="Clik:Engineers", first=3, sort="TRENDING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])

    feed = topic.feed(topic_id="Topic:topic1", first=3, sort="TRENDING")
    assert not feed["posts"]["pageInfo"]["hasNextPage"]
    check_content_order(feed, [])
