from .test_user import add_user_and_account
from command import clik, topic, user, account, external_feed


def test_clik_follow_for_super_admin():
    add_user_and_account(firebase_id="id1", username="random_girl123")
    response = clik.create("Clik1")
    response = clik.profile("Clik:Clik1")
    assert response["num_members"] == 1
    assert response["num_followers"] == 1

    response = account.my_user()
    assert response["cliks_followed"][0]["clik"]["name"] == "Clik1"
    assert response["cliks_followed"][0]["member_type"] == "SUPER_ADMIN"
    assert response["cliks_followed"][0]["settings"]["follow_type"] == "FAVORITE"

    clik.follow("Clik:Clik1")

    response = clik.profile("Clik:Clik1")
    assert response["num_members"] == 1
    assert response["num_followers"] == 1


def test_clik_follow():
    add_user_and_account(firebase_id="id1", username="random_girl123")
    clik.create("Clik1")

    response = clik.profile("Clik:Clik1")
    assert response["num_members"] == 1
    assert response["num_followers"] == 1

    user2 = add_user_and_account(firebase_id="id2", username="random_girl321")
    clik.follow("Clik:Clik1")

    response = clik.profile("Clik:Clik1")
    assert response["num_members"] == 1
    assert response["num_followers"] == 2

    response = account.my_user()
    assert response["cliks_followed"][0]["clik"]["name"] == "Clik1"
    assert response["cliks_followed"][0]["member_type"] == "FOLLOWER"
    assert response["cliks_followed"][0]["settings"]["follow_type"] == "FOLLOW"


def test_clik_unfollow():
    add_user_and_account(firebase_id="id1", username="random_girl123")
    clik.create("Clik1")

    user2 = add_user_and_account(firebase_id="id2", username="random_girl321")
    clik.follow("Clik:Clik1")
    clik.follow("Clik:Clik1")  # second follow ignored

    response = clik.profile("Clik:Clik1")
    assert response["num_members"] == 1
    assert response["num_followers"] == 2

    clik.unfollow("Clik:Clik1")
    clik.unfollow("Clik:Clik1")  # second unfollow ignored
    response = account.my_user()
    assert not response["cliks_followed"]

    response = clik.profile("Clik:Clik1")
    assert response["num_members"] == 1
    assert response["num_followers"] == 1


def test_clik_follow_as_member():
    user1 = add_user_and_account(firebase_id="id1", username="random_girl321")

    add_user_and_account(firebase_id="id2", username="random_girl123")
    clik.create(
        "Clik1", invited_users=[{"userid_or_email": user1, "member_type": "MEMBER"}]
    )

    account.login(firebase_id="id1")
    clik.join("Clik1")
    response = clik.profile("Clik:Clik1")
    assert response["num_members"] == 2
    assert response["num_followers"] == 2

    clik.unfollow("Clik:Clik1")
    response = account.my_user()
    assert not response["cliks_followed"]

    response = clik.profile("Clik:Clik1")
    assert response["num_members"] == 1
    assert response["num_followers"] == 1


def test_follow_several_cliks():
    user_id = add_user_and_account(firebase_id="id1", username="random_girl123")
    clik.create("Clik1")
    clik.create("Clik2")
    clik.create("Clik3")

    clik.follow("Clik:Clik3", "FAVORITE")
    clik.follow("Clik:Clik2", "FOLLOW")
    clik.follow("Clik:Clik1", "FOLLOW")

    response = clik.list(first=2)
    assert len(response["edges"]) == 2
    assert response["pageInfo"]["hasNextPage"]
    response = clik.list(first=2, after=response["pageInfo"]["endCursor"])
    assert len(response["edges"]) == 1
    assert not response["pageInfo"]["hasNextPage"]

    response = account.my_user()
    assert response["cliks_followed"][0]["clik"]["name"] == "Clik3"
    assert response["cliks_followed"][0]["settings"]["follow_type"] == "FAVORITE"
    assert response["cliks_followed"][1]["clik"]["name"] == "Clik1"
    assert response["cliks_followed"][1]["settings"]["follow_type"] == "FOLLOW"
    assert response["cliks_followed"][2]["clik"]["name"] == "Clik2"
    assert response["cliks_followed"][2]["settings"]["follow_type"] == "FOLLOW"


def test_follow_and_unfollow_user():
    user1 = add_user_and_account(firebase_id="id1", username="random_girl123")
    user2 = add_user_and_account(firebase_id="id2", username="random_girl321")
    user.follow(user1)
    response = account.my_user()
    assert response["users_followed"][0]["user"]["id"] == user1
    user.unfollow(user1)

    response = account.my_user()
    assert not response["users_followed"]

    response = account.my_user()
    assert response["cliks_followed"][0]["clik"]["name"] == "Clik3"
    assert response["cliks_followed"][0]["settings"]["follow_type"] == "FAVORITE"
    assert response["cliks_followed"][1]["clik"]["name"] == "Clik1"
    assert response["cliks_followed"][1]["settings"]["follow_type"] == "FOLLOW"
    assert response["cliks_followed"][2]["clik"]["name"] == "Clik2"
    assert response["cliks_followed"][2]["settings"]["follow_type"] == "FOLLOW"


def test_follow_and_unfollow_user():
    user1 = add_user_and_account(firebase_id="id1", username="random_girl123")
    user2 = add_user_and_account(firebase_id="id2", username="random_girl321")
    user.follow(user1)
    response = account.my_user()
    assert response["users_followed"][0]["user"]["id"] == user1
    user.unfollow(user1)

    response = account.my_user()
    assert not response["users_followed"]


def test_follow_many_users():
    user1 = add_user_and_account(firebase_id="id1", username="random_girl123")
    user2 = add_user_and_account(firebase_id="id2", username="random_girl124")
    user3 = add_user_and_account(firebase_id="id3", username="random_girl125")
    user4 = add_user_and_account(firebase_id="id4", username="random_girl126")

    user.follow(user1, "FOLLOW")
    user.follow(user2, "FOLLOW")
    user.follow(user2, "FAVORITE")
    user.follow(user3, "FOLLOW")

    response = user.list(first=3)
    assert len(response["edges"]) == 3
    assert response["pageInfo"]["hasNextPage"]
    response = user.list(first=3, after=response["pageInfo"]["endCursor"])
    assert len(response["edges"]) == 1
    assert not response["pageInfo"]["hasNextPage"]

    response = account.my_user()
    assert response["users_followed"][0]["user"]["username"] == "random_girl124"
    assert response["users_followed"][0]["settings"]["follow_type"] == "FAVORITE"
    assert response["users_followed"][1]["user"]["username"] == "random_girl123"
    assert response["users_followed"][1]["settings"]["follow_type"] == "FOLLOW"
    assert response["users_followed"][2]["user"]["username"] == "random_girl125"
    assert response["users_followed"][2]["settings"]["follow_type"] == "FOLLOW"


def test_follow_and_unfollow_topic():
    user1 = add_user_and_account(firebase_id="id1", username="random_girl123")
    topic.create("root")
    topic.create("physics", parent="root")

    topic.follow("Topic:Physics")
    topic.follow("Topic:Physics")  # second follow ignored
    response = topic.profile("Topic:physics")
    assert response["num_followers"] == 1

    response = account.my_user()
    assert response["topics_followed"][0]["topic"]["name"] == "physics"
    topic.unfollow("Topic:physics")
    topic.unfollow("Topic:physics")  # second unfollow ignored

    response = account.my_user()
    assert not response["topics_followed"]

    response = topic.profile("Topic:physics")
    assert response["num_followers"] == 0


def test_follow_many_topics():
    user1 = add_user_and_account(firebase_id="id1", username="random_girl123")
    topic.create("root")
    topic.create("chemistry", parent="root")
    topic.create("economics", parent="root")
    topic.create("physics", parent="root")

    topic.follow("Topic:chemistry", "FOLLOW")
    topic.follow("Topic:Economics", "FOLLOW")
    topic.follow("Topic:Economics", "FAVORITE")
    topic.follow("Topic:Physics", "FOLLOW")

    response = topic.list(first=3)
    assert len(response["edges"]) == 3
    assert response["pageInfo"]["hasNextPage"]
    response = topic.list(first=3, after=response["pageInfo"]["endCursor"])
    assert len(response["edges"]) == 1
    assert not response["pageInfo"]["hasNextPage"]

    response = account.my_user()
    assert response["topics_followed"][0]["topic"]["name"] == "economics"
    assert response["topics_followed"][0]["settings"]["follow_type"] == "FAVORITE"
    assert response["topics_followed"][1]["topic"]["name"] == "chemistry"
    assert response["topics_followed"][1]["settings"]["follow_type"] == "FOLLOW"
    assert response["topics_followed"][2]["topic"]["name"] == "physics"
    assert response["topics_followed"][2]["settings"]["follow_type"] == "FOLLOW"


def test_unfollow_things_that_do_not_exist():
    add_user_and_account(firebase_id="id1", username="random_girl123")

    response = clik.unfollow("Clik:asdf")
    assert not response["status"]["success"]
    response = topic.unfollow("Topic:asdf")
    assert response["status"]["success"]
    response = user.unfollow("User:123")
    assert response["status"]["success"]


def test_follow_unfollow_external_feed():
    add_user_and_account()
    topic.create("technology")

    external_feed.create(
        feed_name="TestFeed",
        url="http://cdn-dev.weclikd-beta.com/test/test_rss.xml",
        website="https://www.example.com",
        topic="technology",
        summary_source="rss",
        icon_url="https://www.google.com/favicon.ico",
    )

    external_feed.follow("ExternalFeed:TestFeed", "FOLLOW")

    response = account.my_user()
    assert response["feeds_followed"][0]["feed"]["name"] == "TestFeed"
    external_feed.unfollow("ExternalFeed:TestFeed")
    external_feed.unfollow("ExternalFeed:TestFeed")  # second unfollow ignored

    response = account.my_user()
    assert not response["feeds_followed"]

    response = external_feed.list(first=2)
    assert len(response["edges"]) == 1
    assert not response["pageInfo"]["hasNextPage"]
