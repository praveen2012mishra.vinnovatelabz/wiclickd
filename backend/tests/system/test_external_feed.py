from .test_user import add_user_and_account
from command import topic, external_feed


def verify_test_feed(feed, name="TestFeed"):
    assert feed["name"] == name
    assert feed["url"] == "http://cdn-dev.weclikd-beta.com/test/test_rss.xml"
    assert feed["website"] == "http://www.example.com"
    assert feed["base_topic"] == "technology"
    assert "id" in feed
    assert "icon_url" in feed


def test_add_process_remove_and_get_external_feed():
    add_user_and_account()
    topic.create("technology")

    feeds = external_feed.list()
    assert not feeds["edges"]

    external_feed_info = external_feed.create(
        feed_name="TestFeed",
        url="http://cdn-dev.weclikd-beta.com/test/test_rss.xml",
        website="http://www.example.com",
        topic="technology",
        summary_source="rss",
        icon_url="https://www.google.com/favicon.ico",
    )
    verify_test_feed(external_feed_info["external_feed"])

    feeds = external_feed.list()
    assert len(feeds["edges"]) == 1
    verify_test_feed(feeds["edges"][0]["node"])

    profile = external_feed.profile(feeds["edges"][0]["node"]["id"])
    verify_test_feed(profile)
    assert profile["topic"]["id"]
    assert profile["topic"]["name"] == "technology"

    trending = external_feed.list()
    assert len(trending["edges"]) == 1
    verify_test_feed(trending["edges"][0]["node"])

    def verify_feed(feed, external_feed_info):
        assert len(feed["posts"]["edges"]) == 1
        assert feed["posts"]["edges"][0]["node"]["title"] == "Test title"
        assert feed["posts"]["edges"][0]["node"]["summary"] == "Test summary"
        assert (
            feed["posts"]["edges"][0]["node"]["external_feed"]["id"]
            == external_feed_info["id"]
        )
        assert (
            feed["posts"]["edges"][0]["node"]["external_feed"]["icon_url"]
            == external_feed_info["icon_url"]
        )

    assert external_feed.poll()

    topic_feed = topic.feed("Topic:technology")
    verify_feed(topic_feed, external_feed_info["external_feed"])

    topic_feed = external_feed.feed("ExternalFeed:TestFeed")
    verify_feed(topic_feed, external_feed_info["external_feed"])

    response = external_feed.delete(feeds["edges"][0]["node"]["id"])
    verify_test_feed(response["external_feed"])

    feeds = external_feed.list()
    assert not feeds["edges"]

    assert external_feed.poll()


def test_test_external_feed():
    add_user_and_account()
    topic.create("technology")

    response = external_feed.test("http://cdn-dev.weclikd-beta.com/test/test_rss.xml")
    assert response["status"]["custom_status"] == "NEW_FEED"
    assert (
        response["external_feed"]["url"]
        == "http://cdn-dev.weclikd-beta.com/test/test_rss.xml"
    )
    assert response["external_feed"]["website"] == "https://www.example.com"

    external_feed.create(
        feed_name="TestFeed",
        url="http://cdn-dev.weclikd-beta.com/test/test_rss.xml",
        topic="technology",
        website="https://www.example.com",
    )

    response = external_feed.test("http://cdn-dev.weclikd-beta.com/test/test_rss.xml")
    assert response["status"]["custom_status"] == "DUPLICATE_FEED"
    assert (
        response["external_feed"]["url"]
        == "http://cdn-dev.weclikd-beta.com/test/test_rss.xml"
    )
    assert response["external_feed"]["name"] == "TestFeed"
    assert (
        response["external_feed"]["url"]
        == "http://cdn-dev.weclikd-beta.com/test/test_rss.xml"
    )
    assert response["external_feed"]["website"] == "https://www.example.com"
    assert response["external_feed"]["base_topic"] == "technology"
    assert "id" in response["external_feed"]
    assert not response["external_feed"]["icon_url"]


def test_edit_external_feed():
    add_user_and_account()
    topic.create("technology")

    response = external_feed.create(
        feed_name="TestFeed",
        url="http://cdn-dev.weclikd-beta.com/test/test_rss.xml",
        website="http://www.example.com",
        topic="",
        summary_source="rss",
        icon_url="https://www.google.com/favicon.ico",
    )

    response = external_feed.edit(
        topic="technology",
        feed_id=response["external_feed"]["id"],
        feed_name="Test Feed",
    )
    assert response["status"]["success"]
    verify_test_feed(response["external_feed"], name="Test Feed")
