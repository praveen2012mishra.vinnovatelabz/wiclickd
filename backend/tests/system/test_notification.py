from .test_user import add_user_and_account
from command import post, comment, account


def test_comment_notification():
    add_user_and_account(admin=True)
    response = post.create(title="post")["post"]
    post_id = response["id"]
    response = comment.create(post_id, text="comment1")["comment"]
    comment1_id = response["id"]

    user2_id = add_user_and_account(firebase_id="id2", username="user2")
    response = comment.create(comment1_id, text="comment2")["comment"]
    comment2_id = response["id"]
    response = comment.create(post_id, text="comment3")["comment"]
    comment3_id = response["id"]

    account.login()

    response = account.num_notifications()
    assert response["num_unread_notifications"] == 2
    response = account.mark_notifications_as_read()
    assert response["status"]["success"]
    response = account.num_notifications()
    assert response["num_unread_notifications"] == 0

    response = account.notifications(first=1)["notifications"]
    assert len(response["edges"]) == 1
    comment_notification = response["edges"][0]["node"]
    assert comment_notification["contents"]["my_content"]["id"] == post_id
    assert comment_notification["contents"]["my_content"]["title"] == "post"
    assert comment_notification["contents"]["their_comment"]["id"] == comment3_id
    assert comment_notification["contents"]["their_comment"]["text"] == "comment3"
    assert comment_notification["contents"]["post"]["id"] == post_id
    assert comment_notification["contents"]["post"]["title"] == "post"
    assert len(comment_notification["contents"]["discussion"]) == 1
    assert comment_notification["contents"]["discussion"][0]["text"] == "comment3"
    first_notification_id = comment_notification["id"]

    # get next notification
    response = account.notifications(first=2, after=response["pageInfo"]["endCursor"])[
        "notifications"
    ]
    assert len(response["edges"]) == 1
    assert not response["pageInfo"]["hasNextPage"]

    comment_notification = response["edges"][0]["node"]
    assert comment_notification["contents"]["my_content"]["id"] == comment1_id
    assert comment_notification["contents"]["my_content"]["text"] == "comment1"
    assert comment_notification["contents"]["their_comment"]["id"] == comment2_id
    assert comment_notification["contents"]["their_comment"]["text"] == "comment2"
    assert comment_notification["contents"]["post"]["id"] == post_id
    assert comment_notification["contents"]["post"]["title"] == "post"
    assert len(comment_notification["contents"]["discussion"]) == 2
    assert comment_notification["contents"]["discussion"][0]["text"] == "comment1"
    assert comment_notification["contents"]["discussion"][1]["text"] == "comment2"

    for each in response["edges"]:
        response = account.delete_notification(each["node"]["id"])
        assert response["status"]["success"]

    response = account.delete_notification(first_notification_id)
    assert response["status"]["success"]

    response = account.notifications(first=2)["notifications"]
    assert not response["pageInfo"]["hasNextPage"]
    assert not response["edges"]


def check_empty_notification():
    response = account.notifications(first=3)["notifications"]
    assert not response["edges"]


def check_one_notification():
    response = account.notifications(first=3)["notifications"]
    assert len(response["edges"]) == 1
    response = response["edges"][0]["node"]
    account.delete_notification(response["id"])
    return response["contents"]
