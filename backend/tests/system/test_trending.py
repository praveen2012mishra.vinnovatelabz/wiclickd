from .test_user import add_user_and_account
from command import topic, user, clik, admin, external_feed


def test_trending_users():
    user_1 = add_user_and_account(firebase_id="id1", username="test_user1")
    add_user_and_account(firebase_id="id2", username="test_user2")
    user.follow(user_1)

    response = user.list(first=1, sort="TRENDING")
    assert len(response["edges"]) == 1
    assert response["edges"][0]["node"]["username"] == "test_user1"
    response = user.list(
        first=1, sort="TRENDING", after=response["pageInfo"]["endCursor"]
    )
    assert len(response["edges"]) == 1
    assert response["edges"][0]["node"]["username"] == "test_user2"


def test_trending_cliks():
    add_user_and_account()
    response = clik.create("Gophers")
    add_user_and_account(firebase_id="id2", username="adsf")
    clik.create("Golfers")
    clik.follow(response["clik"]["id"])

    response = clik.list(first=1, sort="TRENDING")
    assert "pageInfo" in response
    assert len(response["edges"]) == 1
    assert response["edges"][0]["node"]["name"] == "Gophers"

    response = clik.list(
        first=2, sort="TRENDING", after=response["pageInfo"]["endCursor"]
    )
    assert len(response["edges"]) == 1
    assert response["edges"][0]["node"]["name"] == "Golfers"


def test_trending_topics():
    add_user_and_account(firebase_id="id1", username="user2")
    topic.create("topic1")
    topic.create("topic2")
    topic.create("SCIENCE")

    topic.follow("topic2")
    topic.follow("science")
    response = topic.list(first=2, sort="TRENDING")
    assert "pageInfo" in response
    assert len(response["edges"]) == 2
    assert response["edges"][0]["node"]["name"] == "science"
    assert response["edges"][1]["node"]["name"] == "topic2"

    response = topic.list(
        first=2, sort="TRENDING", after=response["pageInfo"]["endCursor"]
    )
    assert len(response["edges"]) == 1
    assert response["edges"][0]["node"]["name"] == "topic1"


def test_trending_feeds():
    add_user_and_account(firebase_id="id1", username="user2")

    external_feed.create(
        feed_name="TestFeed1",
        url="http://cdn-dev.weclikd-beta.com/test/test_rss.xml",
        website="http://www.example.com",
        topic="",
        summary_source="rss",
        icon_url="https://www.google.com/favicon.ico",
    )

    response = external_feed.create(
        feed_name="TestFeed2",
        url="http://cdn-dev.weclikd-beta.com/test/test_rss2.xml",
        website="http://www.example.com",
        topic="",
        summary_source="rss",
        icon_url="https://www.google.com/favicon.ico",
    )

    external_feed.follow(response["external_feed"]["id"])
    response = external_feed.list(first=1, sort="TRENDING")
    assert response["edges"][0]["node"]["name"] == "TestFeed2"

    response = external_feed.list(
        first=1, sort="TRENDING", after=response["pageInfo"]["endCursor"]
    )
    assert response["edges"][0]["node"]["name"] == "TestFeed1"

    response = external_feed.list(
        first=1, sort="TRENDING", after=response["pageInfo"]["endCursor"]
    )
    assert not response["pageInfo"]["hasNextPage"]
