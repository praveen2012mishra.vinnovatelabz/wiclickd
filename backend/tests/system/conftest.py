import os
import pytest
import pymysql
import redis
import time
import sys
import requests
import logging

logging.getLogger().setLevel(logging.INFO)

os.environ["TESTING"] = "ENABLED"
os.environ["PROJECT_ID"] = "test"

root_dir = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir)
from command import command_utils

conn = None
tables = [
    "account",
    "clik",
    "clik_follow",
    "clik_invite",
    "clik_join_request",
    "clik_member",
    "clik_post",
    "comment",
    "content",
    "content_like",
    "external_account",
    "external_feed",
    "feed_follow",
    "`like`",
    "likes_received",
    "report",
    "stripe_customer",
    "stripe_order",
    "stripe_payout",
    "topic",
    "topic_follow",
    "topic_alias",
    "topic_post",
    "user",
    "user_follow",
    "user_post",
    "user_new_post",
    "new_topic",
    "notification",
    "banned_topic",
    "new_clik",
    "url",
]


def pytest_addoption(parser):
    parser.addoption("--no-cleanup", action="store_true")


def cleanup_database():
    with conn.cursor() as cursor:
        cursor.execute("SET FOREIGN_KEY_CHECKS=0")
        for table in tables:
            cursor.execute(f"TRUNCATE {table}")

        cursor.execute("SET FOREIGN_KEY_CHECKS=1")
    conn.commit()

    r = redis.Redis(host=os.environ.get("REDIS_HOST")[:-5], port=6379, db=0)
    r.flushall()


@pytest.fixture(scope="session", autouse=True)
def test_init_finalize(request):
    global conn
    max_attempts = 120
    attempt = 0
    while attempt < max_attempts:
        try:
            conn_temp = pymysql.connect(
                host=os.environ.get("SQL_HOST")[:-5],
                user=os.environ.get("SQL_USER"),
                password=os.environ.get("SQL_PASSWORD"),
                db="weclikd",
                charset="utf8mb4",
            )
            response = requests.get(f"http://{os.environ.get('ES_HOST')}", timeout=1)
            if response.status_code != 200:
                raise RuntimeError("ES Connection Failed")

            response = requests.get(
                f"http://{os.environ.get('JANUS_HOST')}/v1/healthcheck", timeout=1
            )
            if response.status_code == 200:
                print("Janus is ready")
            else:
                raise RuntimeError(f"Janus Not Ready: {response.text}")
            conn = conn_temp
            break
        except Exception as ex:
            print(ex)
            time.sleep(1)
            attempt += 1
    if not conn or not conn.open:
        sys.exit(1)

    time.sleep(1)
    os.system(
        f"python {os.path.join(root_dir, 'db', 'elasticsearch', 'cli.py')} init all"
    )

    def finalize():
        try:
            if not request.config.getoption("--no-cleanup"):
                cleanup_database()
        except:
            pass

    request.addfinalizer(finalize)


def pytest_sessionfinish(session, exitstatus):
    """
    Called after whole test run finished, right before
    returning the exit status to the system.
    """
    pass


@pytest.fixture(autouse=True)
def before_each_testcase(request):
    cleanup_database()
    command_utils.clear_auth()
