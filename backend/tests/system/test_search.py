from command import account, topic, clik, user, external_feed
from .test_user import add_user_and_account
import requests
import os

ES_HOST = f"http://{os.environ.get('ES_HOST')}"
ROOT_DIR = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir)


def recreate_index(index: str):
    os.system(
        f"python {os.path.join(ROOT_DIR, 'db', 'elasticsearch', 'cli.py')} init {index}"
    )
    refresh_es(index)


def refresh_es(index: str):
    requests.post(f"{ES_HOST}/{index}-test/_refresh")



def test_topic_search():
    recreate_index("topic")
    add_user_and_account()

    topic_id = topic.create("topic1")["topic"]["id"]
    refresh_es("topic")
    topic.follow(topic_id)
    refresh_es("topic")
    response = topic.search("topic1")["everything"]["topics"]
    assert len(response) == 1
    assert response[0]["name"] == "topic1"
    assert response[0]["num_followers"] == 1

    response = topic.search("topic2")["everything"]["topics"]
    print(response)

    topic.edit(
        topic_id=topic_id,
        name="topic2",
        description="new description",
        aliases=["topic-alias"],
    )
    topic.unfollow(topic_id)
    refresh_es("topic")

    response = topic.search("topic1")["everything"]["topics"]
    assert not response

    response = topic.search("topic2")["everything"]["topics"]
    print(response)
    assert len(response) == 1
    response = response[0]
    assert response["id"] == topic_id
    assert response["name"] == "topic2"
    assert response["description"] == "new description"
    assert response["aliases"] == ["topic-alias"]
    assert response["num_followers"] == 0

    topic.delete(topic_id)
    refresh_es("topic")
    response = topic.search("topic2")["everything"]["topics"]
    assert not response


def test_clik_search():
    recreate_index("clik")
    add_user_and_account(firebase_id="id1")

    clik_id = clik.create("clik1")["clik"]["id"]
    refresh_es("clik")
    add_user_and_account(firebase_id="id2", username="asdf")
    clik.follow(clik_id)
    refresh_es("clik")
    response = clik.search("clik1")["everything"]["cliks"]
    assert len(response) == 1
    assert response[0]["name"] == "clik1"
    assert response[0]["num_followers"] == 2
    assert response[0]["num_members"] == 1
    account.login(firebase_id="id1")
    clik.edit(
        clik_id=clik_id, description="new description",
    )
    clik.unfollow(clik_id)
    refresh_es("clik")
    response = clik.search("clik1")["everything"]["cliks"]
    assert len(response) == 1
    response = response[0]
    assert response["id"] == clik_id
    assert response["name"] == "clik1"
    assert response["description"] == "new description"
    assert response["num_followers"] == 1
    assert response["num_members"] == 0

    clik.delete(clik_id)
    refresh_es("clik")
    response = clik.search("clik1")["everything"]["cliks"]
    assert not response


def test_user_search():
    recreate_index("user")
    user_id = add_user_and_account(username="user1")

    refresh_es("user")
    response = user.search("user1")["everything"]["users"]
    assert len(response) == 1
    assert response[0]["username"] == "user1"

    user.edit(
        username="user2", description="new description",
    )
    refresh_es("user")
    response = user.search("user1")["everything"]["users"]
    assert len(response) == 0

    response = user.search("user2")["everything"]["users"]
    response = response[0]
    assert response["id"] == user_id
    assert response["username"] == "user2"
    assert response["description"] == "new description"


def test_feed_search():
    recreate_index("feed")
    add_user_and_account(username="user1")
    response = external_feed.create(
        feed_name="TestFeed",
        url="http://cdn-dev.weclikd-beta.com/test/test_rss.xml",
        website="http://www.example.com",
        topic="",
        summary_source="rss",
        icon_url="https://www.google.com/favicon.ico",
    )
    feed_id = response["external_feed"]["id"]
    refresh_es("feed")
    external_feed.follow(feed_id)
    refresh_es("feed")
    response = external_feed.search("TestFeed")["everything"]["feeds"]
    assert len(response) == 1
    assert response[0]["name"] == "TestFeed"
    assert response[0]["followers"] == 1

    external_feed.edit(
        feed_id=feed_id, feed_name="TestFeed2", website="http://www.example2.com",
    )
    external_feed.unfollow(feed_id=feed_id)
    refresh_es("feed")

    response = external_feed.search("TestFeed")["everything"]["feeds"]
    assert not response

    response = external_feed.search("TestFeed2")["everything"]["feeds"]
    assert len(response) == 1
    response = response[0]
    assert response["id"] == feed_id
    assert response["followers"] == 0
    assert response["name"] == "TestFeed2"
    assert response["website"] == "http://www.example2.com"

    external_feed.delete(feed_id)
    refresh_es("feed")
    response = external_feed.search("TestFeed2")["everything"]["feeds"]
    assert not response
