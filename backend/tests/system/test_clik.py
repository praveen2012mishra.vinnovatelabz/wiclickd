from .test_user import add_user_and_account
from command import clik, account
from .test_notification import check_empty_notification, check_one_notification
import pytest


def verify_clik_info(response, clik_id, members=None):
    assert response["name"].lower() == clik_id[clik_id.find(":") + 1 :].lower()
    assert response["description"] == "description"
    if members:
        assert response["num_members"] == members

    assert response["qualifications"] == ["qualification1", "qualification2"]
    assert response["max_members"] == 10
    assert response["website"] == "https://www.example.com"
    assert response["icon_pic"]


def create_clik_invite_member(name, user1, user2, login_to_member=False):
    add_user_and_account(firebase_id="id1", username=user1, admin=True)
    clik.create(name, invite_only=True)

    # some user tries to join clik
    user_id = add_user_and_account(firebase_id="id2", username=user2)
    response = clik.join(f"Clik:{name}", known_members=[f"User:{user1}"])
    assert response["status"]["custom_status"] == "PENDING"

    account.login(firebase_id="id1")

    # invite user, should have two members
    response = clik.invite(
        clik_id=f"Clik:{name}",
        invited_users=[{"userid_or_email": user_id, "member_type": "MEMBER"}],
    )
    assert response["status"]["success"]

    if login_to_member:
        account.login(firebase_id="id2")


def test_create_and_join():
    user2_id = add_user_and_account(firebase_id="id2", username="user2")
    add_user_and_account(username="user1")

    invited_users = [{"userid_or_email": user2_id, "member_type": "ADMIN"}]
    response = clik.create(
        "Physics", invited_users=invited_users, auto_approve=True, invite_only=True
    )
    assert response["status"]["status"] == "OK"

    response = clik.profile("Clik:Physics")
    verify_clik_info(response, "Physics", members=1)

    response = clik.profile(response["id"])
    verify_clik_info(response, "Physics", members=1)

    check_empty_notification()

    # user is invited, so will automatically be part of clik
    account.login("id2")
    response = check_one_notification()
    assert response["__typename"] == "ClikInviteRequestNotification"
    assert response["inviter"]["username"] == "user1"
    assert response["clik"]["name"] == "Physics"

    response = clik.join("Clik:Physics")
    assert response["status"]["status"] == "OK"
    assert response["status"]["custom_status"] == "JOINED"
    assert response["member_type"] == "ADMIN"

    check_empty_notification()

    response = clik.profile("Clik:Physics")
    verify_clik_info(response, "Physics", members=2)

    response = account.my_user()
    assert response["cliks_followed"][0]["clik"]["name"] == "Physics"
    assert response["cliks_followed"][0]["member_type"] == "ADMIN"

    account.login(firebase_id="id1")
    response = check_one_notification()
    assert response["__typename"] == "ClikInviteAcceptedNotification"
    assert response["invitee"]["username"] == "user2"
    assert response["clik"]["name"] == "Physics"

    # not invited, so will not automatically join
    user3_id = add_user_and_account(firebase_id="firebase_id3", username="user3")
    response = clik.join("Clik:Physics")
    assert response["status"]["status"] == "OK"
    assert response["status"]["custom_status"] == "PENDING"
    assert response["member_type"] is None

    check_empty_notification()

    response = account.my_user()
    assert not response["cliks_followed"]
    assert not response["cliks_followed"]

    # log-in to clik super admin, and invite the third user
    account.login()
    response = check_one_notification()
    assert response["__typename"] == "ClikJoinRequestNotification"
    assert response["invitee"]["username"] == "user3"
    assert response["clik"]["name"] == "Physics"

    invited_users = [
        {"userid_or_email": "user3", "member_type": "MEMBER"},
        {"userid_or_email": "", "member_type": "MEMBER"},
    ]
    response = clik.invite(clik_id="Clik:Physics", invited_users=invited_users)
    assert response["status"]["status"] == "OK"

    response = check_one_notification()
    assert response["__typename"] == "ClikInviteAcceptedNotification"
    assert response["invitee"]["username"] == "user3"
    assert response["clik"]["name"] == "Physics"

    response = clik.profile("Clik:Physics")
    verify_clik_info(response, "Physics", members=3)

    response = clik.members(clik_id="Clik:Physics", first=2)
    assert response["members"]["edges"][0]["node"]["user"]["username"] == "user1"
    assert response["members"]["edges"][0]["node"]["type"] == "SUPER_ADMIN"
    assert response["members"]["edges"][1]["node"]["user"]["username"] == "user2"
    assert response["members"]["edges"][1]["node"]["type"] == "ADMIN"
    assert response["members"]["pageInfo"]["hasNextPage"]

    response = clik.members(
        clik_id="Clik:Physics",
        first=2,
        after=response["members"]["pageInfo"]["endCursor"],
    )
    assert response["members"]["edges"][0]["node"]["user"]["username"] == "user3"
    assert response["members"]["edges"][0]["node"]["type"] == "MEMBER"
    assert not response["members"]["pageInfo"]["hasNextPage"]

    account.login(firebase_id="firebase_id3")
    response = check_one_notification()
    assert response["__typename"] == "ClikJoinAcceptedNotification"
    assert response["inviter"]["username"] == "user1"
    assert response["clik"]["name"] == "Physics"


def test_create_and_get_user_applications():
    admin_id = add_user_and_account(firebase_id="id1", username="user1")
    clik.create(name="Physics", invite_only=True)

    add_user_and_account(firebase_id="id2", username="user2")
    clik.join("Clik:Physics")
    add_user_and_account(firebase_id="id3", username="user3")
    clik.join("Clik:Physics")
    add_user_and_account(firebase_id="id4", username="user4")
    clik.join("Clik:Physics", known_members=[admin_id])

    # user has no permissions to see user applications
    response = clik.user_applications(clik_id="Clik:Physics")
    assert not response["applications"]["edges"]

    # login to admin account
    account.login(firebase_id="id1")

    response = clik.user_applications(clik_id="Clik:Physics", first=2)
    assert response["applications"]["edges"][0]["node"]["user"]["username"] == "user2"
    assert response["applications"]["edges"][1]["node"]["user"]["username"] == "user3"
    assert response["applications"]["pageInfo"]["hasNextPage"]

    response = clik.user_applications(
        clik_id="Clik:Physics",
        first=2,
        after=response["applications"]["pageInfo"]["endCursor"],
    )
    assert (
        not response["applications"]["edges"][0]["node"]["user"]["username"] == "user3"
    )
    assert (
        response["applications"]["edges"][0]["node"]["qualification"] == "qualification"
    )
    assert (
        response["applications"]["edges"][0]["node"]["known_members"][0]["username"]
        == "user1"
    )
    assert not response["applications"]["pageInfo"]["hasNextPage"]


"""
def test_create_clik_and_approve():
    add_user_and_account(firebase_id="id1", username="user1", admin=True)

    response = clik.create("Physics", auto_approve=False)
    assert response["status"] == "PENDING"
    response = clik.create("math", auto_approve=False)
    assert response["status"] == "PENDING"
    response = clik.create("stats", auto_approve=False)
    assert response["status"] == "PENDING"

    # clik has not been created yet since admin has not approve it
    response = clik.profile("Clik:Physics")
    assert response["id"] == "Clik:None"
    assert response["name"] == "ClikNotFound"

    response = clik.applications(first=2)
    assert response["edges"][0]["node"]["clik"]["name"] == "Physics"
    assert response["edges"][0]["node"]["qualification"] == "my qualification"
    assert response["edges"][1]["node"]["clik"]["name"] == "math"
    assert response["edges"][1]["node"]["qualification"] == "my qualification"
    assert response["pageInfo"]["hasNextPage"]

    response = clik.applications(first=2, after=response["pageInfo"]["endCursor"])
    assert response["edges"][0]["node"]["clik"]["name"] == "stats"
    assert response["edges"][0]["node"]["qualification"] == "my qualification"
    assert not response["pageInfo"]["hasNextPage"]

    response = clik.approve(approved=True, clik_id="Clik:physics")
    assert response["success"]
    response = clik.approve(approved=True, clik_id="Clik:stats", clik_name="Stats")
    assert response["success"]
    response = clik.approve(approved=False, clik_id="Clik:math")
    assert response["success"]

    # clik has not been created yet since admin has not approve it
    response = clik.profile("Clik:Physics")
    verify_clik_info(response, clik_id="Clik:Physics")
    response = clik.profile("Clik:Stats")
    verify_clik_info(response, clik_id="Clik:Stats")
    assert response["name"] == "Stats"  # changed the name to upper case
    response = clik.profile("Clik:Math")  # this one was rejected
    assert response["id"] == "Clik:None"
    assert response["name"] == "ClikNotFound"

    # check that clik applications is empty
    response = clik.applications(first=2)
    assert not response["edges"]
    assert not response["pageInfo"]["hasNextPage"]
"""


def test_edit_clik():
    add_user_and_account(firebase_id="firebase_id1", username="user1", admin=True)
    response = clik.create("Physics", auto_approve=True, invite_only=True)
    assert response["status"]["success"]

    # change all clik profile fields
    response = clik.edit(
        clik_id="Clik:Physics",
        description="new description",
        banner_pic="1234",
        website="www.newwebsite.com",
        qualifications=["newqualification"],
        max_members=101,
        invite_only=False,
    )
    assert response["status"]["success"]
    assert not response["clik"]["invite_only"]
    assert response["clik"]["description"] == "new description"

    response = clik.profile("Clik:Physics")
    assert response["description"] == "new description"
    assert "1234" in response["banner_pic"]
    assert response["website"] == "www.newwebsite.com"
    assert response["qualifications"] == ["newqualification"]
    assert response["max_members"] == 101
    assert not response["invite_only"]

    # only change description, every other field should stay the same
    response = clik.edit(clik_id="Clik:Physics", description="new description 2")
    assert response["status"]["success"]

    response = clik.profile("Clik:Physics")
    assert response["description"] == "new description 2"
    assert "1234" in response["banner_pic"]
    assert response["website"] == "www.newwebsite.com"
    assert response["qualifications"] == ["newqualification"]
    assert response["max_members"] == 101


def test_edit_clik_member_has_no_permissions():
    create_clik_invite_member(
        "Physics", user1="user1", user2="user2", login_to_member=True
    )
    clik.edit(
        clik_id="Clik:Physics",
        description="new description 2",
    )
    response = clik.profile("Clik:Physics")
    assert response["description"] != "new description 2"


def test_reject_clik_member():
    add_user_and_account(firebase_id="id1", username="user1", admin=True)
    clik.create("Physics", invite_only=True)

    add_user_and_account(firebase_id="id2", username="user2")
    response = clik.join("Clik:Physics", known_members=["User:user1"])
    assert response["status"]["custom_status"] == "PENDING"

    account.login(firebase_id="id1")
    response = clik.reject_member("Clik:Physics", user_id="User:user2")
    assert response["status"]["success"]

    response = clik.user_applications("Clik:Physics")
    assert not response["applications"]["edges"]


def test_kick_clik_member():
    create_clik_invite_member("Physics", user1="user1", user2="user2")
    response = clik.members("Clik:Physics")
    assert len(response["members"]["edges"]) == 2

    response = clik.profile("Clik:Physics")
    assert response["num_members"] == 2

    # kick user, should have 1 member
    response = clik.kick_member(clik_id="Clik:Physics", user_id="User:user2")
    assert response["status"]["success"]
    response = clik.members("Clik:Physics")
    assert len(response["members"]["edges"]) == 1

    response = clik.profile("Clik:Physics")
    assert response["num_members"] == 1


def test_kick_clik_member_no_permissions():
    create_clik_invite_member(
        "Physics", user1="user1", user2="user2", login_to_member=True
    )
    clik.kick_member(clik_id="Clik:Physics", user_id="User:user1")

    # should still have two members
    response = clik.members("Clik:Physics")
    assert len(response["members"]["edges"]) == 2


def test_promote_clik_member():
    create_clik_invite_member("Physics", user1="user1", user2="user2")
    clik.promote_member(
        clik_id="Clik:Physics", user_id="User:user2", member_type="ADMIN"
    )
    response = clik.members("Clik:Physics")
    assert response["members"]["edges"][0]["node"]["type"] == "SUPER_ADMIN"
    assert response["members"]["edges"][1]["node"]["type"] == "ADMIN"

    clik.promote_member(
        clik_id="Clik:Physics", user_id="User:user2", member_type="MEMBER"
    )
    response = clik.members("Clik:Physics")
    assert response["members"]["edges"][0]["node"]["type"] == "SUPER_ADMIN"
    assert response["members"]["edges"][1]["node"]["type"] == "MEMBER"


def test_promote_clik_member_no_permissions():
    create_clik_invite_member(
        "Physics", user1="user1", user2="user2", login_to_member=True
    )

    # member cannot change super admin's member type
    clik.promote_member(
        clik_id="Clik:Physics", user_id="User:user1", member_type="ADMIN"
    )
    response = clik.members(clik_id="Clik:Physics")
    assert response["members"]["edges"][0]["node"]["type"] == "SUPER_ADMIN"
    assert response["members"]["edges"][1]["node"]["type"] == "MEMBER"


def test_public_clik():
    add_user_and_account(firebase_id="id1", username="user1", admin=True)
    clik.create("Physics", invite_only=False)

    add_user_and_account(firebase_id="id2", username="user2")
    response = clik.join("Clik:Physics")
    assert response["status"]["custom_status"] == "JOINED"

    response = clik.members(clik_id="Clik:Physics")
    assert response["members"]["edges"][0]["node"]["type"] == "SUPER_ADMIN"
    assert response["members"]["edges"][1]["node"]["type"] == "MEMBER"

    check_empty_notification()

    account.login(firebase_id="id1")
    check_empty_notification()


def test_delete_clik():
    add_user_and_account(firebase_id="id1", username="user1", admin=True)
    clik.create("Physics", invite_only=False)

    response = clik.delete("Clik:Physics2")
    assert response["status"]["status"] == "NOT_FOUND"

    response = clik.delete("Clik:Physics")
    assert response["status"]["status"] == "OK"

    response = account.login(firebase_id="id1")
    assert not response["account"]["my_users"][0]["cliks_followed"]


def test_invite_to_clik_via_key():
    add_user_and_account(firebase_id="id1", username="user1", admin=True)
    clik.create("Physics", invite_only=False)

    response = clik.create_invite_key(clik_id="Clik:Physics", member_type="ADMIN")
    invite_key = response["invite_key"]
    assert invite_key

    response = clik.get_invite_key(invite_key)
    assert not response["expired"]
    assert response["clik"]["name"] == "Physics"
    assert response["inviter"]["username"] == "user1"

    # Join Clik with invite key
    add_user_and_account(firebase_id="id2", username="user2")
    response = clik.join(invite_key=invite_key)
    assert response["member_type"] == "ADMIN"

    # check user is following clik now
    response = account.login(firebase_id="id2")
    assert len(response["account"]["my_users"][0]["cliks_followed"]) == 1
    assert (
        response["account"]["my_users"][0]["cliks_followed"][0]["clik"]["name"]
        == "Physics"
    )
    assert (
        response["account"]["my_users"][0]["cliks_followed"][0]["member_type"]
        == "ADMIN"
    )

    # try to create an invite key with member type as admin but fails since admins cannot invite other admins
    response = clik.create_invite_key(clik_id="Clik:Physics", member_type="ADMIN")
    assert response["status"]["status"] == "PERMISSION_DENIED"

    # check inviter got notification
    account.login(firebase_id="id1")
    response = account.notifications(first=2)["notifications"]
    assert len(response["edges"]) == 1
    clik_invite_accepted = response["edges"][0]["node"]
    assert (
        clik_invite_accepted["contents"]["__typename"]
        == "ClikInviteAcceptedNotification"
    )
    assert clik_invite_accepted["contents"]["invitee"]["username"] == "user2"
    assert clik_invite_accepted["contents"]["clik"]["name"] == "Physics"


def test_invite_to_clik_via_key_from_create_account():
    # Create account, clik, clik_invite_key
    add_user_and_account(firebase_id="id1", username="user1", admin=True)
    clik.create("Physics", invite_only=True)

    response = clik.create_invite_key(clik_id="Clik:Physics", member_type="MEMBER")
    invite_key = response["invite_key"]
    assert invite_key

    response = clik.get_invite_key(invite_key)
    assert not response["expired"]
    assert response["clik"]["name"] == "Physics"
    assert response["inviter"]["username"] == "user1"

    # New user creates account with clik invite key
    add_user_and_account(
        firebase_id="id2", username="user2", admin=False, clik_invite_key=invite_key
    )
    response = account.login(firebase_id="id2")
    assert len(response["account"]["my_users"][0]["cliks_followed"]) == 1
    assert (
        response["account"]["my_users"][0]["cliks_followed"][0]["clik"]["name"]
        == "Physics"
    )
    assert (
        response["account"]["my_users"][0]["cliks_followed"][0]["member_type"]
        == "MEMBER"
    )


def test_join_public_clik_via_key_from_create_account():
    # Create account, clik, clik_invite_key
    add_user_and_account(firebase_id="id1", username="user1", admin=True)
    clik.create("Physics", invite_only=False)

    # New user creates account with clik invite key
    add_user_and_account(
        firebase_id="id2", username="user2", admin=False, clik_invite_key="Clik:Physics"
    )
    response = account.login(firebase_id="id2")
    assert len(response["account"]["my_users"][0]["cliks_followed"]) == 1
    assert (
        response["account"]["my_users"][0]["cliks_followed"][0]["clik"]["name"]
        == "Physics"
    )
    assert (
        response["account"]["my_users"][0]["cliks_followed"][0]["member_type"]
        == "MEMBER"
    )
