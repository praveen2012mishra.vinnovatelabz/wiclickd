from command import account, account_settings, command_utils
from command.command_utils import CheckGraphQLError, set_auth, clear_auth
from .utils import verify_node_id
from plutus.constants import stripe_constants
import urllib
import pytest
import stripe
import base64


def verify_account(response):
    if "account" in response:
        response = response["account"]
    verify_node_id("Account", response["id"])
    assert response["first_name"] == "John"
    assert response["last_name"] == "Doe"
    assert response["email"] == "test@example.com"
    assert "subscription" in response["settings"]
    assert "dm_settings" in response["settings"]["privacy"]
    assert "notification_email" in response["settings"]["email_notifications"]
    assert "monthly_earnings" in response["settings"]["email_notifications"]
    assert "clik_notifications" in response["settings"]["email_notifications"]
    assert "weclikd_updates" in response["settings"]["email_notifications"]
    return response.get("my_users")


def test_create_account_and_invite():
    response = account.create(firebase_id="id1", username="user1")
    verify_account(response)
    response = account.info()
    verify_account(response)

    response2 = account.create(
        username="user2", firebase_id="id2", inviter_username="user1"
    )
    verify_account(response2)

    response = account.login(firebase_id="id2")
    assert (
        response["account"]["my_users"][0]["users_followed"][0]["user"]["username"]
        == "user1"
    )

    account.login(firebase_id="id1")
    notifications = account.notifications()["notifications"]
    assert len(notifications["edges"]) == 1
    notifications["edges"][0]["node"]["contents"][
        "__typename"
    ] == "UserInviteAcceptedNotification"
    notifications["edges"][0]["node"]["contents"]["invitee"]["username"] == "user2"


def test_account_does_not_exist():
    clear_auth()
    set_auth(
        firebase_auth=account.create_firebase_jwt(base64.b64encode(b"asdf").decode())
    )
    response = account.login()
    assert response["status"]["status"] == "NOT_FOUND"


def test_error_account_already_exists():
    response = account.check_username(username="thekncokoutvc")
    assert response["status"]["success"]

    response = account.create(username="thekncokoutvc")
    assert response["status"]["success"]
    response = account.create(username="thekncokoutvc")
    assert not response["status"]["success"]
    assert response["status"]["status"] == "ALREADY_EXISTS"

    response = account.check_username(username="thekncokoutvc")
    assert not response["status"]["success"]


""" TODO: Re-test account settings
def test_change_account_settings():
    account_response = await add_account(test_cli, username='theknockoutvc')

    # only change privacy setting
    privacy = {
        'dm_settings': 'DISABLED'
    }
    response = await change_settings(test_cli, privacy=privacy)
    assert response['success']
    response = await get_account(test_cli, account_response['id'])
    verify_account(response)
    assert response['settings']['subscription'] == 'BASIC'
    assert response['settings']['privacy']['dm_settings'] == 'DISABLED'
    assert response['settings']['email_notifications']['monthly_earnings']
    assert response['settings']['email_notifications']['clik_notifications']
    assert response['settings']['email_notifications']['weclikd_updates']

    # only change email_notification setting
    email_notifications = {
        'notification_email': 'changed_email@example.com',
        'monthly_earnings': False,
        'clik_notifications': False,
        'weclikd_updates': False
    }
    response = await change_settings(test_cli, email_notifications=email_notifications)
    assert response['success']
    response = await get_account(test_cli, account_response['id'])
    verify_account(response)
    assert response['settings']['subscription'] == 'BASIC'
    assert response['settings']['privacy']['dm_settings'] == 'DISABLED'
    assert not response['settings']['email_notifications']['monthly_earnings']
    assert not response['settings']['email_notifications']['clik_notifications']
    assert not response['settings']['email_notifications']['weclikd_updates']
"""


def test_change_subscription():
    try:
        if not stripe_constants.STRIPE_SECRET_KEY:
            return
    except:
        return

    stripe.api_key = stripe_constants.STRIPE_SECRET_KEY

    account_response = account.create(username="theknockoutvc")
    account_response = account_response["account"]
    assert account_response["settings"]["subscription"] == "BASIC"

    response = account.payment_info()
    assert response["payment_info"]["__typename"] == "NoPaymentInfo"
    assert not response["payment_info"]["status"]["success"]

    payment = stripe.PaymentMethod.create(
        type="card",
        card={
            "number": "4242424242424242",
            "exp_month": 2,
            "exp_year": 2022,
            "cvc": "222",
        },
    )
    response = account_settings.edit_subscription(
        subscription="GOLD",
        payment_id=payment.id,
        email="test@example.com",
        pricing="YEARLY",
    )
    assert response["status"]["success"]

    response = account.payment_info()
    assert response["payment_info"]["__typename"] == "CreditCard"
    assert response["payment_info"]["last4"] == "4242"
    assert response["payment_info"]["exp_month"] == 2
    assert response["payment_info"]["email"] == "test@example.com"

    payment = stripe.PaymentMethod.create(
        type="card",
        card={
            "number": "4242424242424242",
            "exp_month": 3,
            "exp_year": 2033,
            "cvc": "222",
        },
    )

    # change payment info
    response = account.change_payment_info(payment_id=payment.id)
    assert response["status"]["success"]

    response = account.payment_info()
    assert response["payment_info"]["__typename"] == "CreditCard"
    assert response["payment_info"]["last4"] == "4242"
    assert response["payment_info"]["exp_month"] == 3
    assert response["payment_info"]["email"] == "test@example.com"

    response = account.info()
    assert response["settings"]["subscription"] == "GOLD"

    # cancel
    response = account_settings.edit_subscription(subscription="BASIC")
    assert response["status"]["success"]

    response = account.payment_info()
    assert response["payment_info"]["__typename"] == "CreditCard"
    assert response["payment_info"]["last4"] == "4242"

    response = account.info()
    assert response["settings"]["subscription"] == "BASIC"


def test_stripe_payouts():
    try:
        if not stripe_constants.STRIPE_SECRET_KEY:
            return
    except:
        return

    account.create(username="theknockoutvc")
    parameters = {
        "Authorization": command_utils.current_auth.authorization,
    }
    response = command_utils.rest_get(
        f"/plutus/payout/authorize?{urllib.parse.urlencode(parameters)}"
    )
    assert response.status_code == 302
    assert response.headers["Location"].startswith("https://connect.stripe.com")
    # difficult to test the rest in an automated fashion

