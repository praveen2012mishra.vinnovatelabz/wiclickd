import re
import time
import os

id_regex = re.compile("[a-zA-Z0-9_-]{22}")
DATASTORE_EMULATOR_HOST = os.environ.get("DATASTORE_EMULATOR_HOST", "localhost:8082")
JANUS_HOST = os.environ.get("JANUS_HOST", "localhost:8443")


def verify_node_id(name, value):
    if name:
        assert value.startswith(name + ":")
        length = len(name) + 1
    else:
        length = 0
    assert value[length:].isnumeric()


def verify_async(test_function, *args, seconds_to_retry=3, **kwargs):
    start = time.time()
    attempts = 0
    while True:
        try:
            attempts += 1
            test_function(*args, **kwargs)
            break
        except AssertionError:
            if time.time() - start >= seconds_to_retry:
                assert (
                    False
                ), f"async {test_function.__name__} failed after {attempts} attempts"
            time.sleep(0.2)
