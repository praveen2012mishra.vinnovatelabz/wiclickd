from .utils import *
from .test_user import add_user_and_account
from .test_topic import add_default_topics
from command import post, topic, clik


def verify_post(response, pictures=None):
    from .test_user import verify_user

    if "post" in response:
        response = response["post"]

    verify_node_id("Post", response["id"])

    if response.get("link"):
        assert not response.get("author")
    else:
        verify_user(response["author"])

    assert response["title"] == "title"
    assert response["summary"] == "summary"
    assert response["likes_score"] <= 100
    assert response["reports_score"] <= 100
    assert response["comments_score"] <= 100
    return response["id"]

    if pictures:
        for picture in pictures:
            for picture_link in response["pictures"]:
                if picture in picture_link:
                    break
            else:
                assert False


def test_add_post():
    add_user_and_account()
    add_default_topics()
    response = post.create()
    verify_post(response)


def test_add_post_with_link():
    add_user_and_account()
    response = post.create(url="https://www.example.com")["post"]
    assert response["link"] == "https://www.example.com"
    response = post.info(response["id"])
    assert response["link"] == "https://www.example.com"
    response = post.info(url="https://www.example.com")
    assert response["link"] == "https://www.example.com"


def test_edit_post_modify_all_fields():
    add_user_and_account()
    add_default_topics(additional_topics=["topic3"])
    post_response = post.create(
        topics=["topic1", "topic2"], thumbnail_pic="4321", url="https://www.example.com"
    )["post"]

    response = post.edit(
        post_id=post_response["id"],
        title="modified title",
        summary="modified summary",
        url="https://www.modified.com",
        topics=["topic1", "topic3"],
        thumbnail_pic="1337",
    )
    assert response["status"]["success"]

    response = post.info(post_id=post_response["id"])
    assert response["title"] == "modified title"
    assert response["summary"] == "modified summary"
    assert response["link"] == "https://www.modified.com"
    assert response["topics"] == ["topic1", "topic3", "child-topic", "root-topic"]
    assert "1337" in response["thumbnail_pic"]

    response = topic.feed(topic_id="Topic:topic3")
    assert len(response["posts"]["edges"]) == 1
    response = topic.feed(topic_id="Topic:topic2")
    assert len(response["posts"]["edges"]) == 0
    response = topic.feed(topic_id="Topic:child-topic")
    assert len(response["posts"]["edges"]) == 1
    response = topic.feed(topic_id="Topic:topic1")
    assert len(response["posts"]["edges"]) == 1


def test_edit_post_modify_one_field():
    add_user_and_account(admin=True)
    add_default_topics()
    post_response = post.create(
        topics=["topic1", "topic2"], thumbnail_pic="4321", url="https://www.example.com"
    )["post"]

    response = post.edit(
        post_id=post_response["id"],
        title="modified title",
    )
    assert response["status"]["success"]

    response = post.info(post_id=post_response["id"])
    assert response["title"] == "modified title"
    assert response["summary"] == post_response["summary"]
    assert response["link"] == post_response["link"]
    assert response["topics"] == post_response["topics"]
    assert response["thumbnail_pic"] == post_response["thumbnail_pic"]


def test_edit_post_clear_fields():
    add_user_and_account(admin=True)
    post_response = post.create(
        topics=["topic1", "topic2"], thumbnail_pic="4321", url="https://www.example.com"
    )["post"]

    response = post.edit(
        post_id=post_response["id"],
        title="modified title",
        thumbnail_pic="",
    )
    assert response["status"]["success"]

    response = post.info(post_id=post_response["id"])
    assert response["title"] == "modified title"
    assert response["summary"] == post_response["summary"]
    assert response["link"] == post_response["link"]
    assert response["topics"] == post_response["topics"]
    assert not response["thumbnail_pic"]


def test_share_post():
    add_user_and_account(admin=True)
    clik1 = clik.create("clik1")["clik"]
    clik2 = clik.create("clik2")["clik"]
    response = topic.create("root-topic")
    topic.create("topic1", parent="root-topic")
    topic.create("topic2", parent="root-topic")

    response = post.create(topics=["root-topic"], cliks=["clik1"])
    response = post.share(
        post_id=response["post"]["id"],
        topics=["topic1", "topic2", "root-topic", "asdf"],
        cliks=["clik1", "clik2", "asdf"],
    )
    assert response["status"]["success"]
    assert set(response["post"]["cliks"]) == set(["clik1", "clik2"])
    assert set(response["post"]["topics"]) == set(["root-topic", "topic1", "topic2"])

    assert len(topic.feed("topic1")["posts"]["edges"]) == 1
    assert len(topic.feed("topic2")["posts"]["edges"]) == 1
    assert len(topic.feed("root-topic")["posts"]["edges"]) == 1
    assert len(clik.feed("clik1")["posts"]["edges"]) == 1
    assert len(clik.feed("clik1")["posts"]["edges"]) == 1


def test_share_post_does_not_exist():
    add_user_and_account(admin=True)
    response = post.share(
        post_id=1245,
        topics=["topic1", "topic2", "root-topic"],
        cliks=["clik1", "clik2"],
    )
    assert response["status"]["status"] == "NOT_FOUND"


def test_url_cannot_be_shared_twice():
    add_user_and_account(admin=True)
    post_response = post.create(
        topics=["topic1", "topic2"], thumbnail_pic="4321", url="https://www.google.com"
    )["post"]
    verify_post(post_response)

    post_response = post.create(
        topics=["topic1", "topic2"], thumbnail_pic="4321", url="https://www.google.com"
    )["post"]
    assert post_response["id"] == post_response["id"]
