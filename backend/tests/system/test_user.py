import jwt
from .utils import verify_node_id
from command import account, user
from command.command_utils import CheckGraphQLError


def add_user_and_account(
    firebase_id="id1", username="theknockoutvc", admin=True, clik_invite_key=None
):
    response = account.create(
        firebase_id=firebase_id,
        username=username,
        admin=admin,
        clik_invite_key=clik_invite_key,
    )
    return response["account"]["my_users"][0]["user"]["id"]


def verify_user(response, username="theknockoutvc"):
    verify_node_id("User", response["id"])
    user_id = response["id"][5:]
    del response["id"]
    if "created" in response:
        del response["created"]
    del response["profile_pic"]
    del response["banner_pic"]
    assert response == {
        "username": username,
        "description": "Welcome to my profile.",
        "full_name": username,
    }

    return user_id


def verify_my_user(response, username="theknockoutvc"):
    verify_node_id("MyUser", response["id"])
    my_user_id = response["id"][7:]
    user_id = verify_user(response["user"], username)
    assert my_user_id == user_id
    return my_user_id


def test_username_duplicate():
    account.create(username="theknockoutvc")
    response = account.create(firebase_id="id2", username="theknockoutvc")
    assert response["status"]["status"] == "ALREADY_EXISTS"


def test_edit_user_all_fields():
    def verify_profile(profile, full_name="full_name"):
        assert profile["username"] == "new_username"
        assert profile["full_name"] == full_name
        assert "12" in profile["banner_pic"]
        assert "13" in profile["profile_pic"]
        assert profile["description"] == "new description"

    add_user_and_account()
    response = user.edit(
        username="new_username",
        full_name="full_name",
        banner_pic="12",
        profile_pic="13",
        description="new description",
    )

    assert response["status"]["success"]
    verify_profile(response["user"])

    response = user.profile(user_id="User:new_username")
    verify_profile(response)

    response = user.edit(full_name="full_name_2")
    verify_profile(response["user"], full_name="full_name_2")

    response = user.profile(user_id="User:new_username")
    verify_profile(response, full_name="full_name_2")


def test_edit_user_duplicate_username():
    add_user_and_account(username="user1")
    add_user_and_account(firebase_id="firebase_id2", username="user2")

    response = user.edit(username="user1")

    assert response["status"]["status"] == "ALREADY_EXISTS"
    assert not response.get("user")
