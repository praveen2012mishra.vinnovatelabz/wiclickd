from .test_user import add_user_and_account
from .test_feed import check_content_order
from command import post, comment, account, user


def test_like_post():
    # user 1 comments on post
    add_user_and_account(username="user1")

    post_id = post.create()["post"]["id"]

    comment0 = comment.create(parent_content_id=post_id)["comment"]
    comment1 = comment.create(parent_content_id=post_id)["comment"]
    comment2 = comment.create(parent_content_id=post_id)["comment"]
    comment3 = comment.create(parent_content_id=post_id)["comment"]

    # user 2 likes the post & comment
    add_user_and_account(firebase_id="firebase_id2", username="user2")

    post.like(post_id, like_type="RED")
    response = comment.like(comment0["id"], like_type="RED")
    response = comment.like(comment1["id"], like_type="SILVER")
    response = comment.like(comment2["id"], like_type="GOLD")
    response = comment.like(comment3["id"], like_type="DIAMOND")

    content_with_red_like = comment.info(comment0["id"])
    content_with_silver_like = comment.info(comment1["id"])
    content_with_gold_like = comment.info(comment2["id"])
    content_with_diamond_like = comment.info(comment3["id"])
    assert (
        content_with_red_like["likes_score"] < content_with_silver_like["likes_score"]
    )
    assert (
        content_with_silver_like["likes_score"] < content_with_gold_like["likes_score"]
    )
    assert (
        content_with_gold_like["likes_score"] < content_with_diamond_like["likes_score"]
    )

    assert content_with_red_like["user_like_type"] == "RED"
    assert content_with_silver_like["user_like_type"] == "SILVER"
    assert content_with_gold_like["user_like_type"] == "GOLD"
    assert content_with_diamond_like["user_like_type"] == "DIAMOND"

    # From user2's account:
    # discussing feed for user1 should have that post that user2 liked
    bookmarked_feed = user.feed("user1", first=10, sort="DISCUSSING")
    check_content_order(bookmarked_feed, [post_id])

    # login back to user 1
    """
    # TODO
    account.login()
    response = account.balance()["balance"]
    assert response["likes_monthly_total"]["red"] == 1
    assert response["likes_monthly_total"]["silver"] == 1
    assert response["likes_monthly_total"]["gold"] == 1
    assert response["likes_monthly_total"]["diamond"] == 1
    """

    # user 1 should not see user2's liked post
    bookmarked_feed = user.feed("user2", first=10, sort="DISCUSSING")
    check_content_order(bookmarked_feed, [])


def test_change_like():
    # user 1 comments on post
    add_user_and_account(username="user1")

    response = post.create()["post"]
    post_id = response["id"]
    response = comment.create(parent_content_id=post_id)["comment"]
    comment_id = response["id"]

    # user 2 likes twice
    add_user_and_account(firebase_id="firebase_id2", username="user2")

    comment.like(comment_id, like_type="RED")
    comment.like(comment_id, like_type="SILVER")
    comment.like(comment_id, like_type="GOLD")

    response = comment.info(comment_id)
    assert response["user_like_type"] == "GOLD"

    comment.like(comment_id, like_type="NONE")
    response = comment.info(comment_id)
    assert response["user_like_type"] is None

    account.login()
    """
       # TODO
    response = account.balance()["balance"]
    assert response["likes_monthly_total"]["red"] == 0
    assert response["likes_monthly_total"]["silver"] == 0
    assert response["likes_monthly_total"]["gold"] == 0
    assert response["likes_monthly_total"]["diamond"] == 0
    """

def test_cannot_like_own_content():
    add_user_and_account()
    response = post.create()
    response = comment.create(parent_content_id=response["post"]["id"])
    comment_id = response["comment"]["id"]
    response = comment.like(comment_id, like_type="RED")
    assert response["status"]["success"]

    response = comment.info(comment_id)
    assert not response["user_like_type"]
    assert response["comments_score"] == 0

    """
           # TODO
    response = account.balance()["balance"]
    assert response["likes_monthly_total"]["red"] == 0
    assert response["likes_monthly_total"]["silver"] == 0
    assert response["likes_monthly_total"]["gold"] == 0
    assert response["likes_monthly_total"]["diamond"] == 0
    """
