from .test_user import add_user_and_account
from command import post, feed, comment


def test_report_post():
    add_user_and_account(admin=False)
    response = post.create()["post"]
    comment.create(response["id"])

    # user report
    report_response = post.report(response["id"], reports=[{"type": "LOW_QUALITY"}])
    assert report_response["status"]["success"]

    response = post.info(response["id"])
    assert response["reports_score"] > 0


def test_report_admin():
    # admin reports
    add_user_and_account(admin=True)
    response = post.create()["post"]
    report_response = post.report(response["id"], reports=[{"type": "LOW_QUALITY"}])
    assert report_response["status"]["success"]

    response = feed.admin(sort="DISCUSSING")
    assert not response["posts"]["edges"]
