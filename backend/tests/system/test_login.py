from .test_account import verify_account
from .test_user import verify_my_user, add_user_and_account
from command import account, command_utils
from command.command_utils import CheckGraphQLError


def test_login_one_user():
    add_user_and_account()
    response = account.login()
    my_users = verify_account(response)
    verify_my_user(my_users[0])
    assert len(my_users) == 1


def test_login_with_access_key():
    add_user_and_account(firebase_id="BAH8ntVB88TtatFY2bI1qHRriv82")
    response = account.change_access_key()
    assert response["status"]["success"]
    assert response["access_key"].startswith("wk_")

    command_utils.current_auth.access_key = response["access_key"]
    command_utils.current_auth.authorization = None

    response = account.info()
    my_users = verify_account(response)
    verify_my_user(my_users[0])


def test_login_fail_no_account():
    add_user_and_account()
    response = account.login(firebase_id="invalid_firebase_id")
    assert response["status"]["status"] == "NOT_FOUND"
