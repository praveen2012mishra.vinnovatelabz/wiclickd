FROM python:3.7

WORKDIR /code

COPY ./service/requirements.txt /code/service/requirements.txt
COPY ./Pipfile /code/Pipfile
COPY ./Pipfile.lock /code/Pipfile.lock

RUN pip3 install pipenv
RUN pip3 install -r /code/service/requirements.txt
RUN pipenv lock -r --dev > /code/requirements-test.txt
RUN pip3 install -r /code/requirements-test.txt

COPY ./ /code

# run tests
CMD [ "python3", "-m", "pytest", "tests/system", "-v", "-s", "-x"]
