from sanic import Sanic, response

app = Sanic(__name__)


@app.route("/")
async def root_route(request):
    return response.text("Hello", 200)


app.run(host="0.0.0.0", port=8888, debug=True)
