FROM python:3.7

WORKDIR /code

COPY ./requirements.txt /code/requirements.txt

RUN pip3 install -r requirements.txt

EXPOSE 8080

COPY ./ /code/

CMD [ "python", "/code/main.py" ]