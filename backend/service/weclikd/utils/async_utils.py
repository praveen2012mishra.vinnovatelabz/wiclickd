import functools
import asyncio


def wrap(f):
    @functools.wraps(f)
    def inner(*args, **kwargs):
        loop = asyncio.get_event_loop()
        return loop.run_in_executor(None, lambda: f(*args, **kwargs))

    return inner
