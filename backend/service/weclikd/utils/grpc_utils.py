from grpc import StatusCode
from dataclasses import dataclass


@dataclass
class GrpcStatus:
    code: StatusCode
    details: str

    def __str__(self):
        return f"Code={self.code} Error={self.details}"
