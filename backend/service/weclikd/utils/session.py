from typing import Dict
import logging
from collections.abc import Mapping
from grpc import ServicerContext


class Session(ServicerContext, Mapping):
    def __init__(self, session_context: Dict = None, user_id: int = 0):
        self._session = session_context or {"current_user_id": user_id}
        self.status = None  # SUCCESS
        self.user_msg = None
        self.debug_error_msg = None
        self.grpc_code = None

    @classmethod
    def create_from_grpc(cls, context: ServicerContext):
        return Session({key: value for key, value in context.invocation_metadata()})

    @property
    def endpoint_type(self):
        return self._session.get("endpoint_type")

    @property
    def client_version(self):
        return self._session.get("client_version")

    @property
    def account_id(self):
        return int(self._session.get("current_user_id") or 0)

    @property
    def current_user_id(self):
        return int(self._session.get("current_user_id") or 0)

    @property
    def subscription_type(self):
        return self._session.get("subscription_type")

    @property
    def is_admin(self):
        return self._session.get("is_admin", False)

    def invocation_metadata(self):
        return self._session.items()

    def abort(self, **kwargs):
        pass

    def abort_with_status(self, **kwargs):
        pass

    def add_callback(self, **kwargs):
        pass

    def auth_context(self, **kargs):
        pass

    def cancel(self, **kargs):
        pass

    def is_active(self, **kargs):
        pass

    def peer(self, **kargs):
        pass

    def peer_identities(self, **kargs):
        pass

    def peer_identity_key(self, **kargs):
        pass

    def send_initial_metadata(self, **kargs):
        pass

    def set_code(self, code, **kargs):
        self.grpc_code = code

    def set_details(self, details, **kargs):
        logging.warning(details)
        self.user_msg = str(details)

    def set_custom_status(self, status):
        self.status = status

    def set_trailing_metadata(self, **kargs):
        pass

    def time_remaining(self, **kargs):
        pass

    def set_debug_error_msg(self, msg: str):
        self.debug_error_msg = msg

    def to_pubsub_attributes(self) -> Dict:
        return {key: str(value) for key, value in self._session.items()}

    def __iter__(self):
        for key in self._session:
            yield key

    def __len__(self):
        return len(self._session)

    def __getitem__(self, item):
        return str(self.__session.get(item, ""))
