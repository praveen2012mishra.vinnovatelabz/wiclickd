import time
import datetime
from collections import Mapping
from typing import Dict, Union, Type, List, Optional
from google.protobuf.message import Message
from google.protobuf.json_format import ParseDict, MessageToDict
from google.protobuf.timestamp_pb2 import Timestamp
from sqlalchemy.sql.base import Executable
from .session import Session
from weclikd.utils import time_utils


def row_to_proto(row: Dict, base_proto, data_field="data") -> Type[Message]:
    # Create model from sql database row
    if not isinstance(row, dict):
        row = {**row}
    for field, value in row.get(data_field, {}).items():
        row[field] = value

    if "created" in row:
        row["created"] = time_utils.to_iso_string(row["created"])

    populated_proto = ParseDict(row, base_proto, ignore_unknown_fields=True)
    return populated_proto


def proto_to_data(
    request: Type[Message], remove_fields: Optional[List[str]] = None
) -> Dict:
    proto_dict = MessageToDict(request, preserving_proto_field_name=True)

    if remove_fields is None:
        remove_fields = ["id", "created"]

    for field_path in remove_fields:
        field_path_list = field_path.split(".")
        current = proto_dict
        for field in field_path_list[:-1]:
            current = current[field]
        if field_path_list[-1] in current:
            del current[field_path_list[-1]]

    return proto_dict


class BaseModel:
    proto_class = None

    #
    # All SQL Related Statements that can be generated
    #
    @classmethod
    def insert_stmt(
        cls, request: Type[Message], session: Session, id: Optional[int] = None
    ) -> Executable:
        raise RuntimeError("parent class must implement")

    @classmethod
    def select_stmt(
        cls, request: Union[Type[Message], int], session: Session
    ) -> Executable:
        # Generate select executable sql statement from a protobuf message or id
        raise RuntimeError("parent class must implement")

    @classmethod
    def select_many_stmt(
        cls, request: Union[Type[Message], List[int]], session: Session
    ) -> Executable:
        # Generate select executable sql statement from a protobuf message or id
        raise RuntimeError("parent class must implement")

    @classmethod
    def delete_stmt(
        cls, request: Union[Type[Message], int], session: Session
    ) -> Executable:
        raise RuntimeError("parent class must implement")

    @classmethod
    def update_stmt(
        cls, request: Union[Type[Message], int], session: Session
    ) -> Executable:
        raise RuntimeError("parent class must implement")

    @classmethod
    def row_to_proto(cls, row: Dict, data_field="data", proto=None) -> Type[Message]:
        # Create model from sql database row
        row = {**row}
        for field, value in row.get(data_field, {}).items():
            row[field] = value

        if "created" in row:
            row["created"] = cls.to_iso_string(row["created"])

        populated_proto = ParseDict(row, cls.proto_class(), ignore_unknown_fields=True)
        if proto is not None:
            proto.MergeFrom(populated_proto)
            return proto
        return populated_proto

    @classmethod
    def proto_to_data(
        cls, request: Type[Message], remove_fields: Optional[List[str]] = None
    ) -> Dict:
        proto_dict = MessageToDict(request, preserving_proto_field_name=True)

        if remove_fields is None:
            remove_fields = ["id", "created"]

        for field_path in remove_fields:
            field_path_list = field_path.split(".")
            current = proto_dict
            for field in field_path_list[:-1]:
                current = current[field]
            if field_path_list[-1] in current:
                del current[field_path_list[-1]]

        return proto_dict

    @staticmethod
    def get_current_timestamp() -> Timestamp:
        return Timestamp(seconds=int(time.time()))

    @staticmethod
    def to_iso_string(timestamp, millis=False) -> str:
        if not millis:
            return timestamp.strftime("%Y-%m-%dT%H:%M:%SZ")
        else:
            return timestamp.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

    @staticmethod
    def non_iso_to_iso(timestamp_string) -> str:
        if isinstance(timestamp_string, datetime.datetime):
            return BaseModel.to_iso_string(timestamp_string)
        date, time = timestamp_string.split(" ")
        return f"{date}T{time}Z"

    @staticmethod
    def from_iso_string(timestamp_string, millis=True):
        if not timestamp_string:
            return None
        if not millis:
            return datetime.datetime.strptime(timestamp_string, "%Y-%m-%dT%H:%M:%SZ")
        else:
            return datetime.datetime.strptime(timestamp_string, "%Y-%m-%dT%H:%M:%S.%fZ")

    @staticmethod
    def recursive_data_update(original, new):
        for key, value in new.items():
            if isinstance(value, (Mapping,)):
                original[key] = BaseModel.recursive_data_update(
                    original.get(key, {}), value
                )
            else:
                original[key] = value
        return original
