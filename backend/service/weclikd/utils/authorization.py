from typing import Tuple, Dict
import jwt
import base64
from weclikd.exceptions import AuthenticationError
from weclikd.utils import environment
from firebase_admin import auth
import firebase_admin
import logging


def get_account_id_from_firebase_id(firebase_id):
    account_id = int.from_bytes(
        base64.b64decode(firebase_id)[0:8], byteorder="big", signed=False
    )
    if account_id > (1 << 63 - 1):
        account_id = int(account_id / 2)
    return account_id


def verify_user_token(request) -> Tuple[int, Dict]:
    header = request.headers.get("Authorization")
    return verify_jwt(header)


def verify_user_token_args(request) -> Tuple[int, int, Dict]:
    args = request.args.get("Authorization")
    return verify_jwt(args)


def verify_jwt(
    authorization_token, verify=True, verify_email=False
) -> Tuple[int, Dict]:
    if not authorization_token:
        if verify:
            raise AuthenticationError("User not logged in")
        else:
            return 0, {}

    weclikd_jwt = authorization_token.split()[1]
    if environment.is_test_env():
        auth_json = jwt.decode(
            weclikd_jwt, "secret", verify=False, algorithms=["HS256"]
        )
    else:
        try:
            auth_json = auth.verify_id_token(weclikd_jwt)
        except Exception as ex:
            if 'expired' in str(ex):
                raise AuthenticationError("Token expired")

    if auth_json:
        if auth_json.get("uid"):
            firebase_user_id = auth_json.get("uid")
        elif auth_json.get("user_id"):
            firebase_user_id = auth_json.get("user_id")
        elif verify:
            raise AuthenticationError("User not logged in")

        if not auth_json.get("email_verified") and verify_email:
            raise AuthenticationError("Email is not verified")

        return get_account_id_from_firebase_id(firebase_user_id), auth_json
    return 0, {}


def init_firebase():
    firebase_admin.initialize_app()


init_firebase()
