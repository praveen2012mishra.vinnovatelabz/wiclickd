import logging
import time
import aiomysql
import getpass
import sqlalchemy
from functools import wraps
from databases import Database
from databases.backends.mysql import MySQLBackend
from weclikd.exceptions import DuplicateKeyException
from weclikd.service.service_directory import Endpoint

db_wrapper = None
db_wrapper_2 = None

metadata = sqlalchemy.MetaData()


sql_endpoint: Endpoint = Endpoint(
    "SQL",
    default_user="app",
    default_host="localhost:3306",
    default_password="abc12345",
)


class SqlDatabase:
    def __init__(self, db=None):
        global db_wrapper
        self.tmp_dir = "/tmp"
        self.last_connection_attempt = 0
        self.db = db

        if not db_wrapper:
            db_wrapper = self

    async def start(self):
        if self.db is None or not self.db.is_connected:
            self.last_connection_attempt = time.time()
            db_url = (
                f"mysql://{await sql_endpoint.user}:{await sql_endpoint.password}@"
                f"{await sql_endpoint.host}/"
                f"weclikd?charset=utf8mb4&binary_prefix=true"
            )
            self.db = Database(db_url)
            self.db.mysql_backend = True
            await self.db.connect()
            logging.info("Connected to database")

    def is_connected(self):
        return self.db is not None and self.db.is_connected

    async def stop(self):
        if self.db is None:
            return

        logging.info("Disconnecting from database")
        await self.db.disconnect()

    async def execute(self, *args, **kwargs):
        return await self.db.execute(*args, **kwargs)

    async def execute_many(self, *args, **kwargs):
        return await self.db.execute_many(*args, **kwargs)

    async def fetch_one(self, *args, **kwargs):
        return await self.db.fetch_one(*args, **kwargs)

    async def fetch_all(self, *args, **kwargs):
        return await self.db.fetch_all(*args, **kwargs)

    async def attempt_to_reconnect(self):
        if self.db is None or not self.db.is_connected:
            if time.time() - self.last_connection_attempt > 10:
                await self.start()

    @staticmethod
    def check_duplicate(error_string="", ignore_error=False):
        class DuplicateChecker:
            def __enter__(self):
                pass

            def __exit__(self, _exception_type, exception_value, _traceback):
                if not exception_value:
                    return
                if (
                    "duplicate" in str(exception_value).lower()
                    or "unique constraint" in str(exception_value).lower()
                ):
                    if ignore_error:
                        return True
                    raise DuplicateKeyException(error_string)
                raise exception_value

        return DuplicateChecker()

    @staticmethod
    def check_missing(error_string="", ignore_error=False):
        class MissingChecker:
            def __enter__(self):
                pass

            def __exit__(self, _exception_type, exception_value, _traceback):
                if not exception_value:
                    return

                if "Fetch failed" in str(exception_value):
                    if ignore_error:
                        return True
                    raise Exception(error_string)
                raise exception_value

        return MissingChecker()


class MySQLBackendWithUnixSocketPatch(MySQLBackend):

    # Monkey-patching Databases module so it can connect to msyql using unix-sockets
    async def connect(self) -> None:
        assert self._pool is None, "DatabaseBackend is already running"
        kwargs = self._get_connection_kwargs()
        if "unix_socket" in self._database_url.options:  # set unix socket option here
            kwargs["unix_socket"] = self._database_url.options["unix_socket"]

        self._pool = await aiomysql.create_pool(
            host=self._database_url.hostname
            or "localhost",  # host was set to null, but aiomysql wants localhost
            port=self._database_url.port or 3306,
            user=self._database_url.username or getpass.getuser(),
            password=self._database_url.password,
            db=self._database_url.database,
            autocommit=True,
            **kwargs,
        )


class DBScanner:
    def __init__(self, db: SqlDatabase, model, table, whereclause=None, max_items=0):
        self.db = db
        self.model = model
        self.table = table
        self.whereclause = whereclause

    async def __aiter__(self):
        cursor = None
        while True:
            if self.whereclause is not None:
                sql_query = sqlalchemy.select(
                    [self.table], whereclause=self.whereclause
                ).limit(100)
            else:
                sql_query = (
                    sqlalchemy.select([self.table]).order_by(self.table.c.id).limit(100)
                )

            if cursor and self.whereclause is None:
                sql_query = sql_query.where(self.table.c.id > cursor)

            rows = await self.db.fetch_all(sql_query)
            for row in rows:
                if self.model:
                    yield self.model.row_to_proto(row)
                else:
                    yield row

            if len(rows) == 100 and self.whereclause is None:
                cursor = rows[-1].id
            else:
                break


class DBScanAndProcess:
    def __init__(self, db: SqlDatabase, table, callback, whereclause=None, max_items=0):
        self.db = db
        self.table = table
        self.callback = callback
        self.max_items = max_items
        self.whereclause = whereclause

    async def execute(self) -> int:
        scanner = DBScanner(self.db, None, self.table, self.whereclause)
        num_processed = 0
        async for row in scanner:
            num_processed += 1
            await self.callback(row)
            if num_processed == self.max_items:
                break

        return num_processed


# async def callback(db, row):
#
# table_scan_process = TableScanProcess(table, callback, max_items=0)
# await table_scan_process.execute()


def sql_transaction(func):
    @wraps(func)
    async def wrapper(*args, **kwargs):
        await db_wrapper.attempt_to_reconnect()
        async with db_wrapper.db.transaction():
            return await func(*args, **kwargs, db=db_wrapper)

    return wrapper


def sql_session(func):
    @wraps(func)
    async def wrapper(*args, **kwargs):
        await db_wrapper.attempt_to_reconnect()
        return await func(*args, db=db_wrapper, **kwargs)

    return wrapper


def get_db():
    return db_wrapper


def get_backup_db():
    return db_wrapper_2


def set_backup_db(db):
    global db_wrapper_2
    db_wrapper_2 = db
