from google.protobuf.timestamp_pb2 import Timestamp
import time
import datetime


def get_current_timestamp_pb2() -> Timestamp:
    return Timestamp(seconds=int(time.time()))


def datetime_to_timestamp_pb2(dt: datetime.datetime) -> Timestamp:
    if not dt:
        return Timestamp(seconds=0)
    return Timestamp(seconds=int(dt.timestamp()))


def convert_to_iso(timestamp_string) -> str:
    if isinstance(timestamp_string, datetime.datetime):
        return to_iso_string(timestamp_string)
    date, time = timestamp_string.split(" ")
    return f"{date}T{time}Z"


def from_unix_to_iso(unix_time) -> str:
    return datetime.datetime.fromtimestamp(unix_time).isoformat()


def from_unix_to_datetime(unix_time) -> str:
    return datetime.datetime.fromtimestamp(unix_time)


def to_iso_string(timestamp, millis=False) -> str:
    if not millis:
        return timestamp.strftime("%Y-%m-%dT%H:%M:%SZ")
    else:
        return timestamp.strftime("%Y-%m-%dT%H:%M:%S.%fZ")


def from_iso_string(timestamp_string: str, millis=False) -> datetime.datetime:
    if not timestamp_string:
        return None
    if not millis:
        return datetime.datetime.strptime(timestamp_string, "%Y-%m-%dT%H:%M:%SZ")
    else:
        return datetime.datetime.strptime(timestamp_string, "%Y-%m-%dT%H:%M:%S.%fZ")


def from_db_id_to_datetime(db_id: int) -> datetime.datetime:
    return datetime.datetime.fromtimestamp(1569888000 + (db_id >> 23))


def from_datetime_to_db_id(dt: datetime.datetime) -> int:
    return int(dt.timestamp() - 1569888000) << 23
