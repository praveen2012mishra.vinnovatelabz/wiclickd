import os
from enum import Enum


class DeploymentType(Enum):
    TEST = "test"
    DEV = "dev"
    STAGING = "staging"
    PROD = "prod"
    INFRASTRUCTURE = "infrastructure"


def is_test_env():
    return ENV_TYPE == DeploymentType.TEST


def is_dev_env():
    return ENV_TYPE == DeploymentType.DEV


def is_local_env():
    return os.path.exists("/Users/")


def _get_env_type() -> DeploymentType:
    if PROJECT_ID == "electric-block-241402":
        return DeploymentType.DEV
    elif PROJECT_ID == "graphite-tesla-246507":
        return DeploymentType.STAGING
    elif PROJECT_ID == "weclikd-prod":
        return DeploymentType.PROD
    elif PROJECT_ID == "weclikd-infrastructure":
        return DeploymentType.INFRASTRUCTURE
    else:
        return DeploymentType.TEST


def _get_public_domain():
    if ENV_TYPE == DeploymentType.DEV:
        return "https://electric-block-241402.appspot.com"
    elif ENV_TYPE == DeploymentType.STAGING:
        return "https://www.weclikd-beta.com"
    elif ENV_TYPE == DeploymentType.PROD:
        return "https://www.weclikd.com"
    else:
        return "http://localhost:8080"


def _get_image_bucket():
    if ENV_TYPE == DeploymentType.DEV:
        return "cdn-dev.weclikd-beta.com"
    elif ENV_TYPE == DeploymentType.STAGING:
        return "cdn.weclikd.com"
    elif ENV_TYPE == DeploymentType.PROD:
        return "cdn.weclikd.com"
    else:
        return "cdn-dev.weclikd-beta.com"


PROJECT_ID: str = os.environ.get("PROJECT_ID", "electric-block-241402")
LOGGING_LEVEL: str = os.environ.get("LOGGING_LEVEL", "info")
ENV_TYPE: DeploymentType = _get_env_type()
IMAGE_BUCKET: str = _get_image_bucket()
PUBSUB_TOKEN: str = "vDXdmSNd1n"
PUBLIC_DOMAIN: str = _get_public_domain()
