import hashlib


def string_to_id(data, normalize=True) -> int:
    if not data:
        return None

    if normalize:
        data = data.lower().replace("-", "")
        data = data.replace(" ", "")

    return int(
        int.from_bytes(
            hashlib.sha256(str.encode(data)).digest()[0:8],
            byteorder="big",
            signed=False,
        )
        / 2
    )


def file_to_id(contents) -> int:
    sha256_hash = hashlib.sha256(contents).hexdigest()
    return string_to_id(sha256_hash)


if __name__ == "__main__":
    import sys

    print(string_to_id(sys.argv[1], normalize=False))
