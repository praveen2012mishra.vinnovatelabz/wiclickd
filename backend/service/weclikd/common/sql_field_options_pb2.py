# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: weclikd/common/sql_field_options.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.protobuf import descriptor_pb2 as google_dot_protobuf_dot_descriptor__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='weclikd/common/sql_field_options.proto',
  package='weclikd',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n&weclikd/common/sql_field_options.proto\x12\x07weclikd\x1a google/protobuf/descriptor.proto:9\n\x10\x65xclude_from_sql\x12\x1d.google.protobuf.FieldOptions\x18\xd1\x86\x03 \x01(\x08:4\n\x0bprimary_key\x12\x1d.google.protobuf.FieldOptions\x18\xd2\x86\x03 \x01(\x08:0\n\x07indexed\x12\x1d.google.protobuf.FieldOptions\x18\xd3\x86\x03 \x01(\x08\x62\x06proto3'
  ,
  dependencies=[google_dot_protobuf_dot_descriptor__pb2.DESCRIPTOR,])


EXCLUDE_FROM_SQL_FIELD_NUMBER = 50001
exclude_from_sql = _descriptor.FieldDescriptor(
  name='exclude_from_sql', full_name='weclikd.exclude_from_sql', index=0,
  number=50001, type=8, cpp_type=7, label=1,
  has_default_value=False, default_value=False,
  message_type=None, enum_type=None, containing_type=None,
  is_extension=True, extension_scope=None,
  serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key)
PRIMARY_KEY_FIELD_NUMBER = 50002
primary_key = _descriptor.FieldDescriptor(
  name='primary_key', full_name='weclikd.primary_key', index=1,
  number=50002, type=8, cpp_type=7, label=1,
  has_default_value=False, default_value=False,
  message_type=None, enum_type=None, containing_type=None,
  is_extension=True, extension_scope=None,
  serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key)
INDEXED_FIELD_NUMBER = 50003
indexed = _descriptor.FieldDescriptor(
  name='indexed', full_name='weclikd.indexed', index=2,
  number=50003, type=8, cpp_type=7, label=1,
  has_default_value=False, default_value=False,
  message_type=None, enum_type=None, containing_type=None,
  is_extension=True, extension_scope=None,
  serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key)

DESCRIPTOR.extensions_by_name['exclude_from_sql'] = exclude_from_sql
DESCRIPTOR.extensions_by_name['primary_key'] = primary_key
DESCRIPTOR.extensions_by_name['indexed'] = indexed
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

google_dot_protobuf_dot_descriptor__pb2.FieldOptions.RegisterExtension(exclude_from_sql)
google_dot_protobuf_dot_descriptor__pb2.FieldOptions.RegisterExtension(primary_key)
google_dot_protobuf_dot_descriptor__pb2.FieldOptions.RegisterExtension(indexed)

# @@protoc_insertion_point(module_scope)
