class WeclikdException(Exception):
    pass


class MissingRequiredFieldException(WeclikdException):
    def __init__(self, object_name, field_name):
        super().__init__(self, f"{object_name} is missing field {field_name}")


class DuplicateKeyException(WeclikdException):
    pass


class UnsupportedFormat(WeclikdException):
    pass


class AuthenticationError(WeclikdException):
    pass
