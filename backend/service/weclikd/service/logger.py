import logging
import sys
from weclikd.utils import environment

logger = None


def setup_logger():
    global logger

    # During pytest, this will get called multiple times, causing the logger to be created multiple times.
    # Thus, we see duplicate logging without this check.
    if logger is not None:
        return

    logger = logging.getLogger()
    logger.propagate = False
    logger.setLevel(logging.NOTSET)
    log_format = logging.Formatter(
        "%(levelname)-8s[%(module)s:%(funcName)s:%(lineno)d] %(message)s"
    )
    logging_level = _get_logging_level()

    # INFO and below to stdout
    h1 = logging.StreamHandler(sys.stdout)
    h1.setLevel(logging_level)
    h1.addFilter(lambda record: record.levelno <= logging.INFO)
    h1.setFormatter(log_format)

    # WARNING and above to stderr
    h2 = logging.StreamHandler()
    h2.setLevel(logging.WARNING if logging_level <= logging.WARNING else logging_level)
    h2.setFormatter(log_format)
    logger.addHandler(h1)
    logger.addHandler(h2)

    if not environment.is_test_env() and "main.py" not in sys.argv[0]:
        # Imports the Cloud Logging client library
        import google.cloud.logging
        from google.cloud.logging_v2.handlers import CloudLoggingHandler

        # Instantiates a client
        client = google.cloud.logging.Client()
        handler = CloudLoggingHandler(client)
        google.cloud.logging.handlers.setup_logging(handler)


def _get_logging_level():
    logging_level = environment.LOGGING_LEVEL.lower()
    if logging_level == "debug":
        return logging.DEBUG
    elif logging_level == "info":
        return logging.INFO
    elif logging_level == "warning":
        return logging.WARNING
    elif logging_level == "error":
        return logging.ERROR
    elif logging_level == "critical":
        return logging.CRITICAL
    else:
        raise RuntimeError(f"Invalid logging level: {logging_level}")
