import aioredis
import logging
import time
from weclikd.service.service_directory import Endpoint

redis_pool = None
redis_endpoint: Endpoint = Endpoint(
    "REDIS", default_host="localhost:6379", default_password=""
)

last_checked_time = None


async def start():
    global redis_pool
    global last_checked_time
    redis_host = await redis_endpoint.host
    logging.info(f"Connecting to redis: {redis_host}")
    redis_pool = await aioredis.create_redis_pool(
        f"redis://{redis_host}",
        encoding="utf-8",
        timeout=1,
    )
    await health_check()
    logging.info(f"Connected to redis: {redis_host}")


async def health_check():
    global last_checked_time
    global redis_pool

    if not redis_pool:
        await start()

    try:
        last_checked_time = time.time()
        await redis_pool.ping()
    except Exception:
        logging.error("Trying to ping Redis again...")
        await redis_pool.ping()


async def stop():
    global redis_pool
    if not redis_pool:
        return
    redis_pool.close()
    await redis_pool.wait_closed()
    redis_pool = None


async def get() -> aioredis.Redis:
    if not redis_pool:
        await start()

    if time.time() - last_checked_time > 15:
        await health_check()

    return redis_pool


async def get_pipeline() -> aioredis.Redis:
    if not redis_pool:
        await start()

    if time.time() - last_checked_time > 15:
        await health_check()

    return redis_pool.pipeline()
