from weclikd.service import sql, redis


class Transaction:
    def __init__(self, *args, **kwargs):
        self.sql_transaction = None

    async def __aenter__(self):
        await sql.sql_instance.attempt_to_reconnect()
        self.sql_transaction = await sql.sql_instance.db.transaction().__aenter__()
        redis_pool = await redis.get()
        self.redis = redis_pool.pipeline()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.sql_transaction.__aexit__(exc_tb, exc_val, exc_tb)

        if len(self.redis._results) > 0:
            # if any commands are in the Redis pipeline, execute them
            self.results = await self.redis.execute()

    async def get_redis_pipeline(self):
        redis_pool = await redis.get()
        return redis_pool.pipeline()

    async def get_redis_instance(self):
        return await redis.get()

    async def execute(self, *args, **kwargs):
        return await sql.sql_instance.db.execute(*args, **kwargs)

    async def execute_many(self, *args, **kwargs):
        return await sql.sql_instance.db.execute_many(*args, **kwargs)

    async def fetch_one(self, *args, **kwargs):
        return await sql.sql_instance.db.fetch_one(*args, **kwargs)

    async def fetch_all(self, *args, **kwargs):
        return await sql.sql_instance.db.fetch_all(*args, **kwargs)
