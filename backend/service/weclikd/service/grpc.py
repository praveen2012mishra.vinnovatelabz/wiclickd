from typing import Type, Dict
from google.protobuf.message import Message
from google.protobuf.json_format import MessageToDict
import artemis.grpc
import athena.grpc
import atlas.grpc
import eros.grpc
import hestia.grpc
import plutus.grpc


artemis = artemis.grpc.Artemis()
athena = athena.grpc.Athena()
atlas = atlas.grpc.Atlas()
eros = eros.grpc.Eros()
hestia = hestia.grpc.Hestia()
plutus = plutus.grpc.Plutus()


def to_dict(
    message: Type[Message], field: str = None, including_default_values=False
) -> Dict:
    if not message:
        return {}

    transformed = MessageToDict(
        message,
        preserving_proto_field_name=True,
        including_default_value_fields=including_default_values,
    )
    if field:
        return transformed.get(field, {})
    return transformed


"""

class GRPCServer:
    def __init__(self, servicer, adder, port):
        self.servicer = servicer
        self.adder = adder
        self.port = port

    def start(self, loop):
        self.server = grpc.server(AsyncioExecutor(loop=loop))
        self.adder(self.servicer, self.server)
        self.server.add_insecure_port(f"[::]:{self.port}")
        self.server.start()
        logging.info(f"Staring gRPC server at {self.port}")

    def stop(self):
        self.server.stop(grace=1)

def _loop_mgr(loop: asyncio.AbstractEventLoop):

    asyncio.set_event_loop(loop)
    loop.run_forever()

    # If we reach here, the loop was stopped.
    # We should gather any remaining tasks and finish them.
    pending = asyncio.Task.all_tasks(loop=loop)
    if pending:
        loop.run_until_complete(asyncio.gather(*pending))


class AsyncioExecutor(futures.Executor):
    def __init__(self, *, loop=None):

        super().__init__()
        self._shutdown = False
        self._loop = loop or asyncio.get_event_loop()
        # self._thread = threading.Thread(target=_loop_mgr, args=(self._loop,),
        #                                daemon=True)
        # self._thread.start()

    def submit(self, fn, *args, **kwargs):

        if self._shutdown:
            raise RuntimeError("Cannot schedule new futures after shutdown")

        if not self._loop.is_running():
            raise RuntimeError(
                "Loop must be started before any function can " "be submitted"
            )

        if inspect.iscoroutinefunction(fn):
            coro = fn(*args, **kwargs)
            return asyncio.run_coroutine_threadsafe(coro, self._loop)

        else:
            func = functools.partial(fn, *args, **kwargs)
            return self._loop.run_in_executor(None, func)

    def shutdown(self, wait=True):
        # self._loop.stop()
        self._shutdown = True
        # if wait:
        #    self._thread.join()


# --------------------------------------------------------------------------- #


async def _call_behavior(rpc_event, state, behavior, argument, request_deserializer):
    context = _server._Context(rpc_event, state, request_deserializer)
    try:
        return await behavior(argument, context), True
    except Exception as exception:  # pylint: disable=broad-except
        with state.condition:
            if state.aborted:
                _server._abort(
                    state,
                    rpc_event.call,
                    _server.cygrpc.StatusCode.unknown,
                    b"RPC Aborted",
                )
            elif exception not in state.rpc_errors:
                details = "Exception calling application: {}".format(exception)
                _server._LOGGER.exception(details)
                _server._abort(
                    state,
                    rpc_event.call,
                    _server.cygrpc.StatusCode.unknown,
                    _server._common.encode(details),
                )
        return None, False


async def _take_response_from_response_iterator(rpc_event, state, response_iterator):
    try:
        return await response_iterator.__anext__(), True
    except StopAsyncIteration:
        return None, True
    except Exception as e:  # pylint: disable=broad-except
        with state.condition:
            if e not in state.rpc_errors:
                details = "Exception iterating responses: {}".format(e)
                _server.logging.exception(details)
                _server._abort(
                    state,
                    rpc_event.operation_call,
                    _server.cygrpc.StatusCode.unknown,
                    _server._common.encode(details),
                )
        return None, False


async def _unary_response_in_pool(
    rpc_event,
    state,
    behavior,
    argument_thunk,
    request_deserializer,
    response_serializer,
):
    argument = argument_thunk()
    if argument is not None:
        response, proceed = await _call_behavior(
            rpc_event, state, behavior, argument, request_deserializer
        )
        if proceed:
            serialized_response = _server._serialize_response(
                rpc_event, state, response, response_serializer
            )
            if serialized_response is not None:
                _server._status(rpc_event, state, serialized_response)


async def _stream_response_in_pool(
    rpc_event,
    state,
    behavior,
    argument_thunk,
    request_deserializer,
    response_serializer,
):
    argument = argument_thunk()
    if argument is not None:
        # Notice this calls the normal `_call_behavior` not the awaitable version.
        response_iterator, proceed = _server._call_behavior(
            rpc_event, state, behavior, argument, request_deserializer
        )
        if proceed:
            while True:
                response, proceed = await _take_response_from_response_iterator(
                    rpc_event, state, response_iterator
                )
                if proceed:
                    if response is None:
                        _server._status(rpc_event, state, None)
                        break
                    else:
                        serialized_response = _server._serialize_response(
                            rpc_event, state, response, response_serializer
                        )
                        if serialized_response is not None:
                            proceed = _server._send_response(
                                rpc_event, state, serialized_response
                            )
                            if not proceed:
                                break
                        else:
                            break
                else:
                    break


_server._unary_response_in_pool = _unary_response_in_pool
_server._stream_response_in_pool = _stream_response_in_pool

"""
