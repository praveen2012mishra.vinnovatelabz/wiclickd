from elasticsearch_async import AsyncElasticsearch, AsyncTransport
from elasticsearch_async.connection import AIOHttpConnection
from elasticsearch_async.connection_pool import AsyncConnectionPool
from aiohttp.client_exceptions import ServerFingerprintMismatch
import async_timeout
from elasticsearch import exceptions
from elasticsearch.exceptions import (
    ConnectionError,
    ConnectionTimeout,
    ImproperlyConfigured,
    SSLError,
)
from elasticsearch.connection import Connection
from elasticsearch.compat import urlencode
from elasticsearch.connection.http_urllib3 import create_ssl_context
from weclikd.service.service_directory import Endpoint
from weclikd.utils import environment
from weclikd.service.sql import SQLDb
from weclikd.service import grpc
from aiohttp import ClientTimeout


import asyncio
import ssl
import warnings
import aiohttp
import certifi
import logging
from typing import Dict


es: AsyncElasticsearch = None
es_endpoint: Endpoint = Endpoint(
    "ES",
    default_user="elastic",
    default_host="https://632db149d4724a0ba0a09dad0614b32c.us-central1.gcp.cloud.es.io:9243",
    default_password="",
    do_lookup=False,
)


async def start():
    global es
    es_host = await es_endpoint.host
    logging.info(f"Connecting to elasticsearch: {es_host}")
    try:
        es = AsyncElasticsearch(
            hosts=[await es_endpoint.host],
            http_auth=(await es_endpoint.user, await es_endpoint.password),
            verify_certs=True,
            ca_certs=certifi.where(),
            transport_class=PatchedAsyncTransport,
        )
        await es.info()
    except:
        logging.exception("ES Failed")
        raise
    logging.info(f"Connected to elasticsearch: {es_host}")


async def stop():
    await es.transport.close()


async def health_check():
    try:
        await es.info()
    except:
        logging.exception("ES Failed")
        raise Exception("Failed to connect to elasticsearch")


async def index(index: str, document: Dict, id: str = None):
    try:
        await es.index(
            index=f"{index}-{environment.ENV_TYPE.value}",
            id=id or document["id"],
            body=document,
        )
    except exceptions.ElasticsearchException as ex:
        logging.exception(f"Elastic Search failed on {index}")
        raise Exception(f"Index failed {ex}")


async def update(index: str, document: Dict, id: str = None):
    try:
        await es.update(
            index=f"{index}-{environment.ENV_TYPE.value}",
            id=id or document["id"],
            body={"doc": document},
        )
    except exceptions.ElasticsearchException as ex:
        if isinstance(ex, exceptions.NotFoundError):
            await es.index(
                index=f"{index}-{environment.ENV_TYPE.value}",
                id=id or document["id"],
                body=document,
            )
        else:
            raise Exception(f"Update failed {ex}")


async def delete(index: str, id: str):
    try:
        await es.delete(index=f"{index}-{environment.ENV_TYPE.value}", id=id)
    except exceptions.ElasticsearchException as ex:
        logging.exception(f"Elastic Search failed on {index}")
        raise Exception(f"Delete failed {ex}")


async def update_field(index: str, id: str, **kwargs):
    try:
        await es.update(
            index=f"{index}-{environment.ENV_TYPE.value}", id=id, body={"doc": kwargs}
        )
    except exceptions.ElasticsearchException as ex:
        logging.warning(f"Elastic Update Failed failed on {index}")


async def search(index: str, body: Dict):
    try:
        return await es.search(index=f"{index}-{environment.ENV_TYPE.value}", body=body)
    except exceptions.ElasticsearchException as ex:
        logging.exception(f"Elastic Search failed on {index}")
        raise Exception(f"Search failed {ex}")


async def create_index(index: str, body: Dict):
    try:
        await es.indices.delete(index=f"{index}-{environment.ENV_TYPE.value}")
    except:
        pass
    logging.info(f"Creating Index: {index}")
    await es.indices.create(index=f"{index}-{environment.ENV_TYPE.value}", body=body)


async def refresh_index(index_: str, sql_table: SQLDb):
    logging.info(f"Refreshing Index: {index_}")
    count = 0
    async for item in sql_table.scan():
        await index(index=index_, document=grpc.to_dict(item))
        count += 1
    logging.info(f"Reindexed {count} items")


async def update_mapping(index: str, mapping: Dict):
    await es.indices.put_mapping(
        index=f"{index}-{environment.ENV_TYPE.value}", body=mapping
    )


class PatchedAIOHttpConnection(Connection):
    def __init__(
        self,
        host="localhost",
        port=9200,
        http_auth=None,
        use_ssl=False,
        verify_certs=False,
        ca_certs=None,
        client_cert=None,
        client_key=None,
        loop=None,
        use_dns_cache=True,
        headers=None,
        ssl_context=None,
        **kwargs,
    ):
        super().__init__(host=host, port=port, **kwargs)

        self.loop = asyncio.get_event_loop() if loop is None else loop

        if http_auth is not None:
            if isinstance(http_auth, str):
                http_auth = tuple(http_auth.split(":", 1))

            if isinstance(http_auth, (tuple, list)):
                http_auth = aiohttp.BasicAuth(*http_auth)

        headers = headers or {}
        headers.setdefault("content-type", "application/json")

        # if providing an SSL context, raise error if any other SSL related flag is used
        if ssl_context and (verify_certs or ca_certs):
            raise ImproperlyConfigured(
                "When using `ssl_context`, `use_ssl`, `verify_certs`, `ca_certs` are not permitted"
            )

        if use_ssl or ssl_context:
            cafile = ca_certs
            if not cafile and not ssl_context and verify_certs:
                # If no ca_certs and no sslcontext passed and asking to verify certs
                # raise error
                raise ImproperlyConfigured(
                    "Root certificates are missing for certificate "
                    "validation. Either pass them in using the ca_certs parameter or "
                    "install certifi to use it automatically."
                )
            if verify_certs or ca_certs:
                warnings.warn(
                    "Use of `verify_certs`, `ca_certs` have been deprecated in favor of using SSLContext`",
                    DeprecationWarning,
                )

            if not ssl_context:
                # if SSLContext hasn't been passed in, create one.
                # need to skip if sslContext isn't avail
                try:
                    ssl_context = create_ssl_context(cafile=cafile)
                except AttributeError:
                    ssl_context = None

                if not verify_certs and ssl_context is not None:
                    ssl_context.check_hostname = False
                    ssl_context.verify_mode = ssl.CERT_NONE
                    warnings.warn(
                        "Connecting to %s using SSL with verify_certs=False is insecure."
                        % host
                    )
            if ssl_context:
                use_ssl = True

        self.session = aiohttp.ClientSession(
            auth=http_auth,
            timeout=ClientTimeout(total=self.timeout),
            connector=aiohttp.TCPConnector(
                use_dns_cache=use_dns_cache,
                ssl=ssl_context,
            ),
            headers=headers,
        )

        self.base_url = "http%s://%s:%d%s" % (
            "s" if use_ssl else "",
            host,
            port,
            self.url_prefix,
        )

    @asyncio.coroutine
    def close(self):
        yield from self.session.close()

    @asyncio.coroutine
    def perform_request(
        self, method, url, params=None, body=None, timeout=None, ignore=(), headers=None
    ):
        url_path = url
        if params:
            url_path = "%s?%s" % (url, urlencode(params or {}))
        url = self.base_url + url_path

        start = self.loop.time()
        response = None
        try:
            with async_timeout.timeout(timeout or self.timeout, loop=self.loop):
                response = yield from self.session.request(
                    method, url, data=body, headers=headers
                )
                raw_data = yield from response.text()
            duration = self.loop.time() - start

        except asyncio.CancelledError:
            raise

        except Exception as e:
            self.log_request_fail(
                method, url, url_path, body, self.loop.time() - start, exception=e
            )
            if isinstance(e, ServerFingerprintMismatch):
                raise SSLError("N/A", str(e), e)
            if isinstance(e, asyncio.TimeoutError):
                raise ConnectionTimeout("TIMEOUT", str(e), e)
            raise ConnectionError("N/A", str(e), e)

        finally:
            if response is not None:
                yield from response.release()

        # raise errors based on http status codes, let the client handle those if needed
        if not (200 <= response.status < 300) and response.status not in ignore:
            self.log_request_fail(
                method,
                url,
                url_path,
                body,
                duration,
                status_code=response.status,
                response=raw_data,
            )
            self._raise_error(response.status, raw_data)

        self.log_request_success(
            method, url, url_path, body, response.status, raw_data, duration
        )

        return response.status, response.headers, raw_data


class PatchedAsyncTransport(AsyncTransport):
    def __init__(
        self,
        hosts,
        connection_class=AIOHttpConnection,
        loop=None,
        connection_pool_class=AsyncConnectionPool,
        sniff_on_start=False,
        raise_on_sniff_error=True,
        **kwargs,
    ):
        super().__init__(
            hosts,
            connection_class=PatchedAIOHttpConnection,
            loop=loop,
            connectino_pool_class=connection_pool_class,
            sniff_on_start=sniff_on_start,
            raise_on_sniff_error=raise_on_sniff_error,
            **kwargs,
        )
