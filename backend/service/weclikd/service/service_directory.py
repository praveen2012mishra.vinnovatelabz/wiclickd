from weclikd.utils import async_utils
from weclikd.service.secrets import Secret
import googleapiclient.discovery
import os
import logging

_service_directory = None


class Endpoint:
    def __init__(
        self,
        service_name: str,
        default_user: str = None,
        default_ip: str = None,
        default_port: int = None,
        default_host: str = None,
        default_url: int = None,
        default_password: str = None,
        do_lookup: bool = True,
    ):
        self.service_name = service_name
        self._user = default_user
        self._ip = default_ip
        self._port = default_port
        self._host = default_host
        self._url = default_url
        self._password = default_password
        self.secret = Secret(service_name + "_PASSWORD")
        self.initialized = not do_lookup

    @property
    async def user(self):
        if os.environ.get(self.service_name + "_USER"):
            return os.environ.get(self.service_name + "_USER")
        await self._initialize()
        return self._user

    @property
    async def ip(self):
        if os.environ.get(self.service_name + "_IP"):
            return os.environ.get(self.service_name + "_IP")
        await self._initialize()
        return self._ip

    @property
    async def port(self):
        if os.environ.get(self.service_name + "_PORT"):
            return os.environ.get(self.service_name + "_PORT")
        await self._initialize()
        return self._port

    @property
    async def host(self):
        if os.environ.get(self.service_name + "_HOST"):
            return os.environ.get(self.service_name + "_HOST")
        await self._initialize()
        if not self._host:
            return f"{self.ip}:{self.port}"
        return self._host

    @property
    async def url(self):
        if os.environ.get(self.service_name + "_URL"):
            return os.environ.get(self.service_name + "_URL")
        await self._initialize()
        return self._url

    @property
    async def password(self):
        if os.environ.get(self.service_name + "_PASSWORD"):
            return os.environ.get(self.service_name + "_PASSWORD")

        from weclikd.utils import environment

        if environment.is_test_env():
            return self._password

        return await self.secret.retrieve_async() or self._password

    async def _initialize(self):
        if self.initialized:
            return

        from weclikd.utils import environment

        if environment.is_test_env():
            endpoint = self
        else:
            endpoint = await _get_service_endpoint(
                environment.PROJECT_ID, self.service_name
            )
        self._user = endpoint._user or self._user
        self._ip = endpoint._ip or self._ip
        self._port = endpoint._port or self._port
        self._url = endpoint._url or self._url
        self._host = endpoint._host or self._host
        self.initialized = True


@async_utils.wrap
def _get_service_endpoint(PROJECT_ID: str, service_name: str):
    _init_service_directory()
    endpoint_name = f"projects/{PROJECT_ID}/locations/us-central1/namespaces/weclikd/services/{service_name.lower()}/endpoints/master"
    logging.info(f"Querying Service Directory with endpoint={endpoint_name}")
    endpoint = (
        _service_directory.projects()
        .locations()
        .namespaces()
        .services()
        .endpoints()
        .get(name=endpoint_name)
    )
    output = endpoint.execute()
    logging.info(f"Endpoint Info: {output}")
    return Endpoint(
        service_name=service_name,
        default_user=output.get("metadata", {}).get("user"),
        default_ip=output["address"],
        default_port=output["port"],
        default_host=output.get("metadata", {}).get("host")
        or f"{output['address']}:{output['port']}",
        default_url=output.get("metadata", {}).get("url"),
    )


def _init_service_directory():
    global _service_directory
    if _service_directory:
        return

    _service_directory = googleapiclient.discovery.build(
        "servicedirectory", "v1beta1", cache_discovery=False
    )
