import logging
import time
import aiomysql
import getpass
import sqlalchemy
import base64
import json
from sqlalchemy.dialects.mysql import BIGINT, insert as mysql_insert
from databases import Database
from databases.backends.mysql import MySQLBackend, MySQLConnection
from weclikd.service.service_directory import Endpoint
from weclikd.utils import time_utils
from weclikd.common.filter_pb2 import DefaultFilter
from weclikd.common.sql_field_options_pb2 import exclude_from_sql, primary_key
from typing import Type, Dict, Any, List, Optional, Tuple, AsyncGenerator
from google.protobuf.message import Message
from google.protobuf.json_format import MessageToDict, ParseDict

metadata = sqlalchemy.MetaData()
UnsignedBigInt = BIGINT(unsigned=True)


sql_endpoint: Endpoint = Endpoint(
    "SQL",
    default_user="app",
    default_host="localhost:3306",
    default_password="abc12345",
)


async def start():
    await sql_instance.start()
    await sql_instance_bk.start()


async def stop():
    await sql_instance.stop()
    await sql_instance_bk.stop()


async def health_check():
    if not sql_instance.is_connected():
        logging.info("Attempting to reconnect to MySQL")
        await sql_instance.attempt_to_reconnect()
        if not sql_instance.is_connected():
            logging.error("MySQL Connection Failure")
            raise Exception("MySQL Connection Failure")
        logging.info("Successfully reconnected to MySQL")


class SQLInstance:
    def __init__(self):
        self.last_connection_attempt = 0
        self.db: Database = None

    async def start(self):
        if self.db is None or not self.db.is_connected:
            self.last_connection_attempt = time.time()
            logging.info("Connecting to database...")

            db_url = (
                f"mysql://{await sql_endpoint.user}:{await sql_endpoint.password}@"
                f"{await sql_endpoint.host}/"
                f"weclikd?charset=utf8mb4&binary_prefix=true"
            )
            self.db = Database(db_url, pool_recycle=60, min_size=5, max_size=100)
            self.db._backend = MySQLBackendWithUnixSocketPatch(
                self.db.url, **self.db.options
            )
            self.db.mysql_backend = True

            await self.db.connect()
            logging.info("Connected to database")

    def is_connected(self):
        return self.db is not None and self.db.is_connected

    async def stop(self):
        if self.db is None:
            return

        logging.info("Disconnecting from database")
        await self.db.disconnect()

    async def attempt_to_reconnect(self):
        if self.db is None or not self.db.is_connected:
            if time.time() - self.last_connection_attempt > 10:
                await self.start()

    async def execute(self, *args, **kwargs):
        return await self.db.execute(*args, **kwargs)

    async def execute_many(self, *args, **kwargs):
        return await self.db.execute_many(*args, **kwargs)

    async def fetch_one(self, *args, **kwargs):
        return await self.db.fetch_one(*args, **kwargs)

    async def fetch_all(self, *args, **kwargs):
        return await self.db.fetch_all(*args, **kwargs)


sql_instance = SQLInstance()
sql_instance_bk = SQLInstance()


async def get_sql_client() -> Database:
    await sql_instance.attempt_to_reconnect()
    return sql_instance.db


class SQLDb:
    def __init__(self, table, proto_clazz: Type[Message] = None, order_by=None):
        self.sql_instance = sql_instance
        self.table = table
        self.proto_clazz = proto_clazz
        self.table_columns = [col.name for col in table.c]
        self.order_by = order_by
        self.exclude_from_sql = []
        if proto_clazz:
            for field_descriptor in proto_clazz.DESCRIPTOR.fields:
                if field_descriptor.GetOptions().HasExtension(exclude_from_sql):
                    self.exclude_from_sql.append(field_descriptor.name)

    async def get(self, message: Optional[Message] = None, **kwargs) -> Message:
        await self.sql_instance.attempt_to_reconnect()
        row = await self.sql_instance.db.fetch_one(
            sqlalchemy.select(
                [self.table],
                sqlalchemy.and_(
                    *[
                        getattr(self.table.c, name) == value
                        for name, value in kwargs.items()
                    ]
                ),
            )
        )
        if not row:
            return None

        return self.to_proto(row, message)

    async def get_many(
        self,
        field_name: str = "id",
        values: List[Any] = None,
        messages: Optional[List[Message]] = None,
    ) -> Message:
        await self.sql_instance.attempt_to_reconnect()
        if not values:
            return []

        if not messages:
            messages = [None] * len(values)
        results = {value: msg for value, msg in zip(values, messages)}
        rows = await self.sql_instance.db.fetch_all(
            sqlalchemy.select(
                [self.table], whereclause=getattr(self.table.c, field_name).in_(values)
            )
        )
        if not rows:
            return []
        # order them
        for row in rows:
            results[getattr(row, field_name)] = self.to_proto(
                row, results[getattr(row, field_name)]
            )

        return results.values()

    async def query(self, **kwargs) -> Message:
        await self.sql_instance.attempt_to_reconnect()
        rows = await self.sql_instance.db.fetch_all(
            sqlalchemy.select(
                [self.table],
                sqlalchemy.and_(
                    *[
                        getattr(self.table.c, name) == value
                        for name, value in kwargs.items()
                    ]
                ),
            )
        )
        if not rows:
            return []

        return [self.to_proto(row) for row in rows]

    async def insert(self, message: Message = None, **kwargs) -> int:
        await self.sql_instance.attempt_to_reconnect()
        values = self.to_row(message)
        values.update(dict(kwargs))
        if values.get("id") == "0":
            del values["id"]

        row = await self.sql_instance.db.execute(
            sqlalchemy.insert(self.table, values=values)
        )
        return row.lastrowid

    async def upsert(self, msg: Message = None, **kwargs) -> bool:
        await self.sql_instance.attempt_to_reconnect()
        values = self.to_row(msg)
        values.update(kwargs)
        insert_stmt = (
            mysql_insert(self.table).values(**values).on_duplicate_key_update(values)
        )
        cursor = await self.sql_instance.db.execute(insert_stmt)
        if cursor.rowcount == 1:
            return True
        else:
            return False

    async def update(self, msg: Message = None, **kwargs):
        await self.sql_instance.attempt_to_reconnect()
        values = self.to_row(msg)
        for field, value in kwargs.items():
            if field in values:
                del values[field]
        await self.sql_instance.db.execute(
            sqlalchemy.update(
                self.table,
                whereclause=sqlalchemy.and_(
                    *[
                        getattr(self.table.c, name) == value
                        for name, value in kwargs.items()
                    ]
                ),
                values=values,
            )
        )

    async def delete(self, **kwargs) -> bool:
        await self.sql_instance.attempt_to_reconnect()
        count = await self.delete_many(**kwargs)
        if count:
            assert count == 1
            return True
        else:
            return False

    async def delete_many(self, **kwargs) -> int:
        await self.sql_instance.attempt_to_reconnect()
        cursor = await self.sql_instance.db.execute(
            sqlalchemy.delete(
                self.table,
                whereclause=sqlalchemy.and_(
                    *[
                        getattr(self.table.c, name) == value
                        for name, value in kwargs.items()
                    ]
                ),
            )
        )
        return cursor.rowcount

    async def increment(self, field_name: str, **kwargs):
        await self.sql_instance.attempt_to_reconnect()
        await self.sql_instance.db.execute(
            sqlalchemy.update(
                self.table,
                whereclause=sqlalchemy.and_(
                    *[
                        getattr(self.table.c, name) == value
                        for name, value in kwargs.items()
                    ]
                ),
                values={field_name: getattr(self.table.c, field_name) + 1},
            )
        )

    async def decrement(self, field_name: str, **kwargs):
        await self.sql_instance.attempt_to_reconnect()
        await self.sql_instance.db.execute(
            sqlalchemy.update(
                self.table,
                whereclause=sqlalchemy.and_(
                    *[
                        getattr(self.table.c, name) == value
                        for name, value in kwargs.items()
                    ]
                ),
                values={field_name: getattr(self.table.c, field_name) - 1},
            )
        )

    async def paginate(
        self, filter: DefaultFilter, descending: bool = True, order_by=None, **kwargs
    ) -> Tuple[List[Message], str]:
        await self.sql_instance.attempt_to_reconnect()
        order_by = getattr(self.table.c, order_by) if order_by else self.order_by
        if kwargs:
            if filter.cursor:
                last_id = int(base64.b64decode(filter.cursor).decode())
                whereclause = sqlalchemy.and_(
                    *[
                        getattr(self.table.c, name) == value
                        for name, value in kwargs.items()
                    ],
                    order_by < last_id if descending else order_by > last_id,
                )
            else:
                whereclause = sqlalchemy.and_(
                    *[
                        getattr(self.table.c, name) == value
                        for name, value in kwargs.items()
                    ]
                )
        else:
            if filter.cursor:
                last_id = int(base64.b64decode(filter.cursor).decode())
                whereclause = sqlalchemy.and_(
                    order_by < last_id if descending else order_by > last_id
                )
            else:
                whereclause = None

        order_by_clause = (
            sqlalchemy.desc(order_by) if descending else sqlalchemy.asc(order_by)
        )
        rows = await self.sql_instance.db.fetch_all(
            sqlalchemy.select([self.table], whereclause=whereclause)
            .limit(filter.num_items)
            .order_by(order_by_clause)
        )
        if not rows:
            return [], None

        return (
            [self.to_proto(row) for row in rows],
            base64.b64encode(str(getattr(rows[-1], order_by.name)).encode()).decode()
            if rows
            else None,
        )

    async def scan(self) -> AsyncGenerator[Message, None]:
        filter = DefaultFilter(num_items=25)
        while True:
            results, cursor = await self.paginate(filter=filter)
            for result in results:
                yield result
            if len(results) < 25:
                return

            filter = DefaultFilter(num_items=25, cursor=cursor)

    def to_row(self, msg: Message) -> Dict:
        if not msg:
            return {}

        message_dict = MessageToDict(
            msg, including_default_value_fields=True, preserving_proto_field_name=True
        )
        for field in self.exclude_from_sql:
            message_dict.pop(field, None)

        if "data" in self.table_columns:
            message_dict["data"] = {}
            to_remove = []
            for key, value in message_dict.items():
                if key not in self.table_columns:
                    message_dict["data"][key] = value
                    to_remove.append(key)
            for each in to_remove:
                del message_dict[each]

        if "created" in message_dict:
            message_dict["created"] = time_utils.from_iso_string(
                message_dict["created"]
            )

        return message_dict

    def to_proto(self, row: Dict, msg: Optional[Message] = None) -> Message:
        if not self.proto_clazz and not msg:
            return dict(row)

        proto_dict = dict(row)

        # Create model from sql database row
        if "data" in proto_dict:
            if isinstance(proto_dict["data"], str):
                proto_dict["data"] = json.loads(proto_dict["data"])
            for field, value in proto_dict.get("data").items():
                proto_dict[field] = value

        if "created" in proto_dict:
            proto_dict["created"] = time_utils.to_iso_string(proto_dict["created"])

        if not msg:
            msg = self.proto_clazz()
        return ParseDict(proto_dict, msg, ignore_unknown_fields=True)

    async def execute(self, *args, **kwargs):
        return await self.sql_instance.db.execute(*args, **kwargs)

    async def execute_many(self, *args, **kwargs):
        return await self.sql_instance.db.execute_many(*args, **kwargs)

    async def fetch_one(self, *args, **kwargs):
        return await self.sql_instance.db.fetch_one(*args, **kwargs)

    async def fetch_all(self, *args, **kwargs):
        return await self.sql_instance.db.fetch_all(*args, **kwargs)


class MySQLBackendWithUnixSocketPatch(MySQLBackend):

    # Monkey-patching Databases module so it can connect to msyql using unix-sockets
    async def connect(self) -> None:
        assert self._pool is None, "DatabaseBackend is already running"
        kwargs = self._get_connection_kwargs()
        if "unix_socket" in self._database_url.options:  # set unix socket option here
            kwargs["unix_socket"] = self._database_url.options["unix_socket"]

        self._pool = await aiomysql.create_pool(
            host=self._database_url.hostname
            or "localhost",  # host was set to null, but aiomysql wants localhost
            port=self._database_url.port or 3306,
            user=self._database_url.username or getpass.getuser(),
            password=self._database_url.password,
            db=self._database_url.database,
            autocommit=True,
            **kwargs,
        )

    def connection(self):
        return MySQLConnectionPatched(self, self._dialect)


class MySQLConnectionPatched(MySQLConnection):
    def __init__(self, database: MySQLBackend, dialect):
        self._database = database
        self._dialect = dialect
        self._connection = None  # type: typing.Optional[aiomysql.Connection]

    async def execute(self, query):
        assert self._connection is not None, "Connection is not acquired"
        query, args, context = self._compile(query)
        cursor = await self._connection.cursor()
        try:
            await cursor.execute(query, args)
            return cursor
        finally:
            await cursor.close()
