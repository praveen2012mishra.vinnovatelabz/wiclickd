from typing import Optional, Type, List, Dict, Callable
import base64
import logging
import json
import requests
from google.protobuf.message import Message
from google.auth.exceptions import GoogleAuthError
from google.auth.credentials import AnonymousCredentials
from google.api_core import exceptions
from google.auth.transport import requests as google_requests
from google.cloud import pubsub_v1
from google.oauth2 import id_token
from weclikd.utils import async_utils, environment
from weclikd.utils.session import Session
from weclikd.exceptions import AuthenticationError
from sanic.request import Request
from sanic import Blueprint, response

if environment.is_test_env():
    publisher = pubsub_v1.PublisherClient(credentials=AnonymousCredentials())
    subscriber = pubsub_v1.SubscriberClient(credentials=AnonymousCredentials())
else:
    publisher = pubsub_v1.PublisherClient()
    subscriber = pubsub_v1.SubscriberClient()


class Topic:
    def __init__(self, topic_name: str, event_pb2: Type[Message] = None):
        topics.append(self)
        self.name = topic_name
        self.event_pb2 = event_pb2
        self.json_format = event_pb2 is None
        self.topic_path = publisher.topic_path(environment.PROJECT_ID, topic_name)

    async def publish(
        self, event: Message, session: Optional[Session] = None, mock: bool = True
    ):
        if environment.is_test_env() or mock or environment.is_local_env():
            for subscription in subscriptions.get(self.name, []):
                if subscription.event_handler:
                    await subscription.event_handler(event, session or Session({}))
        else:
            await self._publish_async(event, session, mock)

    @async_utils.wrap
    def _publish_async(
        self, message: Message, session: Optional[Session] = None, mock: bool = True
    ):
        self.publish_sync(message, session, mock)

    def publish_sync(
        self, message: Message, session: Optional[Session] = None, mock: bool = False
    ):
        future = publisher.publish(
            self.topic_path,
            data=message.SerializeToString(),
            **(session.to_pubsub_attributes() if session else {}),
        )
        future.result()

    def create(self):
        try:
            return publisher.create_topic(self.topic_path)
        except exceptions.AlreadyExists:
            pass


class Subscription:
    def __init__(self, topic: Topic, component_name, event_handler=None):
        self.topic_name = topic.name
        self.component_name = component_name
        self.event_pb = topic.event_pb2
        self.topic_path = publisher.topic_path(environment.PROJECT_ID, self.topic_name)
        self.subscription_path = subscriber.subscription_path(
            environment.PROJECT_ID, component_name + "." + self.topic_name
        )
        self.event_handler = event_handler

        # add this subscription to the subscriptions map
        if self.topic_name not in subscriptions:
            subscriptions[self.topic_name] = []

        subscriptions[self.topic_name].append(self)

    def create(self, push_url: str = None, ack_deadline_seconds: int = 300):
        try:
            if push_url:
                push_config = pubsub_v1.types.PushConfig(
                    push_endpoint=f"{push_url}/{self.component_name}/{self.topic_name}?token={environment.PUBSUB_TOKEN}"
                )
                if not environment.is_test_env():
                    push_config.oidc_token.service_account_email = (
                        f"{environment.PROJECT_ID}@appspot.gserviceaccount.com"
                    )
                    push_config.oidc_token.audience = "weclikd.com"
            else:
                push_config = pubsub_v1.types.PushConfig()
            return subscriber.create_subscription(
                self.subscription_path,
                self.topic_path,
                push_config,
                ack_deadline_seconds=ack_deadline_seconds,
            )
        except exceptions.AlreadyExists:
            pass

    def delete(self):
        try:
            return subscriber.delete_subscription(self.subscription_path)
        except exceptions.NotFound:
            pass

    @staticmethod
    def push(url: str, message: Message = None, session: Optional[Session] = None):
        if not session:
            session = {}
        response = requests.post(
            url,
            json={
                "subscription": url.split("/")[-1],
                "message": {
                    "messageId": "1",
                    "data": base64.b64encode(message.SerializeToString()).decode()
                    if message
                    else "",
                    "attributes": {**session},
                },
            },
        )
        return response.status_code < 300


class PushHandler:
    def __init__(
        self,
        request: Request,
        proto_class: Optional[Type[Message]] = None,
        authenticate: bool = True,
    ):
        self.request: Request = request
        self.proto_class: Type[Message] = proto_class
        self.status_code: int = 200
        self._session: Session = Session({})
        self._event: Optional[Message] = None
        self.message_id: str = ""
        self.subscription: str = ""
        self.initialized: bool = False
        self.authenticate = authenticate

    async def __aenter__(self) -> "PushHandler":
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        if exc_type == AuthenticationError:
            self.status_code = 403
            logging.error("Authentication Error")
        elif exc_type:
            logging.error(
                f"Error when handling message from {self.subscription}",
                exc_info=(exc_type, exc_val, exc_tb),
            )
            self.status_code = 200
        return True

    @property
    def event(self):
        if not self.initialized:
            self._initialize()

        return self._event

    @property
    def session(self):
        if not self.initialized:
            self._initialize()

        return self._session

    def _verify(self):
        if not self.authenticate:
            return

        if (
            not self.initialized
            and not environment.is_test_env()
            and not environment.is_local_env()
        ):
            # Verify that the request originates from the application.
            if (
                "token" not in self.request.args
                or self.request.args["token"][0] != environment.PUBSUB_TOKEN
            ):
                raise AuthenticationError()

            # No need to verify authorization token. GCP does this for us

    def _initialize(self):
        self._verify()

        self.pubsub_json = self.request.json
        self.subscription = self.pubsub_json["subscription"]
        self.message_id = self.pubsub_json["message"]["messageId"]
        self._session = Session(self.pubsub_json["message"].get("attributes", {}))
        if self.proto_class:
            self._event = self.proto_class()
            self._event.MergeFromString(
                base64.b64decode(self.pubsub_json["message"]["data"])
            )
        else:
            self._event = json.loads(
                base64.b64decode(self.pubsub_json["message"]["data"]) or "{}"
            )


topics: List[Topic] = []
subscriptions: Dict[str, List[Subscription]] = {}


def add_push_route(
    blueprint: Blueprint,
    callback: Callable,
    subscription: Subscription,
    authenticate: bool = True,
):
    @blueprint.route("/" + subscription.topic_name, methods=["POST"])
    async def push_route(request):
        async with PushHandler(
            request, subscription.event_pb, authenticate=authenticate
        ) as handler:
            await callback(handler.event, handler.session)
        return response.text("Success", handler.status_code)

    subscription.event_handler = callback
