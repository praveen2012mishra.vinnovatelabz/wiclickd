import binascii
import io
import logging
from typing import Union
from weclikd.utils import weclikd_id, async_utils, environment
from weclikd import exceptions
from google.cloud import storage
from PIL import Image
from tempfile import TemporaryFile

if environment.is_test_env():
    client = {}
else:
    client = storage.Client(environment.PROJECT_ID)


def get_bucket(bucket_name: str, **kwargs):
    if environment.is_test_env():
        return InMemoryBucket(bucket_name, **kwargs)
    else:
        return Bucket(bucket_name, **kwargs)


class Bucket:
    def __init__(
        self,
        bucket_name: str,
        cache_control: str = "public, max-age=3600",
        overwrite=False,
    ):
        self.bucket = client.bucket(bucket_name)
        self.cache_control = cache_control
        self.overwrite = overwrite

    def write(
        self, key: str, raw_data: Union[bytes, str], content_type="text/plain"
    ) -> None:
        blob = self.bucket.blob(key)
        if not self.overwrite and blob.exists():
            return

        if self.cache_control:
            blob.cache_control = self.cache_control

        blob.upload_from_string(raw_data, content_type=content_type)

    def upload_from_file(self, key: str, file, content_type="text/plain") -> None:
        blob = self.bucket.blob(key)
        if not self.overwrite and blob.exists():
            return

        if self.cache_control:
            blob.cache_control = self.cache_control

        blob.upload_from_file(file, content_type=content_type)


class InMemoryBucket(Bucket):
    def __init__(
        self,
        bucket_name: str,
        cache_control: str = "public, max-age=3600",
        overwrite=False,
    ):
        if bucket_name not in client:
            client[bucket_name] = {}
        self.bucket = client[bucket_name]
        self.overwrite = overwrite

    def write(self, key: str, raw_data: Union[bytes, str], content_type="text/plain"):
        if not self.overwrite and key in self.bucket:
            return

        self.bucket[key] = raw_data

    def upload_from_file(self, key: str, file, content_type="text/plain"):
        pass


def is_png(first_16_bytes: bytes) -> bool:
    return first_16_bytes[0:8] == b"89504e47"


def is_jpg(first_16_bytes: bytes) -> bool:
    return first_16_bytes[0:6] == b"ffd8ff"


def is_ico(first_16_bytes: bytes):
    return first_16_bytes[0:8] == b"00000100"


def is_gif(first_16_bytes: bytes):
    return first_16_bytes[0:6] == b"474946"


def is_svg(first_16_bytes: bytes):
    return first_16_bytes[0:10] == b"3c3f786d6c" or first_16_bytes[0:8] == b"3c737667"


@async_utils.wrap
def add_picture_gcp(bucket, contents, content_type):
    header_byte = binascii.hexlify(contents[0:16]).lower()
    if (
        not is_jpg(header_byte)
        and not is_png(header_byte)
        and not is_ico(header_byte)
        and not is_gif(header_byte)
        and not is_svg(header_byte)
    ):
        logging.warning(
            f"Only accepts jpeg, ico, svg+xml, gif, and png file formats. Got {content_type}. Header={header_byte}"
        )
        return 0

    picture_id = weclikd_id.file_to_id(contents)
    filename = f"{picture_id}"

    if len(contents) >= 25000:
        with TemporaryFile() as f:
            try:
                image_stream = io.BytesIO(contents)
                image_file = Image.open(image_stream)
                MAX_SIZE = (600, 300)
                image_file.thumbnail(MAX_SIZE)
                image_file.save(f, image_file.format)
                f.seek(0)
            except Exception as ex:
                logging.exception(
                    f"Failed to Resize Image. content_type={content_type}"
                )
                return 0
            bucket.upload_from_file(filename, f, content_type=content_type)
    else:
        bucket.write(filename, contents, content_type=content_type)

    return str(picture_id)
