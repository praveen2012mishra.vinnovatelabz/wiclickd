import os
import logging
from weclikd.utils import async_utils
from google.cloud.secretmanager import SecretManagerServiceClient

CLIENT = None
MAX_RETRIES = 5


class Secret:
    def __init__(self, name: str):
        self.name: str = name
        self.value: str = None
        self.retry: int = 0

    async def __aenter__(self) -> str:
        return self.retrieve_async()

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        return self.__exit__(exc_type, exc_val, exc_tb)

    def __enter__(self) -> str:
        return self.retrieve()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            # something went wrong, try again
            self.retry += 1
            self.value = None
        else:
            # able to use secret successfully, reset retry
            self.retry = 0

    def retrieve(self):
        from weclikd.utils import environment

        if self.value:
            return self.value

        if os.environ.get(self.name):
            self.value = os.environ[self.name]
        else:
            if self.retry >= MAX_RETRIES:
                return self.value

            client = _get_client()
            response = client.access_secret_version(
                request={
                    "name": f"projects/{environment.PROJECT_ID}/secrets/{self.name}/versions/latest"
                }
            )

            self.value = response.payload.data.decode()
        return self.value

    @async_utils.wrap
    def retrieve_async(self):
        logging.info(f"Retrieving secret for {self.name}")
        return self.retrieve()


def _get_client():
    global CLIENT
    if CLIENT is None:
        CLIENT = SecretManagerServiceClient()
    return CLIENT
