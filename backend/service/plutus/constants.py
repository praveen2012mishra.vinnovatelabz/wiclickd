from weclikd.service.secrets import Secret
from dataclasses import dataclass


@dataclass
class StripeConstants:
    _stripe_secret: Secret = Secret("STRIPE_SECRET_KEY")

    STRIPE_API_VERSION: str = "2020-03-02"
    STRIPE_PUBLISHABLE_KEY: str = "pk_test_hFxQCiSLrnSGOUsxfRi9Phxc0031td3fGw"
    STRIPE_WEBHOOK_SECRET: str = "whsec_xDVVLZM46BUAzeNuzwFhgA4NnUweKC6j"
    SUBSCRIPTION_PLAN_ID_MONTHLY: str = "plan_GzWcbOrVhsllsy"
    SUBSCRIPTION_PLAN_ID_YEARLY: str = "plan_H9dKvHYCZiKF87"
    STRIPE_CONNECT_AUTH_URI: str = "https://connect.stripe.com/express/oauth/authorize"
    STRIPE_CONNECT_ACCOUNT_ID: str = "ca_GymB7nh5pfkeu0nBcdJmZ7ROuRGa3MgI"

    @property
    def STRIPE_SECRET_KEY(self) -> str:
        return self._stripe_secret.retrieve()


stripe_constants = StripeConstants()
