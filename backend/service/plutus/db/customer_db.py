from plutus.model.customer_pb2 import Customer, CustomerOrder
from plutus.model.payout_info_pb2 import PayoutInfo
from plutus.db.sql_tables import CustomerOrderTable, CustomerTable, PayoutTable
from weclikd.service.sql import SQLDb

customer_table = SQLDb(table=CustomerTable, proto_clazz=Customer)
order_table = SQLDb(table=CustomerOrderTable, proto_clazz=CustomerOrder)
payout_table = SQLDb(table=PayoutTable, proto_clazz=PayoutInfo)


async def get(account_id: int) -> Customer:
    return await customer_table.get(account_id=account_id)


async def add(customer: Customer):
    return await customer_table.insert(customer, current_order_id=None)


async def add_order(order: CustomerOrder):
    return await order_table.insert(order)


async def get_order(id: int):
    return await order_table.get(id=id)


async def cancel_order(order: CustomerOrder):
    return await order_table.insert(order)


async def update(customer: Customer):
    if customer.current_order_id:
        return await customer_table.update(customer, account_id=customer.account_id)
    else:
        return await customer_table.upsert(customer, current_order_id=None)
