from sqlalchemy import (
    Table,
    Column,
    Integer,
    FLOAT,
    SMALLINT,
    TEXT,
    VARCHAR,
    CHAR,
    text as sqltext,
)
from sqlalchemy.dialects.mysql import TIMESTAMP, BIGINT
from weclikd.service.sql import UnsignedBigInt, metadata


LikeTable = Table(
    "like",
    metadata,
    Column("account_id", UnsignedBigInt, nullable=False, primary_key=True),
    Column("content_id", UnsignedBigInt, nullable=False, primary_key=True),
    Column("like_type", SMALLINT, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

LikesReceivedTable = Table(
    "likes_received",
    metadata,
    Column("account_id", UnsignedBigInt, primary_key=True),
    Column("pay_period", TIMESTAMP, primary_key=True),
    Column("likes_0", Integer, nullable=False, default=0),
    Column("likes_1", Integer, nullable=False, default=0),
    Column("likes_2", Integer, nullable=False, default=0),
    Column("likes_3", Integer, nullable=False, default=0),
)

CustomerTable = Table(
    "stripe_customer",
    metadata,
    Column("account_id", UnsignedBigInt, primary_key=True),
    Column("customer_id", TEXT, nullable=False),
    Column("current_order_id", UnsignedBigInt, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)


CustomerOrderTable = Table(
    "stripe_order",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True),
    Column("account_id", UnsignedBigInt, nullable=False),
    Column("subscription_id", TEXT, nullable=False),
    Column("subscription_type", SMALLINT, nullable=False),
    Column("cancel", SMALLINT, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

PayoutTable = Table(
    "stripe_payout",
    metadata,
    Column("account_id", UnsignedBigInt, primary_key=True),
    Column("stripe_state", VARCHAR(32), nullable=False),
    Column("stripe_user_id", TEXT, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)
