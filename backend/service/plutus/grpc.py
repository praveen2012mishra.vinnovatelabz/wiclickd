import logging
from grpc import ServicerContext, StatusCode
from plutus.api.plutus_pb2_grpc import PlutusServicer
from plutus.api.plutus_pb2 import (
    GetAccountBalanceRequest,
    GetAccountBalanceResponse,
    PaySubscriptionRequest,
    PaySubscriptionResponse,
    CancelSubscriptionRequest,
    CancelSubscriptionResponse,
    GetPaymentInfoRequest,
    GetPaymentInfoResponse,
    ChangePaymentInfoRequest,
    ChangePaymentInfoResponse,
    GetPayoutInfoResponse,
    GetLikesReportRequest,
    GetLikesReportResponse,
)
from plutus.api.money_pb2 import Balance
from weclikd.utils.session import Session
from weclikd.common.subscription_pb2 import Subscription
from weclikd.service.transaction import Transaction
from plutus.model import customer_model, payout_model, model_like
from datetime import datetime


class Plutus(PlutusServicer):
    async def PaySubscription(
        self, request: PaySubscriptionRequest, context: ServicerContext
    ) -> PaySubscriptionResponse:
        logging.info(f"PaySubscription: {request}")
        session = Session.create_from_grpc(context)

        async with Transaction():
            customer = await customer_model.get(session.account_id)
            assert request.type == Subscription.SubscriptionType.GOLD
            if not customer:
                customer = await customer_model.create(
                    session.account_id, request.email, request.payment_id
                )

            order = await customer_model.get_current_order(customer)
            if order and order.subscription_type == Subscription.SubscriptionType.GOLD:
                return PaySubscriptionResponse()

            await customer_model.add_subscription(
                customer, Subscription.SubscriptionType.GOLD, request.pricing
            )
        return PaySubscriptionResponse()

    async def CancelSubscription(
        self, request: CancelSubscriptionRequest, context: ServicerContext
    ) -> CancelSubscriptionResponse:
        logging.info(f"CancelSubscription: {request}")
        session = Session.create_from_grpc(context)

        async with Transaction():
            customer = await customer_model.get(session.account_id)
            if not customer:
                context.set_code(StatusCode.NOT_FOUND)
                context.set_details(f"Do not have a subscription")
                return CancelSubscriptionResponse()

            order = await customer_model.get_current_order(customer)
            if not order:
                return CancelSubscriptionResponse()
            if order.subscription_type == Subscription.SubscriptionType.BASIC:
                return CancelSubscriptionResponse()

            await customer_model.cancel_subscription(customer, order)

        return CancelSubscriptionResponse()

    async def GetPaymentInfo(
        self, request: GetPaymentInfoRequest, context: ServicerContext
    ) -> GetPaymentInfoResponse:
        logging.info(f"GetPaymentInfo: {request}")
        session = Session.create_from_grpc(context)
        customer = await customer_model.get(session.account_id)
        if not customer:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f"Do not have a subscription")
            return GetPaymentInfoResponse()
        return await customer_model.get_default_payment_info(customer)

    async def GetPayoutInfo(
        self, request: GetPaymentInfoRequest, context: ServicerContext
    ) -> GetPaymentInfoResponse:
        logging.info(f"GetPayoutInfo: {request}")
        session = Session.create_from_grpc(context)
        payout_info = await payout_model.get_payout_info(session.account_id)
        return GetPayoutInfoResponse(
            has_stripe_account=payout_info is not None
            and payout_info.stripe_user_id != ""
        )

    async def ChangePaymentInfo(
        self, request: ChangePaymentInfoRequest, context: ServicerContext
    ) -> ChangePaymentInfoResponse:
        logging.info(f"ChangePaymentInfo: {request}")
        session = Session.create_from_grpc(context)
        customer = await customer_model.get(session.account_id)
        if not customer:
            await customer_model.create(
                session.account_id, request.email, request.payment_id
            )
        else:
            await customer_model.update_payment(
                customer, request.payment_id, request.email
            )
        return ChangePaymentInfoResponse()

    async def GetAccountBalance(
        self,
        request: GetAccountBalanceRequest,
        context: ServicerContext,
    ) -> GetAccountBalanceResponse:
        logging.info(f"Processing GetAccountBalance:\n{request}")
        now = datetime.now()
        pay_period = datetime(
            year=now.year, month=now.month, day=1, hour=0, minute=0, second=0
        )
        likes = await model_like.get_monthly_total(
            account_id=context.account_id, pay_period=pay_period
        )
        return GetAccountBalanceResponse(balance=Balance(likes_monthly_total=likes))

    async def GetLikesReport(
        self, request: GetLikesReportRequest, context: ServicerContext
    ) -> GetLikesReportResponse:
        logging.info(f"Processing GetLikesReport:\n{request}")
        return GetLikesReportResponse()
