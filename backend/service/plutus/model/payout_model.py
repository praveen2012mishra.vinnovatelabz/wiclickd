from typing import Dict, Tuple
import uuid
from plutus.constants import stripe_constants
from plutus.model.payout_info_pb2 import PayoutInfo
from plutus.db.customer_db import payout_table
from plutus import stripe
from weclikd.utils import environment
import urllib


async def get_payout_info(account_id: int) -> PayoutInfo:
    return await payout_table.get(account_id=account_id)


async def get_stripe_authorize_uri(account_id: int, redirect_url: str) -> str:
    random_state = uuid.uuid4().hex
    parameters = {
        "client_id": stripe_constants.STRIPE_CONNECT_ACCOUNT_ID,
        "state": random_state,
        "redirect_uri": redirect_url,
        "stripe_user[business_type]": "individual",
        "stripe_user[country]": "US",
    }

    payout_info = PayoutInfo(
        account_id=account_id,
        stripe_state=random_state,
    )
    await payout_table.upsert(payout_info)

    # other fields that can be prepopulated in the express onboarding flow
    #
    # 'stripe_user[business_name]': req.user.businessName || undefined,
    # 'stripe_user[first_name]': req.user.firstName || undefined,
    # 'stripe_user[last_name]': req.user.lastName || undefined,
    # 'stripe_user[email]': req.user.email || undefined,
    # // 'suggested_capabilities[]': 'card_payments',
    # // 'stripe_user[street_address]': req.user.address || undefined,
    # // 'stripe_user[city]': req.user.city || undefined,
    # // 'stripe_user[zip]': req.user.postalCode || undefined,
    # // 'stripe_user[state]': req.user.city || undefined,
    return f"{stripe_constants.STRIPE_CONNECT_AUTH_URI}?{urllib.parse.urlencode(parameters)}"


async def verify_stripe_oauth_token(
    account_id: int, state: str, code: str
) -> Tuple[Dict, int]:
    payout_info = await payout_table.get(account_id=account_id)
    if payout_info.stripe_state != state:
        return {"error": "Incorrect state parameter: " + state}, 403

    # Send the authorization code to Stripe's API.
    stripe_response = await stripe.get_oauth_token(code)
    if "error" in stripe_response:
        return stripe_response, 400

    stripe_user_id = stripe_response["stripe_user_id"]
    payout_info.stripe_state = ""
    payout_info.stripe_user_id = stripe_user_id
    await payout_table.update(payout_info, account_id=account_id)

    return {"success": True}, 200


async def get_stripe_dashboard_url(account_id: int) -> str:
    payout_info = await payout_table.get(account_id=account_id)
    return await stripe.create_login_link(
        stripe_user_id=payout_info.stripe_user_id,
        redirect_url=environment.PUBLIC_DOMAIN + "/settings",
    )
