from datetime import datetime, timedelta
import sqlalchemy
from sqlalchemy.dialects.mysql import insert as mysql_insert
from atlas.api.like_pb2 import Likes
from weclikd.service import sql

from plutus.db.sql_tables import LikesReceivedTable


async def insert(account_id: int, like_type: int, pay_period: datetime):
    values = {
        "account_id": account_id,
        "pay_period": pay_period,
        "likes_0": 0,
        "likes_1": 0,
        "likes_2": 0,
        "likes_3": 0,
    }
    on_update = {
        "account_id": account_id,
        "pay_period": pay_period,
    }
    if like_type == Likes.RED:
        values["likes_0"] = 1
        on_update["likes_0"] = LikesReceivedTable.c.likes_0 + 1
    elif like_type == Likes.SILVER:
        values["likes_1"] = 1
        on_update["likes_1"] = LikesReceivedTable.c.likes_1 + 1
    elif like_type == Likes.GOLD:
        values["likes_2"] = 1
        on_update["likes_2"] = LikesReceivedTable.c.likes_2 + 1
    else:
        values["likes_3"] = 1
        on_update["likes_3"] = LikesReceivedTable.c.likes_3 + 1

    await sql.sql_instance.execute(
        mysql_insert(LikesReceivedTable)
        .values(values)
        .on_duplicate_key_update(**on_update)
    )


async def remove(account_id: int, like_type: int, pay_period: datetime):
    on_update = {
        "account_id": account_id,
        "pay_period": pay_period,
    }
    if like_type == Likes.RED:
        on_update["likes_0"] = LikesReceivedTable.c.likes_0 - 1
    elif like_type == Likes.SILVER:
        on_update["likes_1"] = LikesReceivedTable.c.likes_1 - 1
    elif like_type == Likes.GOLD:
        on_update["likes_2"] = LikesReceivedTable.c.likes_2 - 1
    else:
        on_update["likes_3"] = LikesReceivedTable.c.likes_3 - 1
    await sql.sql_instance.execute(
        sqlalchemy.update(
            LikesReceivedTable,
            whereclause=sqlalchemy.and_(
                LikesReceivedTable.c.account_id == account_id,
                LikesReceivedTable.c.pay_period == pay_period,
            ),
            values=on_update,
        )
    )


async def get_monthly_total(account_id: int, pay_period: datetime):
    row = await sql.sql_instance.fetch_one(
        sqlalchemy.select(
            [LikesReceivedTable],
            whereclause=sqlalchemy.and_(
                LikesReceivedTable.c.account_id == account_id,
                LikesReceivedTable.c.pay_period == pay_period,
            ),
        )
    )
    if not row:
        return Likes()

    return Likes(
        red=row.likes_0,
        silver=row.likes_1,
        gold=row.likes_2,
        diamond=row.likes_3,
    )


async def get_report(
    account_id: int, pay_period_start: datetime, pay_period_end: datetime
):
    rows = await sql.sql_instance.fetch_all(
        sqlalchemy.select(
            [LikesReceivedTable],
            whereclause=sqlalchemy.and_(
                LikesReceivedTable.c.account_id == account_id,
                sqlalchemy.and_(
                    LikesReceivedTable.c.pay_period >= pay_period_start,
                    LikesReceivedTable.c.pay_period <= pay_period_end,
                ),
            ),
            order_by=LikesReceivedTable.c.pay_period.asc(),
        )
    )
    report = []
    previous_pay_period = pay_period_start
    for row in rows:
        while True:
            if row.pay_period - previous_pay_period > timedelta(months=1):
                previous_pay_period += timedelta(months=1)
                report.append(Likes(pay_period=previous_pay_period))
            else:
                break

        report.append(
            Likes(
                pay_period=row.pay_period,
                red=row.likes_0,
                silver=row.likes_1,
                gold=row.likes_2,
                diamond=row.likes_3,
            )
        )
        previous_pay_period = row.pay_period
    return report
