from typing import Optional
from weclikd.common.subscription_pb2 import Subscription
from plutus.api.plutus_pb2 import GetPaymentInfoResponse, PaySubscriptionRequest
from plutus.db import customer_db
from plutus.model.customer_pb2 import Customer, CustomerOrder
from plutus import stripe


async def get(account_id: int) -> Optional[Customer]:
    return await customer_db.get(account_id=account_id)


async def get_current_order(customer: Customer) -> Optional[CustomerOrder]:
    if customer.current_order_id:
        return await customer_db.get_order(customer.current_order_id)
    return None


async def create(account_id: int, email: str, payment_id: str) -> Customer:
    stripe_customer = await stripe.create_customer(payment_id=payment_id, email=email)
    customer = Customer(
        account_id=account_id,
        customer_id=stripe_customer.id,
    )
    await customer_db.add(customer)
    return customer


async def add_subscription(
    customer: Customer,
    subscription_type: Subscription.SubscriptionType,
    pricing: PaySubscriptionRequest.PricingIntervalType,
):
    subscription = await stripe.create_subscription(
        customer.customer_id, subscription_type, pricing
    )
    order = CustomerOrder(
        account_id=customer.account_id,
        subscription_id=subscription.id,
        subscription_type=subscription_type,
        cancel=0,
    )
    order_id = await customer_db.add_order(order)
    customer.current_order_id = order_id
    await customer_db.update(customer)


async def cancel_subscription(customer: Customer, order: CustomerOrder):
    await stripe.cancel_subscription(order.subscription_id)
    order = CustomerOrder(
        account_id=order.account_id,
        subscription_id=order.subscription_id,
        subscription_type=order.subscription_type,
        cancel=1,
    )
    await customer_db.add_order(order)
    customer.current_order_id = 0
    await customer_db.update(customer)


async def update_payment(customer: Customer, payment_id: str, email: str):
    await stripe.update_payment(customer.customer_id, payment_id, email)


async def get_default_payment_info(customer: Customer):
    stripe_customer = await stripe.get_customer(customer.customer_id)
    if not stripe_customer["invoice_settings"].get("default_payment_method"):
        return GetPaymentInfoResponse(
            status=GetPaymentInfoResponse.Status.NO_PAYMENT_INFO
        )

    payment_info = await stripe.get_payment_info(
        stripe_customer["invoice_settings"]["default_payment_method"]
    )
    if payment_info["type"] != "card":
        return GetPaymentInfoResponse(
            status=GetPaymentInfoResponse.Status.NO_PAYMENT_INFO
        )

    return GetPaymentInfoResponse(
        card=GetPaymentInfoResponse.CreditCard(
            last4=payment_info["card"]["last4"],
            brand=payment_info["card"]["brand"],
            exp_month=payment_info["card"]["exp_month"],
            exp_year=payment_info["card"]["exp_year"],
            email=payment_info["billing_details"]["email"] or stripe_customer["email"],
        )
    )
