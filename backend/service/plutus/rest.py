from sanic import Blueprint, response
from plutus.constants import stripe_constants
from plutus.model import payout_model
from weclikd.utils.authorization import verify_user_token_args
from urllib.parse import urlparse

import logging
from datetime import datetime
from atlas.event.content_pb2 import LikeContentEvent
from weclikd.utils.session import Session
from weclikd.service import pubsub
from atlas.api.content_info_pb2 import ContentInfo

from plutus.event import subscription
from plutus.model import model_like

plutus_api = Blueprint("plutus", url_prefix="/plutus")


@plutus_api.route("/stripe-public-key", methods=["GET"])
async def push_route(request):
    return response.json({"publicKey": stripe_constants.STRIPE_PUBLISHABLE_KEY})


@plutus_api.route("/payout/authorize", methods=["GET"])
async def authorize_route(request):
    account_id, _ = verify_user_token_args(request)
    parsed = urlparse(request.url)
    redirect_url = f"{parsed.scheme}://{parsed.netloc}/payout/token"
    stripe_auth_url = await payout_model.get_stripe_authorize_uri(
        account_id, redirect_url=redirect_url
    )
    return response.redirect(stripe_auth_url)


@plutus_api.route("/payout/token", methods=["GET"])
async def token_route(request):
    account_id, _ = verify_user_token_args(request)
    stripe_response, http_code = await payout_model.verify_stripe_oauth_token(
        account_id=account_id,
        state=request.args.get("state"),
        code=request.args.get("code"),
    )
    return response.json(stripe_response, status=http_code)


@plutus_api.route("/payout/dashboard", methods=["GET"])
async def dashboard_request(request):
    account_id, _ = verify_user_token_args(request)
    stripe_dashboard_url = await payout_model.get_stripe_dashboard_url(account_id)
    return response.redirect(stripe_dashboard_url)


@plutus_api.route("/payout/transfer", methods=["GET"])
async def payout_customer(request):
    pass
    """
    verify_platform_token(request)
    await payout_model.transfer(
        account_id=request.json['account_id'],
        amount=request.json['amount'],
    )
    """


async def process_like_content(event: LikeContentEvent, session: Session):
    logging.info(f"Processing LikeContentEvent: {event}")
    content_type = (
        event.post.content_info.type
        if event.post.content_info.id
        else event.comment.content_info.type
    )
    user_id = (
        event.post.content_info.user_id
        if event.post.content_info.id
        else event.comment.content_info.user_id
    )
    created = event.created.ToDatetime()
    previous_created = event.prev_created.ToDatetime()
    pay_period = datetime(
        year=created.year,
        month=created.month,
        day=1,
        hour=0,
        minute=0,
        second=0,
        microsecond=0,
    )

    if (content_type == ContentInfo.POST and event.post.url) or not user_id:
        # Post with URL's are not monetized currently
        return

    if not event.type:
        if (
            created.year == previous_created.year
            and created.month == previous_created.month
        ):
            await model_like.remove(
                account_id=user_id,
                like_type=event.prev_like_type,
                pay_period=pay_period,
            )
        return

    if event.prev_like_type:
        if (
            created.year == previous_created.year
            and created.month == previous_created.month
        ):
            await model_like.remove(
                account_id=user_id,
                like_type=event.prev_like_type,
                pay_period=pay_period,
            )
            pass

    await model_like.insert(
        account_id=user_id, like_type=event.type, pay_period=pay_period
    )

    # await db.execute(LikesReceivedModel.insert_stmt(session.account_id))


pubsub.add_push_route(
    blueprint=plutus_api,
    callback=process_like_content,
    subscription=subscription.like_content_subscription,
    authenticate=True,
)
