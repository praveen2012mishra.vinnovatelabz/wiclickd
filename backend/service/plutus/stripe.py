from weclikd.utils import async_utils
from weclikd.common.subscription_pb2 import Subscription
from plutus.api.plutus_pb2 import PaySubscriptionRequest
from plutus.constants import stripe_constants
import stripe


@async_utils.wrap
def create_customer(payment_id: str, email: str) -> stripe.Customer:
    stripe.api_key = stripe_constants.STRIPE_SECRET_KEY
    return stripe.Customer.create(
        payment_method=payment_id,
        email=email,
        invoice_settings={"default_payment_method": payment_id},
    )


@async_utils.wrap
def get_customer(customer_id: str) -> stripe.Customer:
    stripe.api_key = stripe_constants.STRIPE_SECRET_KEY
    return stripe.Customer.retrieve(customer_id)


@async_utils.wrap
def get_payment_info(payment_id: str) -> stripe.PaymentMethod:
    stripe.api_key = stripe_constants.STRIPE_SECRET_KEY
    return stripe.PaymentMethod.retrieve(payment_id)


@async_utils.wrap
def create_subscription(
    customer_id: str,
    subscription_type: Subscription.SubscriptionType,
    pricing: PaySubscriptionRequest.PricingIntervalType,
):
    stripe.api_key = stripe_constants.STRIPE_SECRET_KEY
    if pricing == PaySubscriptionRequest.PricingIntervalType.YEARLY:
        plan_id = stripe_constants.SUBSCRIPTION_PLAN_ID_YEARLY
    else:
        plan_id = stripe_constants.SUBSCRIPTION_PLAN_ID_MONTHLY
    return stripe.Subscription.create(
        customer=customer_id,
        items=[
            {
                "plan": plan_id,
            },
        ],
        expand=["latest_invoice.payment_intent"],
    )


@async_utils.wrap
def cancel_subscription(subscription_id: str):
    stripe.api_key = stripe_constants.STRIPE_SECRET_KEY
    return stripe.Subscription.modify(subscription_id, cancel_at_period_end=True)


@async_utils.wrap
def update_payment(customer_id: str, payment_id: str, email: str):
    stripe.api_key = stripe_constants.STRIPE_SECRET_KEY
    stripe.PaymentMethod.attach(
        payment_id,
        customer=customer_id,
    )
    stripe.Customer.modify(
        customer_id,
        email=email if email else None,
        invoice_settings={"default_payment_method": payment_id},
    )


@async_utils.wrap
def get_oauth_token(code: str):
    stripe.api_key = stripe_constants.STRIPE_SECRET_KEY
    try:
        response = stripe.OAuth.token(grant_type="authorization_code", code=code)
    except stripe.oauth_error.OAuthError as e:
        return {"error": "Invalid authorization code: " + code}
    return response


@async_utils.wrap
def create_login_link(stripe_user_id: str, redirect_url: str) -> str:
    stripe.api_key = stripe_constants.STRIPE_SECRET_KEY
    response = stripe.Account.create_login_link(
        stripe_user_id, redirect_url=redirect_url
    )
    return response["url"] + "#/account"
