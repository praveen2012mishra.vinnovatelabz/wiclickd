from weclikd.service import pubsub
import atlas.event.topic as atlas_topic
import eros.event.topic as eros_topic


create_account_subscription = pubsub.Subscription(
    eros_topic.create_acccount_topic, "plutus"
)
create_comment_subscription = pubsub.Subscription(
    atlas_topic.create_comment_topic, "plutus"
)
create_post_subscription = pubsub.Subscription(atlas_topic.create_post_topic, "plutus")
like_content_subscription = pubsub.Subscription(
    atlas_topic.like_content_topic, "plutus"
)
