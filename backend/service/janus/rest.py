from sanic import Blueprint
from weclikd.service import pubsub
from weclikd.utils.session import Session
from weclikd.service import grpc
from atlas.api.atlas_pb2 import CreatePostRequest
from atlas.event.content_pb2 import CreateExternalPostEvent
from janus.graphql.content import CreatePost
from janus.es import es
from janus.event import subscription

janus_api = Blueprint("janus", url_prefix="/janus")


async def create_external_posts(event: CreateExternalPostEvent, session: Session):
    for external_post in event.posts:
        (
            validated_topics,
            topic_ids,
            thumbnail_picture_id,
        ) = await CreatePost.validate_topics(external_post.topics, session)
        await grpc.atlas.CreatePost(
            request=CreatePostRequest(
                title=external_post.title,
                summary=external_post.summary,
                url=external_post.url,
                topics=validated_topics,
                topic_ids=topic_ids,
                thumbnail_picture_id=external_post.thumbnail_picture_id
                or thumbnail_picture_id,
                feed_id=external_post.feed_id,
                icon_url=external_post.icon_url,
                other_urls=[external_post.unnormalized_url] if external_post.unnormalized_url else [],
            ),
            context=session,
        )


# external feed
pubsub.add_push_route(
    blueprint=janus_api,
    callback=create_external_posts,
    subscription=subscription.create_external_post_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.index_feed,
    subscription=subscription.create_external_feed_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.index_feed,
    subscription=subscription.update_external_feed_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.delete_feed,
    subscription=subscription.delete_external_feed_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.increment_external_feed_followers,
    subscription=subscription.follow_external_feed_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.decrement_external_feed_followers,
    subscription=subscription.unfollow_external_feed_subscription,
)

# clik
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.index_clik,
    subscription=subscription.create_clik_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.update_clik,
    subscription=subscription.update_clik_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.delete_clik,
    subscription=subscription.delete_clik_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.increment_clik_followers,
    subscription=subscription.follow_clik_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.decrement_clik_followers,
    subscription=subscription.unfollow_clik_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.increment_clik_members,
    subscription=subscription.create_clik_member_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.decrement_clik_members,
    subscription=subscription.delete_clik_member_subscription,
)

# post
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.index_post,
    subscription=subscription.create_post_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.update_post,
    subscription=subscription.update_post_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.delete_post,
    subscription=subscription.delete_post_subscription,
)

# topic
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.index_topic,
    subscription=subscription.create_topic_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.update_topic,
    subscription=subscription.update_topic_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.delete_topic,
    subscription=subscription.delete_topic_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.increment_topic_followers,
    subscription=subscription.follow_topic_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.decrement_topic_followers,
    subscription=subscription.unfollow_topic_subscription,
)

# user
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.index_user,
    subscription=subscription.create_user_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.update_user,
    subscription=subscription.update_user_subscription,
)
pubsub.add_push_route(
    blueprint=janus_api,
    callback=es.delete_user,
    subscription=subscription.delete_user_subscription,
)
