from sanic import response
from atlas.api.comment_pb2 import DiscussionFilter, Discussion
from weclikd.service import grpc
from atlas.api.atlas_pb2 import GetDiscussionRequest, GetPostFromUrlRequest
from eros.api.eros_pb2 import GetProfilesRequest
from weclikd.utils import weclikd_id, time_utils
from weclikd.utils.compression import compress
from janus.graphql.auth_middleware import optional_user_auth, setup_request_auth
from janus.graphql.score_calculator import (
    get_comments_score,
    get_likes_score,
    get_reports_score,
)
from janus.utils import normalize_url
from sanic import Blueprint
import logging

comments = Blueprint("comments", url_prefix="/comments")


class MockGraphQLContext:
    def __init__(self, request):
        self.context = {"request": request}


def user_like_type_to_str(int_value):
    if int_value == 0:
        return None
    elif int_value == 1:
        return "RED"
    elif int_value == 2:
        return "SILVER"
    elif int_value == 3:
        return "GOLD"
    elif int_value == 4:
        return "DIAMOND"
    return None


def populate_user_id_to_comments_and_structure(
    response: Discussion, user_id_to_comments, session
):
    graphql_response = {"edges": []}
    for i, comment in enumerate(response.comments):
        graphql_comment = {
            "node": {
                "id": f"Comment:{comment.content_info.id}",
                "author": {
                    "id": f"User:{comment.content_info.user_id}",
                },
                "text": comment.content_info.text,
                "clik": comment.clik if comment.clik else None,
                "created": time_utils.from_unix_to_iso(
                    comment.content_info.created.seconds
                ),
                "likes_score": get_likes_score(
                    grpc.to_dict(comment.content_info.likes, {})
                ),
                "comments_score": get_comments_score(comment.content_info.num_comments),
                "reports_score": get_reports_score(comment.content_info.num_reports),
                "user_like_type": user_like_type_to_str(
                    comment.content_info.user_like_type
                ),
            }
        }
        if session.is_admin:
            graphql_comment["node"]["admin_stats"] = {
                "likes": {
                    "red": comment.content_info.likes.red,
                    "silver": comment.content_info.likes.silver,
                    "gold": comment.content_info.likes.gold,
                    "diamond": comment.content_info.likes.diamond,
                },
                "num_likes": comment.content_info.likes.red
                + comment.content_info.likes.silver
                + comment.content_info.likes.gold
                + comment.content_info.likes.diamond,
                "num_reports": comment.content_info.num_reports,
                "num_comments": comment.content_info.num_comments,
            }
        if comment.content_info.user_id not in user_id_to_comments:
            user_id_to_comments[comment.content_info.user_id] = [
                graphql_comment["node"]["author"]
            ]
        else:
            user_id_to_comments[comment.content_info.user_id].append(
                graphql_comment["node"]["author"]
            )

        graphql_response["edges"].append(graphql_comment)
        graphql_comment["comments"] = populate_user_id_to_comments_and_structure(
            comment.discussion, user_id_to_comments, session
        )

    return graphql_response


async def resolve_users(user_id_to_comments, session):
    graphql_response = await grpc.eros.GetProfiles(
        request=GetProfilesRequest(user_ids=user_id_to_comments.keys()), context=session
    )
    for profile in graphql_response.profiles:
        for comments_author in user_id_to_comments[profile.id]:
            comments_author["username"] = profile.username
            comments_author["profile_pic"] = normalize_url(str(profile.profile_picture_id))


@optional_user_auth
async def resolve_comments(
    root, info, session, post_id=0, clik_ids=None, sort="NEW", url=None
):
    if url:
        url_response = await grpc.atlas.GetPostFromUrl(
            request=GetPostFromUrlRequest(url=url), context=session
        )
        post_id = url_response.post.content_info.id
        if not post_id:
            return {"data": None}

    graphql_response = await grpc.atlas.GetDiscussion(
        request=GetDiscussionRequest(
            content_id=int(post_id),
            discussion_filter=DiscussionFilter(
                thread_length=7, num_replies=10, clik_ids=clik_ids or [], sort=sort
            ),
        ),
        context=session,
    )
    user_id_to_comments = {}
    comments_response = populate_user_id_to_comments_and_structure(
        graphql_response.discussion, user_id_to_comments, session
    )
    await resolve_users(user_id_to_comments, session)
    return {"data": {"comments": comments_response}}


@comments.route("/<post_id>", methods=["GET"])
@compress.compress()
async def get_comments(request, post_id):
    if "clik" in request.args:
        clik_ids = [weclikd_id.string_to_id(each) for each in request.args["clik"][0]]
    else:
        clik_ids = None
    if "sort" in request.args:
        sort = request.args["sort"][0].upper()
    else:
        sort = "NEW"

    await setup_request_auth(request)
    return response.json(
        await resolve_comments(
            None,
            MockGraphQLContext(request),
            post_id=post_id,
            clik_ids=clik_ids,
            sort=sort,
        ),
        escape_forward_slashes=False
    )


@comments.route("/", methods=["GET"])
@compress.compress()
async def get_comments_url(request):
    if "clik" in request.args:
        clik_ids = [weclikd_id.string_to_id(each) for each in request.args["clik"][0]]
    else:
        clik_ids = None

    url = request.args["url"][0] if "url" in request.args else None
    if not url:
        logging.error("URL parameter not set")
        return response.json({"data": None}, status=500)

    await setup_request_auth(request)
    return response.json(
        await resolve_comments(
            None, MockGraphQLContext(request), post_id=None, clik_ids=clik_ids, url=url
        )
    )
