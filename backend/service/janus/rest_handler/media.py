from sanic import Blueprint, response
from weclikd.service import storage
from janus.utils import normalize_url
from weclikd.utils import environment
from weclikd.utils.session import Session
from weclikd.service import grpc
from artemis.api.artemis_pb2 import DownloadImageRequest

bucket = storage.get_bucket(environment.IMAGE_BUCKET)
media = Blueprint("media", url_prefix="/media")


@media.route("/profiles", methods=["POST"])
async def add_profile_picture(request):
    return await _add_picture(request)


@media.route("/banners", methods=["POST"])
async def add_banner_picture(request):
    return await _add_picture(request)


@media.route("/contents", methods=["POST"])
async def add_picture(request):
    return await _add_picture(request)


async def _add_picture(request):
    if request.headers["content-type"] == "application/json":
        session = Session()
        download_image_response = await grpc.artemis.DownloadImage(
            request=DownloadImageRequest(url=request.json["url"]), context=session
        )
        image_id = str(download_image_response.image_id)
        if image_id:
            return response.json(
                {"status": "success", "id": image_id},
                headers={"Location": normalize_url(image_id)},
                status=201,
            )
        else:
            return response.text(session.user_msg, status=500)

    uploaded_file = request.files.get("file")
    picture_id = await storage.add_picture_gcp(
        bucket, uploaded_file.body, uploaded_file.type
    )
    return response.json(
        {"status": "success", "id": picture_id},
        headers={"Location": normalize_url(picture_id)},
        status=201,
    )
