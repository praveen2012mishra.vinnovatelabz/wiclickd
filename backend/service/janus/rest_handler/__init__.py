from sanic import Blueprint

from .media import media
from .comments import comments

v1_api = Blueprint.group(media, comments, url_prefix="/v1")
