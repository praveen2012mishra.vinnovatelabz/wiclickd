from weclikd.service import pubsub
import artemis.event.topic as artemis_topic
import athena.event.topic as athena_topic
import atlas.event.topic as atlas_topic
import eros.event.topic as eros_topic
import hestia.event.topic as hestia_topic


create_external_feed_subscription = pubsub.Subscription(
    artemis_topic.create_external_feed_topic, "janus"
)
update_external_feed_subscription = pubsub.Subscription(
    artemis_topic.update_external_feed_topic, "janus"
)
delete_external_feed_subscription = pubsub.Subscription(
    artemis_topic.delete_external_feed_topic, "janus"
)
create_external_post_subscription = pubsub.Subscription(
    artemis_topic.create_external_post_topic, "janus"
)
follow_external_feed_subscription = pubsub.Subscription(
    artemis_topic.follow_external_feed_topic, "janus"
)
unfollow_external_feed_subscription = pubsub.Subscription(
    artemis_topic.unfollow_external_feed_topic, "janus"
)

create_user_subscription = pubsub.Subscription(eros_topic.create_user_topic, "janus")
update_user_subscription = pubsub.Subscription(eros_topic.update_user_topic, "janus")
delete_user_subscription = pubsub.Subscription(eros_topic.delete_user_topic, "janus")

create_topic_subscription = pubsub.Subscription(
    athena_topic.create_topic_topic, "janus"
)
update_topic_subscription = pubsub.Subscription(
    athena_topic.update_topic_topic, "janus"
)
delete_topic_subscription = pubsub.Subscription(
    athena_topic.delete_topic_topic, "janus"
)
follow_topic_subscription = pubsub.Subscription(
    athena_topic.follow_topic_topic, "janus"
)
unfollow_topic_subscription = pubsub.Subscription(
    athena_topic.unfollow_topic_topic, "janus"
)

create_post_subscription = pubsub.Subscription(atlas_topic.create_post_topic, "janus")
update_post_subscription = pubsub.Subscription(atlas_topic.update_post_topic, "janus")
delete_post_subscription = pubsub.Subscription(atlas_topic.delete_post_topic, "janus")

create_clik_subscription = pubsub.Subscription(hestia_topic.create_clik_topic, "janus")
update_clik_subscription = pubsub.Subscription(hestia_topic.update_clik_topic, "janus")
delete_clik_subscription = pubsub.Subscription(hestia_topic.delete_clik_topic, "janus")
follow_clik_subscription = pubsub.Subscription(hestia_topic.follow_clik_topic, "janus")
unfollow_clik_subscription = pubsub.Subscription(
    hestia_topic.unfollow_clik_topic, "janus"
)
create_clik_member_subscription = pubsub.Subscription(
    hestia_topic.create_clik_member_topic, "janus"
)
delete_clik_member_subscription = pubsub.Subscription(
    hestia_topic.delete_clik_member_topic, "janus"
)
