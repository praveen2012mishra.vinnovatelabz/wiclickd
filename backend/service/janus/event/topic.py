from weclikd.service import pubsub
from janus.event import janus_pb2


refresh_trending_topic = pubsub.Topic(
    "refresh-trending", janus_pb2.RefreshTrendingEvent
)
