from weclikd.utils import environment


def normalize_url(picture_id):
    if not picture_id:
        return None
    if picture_id.startswith("http"):
        return picture_id
    return f"https://{environment.IMAGE_BUCKET}/{picture_id}"
