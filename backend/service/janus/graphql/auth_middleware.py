import jwt
import logging
import json
from sanic import response as sanic_response
from sanic.exceptions import Unauthorized
from weclikd.utils import session
from weclikd.service import grpc
from eros.api.eros_pb2 import (
    GetAccountIdFromAccessKeyRequest,
    GetAccountIdFromAccessKeyResponse,
)
from weclikd.utils.authorization import verify_jwt


async def setup_request_auth(request):
    if not hasattr(request.ctx, "auth") or not request.ctx.auth:
        logging.debug("Doing Authentication")
        auth = {}
        header = request.headers.get("Authorization")
        account_id, claims = verify_jwt(header, verify=False, verify_email=False)
        if account_id:
            auth["user_id"] = account_id
            auth["claims"] = claims
        else:
            access_key = request.headers.get("X-Weclikd-Access-Key")
            if access_key:
                context = session.Session({})
                response = await grpc.eros.GetAccountIdFromAccessKey(
                    request=GetAccountIdFromAccessKeyRequest(access_key=access_key),
                    context=context,
                )

                if not context.grpc_code:
                    auth["user_id"] = response.account_id
                    auth["claims"] = grpc.to_dict(response.custom_claims) or {}
                    auth["claims"]["email_verified"] = True
                else:
                    return sanic_response.text(
                        f"Invalid Access Key: {access_key}", status=401
                    )

        logging.debug("Setting auth: {}".format(auth))
        request.ctx.auth = auth

        if request.path.startswith("/graphql"):
            logging.debug("GraphQL Request: {}".format(json.dumps(request.json)))
    else:
        request.ctx.auth = None


def user_auth(func):
    def wrapper(root, info, *args, **kwargs):
        auth = info.context["request"].ctx.auth or {}
        if "user_id" not in auth:
            raise Unauthorized("Log in to Weclikd")
        # if not auth["claims"].get("email_verified"):
        #     raise Unauthorized("Email is not verified.")

        current_session = session.Session(
            {
                "current_user_id": auth["user_id"],
                "is_admin": auth.get("claims", {}).get("admin", False),
            }
        )
        info.context["session"] = current_session
        return func(root, info, *args, session=current_session, **kwargs)

    return wrapper


def account_auth(func):
    def wrapper(root, info, *args, **kwargs):
        auth = info.context["request"].ctx.auth or {}
        if "user_id" not in auth:
            raise Unauthorized("Log in to Weclikd")

        current_session = session.Session({"current_user_id": auth["user_id"]})
        info.context["session"] = current_session
        return func(root, info, *args, session=current_session, **kwargs)

    return wrapper


def optional_user_auth(func):
    def wrapper(root, info, *args, **kwargs):
        auth = info.context["request"].ctx.auth or {}
        # if auth.get("claims"):
        #    if not auth["claims"].get("email_verified"):
        #        raise Unauthorized("Email is not verified.")

        current_session = session.Session(
            {
                "current_user_id": auth.get("user_id", 0),
                "is_admin": auth.get("claims", {}).get("admin", False),
            }
        )
        info.context["session"] = current_session
        return func(root, info, *args, session=current_session, **kwargs)

    return wrapper


def admin_auth(func):
    def wrapper(root, info, *args, **kwargs):
        auth = info.context["request"].ctx.auth or {}
        if "user_id" not in auth:
            raise Unauthorized("Log in to Weclikd")

        # if not auth["claims"].get("email_verified"):
        #    raise Unauthorized("Email is not verified.")

        if not auth["claims"].get("admin"):
            raise Unauthorized("Not an Admin")

        current_session = session.Session(
            {
                "current_user_id": auth.get("user_id", 0),
                "is_admin": True,
            }
        )

        info.context["session"] = current_session
        return func(root, info, *args, session=current_session, **kwargs)

    return wrapper


def _get_decoded_authorization(request):
    firebase_header = request.headers.get("Authorization")
    if not firebase_header:
        return None
    firebase_jwt = firebase_header.split()[1]
    return jwt.decode(firebase_jwt, verify=False)
