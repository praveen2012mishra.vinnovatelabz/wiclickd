import graphene
from .connection_field import FeedConnectionField, SortEnum
from .content import ContentConnection
from .utils import decode_after
from .auth_middleware import optional_user_auth
from weclikd.service import grpc
from atlas.api.atlas_pb2 import GetFeedRequest
from atlas.api.comment_pb2 import DiscussionFilter
from atlas.api.post_pb2 import FeedFilter


class HomeFeed(graphene.ObjectType):
    posts = FeedConnectionField(ContentConnection)

    @optional_user_auth
    async def resolve_posts(
        self, _info, session, first=25, after=None, sort=None, **kargs
    ):
        return await grpc.atlas.GetFeed(
            request=GetFeedRequest(
                home=True,
                feed_filter=FeedFilter(
                    num_posts=first,
                    sort=SortEnum.to_proto(sort),
                    cursor=decode_after(after),
                ),
                discussion_filter=DiscussionFilter(
                    thread_length=0,
                    num_replies=0,
                    sort=SortEnum.to_proto(sort),
                ),
            ),
            context=session,
        )
