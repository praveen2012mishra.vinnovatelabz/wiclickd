import base64
import graphene
from typing import List
from weclikd import utils
from weclikd.utils import weclikd_id
from weclikd.common.wrappers_pb2 import BoolEnum
from eros.api.eros_pb2 import GetIdFromUsernameRequest
from weclikd.service import grpc

node_registry = {}


def register_node(node):
    node_registry[node.__name__] = node
    return node


def decode_after(after):
    if not after:
        return None
    decoded = base64.b64decode(after).decode()
    return base64.b64encode(decoded[: decoded.rfind(":")].encode()).decode()


async def get_integer_id_from_global_id(user_id: str, session) -> int:
    return (await get_integer_ids_from_global_ids([user_id], session))[0]


async def get_integer_ids_from_global_ids(user_ids: List[str], session) -> List[int]:
    usernames = []
    ids = []
    for user_id in user_ids:
        username_or_id = user_id[user_id.find(":") + 1 :]
        if username_or_id.isnumeric():
            ids.append(int(username_or_id))
        else:
            usernames.append(username_or_id)

    response = await grpc.eros.GetIdFromUsername(
        request=GetIdFromUsernameRequest(usernames=usernames), context=session
    )
    return ids + list(response.user_ids)


class CustomNode(graphene.relay.Node):
    class Meta:
        name = "CustomNode"

    @staticmethod
    def to_global_id(node_type, node_id):
        if isinstance(node_id, bytes):
            return "{}:{}".format(node_type, utils.weclikd_encode_base64(node_id))
        else:
            return "{}:{}".format(node_type, node_id)

    @staticmethod
    def get_node_from_id(info, node_id, only_type):
        node_id = node_id[node_id.find(":") + 1 :]
        return node_registry.get(only_type).get_node(info, node_id)

    @staticmethod
    def get_node_from_global_id(info, global_id, only_type=None):
        global_id_split = global_id.split(":")
        if len(global_id_split) < 2:
            raise Exception(f"Invalid id: {global_id}")
        node_type = global_id_split[0]
        node_id = ":".join(global_id_split[1:])

        if only_type:
            # We assure that the node type that we want to retrieve
            # is the same that was indicated in the field type
            assert node_type == only_type._meta.name, "Received not compatible node."

        node = node_registry.get(node_type)
        if not node:
            raise Exception("Invalid node type {}".format(node))
        return node.get_node(info, node_id)

    @staticmethod
    def get_id_from_global_id(global_id, allow_string=False, hash=True):
        if global_id is None:
            return None
        int_id = global_id[global_id.find(":") + 1 :]
        if not int_id.isnumeric():
            if not allow_string:
                raise Exception(f"Invalid id. Must be integer id={global_id}")
            elif hash:
                int_id = weclikd_id.string_to_id(int_id)
            else:
                return int_id
        return int(int_id)


class WeclikdDateTime(graphene.DateTime):
    @staticmethod
    def serialize(dt):
        return dt[:-1]

    @classmethod
    def parse_literal(cls, node):
        return super(WeclikdDateTime, cls).parse_literal(node)

    @staticmethod
    def parse_value(value):
        return graphene.DateTime.parse_value(value)


class WeclikdBool(graphene.Boolean):
    @staticmethod
    def serialize(dt):
        return True if dt == "TRUE" else False

    @staticmethod
    def parse_value(value):
        return BoolEnum.TRUE if value else BoolEnum.FALSE
