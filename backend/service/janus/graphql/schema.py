import graphene
from .utils import CustomNode
from .account import (
    CreateAccount,
    Login,
    Account,
    ChangeAccountSettings,
    ChangeSubscription,
    ChangePaymentInfo,
    ChangeAccessKey,
    MarkNotificationsAsRead,
    DeleteNotification,
)
from .home_feed import HomeFeed
from .user import (
    MyUser,
    User,
    UserConnection,
    EditUser,
    CheckUsername,
)
from .follow import (
    FollowUser,
    UnfollowUser,
    FollowClik,
    UnfollowClik,
    FollowTopic,
    UnfollowTopic,
    FollowExternalFeed,
    UnfollowExternalFeed,
)
from .clik import (
    Clik,
    ClikConnection,
    CreateClik,
    JoinClik,
    InviteToClik,
    ClikApplicationConnection,
    ApproveClik,
    PromoteClikMember,
    KickClikMember,
    EditClik,
    RejectClikMember,
    DeleteClik,
    ClikInviteKey,
    CreateClikInviteKey,
)
from .topic import (
    Topic,
    TopicConnection,
    ApproveTopic,
    EditTopic,
    CreateTopic,
    DeleteTopic,
)
from .content import (
    Post,
    CreatePost,
    CreateComment,
    LikeContent,
    CommentConnection,
    DeleteContent,
    EditPost,
    SharePost,
    Comment,
    EditComment,
)
from .report import (
    ReportContent,
    ReportUser,
    BanUser,
)
from .external_feed import (
    CreateExternalFeed,
    EditExternalFeed,
    TestExternalFeed,
    DeleteExternalFeed,
    ExternalFeed,
    ExternalFeedConnection,
)
from .url_info import UrlInfo
from .search import Search
from .auth_middleware import optional_user_auth, admin_auth, user_auth
from .connection_field import CommentConnectionField, DefaultConnectionField, SortEnum
from weclikd.service import grpc
from weclikd.common.filter_pb2 import DefaultFilter
from artemis.api.artemis_pb2 import GetAllExternalFeedsRequest
from eros.api.eros_pb2 import GetAllUsersRequest
from atlas.api.atlas_pb2 import GetDiscussionRequest
from atlas.api.comment_pb2 import DiscussionFilter
from athena.api.athena_pb2 import GetAllTopicsRequest
from hestia.api.hestia_pb2 import (
    GetCliksToApproveRequest,
    GetAllCliksRequest,
    GetClikInviteKeyInfoRequest,
)


class Query(graphene.ObjectType):
    node = CustomNode.Field()
    search = graphene.Field(Search)
    home_feed = graphene.Field(HomeFeed)
    account = graphene.Field(Account, id=graphene.ID())
    my_user = CustomNode.Field(MyUser, id=graphene.ID(required=True))
    user = CustomNode.Field(User, id=graphene.ID(required=True))
    clik = CustomNode.Field(Clik, id=graphene.ID(required=True))
    clik_invite_key = graphene.Field(
        ClikInviteKey, invite_key=graphene.String(required=True)
    )
    topic = CustomNode.Field(Topic, id=graphene.ID(required=True))
    external_feed = CustomNode.Field(ExternalFeed, id=graphene.ID(required=True))
    post = graphene.Field(Post, id=graphene.ID(required=True), url=graphene.String())
    comment = CustomNode.Field(Comment, id=graphene.ID(required=True))

    comments = CommentConnectionField(
        CommentConnection,
        id=graphene.ID(required=True),
        clik_ids=graphene.List(graphene.ID),
        user_ids=graphene.List(graphene.ID),
        sort=graphene.Argument(SortEnum),
    )
    clik_applications = DefaultConnectionField(
        ClikApplicationConnection, "applications"
    )
    url_info = graphene.Field(UrlInfo, url=graphene.String(required=True))
    external_feed_list = DefaultConnectionField(
        ExternalFeedConnection, "feeds", sort=SortEnum(default_value="NEW")
    )
    topic_list = DefaultConnectionField(
        TopicConnection, "topics", sort=SortEnum(default_value="TRENDING")
    )
    clik_list = DefaultConnectionField(
        ClikConnection, "cliks", sort=SortEnum(default_value="TRENDING")
    )
    user_list = DefaultConnectionField(
        UserConnection, "users", sort=SortEnum(default_value="TRENDING")
    )

    def resolve_search(self, _info, **_kwargs):
        return Search()

    def resolve_home_feed(self, _info, **_kwargs):
        return HomeFeed()

    async def resolve_post(self, info, id=None, url=None):
        if url:
            return await UrlInfo.resolve_post_from_url(self, info, url=url)
        else:
            return CustomNode.get_node_from_global_id(info, id, only_type=Post)

    @user_auth
    def resolve_account(self, info, session, id=None):
        return CustomNode.get_node_from_global_id(info, f"Account:{session.account_id}")

    @optional_user_auth
    async def resolve_trending_cliks(self, _info, session, **_kwargs):
        response = await grpc.hestia.GetAllCliks(
            request=GetAllCliksRequest(
                filter=DefaultFilter(sort=DefaultFilter.TRENDING, num_items=10)
            ),
            context=session,
        )
        return grpc.to_dict(response).get("cliks", [])

    @optional_user_auth
    async def resolve_trending_users(self, _info, session, **_kwargs):
        response = await grpc.eros.GetAllUsers(
            request=GetAllUsersRequest(
                filter=DefaultFilter(sort=DefaultFilter.TRENDING, num_items=10)
            ),
            context=session,
        )
        return grpc.to_dict(response).get("profiles", [])

    @optional_user_auth
    async def resolve_trending_topics(self, _info, session, **_kwargs):
        response = await grpc.athena.GetAllTopics(
            request=GetAllTopicsRequest(
                filter=DefaultFilter(sort=DefaultFilter.TRENDING, num_items=10)
            ),
            context=session,
        )
        return grpc.to_dict(response).get("topics", [])

    @optional_user_auth
    async def resolve_trending_external_feeds(self, _info, session, **_kwargs):
        response = await grpc.artemis.GetAllExternalFeeds(
            request=GetAllExternalFeedsRequest(
                filter=DefaultFilter(sort=DefaultFilter.TRENDING, num_items=10)
            ),
            context=session,
        )
        return grpc.to_dict(response).get("feeds", [])

    @optional_user_auth
    async def resolve_comments(
        self, info, session, id=None, user_ids=None, clik_ids=None, sort="NEW", **kwargs
    ):
        (
            num_replies,
            thread_length,
        ) = CommentConnectionField.get_thread_length_and_num_replies(
            info.field_asts, info.variable_values
        )
        response = await grpc.atlas.GetDiscussion(
            request=GetDiscussionRequest(
                content_id=CustomNode.get_id_from_global_id(id),
                discussion_filter=DiscussionFilter(
                    thread_length=num_replies,
                    num_replies=num_replies,
                    cursor=CommentConnectionField.get_first_comments_cursor(info),
                    clik_ids=[
                        CustomNode.get_id_from_global_id(each, allow_string=True)
                        for each in clik_ids or []
                    ],
                    sort=sort or "NEW",
                ),
            ),
            context=session,
        )
        return grpc.to_dict(response)["discussion"]

    @admin_auth
    async def resolve_clik_applications(
        self, info, session, first=None, after=None, **kwargs
    ):
        return await grpc.hestia.GetCliksToApprove(
            request=GetCliksToApproveRequest(
                filter=DefaultFilter(num_items=first, cursor=after)
            ),
            context=session,
        )

    async def resolve_url_info(self, info, url):
        return await UrlInfo.resolve_link_info(self, info, url=url)

    @optional_user_auth
    async def resolve_external_feed_list(
        self, info, session, first=None, after=None, sort=None, **kwargs
    ):
        return await grpc.artemis.GetAllExternalFeeds(
            request=GetAllExternalFeedsRequest(
                filter=DefaultFilter(num_items=first, sort=sort, cursor=after)
            ),
            context=session,
        )

    @optional_user_auth
    async def resolve_clik_list(
        self, info, session, first=None, after=None, sort=None, **kwargs
    ):
        return await grpc.hestia.GetAllCliks(
            request=GetAllCliksRequest(
                filter=DefaultFilter(num_items=first, sort=sort, cursor=after)
            ),
            context=session,
        )

    @optional_user_auth
    async def resolve_topic_list(
        self, info, session, first=None, after=None, sort=None, **kwargs
    ):
        return await grpc.athena.GetAllTopics(
            request=GetAllTopicsRequest(
                filter=DefaultFilter(num_items=first, sort=sort, cursor=after)
            ),
            context=session,
        )

    @optional_user_auth
    async def resolve_user_list(
        self, info, session, first=None, after=None, sort=None, **kwargs
    ):
        return await grpc.eros.GetAllUsers(
            request=GetAllUsersRequest(
                filter=DefaultFilter(num_items=first, sort=sort, cursor=after)
            ),
            context=session,
        )

    @optional_user_auth
    async def resolve_clik_invite_key(self, info, session, invite_key, **kwargs):
        response = await grpc.hestia.GetClikInviteKeyInfo(
            request=GetClikInviteKeyInfoRequest(
                invite_key=invite_key,
            ),
            context=session,
        )
        return grpc.to_dict(response)


class Mutation(graphene.ObjectType):
    login = Login.Field()
    account_create = CreateAccount.Field()
    account_change_settings = ChangeAccountSettings.Field()
    account_change_subscription = ChangeSubscription.Field()
    account_change_payment_info = ChangePaymentInfo.Field()
    account_change_access_key = ChangeAccessKey.Field()
    account_mark_notifications_as_read = MarkNotificationsAsRead.Field()
    account_notification_delete = DeleteNotification.Field()

    user_follow = FollowUser.Field()
    user_unfollow = UnfollowUser.Field()
    user_edit = EditUser.Field()
    user_report = ReportUser.Field()
    # user_ban = BanUser.Field()
    username_check = CheckUsername.Field()

    clik_follow = FollowClik.Field()
    clik_unfollow = UnfollowClik.Field()
    clik_create = CreateClik.Field()
    clik_create_invite_key = CreateClikInviteKey.Field()
    clik_join = JoinClik.Field()
    clik_invite = InviteToClik.Field()
    clik_approve = ApproveClik.Field()
    clik_member_reject = RejectClikMember.Field()
    clik_member_promote = PromoteClikMember.Field()
    clik_member_kick = KickClikMember.Field()
    clik_edit = EditClik.Field()
    clik_delete = DeleteClik.Field()

    topic_follow = FollowTopic.Field()
    topic_unfollow = UnfollowTopic.Field()
    topic_approve = ApproveTopic.Field()
    topic_edit = EditTopic.Field()
    topic_create = CreateTopic.Field()
    topic_delete = DeleteTopic.Field()

    post_share = SharePost.Field()
    post_create = CreatePost.Field()
    post_edit = EditPost.Field()
    comment_create = CreateComment.Field()
    comment_edit = EditComment.Field()
    content_like = LikeContent.Field()
    content_report = ReportContent.Field()
    content_delete = DeleteContent.Field()

    external_feed_follow = FollowExternalFeed.Field()
    external_feed_unfollow = UnfollowExternalFeed.Field()
    external_feed_create = CreateExternalFeed.Field()
    external_feed_test = TestExternalFeed.Field()
    external_feed_delete = DeleteExternalFeed.Field()
    external_feed_edit = EditExternalFeed.Field()


schema = graphene.Schema(query=Query, mutation=Mutation, auto_camelcase=False)
