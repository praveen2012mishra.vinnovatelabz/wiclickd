import graphene


class LikeTotals(graphene.ObjectType):
    red = graphene.Field(graphene.Int, default_value=0)
    silver = graphene.Field(graphene.Int, default_value=0)
    gold = graphene.Field(graphene.Int, default_value=0)
    diamond = graphene.Field(graphene.Int, default_value=0)
