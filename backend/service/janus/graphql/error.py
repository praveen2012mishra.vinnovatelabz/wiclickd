from typing import Optional
import graphene
from weclikd.utils.session import Session
from grpc import StatusCode


class StatusType(graphene.Enum):
    UNKNOWN = "unknown"
    OK = "ok"
    INVALID_ARGUMENT = "invalid argument"
    DEADLINE_EXCEEDED = "deadline exceeded"
    NOT_FOUND = "not found"
    ALREADY_EXISTS = "already exists"
    PERMISSION_DENIED = "permission denied"
    RESUORCE_EXHAUSTED = "resource exhausted"
    FAILED_PRECONDITION = "failed precondition"
    ABORTED = "aborted"
    OUT_OF_RANGE = "out of range"
    UNIMPLEMENTED = "unimplemented"
    INTERNAL = "internal"
    UNAVAILABLE = "unavailable"
    DATA_LOSS = "data loss"
    UNAUTHENTICATED = "unauthenticated"


class WeclikdStatus(graphene.ObjectType):
    success = graphene.Boolean()
    status = graphene.Field(StatusType)
    custom_status = graphene.String()
    user_msg = graphene.String()
    debug_error_msg = graphene.String()

    @classmethod
    def create(
        cls,
        status: StatusType,
        custom_status: Optional[str] = None,
        user_msg: str = None,
    ):
        return {
            "success": status == StatusType.OK,
            "status": status,
            "custom_status": custom_status,
            "user_msg": user_msg,
        }

    @classmethod
    def from_grpc(cls, session: Session, custom_status: Optional[str] = None):
        return {
            "success": session.grpc_code is None or session.grpc_code == StatusCode.OK,
            "status": StatusType.OK
            if session.grpc_code is None
            else session.grpc_code.value[1],
            "custom_status": session.status,
            "user_msg": session.user_msg,
        }
