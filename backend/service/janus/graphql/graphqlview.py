from functools import partial
from cgi import parse_header
import traceback
from promise import Promise
from sanic.response import HTTPResponse
from sanic.views import HTTPMethodView
from graphql.error.base import GraphQLError
from graphql.type.schema import GraphQLSchema
from graphql.execution.executors.asyncio import AsyncioExecutor
from graphql_server import (
    HttpQueryError,
    default_format_error,
    encode_execution_results,
    json_encode,
    load_json_body,
    run_http_query,
)
from weclikd.utils import environment


class GraphQLView(HTTPMethodView):
    schema = None
    executor = None
    root_value = None
    context = None
    context_cb = None
    pretty = False
    middleware = None
    batch = False
    max_age = 86400

    _enable_async = True

    methods = ["GET", "POST", "PUT", "DELETE"]

    def __init__(self, **kwargs):
        super(GraphQLView, self).__init__()
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)

        self._enable_async = self._enable_async and isinstance(
            self.executor, AsyncioExecutor
        )
        assert isinstance(
            self.schema, GraphQLSchema
        ), "A Schema is required to be provided to GraphQLView."

    def get_root_value(self, _request):
        return self.root_value

    def get_middleware(self, _request):
        return self.middleware

    def get_executor(self, _request):
        return self.executor

    # format_error = staticmethod(default_format_error)
    @staticmethod
    def format_error(error):
        # type: (Exception) -> Dict[str, Any]
        # Protect against UnicodeEncodeError when run in py2 (#216)
        try:
            message = str(error)
        except UnicodeEncodeError:
            message = error.message.encode("utf-8")  # type: ignore
        formatted_error = {"message": message}  # type: Dict[str, Any]
        if isinstance(error, GraphQLError):
            if error.locations is not None:
                formatted_error["locations"] = [
                    {"line": loc.line, "column": loc.column} for loc in error.locations
                ]
            if error.path is not None:
                formatted_error["path"] = error.path

            if error.extensions is not None:
                formatted_error["extensions"] = error.extensions

        if environment.is_test_env() or environment.is_dev_env():
            formatted_error["callstack"] = traceback.format_exception(
                type(error), error, error.stack, limit=20
            )

        return formatted_error

    encode = staticmethod(json_encode)

    # these functions are defined to workaround an issue where sanic does not think these methods are not
    # enabled unless these functions are defined.
    async def post(self, request, *args, **kwargs):
        pass

    async def get(self, request, *args, **kwargs):
        pass

    async def options(self, request, *args, **kwargs):
        pass

    async def dispatch_request(self, request, query=None, *args, **kwargs):
        try:
            request_method = request.method.lower()
            data = self.parse_body(request)

            pretty = self.pretty or request.args.get("pretty")
            self.context = self.context_cb(request, self.context)
            if request_method != "options":
                execution_results, all_params = run_http_query(
                    self.schema,
                    request_method,
                    data,
                    query_data=request.args,
                    batch_enabled=self.batch,
                    # Execute options
                    return_promise=self._enable_async,
                    root=self.get_root_value(request),
                    context=self.context,
                    middleware=self.get_middleware(request),
                    executor=self.get_executor(request),
                )
                awaited_execution_results = await Promise.all(execution_results)
                result, status_code = encode_execution_results(
                    awaited_execution_results,
                    is_batch=isinstance(data, list),
                    format_error=self.format_error,
                    encode=partial(self.encode, pretty=pretty),
                )

                return HTTPResponse(
                    result, status=status_code, content_type="application/json"
                )

            else:
                return self.process_preflight(request)

        except HttpQueryError as e:
            return HTTPResponse(
                self.encode({"errors": [default_format_error(e)]}),
                status=e.status_code,
                headers=e.headers,
                content_type="application/json",
            )

    # noinspection PyBroadException
    def parse_body(self, request):
        content_type = self.get_mime_type(request)
        if content_type == "application/graphql":
            return {"query": request.body.decode("utf8")}

        elif content_type == "application/json":
            return load_json_body(request.body.decode("utf8"))

        elif content_type in (
            "application/x-www-form-urlencoded",
            "multipart/form-data",
        ):
            return request.form

        return {}

    @staticmethod
    def get_mime_type(request):
        # We use mimetype here since we don't need the other
        # information provided by content_type
        if "content-type" not in request.headers:
            return None

        mime_type, _ = parse_header(request.headers["content-type"])
        return mime_type

    def process_preflight(self, request):
        """Preflight request support for apollo-client
        https://www.w3.org/TR/cors/#resource-preflight-requests"""
        origin = request.headers.get("Origin", "")
        method = request.headers.get("Access-Control-Request-Method", "").upper()

        if method and method in self.methods:
            return HTTPResponse(
                status=200,
                headers={
                    "Access-Control-Allow-Origin": origin,
                    "Access-Control-Allow-Methods": ", ".join(self.methods),
                    "Access-Control-Max-Age": str(self.max_age),
                },
            )
        else:
            return HTTPResponse(
                status=400,
            )
