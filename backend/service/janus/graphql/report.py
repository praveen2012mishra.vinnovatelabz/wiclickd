import graphene
from atlas.api.report_pb2 import Severity
from janus.graphql.auth_middleware import user_auth, admin_auth
from janus.graphql.utils import CustomNode
from janus.graphql.error import WeclikdStatus
from janus.graphql.content import ContentUnion
from janus.graphql.external_feed import ExternalFeed
from janus.graphql.topic import Topic
from janus.graphql.clik import Clik
from janus.graphql.user import User
from weclikd.service import grpc
from atlas.api.atlas_pb2 import ReportContentRequest
from atlas.api import report_pb2


class SeverityType(graphene.Enum):
    WARNING = "WARNING"
    TEMPORARY_BAN = "TEMPORARY_BAN"
    PERMANENT_BAN = "PERMANENT_BAN"

    @staticmethod
    def to_proto(value):
        if value == SeverityType.WARNING:
            return Severity.SeverityType.WARNING
        elif value == SeverityType.TEMPORARY_BAN:
            return Severity.SeverityType.TEMPORARY_BAN
        else:
            return Severity.SeverityType.PERMANENT_BAN


class ReportType(graphene.Enum):
    OFF_TOPIC = "OFF_TOPIC"
    LOW_QUALITY = "LOW_QUALITY"
    HATE = "HATE"
    OTHER = "OTHER"


class ClikReportType(graphene.Enum):
    MISMANAGEMENT = "MISMANAGEMENT"
    HATE = "HATE"
    OTHER = "OTHER"


class UserReportType(graphene.Enum):
    BANNED_CONTENT = "BANNED_CONTENT"
    SPAM = "SPAM"
    IMPERSONATION = "IMPERSONATION"
    SOLICITATION = "SOLICITATION"
    OTHER = "OTHER"


class ContentReport(graphene.ObjectType):
    id = graphene.ID(required=True)
    content = graphene.Field(ContentUnion)
    violations = graphene.List(ReportType)
    off_topic_cliks = graphene.List(graphene.String)
    off_topic_topics = graphene.List(graphene.String)
    message = graphene.String()


class UserReport(graphene.ObjectType):
    id = graphene.ID(required=True)
    user = graphene.Field(User)
    violations = graphene.List(UserReportType)
    related_report = graphene.Field("janus.graphql.report.ReportUnion")
    message = graphene.String()


# TOOD: remove
class SingleReport(graphene.InputObjectType):
    type = graphene.Field(ReportType, required=True)
    off_topic_cliks = graphene.List(graphene.String)
    off_topic_topics = graphene.List(graphene.String)


# rename to ContentReportInput
class ReportInput(graphene.InputObjectType):
    content_id = graphene.Field(graphene.ID, required=True)
    reports = graphene.List(SingleReport, required=True)
    # off_topic_cliks = graphene.List(graphene.String)
    # off_topic_topics = graphene.List(graphene.String)


class ReportUnion(graphene.Union):
    class Meta:
        types = (ContentReport,)

    @staticmethod
    def get_reports(type: str, first: str = 10, after: str = None):
        pass


class Counter(graphene.ObjectType):
    name = graphene.String()
    count = graphene.Int()


class ContentReportSummary(graphene.ObjectType):
    off_topic_topics_counter = graphene.List(Counter)
    num_violations_counter = graphene.List(Counter)


class ReportContentOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class ReportContent(graphene.Mutation):
    class Arguments:
        input = ReportInput(required=True)

    Output = ReportContentOutput

    @user_auth
    async def mutate(self, _info, input, session):
        # TODO: if clik admin, unshare post instead
        # TODO: if weclikd admin, remove instead

        await grpc.atlas.ReportContent(
            request=ReportContentRequest(
                content_id=CustomNode.get_id_from_global_id(input.content_id),
                reports=[
                    report_pb2.Report(
                        type=each.type,
                        off_topic_cliks=each.off_topic_cliks,
                        off_topic_topics=each.off_topic_topics,
                    )
                    for each in input.reports
                ],
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class ReportUserInput(graphene.InputObjectType):
    user_id = graphene.ID(required=True)
    violations = graphene.List(UserReportType)
    related_report_id = graphene.ID()
    message = graphene.String()


class ReportUserOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    report = graphene.Field(UserReport)


class ReportUser(graphene.Mutation):
    class Arguments:
        input = ReportUserInput(required=True)

    Output = ReportUserOutput

    @admin_auth
    async def mutate(self, info, input, session):
        # TODO  also implement GetUserReport
        return {"status": {"success": True, "status": "SUCCESS"}}


"""
class ReportExternalFeedInput(graphene.InputObjectType):
    feed_id = graphene.ID(required=True)
    violations = graphene.List(ReportType)
    message = graphene.String()


class ExternalFeedReport(graphene.ObjectType):
    id = graphene.ID(required=True)
    external_feed = graphene.Field(ExternalFeed)
    violations = graphene.List(ReportType)
    message = graphene.String()


class ReportExternalFeedOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    report = graphene.Field(ExternalFeedReport)


class ReportExternalFeed(graphene.Mutation):
    class Arguments:
        input = ReportExternalFeedInput(required=True)

    Output = ReportExternalFeedOutput

    @admin_auth
    async def mutate(self, info, input, session):
        return {"status": {"success": True, "status": "SUCCESS"}}


class ReportTopicInput(graphene.InputObjectType):
    topic_id = graphene.ID(required=True)
    violations = graphene.List(ReportType)
    message = graphene.String()


class TopicReport(graphene.ObjectType):
    id = graphene.ID(required=True)
    topic = graphene.Field(Topic)
    violations = graphene.List(ReportType)
    message = graphene.String()


class ReportTopicOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    report = graphene.Field(TopicReport)


class ReportTopic(graphene.Mutation):
    class Arguments:
        input = ReportTopicInput(required=True)

    Output = ReportTopicOutput

    @admin_auth
    async def mutate(self, info, input, session):
        return {"status": {"success": True, "status": "SUCCESS"}}


class ReportClikInput(graphene.InputObjectType):
    topic_id = graphene.ID(required=True)
    violations = graphene.List(ClikReportType)
    offending_users = graphene.List(graphene.ID)
    message = graphene.String()


class ClikReport(graphene.ObjectType):
    id = graphene.ID(required=True)
    clik = graphene.Field(Clik)
    violations = graphene.List(ReportType)
    offending_users = graphene.List(User)
    message = graphene.String()


class ReportClikOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    report = graphene.Field(ClikReport)


class ReportClik(graphene.Mutation):
    class Arguments:
        input = ReportClikInput(required=True)

    Output = ReportClikOutput

    @admin_auth
    async def mutate(self, info, input, session):
        return {"status": {"success": True, "status": "SUCCESS"}}
"""


class BanUserInput(graphene.InputObjectType):
    user_id = graphene.ID(required=True)
    report_id = graphene.ID()
    severity = graphene.Field(SeverityType)
    message = graphene.String()


class UserBan(graphene.ObjectType):
    id = graphene.ID(required=True)
    user = graphene.Field(User)
    report = graphene.Field(UserReport)
    severity = graphene.Field(SeverityType)
    message = graphene.String()
    duration_days = graphene.Int()  # days


class BanUserOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    ban = graphene.Field(UserBan)


class BanUser(graphene.Mutation):
    class Arguments:
        input = BanUserInput(required=True)

    Output = BanUserOutput

    @admin_auth
    async def mutate(self, info, input, session):
        return {"status": {"success": True, "status": "SUCCESS"}}


"""

class UnbanUserInput(graphene.InputObjectType):
    user_id = graphene.ID(required=True)
    ban_id = graphene.ID()
    message = graphene.String()
    severity = graphene.Field(SeverityType)
    duration_days = graphene.Int()


class UnbanUserOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    ban = graphene.Field(UserBan)


class UnbanUser(graphene.Mutation):
    class Arguments:
        input = UnbanUserInput(required=True)

    Output = UnbanUserOutput

    @admin_auth
    async def mutate(self, info, input, session):
        return {"status": {"success": True, "status": "SUCCESS"}}


class TypeName(graphene.Enum):
    USER = "USER"
    TOPIC = "TOPIC"
    CLIK = "CLIK"
    EXTERNAL_FEED = "EXTERNAL_FEED"
    CONTENT = "CONTENT"


"""
