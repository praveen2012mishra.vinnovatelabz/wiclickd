import graphene
from .external_feed import ExternalFeed
from .auth_middleware import user_auth
from .utils import CustomNode, WeclikdDateTime
from .error import WeclikdStatus
from hestia.api import clik_pb2
from weclikd.common import follow_pb2 as common_follow_pb2
from eros.api.eros_pb2 import FollowUserRequest, UnfollowUserRequest
from athena.api.athena_pb2 import FollowTopicRequest, UnfollowTopicRequest
from hestia.api.hestia_pb2 import FollowClikRequest, UnfollowClikRequest
from artemis.api.artemis_pb2 import FollowFeedRequest, UnfollowFeedRequest
from weclikd.service import grpc

# Shared Fields


class FollowType(graphene.Enum):
    UNKNOWN = "UNKNOWN"
    FOLLOW = "FOLLOW"
    FAVORITE = "FAVORITE"


class FollowSettingsFields(graphene.AbstractType):
    filter_value = graphene.Field(graphene.Int)
    follow_type = graphene.Field(FollowType)


# Output Types


class FollowSettings(graphene.ObjectType, FollowSettingsFields):
    filter_value = graphene.Field(graphene.Int, default_value=100)
    follow_type = graphene.Field(FollowType, default_value="FOLLOW")

    @staticmethod
    def to_proto(value):
        if value == FollowType.FOLLOW:
            return common_follow_pb2.FollowSettings.FollowType.FOLLOW
        elif value == FollowType.FAVORITE:
            return common_follow_pb2.FollowSettings.FollowType.FAVORITE
        else:
            return common_follow_pb2.FollowSettings.FollowType.UNKNOWN


class ClikMemberType(graphene.Enum):
    UNKNOWN = "UNKNOWN"
    OBSERVER = "OBSERVER"
    FOLLOWER = "FOLLOWER"
    MEMBER = "MEMBER"
    ADMIN = "ADMIN"
    SUPER_ADMIN = "SUPER_ADMIN"

    @staticmethod
    def to_proto(value):
        if value == ClikMemberType.OBSERVER:
            return clik_pb2.ClikMember.ClikMemberType.OBSERVER
        elif value == ClikMemberType.FOLLOWER:
            return clik_pb2.ClikMember.ClikMemberType.FOLLOWER
        elif value == ClikMemberType.MEMBER:
            return clik_pb2.ClikMember.ClikMemberType.MEMBER
        elif value == ClikMemberType.ADMIN:
            return clik_pb2.ClikMember.ClikMemberType.ADMIN
        elif value == ClikMemberType.SUPER_ADMIN:
            return clik_pb2.ClikMember.ClikMemberType.SUPER_ADMIN
        else:
            return clik_pb2.ClikMember.ClikMemberType.UNKNOWN


class ClikMember(graphene.ObjectType):
    user = graphene.Field("janus.graphql.user.User")
    type = graphene.Field(ClikMemberType)
    joined = graphene.Field(WeclikdDateTime)

    async def resolve_user(self, info):
        return info.context["data_loaders"]["users"].load(self.get("user_id"))


class ClikFollowSettings(graphene.ObjectType):
    settings = graphene.Field(FollowSettings)
    member_type = graphene.Field(ClikMemberType, default_value="FOLLOWER")
    created = graphene.Field(WeclikdDateTime)
    clik = graphene.Field("janus.graphql.clik.Clik")


class UserFollowSettings(graphene.ObjectType):
    settings = graphene.Field(FollowSettings)
    user = graphene.Field("janus.graphql.user.User")
    created = WeclikdDateTime()


class TopicFollowSettings(graphene.ObjectType):
    settings = graphene.Field(FollowSettings)
    topic = graphene.Field("janus.graphql.topic.Topic")
    created = WeclikdDateTime()


class FeedFollowSettings(graphene.ObjectType):
    settings = graphene.Field(FollowSettings)
    feed = graphene.Field("janus.graphql.external_feed.ExternalFeed")
    created = WeclikdDateTime()


class FollowOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class UnfollowOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


# InputTypes


class FollowSettingsInput(graphene.InputObjectType, FollowSettingsFields):
    pass


class FollowUserInput(graphene.InputObjectType):
    user_id = graphene.Field(graphene.ID, required=True)
    follow_type = graphene.Field(FollowType, default_value="FOLLOW")


class FollowTopicInput(graphene.InputObjectType):
    topic_id = graphene.Field(graphene.ID, required=True)
    follow_type = graphene.Field(FollowType, default_value="FOLLOW")


class FollowClikInput(graphene.InputObjectType):
    clik_id = graphene.Field(graphene.ID, required=True)
    follow_type = graphene.Field(FollowType, default_value="FOLLOW")


# Mutations


class FollowUser(graphene.Mutation):
    class Arguments:
        input = FollowUserInput(required=True)

    Output = FollowOutput

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.eros.FollowUser(
            request=FollowUserRequest(
                user_id=CustomNode.get_id_from_global_id(input.user_id),
                settings=common_follow_pb2.FollowSettings(
                    follow_type=FollowSettings.to_proto(input.follow_type)
                ),
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class UnfollowUserInput(graphene.InputObjectType):
    user_id = graphene.ID()


class UnfollowUser(graphene.Mutation):
    class Arguments:
        input = UnfollowUserInput()

    Output = UnfollowOutput

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.eros.UnfollowUser(
            request=UnfollowUserRequest(
                user_id=CustomNode.get_id_from_global_id(input.user_id)
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class FollowTopic(graphene.Mutation):
    class Arguments:
        input = FollowTopicInput(required=True)

    Output = FollowOutput

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.athena.FollowTopic(
            request=FollowTopicRequest(
                topic_id=CustomNode.get_id_from_global_id(
                    input.topic_id, allow_string=True
                ),
                settings=common_follow_pb2.FollowSettings(
                    follow_type=FollowSettings.to_proto(input.follow_type)
                ),
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class UnfollowTopicInput(graphene.InputObjectType):
    topic_id = graphene.ID()


class UnfollowTopic(graphene.Mutation):
    class Arguments:
        input = UnfollowTopicInput(required=True)

    Output = UnfollowOutput

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.athena.UnfollowTopic(
            request=UnfollowTopicRequest(
                topic_id=CustomNode.get_id_from_global_id(
                    input.topic_id, allow_string=True
                )
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class FollowClik(graphene.Mutation):
    class Arguments:
        input = FollowClikInput(required=True)

    Output = FollowOutput

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.hestia.FollowClik(
            request=FollowClikRequest(
                clik_id=CustomNode.get_id_from_global_id(
                    input.clik_id, allow_string=True
                ),
                settings=common_follow_pb2.FollowSettings(
                    follow_type=FollowSettings.to_proto(input.follow_type)
                ),
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class UnfollowClikInput(graphene.InputObjectType):
    clik_id = graphene.ID()


class UnfollowClik(graphene.Mutation):
    class Arguments:
        input = UnfollowClikInput()

    Output = UnfollowOutput

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.hestia.UnfollowClik(
            request=UnfollowClikRequest(
                clik_id=CustomNode.get_id_from_global_id(
                    input.clik_id, allow_string=True
                )
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class FollowExternlFeedInput(graphene.InputObjectType):
    feed_id = graphene.Field(graphene.ID, required=True)
    follow_type = graphene.Field(FollowType, default_value="FOLLOW")


class FollowExternalFeed(graphene.Mutation):
    class Arguments:
        input = FollowExternlFeedInput(required=True)

    Output = FollowOutput

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.artemis.FollowFeed(
            request=FollowFeedRequest(
                feed_id=await ExternalFeed.normalize_id(input.feed_id, session),
                settings=common_follow_pb2.FollowSettings(
                    follow_type=FollowSettings.to_proto(input.follow_type)
                ),
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class UnfollowExternalFeedInput(graphene.InputObjectType):
    feed_id = graphene.ID()


class UnfollowExternalFeed(graphene.Mutation):
    class Arguments:
        input = UnfollowExternalFeedInput()

    Output = UnfollowOutput

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.artemis.UnfollowFeed(
            request=UnfollowFeedRequest(
                feed_id=await ExternalFeed.normalize_id(input.feed_id, session),
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}
