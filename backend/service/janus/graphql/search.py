import graphene
from .topic import Topic
from .clik import Clik
from .content import Post
from .user import User
from .external_feed import ExternalFeed
from .auth_middleware import optional_user_auth
from janus.es import es


class SearchInput(graphene.InputObjectType):
    topics = graphene.List(graphene.String)
    cliks = graphene.List(graphene.String)
    users = graphene.List(graphene.String)
    feeds = graphene.List(graphene.String)
    terms = graphene.List(graphene.String)


class Search(graphene.ObjectType):
    class SearchEverything(graphene.ObjectType):
        topics = graphene.List(Topic)
        cliks = graphene.List(Clik)
        users = graphene.List(User)
        posts = graphene.List(Post)
        feeds = graphene.List(ExternalFeed)

    everything = graphene.Field(
        SearchEverything, input=graphene.Argument(SearchInput, required=True)
    )
    topics = graphene.List(Topic, prefix=graphene.String(required=True))
    cliks = graphene.List(Clik, prefix=graphene.String(required=True))
    users = graphene.List(User, prefix=graphene.String(required=True))
    feeds = graphene.List(ExternalFeed, prefix=graphene.String(required=True))

    @optional_user_auth
    async def resolve_everything(self, _info, session, input):
        return await es.search_everything(
            input.terms, input.topics, input.cliks, input.users, input.feeds
        )

    @optional_user_auth
    async def resolve_topics(self, _info, session, prefix=None):
        return await es.search_topics(prefix)

    @optional_user_auth
    async def resolve_cliks(self, _info, session, prefix=None):
        return await es.search_cliks(prefix)

    @optional_user_auth
    async def resolve_users(self, _info, session, prefix=None):
        return await es.search_users(prefix)

    @optional_user_auth
    async def resolve_feeds(self, _info, session, prefix=None):
        return await es.search_feeds(prefix)
