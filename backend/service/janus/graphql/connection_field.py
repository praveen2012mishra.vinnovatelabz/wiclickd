from functools import partial
import base64
import inspect
import graphene
from .utils import CustomNode
from promise import Promise
from graphql.language import ast
from graphene.relay.connection import PageInfo
from weclikd.service import grpc
from atlas.api.comment_pb2 import DiscussionFilter
from atlas.api.post_pb2 import FeedFilter


class SortEnum(graphene.Enum):
    DEFAULT = "DEFAULT"
    TRENDING = "TRENDING"
    REPORTED = "REPORTED"
    DISCUSSING = "DISCUSSING"
    NEW = "NEW"

    @staticmethod
    def to_proto(enum):
        if enum == SortEnum.DEFAULT or enum == SortEnum.NEW:
            return FeedFilter.Sort.NEW
        elif enum == SortEnum.REPORTED:
            return FeedFilter.Sort.REPORTED
        elif enum == SortEnum.TRENDING:
            return FeedFilter.Sort.TRENDING
        elif enum == SortEnum.DISCUSSING:
            return FeedFilter.Sort.DISCUSSING
        else:
            return FeedFilter.Sort.UNKNOWN


class DefaultConnectionField(graphene.ConnectionField):
    def __init__(self, connection_type, grpc_field, *args, **kwargs):
        super().__init__(connection_type, *args, **kwargs)
        self.grpc_field = grpc_field

    @classmethod
    async def resolve_connection(
        cls, connection_type, grpc_field, info, args, resolved
    ):
        if not resolved:
            return connection_type(edges=[], page_info=PageInfo(has_next_page=False))

        response = grpc.to_dict(resolved)
        if not response.get(grpc_field):
            return connection_type(edges=[], page_info=PageInfo(has_next_page=False))

        edges = []
        for each in response[grpc_field]:
            edges.append(connection_type.Edge(node=each))

        return connection_type(
            edges=edges,
            page_info=PageInfo(
                has_next_page=len(edges) == args.get("first", 0),
                end_cursor=response.get("end_cursor"),
            ),
        )

    @classmethod
    def connection_resolver(
        cls, resolver, connection_type, grpc_field, root, info, **args
    ):
        resolved = resolver(root, info, **args)
        on_resolve = partial(
            cls.resolve_connection, connection_type, grpc_field, info, args
        )
        return Promise.resolve(resolved).then(on_resolve)

    def get_resolver(self, parent_resolver):
        return partial(
            self.connection_resolver, parent_resolver, self.type, self.grpc_field
        )


class FeedConnectionField(graphene.ConnectionField):
    def __init__(self, connection_type, *args, **kwargs):
        super().__init__(
            connection_type, *args, **kwargs, sort=SortEnum(default_value="NEW")
        )

    @classmethod
    async def resolve_connection(cls, connection_type, info, args, resolved):
        response = grpc.to_dict(resolved)
        if not response.get("feed") or not response["feed"].get("posts"):
            return connection_type(edges=[], page_info=PageInfo(has_next_page=False))

        edges = []
        base_cursor = base64.b64decode(
            response["feed"]["feed_page_info"]["cursor"]
        ).decode()
        for i, post in enumerate(response["feed"]["posts"]):
            post.update(post["content_info"])
            current_cursor = base64.b64encode(f"{base_cursor}:{i}".encode()).decode()
            edges.append(connection_type.Edge(node=post, cursor=current_cursor))

        return connection_type(
            edges=edges,
            page_info=PageInfo(
                has_next_page=response["feed"]["feed_page_info"].get(
                    "has_next_page", False
                ),
                end_cursor=current_cursor,
            ),
        )

    @classmethod
    def connection_resolver(cls, resolver, connection_type, root, info, **args):
        resolved = resolver(root, info, **args)
        on_resolve = partial(cls.resolve_connection, connection_type, info, args)
        return Promise.resolve(resolved).then(on_resolve)

    def get_resolver(self, parent_resolver):
        return partial(self.connection_resolver, parent_resolver, self.type)


class CommentConnectionField(graphene.ConnectionField):
    def __init__(self, connection_type, *args, **kwargs):
        super().__init__(connection_type, *args, **kwargs)

    @classmethod
    async def resolve_connection(cls, connection_type, info, args, resolved):
        if inspect.isawaitable(resolved):
            resolved = await resolved

        if not resolved:
            return connection_type(edges=[], page_info=PageInfo(has_next_page=False))

        edges = []
        base_cursor = base64.b64decode(
            resolved["discussion_page_info"]["cursor"]
        ).decode()
        for i, comment in enumerate(resolved["comments"]):
            if i == args.get("first"):
                break
            comment.update(comment["content_info"])
            current_cursor = base64.b64encode(f"{base_cursor}:{i}".encode()).decode()
            edges.append(connection_type.Edge(node=comment, cursor=current_cursor))

        has_next_page = resolved["discussion_page_info"].get(
            "has_next_page", False
        ) or len(resolved["comments"]) > args.get("first", 0)
        return connection_type(
            edges=edges,
            page_info=PageInfo(has_next_page=has_next_page, end_cursor=current_cursor),
        )

    @classmethod
    def connection_resolver(cls, resolver, connection_type, root, info, **args):
        resolved = resolver(root, info, **args)
        return cls.resolve_connection(connection_type, info, args, resolved)

    def get_resolver(self, parent_resolver):
        return partial(self.connection_resolver, parent_resolver, self.type)

    @staticmethod
    def get_discussion_filter(info, **args):
        (
            num_replies,
            thread_length,
        ) = CommentConnectionField.get_thread_length_and_num_replies(
            info.field_asts, info.variable_values
        )
        return DiscussionFilter(
            thread_length=thread_length,
            num_replies=num_replies,
            cursor=CommentConnectionField.get_first_comments_cursor(info),
            clik_id=CommentConnectionField.get_first_clik_id(info),
            sort=args["sort"] if "sort" in args else "NEW",
        )

    @staticmethod
    def get_thread_length_and_num_replies(field_asts, variables, thread_length=0):
        max_num_replies = 0
        for field in field_asts:
            if field.name.value == "comments":
                for argument in field.arguments:
                    if argument.name.value != "first":
                        continue
                    if isinstance(argument.value, ast.Variable):
                        max_num_replies = (
                            max_num_replies
                            if variables[argument.value.name.value] < max_num_replies
                            else variables[argument.value.name.value]
                        )
                    else:
                        max_num_replies = (
                            max_num_replies
                            if int(argument.value.value) < max_num_replies
                            else int(argument.value.value)
                        )
                thread_length += 1

            if hasattr(field, "selection_set") and field.selection_set:
                (
                    num_replies,
                    thread_length,
                ) = CommentConnectionField.get_thread_length_and_num_replies(
                    field.selection_set.selections, variables, thread_length
                )

                if num_replies > max_num_replies:
                    max_num_replies = num_replies

        return max_num_replies, thread_length

    @staticmethod
    def get_first_comments_cursor(info) -> object:
        if info.field_name == "post":
            fields = info.field_asts[0]
            for field in fields.selection_set.selections:
                if field.name.value == "comments":
                    arguments = field.arguments
                    break
            else:
                return None
        else:
            arguments = info.field_asts[0].arguments

        for argument in arguments:
            if argument.name.value != "after":
                continue

            if isinstance(argument.value, ast.Variable):
                if argument.value.name.value not in info.variable_values:
                    return ""
                after, _ = (
                    base64.b64decode(
                        info.variable_values[argument.value.name.value].encode()
                    )
                    .decode()
                    .split(":")
                )
            else:
                after, _ = (
                    base64.b64decode(argument.value.value.encode()).decode().split(":")
                )

            return base64.b64encode(after.encode()).decode()

        return None

    """
    @staticmethod
    def get_first_clik_id(info) -> object:
        if info.field_name == "post":
            fields = info.field_asts[0]
            for field in fields.selection_set.selections:
                if field.name.value == "comments":
                    arguments = field.arguments
                    break
            else:
                return None
        else:
            arguments = info.field_asts[0].arguments

        for argument in arguments:
            if argument.name.value != "clik_id":
                continue

            if isinstance(argument.value, ast.Variable):
                if argument.value.name.value not in info.variable_values:
                    return None
                return CustomNode.get_id_from_global_id(
                    info.variable_values[argument.value.name.value], allow_string=True
                )
            else:
                return CustomNode.get_id_from_global_id(argument.value.value)
        return None
    """
