import graphene
import random
from .follow import (
    UserFollowSettings,
    ClikFollowSettings,
    TopicFollowSettings,
    FeedFollowSettings,
)
from .content import ContentConnection
from .connection_field import FeedConnectionField, SortEnum
from .utils import CustomNode, register_node, WeclikdDateTime, decode_after
from .auth_middleware import (
    user_auth,
    optional_user_auth,
)
from .error import WeclikdStatus
from janus.utils import normalize_url
from weclikd.service import grpc
from eros.api.eros_pb2 import (
    GetUsersFollowedRequest,
    EditProfileRequest,
    CheckUsernameRequest,
)
from artemis.api.artemis_pb2 import GetFeedsFollowedRequest
from athena.api.athena_pb2 import GetTopicsFollowedRequest
from hestia.api.hestia_pb2 import GetCliksFollowedRequest
from atlas.api.atlas_pb2 import GetFeedRequest
from atlas.api.post_pb2 import FeedFilter
from atlas.api.comment_pb2 import DiscussionFilter


class UserFields(graphene.AbstractType):
    username = graphene.String(required=True)
    full_name = graphene.String()
    profile_pic = graphene.String()
    banner_pic = graphene.String()
    description = graphene.String()


@register_node
class User(graphene.ObjectType, UserFields):
    class Meta:
        interfaces = (CustomNode,)

    created = WeclikdDateTime()
    posts = FeedConnectionField(ContentConnection)
    username = graphene.String(required=True, default_value="Unknown")
    score = graphene.Int()

    @classmethod
    async def get_node(cls, info, node_id):
        return info.context["data_loaders"]["users"].load(node_id)

    def resolve_profile_pic(self, _info):
        return normalize_url(self.get("profile_picture_id"))

    def resolve_banner_pic(self, _info):
        return normalize_url(self.get("banner_picture_id"))

    @optional_user_auth
    async def resolve_posts(self, _info, session, first=10, sort=None, after=None):
        if not self.get("id"):
            return None

        return await grpc.atlas.GetFeed(
            request=GetFeedRequest(
                user_id=CustomNode.get_id_from_global_id(self.get("id")),
                feed_filter=FeedFilter(
                    num_posts=first,
                    cursor=decode_after(after),
                    sort=SortEnum.to_proto(sort),
                ),
                discussion_filter=DiscussionFilter(
                    thread_length=0, num_replies=0, sort=SortEnum.to_proto(sort)
                ),
            ),
            context=session,
        )

    def resolve_score(self, info):
        return random.randint(1, 100)


class UserConnection(graphene.Connection):
    class Meta:
        node = User


@register_node
class MyUser(graphene.ObjectType):
    class Meta:
        interfaces = (CustomNode,)

    user = graphene.Field(User, required=True)
    settings = graphene.JSONString()
    cliks_followed = graphene.List(ClikFollowSettings)
    users_followed = graphene.List(UserFollowSettings)
    topics_followed = graphene.List(TopicFollowSettings)
    feeds_followed = graphene.List(FeedFollowSettings)

    def resolve_user(self, _info):
        return self.get("profile")

    @staticmethod
    def resolve_id(self, _info):
        if self.get("id"):
            return self.get("id")
        return self.get("profile").get("id")

    @classmethod
    async def get_node(cls, _info, node_id):
        user = await User.get_node(_info, node_id)
        return {"id": node_id, "profile": user}

    @user_auth
    async def resolve_cliks_followed(self, _info, session):
        # Get the cliks followed and the cliks joined
        response0 = await grpc.hestia.GetCliksFollowed(
            request=GetCliksFollowedRequest(), context=session
        )

        # Combine the two lists into one
        followed_cliks = grpc.to_dict(response0).get("followed_cliks", [])

        def get_clik_priority(clik_settings):
            # favorites first
            # then order by member type
            # then order by clik name in alphabetic order
            priority = 0
            member_type = clik_settings.get("member_type")
            if member_type == "SUPER_ADMIN":
                priority += 3
            elif member_type == "ADMIN":
                priority += 2
            elif member_type == "MEMBER":
                priority += 1

            follow_type = clik_settings["settings"].get("follow_type")
            if follow_type == "FAVORITE":
                priority += 10

            return chr(126 - priority) + clik_settings["clik"]["name"].lower()

        followed_cliks.sort(key=lambda x: get_clik_priority(x))
        return followed_cliks

    @user_auth
    async def resolve_users_followed(self, info, session):
        response = await grpc.eros.GetUsersFollowed(
            request=GetUsersFollowedRequest(),
            context=session,
        )
        followed_users = grpc.to_dict(response).get("followed_users", [])
        followed_users.sort(
            key=lambda x: ("a" if x["settings"]["follow_type"] == "FAVORITE" else "b")
            + x["user"]["username"].lower()
        )
        return followed_users

    @user_auth
    async def resolve_topics_followed(self, info, session):
        response = await grpc.athena.GetTopicsFollowed(
            request=GetTopicsFollowedRequest(), context=session
        )

        followed_topics = grpc.to_dict(response).get("followed_topics", [])
        if not followed_topics:
            return followed_topics

        followed_topics.sort(
            key=lambda x: ("a" if x["settings"]["follow_type"] == "FAVORITE" else "b")
            + x["topic"]["name"].lower()
        )
        return followed_topics

    @user_auth
    async def resolve_feeds_followed(self, info, session):
        response = await grpc.artemis.GetFeedsFollowed(
            request=GetFeedsFollowedRequest(), context=session
        )

        followed_feeds = grpc.to_dict(response).get("followed_feeds", [])
        if not followed_feeds:
            return followed_feeds

        followed_feeds.sort(
            key=lambda x: ("a" if x["settings"]["follow_type"] == "FAVORITE" else "b")
            + x["feed"]["name"].lower()
        )
        return followed_feeds


class EditUserInput(graphene.InputObjectType, UserFields):
    username = graphene.Field(graphene.String)


class EditUserOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    user = graphene.Field(User)


class EditUser(graphene.Mutation):
    class Arguments:
        input = EditUserInput(required=True)

    Output = EditUserOutput

    @user_auth
    async def mutate(self, _info, input, session):
        response = await grpc.eros.EditProfile(
            request=EditProfileRequest(
                username=input.username,
                full_name=input.full_name,
                banner_picture_id=int(input.banner_pic) if input.banner_pic else None,
                profile_picture_id=int(input.profile_pic)
                if input.profile_pic
                else None,
                description=input.description,
            ),
            context=session,
        )
        return {
            "status": WeclikdStatus.from_grpc(session),
            "user": grpc.to_dict(response).get("profile"),
        }


class CheckUsernameInput(graphene.InputObjectType):
    username = graphene.String()


class CheckUsernameOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class CheckUsername(graphene.Mutation):
    class Arguments:
        input = CheckUsernameInput(required=True)

    Output = CheckUsernameOutput

    @optional_user_auth
    async def mutate(self, info, input, session):
        response = await grpc.eros.CheckUsername(
            request=CheckUsernameRequest(username=input.username), context=session
        )
        grpc.to_dict(response)
        return {"status": WeclikdStatus.from_grpc(session)}
