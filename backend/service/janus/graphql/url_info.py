import graphene

from .content import Post
from .auth_middleware import user_auth, optional_user_auth
from weclikd.service import grpc
from atlas.api.atlas_pb2 import GetPostFromUrlRequest
from artemis.api.artemis_pb2 import UnfurlRequest


class UrlInfo(graphene.ObjectType):
    class UrlInfoEnum(graphene.Enum):
        ALREADY_SHARED = "ALREADY_SHARED"
        UNFURL_SUCCESS = "UNFURL_SUCCESS"
        UNFURL_FAIL = "UNFURL_FAIL"

    class Unfurl(graphene.ObjectType):
        normalized_url = graphene.String()
        title = graphene.String()
        summary = graphene.String()
        thumbnail_url = graphene.String()

    status = UrlInfoEnum(required=True)
    post = graphene.Field(Post)
    unfurl = graphene.Field(Unfurl)

    @staticmethod
    @user_auth
    async def resolve_link_info(_root, _info, session, url=None):
        if not url.startswith("https://"):
            raise ValueError("Url must start with https://")

        response = await grpc.atlas.GetPostFromUrl(
            request=GetPostFromUrlRequest(url=url), context=session
        )
        response = grpc.to_dict(response)
        if not response.get("post"):
            response = await grpc.artemis.Unfurl(
                request=UnfurlRequest(url=url), context=session
            )
            if response:
                return {
                    "status": UrlInfo.UrlInfoEnum.UNFURL_SUCCESS,
                    "unfurl": response,
                }
            else:
                return {"status": UrlInfo.UrlInfoEnum.UNFURL_FAIL}
        else:
            return {
                "status": UrlInfo.UrlInfoEnum.ALREADY_SHARED,
                "post": response["post"],
            }

    @staticmethod
    @optional_user_auth
    async def resolve_post_from_url(_root, _info, session, url=None):
        if not url.startswith("https://"):
            raise ValueError("Url must start with https://")

        response = await grpc.atlas.GetPostFromUrl(
            request=GetPostFromUrlRequest(url=url), context=session
        )
        return grpc.to_dict(response).get("post", {"id": "Post:None"})
