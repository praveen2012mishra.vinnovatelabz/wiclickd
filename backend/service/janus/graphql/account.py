import graphene
from .user import MyUser, User
from .clik import Clik
from .content import ContentUnion, Comment, Post
from .like import LikeTotals
from .utils import CustomNode, register_node, WeclikdDateTime, WeclikdBool
from .connection_field import DefaultConnectionField
from .auth_middleware import user_auth, account_auth
from .error import WeclikdStatus
from weclikd.common.filter_pb2 import DefaultFilter
from weclikd.common.follow_pb2 import FollowSettings

from eros.api.eros_pb2 import (
    CreateAccountRequest,
    GetAccountRequest,
    GetProfilesRequest,
    FollowUserRequest,
    ChangeAccountSettingsRequest,
    ChangeSubscriptionRequest,
    ChangeAccessKeyRequest,
    GetNotificationsRequest,
    GetNumUnreadNotificationsRequest,
    MarkNotificationsAsReadRequest,
    DeleteNotificationRequest,
)
from plutus.api.plutus_pb2 import (
    GetAccountBalanceRequest,
    PaySubscriptionRequest,
    PaySubscriptionResponse,
    CancelSubscriptionRequest,
    ChangePaymentInfoRequest,
    GetPaymentInfoRequest,
    GetPayoutInfoRequest,
)
from athena.api.athena_pb2 import FollowRootTopicsRequest
from hestia.api.hestia_pb2 import JoinClikRequest
from weclikd.service import grpc
from eros.api import account_pb2


class SubscriptionType(graphene.Enum):
    BASIC = "BASIC"
    GOLD = "GOLD"


class PricingIntervalType(graphene.Enum):
    MONTHLY = "MONTHLY"
    YEARLY = "YEARLY"


class AccountFields(graphene.AbstractType):
    first_name = graphene.String()
    last_name = graphene.String()
    email = graphene.String()


class EmailNotificationSettingsFields(graphene.AbstractType):
    notification_email = graphene.String()
    monthly_earnings = WeclikdBool()
    clik_notifications = WeclikdBool()
    weclikd_updates = WeclikdBool()


class EmailNotificationSettings(graphene.ObjectType, EmailNotificationSettingsFields):
    pass


class EmailNotificationSettingsInput(
    graphene.InputObjectType, EmailNotificationSettingsFields
):
    pass


class DMSettingsEnum(graphene.Enum):
    DISABLED = "DISABLED"
    EVERYONE = "EVERYONE"
    # CLIK_MEMBERS = 'CLIK_MEMBERS'


# class ProfileSettings(graphene.Enum):
#     PRIVATE = 'PRIVATE'
#     PUBLIC = 'PUBLIC'  # Display LinkedIn Profile


class PrivacySettingsFields(graphene.AbstractType):
    dm_settings = graphene.Field(DMSettingsEnum)
    # profile_settings = graphene.Field(ProfileSettings)


class PrivacySettings(graphene.ObjectType, PrivacySettingsFields):
    pass


class PrivacySettingsInput(graphene.InputObjectType, PrivacySettingsFields):
    pass


class AccountSettings(graphene.ObjectType):
    subscription = graphene.Field(SubscriptionType)
    privacy = graphene.Field(PrivacySettings, required=True)
    email_notifications = graphene.Field(EmailNotificationSettings)


class AccountSettingsInput(graphene.InputObjectType):
    privacy = graphene.Field(PrivacySettingsInput)
    email_notifications = graphene.Field(EmailNotificationSettingsInput)


class LikeTotalsHistory(graphene.ObjectType):
    version = graphene.Field(graphene.Int)
    data = graphene.Field(graphene.JSONString)


class AccountBalance(graphene.ObjectType):
    likes_monthly_total = graphene.Field(LikeTotals)
    # likes_history = graphene.Field(LikeTotalsHistory)

    @user_auth
    async def resolve_likes_monthly_total(self, info, session):
        response = await grpc.plutus.GetAccountBalance(
            request=GetAccountBalanceRequest(), context=session
        )
        return grpc.to_dict(response).get("balance", {}).get("likes_monthly_total", {})

    @user_auth
    async def resolve_likes_history(self, _info, session):
        return [
            {
                "month": "October 2020",
                "red": 60,
                "silver": 32,
                "gold": 10,
                "blue": 2,
            },
            {
                "month": "November 2020",
                "red": 60,
                "silver": 32,
                "gold": 10,
                "blue": 2,
            },
        ]


class CreditCard(graphene.ObjectType):
    last4 = graphene.String()
    brand = graphene.String()
    exp_month = graphene.Int()
    exp_year = graphene.Int()
    email = graphene.String()


class NoPaymentInfo(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class PaymentInfo(graphene.Union):
    class Meta:
        types = (CreditCard, NoPaymentInfo)


class PayoutInfo(graphene.ObjectType):
    has_stripe_account = graphene.Boolean(default_value=False)


class CommentNotification(graphene.ObjectType):
    my_content = graphene.Field(ContentUnion)
    their_comment = graphene.Field(Comment)
    post = graphene.Field(Post)
    discussion = graphene.List(Comment)

    def resolve_my_content(self, info):
        my_content_id = self.get("my_content_id")
        post_id = self.get("post_id")
        if post_id == my_content_id:
            return info.context["data_loaders"]["posts"].load(post_id)
        else:
            return info.context["data_loaders"]["comments"].load(my_content_id)

    def resolve_their_comment(self, info):
        return info.context["data_loaders"]["comments"].load(
            self.get("their_comment_id")
        )

    def resolve_post(self, info):
        return info.context["data_loaders"]["posts"].load(self.get("post_id"))

    def resolve_discussion(self, info):
        return [
            info.context["data_loaders"]["comments"].load(each)
            for each in self.get("thread_ids")
        ]


class ClikInviteRequestNotification(graphene.ObjectType):
    inviter = graphene.Field(User)
    clik = graphene.Field(Clik)

    def resolve_inviter(self, info):
        return info.context["data_loaders"]["users"].load(self.get("inviter_user_id"))

    def resolve_clik(self, info):
        return info.context["data_loaders"]["cliks"].load(self.get("clik_id"))


class ClikInviteAcceptedNotification(graphene.ObjectType):
    invitee = graphene.Field(User)
    clik = graphene.Field(Clik)

    def resolve_invitee(self, info):
        return info.context["data_loaders"]["users"].load(self.get("invitee_user_id"))

    def resolve_clik(self, info):
        return info.context["data_loaders"]["cliks"].load(self.get("clik_id"))


class ClikJoinRequestNotification(graphene.ObjectType):
    invitee = graphene.Field(User)
    clik = graphene.Field(Clik)

    def resolve_invitee(self, info):
        return info.context["data_loaders"]["users"].load(self.get("invitee_user_id"))

    def resolve_clik(self, info):
        return info.context["data_loaders"]["cliks"].load(self.get("clik_id"))


class ClikJoinAcceptedNotification(graphene.ObjectType):
    inviter = graphene.Field(User)
    clik = graphene.Field(Clik)

    def resolve_inviter(self, info):
        return info.context["data_loaders"]["users"].load(self.get("inviter_user_id"))

    def resolve_clik(self, info):
        return info.context["data_loaders"]["cliks"].load(self.get("clik_id"))


class UserInviteAcceptedNotification(graphene.ObjectType):
    invitee = graphene.Field(User)

    def resolve_invitee(self, info):
        return info.context["data_loaders"]["users"].load(self.get("invitee_user_id"))


class NotificationContent(graphene.Union):
    class Meta:
        types = (
            CommentNotification,
            ClikInviteAcceptedNotification,
            ClikInviteRequestNotification,
            ClikJoinRequestNotification,
            ClikJoinAcceptedNotification,
            UserInviteAcceptedNotification,
        )

    @classmethod
    def resolve_type(cls, instance, info):
        if instance.get("type") == "CommentNotification":
            return CommentNotification
        elif instance.get("type") == "ClikInviteRequestNotification":
            return ClikInviteRequestNotification
        elif instance.get("type") == "ClikInviteAcceptedNotification":
            return ClikInviteAcceptedNotification
        elif instance.get("type") == "ClikJoinRequestNotification":
            return ClikJoinRequestNotification
        elif instance.get("type") == "ClikJoinAcceptedNotification":
            return ClikJoinAcceptedNotification
        elif instance.get("type") == "UserInviteAcceptedNotification":
            return UserInviteAcceptedNotification
        return None


class Notification(graphene.ObjectType):
    id = graphene.ID()
    contents = graphene.Field(NotificationContent)
    created = graphene.Field(WeclikdDateTime)

    def resolve_contents(self, info):
        if "comment" in self:
            self["comment"]["type"] = "CommentNotification"
            return self["comment"]
        if "clik_invite_request" in self:
            self["clik_invite_request"]["type"] = "ClikInviteRequestNotification"
            return self["clik_invite_request"]
        if "clik_invite_accepted" in self:
            self["clik_invite_accepted"]["type"] = "ClikInviteAcceptedNotification"
            return self["clik_invite_accepted"]
        if "clik_join_request" in self:
            self["clik_join_request"]["type"] = "ClikJoinRequestNotification"
            return self["clik_join_request"]
        if "clik_join_accepted" in self:
            self["clik_join_accepted"]["type"] = "ClikJoinAcceptedNotification"
            return self["clik_join_accepted"]
        if "user_invite_accepted" in self:
            self["user_invite_accepted"]["type"] = "UserInviteAcceptedNotification"
            return self["user_invite_accepted"]

        return None


class NotificationConnection(graphene.Connection):
    class Meta:
        node = Notification


@register_node
class Account(graphene.ObjectType, AccountFields):
    class Meta:
        interfaces = (CustomNode,)

    settings = graphene.Field(AccountSettings)
    created = WeclikdDateTime()
    secret_key = graphene.String()
    my_users = graphene.List(MyUser, default_value=[])
    balance = graphene.Field(AccountBalance)
    payment_info = graphene.Field(PaymentInfo)
    payout_info = graphene.Field(PayoutInfo)
    notifications = DefaultConnectionField(NotificationConnection, "notifications")
    num_unread_notifications = graphene.Int()

    @classmethod
    @user_auth
    async def get_node(cls, info, node_id, session):
        response = await grpc.eros.GetAccount(
            request=GetAccountRequest(account_id=session.current_user_id),
            context=session,
        )
        return grpc.to_dict(response).get("account")

    @user_auth
    async def resolve_payment_info(self, _info, session):
        response = await grpc.plutus.GetPaymentInfo(
            request=GetPaymentInfoRequest(), context=session
        )
        if session.grpc_code:
            return NoPaymentInfo(status=WeclikdStatus.from_grpc(session))

        return CreditCard(
            last4=response.card.last4,
            brand=response.card.brand,
            exp_month=response.card.exp_month,
            exp_year=response.card.exp_year,
            email=response.card.email,
        )

    @user_auth
    async def resolve_payout_info(self, _info, session):
        response = await grpc.plutus.GetPayoutInfo(
            request=GetPayoutInfoRequest(), context=session
        )
        return grpc.to_dict(response)

    @user_auth
    async def resolve_notifications(self, _info, session, first=10, after=None, **args):
        return await grpc.eros.GetNotifications(
            request=GetNotificationsRequest(
                filter=DefaultFilter(num_items=first, cursor=after),
            ),
            context=session,
        )

    @user_auth
    async def resolve_num_unread_notifications(self, _info, session):
        response = await grpc.eros.GetNumUnreadNotifications(
            request=GetNumUnreadNotificationsRequest(), context=session
        )
        return response.num_unread_notifications

    @user_auth
    def resolve_balance(self, info, session):
        return {}


class AccountInput(graphene.InputObjectType, AccountFields):
    username = graphene.String()
    inviter_username = graphene.String()
    clik_invite_key = graphene.String()


class CreateAccountOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    account = graphene.Field(Account)


class CreateAccount(graphene.Mutation):
    class Arguments:
        input = AccountInput(required=True)

    Output = CreateAccountOutput

    @account_auth
    async def mutate(self, info, input, session):
        if not input.username:
            raise Exception("Username is required")

        account_response = await grpc.eros.CreateAccount(
            request=CreateAccountRequest(
                email=input.email,
                first_name=input.first_name,
                last_name=input.last_name,
                username=input.username,
                inviter_username=input.inviter_username,
                firebase_id=info.context["request"].ctx.auth["claims"]["uid"] or {},
            ),
            context=session,
        )
        if input.clik_invite_key:
            await grpc.hestia.JoinClik(
                request=JoinClikRequest(invite_key=input.clik_invite_key),
                context=session,
            )

        if input.inviter_username:
            response = await grpc.eros.GetProfiles(
                request=GetProfilesRequest(usernames=[input.inviter_username]),
                context=session,
            )
            if response.profiles and response.profiles[0]:
                await grpc.eros.FollowUser(
                    request=FollowUserRequest(
                        user_id=response.profiles[0].id,
                        settings=FollowSettings(follow_type=FollowSettings.FOLLOW),
                    ),
                    context=session,
                )

        await grpc.athena.FollowRootTopics(
            request=FollowRootTopicsRequest(), context=session
        )

        return {
            "status": WeclikdStatus.from_grpc(session),
            "account": grpc.to_dict(account_response).get("account"),
        }


class LoginOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    account = graphene.Field(Account)


class Login(graphene.Mutation):
    Output = LoginOutput

    @user_auth
    async def mutate(self, _info, session):
        response = await grpc.eros.GetAccount(
            request=GetAccountRequest(account_id=session.account_id),
            context=session,
        )
        return {
            "status": WeclikdStatus.from_grpc(session),
            "account": grpc.to_dict(response).get("account"),
        }


class ChangeSubscriptionInput(graphene.InputObjectType):
    type = graphene.Field(SubscriptionType, required=True)
    payment_id = graphene.String()
    email = graphene.String()
    pricing = graphene.Field(PricingIntervalType)


class ChangeSubscriptionOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class ChangeSubscription(graphene.Mutation):
    class Arguments:
        input = ChangeSubscriptionInput(required=True)

    Output = ChangeSubscriptionOutput

    @user_auth
    async def mutate(self, _info, input, session):
        if input.type == SubscriptionType.BASIC:
            response = await grpc.plutus.CancelSubscription(
                request=CancelSubscriptionRequest(), context=session
            )
        else:
            response = await grpc.plutus.PaySubscription(
                request=PaySubscriptionRequest(
                    type=input.type,
                    payment_id=input.payment_id,
                    email=input.email,
                    pricing=input.pricing,
                ),
                context=session,
            )

        if session.grpc_code:
            return {"status": WeclikdStatus.from_grpc(session)}

        # TODO: eros gets this asynchronously
        await grpc.eros.ChangeSubscription(
            request=ChangeSubscriptionRequest(type=input.type),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class ChangeAccountSettingsOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class ChangeAccountSettings(graphene.Mutation):
    class Arguments:
        input = AccountSettingsInput(required=True)

    Output = ChangeAccountSettingsOutput

    @user_auth
    async def mutate(self, _info, input, session):
        email_notifications = input.get("email_notifications") or {}
        privacy = input.get("privacy") or {}
        await grpc.eros.ChangeAccountSettings(
            request=ChangeAccountSettingsRequest(
                settings=account_pb2.AccountSettings(
                    privacy=account_pb2.PrivacySettings(
                        dm_settings=privacy.get("dm_settings")
                    ),
                    email_notifications=account_pb2.EmailNotificationSettings(
                        notification_email=email_notifications.get(
                            "notification_email"
                        ),
                        monthly_earnings=email_notifications.get("monthly_earnings"),
                        clik_notifications=email_notifications.get(
                            "clik_notifications"
                        ),
                        weclikd_updates=email_notifications.get("weclikd_updates"),
                    ),
                )
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class ChangePaymentInfoInput(graphene.InputObjectType):
    payment_id = graphene.Field(graphene.String)
    email = graphene.Field(graphene.String)


class ChangePaymentInfoResponse(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class ChangePaymentInfo(graphene.Mutation):
    class Arguments:
        input = ChangePaymentInfoInput(required=True)

    Output = ChangePaymentInfoResponse

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.plutus.ChangePaymentInfo(
            request=ChangePaymentInfoRequest(
                payment_id=input.payment_id or "", email=input.email or ""
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class ChangeAccessKeyInput(graphene.InputObjectType):
    purpose = graphene.String()


class ChangeAccessKeyResponse(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    access_key = graphene.String()


class ChangeAccessKey(graphene.Mutation):
    class Arguments:
        input = ChangeAccessKeyInput(required=True)

    Output = ChangeAccessKeyResponse

    @user_auth
    async def mutate(self, info, input, session):
        response = await grpc.eros.ChangeAccessKey(
            request=ChangeAccessKeyRequest(),
            context=session,
        )
        response = grpc.to_dict(response)
        return {
            "status": WeclikdStatus.from_grpc(session),
            "access_key": response.get("access_key"),
        }


class MarkNotificationsAsReadOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class MarkNotificationsAsRead(graphene.Mutation):
    # class Arguments:
    #     input = MarkNotificationsAsReadInput(required=True)

    Output = MarkNotificationsAsReadOutput

    @user_auth
    async def mutate(self, info, session):
        await grpc.eros.MarkNotificationsAsRead(
            request=MarkNotificationsAsReadRequest(),
            context=session,
        )
        return {
            "status": WeclikdStatus.from_grpc(session),
        }


class DeleteNotificationInput(graphene.InputObjectType):
    notification_id = graphene.Field(graphene.ID)


class DeleteNotificationOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    notification = graphene.Field(Notification)


class DeleteNotification(graphene.Mutation):
    class Arguments:
        input = DeleteNotificationInput(required=True)

    Output = DeleteNotificationOutput

    @user_auth
    async def mutate(self, info, input, session):
        response = await grpc.eros.DeleteNotification(
            request=DeleteNotificationRequest(
                notification_id=CustomNode.get_id_from_global_id(
                    input.notification_id, allow_string=False
                )
            ),
            context=session,
        )
        return {
            "status": WeclikdStatus.from_grpc(session),
            "notification": grpc.to_dict(response, "notification"),
        }
