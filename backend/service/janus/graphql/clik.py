import graphene

from .content import ContentConnection
from .auth_middleware import user_auth, optional_user_auth, admin_auth
from .connection_field import FeedConnectionField, DefaultConnectionField, SortEnum
from .follow import ClikMember, ClikMemberType
from .utils import (
    CustomNode,
    register_node,
    WeclikdDateTime,
    decode_after,
    get_integer_id_from_global_id,
    get_integer_ids_from_global_ids,
)
from .error import WeclikdStatus
from janus.utils import normalize_url
from atlas.api.atlas_pb2 import GetFeedRequest
from hestia.api.hestia_pb2 import (
    CreateClikRequest,
    JoinClikRequest,
    InviteToClikRequest,
    InvitedUser,
    GetMembersOfClikRequest,
    GetApplicationsToClikRequest,
    ApproveClikRequest,
    KickClikMemberRequest,
    RejectClikMemberRequest,
    PromoteClikMemberRequest,
    EditClikRequest,
    DeleteClikRequest,
    CreateClikInviteKeyRequest,
)
from eros.api.eros_pb2 import GetProfilesRequest
from weclikd.common.filter_pb2 import DefaultFilter
from atlas.api.post_pb2 import FeedFilter
from atlas.api.comment_pb2 import DiscussionFilter
from weclikd.service import grpc


class ClikFields(graphene.Interface):
    name = graphene.String(required=True)
    description = graphene.String(required=True)
    banner_pic = graphene.String()
    icon_pic = graphene.String()
    website = graphene.String()
    qualifications = graphene.List(graphene.String)
    max_members = graphene.Int()
    invite_only = graphene.Boolean(default_value=False)


class ClikMemberConnection(graphene.Connection):
    class Meta:
        node = ClikMember


class ClikUserApplication(graphene.ObjectType):
    user = graphene.Field("janus.graphql.user.User")
    known_members = graphene.List("janus.graphql.user.User")
    qualification = graphene.String()
    created = WeclikdDateTime()

    def resolve_user(self, info):
        return info.context["data_loaders"]["users"].load(self.get("user_id"))

    def resolve_known_members(self, info):
        return info.context["data_loaders"]["users"].load_many(
            self.get("known_members", [])
        )


class ClikUserApplicationConnection(graphene.Connection):
    class Meta:
        node = ClikUserApplication


@register_node
class Clik(graphene.ObjectType):
    class Meta:
        interfaces = (
            CustomNode,
            ClikFields,
        )

    created = WeclikdDateTime()
    num_followers = graphene.Int(default_value=0)
    num_members = graphene.Int(default_value=0)
    posts = FeedConnectionField(ContentConnection)
    members = DefaultConnectionField(ClikMemberConnection, "members")
    applications = DefaultConnectionField(ClikUserApplicationConnection, "applications")

    @classmethod
    async def get_node(cls, info, node_id):
        return info.context["data_loaders"]["cliks"].load(node_id)

    @optional_user_auth
    async def resolve_posts(
        self, _info, session, first=10, after=None, sort=None, **_args
    ):
        if not self.get("id"):
            return

        return await grpc.atlas.GetFeed(
            request=GetFeedRequest(
                clik_id=CustomNode.get_id_from_global_id(self.get("id")),
                feed_filter=FeedFilter(
                    num_posts=first,
                    cursor=decode_after(after),
                    sort=SortEnum.to_proto(sort),
                ),
                discussion_filter=DiscussionFilter(
                    thread_length=0, num_replies=0, sort=SortEnum.to_proto(sort)
                ),
            ),
            context=session,
        )

    @user_auth
    async def resolve_members(self, _info, session, first=10, after=None, **args):
        if not self.get("id"):
            return

        return await grpc.hestia.GetMembersOfClik(
            request=GetMembersOfClikRequest(
                clik_id=int(self.get("id")),
                filter=DefaultFilter(num_items=first, cursor=after),
            ),
            context=session,
        )

    @user_auth
    async def resolve_applications(self, _info, session, first=10, after=None, **args):
        if not self.get("id"):
            return

        return await grpc.hestia.GetApplicationsToClik(
            request=GetApplicationsToClikRequest(
                clik_id=int(self.get("id")),
                filter=DefaultFilter(num_items=first, cursor=after),
            ),
            context=session,
        )

    def resolve_name(self, _info):
        return self.get("name", "ClikNotFound")

    def resolve_description(self, _info):
        return self.get("description", "")

    def resolve_banner_pic(self, _info):
        return normalize_url(self.get("banner_picture_id"))

    def resolve_icon_pic(self, _info):
        return normalize_url(self.get("icon_picture_id"))

    def resolve_num_followers(self, _info):
        return self.get("followers", 0)

    def resolve_num_members(self, _info):
        return self.get("members", 0)


class ClikConnection(graphene.Connection):
    class Meta:
        node = Clik


class InvitedUserInput(graphene.InputObjectType):
    userid_or_email = graphene.String()
    member_type = graphene.Field(ClikMemberType)


class CreateClikInput(graphene.InputObjectType, ClikFields):
    invited_users = graphene.List(InvitedUserInput)


class CreateClikOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    member_type = graphene.Field(ClikMemberType)
    clik = graphene.Field(Clik)


class ClikInviteKey(graphene.ObjectType):
    clik = graphene.Field(Clik)
    inviter = graphene.Field("janus.graphql.user.User")
    member_type = graphene.Field(ClikMemberType)
    expired = graphene.Boolean()
    created = graphene.Field(WeclikdDateTime)

    def resolve_inviter(self, info):
        return info.context["data_loaders"]["users"].load(self.get("inviter_user_id"))

    def resolve_clik(self, info):
        return info.context["data_loaders"]["cliks"].load(self.get("clik_id"))


class CreateClikInviteKeyInput(graphene.InputObjectType):
    clik_id = graphene.ID(required=True)
    member_type = graphene.Field(ClikMemberType)


class CreateClikInviteKeyOutput(graphene.ObjectType):
    invite_key = graphene.String()
    status = graphene.Field(WeclikdStatus)


class CreateClikInviteKey(graphene.Mutation):
    class Arguments:
        input = CreateClikInviteKeyInput(required=True)

    Output = CreateClikInviteKeyOutput

    @user_auth
    async def mutate(self, _info, input, session):
        response = await grpc.hestia.CreateClikInviteKey(
            request=CreateClikInviteKeyRequest(
                clik_id=CustomNode.get_id_from_global_id(
                    input.clik_id, allow_string=True
                ),
                member_type=ClikMemberType.to_proto(input.member_type),
            ),
            context=session,
        )
        return {
            "invite_key": response.invite_key,
            "status": WeclikdStatus.from_grpc(session),
        }


class InviteToClikInput(graphene.InputObjectType):
    clik_id = graphene.ID(required=True)
    invited_users = graphene.List(InvitedUserInput)


async def transform_invited_users(invited_users, session):
    transformed = []
    for each in invited_users or []:
        if not each.userid_or_email:
            continue
        elif each.userid_or_email.find("@") > 0:
            transformed.append(
                InvitedUser(
                    email=each.userid_or_email,
                    member_type=ClikMemberType.to_proto(each.member_type),
                )
            )
        else:
            user_id = each.userid_or_email[each.userid_or_email.find(":") + 1 :]
            if not user_id.isnumeric():
                response = await grpc.eros.GetProfiles(
                    request=GetProfilesRequest(usernames=[user_id]), context=session
                )
                if response.profiles and response.profiles[0].id:
                    transformed.append(
                        InvitedUser(
                            user_id=response.profiles[0].id,
                            member_type=ClikMemberType.to_proto(each.member_type),
                        )
                    )
            else:
                transformed.append(
                    InvitedUser(
                        user_id=int(user_id),
                        member_type=ClikMemberType.to_proto(each.member_type),
                    )
                )
    return transformed


class InviteToClikOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class InviteToClik(graphene.Mutation):
    class Arguments:
        input = InviteToClikInput(required=True)

    Output = InviteToClikOutput

    @user_auth
    async def mutate(self, _info, input, session):
        if input.invited_users:
            await grpc.hestia.InviteToClik(
                request=InviteToClikRequest(
                    clik_id=CustomNode.get_id_from_global_id(
                        input.clik_id, allow_string=True
                    ),
                    invited_users=await transform_invited_users(
                        input.invited_users, session
                    ),
                ),
                context=session,
            )
        return {"status": WeclikdStatus.from_grpc(session)}


class JoinClikInput(graphene.InputObjectType):
    clik_id = graphene.ID()
    qualification = graphene.String()
    known_members = graphene.List(graphene.ID)
    invite_key = graphene.String()


class JoinClikOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    member_type = graphene.Field(ClikMemberType)


class JoinClik(graphene.Mutation):
    class Arguments:
        input = JoinClikInput(required=True)

    Output = JoinClikOutput

    @user_auth
    async def mutate(self, _info, input, session):
        response = await grpc.hestia.JoinClik(
            request=JoinClikRequest(
                clik_id=CustomNode.get_id_from_global_id(
                    input.clik_id, allow_string=True
                )
                if input.clik_id
                else 0,
                qualification=input.qualification,
                known_members=await get_integer_ids_from_global_ids(
                    input.known_members, session
                ),
                invite_key=input.invite_key,
            ),
            context=session,
        )
        response = grpc.to_dict(response)
        response["status"] = WeclikdStatus.from_grpc(session)
        return response


class CreateClik(graphene.Mutation):
    class Arguments:
        input = CreateClikInput(required=True)

    Output = CreateClikOutput

    @user_auth
    async def mutate(self, _info, input, session):
        response = await grpc.hestia.CreateClik(
            request=CreateClikRequest(
                name=input.name,
                description=input.description,
                banner_picture_id=int(input.banner_pic) if input.banner_pic else 0,
                icon_picture_id=int(input.icon_pic) if input.icon_pic else 0,
                website=input.website,
                qualifications=input.qualifications,
                max_members=input.max_members,
                invited_users=await transform_invited_users(
                    input.invited_users, session
                ),
                invite_only=input.invite_only,
            ),
            context=session,
        )
        response = grpc.to_dict(response)
        response["status"] = WeclikdStatus.from_grpc(session)
        return response


class ClikApplication(graphene.ObjectType):
    clik = graphene.Field(Clik)
    qualification = graphene.String()
    user = graphene.Field("janus.graphql.user.User")

    def resolve_user(self, info):
        return info.context["data_loaders"]["users"].load(self.get("user_id"))


class ClikApplicationConnection(graphene.Connection):
    class Meta:
        node = ClikApplication


class ApproveClikInput(graphene.InputObjectType):
    clik_id = graphene.ID(required=True)
    clik_name = graphene.String()
    approved = graphene.Boolean(required=True)
    message = graphene.String()


class ApproveClikOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class ApproveClik(graphene.Mutation):
    class Arguments:
        input = ApproveClikInput(required=True)

    Output = ApproveClikOutput

    @admin_auth
    async def mutate(self, _info, input, session):
        await grpc.hestia.ApproveClik(
            request=ApproveClikRequest(
                clik_id=CustomNode.get_id_from_global_id(
                    input.clik_id, allow_string=True
                ),
                clik_name=input.clik_name,
                approved=input.approved,
                message=input.message,
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class PromoteClikMemberInput(graphene.InputObjectType):
    clik_id = graphene.ID(required=True)
    user_id = graphene.ID(required=True)
    member_type = graphene.Field(ClikMemberType, required=True)


class PromoteClikMemberOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class PromoteClikMember(graphene.Mutation):
    class Arguments:
        input = PromoteClikMemberInput(required=True)

    Output = PromoteClikMemberOutput

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.hestia.PromoteClikMember(
            request=PromoteClikMemberRequest(
                clik_id=CustomNode.get_id_from_global_id(
                    input.clik_id, allow_string=True
                ),
                user_id=await get_integer_id_from_global_id(input.user_id, session),
                member_type=input.member_type,
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class KickClikMemberInput(graphene.InputObjectType):
    clik_id = graphene.ID(required=True)
    user_id = graphene.ID(required=True)


class KickClikMemberOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class KickClikMember(graphene.Mutation):
    class Arguments:
        input = KickClikMemberInput(required=True)

    Output = KickClikMemberOutput

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.hestia.KickClikMember(
            request=KickClikMemberRequest(
                clik_id=CustomNode.get_id_from_global_id(
                    input.clik_id, allow_string=True
                ),
                user_id=await get_integer_id_from_global_id(input.user_id, session),
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class EditClikInput(graphene.InputObjectType):
    clik_id = graphene.ID(required=True)
    description = graphene.String()
    banner_pic = graphene.String()
    icon_pic = graphene.String()
    website = graphene.String()
    qualifications = graphene.List(graphene.String)
    max_members = graphene.Int()
    invite_only = graphene.Boolean(default=True)


class EditClikOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    clik = graphene.Field(Clik)


class EditClik(graphene.Mutation):
    class Arguments:
        input = EditClikInput(required=True)

    Output = EditClikOutput

    @user_auth
    async def mutate(self, _info, input, session):
        response = await grpc.hestia.EditClik(
            request=EditClikRequest(
                clik_id=CustomNode.get_id_from_global_id(
                    input.clik_id, allow_string=True
                ),
                description=input.description,
                banner_picture_id=int(input.banner_pic or 0),
                icon_picture_id=int(input.banner_pic or 0),
                website=input.website,
                qualifications=input.qualifications,
                max_members=input.max_members,
                invite_only=input.invite_only,
            ),
            context=session,
        )
        return {
            "status": WeclikdStatus.from_grpc(session),
            "clik": grpc.to_dict(response, "clik"),
        }


class RejectClikMemberInput(graphene.InputObjectType):
    clik_id = graphene.ID(required=True)
    user_id = graphene.ID(required=True)


class RejectClikMemberOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class RejectClikMember(graphene.Mutation):
    class Arguments:
        input = RejectClikMemberInput(required=True)

    Output = RejectClikMemberOutput

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.hestia.RejectClikMember(
            request=RejectClikMemberRequest(
                clik_id=CustomNode.get_id_from_global_id(
                    input.clik_id, allow_string=True
                ),
                user_id=await get_integer_id_from_global_id(input.user_id, session),
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class DeleteClikInput(graphene.InputObjectType):
    clik_id = graphene.ID(required=True)


class DeleteClikOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    clik = graphene.Field(Clik)


class DeleteClik(graphene.Mutation):
    class Arguments:
        input = DeleteClikInput(required=True)

    Output = DeleteClikOutput

    @admin_auth
    async def mutate(self, _info, input, session):
        response = await grpc.hestia.DeleteClik(
            request=DeleteClikRequest(
                clik_id=CustomNode.get_id_from_global_id(
                    input.clik_id, allow_string=True
                )
            ),
            context=session,
        )
        return {
            "status": WeclikdStatus.from_grpc(session),
            "clik": grpc.to_dict(response, "clik"),
        }
