import graphene
from typing import List, Tuple

from .utils import CustomNode, register_node, WeclikdDateTime
from .connection_field import CommentConnectionField, SortEnum
from .like import LikeTotals
from .external_feed import ExternalFeed
from .error import WeclikdStatus, StatusType
from .auth_middleware import user_auth, optional_user_auth, admin_auth
from .score_calculator import get_comments_score, get_likes_score, get_reports_score
from janus.utils import normalize_url
from atlas.api.atlas_pb2 import (
    CreatePostRequest,
    CreateCommentRequest,
    RemoveContentRequest,
    EditPostRequest,
    SharePostRequest,
    GetCommentsRequest,
    GetDiscussionRequest,
)
from artemis.api.artemis_pb2 import DownloadImageRequest
from hestia.api.hestia_pb2 import IsClikMemberRequest
from hestia.api.clik_pb2 import ClikMember
from atlas.api.atlas_pb2 import LikeContentRequest
from atlas.api.atlas_pb2 import GetPostsRequest
from atlas.api.comment_pb2 import DiscussionFilter
from athena.api.athena_pb2 import GetTopicsRequest
from weclikd.service import grpc


class LikeType(graphene.Enum):
    NONE = "NONE"
    RED = "RED"
    SILVER = "SILVER"
    GOLD = "GOLD"
    DIAMOND = "DIAMOND"


class AdminStats(graphene.ObjectType):
    likes = graphene.Field(LikeTotals)
    num_likes = graphene.Int()
    num_reports = graphene.Int()
    num_comments = graphene.Int()
    sharer = graphene.Field("janus.graphql.user.User")

    async def resolve_sharer(self, info):
        if self.get("sharer_id"):
            return info.context["data_loaders"]["users"].load(self["sharer_id"])


class Content(graphene.Interface):
    id = graphene.ID(required=True)
    author = graphene.Field("janus.graphql.user.User")
    text = graphene.String()
    likes_score = graphene.Int()
    comments_score = graphene.Int()
    reports_score = graphene.Int()
    comments = CommentConnectionField(
        "janus.graphql.content.CommentConnection",
        clik_ids=graphene.Argument(graphene.List(graphene.ID)),
        user_ids=graphene.Argument(graphene.List(graphene.ID)),
        sort=graphene.Argument(SortEnum, default_value="NEW"),
    )
    created = WeclikdDateTime()
    user_like_type = graphene.Field(LikeType)
    admin_stats = graphene.Field(AdminStats)

    @staticmethod
    def _resolve_helper(obj, field_name, default_value=None):
        return obj.get("content_info", {}).get(field_name, default_value)

    def resolve_id(self, info):
        return Content._resolve_helper(self, "id")

    async def resolve_author(self, info):
        user_id = Content._resolve_helper(self, "user_id")
        if user_id and not self.get("url"):
            return info.context["data_loaders"]["users"].load(user_id)

    def resolve_text(self, info):
        return Content._resolve_helper(self, "text")

    @optional_user_auth
    async def resolve_comments(
        self, info, session, clik_ids=None, user_ids=None, sort=None, **kwargs
    ):
        (
            num_replies,
            thread_length,
        ) = CommentConnectionField.get_thread_length_and_num_replies(
            info.field_asts, info.variable_values
        )

        response = await grpc.atlas.GetDiscussion(
            request=GetDiscussionRequest(
                content_id=CustomNode.get_id_from_global_id(
                    Content._resolve_helper(self, "id")
                ),
                discussion_filter=DiscussionFilter(
                    thread_length=thread_length,
                    num_replies=num_replies,
                    cursor=CommentConnectionField.get_first_comments_cursor(info),
                    clik_ids=[
                        CustomNode.get_id_from_global_id(each, allow_string=True)
                        for each in clik_ids or []
                    ],
                    sort="NEW",
                ),
            ),
            context=session,
        )
        return grpc.to_dict(response)["discussion"]

    def resolve_likes_score(self, info):
        return get_likes_score(Content._resolve_helper(self, "likes", {}))

    def resolve_comments_score(self, info):
        return get_comments_score(Content._resolve_helper(self, "num_comments", 0))

    def resolve_reports_score(self, info):
        return get_reports_score(Content._resolve_helper(self, "num_reports", 0))

    def resolve_created(self, info):
        return Content._resolve_helper(self, "created")

    @optional_user_auth
    def resolve_user_like_type(self, info, session):
        if session.current_user_id:
            return Content._resolve_helper(self, "user_like_type")
        else:
            return None

    @classmethod
    def resolve_type(cls, instance, info):
        content_type = instance.get("type", "default").lower()
        if content_type == "post":
            return Post
        elif content_type == "comment":
            return Comment
        else:
            return None

    @admin_auth
    def resolve_admin_stats(self, info, session):
        likes = Content._resolve_helper(self, "likes")
        return {
            "likes": likes,
            "num_reports": Content._resolve_helper(self, "num_reports", 0),
            "num_comments": Content._resolve_helper(self, "num_comments", 0),
            "num_likes": sum([each for each in likes.values()]),
            "sharer_id": self.get("sharer_id"),
        }


class ContentInput(graphene.AbstractType):
    text = graphene.String()


class PostInput(graphene.InputObjectType, ContentInput):
    title = graphene.String(required=True)
    summary = graphene.Field(graphene.String, required=True)
    thumbnail_pic = graphene.String()
    thumbnail_pic_url = graphene.String()
    pictures = graphene.List(graphene.String)
    topics = graphene.List(graphene.String)
    cliks = graphene.List(graphene.String)
    link = graphene.String()


class CommentInput(graphene.InputObjectType, ContentInput):
    parent_content_id = graphene.Field(graphene.ID, required=True)
    clik = graphene.Field(graphene.String)


class LikeInput(graphene.InputObjectType):
    content_id = graphene.Field(graphene.ID, required=True)
    type = graphene.Field(LikeType, required=True)


# Output


class ReasonsShared(graphene.ObjectType):
    users = graphene.List("janus.graphql.user.User")
    cliks = graphene.List("janus.graphql.clik.Clik")

    def resolve_users(self, info):
        return [
            info.context["data_loaders"]["users"].load(user_id)
            for user_id in self.get("user_ids") or []
        ]

    def resolve_cliks(self, info):
        return [
            info.context["data_loaders"]["cliks"].load(clik_id)
            for clik_id in self.get("clik_ids") or []
        ]


@register_node
class Post(graphene.ObjectType):
    class Meta:
        interfaces = (
            Content,
            CustomNode,
        )

    title = graphene.String()
    summary = graphene.String()
    thumbnail_pic = graphene.String()
    pictures = graphene.List(graphene.String)
    topics = graphene.List(graphene.String)
    cliks = graphene.List(graphene.String)
    sharer = graphene.Field("janus.graphql.user.User")
    link = graphene.String()
    external_feed = graphene.Field(ExternalFeed)
    reasons_shared = graphene.Field(ReasonsShared)

    def resolve_thumbnail_pic(self, _info):
        return normalize_url(self.get("thumbnail_picture_id"))

    def resolve_pictures(self, _info):
        return [normalize_url(each) for each in self.get("picture_ids", [])]

    def resolve_link(self, _info):
        return self.get("url")

    @classmethod
    @optional_user_auth
    async def get_node(cls, info, node_id, session):
        response = await grpc.atlas.GetPosts(
            request=GetPostsRequest(
                ids=[int(node_id)],
            ),
            context=session,
        )
        posts = grpc.to_dict(response)["posts"]
        if len(posts) != 1:
            raise Exception(f"Post {node_id}: does not exist")
        return posts[0]


class EditCommentInput(graphene.InputObjectType):
    content_id = graphene.ID(required=True)
    text = graphene.String()


class EditCommentOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class EditComment(graphene.Mutation):
    class Arguments:
        input = EditCommentInput(required=True)

    Output = EditCommentOutput

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.atlas.EditComment(
            request=EditPostRequest(
                content_id=CustomNode.get_id_from_global_id(input.content_id),
                text=input.text,
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class ContentConnection(graphene.Connection):
    class Meta:
        node = Post


@register_node
class Comment(graphene.ObjectType):
    class Meta:
        interfaces = (Content, CustomNode)

    parent_content_id = graphene.Field(graphene.ID, required=True)
    clik = graphene.Field(graphene.String)

    @classmethod
    @optional_user_auth
    async def get_node(cls, info, node_id, session):
        response = await grpc.atlas.GetComments(
            request=GetCommentsRequest(
                ids=[int(node_id)],
            ),
            context=session,
        )
        comments = grpc.to_dict(response).get("comments", [])
        if len(comments) != 1:
            raise Exception(f"Comment {node_id}: does not exist")
        return comments[0]


class CommentConnection(graphene.Connection):
    class Meta:
        node = Comment


class CreatePostOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    post = graphene.Field(Post)


class CreatePost(graphene.Mutation):
    class Arguments:
        input = PostInput(required=True)

    Output = CreatePostOutput

    @staticmethod
    async def validate_topics(
        topics: List[str], session
    ) -> Tuple[List[str], List[int]]:
        response = await grpc.athena.GetTopics(
            request=GetTopicsRequest(topic_names=topics, retrieve_parents=True),
            context=session,
        )
        validated_topics = grpc.to_dict(response, "topics") or []
        validated_topics.sort(key=lambda x: len(x.get("parents") or []), reverse=True)
        topic_ids = [int(topic["id"]) for topic in validated_topics if topic.get("id")]
        if topic_ids:
            banner_pic = int(validated_topics[0].get("banner_picture_id", 0))
            validated_topics = [
                each["name"] for each in validated_topics if each.get("id")
            ]
        else:
            banner_pic = 0
            validated_topics = []
        return validated_topics, topic_ids, banner_pic

    @staticmethod
    async def validate_cliks(cliks: List[str], session) -> List[str]:
        if not cliks:
            return []

        response = await grpc.hestia.IsClikMember(
            request=IsClikMemberRequest(user_id=session.current_user_id, cliks=cliks),
            context=session,
        )
        validated_cliks = []
        for clik in cliks:
            if response.clik_members[clik] != ClikMember.UNKNOWN:
                validated_cliks.append(clik)
        return validated_cliks

    @user_auth
    async def mutate(self, _info, input, session):
        if input.link and not input.link.startswith("https://"):
            raise ValueError("Url must start with https://")

        thumbnail_picture_id = 0
        if input.thumbnail_pic_url:
            response = await grpc.artemis.DownloadImage(
                request=DownloadImageRequest(url=input.thumbnail_pic_url),
                context=session,
            )
            thumbnail_picture_id = response.image_id

        (
            validated_topics,
            topic_ids,
            topic_banner_pic,
        ) = await CreatePost.validate_topics(input.topics, session)

        # Check user has permissions to comment to clik
        validated_cliks = await CreatePost.validate_cliks(input.cliks, session)

        response = await grpc.atlas.CreatePost(
            request=CreatePostRequest(
                title=input.title,
                summary=input.summary,
                text=input.text,
                url=input.link,
                cliks=validated_cliks,
                topics=validated_topics,
                topic_ids=topic_ids,
                thumbnail_picture_id=thumbnail_picture_id or topic_banner_pic,
                picture_ids=[int(each) for each in input.pictures or []],
            ),
            context=session,
        )
        return {
            "status": WeclikdStatus.from_grpc(session),
            "post": grpc.to_dict(response).get("post", {}),
        }


class CreateCommentOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    comment = graphene.Field(Comment)


class CreateComment(graphene.Mutation):
    class Arguments:
        input = CommentInput(required=True)

    Output = CreateCommentOutput

    @user_auth
    async def mutate(self, _info, input, session):
        # Check user has permissions to comment to clik
        if input.clik:
            validated_cliks = await CreatePost.validate_cliks([input.clik], session)
            if not validated_cliks:
                return {
                    "status": {
                        "success": False,
                        "status": StatusType.PERMISSION_DENIED,
                        "user_msg": f"Not a member of clik {input.clik}",
                    }
                }

        response = await grpc.atlas.CreateComment(
            request=CreateCommentRequest(
                text=input.text,
                parent_content_id=CustomNode.get_id_from_global_id(
                    input.parent_content_id
                ),
                clik=input.clik,
            ),
            context=session,
        )
        return {
            "status": WeclikdStatus.from_grpc(session),
            "comment": grpc.to_dict(response).get("comment"),
        }


class LikeContentOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class LikeContent(graphene.Mutation):
    class Arguments:
        input = LikeInput(required=True)

    Output = LikeContentOutput

    @user_auth
    async def mutate(self, _info, input, session):
        response = await grpc.atlas.LikeContent(
            request=LikeContentRequest(
                type=input.type,
                content_id=CustomNode.get_id_from_global_id(input.content_id),
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class DeleteInput(graphene.InputObjectType):
    content_id = graphene.ID(required=True)


class DeleteContentOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class DeleteContent(graphene.Mutation):
    class Arguments:
        input = DeleteInput(required=True)

    Output = DeleteContentOutput

    @user_auth
    async def mutate(self, _info, input, session):
        await grpc.atlas.RemoveContent(
            request=RemoveContentRequest(
                content_id=CustomNode.get_id_from_global_id(input.content_id),
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class EditPostInput(graphene.InputObjectType):
    content_id = graphene.ID(required=True)
    topics = graphene.List(graphene.String)
    title = graphene.String()
    summary = graphene.String()
    text = graphene.String()
    thumbnail_pic = graphene.String()
    pictures = graphene.List(graphene.String)
    link = graphene.String()


class EditPostOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class EditPost(graphene.Mutation):
    class Arguments:
        input = EditPostInput(required=True)

    Output = EditPostOutput

    @admin_auth
    async def mutate(self, _info, input, session):
        if input.link and not input.link.startswith("https://"):
            raise ValueError("Url must start with https://")

        # work around protobuf design where there is no distinction between not set and default value
        if input.pictures == []:
            input.pictures = [1]
        if input.thumbnail_pic == "":
            input.thumbnail_pic = "1"

        (
            validated_topics,
            topic_ids,
            _,
        ) = await CreatePost.validate_topics(input.topics, session)

        await grpc.atlas.EditPost(
            request=EditPostRequest(
                content_id=CustomNode.get_id_from_global_id(input.content_id),
                topics=validated_topics,
                topic_ids=topic_ids,
                title=input.title,
                summary=input.summary,
                text=input.text,
                thumbnail_picture_id=int(input.thumbnail_pic or 0),
                picture_ids=[int(each) for each in input.pictures or []],
                url=input.link,
            ),
            context=session,
        )
        return {"status": WeclikdStatus.from_grpc(session)}


class SharePostInput(graphene.InputObjectType):
    content_id = graphene.ID(required=True)
    topics = graphene.List(graphene.String)
    cliks = graphene.List(graphene.String)


class SharePostOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    post = graphene.Field(Post)


class SharePost(graphene.Mutation):
    class Arguments:
        input = SharePostInput(required=True)

    Output = SharePostOutput

    @admin_auth
    async def mutate(self, _info, input, session):
        (
            validated_topics,
            topic_ids,
            _,
        ) = await CreatePost.validate_topics(input.topics, session)

        # Check user has permissions to comment to clik
        validated_cliks = await CreatePost.validate_cliks(input.cliks, session)

        response = await grpc.atlas.SharePost(
            request=SharePostRequest(
                content_id=CustomNode.get_id_from_global_id(input.content_id),
                topics=validated_topics,
                topic_ids=topic_ids,
                cliks=validated_cliks,
            ),
            context=session,
        )
        return {
            "status": WeclikdStatus.from_grpc(session),
            "post": grpc.to_dict(response, "post"),
        }


class ContentUnion(graphene.Union):
    class Meta:
        types = (Post, Comment)

    @classmethod
    def resolve_type(cls, instance, info):
        if instance.get("content_info", {}).get("type") == "POST":
            return Post
        else:
            return Comment
