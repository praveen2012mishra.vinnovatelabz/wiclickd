import graphene

from weclikd.utils import weclikd_id
from weclikd.service import grpc
from artemis.api.artemis_pb2 import (
    AddExternalFeedRequest,
    EditExternalFeedRequest,
    RemoveExternalFeedRequest,
    GetFeedsRequest,
    TestExternalFeedRequest,
)
from atlas.api.atlas_pb2 import GetFeedRequest
from atlas.api.post_pb2 import FeedFilter
from atlas.api.comment_pb2 import DiscussionFilter
from artemis.api import external_feed_pb2
from athena.api.athena_pb2 import GetTopicsRequest
from janus.graphql.error import WeclikdStatus, StatusType
from janus.graphql.auth_middleware import optional_user_auth, user_auth
from janus.graphql.auth_middleware import admin_auth
from janus.graphql.utils import WeclikdDateTime, register_node, CustomNode, decode_after
from janus.utils import normalize_url
from janus.graphql.connection_field import FeedConnectionField, SortEnum


@register_node
class ExternalFeed(graphene.ObjectType):
    class Meta:
        interfaces = (CustomNode,)

    name = graphene.String()
    url = graphene.String()
    followers = graphene.Int()
    website = graphene.String()
    base_topic = graphene.String()
    topic = graphene.Field("janus.graphql.topic.Topic")
    icon_url = graphene.String()
    posts = FeedConnectionField("janus.graphql.content.ContentConnection")
    created = graphene.Field(WeclikdDateTime)

    @staticmethod
    async def normalize_id(feed_id: int, session):
        feed_id = CustomNode.get_id_from_global_id(
            feed_id, allow_string=True, hash=False
        )
        if isinstance(feed_id, int):
            return feed_id
        else:
            feed_names = [feed_id]

        response = await grpc.artemis.GetFeeds(
            request=GetFeedsRequest(feed_ids=[], feed_names=feed_names),
            context=session,
        )
        if not response.feeds:
            raise Exception("Feed does not exist")
        return response.feeds[0].id

    @classmethod
    @optional_user_auth
    async def get_node(cls, info, node_id, session):
        if not node_id.isnumeric():
            feed_names = [node_id]
            feed_ids = []
        else:
            feed_names = []
            feed_ids = [int(node_id)]

        response = await grpc.artemis.GetFeeds(
            request=GetFeedsRequest(feed_ids=feed_ids, feed_names=feed_names),
            context=session,
        )
        if not response.feeds:
            return {"id": "ExternalFeed:None"}
        return grpc.to_dict(response.feeds[0])

    def resolve_icon_url(self, info):
        return normalize_url(self.get("icon_url"))

    def resolve_topic(self, info):
        return info.context["data_loaders"]["topics"].load(
            str(weclikd_id.string_to_id(self.get("base_topic")))
        )

    @optional_user_auth
    async def resolve_posts(
        self, _info, session, first=10, after=None, sort=None, **_args
    ):
        if not self.get("id"):
            return []
        return await grpc.atlas.GetFeed(
            request=GetFeedRequest(
                feed_id=await ExternalFeed.normalize_id(self.get("id"), session),
                feed_filter=FeedFilter(
                    num_posts=first,
                    cursor=decode_after(after),
                    sort=SortEnum.to_proto(sort),
                ),
                discussion_filter=DiscussionFilter(
                    thread_length=0, num_replies=0, sort=SortEnum.to_proto(sort)
                ),
            ),
            context=session,
        )


class SummarySource(graphene.Enum):
    UNKNOWN_SOURCE = "UNKNOWN_SOURCE"
    RSS_DESCRIPTION = "RSS_DESCRIPTION"
    UNFURL = "UNFURL"


class CreateExternalFeedInput(graphene.InputObjectType):
    name = graphene.String()
    url = graphene.String()
    website = graphene.String()
    base_topic = graphene.String()
    icon_url = graphene.String()
    summary_source = graphene.Field(SummarySource)


class CreateExternalFeedOutput(graphene.ObjectType):
    external_feed = graphene.Field(ExternalFeed)
    status = graphene.Field(WeclikdStatus)


class CreateExternalFeed(graphene.Mutation):
    class Arguments:
        input = CreateExternalFeedInput(required=True)

    Output = CreateExternalFeedOutput

    @user_auth
    async def mutate(self, _info, input, session):
        if input.base_topic:
            response = await grpc.athena.GetTopics(
                request=GetTopicsRequest(topic_names=[input.base_topic]),
                context=session,
            )

            if not response.topics or not response.topics[0].id:
                return {
                    "status": WeclikdStatus.create(
                        status=StatusType.INVALID_ARGUMENT,
                        custom_status="INVALID_TOPIC",
                        user_msg=f"Topic {input.base_topic} does not exist.",
                    )
                }
            base_topic = response.topics[0].name
        else:
            base_topic = ""

        response = await grpc.artemis.AddExternalFeed(
            request=AddExternalFeedRequest(
                name=input.name,
                url=input.url,
                website=input.website,
                type=external_feed_pb2.ExternalFeedSource.Type.RSS,
                base_topic=base_topic,
                icon_url=input.icon_url,
                summary_source=external_feed_pb2.ExternalFeedSource.SummarySource.Value(
                    input.summary_source
                ),
            ),
            context=session,
        )
        response = grpc.to_dict(response)
        return {
            "status": WeclikdStatus.from_grpc(session),
            "external_feed": response.get("feed"),
        }


class EditExternalFeedInput(graphene.InputObjectType):
    feed_id = graphene.ID(required=True)
    name = graphene.String()
    website = graphene.String()
    base_topic = graphene.String()
    icon_url = graphene.String()
    summary_source = graphene.Field(SummarySource)


class EditExternalFeedOutput(graphene.ObjectType):
    external_feed = graphene.Field(ExternalFeed)
    status = graphene.Field(WeclikdStatus)


class EditExternalFeed(graphene.Mutation):
    class Arguments:
        input = EditExternalFeedInput(required=True)

    Output = EditExternalFeedOutput

    @user_auth
    async def mutate(self, _info, input, session):
        if input.base_topic:
            response = await grpc.athena.GetTopics(
                request=GetTopicsRequest(topic_names=[input.base_topic]),
                context=session,
            )

            if not response.topics or not response.topics[0].id:
                return {
                    "status": WeclikdStatus.create(
                        status=StatusType.INVALID_ARGUMENT,
                        custom_status="INVALID_TOPIC",
                        user_msg=f"Topic {input.base_topic} does not exist.",
                    )
                }

        feed_id = await ExternalFeed.normalize_id(input.feed_id, session)
        response = await grpc.artemis.EditExternalFeed(
            request=EditExternalFeedRequest(
                feed_id=feed_id,
                name=input.name,
                website=input.website,
                type=external_feed_pb2.ExternalFeedSource.Type.RSS,
                base_topic=input.base_topic,
                icon_url=input.icon_url,
                summary_source=external_feed_pb2.ExternalFeedSource.SummarySource.Value(
                    input.summary_source
                ),
            ),
            context=session,
        )
        response = grpc.to_dict(response)
        return {
            "status": WeclikdStatus.from_grpc(session),
            "external_feed": response.get("feed"),
        }


class TestExternalFeedInput(graphene.InputObjectType):
    url = graphene.String()


class TestExternalFeedOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    external_feed = graphene.Field(ExternalFeed)


class TestExternalFeed(graphene.Mutation):
    class Arguments:
        input = TestExternalFeedInput(required=True)

    Output = TestExternalFeedOutput

    @user_auth
    async def mutate(self, _info, input, session):
        response = await grpc.artemis.TestExternalFeed(
            request=TestExternalFeedRequest(url=input.url),
            context=session,
        )
        response = grpc.to_dict(response)
        return {
            "status": WeclikdStatus.from_grpc(session),
            "external_feed": response.get("feed"),
        }


class DeleteExternalFeedInput(graphene.InputObjectType):
    feed_id = graphene.ID(required=True)


class DeleteExternalFeedOutput(graphene.ObjectType):
    external_feed = graphene.Field(ExternalFeed)
    status = graphene.Field(WeclikdStatus)


class DeleteExternalFeed(graphene.Mutation):
    class Arguments:
        input = DeleteExternalFeedInput(required=True)

    Output = DeleteExternalFeedOutput

    @admin_auth
    async def mutate(self, _info, input, session):
        response = await grpc.artemis.RemoveExternalFeed(
            request=RemoveExternalFeedRequest(
                feed_id=await ExternalFeed.normalize_id(input.feed_id, session)
            ),
            context=session,
        )
        response = grpc.to_dict(response)
        return {
            "status": WeclikdStatus.from_grpc(session),
            "external_feed": response.get("feed"),
        }


class ExternalFeedConnection(graphene.Connection):
    class Meta:
        node = ExternalFeed
