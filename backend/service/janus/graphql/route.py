from sanic import Blueprint
from sanic import response as sanic_response
import logging
import time
from graphql.execution.executors.asyncio import AsyncioExecutor
from janus.data_loader import (
    content_data_loader,
    user_data_loader,
    clik_data_loader,
    topic_data_loader,
)
from janus.graphql.schema import schema
from janus.graphql.graphqlview import GraphQLView
from janus.graphql.auth_middleware import setup_request_auth
from weclikd.utils.compression import compress
from pyinstrument import Profiler

graphql_api = Blueprint("graphql", url_prefix="/graphql")
graphql_handler = None


def init_graphql(loop):
    global handler
    handler = GraphQLView.as_view(
        schema=schema,
        middleware=[graphql_middleware],
        context_cb=get_graphql_context,
        executor=AsyncioExecutor(loop),
    )


@graphql_api.route("/", methods=["POST", "OPTIONS"])
@compress.compress()
async def graphql_route(request):
    return await handler(request)


@graphql_api.route("/<graphql_query>", methods=["POST", "OPTIONS"])
@compress.compress()
async def graphql_route(request, graphql_query):
    return await handler(request)


def graphql_middleware(next, root, info, **args):
    if root is None:
        if info.operation.name is None:
            raise ValueError("All GraphQL queries must have a name")
        info.context[
            "request"
        ].ctx.graphql_query_name = f"{info.parent_type}:{info.operation.name.value}"
    return next(root, info, **args)


def get_graphql_context(request, previous_context):
    context = previous_context.copy() if previous_context else {}
    data_loaders = {
        "users": user_data_loader.UserLoader(context),
        "cliks": clik_data_loader.ClikLoader(context),
        "posts": content_data_loader.PostLoader(context),
        "comments": content_data_loader.CommentLoader(context),
        "topics": topic_data_loader.TopicLoader(context),
    }

    context.update({"data_loaders": data_loaders, "request": request})
    return context


@graphql_api.middleware("request")
async def request_middleware(request, *_args, **_kwarg):
    if request.args.get("profiler") is not None:
        logging.info("Profiling Request Start")
        request.ctx.profiler = Profiler()
        request.ctx.profiler.start()
    request.ctx.start_time = time.time()
    return await setup_request_auth(request)


@graphql_api.middleware("response")
async def response_middleware(request, response, *_args, **_kwarg):
    profiler_arg = request.args.get("profiler")
    if profiler_arg is not None:
        logging.info("Profiling Request Stop")
        request.ctx.profiler.stop()
        if profiler_arg == "text":
            output = request.ctx.profiler.output_text(show_all=True)
            return sanic_response.text(output)
        else:
            output_html = request.ctx.profiler.output_html()
            return sanic_response.html(output_html)

    if hasattr(request.ctx, "graphql_query_name"):
        total_time = round((time.time() - request.ctx.start_time) * 1000, 1)
        logging.info(f"GraphQL {request.ctx.graphql_query_name} {total_time} ms")
