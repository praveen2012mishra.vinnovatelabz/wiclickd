import math
from typing import Dict


def get_comments_score(num_comments: int) -> int:
    score = int(math.log(num_comments + 1) * 25)
    if score > 100:
        return 100
    else:
        return score


def get_likes_score(likes: Dict) -> int:
    score = likes.get("red", 0)
    score += likes.get("silver", 0) * 2
    score += likes.get("gold", 0) * 10
    score += likes.get("diamond", 0) * 50
    if score > 100:
        return 100
    else:
        return score


def get_reports_score(num_reports: int) -> int:
    score = num_reports * 10
    if score > 100:
        return 100
    else:
        return score
