import graphene

from .error import WeclikdStatus
from .content import ContentConnection, Post
from .utils import CustomNode, register_node, WeclikdDateTime, decode_after
from .connection_field import FeedConnectionField, SortEnum
from .auth_middleware import optional_user_auth, admin_auth, user_auth
from janus.utils import normalize_url
from atlas.api.atlas_pb2 import GetFeedRequest
from athena.api import topic_pb2
from athena.api.athena_pb2 import (
    EditTopicRequest,
    ProposeTopicRequest,
    DeleteTopicRequest,
)
from atlas.api.post_pb2 import FeedFilter
from atlas.api.comment_pb2 import DiscussionFilter
from weclikd.service import grpc


class TopicKeyword(graphene.ObjectType):
    name = graphene.String()
    weight = graphene.Int()


@register_node
class Topic(graphene.ObjectType):
    class Meta:
        interfaces = (CustomNode,)

    created = WeclikdDateTime()
    name = graphene.String()
    parents = graphene.List(graphene.String)
    description = graphene.String()
    banner_pic = graphene.String()
    num_followers = graphene.Int(default_value=0)
    posts = FeedConnectionField(ContentConnection)
    aliases = graphene.List(graphene.String)
    keywords = graphene.List(TopicKeyword)
    children = graphene.List("janus.graphql.topic.Topic")

    @classmethod
    def get_node(cls, info, node_id):
        return info.context["data_loaders"]["topics"].load(node_id)

    @classmethod
    async def get_topic_from_global_id(cls, info, global_id):
        node_id = CustomNode.get_id_from_global_id(global_id)
        return await cls.get_node(info, node_id)

    @optional_user_auth
    async def resolve_posts(
        self, _info, session, first=10, after=None, sort=None, **_args
    ):
        if not self.get("id"):
            return []
        return await grpc.atlas.GetFeed(
            request=GetFeedRequest(
                topic_id=CustomNode.get_id_from_global_id(self.get("id")),
                feed_filter=FeedFilter(
                    num_posts=first,
                    cursor=decode_after(after),
                    sort=SortEnum.to_proto(sort),
                ),
                discussion_filter=DiscussionFilter(
                    thread_length=0, num_replies=0, sort=SortEnum.to_proto(sort)
                ),
            ),
            context=session,
        )

    def resolve_banner_pic(self, _info):
        return normalize_url(self.get("banner_picture_id"))

    def resolve_num_followers(self, _info):
        return self.get("followers", 0)

    def resolve_children(self, info):
        return [
            info.context["data_loaders"]["topics"].load(child_name)
            for child_name in self.get("children") or []
        ]


class TopicConnection(graphene.Connection):
    class Meta:
        node = Topic


class ApproveTopicInput(graphene.InputObjectType):
    topic_id = graphene.ID(required=True)
    topic_name = graphene.String()
    approved = graphene.Boolean(required=True)


class ApproveTopicOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class ApproveTopic(graphene.Mutation):
    class Arguments:
        input = ApproveTopicInput(required=True)

    Output = ApproveTopicOutput

    @admin_auth
    async def mutate(self, _info, input, session):
        return {"status": {"success": True}}


class TopicKeywordInput(graphene.InputObjectType):
    name = graphene.String()
    weight = graphene.Int()


class EditTopicInput(graphene.InputObjectType):
    topic_id = graphene.ID(required=True)
    name = graphene.String()
    description = graphene.String()
    banner_pic = graphene.String()
    parent = graphene.String()
    keywords = graphene.List(TopicKeywordInput)
    aliases = graphene.List(graphene.String)


class EditTopicOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    topic = graphene.Field(Topic)


class EditTopic(graphene.Mutation):
    class Arguments:
        input = EditTopicInput(required=True)

    Output = EditTopicOutput

    @admin_auth
    async def mutate(self, _info, input, session):
        response = await grpc.athena.EditTopic(
            request=EditTopicRequest(
                topic_id=CustomNode.get_id_from_global_id(
                    input.topic_id, allow_string=False
                ),
                name=input.name,
                description=input.description,
                banner_picture_id=int(input.banner_pic or 0),
                parent=input.parent if input.parent != "" else "NONE",
                aliases=input.aliases,
                keywords=[
                    topic_pb2.Topic.Keyword(name=each.name, weight=each.weight)
                    for each in input.keywords or []
                ],
            ),
            context=session,
        )
        response = grpc.to_dict(response)
        return {
            "status": WeclikdStatus.from_grpc(session),
            "topic": response.get("topic"),
        }


class CreateTopicInput(graphene.InputObjectType):
    name = graphene.String(required=True)
    parent = graphene.String()
    description = graphene.String()
    banner_pic = graphene.String()
    aliases = graphene.List(graphene.String)
    keywords = graphene.List(TopicKeywordInput)


class CreateTopicOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)
    topic = graphene.Field(Topic)


class CreateTopic(graphene.Mutation):
    class Arguments:
        input = CreateTopicInput(required=True)

    Output = CreateTopicOutput

    @user_auth
    async def mutate(self, _info, input, session):
        response = await grpc.athena.ProposeTopic(
            request=ProposeTopicRequest(
                name=input.name,
                parent=input.parent,
                description=input.description,
                banner_picture_id=int(input.banner_pic or 0),
                aliases=input.aliases or [],
                keywords=[
                    topic_pb2.Topic.Keyword(name=each.name, weight=each.weight)
                    for each in input.keywords or []
                ],
            ),
            context=session,
        )
        response = grpc.to_dict(response)
        return {
            "status": WeclikdStatus.from_grpc(session),
            "topic": response.get("topic"),
        }


class DeleteTopicInput(graphene.InputObjectType):
    topic_id = graphene.ID(required=True)


class DeleteTopicOutput(graphene.ObjectType):
    status = graphene.Field(WeclikdStatus)


class DeleteTopic(graphene.Mutation):
    class Arguments:
        input = DeleteTopicInput(required=True)

    Output = DeleteTopicOutput

    @admin_auth
    async def mutate(self, info, input, session):
        await grpc.athena.DeleteTopic(
            request=DeleteTopicRequest(
                topic_id=CustomNode.get_id_from_global_id(input.topic_id)
            ),
            context=session,
        )
        return {
            "status": WeclikdStatus.from_grpc(session),
        }
