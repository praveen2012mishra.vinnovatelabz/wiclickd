import logging
from .base_data_loader import BaseDataLoader
from eros.api.eros_pb2 import GetProfilesRequest
from weclikd.service import grpc


class UserLoader(BaseDataLoader):

    id_field_names = ["id", "username"]

    async def batch_load_from_sql(self, keys):
        logging.info("Batch Users: {}".format(keys))
        request = GetProfilesRequest()
        for each in keys:
            if isinstance(each, int) or each.isnumeric():
                request.user_ids.append(int(each))
            else:
                request.usernames.append(each.lower())

        response = await grpc.eros.GetProfiles(request=request, context=None)
        response = grpc.to_dict(response).get("profiles")
        return response if response else []
