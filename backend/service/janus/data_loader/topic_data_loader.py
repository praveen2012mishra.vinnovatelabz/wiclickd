import logging
from .base_data_loader import BaseDataLoader
from athena.api.athena_pb2 import GetTopicsRequest
from weclikd.utils.session import Session
from weclikd.service import grpc


class TopicLoader(BaseDataLoader):

    id_field_names = ["id", "name"]
    keys_in_order = True

    async def batch_load_from_sql(self, keys):
        logging.info("Batch Topics: {}".format(keys))
        request = GetTopicsRequest()
        for each in keys:
            if each.isnumeric():
                request.topic_ids.append(int(each))
            else:
                request.topic_names.append(each)

        response = await grpc.athena.GetTopics(request=request, context=Session({}))
        response = grpc.to_dict(response).get("topics")
        return response if response else []
