import logging
from .base_data_loader import BaseDataLoader
from atlas.api.atlas_pb2 import GetPostsRequest, GetCommentsRequest
from atlas.api.comment_pb2 import DiscussionFilter
from weclikd.utils.session import Session
from weclikd.service import grpc


class PostLoader(BaseDataLoader):

    keys_in_order = True

    async def batch_load_from_sql(self, keys):
        logging.info("Batch Post: {}".format(keys))

        response = await grpc.atlas.GetPosts(
            request=GetPostsRequest(
                ids=[int(each) for each in keys],
                discussion_filter=DiscussionFilter(thread_length=0, num_replies=0),
            ),
            context=self.graphql_context["session"]
            if "session" in self.graphql_context
            else Session({}),
        )
        return grpc.to_dict(response).get("posts")


class CommentLoader(BaseDataLoader):

    keys_in_order = True

    async def batch_load_from_sql(self, keys):
        logging.info("Batch Comment: {}".format(keys))

        response = await grpc.atlas.GetComments(
            request=GetPostsRequest(
                ids=[int(each) for each in keys],
                discussion_filter=DiscussionFilter(thread_length=0, num_replies=0),
            ),
            context=self.graphql_context["session"]
            if "session" in self.graphql_context
            else Session({}),
        )
        return grpc.to_dict(response).get("comments")
