import logging
from .base_data_loader import BaseDataLoader
from janus.graphql.utils import CustomNode
from hestia.api.hestia_pb2 import GetClikInfosRequest
from weclikd.utils.session import Session
from weclikd.service import grpc


class ClikLoader(BaseDataLoader):

    id_field_names = ["id", "name"]

    async def batch_load_from_sql(self, keys):
        logging.info("Batch Cliks: {}".format(keys))
        request = GetClikInfosRequest()
        for each in keys:
            request.clik_ids.append(
                CustomNode.get_id_from_global_id(each, allow_string=True)
            )

        response = await grpc.hestia.GetClikInfos(request=request, context=Session({}))
        response = grpc.to_dict(response).get("cliks")
        return response if response else []
