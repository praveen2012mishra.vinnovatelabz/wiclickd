from typing import Dict, List
from weclikd.utils.session import Session
from athena.event.topic_pb2 import (
    CreateTopicEvent,
    EditTopicEvent,
    DeleteTopicEvent,
    FollowTopicEvent,
    UnfollowTopicEvent,
)
from eros.event.account_pb2 import (
    CreateUserEvent,
    EditUserEvent,
    DeleteAccountEvent,
)
from hestia.event.clik_pb2 import (
    CreateClikEvent,
    EditClikEvent,
    DeleteClikEvent,
    FollowClikEvent,
    UnfollowClikEvent,
    CreateClikMemberEvent,
    DeleteClikMemberEvent,
)
from atlas.event.content_pb2 import (
    CreatePostEvent,
    EditPostEvent,
    DeletePostEvent,
    DeleteCommentEvent,
    EditCommentEvent,
)
from artemis.event.external_feed_event_pb2 import (
    CreateExternalFeedEvent,
    EditExternalFeedEvent,
    DeleteExternalFeedEvent,
    FollowExternalFeedEvent,
    UnfollowExternalFeedEvent,
)
from weclikd.service import grpc, es
import asyncio


async def search_everything(
    terms: List[str],
    topics: List[str],
    cliks: List[str],
    users: List[str],
    feeds: List[str],
) -> Dict:
    cliks = cliks or []
    topics = topics or []
    users = users or []
    feeds = feeds or []
    terms = terms or []
    results = await asyncio.gather(
        _search_posts(terms, cliks, topics),
        _search_topics(terms, topics),
        _search_cliks(terms, cliks),
        _search_users(terms, users),
        _search_feeds(terms, feeds, topics),
        return_exceptions=True,
    )
    return {
        "posts": results[0],
        "topics": results[1],
        "cliks": results[2],
        "users": results[3],
        "feeds": results[4],
    }


async def search_topics(prefix: str) -> List[Dict]:
    return await _completion_search("topic", prefix)


async def search_users(prefix: str) -> List[Dict]:
    return await _completion_search("user", prefix, field_names=["username.completion"])


async def search_cliks(prefix: str) -> List[Dict]:
    return await _completion_search("clik", prefix)


async def search_feeds(prefix: str) -> List[Dict]:
    return await _completion_search("feed", prefix, field_names=["name.completion", "website.completion"])


async def index_topic(event: CreateTopicEvent, session: Session):
    await es.index("topic", grpc.to_dict(event, "topic", including_default_values=True))


async def index_user(event: CreateUserEvent, session: Session):
    profile = grpc.to_dict(event, "profile", including_default_values=True)
    await es.index("user", profile)


async def index_clik(event: CreateClikEvent, session: Session):
    await es.index("clik", grpc.to_dict(event, "clik", including_default_values=True))


async def index_post(event: CreatePostEvent, session: Session):
    await es.index(
        "post",
        grpc.to_dict(event, "post", including_default_values=True),
        event.post.content_info.id,
    )


async def index_feed(event: CreateExternalFeedEvent, session: Session):
    await es.index("feed", grpc.to_dict(event, "feed", including_default_values=True))


async def update_topic(event: EditTopicEvent, session: Session):
    await es.update("topic", grpc.to_dict(event, "topic"))


async def update_user(event: EditUserEvent, session: Session):
    await es.update("user", grpc.to_dict(event, "profile"))


async def update_clik(event: EditClikEvent, session: Session):
    await es.update("clik", grpc.to_dict(event, "clik"))


async def update_post(event: EditPostEvent, session: Session):
    await es.update("post", grpc.to_dict(event, "post"), event.post.content_info.id)


async def update_feed(event: EditExternalFeedEvent, session: Session):
    await es.update("feed", grpc.to_dict(event, "feed"))


async def delete_user(event: DeleteAccountEvent, session: Session):
    for user_id in event.user_ids:
        await es.delete("user", user_id)


async def delete_topic(event: DeleteTopicEvent, session: Session):
    await es.delete("topic", event.topic_id)


async def delete_clik(event: DeleteClikEvent, session: Session):
    await es.delete("clik", event.clik_id)


async def delete_post(event: DeletePostEvent, session: Session):
    await es.delete("post", event.content_id)


async def delete_feed(event: DeleteExternalFeedEvent, session: Session):
    await es.delete("feed", event.feed_id)


async def increment_topic_followers(event: FollowTopicEvent, session: Session):
    await es.update_field("topic", id=event.topic_id, followers=event.followers)


async def decrement_topic_followers(event: UnfollowTopicEvent, session: Session):
    await es.update_field("topic", id=event.topic_id, followers=event.followers)


async def increment_clik_followers(event: FollowClikEvent, session: Session):
    await es.update_field("clik", id=event.clik_id, followers=event.followers)


async def decrement_clik_followers(event: UnfollowClikEvent, session: Session):
    await es.update_field("clik", id=event.clik_id, followers=event.followers)


async def increment_clik_members(event: CreateClikMemberEvent, session: Session):
    await es.update_field("clik", id=event.member.clik_id, members=event.members)


async def decrement_clik_members(event: DeleteClikMemberEvent, session: Session):
    await es.update_field("clik", id=event.member.clik_id, members=event.members)


async def increment_external_feed_followers(
    event: FollowExternalFeedEvent, session: Session
):
    await es.update_field("feed", id=event.feed_id, followers=event.followers)


async def decrement_external_feed_followers(
    event: UnfollowExternalFeedEvent, session: Session
):
    await es.update_field("feed", id=event.feed_id, followers=event.followers)


async def _completion_search(
    object_type: str, prefix: str, field_names=None
) -> List[Dict]:
    field_names = ["name.completion"] if not field_names else field_names
    response = await es.search(
        index=object_type,
        body={
            "query": {
                "multi_match": {
                    "query": prefix,
                    "type": "bool_prefix",
                    "fields": field_names
                }
            }
        },
    )
    return [each["_source"] for each in response["hits"]["hits"]]


async def _search_posts(terms: List[str], cliks: List[str], topics: List[str]):
    if not terms and not cliks and not topics:
        return []

    response = await es.search(
        index="post",
        body={
            "query": {
                "bool": {
                    "should": [
                        {
                            "multi_match": {
                                "query": " ".join(terms),
                                "fields": ["title^3", "summary^2", "url"],
                            }
                        },
                        {"terms": {"topics": topics}},
                        {"terms": {"cliks": cliks}},
                    ]
                }
            }
        },
    )
    return [each["_source"] for each in response["hits"]["hits"]]


async def _search_topics(terms: List[str], topics: List[str]):
    if not terms and not topics:
        return []

    response = await es.search(
        index="topic",
        body={
            "query": {
                "bool": {
                    "should": [
                        {
                            "match": {
                                "name": {
                                    "query": " ".join(terms),
                                    "fuzziness": "1",
                                    "boost": 0.25,
                                }
                            }
                        },
                        {"terms": {"name.keyword": topics}},
                    ]
                }
            }
        },
    )
    return [each["_source"] for each in response["hits"]["hits"]]


async def _search_cliks(terms: List[str], cliks: List[str]):
    if not terms and not cliks:
        return []

    response = await es.search(
        index="clik",
        body={
            "query": {
                "bool": {
                    "should": [
                        {
                            "match": {
                                "name": {
                                    "query": " ".join(terms),
                                    "fuzziness": "1",
                                    "boost": 0.25,
                                }
                            }
                        },
                        {"terms": {"name.keyword": cliks}},
                    ]
                }
            }
        },
    )
    return [each["_source"] for each in response["hits"]["hits"]]


async def _search_users(terms: List[str], users: List[str]):
    response = await es.search(
        index="user",
        body={
            "query": {
                "bool": {
                    "should": [
                        {
                            "match": {
                                "username": {
                                    "query": " ".join(terms),
                                    "fuzziness": "1",
                                    "boost": 0.25,
                                }
                            }
                        },
                        {"terms": {"username.keyword": users}},
                    ]
                }
            }
        },
    )
    return [each["_source"] for each in response["hits"]["hits"]]


async def _search_feeds(terms: List[str], feeds: List[str], topics: List[str]):
    response = await es.search(
        index="feed",
        body={
            "query": {
                "bool": {
                    "should": [
                        {
                            "match": {
                                "name": {
                                    "query": " ".join(terms),
                                    "fuzziness": "1",
                                    "boost": 0.25,
                                }
                            }
                        },
                        {"terms": {"base_topic": topics}},
                        {"terms": {"name.keyword": feeds}},
                    ]
                }
            }
        },
    )
    return [each["_source"] for each in response["hits"]["hits"]]
