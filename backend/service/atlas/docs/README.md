# Atlas Design Doc

## Overview and Context

Atlas is the microservice responsible for storing and organizing all the posts and discussions on Weclikd.

There are four places where posts are organized: topics, external feeds, users, cliks.
The posts can be sorted by popularity (under Trending), by order (under New), by the time the user commented (under Discussion),
or by number of reports (under Reported, only for admins)


Discussions are a tree structure of comments. Each top-level comment and its children is one discussion.

## Functionality Overview

1) CRUD Posts and Comments
2) Like, Report content
3) View Feeds (clik, user, home, topic, external feeds)

This microservice is not responsible for storing topics, cliks, or users.

## Storage

MySQL was chosen to store all data at least for now. 

MySQL

Pros:
* No vendor lockdown
* Easy to get started
* Cheaper than vendor solutions for moderate/high traffic

Cons:
* Write operations will be slow for non-US customers since content is not distributed.
* Scaling to Petabytes require sharding.
* No free tier

Mitigation:
* Write operations don't need to be fast.
* Won't need to scale to Petabytes for a very long time
* $27/month is not bad.


### Tables

Content (sort by id)
* content type (post or comment)
* like count
* report count
* data fields (title, summary, etc) 

Comment
* comment to parent content relationship

TopicPost
ClikPost
UserPost
FeedPost
* Topic to x relationship

Url
* url to content id 

UserLike
* user like to content relationship


Report
* report data 

### Caching

Redis will be exclusively used for caching. The key and its description are listed below.

#### Posts and Comments
Posts and comments are split into two different keys. The metrics will most likely change frequently,
and is best updated in a hash, while there are parts of the content which will not be updated frequently.
Expiration: None
* p<content-id>: string => json_string(title, summary, ...)
* d<content-id>: string => json_string(text, user_id, ...)
* c<content-id>: hash => red, silver, gold, num_reports, num_comments


#### Feeds Sorted by Time (Home, User, Topics, Cliks)
We will use a sorted set here instead of a simple list so that it is easier
to paginate. If we were to use a simple list, we would be forced to save the index as a cursor for feed
pagination. However, new posts can come in and the index will be out-dated. By using a sorted list and setting the
content id as both the value and the score (content ids are based off of the timestamp), we can use the
content id itself as the cursor. Expiration: None
* home-new sorted_set => value: <content-id> score: <content-id>
* T<content-id>-new sorted_set => value: <content-id> score: <content-id>
* C<content-id>-new sorted_set => value: <content-id> score: <content-id>
* U<content-id>-new sorted_set => value: <content-id> score: <content-id>


#### Feeds Sorted by Score (Home, User, Topics, Cliks)
Each feed will have a sorted set of pot ids. Every minute, 
the backend will recalculate the score for posts that are no older than 7 days.
After 7 days, the post will be archived (read-only) and it will have a constant score.
For each user, topic, and clik the post is shared to, the corresponding feed will be updated with the
content's new score. Expiration: None
Expiration: None 
* home-trend => value: <content-id> score: <post's score>
* T<topic_id>-trend => value: <content-id> score: <post's score>
* C<clik_id>-trend => value: <content-id> score: <post's score>
* U<user_id>-trend => value: <content-id> score: <post's score>


