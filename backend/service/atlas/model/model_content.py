from typing import List, Union
from atlas.api.content_info_pb2 import ContentInfo
from atlas.api.post_pb2 import Post
from atlas.api.comment_pb2 import Comment
from atlas.api.like_pb2 import Likes
from atlas.api.report_pb2 import Report
from weclikd.service.sql import SQLInstance, get_sql_client
from weclikd.service.transaction import Transaction as SqlTransaction
from atlas.db import db_content
from datetime import datetime


async def retrieve(db: SQLInstance, content_id: int) -> Union["Comment", "Post"]:
    if not db:
        db = await get_sql_client()

    return await db_content.get_content(db, content_id)


async def report(content_id: int, reports: List[Report], user_id: int):
    async with SqlTransaction() as db:
        for report in reports:
            if report.type != Report.ReportType.OFF_TOPIC:
                await db_content.increment_report_count(db, content_id)
                del report.off_topic_cliks[:]
                del report.off_topic_topics[:]
            await db_content.insert_report(db, content_id, report, user_id)


async def like(content_id: int, like_type: Likes.LikeType, user_id: int, ts: datetime):
    if like_type == Likes.NONE:
        async with SqlTransaction() as db:
            prev_like_type, prev_created = await db_content.get_like(
                db, content_id, user_id
            )
            if prev_like_type:
                await db_content.decrement_like_count(db, content_id, prev_like_type)

            await db_content.remove_like(db, user_id=user_id, content_id=content_id)
            return prev_like_type, prev_created

    async with SqlTransaction() as db:
        prev_like_type, prev_created = await db_content.get_like(
            db, content_id, user_id
        )
        if prev_like_type:
            await db_content.decrement_like_count(db, content_id, prev_like_type)

        await db_content.insert_like(db, content_id, like_type, user_id, ts)
        await db_content.increment_like_count(db, content_id, like_type)
        return prev_like_type, prev_created
        # updating score on comments will happen immediately
        # updating score on posts will happen on an interval for three reasons
        # 1) The score of a post depends on time. Every minute, all post's score changes.
        #    This is so very popular posts will not end up on the front page over time
        # 2) The post's score depends on several factors, including the scores on the comments.
        #    Updating the post's score on every event will cause too much stress on the backend.
        # 3) Reduce latency when performing an action.
        # if content_type == ContentInfo.ContentType.COMMENT:
        #    comment = await db_content.get_content(db, content_id)
        #    db_comment.update_score_on_like(db, comment, like_type)
