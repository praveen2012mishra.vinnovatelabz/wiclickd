from atlas.api.post_pb2 import Feed, FeedFilter
from weclikd.service.transaction import Transaction as SqlTransaction
from weclikd.service import redis
from weclikd.common.filter_pb2 import DefaultFilter
from atlas.db import db_feed, db_feed_trending, feed_utils, db_feed_new
from atlas.model import model_post
import logging
import time


async def get_feed(
    feed_id: int, feed_type: db_feed.FeedType, filter: FeedFilter, user_id: int
) -> Feed:
    if not user_id and filter.sort == FeedFilter.Sort.DISCUSSING:
        return Feed()

    if (
        user_id
        and feed_type == db_feed.FeedType.HOME
        and filter.sort == FeedFilter.Sort.NEW
    ):
        paginator = feed_utils.NewFeedPaginateTracker.init(filter)
        content_id_to_reasons_shared = await db_feed_new.get_user_new_feed(
            user_id=user_id, paginator_tracker=paginator
        )
        content_ids = list(content_id_to_reasons_shared.keys())
        posts = await model_post.fetch_many(content_ids, user_id)
        page_info = paginator.get_page_info(content_ids)
        for reasons_shared, post in zip(content_id_to_reasons_shared.values(), posts):
            post.reasons_shared.MergeFrom(reasons_shared)

        return Feed(posts=posts, feed_page_info=page_info)
    else:
        content_ids, page_info = await db_feed.get_content_ids(
            feed_id=feed_id, feed_type=feed_type, filter=filter, user_id=user_id
        )
        posts = await model_post.fetch_many(content_ids, user_id)
        return Feed(posts=posts, feed_page_info=page_info)


async def refresh_scores(event, session):
    logging.info("Refreshing scores for posts")
    feed_filter = FeedFilter()
    feed_filter.num_posts = 200
    feed_filter.sort = FeedFilter.Sort.NEW
    total_posts = 0
    start_time = int(time.time())
    while total_posts < 100000:
        feed = await get_feed(
            feed_id="", feed_type=db_feed.FeedType.HOME, filter=feed_filter, user_id=0
        )

        pipeline = await redis.get_pipeline()
        for post in feed.posts:
            db_feed_trending.update_score(pipeline, post, start_time)
        await pipeline.execute()

        total_posts += len(feed.posts)

        if not feed.feed_page_info.has_next_page or time.time() - start_time > 300:
            logging.info(f"Refreshed {total_posts} posts.")
            break

        feed_filter.cursor = feed.feed_page_info.cursor

    logging.info("Done refreshing scores for posts")


async def delete_topic_feed(topic_id: int):
    async with SqlTransaction() as db:
        await db_feed.delete_topic_feed(db, topic_id)


async def refresh_user_trending(user_id: int):
    redis_pipeine = await redis.get_pipeline()
    feed_utils.invalidate_cache(
        redis_pipeine, feed_utils.FeedType.HOME, user_id, sort_by=DefaultFilter.TRENDING
    )
    await redis_pipeine.execute()
