from typing import List, Union
from atlas.api.comment_pb2 import Discussion, Comment, DiscussionFilter
from atlas.api.post_pb2 import Post
from weclikd.service.sql import get_sql_client
from weclikd.service.transaction import Transaction as SqlTransaction
from atlas.db import db_content, db_feed, db_comment


async def insert(comment: Comment):
    async with SqlTransaction() as db:
        # insert, share to user's table, and increment comment counts
        comment.content_info.id = await db_comment.insert(db, comment)
        await db_feed.share_to_user(
            db, content_id=comment.thread_ids[0], user_id=comment.content_info.user_id
        )
        await db_content.increment_comments_count(db, comment)

        # update score
        db_comment.update_score_on_comment(db, comment)


async def fetch_many(content_ids: List[int], user_id: int) -> List[Comment]:
    db = await get_sql_client()
    comments = await db_comment.fetch_many(db, content_ids)
    await db_content.populate_user_like(db, comments, user_id)

    return comments


async def populate_discussion(
    content: Union[Post, Comment], filter: DiscussionFilter, user_id: int
):
    db = await get_sql_client()
    if filter.sort == DiscussionFilter.DISCUSSING:
        await db_comment.populate_discussion_discussing(
            db, content, filter, user_id
        )
    else:
        await db_comment.populate_discussion(db, content, filter)
    flattened_comments = _discussion_to_flattened_comments(content.discussion)
    await db_content.populate_user_like(db, flattened_comments, user_id)
    return content.discussion


async def remove(comment: Comment):
    async with SqlTransaction() as db:
        await db_comment.remove(db, comment)
        await db_content.decrement_comments_count(db, comment)


async def update(comment: Comment):
    async with SqlTransaction() as db:
        await db_comment.update(db, comment)


def _discussion_to_flattened_comments(discussion: Discussion) -> List[Comment]:
    comments = []
    for comment in discussion.comments:
        comments.append(comment)
        comments.extend(_discussion_to_flattened_comments(comment.discussion))
    return comments
