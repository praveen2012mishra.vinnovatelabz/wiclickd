from typing import List
from atlas.api.post_pb2 import Post
from atlas.db import db_feed, db_post
from weclikd.service.transaction import Transaction as SqlTransaction


async def share(post: Post, topic_ids: List[int], db: SqlTransaction):
    await db_feed.share_to_user(
        db, post.content_info.id, post.content_info.user_id or post.sharer_id
    )

    if post.external_feed.id:
        await db_feed.share_to_feed(db, post.content_info.id, post.external_feed.id)
    await db_feed.share_to_cliks(db, post.content_info.id, post.cliks)
    await db_feed.share_to_topics(db, post.content_info.id, topic_ids)


async def update(
    modified_post: Post, original_post: Post, topic_ids: List[int], db: SqlTransaction
):
    if modified_post.topics:
        # share new topics, unshare removed topics
        original_topics = set(original_post.topic_ids)
        modified_topics = set(topic_ids)
        deleted_topics = original_topics - modified_topics
        added_topics = modified_topics - original_topics
        await db_feed.unshare_to_topics(
            db, modified_post.content_info.id, deleted_topics
        )
        await db_feed.share_to_topics(db, modified_post.content_info.id, added_topics)

        # delete original post's topics, will be replaced by modified post
        del original_post.topics[:]
        del original_post.topic_ids[:]

    if modified_post.cliks:
        original_cliks = set(original_post.cliks)
        modified_cliks = set(modified_post.cliks)
        deleted_cliks = original_cliks - modified_cliks
        added_cliks = modified_cliks - original_cliks
        await db_feed.unshare_to_cliks(db, modified_post.content_info.id, deleted_cliks)
        await db_feed.share_to_cliks(db, modified_post.content_info.id, added_cliks)
        del original_post.cliks[:]
        del original_post.clik_ids[:]


async def share_to_clik(post: Post, clik: str):
    async with SqlTransaction() as db:
        await db_feed.share_to_cliks(db, post.content_info.id, [clik])
        post.cliks.append(clik)
        await db_post.update(db, post)


async def merge_sharing(new_post: Post, old_post: Post, db: SqlTransaction):
    if new_post.external_feed.id:
        if new_post.external_feed.id != old_post.external_feed.id:
            await db_feed.share_to_feed(
                db, old_post.content_info.id, new_post.external_feed.id
            )
        if not old_post.external_feed.id:
            # add feed info to old post
            old_post.external_feed.CopyFrom(new_post.external_feed)
            await db_post.update(db, old_post)

    return old_post


async def unshare(post: Post, db: SqlTransaction):
    if post.topics:
        topic_ids = await db_feed.get_topics_shared(db, post.content_info.id)
        await db_feed.unshare_to_topics(db, post.content_info.id, topic_ids)
    if post.cliks:
        await db_feed.unshare_to_cliks(db, post.content_info.id, post.cliks)
    if post.external_feed:
        await db_feed.unshare_to_feed(db, post.content_info.id, post.external_feed.id)
