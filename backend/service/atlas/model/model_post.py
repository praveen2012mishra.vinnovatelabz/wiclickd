from typing import List, Optional
from atlas.api.post_pb2 import Post
from atlas.model import model_share
from weclikd.service.transaction import Transaction as SqlTransaction
from weclikd.service.sql import get_sql_client
from weclikd.utils import url_normalize
from atlas.db import db_content, db_post


async def insert(post: Post, topic_ids: List[int]):
    async with SqlTransaction() as db:
        await db_post.insert(db, post)
        if post.url:
            await db_post.insert_url(db, post.content_info.id, post.url)
        for url in post.other_urls:
            await db_post.insert_url(db, post.content_info.id, url)

        await model_share.share(post, topic_ids, db)
    return post


async def fetch_many(content_ids: List[int], user_id: int) -> List[Post]:
    db = await get_sql_client()
    posts = await db_post.fetch_many(db, content_ids)
    await db_content.populate_user_like(db, posts, user_id)

    return posts


async def retrieve_using_url(url: str, user_id: int = 0) -> Optional[Post]:
    db = await get_sql_client()
    url = url_normalize.url_normalize(url)
    post = await db_post.fetch_using_url(db, url)
    if post and user_id:
        await db_content.populate_user_like(db, [post], user_id)
    return post


async def update(modified_post: Post, topic_ids: List[int]):
    async with SqlTransaction() as db:
        posts = await db_post.fetch_many(db, [modified_post.content_info.id])
        if not posts:
            return

        original_post = posts[0]

        # original post w/o url cannot be modified to have one
        modified_post.url = (
            url_normalize.url_normalize(modified_post.url) if original_post.url else ""
        )

        await model_share.update(modified_post, original_post, topic_ids, db)

        # update pictures
        if modified_post.picture_ids:
            del original_post.picture_ids[:]
            if modified_post.picture_ids[0] == 1:
                del modified_post.picture_ids[:]
        if modified_post.thumbnail_picture_id == 1:
            original_post.thumbnail_picture_id = 0
            modified_post.thumbnail_picture_id = 0

        # update and commit
        original_post.MergeFrom(modified_post)
        await db_post.update(db, original_post)
        return original_post


async def remove(post: Post):
    async with SqlTransaction() as db:
        await model_share.unshare(post, db)
        await db_post.remove(db, post)
