import sqlalchemy
from sqlalchemy import text as sql_text
from typing import List
from atlas.db.feed_utils import FeedType, FeedPaginateTracker
from atlas.api.content_info_pb2 import ContentInfo
from atlas.db.tables import (
    ContentTable,
    ClikPostTable,
    TopicPostTable,
    UserPostTable,
    FeedPostTable,
    ContentLikeTable,
)
from weclikd.service.transaction import Transaction as SqlTransaction


async def get_discussing_feed(
    feed_id: int,
    feed_type: FeedType,
    paginator_tracker: FeedPaginateTracker,
    user_id: int,
) -> List[int]:
    if not user_id:
        return []

    if feed_type == FeedType.HOME:
        post_table = None
        post_table_id_column = None
    elif feed_type == FeedType.CLIK:
        post_table = ClikPostTable
        post_table_id_column = ClikPostTable.c.clik_id
    elif feed_type == FeedType.USER:
        post_table = UserPostTable.alias("user_post_2")
        post_table_id_column = post_table.c.user_id
    elif feed_type == FeedType.FEED:
        post_table = FeedPostTable
        post_table_id_column = post_table.c.feed_id
    else:
        assert feed_type == FeedType.TOPIC
        post_table = TopicPostTable
        post_table_id_column = TopicPostTable.c.topic_id

    if post_table is not None:
        # Join the source's posts (clik, user, topic, feed) with the user's post table
        join_post = post_table.join(
            ContentTable, post_table.c.content_id == ContentTable.c.id
        ).join(UserPostTable, UserPostTable.c.content_id == ContentTable.c.id)
        whereclauses_post = [
            ContentTable.c.state != ContentInfo.ContentState.REMOVED,
            post_table_id_column == feed_id,
            UserPostTable.c.user_id == user_id,
        ]
        if paginator_tracker.after:
            whereclauses_post.append(post_table.c.content_id < paginator_tracker.after)

        # Join the source's posts with the posts that the user has liked
        join_like = post_table.join(
            ContentTable, post_table.c.content_id == ContentTable.c.id
        ).join(ContentLikeTable, ContentLikeTable.c.content_id == ContentTable.c.id)
        whereclauses_like = [
            ContentTable.c.state != ContentInfo.ContentState.REMOVED,
            post_table_id_column == feed_id,
            ContentLikeTable.c.user_id == user_id,
        ]
        if paginator_tracker.after:
            whereclauses_like.append(post_table.c.content_id < paginator_tracker.after)
    else:
        # Get the users's posts
        join_post = UserPostTable.join(
            ContentTable, UserPostTable.c.content_id == ContentTable.c.id
        )
        whereclauses_post = [
            ContentTable.c.state != ContentInfo.ContentState.REMOVED,
            UserPostTable.c.user_id == user_id,
        ]
        if paginator_tracker.after:
            whereclauses_post.append(
                UserPostTable.c.content_id < paginator_tracker.after
            )

        # Get the posts that the user has liked
        join_like = ContentLikeTable.join(
            ContentTable, ContentLikeTable.c.content_id == ContentTable.c.id
        ).join(TopicPostTable, TopicPostTable.c.content_id == ContentTable.c.id)
        whereclauses_like = [
            ContentTable.c.state != ContentInfo.ContentState.REMOVED,
            ContentLikeTable.c.user_id == user_id,
        ]
        if paginator_tracker.after:
            whereclauses_post.append(
                ContentLikeTable.c.content_id < paginator_tracker.after
            )

    # combine the posts that the user has liked and the user's posts
    async with SqlTransaction() as db:
        rows = await db.fetch_all(
            sqlalchemy.sql.union(
                sqlalchemy.select(
                    [ContentTable.c.id],
                    from_obj=join_post,
                    whereclause=sqlalchemy.and_(*whereclauses_post),
                    limit=paginator_tracker.limit,
                    order_by=sqlalchemy.desc(ContentTable.c.id),
                ),
                sqlalchemy.select(
                    [ContentTable.c.id],
                    from_obj=join_like,
                    whereclause=sqlalchemy.and_(*whereclauses_like),
                    limit=paginator_tracker.limit,
                    order_by=sqlalchemy.desc(ContentTable.c.id),
                ),
            )
            .order_by(sql_text("id DESC"))
            .limit(paginator_tracker.limit)
        )
    paginator_tracker.set_has_next_page(rows)

    return [row.id for row in rows]
