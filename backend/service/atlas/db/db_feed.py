import sqlalchemy
from sqlalchemy.dialects.mysql import insert as mysql_insert
from typing import List, Tuple
from atlas.db.feed_utils import FeedType, FeedPaginateTracker, NewFeedPaginateTracker
from atlas.db import db_feed_new, db_feed_trending, db_feed_discussing
from atlas.api.post_pb2 import FeedFilter
from atlas.db.tables import (
    ClikPostTable,
    TopicPostTable,
    UserPostTable,
    FeedPostTable,
)
from weclikd.utils import weclikd_id
from weclikd.common.page_info_pb2 import PageInfo
from weclikd.service.transaction import Transaction as SqlTransaction
import logging


async def get_content_ids(
    feed_id: int, feed_type: FeedType, filter: FeedFilter, user_id: int
) -> Tuple[List[int], PageInfo]:
    paginator_tracker = FeedPaginateTracker.init(filter)
    if filter.sort == FeedFilter.Sort.NEW:
        paginator_tracker = NewFeedPaginateTracker.init(filter)
        content_ids = await db_feed_new.get_new_feed(
            feed_id, feed_type, paginator_tracker, user_id
        )
    elif filter.sort == FeedFilter.Sort.TRENDING:
        content_ids = await db_feed_trending.get_trending_feed(
            feed_id, feed_type, paginator_tracker, user_id
        )
    elif filter.sort == FeedFilter.Sort.DISCUSSING:
        content_ids = await db_feed_discussing.get_discussing_feed(
            feed_id, feed_type, paginator_tracker, user_id
        )
    else:
        paginator_tracker = NewFeedPaginateTracker.init(filter)
        content_ids = await db_feed_new.get_new_feed(
            feed_id, feed_type, paginator_tracker, user_id
        )

    return content_ids, paginator_tracker.get_page_info(content_ids)


async def delete_topic_feed(db: SqlTransaction, topic_id: int):
    await db.execute(
        sqlalchemy.delete(
            TopicPostTable, whereclause=TopicPostTable.c.topic_id == topic_id
        )
    )
    db.redis.delete(f"T{topic_id}-trend")


async def share_to_user(db: SqlTransaction, content_id: int, user_id: int):
    if not user_id:
        return

    try:
        insert_stmt = (
            mysql_insert(UserPostTable)
            .values(**{"content_id": content_id, "user_id": user_id})
            .on_duplicate_key_update({"content_id": content_id, "user_id": user_id})
        )
        await db.execute(insert_stmt)
    except:
        logging.exception(f"Failed to share to user: {user_id} {content_id}")


async def share_to_cliks(db: SqlTransaction, content_id: int, cliks: List[str]):
    clik_ids = [weclikd_id.string_to_id(clik) for clik in cliks]
    await db.execute_many(
        sqlalchemy.insert(ClikPostTable),
        values=[{"content_id": content_id, "clik_id": clik_id} for clik_id in clik_ids],
    )


async def unshare_to_cliks(db: SqlTransaction, content_id: int, cliks: List[str]):
    clik_ids = [weclikd_id.string_to_id(clik) for clik in cliks]
    await db.execute(
        sqlalchemy.delete(
            ClikPostTable,
            whereclause=sqlalchemy.and_(
                ClikPostTable.c.content_id == content_id,
                ClikPostTable.c.clik_id.in_(clik_ids),
            ),
        )
    )
    for clik_id in clik_ids:
        db.redis.zrem(f"C{clik_id}-trend", content_id)


async def get_topics_shared(db: SqlTransaction, content_id: int) -> List[int]:
    rows = await db.fetch_all(
        sqlalchemy.select(
            [TopicPostTable.c.topic_id],
            whereclause=TopicPostTable.c.content_id == content_id,
        )
    )
    return [row.topic_id for row in rows]


async def get_cliks_shared(db: SqlTransaction, content_id: int) -> List[int]:
    rows = await db.fetch_all(
        sqlalchemy.select(
            [ClikPostTable.c.topic_id],
            whereclause=ClikPostTable.c.content_id == content_id,
        )
    )
    return [row.topic_id for row in rows]


async def share_to_topics(db: SqlTransaction, content_id: int, topic_ids: List[int]):
    await db.execute_many(
        sqlalchemy.insert(TopicPostTable),
        values=[
            {"content_id": content_id, "topic_id": topic_id} for topic_id in topic_ids
        ],
    )


async def unshare_to_topics(db: SqlTransaction, content_id: int, topic_ids: List[int]):
    await db.execute(
        sqlalchemy.delete(
            TopicPostTable,
            whereclause=sqlalchemy.and_(
                TopicPostTable.c.content_id == content_id,
                TopicPostTable.c.topic_id.in_(topic_ids),
            ),
        )
    )
    for topic_id in topic_ids:
        db.redis.zrem(f"T{topic_id}-trend", content_id)


async def share_to_feed(db: SqlTransaction, content_id: int, feed_id: int):
    await db.execute(
        sqlalchemy.insert(FeedPostTable),
        values={"content_id": content_id, "feed_id": feed_id},
    )


async def unshare_to_feed(db: SqlTransaction, content_id: int, feed_id: int):
    await db.execute(
        sqlalchemy.delete(
            FeedPostTable,
            whereclause=sqlalchemy.and_(
                FeedPostTable.c.content_id == content_id,
                FeedPostTable.c.feed_id == feed_id,
            ),
        )
    )
    db.redis.zrem(f"F{feed_id}-trend", content_id)
