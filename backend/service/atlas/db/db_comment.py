import sqlalchemy
import base64
import json
from typing import Union, List
from google.protobuf.json_format import ParseDict
from weclikd.service.transaction import Transaction as SqlTransaction
from atlas.api.comment_pb2 import Comment, DiscussionFilter, Discussion
from atlas.api.post_pb2 import Post
from atlas.api.like_pb2 import Likes
from atlas.api.content_info_pb2 import ContentInfo
from atlas.db import db_content
from atlas.db.tables import CommentTable, ContentTable
from weclikd.utils import weclikd_id, time_utils
import logging


async def insert(db: SqlTransaction, comment: Comment) -> int:
    content_id = await db_content.insert(db, comment)
    await db.execute(
        sqlalchemy.insert(
            CommentTable,
            values={
                "content_id": content_id,
                "clik_id": weclikd_id.string_to_id(comment.clik),
                "parent_content_id": comment.parent_content_id,
                "post_id": comment.thread_ids[0],
                "discussion_id": comment.thread_ids[1]
                if len(comment.thread_ids) >= 2
                else content_id,
                "user_id": comment.content_info.user_id,
                "level": len(comment.thread_ids) - 1,
            },
        )
    )

    comment.content_info.id = content_id
    return content_id


async def fetch_many(db: SqlTransaction, content_ids: List[int]) -> List[Comment]:
    id_to_comments = {}
    rows = await db.fetch_all(
        sqlalchemy.select([ContentTable], ContentTable.c.id.in_(tuple(content_ids)))
    )
    for row in rows:
        comment = db_content.row_to_proto(row)
        id_to_comments[comment.content_info.id] = comment

    # since sql does not return items in order, we have to order them here.
    comments_in_order = []
    for content_id in content_ids:
        comment = id_to_comments.get(int(content_id))
        if comment is None:
            logging.critical(f"Failed to get comment:{content_id}.")
        else:
            comments_in_order.append(comment)

    return comments_in_order


async def remove(db: SqlTransaction, comment: Comment):
    await db_content.remove(db, comment.content_info.id)


async def update(db: SqlTransaction, comment: Comment):
    await db_content.update(db, comment)


async def populate_discussion(
    db: SqlTransaction, content: Union[Post, Comment], filter: DiscussionFilter
) -> Discussion:
    thread_length = filter.thread_length
    num_replies = filter.num_replies
    cursor = filter.cursor
    if num_replies == 0 or thread_length == 0:
        return content.discussion

    where_cursor_clause = (
        f"and c0.content_id > {base64.b64decode(cursor).decode()}" if cursor else ""
    )
    where_clik_clause = (
        f"and c0.clik_id in ({','.join([str(each) for each in filter.clik_ids])})"
        if filter.clik_ids
        else ""
    )

    # Do a recursive sql query to get all comments at once
    # Take a look at the link below for an explanation
    # https://stackoverflow.com/questions/20215744/how-to-create-a-mysql-hierarchical-recursive-query
    recursive_sql_query = (
        "select {SQL_SELECT_IDS}, \n"
        "    {SELECT_COMMENT_COLUMNS} \n"
        "from comment c0 \n"
        "left join content on id = c0.content_id \n"
        "{SQL_JOINS} \n"
        "where {PARENT_CONTENT_ID} in ({SQL_IDS}) {WHERE_AFTER_CURSOR} {WHERE_CLIK_ID} and content.state != 2\n"
        "order by {SQL_ORDER_BY}, created "
    )
    recursive_sql_query = recursive_sql_query.format(
        SQL_SELECT_IDS=", \n".join(
            [
                f"c{i}.parent_content_id as c{i}_parent_content_id"
                for i in range(thread_length)
            ]
        ),
        SELECT_COMMENT_COLUMNS=", \n".join(
            [f"{column} as {column.name}" for column in ContentTable.c]
        ),
        SQL_JOINS=" \n".join(
            [
                f"left join comment c{i+1} on c{i+1}.content_id = c{i}.parent_content_id "
                for i in range(thread_length - 1)
            ]
        ),
        SQL_IDS=", \n".join([f"c{i}.parent_content_id" for i in range(thread_length)]),
        PARENT_CONTENT_ID=content.content_info.id,
        WHERE_AFTER_CURSOR=where_cursor_clause,
        WHERE_CLIK_ID=where_clik_clause,
        SQL_ORDER_BY=", ".join(
            [f"c{i}_parent_content_id" for i in range(thread_length)]
        ),
    )
    rows = await db.fetch_all(recursive_sql_query)
    parent_to_discussion = {content.content_info.id: content.discussion}
    for row in rows:
        row = {**row}
        if row["c0_parent_content_id"] not in parent_to_discussion:
            continue

        discussion = parent_to_discussion[row["c0_parent_content_id"]]
        if len(discussion.comments) >= num_replies:
            discussion.discussion_page_info.has_next_page = True
            continue

        row["data"] = json.loads(row["data"])
        row["data"]["content_info"]["id"] = row["id"]
        row["data"]["content_info"]["num_comments"] = row["num_comments"]
        row["data"]["content_info"]["created"] = time_utils.convert_to_iso(
            row["created"]
        )

        for field, value in row.get("data", {}).items():
            row[field] = value

        comment = discussion.comments.add()
        discussion.discussion_page_info.cursor = base64.b64encode(
            str(row["id"]).encode()
        ).decode()
        parent_to_discussion[row["id"]] = comment.discussion
        ParseDict(row, comment, ignore_unknown_fields=True)

    return content.discussion


async def populate_discussion_discussing(
    db: SqlTransaction,
    content: Union[Post, Comment],
    filter: DiscussionFilter,
    user_id: int,
) -> Discussion:
    # TODO: check discussion filters like num_levels and max_thread, from comment
    thread_length = filter.thread_length
    num_replies = filter.num_replies
    if num_replies == 0 or thread_length == 0:
        return content.discussion

    # select distinct discussion_ids where user commented
    """
    SELECT content.*, comment.level, comment.parent_content_id from content join comment on content.id = comment.content_id where comment.discussion_id in (
	    SELECT distinct(discussion_id) from comment where post_id = 30888674985007 and user_id = 7551968383566660608
    )"""
    # find all comments with same discussion id

    post_id = (
        content.content_info.id
        if content.content_info.type == ContentInfo.POST
        else content.thread_ids[-1]
    )
    where_clik_clause = (
        f"and comment.clik_id in ({','.join([str(each) for each in filter.clik_ids])})"
        if filter.clik_ids
        else ""
    )
    query = (
        "select * from content\n"
        "join comment on comment.content_id = content.id\n"
        f"where comment.post_id = {post_id} and comment.user_id = {user_id} {where_clik_clause} and content.state != 2\n"
        f"order_by comment.level asc, content.id asc"
    )

    rows = await db.fetch_all(query)
    parent_to_discussion = {content.content_info.id: content.discussion}
    for row in rows:
        row = {**row}
        if row["parent_content_id"] not in parent_to_discussion:
            continue

        discussion = parent_to_discussion[row["parent_content_id"]]
        if len(discussion.comments) >= num_replies:
            discussion.discussion_page_info.has_next_page = True
            continue

        row["data"] = json.loads(row["data"])
        row["data"]["content_info"]["id"] = row["id"]
        row["data"]["content_info"]["num_comments"] = row["num_comments"]
        row["data"]["content_info"]["created"] = time_utils.convert_to_iso(
            row["created"]
        )

        for field, value in row.get("data", {}).items():
            row[field] = value

        comment = discussion.comments.add()
        discussion.discussion_page_info.cursor = base64.b64encode(
            str(row["id"]).encode()
        ).decode()
        parent_to_discussion[row["id"]] = comment.discussion
        ParseDict(row, comment, ignore_unknown_fields=True)
    return content.discussion


def update_score_on_comment(db: SqlTransaction, comment: Comment):
    pass


def update_score_on_like(
    db: SqlTransaction, comment: Comment, like_type: Likes.LikeType
):
    pass
