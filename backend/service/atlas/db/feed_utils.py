from dataclasses import dataclass
from typing import List
import time
from enum import Enum
from aioredis import Redis
from atlas.api.post_pb2 import FeedFilter
from weclikd.common.page_info_pb2 import PageInfo
from weclikd.common.filter_pb2 import DefaultFilter
import base64


@dataclass
class FeedPaginateTracker:
    sort: FeedFilter.Sort = FeedFilter.Sort.NEW
    limit: int = 20
    after: str = ""
    has_next_page: bool = False
    everything_cached: bool = False

    @classmethod
    def init(cls, feed_filter: FeedFilter) -> "FeedPaginateTracker":
        return FeedPaginateTracker(
            sort=feed_filter.sort,
            limit=feed_filter.num_posts + 1 if feed_filter.num_posts else 11,
            after=base64.b64decode(feed_filter.cursor).decode()
            if feed_filter.cursor
            else None,
            has_next_page=False,
            everything_cached=False,
        )

    def get_page_info(self, content_ids: List[int]) -> PageInfo:
        return PageInfo(
            has_next_page=self.has_next_page,
            cursor=base64.b64encode(str(content_ids[-1]).encode()).decode()
            if content_ids
            else "",
        )

    def set_has_next_page(self, list_content: List):
        self.has_next_page = len(list_content) == self.limit
        if self.has_next_page:
            del list_content[-1]


@dataclass
class NewFeedPaginateTracker:
    limit: int = 20
    previous: str = ""
    after: str = ""
    has_next_page: bool = False
    everything_cached: bool = False

    @classmethod
    def init(cls, feed_filter: FeedFilter) -> "NewFeedPaginateTracker":
        if feed_filter.cursor:
            cursor = base64.b64decode(feed_filter.cursor).decode()
            previous, after = cursor.split(",")
        else:
            previous = None
            after = None
        return NewFeedPaginateTracker(
            limit=feed_filter.num_posts + 1 if feed_filter.num_posts else 11,
            previous=previous,
            after=after,
            has_next_page=False,
            everything_cached=False,
        )

    def get_page_info(self, content_ids: List[int]) -> PageInfo:
        return PageInfo(
            has_next_page=self.has_next_page,
            cursor=base64.b64encode(f"{self.previous},{self.after}".encode()).decode()
            if content_ids
            else "",
        )

    def set_has_next_page(self, list_content: List):
        self.has_next_page = len(list_content) == self.limit
        if self.has_next_page:
            del list_content[-1]


seconds_one_hour = 60 * 60
seconds_one_day = seconds_one_hour * 24
seconds_one_week = seconds_one_day * 7


class FeedType(Enum):
    CLIK = "C"
    TOPIC = "T"
    HOME = "H"
    USER = "U"
    FEED = "F"

    def get_redis_expiration(self):
        if self.value == "C" or self.value == "T" or self.value == "F":
            return seconds_one_day
        elif (
            self.value == "U"
        ):  # a lot more users than cliks, topics, feeds. Need to set a short expiration time
            return seconds_one_hour
        return seconds_one_week  # only one global home feed, can set a long expiration time


def invalidate_cache(
    redis_pipeline: Redis,
    feed_type: FeedType,
    feed_id: int,
    sort_by: DefaultFilter.Sort,
):
    redis_pipeline.hset(f"{feed_type.value}{feed_id}-session", str(sort_by), 0)
    redis_pipeline.expire(
        f"{feed_type.value}{feed_id}-session", feed_type.get_redis_expiration()
    )


def reset_cache_last_update(
    redis_pipeline: Redis,
    feed_type: FeedType,
    feed_id: int,
    sort_by: DefaultFilter.Sort,
):
    redis_pipeline.hset(
        f"{feed_type.value}{feed_id}-session", str(sort_by), int(time.time())
    )
    redis_pipeline.expire(
        f"{feed_type.value}{feed_id}-session", feed_type.get_redis_expiration()
    )


async def is_cache_updated(
    redis_instance: Redis,
    feed_type: FeedType,
    feed_id: int,
    sort_by: DefaultFilter.Sort,
    timeout_seconds: int,
):
    last_update = int(
        await redis_instance.hget(f"{feed_type.value}{feed_id}-session", str(sort_by))
        or 0
    )
    if time.time() - last_update >= timeout_seconds:
        return False
    return True
