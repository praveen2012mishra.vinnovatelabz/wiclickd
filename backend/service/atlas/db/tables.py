from sqlalchemy import (
    Table,
    Column,
    Integer,
    ForeignKey,
    JSON,
    SMALLINT,
    VARCHAR,
    text as sqltext,
)
from sqlalchemy.dialects.mysql import TIMESTAMP
from weclikd.service.sql import UnsignedBigInt, metadata


ContentTable = Table(
    "content",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True, autoincrement=True),
    Column("likes_0", Integer, nullable=False, default=0),
    Column("likes_1", Integer, nullable=False, default=0),
    Column("likes_2", Integer, nullable=False, default=0),
    Column("likes_3", Integer, nullable=False, default=0),
    Column("num_reports", Integer, nullable=False, default=0),
    Column("state", SMALLINT, nullable=False, default=1),
    Column("num_comments", Integer, nullable=False, default=0),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

ContentLikeTable = Table(
    "content_like",
    metadata,
    Column("user_id", UnsignedBigInt, nullable=False, primary_key=True),
    Column(
        "content_id",
        UnsignedBigInt,
        ForeignKey("content.id"),
        nullable=False,
        primary_key=True,
    ),
    Column("like_type", SMALLINT, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)


ClikPostTable = Table(
    "clik_post",
    metadata,
    Column("clik_id", UnsignedBigInt, ForeignKey("clik.id"), primary_key=True),
    Column("content_id", UnsignedBigInt, ForeignKey("content.id"), primary_key=True),
)

CommentTable = Table(
    "comment",
    metadata,
    Column("content_id", UnsignedBigInt, ForeignKey("content.id"), primary_key=True),
    Column(
        "parent_content_id", UnsignedBigInt, ForeignKey("content.id"), primary_key=True
    ),
    Column("post_id", UnsignedBigInt, ForeignKey("content.id"), default=None),
    Column("clik_id", UnsignedBigInt, default=None),
    Column("user_id", UnsignedBigInt, default=0),
    Column("discussion_id", UnsignedBigInt, ForeignKey("content.id"), default=0),
    Column("level", SMALLINT, default=0),
)

UrlTable = Table(
    "url",
    metadata,
    Column("url", VARCHAR(256), primary_key=True),
    Column("content_id", UnsignedBigInt, ForeignKey("content.id"), nullable=False),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

TopicPostTable = Table(
    "topic_post",
    metadata,
    Column("topic_id", UnsignedBigInt, ForeignKey("topic.id"), primary_key=True),
    Column("content_id", UnsignedBigInt, ForeignKey("content.id"), primary_key=True),
)

UserPostTable = Table(
    "user_post",
    metadata,
    Column("user_id", UnsignedBigInt, ForeignKey("user.id"), primary_key=True),
    Column("content_id", UnsignedBigInt, ForeignKey("content.id"), primary_key=True),
)

UserNewPostTable = Table(
    "user_new_post",
    metadata,
    Column("user_id", UnsignedBigInt, ForeignKey("user.id"), primary_key=True),
    Column("content_id", UnsignedBigInt, ForeignKey("content.id"), primary_key=True),
    Column("priority", UnsignedBigInt),
    Column("data", JSON),
)

PostReadTable = Table(
    "post_read",
    metadata,
    Column("user_id", UnsignedBigInt, ForeignKey("user.id"), primary_key=True),
    Column("content_id", UnsignedBigInt, ForeignKey("content.id"), primary_key=True),
)

FeedPostTable = Table(
    "feed_post",
    metadata,
    Column("feed_id", UnsignedBigInt, primary_key=True),
    Column("content_id", UnsignedBigInt, ForeignKey("content.id"), primary_key=True),
)

ReportTable = Table(
    "report",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True, autoincrement=True),
    Column("content_id", UnsignedBigInt, ForeignKey("content.id")),
    Column("user_id", UnsignedBigInt),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)
