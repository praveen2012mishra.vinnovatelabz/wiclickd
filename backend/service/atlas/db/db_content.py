import sqlalchemy
from sqlalchemy.dialects.mysql import insert as mysql_insert
from typing import Type, List, Union
from google.protobuf.message import Message
from atlas.api.post_pb2 import Post
from atlas.api.comment_pb2 import Comment
from atlas.api.report_pb2 import Report
from weclikd.utils.model import BaseModel
from atlas.api.content_info_pb2 import ContentInfo
from atlas.api.like_pb2 import Likes
from weclikd.service.transaction import Transaction as SqlTransaction
from weclikd.utils import model
from atlas.db.tables import ContentTable, ContentLikeTable, ReportTable
from datetime import datetime

content_expiry = 60 * 60 * 24  # 1 day


def row_to_proto(row) -> Type[Message]:
    sql = {**row}
    sql["data"]["content_info"]["id"] = sql["id"]
    sql["data"]["content_info"]["created"] = BaseModel.to_iso_string(sql["created"])
    sql["data"]["content_info"]["num_comments"] = sql["num_comments"]
    sql["data"]["content_info"]["num_reports"] = sql["num_reports"]
    sql["data"]["content_info"]["likes"] = {
        "red": sql["likes_0"],
        "silver": sql["likes_1"],
        "gold": sql["likes_2"],
        "diamond": sql["likes_3"],
    }

    content_type = sql["data"]["content_info"]["type"]
    if content_type == "COMMENT" or content_type == ContentInfo.COMMENT:
        return model.row_to_proto(sql, Comment())
    else:
        return model.row_to_proto(sql, Post())


async def insert(db: SqlTransaction, content: Type[Message]):
    data = BaseModel.proto_to_data(content)
    if "created" in data["content_info"]:
        del data["content_info"]["created"]
    cursor = await db.execute(
        sqlalchemy.insert(
            ContentTable,
            values={
                "data": data,
                "likes_0": 0,
                "likes_1": 0,
                "likes_2": 0,
                "likes_3": 0,
                "state": ContentInfo.ContentState.CREATED,
                "num_reports": 0,
                "num_comments": 0,
            },
        )
    )
    content.content_info.id = cursor.lastrowid
    return content.content_info.id


async def update(db: SqlTransaction, content: Type[Message]):
    data = BaseModel.proto_to_data(content, remove_fields=["content_info"])
    data["content_info"] = {
        "type": content.content_info.type,
        "user_id": content.content_info.user_id,
        "text": content.content_info.text,
    }

    await db.execute(
        sqlalchemy.update(
            ContentTable,
            whereclause=ContentTable.c.id == content.content_info.id,
            values={
                "data": data,
            },
        )
    )


async def get_content(db: SqlTransaction, content_id: int) -> Union[Post, Comment]:
    row = await db.fetch_one(
        sqlalchemy.select([ContentTable], whereclause=ContentTable.c.id == content_id)
    )
    if not row:
        return None

    return row_to_proto(row)


async def increment_comments_count(db: SqlTransaction, comment: Comment):
    await db.execute(
        sqlalchemy.update(
            ContentTable,
            whereclause=(ContentTable.c.id.in_(comment.thread_ids)),
            values={"num_comments": ContentTable.c.num_comments + 1},
        )
    )


async def decrement_comments_count(db: SqlTransaction, comment: Comment):
    await db.execute(
        sqlalchemy.update(
            ContentTable,
            whereclause=(ContentTable.c.id.in_(comment.thread_ids)),
            values={"num_comments": ContentTable.c.num_comments - 1},
        )
    )


async def insert_report(
    db: SqlTransaction, content_id: int, report: Report, user_id: int
):
    await db.execute(
        sqlalchemy.insert(
            ReportTable,
            values={
                "content_id": content_id,
                "user_id": user_id,
                "data": {
                    "type": report.type,
                    "off_topic_cliks": list(report.off_topic_cliks),
                    "off_topic_topics": list(report.off_topic_topics),
                },
            },
        )
    )


async def increment_report_count(db: SqlTransaction, content_id: int):
    await db.execute(
        sqlalchemy.update(
            ContentTable,
            whereclause=ContentTable.c.id == content_id,
            values={"num_reports": ContentTable.c.num_reports + 1},
        )
    )


async def get_like(db: SqlTransaction, content_id: int, user_id: int):
    row = await db.fetch_one(
        sqlalchemy.select(
            [ContentLikeTable.c.like_type, ContentLikeTable.c.created],
            whereclause=sqlalchemy.and_(
                ContentLikeTable.c.content_id == content_id,
                ContentLikeTable.c.user_id == user_id,
            ),
        )
    )
    return (row.like_type, row.created) if row else (Likes.NONE, None)


async def insert_like(
    db: SqlTransaction,
    content_id: int,
    like_type: Likes.LikeType,
    user_id: int,
    ts: datetime,
):
    values = {
        "user_id": user_id,
        "content_id": content_id,
        "like_type": like_type,
        "created": ts,
    }
    await db.execute(
        mysql_insert(ContentLikeTable).values(values).on_duplicate_key_update(values)
    )


async def remove_like(db: SqlTransaction, content_id: int, user_id: int):
    await db.execute(
        sqlalchemy.delete(
            ContentLikeTable,
            whereclause=sqlalchemy.and_(
                ContentLikeTable.c.user_id == user_id,
                ContentLikeTable.c.content_id == content_id,
            ),
        )
    )


async def increment_like_count(
    db: SqlTransaction, content_id: int, like_type: Likes.LikeType
):
    if like_type == Likes.LikeType.RED:
        await db.execute(
            sqlalchemy.update(
                ContentTable,
                whereclause=content_id == ContentTable.c.id,
                values={"likes_0": ContentTable.c.likes_0 + 1},
            )
        )
    elif like_type == Likes.LikeType.SILVER:
        await db.execute(
            sqlalchemy.update(
                ContentTable,
                whereclause=content_id == ContentTable.c.id,
                values={"likes_1": ContentTable.c.likes_1 + 1},
            )
        )
    elif like_type == Likes.LikeType.GOLD:
        await db.execute(
            sqlalchemy.update(
                ContentTable,
                whereclause=content_id == ContentTable.c.id,
                values={"likes_2": ContentTable.c.likes_2 + 1},
            )
        )
    elif like_type == Likes.LikeType.DIAMOND:
        await db.execute(
            sqlalchemy.update(
                ContentTable,
                whereclause=content_id == ContentTable.c.id,
                values={"likes_3": ContentTable.c.likes_3 + 1},
            )
        )


async def decrement_like_count(
    db: SqlTransaction, content_id: int, like_type: Likes.LikeType
):
    if like_type == Likes.LikeType.RED:
        await db.execute(
            sqlalchemy.update(
                ContentTable,
                whereclause=content_id == ContentTable.c.id,
                values={"likes_0": ContentTable.c.likes_0 - 1},
            )
        )
    elif like_type == Likes.LikeType.SILVER:
        await db.execute(
            sqlalchemy.update(
                ContentTable,
                whereclause=content_id == ContentTable.c.id,
                values={"likes_1": ContentTable.c.likes_1 - 1},
            )
        )
    elif like_type == Likes.LikeType.GOLD:
        await db.execute(
            sqlalchemy.update(
                ContentTable,
                whereclause=content_id == ContentTable.c.id,
                values={"likes_2": ContentTable.c.likes_2 - 1},
            )
        )
    elif like_type == Likes.LikeType.DIAMOND:
        await db.execute(
            sqlalchemy.update(
                ContentTable,
                whereclause=content_id == ContentTable.c.id,
                values={"likes_3": ContentTable.c.likes_3 - 1},
            )
        )


async def populate_user_like(
    db: SqlTransaction, contents: List[Union[Post, Comment]], user_id: int
) -> None:
    if not user_id:
        return

    content_ids = [content.content_info.id for content in contents]
    rows = await db.fetch_all(
        sqlalchemy.select(
            [ContentLikeTable.c.content_id, ContentLikeTable.c.like_type],
            whereclause=sqlalchemy.and_(
                ContentLikeTable.c.user_id == user_id,
                ContentLikeTable.c.content_id.in_(tuple(content_ids)),
            ),
        )
    )
    liked_contents = {row.content_id: row.like_type for row in rows}
    if not liked_contents:
        return

    for content in contents:
        content.content_info.user_like_type = liked_contents.get(
            content.content_info.id, 0
        )


async def remove(db: SqlTransaction, content_id: int):
    await db.execute(
        sqlalchemy.update(
            ContentTable,
            whereclause=content_id == ContentTable.c.id,
            values={"state": ContentInfo.ContentState.REMOVED},
        )
    )
