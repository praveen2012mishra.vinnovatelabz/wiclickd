import sqlalchemy
import json
import logging
from typing import List, Tuple, Optional, Dict
from google.protobuf.json_format import ParseDict
from atlas.api.post_pb2 import Post
from weclikd.utils.model import BaseModel
from weclikd.service.transaction import Transaction as SqlTransaction
from weclikd.service.sql import sql_instance
from atlas.db.tables import ContentTable, UrlTable, FeedPostTable
from atlas.db import db_content


async def insert(db: SqlTransaction, post: Post) -> int:
    post.content_info.id = await db_content.insert(db, post)
    db.redis.zadd(f"H-trend", 1, post.content_info.id)
    db.redis.expire(f"H-trend", db_content.content_expiry)
    return post.content_info.id


async def update(db: SqlTransaction, post: Post):
    await db_content.update(db, post)
    post_info = json.dumps(
        BaseModel.proto_to_data(post, remove_fields=["content_info", "discussion"])
    )
    db.redis.set(f"p{post.content_info.id}", post_info)


async def fetch_many(db: SqlTransaction, content_ids: List[int]) -> List[Post]:
    id_to_posts = {}
    rows = await db.fetch_all(
        sqlalchemy.select([ContentTable], ContentTable.c.id.in_(tuple(content_ids)))
    )
    for row in rows:
        post = db_content.row_to_proto(row)
        id_to_posts[row.id] = post

    # since sql does not return items in order, we have to order them here.
    populated_posts = []
    for content_id in content_ids:
        post = id_to_posts.get(int(content_id))
        if post is None:
            logging.critical(f"Failed to get Post:{content_id}.")
        else:
            populated_posts.append(post)
    return populated_posts


async def fetch_using_url(db: SqlTransaction, url: str) -> Optional[Post]:
    row = await db.fetch_one(
        sqlalchemy.select(
            [ContentTable],
            from_obj=ContentTable.join(
                UrlTable, UrlTable.c.content_id == ContentTable.c.id
            ),
            whereclause=UrlTable.c.url == url,
        )
    )
    return db_content.row_to_proto(row) if row else None


async def fetch_urls_shared_to_external_feed(feed_id: int, urls: List[str]):
    rows = await sql_instance.fetch_all(
        sqlalchemy.select(
            [UrlTable.c.url],
            from_obj=ContentTable.join(
                UrlTable, UrlTable.c.content_id == ContentTable.c.id
            ).join(FeedPostTable, FeedPostTable.c.content_id == ContentTable.c.id),
            whereclause=sqlalchemy.and_(
                UrlTable.c.url.in_(tuple(urls)), FeedPostTable.c.feed_id == feed_id
            ),
        )
    )
    return [row.url for row in rows]


async def fetch_many_using_url(urls: List[str]) -> Dict[str, Post]:
    rows = await sql_instance.fetch_all(
        sqlalchemy.select(
            [ContentTable],
            from_obj=ContentTable.join(
                UrlTable, UrlTable.c.content_id == ContentTable.c.id
            ),
            whereclause=UrlTable.c.url.in_(tuple(urls)),
        )
    )
    url_to_post = {url: None for url in urls}
    for row in rows:
        post = db_content.row_to_proto(row)
        url_to_post[post.url] = post
    return url_to_post


async def insert_url(db: SqlTransaction, content_id: int, url: str):
    return await db.execute(
        sqlalchemy.insert(
            UrlTable, values={"url": url, "content_id": content_id, "data": {}}
        )
    )


async def remove(db: SqlTransaction, post: Post):
    db.redis.zrem(f"H-trend", post.content_info.id)
    await db_content.remove(db, post.content_info.id)
