import sqlalchemy
from typing import List, Dict, Tuple
from atlas.api.content_info_pb2 import ContentInfo
from atlas.api.post_pb2 import ReasonsShared
from weclikd.common.filter_pb2 import DefaultFilter
from weclikd.service.sql import get_sql_client
from weclikd.service import redis
from atlas.db.tables import (
    ContentTable,
    ClikPostTable,
    TopicPostTable,
    UserPostTable,
    FeedPostTable,
    UserNewPostTable,
    PostReadTable,
)
from atlas.db import feed_utils
from atlas.db.feed_utils import NewFeedPaginateTracker, FeedPaginateTracker, FeedType
import logging
import time
import itertools
from datetime import datetime, timedelta
from google.protobuf.json_format import MessageToJson, ParseDict


async def get_new_feed(
    feed_id: int,
    feed_type: FeedType,
    paginator_tracker: NewFeedPaginateTracker,
    user_id: int = 0,
    newer_than: datetime = None,
) -> List[id]:
    if not feed_id:
        feed_id = ""

    if user_id and paginator_tracker.after:
        # if user is logged in and is paginating, mark posts as read
        await _mark_as_read(
            feed_id=feed_id,
            feed_type=feed_type,
            user_id=user_id,
            start_cursor=paginator_tracker.previous,
            end_cursor=paginator_tracker.after,
        )

    # get posts from mysql
    content_ids = await _get_new_feed_sql(
        feed_type, feed_id, paginator_tracker, user_id, newer_than=newer_than
    )

    if content_ids:
        paginator_tracker.previous = content_ids[0]
        paginator_tracker.after = content_ids[-1]
    return content_ids


async def get_user_new_feed(
    user_id: int, paginator_tracker: NewFeedPaginateTracker
) -> Dict[int, ReasonsShared]:
    if not paginator_tracker.after:
        if await feed_utils.is_cache_updated(
            redis_instance=await redis.get(),
            feed_type=FeedType.HOME,
            feed_id=user_id,
            sort_by=DefaultFilter.NEW,
            timeout_seconds=60,
        ):
            logging.info(f"User {user_id} feed is already updated.")
        else:
            posts_favorites, posts_following = await _get_new_posts_from_following(
                user_id
            )
            # alternate posts so that spammy feeds do not clog the front page
            user_new_feed = _alternate_posts(posts_favorites, posts_following)
            logging.info(f"Adding {len(user_new_feed)} posts to user's new feed")
            await _add_new_posts_to_db(user_id=user_id, posts=user_new_feed)
            await _reset_new_posts_cache(user_id)

    else:
        # user is paginating, get the next items in the database
        logging.info(f"User {user_id} feed paginating {paginator_tracker.after}")
        await _mark_as_read_home_feed(
            user_id=user_id,
            start_cursor=paginator_tracker.previous,
            end_cursor=paginator_tracker.after,
        )

    return await _get_new_posts_from_db(
        user_id=user_id, paginator_tracker=paginator_tracker
    )


async def _get_new_feed_sql(
    feed_type: FeedType,
    feed_id: int,
    paginator_tracker: FeedPaginateTracker,
    user_id: int,
    newer_than: datetime = None,
) -> List[int]:
    whereclauses = [ContentTable.c.state != ContentInfo.ContentState.REMOVED]
    if feed_type == FeedType.CLIK:
        post_table = ClikPostTable
        whereclauses.append(ClikPostTable.c.clik_id == feed_id)
    elif feed_type == FeedType.USER:
        post_table = UserPostTable
        whereclauses.append(UserPostTable.c.user_id == feed_id)
    elif feed_type == FeedType.TOPIC:
        post_table = TopicPostTable
        whereclauses.append(TopicPostTable.c.topic_id == feed_id)
    elif feed_type == FeedType.HOME:
        post_table = TopicPostTable
    elif feed_type == FeedType.FEED:
        post_table = FeedPostTable
        whereclauses.append(FeedPostTable.c.feed_id == feed_id)
    else:
        raise Exception(f"Invalid feed type {feed_type}")

    join = post_table.join(ContentTable, post_table.c.content_id == ContentTable.c.id)

    if paginator_tracker.after:
        whereclauses.append(post_table.c.content_id < paginator_tracker.after)
    if newer_than:
        whereclauses.append(ContentTable.c.created >= newer_than)

    db = await get_sql_client()
    rows = await db.fetch_all(
        sqlalchemy.select(
            [ContentTable.c.id],
            from_obj=join,
            whereclause=sqlalchemy.and_(*whereclauses),
            limit=paginator_tracker.limit,
            order_by=sqlalchemy.desc(ContentTable.c.id),
        ).distinct()
    )
    paginator_tracker.set_has_next_page(rows)

    return [row.id for row in rows]


class PostsFromSource:
    def __init__(self, feed_type: FeedType, feed_id: int, content_ids: List[int]):
        self.feed_type = feed_type
        self.feed_id = feed_id
        self.content_ids = content_ids
        self.length = len(content_ids)
        self.index = 0

    def __iter__(self):
        self.index = 0
        return self

    def __next__(self):
        if self.index >= self.length:
            raise StopIteration
        content_id = self.content_ids[self.index]
        self.index += 1
        return self.feed_type, self.feed_id, content_id


async def _get_new_posts_from_following(
    user_id: int,
) -> Tuple[List[PostsFromSource], List[PostsFromSource]]:
    from artemis.model import external_feed_model
    from athena.model import topic_model
    from eros.model import model_user
    from hestia.model import clik_model

    following_external_feeds = await external_feed_model.retrieve_following(user_id)
    following_topics = await topic_model.retrieve_following(user_id)
    following_users = await model_user.retrieve_following(user_id)
    following_cliks = await clik_model.retrieve_following(user_id)

    post_favorites = []
    post_following = []
    newer_than = datetime.now() - timedelta(days=31)
    for each in following_cliks:
        content_ids = await get_new_feed(
            feed_id=each.clik.id,
            feed_type=FeedType.CLIK,
            paginator_tracker=NewFeedPaginateTracker(limit=100),
            user_id=user_id,
            newer_than=newer_than,
        )
        if each.settings.follow_type == 1:
            post_following.append(
                PostsFromSource(FeedType.CLIK, each.clik.id, content_ids)
            )
        else:
            post_following.append(
                PostsFromSource(FeedType.CLIK, each.clik.id, content_ids)
            )

    for each in following_users:
        content_ids = await get_new_feed(
            feed_id=each.user.id,
            feed_type=FeedType.USER,
            paginator_tracker=NewFeedPaginateTracker(limit=100),
            user_id=user_id,
            newer_than=newer_than,
        )
        if each.settings.follow_type == 1:
            post_following.append(
                PostsFromSource(FeedType.USER, each.user.id, content_ids)
            )
        else:
            post_following.append(
                PostsFromSource(FeedType.USER, each.user.id, content_ids)
            )

    for each in following_topics:
        content_ids = await get_new_feed(
            feed_id=each.topic.id,
            feed_type=FeedType.TOPIC,
            paginator_tracker=NewFeedPaginateTracker(limit=100),
            user_id=user_id,
            newer_than=newer_than,
        )
        if each.settings.follow_type == 1:
            post_following.append(
                PostsFromSource(FeedType.TOPIC, each.topic.id, content_ids)
            )
        else:
            post_following.append(
                PostsFromSource(FeedType.TOPIC, each.topic.id, content_ids)
            )

    for each in following_external_feeds:
        content_ids = await get_new_feed(
            feed_id=each.feed.id,
            feed_type=FeedType.FEED,
            paginator_tracker=NewFeedPaginateTracker(limit=100),
            user_id=user_id,
            newer_than=newer_than,
        )
        if each.settings.follow_type == 1:
            post_following.append(
                PostsFromSource(FeedType.FEED, each.feed.id, content_ids)
            )
        else:
            post_following.append(
                PostsFromSource(FeedType.FEED, each.feed.id, content_ids)
            )

    return post_favorites, post_following


def _alternate_posts(
    post_favorites: List[PostsFromSource], post_following: List[PostsFromSource]
) -> Dict[int, ReasonsShared]:
    alternated_posts = {}  # content_id => Post

    post_favorites_iterator = itertools.zip_longest(*post_favorites)
    post_following_iterator = itertools.zip_longest(*post_following)
    while True:
        favorite_posts_0 = next(post_favorites_iterator, None)
        favorite_posts_1 = next(post_favorites_iterator, None)
        following_post = next(post_following_iterator, None)

        if (
            favorite_posts_0 is None
            and favorite_posts_1 is None
            and following_post is None
        ):
            break

        def add_posts(posts):
            for post in posts or []:
                if not post:
                    continue
                feed_type, feed_id, content_id = post
                if content_id in alternated_posts:
                    reasons_shared = alternated_posts[content_id]
                else:
                    reasons_shared = ReasonsShared()
                    alternated_posts[content_id] = reasons_shared

                if feed_type == FeedType.CLIK:
                    reasons_shared.clik_ids.append(feed_id)
                elif feed_type == FeedType.USER:
                    reasons_shared.user_ids.append(feed_id)
                elif feed_type == FeedType.TOPIC:
                    reasons_shared.topic_ids.append(feed_id)
                elif feed_type == FeedType.FEED:
                    reasons_shared.feed_ids.append(feed_id)

        add_posts(favorite_posts_0)
        add_posts(favorite_posts_1)
        add_posts(following_post)

    return alternated_posts


async def _add_new_posts_to_db(user_id: int, posts: Dict[int, ReasonsShared]):
    if not posts:
        return
    current_time = int(time.time()) + 1000
    values = [
        f"({user_id}, {content_id}, {current_time - i}, '{{ \"reasons_shared\": {MessageToJson(reasons_shared, preserving_proto_field_name=True)} }}')"
        for i, (content_id, reasons_shared) in enumerate(posts.items())
    ]

    sql_query = f"""
        INSERT INTO user_new_post(user_id, content_id, priority, data) VALUES 
        {', '.join(values)}
        ON DUPLICATE KEY
        UPDATE user_id=values(user_id), content_id=values(content_id), priority=values(priority), data=values(data)
    """
    db = await get_sql_client()
    await db.execute(sql_query)


async def _reset_new_posts_cache(user_id: int):
    redis_pipeline = await redis.get_pipeline()
    feed_utils.reset_cache_last_update(
        redis_pipeline, FeedType.HOME, user_id, DefaultFilter.NEW
    )
    await redis_pipeline.execute()


async def _get_new_posts_from_db(
    user_id: int, paginator_tracker: NewFeedPaginateTracker
) -> Dict[int, ReasonsShared]:
    db = await get_sql_client()
    if paginator_tracker.after:
        rows = await db.fetch_all(
            sqlalchemy.select(
                [UserNewPostTable],
                from_obj=UserNewPostTable.outerjoin(
                    PostReadTable,
                    sqlalchemy.and_(
                        PostReadTable.c.content_id == UserNewPostTable.c.content_id,
                        PostReadTable.c.user_id == user_id,
                    ),
                ),
                whereclause=sqlalchemy.and_(
                    UserNewPostTable.c.user_id == user_id,
                    PostReadTable.c.content_id == None,
                    UserNewPostTable.c.priority < int(paginator_tracker.after),
                ),
                limit=paginator_tracker.limit,
                order_by=sqlalchemy.desc(UserNewPostTable.c.priority),
            )
        )
    else:
        rows = await db.fetch_all(
            sqlalchemy.select(
                [UserNewPostTable],
                from_obj=UserNewPostTable.outerjoin(
                    PostReadTable,
                    sqlalchemy.and_(
                        PostReadTable.c.content_id == UserNewPostTable.c.content_id,
                        PostReadTable.c.user_id == user_id,
                    ),
                ),
                whereclause=sqlalchemy.and_(
                    UserNewPostTable.c.user_id == user_id,
                    PostReadTable.c.content_id == None,
                ),
                limit=paginator_tracker.limit,
                order_by=sqlalchemy.desc(UserNewPostTable.c.priority),
            )
        )
    paginator_tracker.has_next_page = paginator_tracker.limit <= len(rows)
    paginator_tracker.previous = rows[0].priority if rows else None
    if paginator_tracker.has_next_page:
        paginator_tracker.after = rows[-2].priority if rows else None
        return {
            row.content_id: ParseDict(
                row.data["reasons_shared"],
                ReasonsShared(),
                ignore_unknown_fields=True,
            )
            for row in rows[:-1]
        }
    else:
        paginator_tracker.after = rows[-1].priority if rows else None
        return {
            row.content_id: ParseDict(
                row.data["reasons_shared"],
                ReasonsShared(),
                ignore_unknown_fields=True,
            )
            for row in rows
        }


async def _mark_as_read_home_feed(user_id: int, start_cursor: int, end_cursor: int):
    db = await get_sql_client()
    rows = await db.fetch_all(
        sqlalchemy.select(
            [UserNewPostTable.c.content_id],
            whereclause=sqlalchemy.and_(
                UserNewPostTable.c.user_id == user_id,
                UserNewPostTable.c.priority >= int(end_cursor),
                UserNewPostTable.c.priority <= int(start_cursor),
            ),
        )
    )

    await db.execute(
        sqlalchemy.delete(
            UserNewPostTable,
            whereclause=sqlalchemy.and_(
                UserNewPostTable.c.user_id == user_id,
                UserNewPostTable.c.priority >= int(end_cursor),
                UserNewPostTable.c.priority <= int(start_cursor),
            ),
        )
    )

    if rows:
        logging.info(f"Marking {len(rows)} posts as read.")
        posts_read = [f"({user_id}, {row.content_id})" for row in rows]
        sql_query = f"""
            INSERT INTO post_read(user_id, content_id) VALUES 
            {', '.join(posts_read)}
            ON DUPLICATE KEY
            UPDATE user_id=values(user_id), content_id=values(content_id)
        """
        await db.execute(sql_query)


async def _mark_as_read(
    feed_id: int, feed_type: FeedType, user_id: int, start_cursor: int, end_cursor: int
):
    db = await get_sql_client()

    if feed_type == FeedType.CLIK:
        post_table = ClikPostTable
        post_table_feed_id_col = ClikPostTable.c.clik_id
    elif feed_type == FeedType.USER:
        post_table = UserPostTable
        post_table_feed_id_col = UserPostTable.c.user_id
    elif feed_type == FeedType.TOPIC:
        post_table = TopicPostTable
        post_table_feed_id_col = TopicPostTable.c.topic_id
    elif feed_type == FeedType.FEED:
        post_table = FeedPostTable
        post_table_feed_id_col = FeedPostTable.c.feed_id
    else:
        raise Exception(f"Invalid feed type {feed_type}")

    rows = await db.fetch_all(
        sqlalchemy.select(
            [post_table.c.content_id],
            whereclause=sqlalchemy.and_(
                post_table_feed_id_col == int(feed_id),
                post_table.c.content_id >= int(end_cursor),
                post_table.c.content_id <= int(start_cursor),
            ),
        )
    )

    if rows:
        logging.info(f"Marking {len(rows)} posts as read.")
        posts_read = [f"({user_id}, {row.content_id})" for row in rows]
        sql_query = f"""
            INSERT INTO post_read(user_id, content_id) VALUES 
            {', '.join(posts_read)}
            ON DUPLICATE KEY
            UPDATE user_id=values(user_id), content_id=values(content_id)
        """
        await db.execute(sql_query)
