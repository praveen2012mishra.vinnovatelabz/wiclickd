from aioredis import Redis
from atlas.db import feed_utils
from atlas.db.feed_utils import FeedType, FeedPaginateTracker, NewFeedPaginateTracker
from atlas.api.post_pb2 import Post, FeedFilter
from atlas.db import db_feed_new, db_post
from weclikd.common.filter_pb2 import DefaultFilter
from weclikd.service.transaction import Transaction as SqlTransaction
from weclikd.utils import weclikd_id
from weclikd.service import redis
import math
import time
import logging
from typing import List


async def get_trending_feed(
    feed_id: int,
    feed_type: FeedType,
    paginator_tracker: FeedPaginateTracker,
    user_id: int,
) -> List[int]:
    redis_instance = await redis.get()
    await _refresh_trending(
        redis_instance, feed_type, feed_id, paginator_tracker, user_id
    )

    if user_id and feed_type == FeedType.HOME:
        feed_id = f"{user_id}"
    if not user_id and feed_type == FeedType.HOME:
        feed_id = ""

    content_ids = await _get_trending_feed(
        redis_instance, f"{feed_type.value}{feed_id}-trend", paginator_tracker
    )
    if not content_ids:
        await _refresh_trending(
            redis_instance,
            feed_type,
            feed_id,
            paginator_tracker,
            user_id,
            force_refresh=True,
        )

    return await _get_trending_feed(
        redis_instance, f"{feed_type.value}{feed_id}-trend", paginator_tracker
    )


def _update_timeout(feed_type: FeedType):
    if feed_type == FeedType.USER:
        # 30 minutes longer than other feeds since user feeds is less likely to change
        return 30 * 60
    else:
        # 5 minutes
        return 5 * 60


async def _get_trending_not_updated(
    feed_type: FeedType, feed_ids: List[int]
) -> List[int]:
    # returns the feeds that need to be refreshed
    pipeline = await redis.get_pipeline()
    for feed_id in feed_ids:
        pipeline.hget(
            f"{feed_type.value}{feed_id}-session", str(DefaultFilter.TRENDING)
        )
    results = await pipeline.execute()

    current_time = time.time()
    timeout = _update_timeout(feed_type)
    needs_updating = []
    for result, feed_id in zip(results, feed_ids):
        result = result if result else 0
        if current_time - result >= timeout:
            needs_updating.append(feed_id)
    return needs_updating


def update_score(redis_pipeline: Redis, post: Post, current_time: int):
    score = _get_post_score(post, current_time)
    if score <= 0:
        return

    for clik in post.cliks:
        redis_pipeline.zadd(
            f"C{weclikd_id.string_to_id(clik)}-trend",
            score,
            post.content_info.id,
            changed=True,
        )
        redis_pipeline.zremrangebyrank(
            f"C{weclikd_id.string_to_id(clik)}-trend", start=0, stop=-100
        )
    for topic in post.topics:
        redis_pipeline.zadd(
            f"T{weclikd_id.string_to_id(topic)}-trend",
            score,
            post.content_info.id,
            changed=True,
        )
        redis_pipeline.zremrangebyrank(
            f"T{weclikd_id.string_to_id(topic)}-trend", start=0, stop=-100
        )
    if post.external_feed.id:
        redis_pipeline.zadd(
            f"F{post.external_feed.id}-trend", score, post.content_info.id, changed=True
        )
        redis_pipeline.zremrangebyrank(
            f"F{post.external_feed.id}-trend", start=0, stop=-100
        )
    redis_pipeline.zadd("H-trend", score, post.content_info.id, changed=True)
    redis_pipeline.zremrangebyrank(f"H-trend", start=0, stop=-1000)


def _get_post_score(post: Post, current_time: int) -> int:
    if post.content_info.num_comments == 0:
        return 0

    comments_likes_score = (
        post.content_info.likes.red
        + post.content_info.likes.silver * 2
        + post.content_info.likes.gold * 4
        + post.content_info.likes.diamond * 8
        + post.content_info.num_comments * 16
        - post.content_info.num_reports * 16
    )

    if comments_likes_score >= 200000:
        comments_likes_score = 200000
    elif comments_likes_score < 1:
        comments_likes_score = 1

    post_age_days = (current_time - post.content_info.created.seconds + 10) / 86400.0
    return (100000 + 25000 * math.log(comments_likes_score, 2)) / math.pow(
        2, post_age_days / 7
    )


async def _refresh_trending(
    redis_instance: Redis,
    feed_type: FeedType,
    feed_id: int,
    paginator_tracker: FeedPaginateTracker,
    user_id: int,
    force_refresh: bool = False,
):
    if paginator_tracker.after:  # don't need to refresh as the user is paginating
        return

    if (
        not force_refresh
        and feed_type in [FeedType.CLIK, FeedType.TOPIC, FeedType.FEED]
        or (feed_type == FeedType.HOME and not user_id)
    ):
        # Trending for these sections are updated in atlas/refresh-trending REST call
        # This is because these sources are more likely checked by many users
        # so it is more efficient to have a separate process updating the trending
        # instead of having to check to refresh trending on each call to get the feed
        return

    if await feed_utils.is_cache_updated(
        redis_instance,
        feed_type,
        user_id,
        DefaultFilter.TRENDING,
        _update_timeout(feed_type),
    ):
        return

    if user_id and feed_type == FeedType.HOME:
        # special handling for user home trending
        # combine all the trending section of the sources that the user follows
        await _refresh_user_home_trending(redis_instance, user_id)
        return

    await _refresh_trending_default(redis_instance, feed_type, feed_id)


async def _refresh_user_home_trending(redis_instance: Redis, user_id: int):
    # get all following
    logging.info(f"Retrieving trending feed for user {user_id}")
    from artemis.model import external_feed_model
    from athena.model import topic_model
    from eros.model import model_user
    from hestia.model import clik_model

    following_external_feeds = await external_feed_model.retrieve_following(user_id)
    following_topics = await topic_model.retrieve_following(user_id)
    following_users = await model_user.retrieve_following(user_id)
    following_cliks = await clik_model.retrieve_following(user_id)

    keys = []
    for following in following_external_feeds:
        keys.append((f"F{following.feed.id}-trend", following.settings.follow_type))
    for following in following_topics:
        keys.append((f"T{following.topic.id}-trend", following.settings.follow_type))
    for following in following_cliks:
        keys.append((f"C{following.clik.id}-trend", following.settings.follow_type))

    user_ids = []
    for following in following_users:
        user_ids.append(following.user.id)
        keys.append((f"U{following.user.id}-trend", following.settings.follow_type))

    if not keys:
        await redis_instance.delete(f"H{user_id}-trend")
        return

    user_trending_to_update = await _get_trending_not_updated(FeedType.USER, user_ids)
    for following_user_id in user_trending_to_update:
        await _refresh_trending_default(
            redis_instance, FeedType.USER, following_user_id
        )

    redis_pipeline = await redis.get_pipeline()
    feed_utils.reset_cache_last_update(
        redis_pipeline, FeedType.HOME, user_id, DefaultFilter.TRENDING
    )
    await redis_pipeline.execute()

    num_items = await redis_instance.zunionstore(
        f"H{user_id}-trend", *keys, with_weights=True
    )
    logging.info(f"Refreshed user home trending for {user_id} with {num_items} posts")


async def _refresh_trending_default(
    redis_instance: Redis, feed_type: FeedType, feed_id: int
):
    # get top 100 posts from database
    # store them in redis according to its score
    # TODO: for user's trending feed, it should be ordered by the comments score, but we will do that later.
    post_ids = await db_feed_new.get_new_feed(
        feed_id=feed_id,
        feed_type=feed_type,
        paginator_tracker=NewFeedPaginateTracker.init(
            FeedFilter(
                num_posts=100,
                sort=FeedFilter.Sort.NEW,
            )
        ),
    )
    if not post_ids:
        return

    async with SqlTransaction() as db:
        posts = await db_post.fetch_many(db=db, content_ids=post_ids)
    current_time = time.time()
    redis_args = []
    for post in posts:
        score = _get_post_score(post, current_time)
        if score > 0:
            redis_args.extend([score, post.content_info.id])

    if redis_args:
        await redis_instance.zadd(
            f"{feed_type.value}{feed_id}-trend",
            redis_args[0],
            redis_args[1],
            *redis_args[2:],
        )


async def _get_trending_feed(
    redis_instance: Redis,
    redis_key: str,
    paginator_tracker: FeedPaginateTracker,
) -> List[int]:
    # always look for trending feed from redis
    # assume that it is populated there by previous calls
    if paginator_tracker.after:
        feed_content_ids = await redis_instance.eval(
            script=(
                "local rank = redis.call('ZREVRANK', KEYS[1], ARGV[1]); "
                "if not rank then\n"
                "  return {};"
                "end\n"
                "return redis.call('ZREVRANGE', KEYS[1], rank+1, rank+tonumber(ARGV[2]));"
            ),
            keys=[redis_key],
            args=[paginator_tracker.after, paginator_tracker.limit],
        )
    else:
        feed_content_ids = await redis_instance.zrevrange(
            redis_key, start=0, stop=paginator_tracker.limit - 1
        )
    paginator_tracker.everything_cached = True
    paginator_tracker.set_has_next_page(feed_content_ids)
    return feed_content_ids
