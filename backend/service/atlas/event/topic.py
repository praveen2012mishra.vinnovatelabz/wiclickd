from weclikd.service import pubsub
from atlas.event import content_pb2

refresh_trending_topic = pubsub.Topic("refresh-trending")
create_post_topic = pubsub.Topic("post-create", content_pb2.CreatePostEvent)
delete_post_topic = pubsub.Topic("post-delete", content_pb2.DeletePostEvent)
update_post_topic = pubsub.Topic("post-update", content_pb2.EditPostEvent)
create_comment_topic = pubsub.Topic("comment-create", content_pb2.CreateCommentEvent)
delete_comment_topic = pubsub.Topic("comment-delete", content_pb2.DeleteCommentEvent)
update_comment_topic = pubsub.Topic("comment-update", content_pb2.EditCommentEvent)

like_content_topic = pubsub.Topic("content-like", content_pb2.LikeContentEvent)
