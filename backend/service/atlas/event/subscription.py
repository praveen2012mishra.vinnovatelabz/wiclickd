from weclikd.service import pubsub
from atlas.event.topic import refresh_trending_topic
import hestia.event.topic as hestia_topic
import athena.event.topic as athena_topic
import artemis.event.topic as artemis_topic
import eros.event.topic as eros_topic

refresh_trending_subscription = pubsub.Subscription(refresh_trending_topic, "atlas")
delete_topic_subscription = pubsub.Subscription(
    athena_topic.delete_topic_topic, "atlas"
)
delete_external_feed_subscription = pubsub.Subscription(
    artemis_topic.delete_external_feed_topic, "atlas"
)
delete_clik_subscription = pubsub.Subscription(hestia_topic.delete_clik_topic, "atlas")
delete_user_subscription = pubsub.Subscription(eros_topic.delete_user_topic, "atlas")

follow_clik_subscription = pubsub.Subscription(
    hestia_topic.follow_clik_topic, component_name="atlas"
)
unfollow_clik_subscription = pubsub.Subscription(
    hestia_topic.unfollow_clik_topic, component_name="atlas"
)
follow_topic_subscription = pubsub.Subscription(
    athena_topic.follow_topic_topic, component_name="atlas"
)
unfollow_topic_subscription = pubsub.Subscription(
    athena_topic.unfollow_topic_topic, component_name="atlas"
)
follow_external_feed_subscription = pubsub.Subscription(
    artemis_topic.follow_external_feed_topic, component_name="atlas"
)
unfollow_external_feed_subscription = pubsub.Subscription(
    artemis_topic.unfollow_external_feed_topic, component_name="atlas"
)
follow_user_subscription = pubsub.Subscription(
    eros_topic.follow_user_topic, component_name="atlas"
)
unfollow_user_subscription = pubsub.Subscription(
    eros_topic.unfollow_user_topic, component_name="atlas"
)
