from sanic import Blueprint
import logging
from weclikd.service import pubsub
from atlas.event import subscription
from weclikd.utils.session import Session
from athena.event.topic_pb2 import DeleteTopicEvent
from atlas.model import model_feed


atlas_api = Blueprint("atlas", url_prefix="/atlas")


async def delete_topic(event: DeleteTopicEvent, session: Session):
    logging.info(f"Processing Topic Event: {event}")
    await model_feed.delete_topic_feed(event.topic_id)


pubsub.add_push_route(
    blueprint=atlas_api,
    callback=model_feed.refresh_scores,
    subscription=subscription.refresh_trending_subscription,
    authenticate=False,
)
pubsub.add_push_route(
    blueprint=atlas_api,
    callback=delete_topic,
    subscription=subscription.delete_topic_subscription,
)


async def refresh_user_trending(event, session):
    logging.info(f"Refreshing User {session.current_user_id} trending page")
    await model_feed.refresh_user_trending(session.current_user_id)


# follow / unfollow
pubsub.add_push_route(
    blueprint=atlas_api,
    callback=refresh_user_trending,
    subscription=subscription.follow_clik_subscription,
)
pubsub.add_push_route(
    blueprint=atlas_api,
    callback=refresh_user_trending,
    subscription=subscription.unfollow_clik_subscription,
)
pubsub.add_push_route(
    blueprint=atlas_api,
    callback=refresh_user_trending,
    subscription=subscription.follow_topic_subscription,
)
pubsub.add_push_route(
    blueprint=atlas_api,
    callback=refresh_user_trending,
    subscription=subscription.unfollow_topic_subscription,
)
pubsub.add_push_route(
    blueprint=atlas_api,
    callback=refresh_user_trending,
    subscription=subscription.follow_user_subscription,
)
pubsub.add_push_route(
    blueprint=atlas_api,
    callback=refresh_user_trending,
    subscription=subscription.unfollow_user_subscription,
)
pubsub.add_push_route(
    blueprint=atlas_api,
    callback=refresh_user_trending,
    subscription=subscription.follow_external_feed_subscription,
)
pubsub.add_push_route(
    blueprint=atlas_api,
    callback=refresh_user_trending,
    subscription=subscription.unfollow_external_feed_subscription,
)
