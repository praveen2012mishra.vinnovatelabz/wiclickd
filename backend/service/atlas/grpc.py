import logging
import time
from datetime import datetime
from grpc import ServicerContext, StatusCode
from atlas.api.atlas_pb2_grpc import AtlasServicer
from atlas.api.atlas_pb2 import (
    GetPostsRequest,
    GetPostsResponse,
    GetDiscussionRequest,
    GetDiscussionResponse,
    GetFeedRequest,
    GetFeedResponse,
    CreatePostRequest,
    CreatePostResponse,
    GetPostFromUrlRequest,
    GetPostFromUrlResponse,
    CreateCommentRequest,
    CreateCommentResponse,
    EditPostRequest,
    EditPostResponse,
    RemoveContentRequest,
    RemoveContentResponse,
    ReportContentRequest,
    ReportContentResponse,
    GetCommentsRequest,
    GetCommentsResponse,
    EditCommentRequest,
    EditCommentResponse,
    LikeContentRequest,
    LikeContentResponse,
    SharePostRequest,
    SharePostResponse,
)
from atlas.api.comment_pb2 import Comment, DiscussionFilter
from atlas.api.post_pb2 import Post
from atlas.api.content_info_pb2 import ContentInfo
from artemis.api.external_feed_pb2 import ExternalFeedSource
from atlas.event.content_pb2 import (
    CreatePostEvent,
    CreateCommentEvent,
    EditPostEvent,
    EditCommentEvent,
    DeletePostEvent,
    DeleteCommentEvent,
    LikeContentEvent,
)
from weclikd.utils import time_utils, weclikd_id
from weclikd.service.transaction import Transaction as SqlTransaction
from atlas.model import (
    model_comment,
    model_post,
    model_content,
    model_share,
    model_feed,
)
from atlas.db.db_feed import FeedType
from atlas.event import topic


class Atlas(AtlasServicer):
    async def CreatePost(
        self, request: CreatePostRequest, context: ServicerContext
    ) -> CreatePostResponse:
        logging.info(f"Processing CreatePost:\n{request}")
        new_post = Post(
            content_info=ContentInfo(
                type=ContentInfo.ContentType.POST,
                user_id=context.current_user_id if not request.url else 0,
                text=request.text,
                created=time_utils.get_current_timestamp_pb2(),
            ),
            cliks=request.cliks,
            clik_ids=[weclikd_id.string_to_id(each) for each in request.cliks],
            topics=request.topics,
            topic_ids=request.topic_ids,
            title=request.title,
            summary=request.summary,
            url=request.url,
            sharer_id=context.current_user_id if request.url else 0,
            thumbnail_picture_id=request.thumbnail_picture_id,
            picture_ids=request.picture_ids,
            external_feed=ExternalFeedSource(
                id=request.feed_id, icon_url=request.icon_url
            ),
            other_urls=request.other_urls,
        )

        async with SqlTransaction() as db:
            old_post = await model_post.retrieve_using_url(request.url)
            if old_post:
                logging.info(f"Post was already shared")
                new_post = await model_share.merge_sharing(new_post, old_post, db)
                await topic.update_post_topic.publish(
                    event=EditPostEvent(post=new_post), session=context
                )
                context.set_code(StatusCode.ALREADY_EXISTS)
                context.set_details(f"Post with url {request.url} was already shared")
                return CreatePostResponse(post=new_post)
            await model_post.insert(new_post, request.topic_ids)

        await topic.create_post_topic.publish(
            event=CreatePostEvent(post=new_post), session=context
        )
        return CreatePostResponse(post=new_post)

    async def CreateComment(
        self, request: CreateCommentRequest, context: ServicerContext
    ) -> CreateCommentResponse:
        logging.info(f"Processing CreateComment:\n{request}")

        # Insert the comment to the DB
        comment = Comment(
            clik=request.clik,
            content_info=ContentInfo(
                type=ContentInfo.ContentType.COMMENT,
                user_id=context.current_user_id,
                text=request.text,
                created=time_utils.get_current_timestamp_pb2(),
            ),
            parent_content_id=request.parent_content_id,
        )

        # check parent content exists, get discussion thread ids at the same time
        parent = await model_content.retrieve(
            db=None, content_id=comment.parent_content_id
        )
        if not parent:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(
                f'Parent content "{comment.parent_content_id}" does not exist.'
            )
            return CreateCommentResponse()

        # check parent content is shared to the same clik.
        # if the parent content is a post, then the post should be shared to the clik
        clik_id = weclikd_id.string_to_id(comment.clik)
        if parent.content_info.type == ContentInfo.ContentType.COMMENT:
            if parent.clik.lower() != comment.clik.lower():
                context.set_code(StatusCode.INVALID_ARGUMENT)
                context.set_details(f'"Parent comment not posted to the same clik"')
                return CreateCommentResponse()
        elif clik_id and parent.content_info.type == ContentInfo.ContentType.POST:
            if comment.clik.lower() not in [clik.lower for clik in parent.cliks]:
                await model_share.share_to_clik(parent, comment.clik)

        # save discussion thread ids
        if parent.content_info.type == ContentInfo.ContentType.COMMENT:
            comment.thread_ids.extend(parent.thread_ids)
        comment.thread_ids.append(parent.content_info.id)

        await model_comment.insert(comment)

        # Broadcast event that comment was created
        if parent.content_info.type == ContentInfo.ContentType.COMMENT:
            await topic.create_comment_topic.publish(
                event=CreateCommentEvent(comment=comment, parent_comment=parent),
                session=context,
            )
        else:
            await topic.create_comment_topic.publish(
                event=CreateCommentEvent(comment=comment, post=parent), session=context
            )

        return CreateCommentResponse(comment=comment)

    async def GetPosts(
        self, request: GetPostsRequest, context: ServicerContext
    ) -> GetPostsResponse:
        logging.info(f"GetPosts: {request}")
        posts = await model_post.fetch_many(request.ids, context.current_user_id)
        response = GetPostsResponse(posts=posts)
        for post in response.posts:
            await model_comment.populate_discussion(
                post, request.discussion_filter, context.current_user_id
            )

        return response

    async def GetComments(
        self, request: GetCommentsRequest, context: ServicerContext
    ) -> GetCommentsResponse:
        logging.info(f"GetComments: {request}")
        comments = await model_comment.fetch_many(request.ids, context.current_user_id)
        response = GetCommentsResponse(comments=comments)
        for comment in response.comments:
            await model_comment.populate_discussion(
                comment, request.discussion_filter, context.current_user_id
            )

        return response

    async def GetPostFromUrl(
        self, request: GetPostFromUrlRequest, context: ServicerContext
    ) -> GetPostFromUrlResponse:
        logging.info(f"GetPostFromUrl: {request}")
        return GetPostFromUrlResponse(
            post=await model_post.retrieve_using_url(
                request.url, context.current_user_id
            )
        )

    async def GetDiscussion(
        self, request: GetDiscussionRequest, context: ServicerContext
    ) -> GetDiscussionResponse:
        logging.info(f"GetDiscussion: {request}")
        post = Post()
        post.content_info.id = request.content_id
        await model_comment.populate_discussion(
            post, request.discussion_filter, context.current_user_id
        )
        return GetDiscussionResponse(discussion=post.discussion)

    async def GetFeed(
        self, request: GetFeedRequest, context: ServicerContext
    ) -> GetFeedResponse:
        logging.info(f"GetFeed: {request}")
        if request.user_id:
            feed_id = request.user_id
            feed_type = FeedType.USER
        elif request.clik_id:
            feed_id = request.clik_id
            feed_type = FeedType.CLIK
        elif request.topic_id:
            feed_id = request.topic_id
            feed_type = FeedType.TOPIC
        elif request.feed_id:
            feed_id = request.feed_id
            feed_type = FeedType.FEED
        elif request.home:
            feed_id = request.user_id
            feed_type = FeedType.HOME
        else:
            raise Exception(f"Internal Error: {request}")

        feed = await model_feed.get_feed(
            feed_id=feed_id,
            feed_type=feed_type,
            filter=request.feed_filter,
            user_id=context.current_user_id,
        )
        for post in feed.posts:
            await model_comment.populate_discussion(
                post, request.discussion_filter, context.current_user_id
            )

        return GetFeedResponse(feed=feed)

    async def RemoveContent(
        self, request: RemoveContentRequest, context: ServicerContext
    ) -> RemoveContentResponse:
        logging.info(f"RemoveContent: {request} {context}")
        content = await model_content.retrieve(db=None, content_id=request.content_id)
        if not content:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f"Content does not exist {request.content_id}")
            return RemoveContentResponse()
        if content.content_info.state == ContentInfo.ContentState.REMOVED:
            context.set_code(StatusCode.INVALID_ARGUMENT)
            context.set_details(f"Content already removed {request.content_id}")
            return RemoveContentResponse()
        if (
            not context.is_admin
            and content.content_info.user_id != context.current_user_id
        ):
            context.set_code(StatusCode.PERMISSION_DENIED)
            context.set_details(f"No permissions to remove {request.content_id}")
            return RemoveContentResponse()
        if not context.is_admin and (
            time.time() - content.content_info.created.seconds > 10 * 60
        ):
            context.set_code(StatusCode.DEADLINE_EXCEEDED)
            context.set_details(
                f"Can only remove content within 10 minutes of content creation."
            )
            return RemoveContentResponse()

        if content.content_info.type == ContentInfo.ContentType.POST:
            await model_post.remove(content)
            await topic.delete_post_topic.publish(
                DeletePostEvent(content_id=request.content_id), session=context
            )
        elif content.content_info.type == ContentInfo.ContentType.COMMENT:
            await model_comment.remove(content)
            await topic.delete_comment_topic.publish(
                DeleteCommentEvent(content_id=request.content_id), session=context
            )

        return RemoveContentResponse()

    async def EditPost(
        self, request: EditPostRequest, context: ServicerContext
    ) -> EditPostResponse:
        logging.info(f"EditPost: {request}")
        post = Post(
            content_info=ContentInfo(
                id=request.content_id,
                text=request.text,
            ),
            cliks=request.cliks,
            topics=request.topics,
            topic_ids=request.topic_ids,
            title=request.title,
            summary=request.summary,
            url=request.url,
            thumbnail_picture_id=request.thumbnail_picture_id,
            picture_ids=request.picture_ids,
        )
        post = await model_post.update(post, request.topic_ids)
        if not post:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f"Post {request.content_id} was not found")
            return EditPostResponse()

        await topic.update_post_topic.publish(
            event=EditPostEvent(post=post), session=context
        )
        return EditPostResponse()

    async def SharePost(
        self, request: SharePostRequest, context: ServicerContext
    ) -> SharePostResponse:
        logging.info(f"Share Post: {request}")
        original_post = await model_content.retrieve(None, request.content_id)
        if not original_post or original_post.content_info.type != ContentInfo.POST:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f"{request.content_id} is not a comment.")
            return SharePostResponse()

        modified_post = Post()
        modified_post.CopyFrom(original_post)
        modified_post.cliks[:] = list(set(original_post.cliks) | set(request.cliks))
        modified_post.clik_ids[:] = list(
            set(original_post.clik_ids)
            | set([weclikd_id.string_to_id(each) for each in request.cliks])
        )
        modified_post.topics[:] = list(set(original_post.topics) | set(request.topics))
        modified_post.topic_ids[:] = list(
            set(original_post.topic_ids) | set(request.topic_ids)
        )
        post = await model_post.update(modified_post, modified_post.topic_ids)
        return SharePostResponse(post=post)

    async def EditComment(
        self, request: EditCommentRequest, context: ServicerContext
    ) -> EditCommentResponse:
        logging.info(f"EditComment: {request}")
        comment = await model_content.retrieve(None, request.content_id)

        # check valid comment
        if not comment or comment.content_info.type != ContentInfo.ContentType.COMMENT:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f"{request.content_id} is not a comment.")
            return EditCommentResponse()

        # check user created comment
        if comment.content_info.user_id != context.current_user_id:
            context.set_code(StatusCode.PERMISSION_DENIED)
            context.set_details(f"You do not have permissions to edit this comment.")
            return EditCommentResponse()

        # check user created comment
        comment_age_seconds = time.time() - comment.content_info.created.seconds
        if comment_age_seconds > 10 * 60:  # 10 minutes
            context.set_code(StatusCode.DEADLINE_EXCEEDED)
            context.set_details(f"Comment cannot be edited after 10 minutes.")
            return EditCommentResponse()

        # if text is empty delete it
        if not request.text:
            await model_comment.remove(comment)
            return EditCommentResponse()

        comment.content_info.text = request.text
        await model_comment.update(comment)

        await topic.update_comment_topic.publish(
            event=EditCommentEvent(comment=comment), session=context
        )
        return EditCommentResponse()

    async def ReportContent(
        self, request: ReportContentRequest, context: ServicerContext
    ) -> ReportContentResponse:
        logging.info(f"ReportContent: {request}")
        await model_content.report(
            request.content_id, request.reports, context.current_user_id
        )
        if context.is_admin:
            await self.RemoveContent(
                request=RemoveContentRequest(content_id=request.content_id),
                context=context,
            )
        return ReportContentResponse()

    async def LikeContent(
        self, request: LikeContentRequest, context: ServicerContext
    ) -> LikeContentResponse:
        logging.info(f"LikeContent: {request}")
        now = datetime.now()
        async with SqlTransaction() as db:
            content = await model_content.retrieve(db, content_id=request.content_id)
            if not content:
                context.set_code(StatusCode.NOT_FOUND)
                context.set_details(f"Content {request.content_id} not found")
                return LikeContentResponse()

            if content.content_info.user_id == context.current_user_id:
                return LikeContentResponse()

            prev_like_type, prev_created = await model_content.like(
                request.content_id, request.type, context.current_user_id, now
            )

        await topic.like_content_topic.publish(
            event=LikeContentEvent(
                post=content if content.content_info.type == ContentInfo.POST else None,
                comment=content
                if content.content_info.type == ContentInfo.COMMENT
                else None,
                type=request.type,
                created=time_utils.datetime_to_timestamp_pb2(now),
                prev_like_type=prev_like_type,
                prev_created=time_utils.datetime_to_timestamp_pb2(now),
            ),
            session=context,
        )
        return LikeContentResponse()
