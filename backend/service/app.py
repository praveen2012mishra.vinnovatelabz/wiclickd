import logging
import asyncio
import httpx
import traceback
from sanic import Sanic, exceptions as sanic_exceptions
from sanic.handlers import ErrorHandler
from sanic.response import text
from sanic_cors import CORS
from janus.rest_handler import v1_api
from janus.graphql.route import graphql_api, init_graphql
from janus.rest import janus_api
from artemis.rest import artemis_api
from athena.rest import athena_api
from atlas.rest import atlas_api
from eros.rest import eros_api
from hestia.rest import hestia_api
from plutus.rest import plutus_api
from weclikd.utils import environment

from weclikd.service import logger, sql, redis, es


def create_app():
    logger.setup_logger()
    logging.info("==============")
    logging.info("Starting Janus")
    logging.info("==============")

    app = Sanic(__name__)
    CORS(app, automatic_options=True, max_age=86400)
    app.blueprint(v1_api)
    app.blueprint(artemis_api)
    app.blueprint(athena_api)
    app.blueprint(janus_api)
    app.blueprint(eros_api)
    app.blueprint(hestia_api)
    app.blueprint(graphql_api)
    app.blueprint(atlas_api)
    app.blueprint(plutus_api)
    app.error_handler = CustomErrorHandler()
    app.config.RESPONSE_TIMEOUT = 600

    @app.route("/")
    async def root_route(request):
        raise httpx.exceptions.HTTPError()
        return text("Not Found", 404)

    @app.route("/favicon.ico")
    async def favicon_route(request):
        return text("Not Found", 404)

    @app.route("/_ah/warmup")
    async def warm_up(_request):
        logging.info("Warm up function called")
        # Google App Engine will try its best to call this first. No guarantees though.
        await sql.start()
        return text("Success", status=200)

    @app.route("/v1/healthcheck")
    async def healthcheck(_request):
        logging.info("Healthcheck API called")
        services_to_check = {
            "elasticsearch": es.health_check(),
            "redis": redis.health_check(),
            "sql": sql.health_check(),
        }
        results = await asyncio.gather(
            *services_to_check.values(), return_exceptions=True
        )
        for service_name, result in zip(services_to_check, results):
            if result:
                logging.error(f"Service {service_name} failed: {repr(result)}")
                return text(f"Service {service_name} failed: {repr(result)}", 500)

        return text("Success", status=200)

    @app.listener("before_server_start")
    async def init_server(_app, loop, *_args, **_kwargs):
        logging.info("Starting database")
        services_to_start = {
            "elasticsearch": es.start(),
            "redis": redis.start(),
            "sql": sql.start(),
        }
        results = await asyncio.gather(
            *services_to_start.values(), return_exceptions=True
        )
        for service_name, result in zip(services_to_start, results):
            if result:
                logging.error(f"Error starting {service_name}: {repr(result)}")

        init_graphql(loop)

        # hack to try to get around issue with middleware not being registered before /_ah/warmup call
        for after_server_start_callback in _app.listeners["after_server_start"]:
            after_server_start_callback(_app, loop)
        logging.info("Initialized Server")

    @app.listener("after_server_stop")
    async def finalize_server(_app, *_args, **_kwargs):
        await asyncio.gather(
            es.stop(),
            redis.stop(),
            sql.stop(),
            return_exceptions=True,
        )

    return app


class CustomErrorHandler(ErrorHandler):
    def default(self, request, exception):
        if isinstance(
            exception,
            (
                sanic_exceptions.MethodNotSupported,
                httpx.exceptions.NetworkError,
                httpx.exceptions.HTTPError,
                httpx.exceptions.TimeoutException,
                httpx.exceptions.ConnectTimeout,
                asyncio.futures.TimeoutError,
            ),
        ):
            logging.error(
                "Path: {} Input: {} Exception: {}".format(
                    request.path, request.body, exception
                )
            )
        else:
            logging.exception(
                "Path: {} Input: {} Exception: {}".format(
                    request.path, request.body, exception
                )
            )
        if environment.is_test_env() or environment.is_dev_env():
            return text(
                "".join(traceback.format_tb(exception.__traceback__)), status=500
            )
        else:
            return text("Server Error", status=500)
