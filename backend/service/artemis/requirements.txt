beautifulsoup4==4.8.2
extraction==0.3
feedparser==5.2.1
httpx==0.9.3
google-api-core[grpc]==1.16.0
google-auth==1.11.2
google-cloud-core==1.3.0
google-cloud-pubsub==1.2.0
google-cloud-secret-manager==0.2.0
google-cloud-storage==1.26.0
gunicorn==20.0.4
sanic
