from typing import List, Tuple, Dict
from artemis.api.artemis_pb2 import FollowedFeed
from artemis.db.external_feed_db import (
    feed_follow_db,
    feed_db,
)
from weclikd.common.follow_pb2 import FollowSettings
from weclikd.common.filter_pb2 import DefaultFilter
from weclikd.service.transaction import Transaction
from artemis.api.external_feed_pb2 import ExternalFeedSource
from atlas.event.content_pb2 import CreateExternalPostEvent
from atlas.db import db_post
from artemis.event.topic import (
    create_external_post_topic,
    process_external_feed_item_topic,
)
from artemis.event.external_feed_event_pb2 import ProcessExternalFeedItemEvent
from weclikd.utils import weclikd_id, environment, url_normalize
from artemis.download_image import add_picture_from_url
from artemis.model import rss, text_normalizer
from artemis.extract_url import unfurl
import logging


async def create(feed: ExternalFeedSource) -> str:
    parsed_feed, error = await rss.RSSFeed.parse(feed.url)
    if error:
        return False

    if feed.icon_url:
        image_id, status = await add_picture_from_url(feed.icon_url)
        if status:
            logging.error(f"Unable to retrieve feed icon: {status.details}")
            return status.details

        feed.icon_url = str(image_id)

    feed.id = weclikd_id.string_to_id(url_normalize.url_normalize(feed.url))
    await feed_db.insert(feed)

    if not environment.is_test_env():
        await poll_feed(feed)


async def get_feeds(
    feed_ids: List[int], feed_names: List[str]
) -> List[ExternalFeedSource]:
    feeds = []
    if feed_ids:
        feeds = await feed_db.get_many(values=feed_ids)

    if feed_names:
        feeds.extend(await feed_db.get_many(field_name="name", values=feed_names))

    return feeds


async def update(feed: ExternalFeedSource, upload_icon: bool) -> str:
    if upload_icon:
        image_id, status = await add_picture_from_url(feed.icon_url)
        if status:
            logging.error(f"Unable to retrieve feed icon {status.details}")
            return status.details

        feed.icon_url = str(image_id)

    await feed_db.update(msg=feed, id=feed.id)


async def delete(feed_id) -> ExternalFeedSource:
    external_feed = await feed_db.get(id=feed_id)
    if not external_feed:
        return False
    async with Transaction(sql=True):
        await feed_db.delete(id=feed_id)
        await feed_follow_db.delete(feed_id=feed_id)
    return external_feed


async def retrieve(feed_id: int) -> ExternalFeedSource:
    return await feed_db.get(id=feed_id)


async def retrieve_from_url(url: str) -> ExternalFeedSource:
    # url = url_normalize.url_normalize(url)
    return await feed_db.get(id=weclikd_id.string_to_id(url))


async def follow(user_id: int, feed_id: int, settings: FollowSettings) -> bool:
    async with Transaction(sql=True):
        inserted = await feed_follow_db.upsert(
            user_id=user_id,
            feed_id=feed_id,
            data={"follow_type": settings.follow_type},
        )
        if inserted:
            await feed_db.increment("followers", id=feed_id)
            return True
        return False


async def unfollow(user_id: int, feed_id: int) -> bool:
    async with Transaction(sql=True):
        deleted = await feed_follow_db.delete(user_id=user_id, feed_id=feed_id)
        if deleted:
            await feed_db.decrement("followers", id=feed_id)
            return True
        return False


async def retrieve_following(user_id) -> List[FollowedFeed]:
    async with Transaction(sql=True):
        followed_feeds = await feed_follow_db.query(user_id=user_id)
        feeds = await feed_db.get_many(values=[each.feed.id for each in followed_feeds])
        for followed_feed, feed in zip(followed_feeds, feeds):
            followed_feed.feed.CopyFrom(feed)
    return followed_feeds


async def test(url: str) -> Tuple[ExternalFeedSource, str]:
    rss_feed, error = await rss.RSSFeed.parse(url)
    if error:
        return None, error

    return await rss_feed.to_external_feed_source(), error


async def retrieve_all(filter: DefaultFilter) -> Tuple[List[ExternalFeedSource], str]:
    if filter.sort == DefaultFilter.TRENDING:
        return await feed_db.get_trending(filter)

    return await feed_db.paginate(filter)


async def poll_feed(external_feed: ExternalFeedSource, rss_feed: rss.RSSFeed = None):
    logging.info(f"Polling Feed {external_feed.name}")
    try:
        if not rss_feed:
            rss_feed, error = await rss.RSSFeed.parse(external_feed.url)
            if error:
                logging.error(f"Unable to poll rss feed {external_feed.url}: {error}")
                return

            if external_feed.last_update == rss_feed.last_update:
                logging.info(f"Feed {external_feed.name} has no new content")
                return

        rss_entries_map = {}
        urls = []
        for entry in rss_feed.entries:
            rss_entries_map[entry.link] = entry
            urls.append(entry.link) #  url_normalize.url_normalize(entry.link))

        # Get urls shared already
        urls_already_shared = await db_post.fetch_urls_shared_to_external_feed(
            external_feed.id, urls
        )
        for url in set(urls) - set(urls_already_shared):
            logging.info(f"Queuing {url}")
            await process_external_feed_item_topic.publish(
                event=ProcessExternalFeedItemEvent(
                    source=external_feed,
                    url=url,
                    title=rss_entries_map[url].title,
                    summary=rss_entries_map[url].get("description", ""),
                ),
                mock=False,
            )
        external_feed.last_update = rss_feed.last_update
        await feed_db.update(msg=external_feed, id=external_feed.id)
    except Exception as ex:
        logging.exception(f"Failed to process feed {external_feed.url}: error={ex}")


async def poll_all_feeds(request, session):
    async for external_feed in feed_db.scan():
        await poll_feed(external_feed)

    logging.info(f"Finished Polling")


async def process_external_feed_item(request: ProcessExternalFeedItemEvent, session):
    logging.info(f"Processing {request.url}")
    unfurl_results, code = await unfurl(request.url, get_image=True)
    if code:
        logging.warning(f"Not indexing {request.url}. Reason = {code}")
        return
    base_url = url_normalize.get_base_url(unfurl_results.normalized_url)
    if not base_url.startswith("https"):
        logging.warning(f"Not indexing {unfurl_results.normalized_url}. Not Https")
        return

    create_post_event = CreateExternalPostEvent()
    post = create_post_event.posts.add()
    if request.source.base_topic:
        post.topics.extend([request.source.base_topic])
    if (
        request.source.summary_source
        == ExternalFeedSource.SummarySource.RSS_DESCRIPTION
    ):
        post.title = text_normalizer.normalize_title(request.title, base_url)
        post.summary = text_normalizer.normalize_summary(request.summary, base_url)
    else:
        post.title = (
            text_normalizer.normalize_title(unfurl_results.title, base_url)
            if unfurl_results.title
            else request.title
        )
        post.summary = (
            text_normalizer.normalize_summary(unfurl_results.summary, base_url)
            if unfurl_results.summary
            else request.summary
        )
    post.url = unfurl_results.normalized_url
    post.feed_id = request.source.id
    post.icon_url = request.source.icon_url
    post.thumbnail_picture_id = unfurl_results.thumbnail_picture_id
    post.unnormalized_url = request.url if post.url != request.url else ''

    await create_external_post_topic.publish(create_post_event, session)


async def refresh_trending():
    feeds, _ = await feed_db.paginate(
        filter=DefaultFilter(num_items=200), order_by="followers"
    )
    await feed_db.cache_trending(feeds)
