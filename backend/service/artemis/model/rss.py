import feedparser
import httpx
import logging
from bs4 import BeautifulSoup
from typing import Tuple, Optional
from artemis.api.external_feed_pb2 import ExternalFeedSource
from weclikd.utils import async_utils, url_normalize


class RSSFeed:
    def __init__(self, url: str, parser_dict: feedparser.FeedParserDict):
        self._rss = parser_dict
        self.url = url
        self._favicon_url = None

    @staticmethod
    async def parse(url) -> Tuple["RSSFeed", str]:
        parser_dict, error = await _parse(url)
        if error:
            return None, error
        if not parser_dict:
            return None, "Feed is Empty"
        return RSSFeed(url=url, parser_dict=parser_dict), error

    @property
    def link(self) -> Optional[str]:
        if "link" in self._rss.feed:
            return self._rss.feed.link
        elif "links" in self._rss.feed:
            return self._rss.feed.links[0]["href"]
        else:
            return None

    @property
    def last_update(self):
        try:
            if "updated" not in self._rss.feed:
                if "published" in self._rss.entries[0]:
                    return self._rss.entries[0].published
                else:
                    return self._rss.entries[0].updated
            return self._rss.feed.updated
        except:
            logging.exception(f"Failed to get last_update for {self.url}")
            logging.info(self._rss.feed)
            logging.info(self._rss.entries[0])
            raise

    @property
    def entries(self):
        return [RSSEntry(each) for each in self._rss.entries]

    @property
    async def icon_url(self) -> Optional[str]:
        if self._favicon_url:
            return self._favicon_url

        if not self.link:
            return None

        base = url_normalize.get_base_url(self.link)
        favicon_url = base + "/favicon.ico"
        try:
            async with httpx.AsyncClient(timeout=5.0) as client:
                response = await client.get(self.link)
            head_section = BeautifulSoup(response.text, "html.parser").head.find_all(
                "link"
            )
            for each in head_section:
                for val in each.attrs.get("rel", []):
                    if val.lower() == "icon":
                        favicon_url = each.attrs.get("href")
                        if not favicon_url.startswith("http"):
                            favicon_url = base + favicon_url

            async with httpx.AsyncClient(timeout=5.0) as client:
                response = await client.get(favicon_url)
                if response.status_code < 400:
                    self._favicon_url = favicon_url
                    return favicon_url
                return None
        except Exception as ex:
            logging.warning(f"Could not find favicon URL: {ex}")
            return None

    async def to_external_feed_source(self) -> Optional[ExternalFeedSource]:
        link = self.link
        if not link:
            return None

        return ExternalFeedSource(
            url=self.url,
            website=url_normalize.url_normalize(link),
            icon_url=await self.icon_url,
            name=self._rss.feed.title,
            type=ExternalFeedSource.Type.RSS,
        )


class RSSEntry(feedparser.FeedParserDict):

    @property
    def link(self):
        if 'link' not in self:
            if 'links' not in self:
                raise Exception(f'Link property not found. {self}')
            else:
                return self.links[0]['href']
        else:
            return self.get("link")


@async_utils.wrap
def _parse(url):
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36"
    }
    parsed_feed = feedparser.parse(url, request_headers=headers)
    if not parsed_feed.entries:
        return None, parsed_feed.get("bozo_exception")
    return parsed_feed, None
