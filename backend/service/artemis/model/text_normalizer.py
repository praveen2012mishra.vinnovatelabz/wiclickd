from bs4 import BeautifulSoup
import html


def normalize_title(text, base_url):
    normalized = _normalize_text(text)
    if len(normalized) > 200:
        return normalized[:197] + "..."
    return normalized


def normalize_summary(text, base_url):
    normalized = _normalize_text(text)
    if len(normalized) > 500:
        return normalized[:477] + "..."
    return normalized


def _normalize_text(text):
    soup = BeautifulSoup(text, "html.parser")
    whitelist = ["p", "[document]", "em", "a"]
    text_elements = [t for t in soup.find_all(text=True) if t.parent.name in whitelist]
    return "".join(
        [html.unescape(str(each)) for each in text_elements if str(each).strip()]
    )
