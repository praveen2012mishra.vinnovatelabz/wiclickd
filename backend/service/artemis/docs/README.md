# Athena Design Doc

## Overview and Context

Artemis, the goddess of the hunt, is the microservice responsible for hunting for info on the internet.
It is primarily responsible for retrieving sites to index and to also unfurl user-submitted sites.

## Goals

1) Add and remove feeds that will be monitored.
2) Extract articles that should be indexed on our site.
3) Unfurl websites.
4) Download images from external sources.

## Proposed Solution

## Interface

###gRPC (Synchronous)
* AddExternalFeed
* RemoveExternalFeed
* GetExternalFeeds
* MonitorExternalFeeds
* Unfurl
* DownloadImage 

###Published Events (Asynchronous)
* AddExternalPost

###Subscribed Events
None

## Storage

Currently uses MySQL but Datastore is also a good choice. This database will be extremely read heavy
so caching is important.


### Tables

topic - stores all topics and its information, including its ancestors (root topic to this topic)

topic_relationship - relationship between child topics and parent topics

new_topic - new topics that are requested by users

banned_topic - topics that have been banned by Weclikd admins


### Caching

#### topics
Topics will be stored using the hash data type. This is so we can quickly update the follower count.
This will not take up a lot of space compared to other tables like posts.
It also does not have very many fields so retrieving all keys isn't a problem. Expiration: none

T<topic-id> hash: name, description, parents, num_followers, children


## Testability

## Monitoring and Alerting

