from weclikd.service import pubsub
from artemis.event import external_feed_event_pb2
from atlas.event import content_pb2

poll_external_feed_topic = pubsub.Topic("external-feed-poll")
process_external_feed_item_topic = pubsub.Topic(
    "external-feed-item-process", external_feed_event_pb2.ProcessExternalFeedItemEvent
)

create_external_feed_topic = pubsub.Topic(
    "external-feed-create", external_feed_event_pb2.CreateExternalFeedEvent
)
update_external_feed_topic = pubsub.Topic(
    "external-feed-update", external_feed_event_pb2.CreateExternalFeedEvent
)
delete_external_feed_topic = pubsub.Topic(
    "external-feed-delete", external_feed_event_pb2.DeleteExternalFeedEvent
)
follow_external_feed_topic = pubsub.Topic(
    "external-feed-follow", external_feed_event_pb2.FollowExternalFeedEvent
)
unfollow_external_feed_topic = pubsub.Topic(
    "external-feed-unfollow", external_feed_event_pb2.UnfollowExternalFeedEvent
)

create_external_post_topic = pubsub.Topic(
    "external-post-create", content_pb2.CreateExternalPostEvent
)
