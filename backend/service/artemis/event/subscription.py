from weclikd.service import pubsub
from artemis.event import topic

poll_external_feed_subscription = pubsub.Subscription(
    topic.poll_external_feed_topic, "artemis"
)
process_external_feed_item_subscription = pubsub.Subscription(
    topic.process_external_feed_item_topic, "artemis"
)
