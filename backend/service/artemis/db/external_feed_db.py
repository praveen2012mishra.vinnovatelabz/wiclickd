from sqlalchemy import (
    Table,
    Column,
    Integer,
    JSON,
    TIMESTAMP,
    VARCHAR,
    text as sqltext,
)
from typing import Optional, List, Tuple
from google.protobuf.message import Message
from weclikd.service.sql import UnsignedBigInt, SQLDb, metadata
from weclikd.common.filter_pb2 import DefaultFilter
from artemis.api.artemis_pb2 import FollowedFeed
from artemis.api.external_feed_pb2 import ExternalFeedSource
import sqlalchemy
import base64


FollowFeedTable = Table(
    "feed_follow",
    metadata,
    Column("user_id", UnsignedBigInt, primary_key=True),
    Column("feed_id", UnsignedBigInt, primary_key=True),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

ExternalFeedTable = Table(
    "external_feed",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True),
    Column("name", VARCHAR(64)),
    Column("followers", Integer),
    Column("data", JSON),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)


class FollowFeedSQLDb(SQLDb):
    def __init__(self):
        super().__init__(FollowFeedTable, proto_clazz=FollowedFeed)

    def to_proto(self, row, msg: Optional[Message] = None) -> FollowedFeed:
        proto: FollowedFeed = super().to_proto(row, msg)
        proto.feed.id = row.feed_id
        proto.settings.follow_type = row.data["follow_type"]
        return proto


class FeedDB(SQLDb):
    def __init__(self):
        super().__init__(
            ExternalFeedTable,
            proto_clazz=ExternalFeedSource,
            order_by=ExternalFeedTable.c.id,
        )

    async def get_trending(
        self, filter: DefaultFilter
    ) -> Tuple[List[ExternalFeedSource], str]:
        decoded_cursor = (
            base64.b64decode(filter.cursor).decode() if filter.cursor else None
        )
        num_followers, last_id = (
            decoded_cursor.split(",") if decoded_cursor else ("99999999999", 0)
        )

        rows = await self.sql_instance.db.fetch_all(
            sqlalchemy.select(
                [ExternalFeedTable],
                whereclause=sqlalchemy.or_(
                    ExternalFeedTable.c.followers < num_followers,
                    sqlalchemy.and_(
                        ExternalFeedTable.c.followers == num_followers,
                        ExternalFeedTable.c.id > last_id,
                    ),
                ),
                limit=filter.num_items,
            )
            .order_by(ExternalFeedTable.c.followers.desc())
            .order_by(ExternalFeedTable.c.id.asc())
        )
        cursor = (
            base64.b64encode(f"{rows[-1].followers},{rows[-1].id}".encode()).decode()
            if rows
            else ""
        )
        return [self.to_proto(row) for row in rows], cursor


feed_follow_db = FollowFeedSQLDb()
feed_db = FeedDB()
