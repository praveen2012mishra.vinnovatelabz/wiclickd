import logging
import extraction
import httpx
from typing import Tuple
from grpc import StatusCode
from weclikd.utils import async_utils, url_normalize, grpc_utils
from artemis.api.artemis_pb2 import UnfurlResponse
from weclikd.exceptions import WeclikdException
from artemis.download_image import add_picture_from_url


class ExtractError(WeclikdException):
    pass


@async_utils.wrap
def extract(text, url):
    try:
        return extraction.Extractor().extract(text, source_url=url)
    except:
        logging.exception(f"Failed to extract info from {url}")
        raise ExtractError()


MAX_CONTENT_SIZE = 2 * 1024 * 1024


async def http_get(url, timeout=20.0):
    headers = {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "accept-encoding": "gzip, deflate",
        "accept-language": "en-US,en",
        "cache-control": "max-age=0",
        "upgrade-insecure-requests": "1",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36",
        "x-requested-with": "XMLHttpRequest",
    }
    client = httpx.AsyncClient()
    async with client.stream("GET", url, headers=headers, timeout=timeout) as response:
        if "text" not in response.headers.get("Content-Type", ""):
            logging.error(
                f"Unsupported Content Type: {response.headers.get('Content-Type')}"
            )
            raise httpx.InvalidURL(
                f"Unsupported Content Type: {response.headers.get('Content-Type')}"
            )

        if int(response.headers.get("Content-Length", 0)) >= MAX_CONTENT_SIZE:
            logging.error(
                f"Content size too large for {url}: {response.headers.get('Content-Length')}"
            )
            raise httpx.InvalidURL("Payload too large")

        text = ""
        async for chunk in response.aiter_text():
            text += chunk
            if len(text) >= MAX_CONTENT_SIZE:
                logging.error(f"Content size too large")
                raise httpx.InvalidURL("Payload too large")

        return text, str(response.url)


BLACK_LISTED_SITES = ["bloomberg.com"]


async def unfurl(
    url, get_image: bool = False
) -> Tuple[UnfurlResponse, grpc_utils.GrpcStatus]:
    try:
        for each in BLACK_LISTED_SITES:
            if each in url:
                return (
                    UnfurlResponse(
                        title="",
                        summary="",
                        thumbnail_url="",
                        thumbnail_picture_id=0,
                        normalized_url=url_normalize.url_normalize(url),
                    ),
                    None,
                )

        if url.endswith('.mp3'):
            return (
                UnfurlResponse(
                    title="",
                    summary="",
                    thumbnail_url="",
                    thumbnail_picture_id=0,
                    normalized_url=url_normalize.url_normalize(url),
                ),
                None,
            )

        text, redirect_url = await http_get(url)
        extracted = await extract(text, url)
        if get_image and extracted.image and extracted.image.startswith("http"):
            image_id, status = await add_picture_from_url(extracted.image)
            thumbnail_picture_id = image_id
        else:
            thumbnail_picture_id = 0
        return (
            UnfurlResponse(
                title=extracted.title,
                summary=extracted.description,
                thumbnail_url=extracted.image,
                thumbnail_picture_id=thumbnail_picture_id,
                normalized_url=url_normalize.url_normalize(redirect_url),
            ),
            None,
        )
    except httpx.InvalidURL as ex:
        logging.error(f"Failed to unfurl {url}: {ex}")
        return (
            None,
            grpc_utils.GrpcStatus(
                code=StatusCode.INVALID_ARGUMENT, details=f"{url} is invalid"
            ),
        )
    except httpx.TimeoutException as ex:
        logging.error(f"Failed to unfurl {url}: {ex}")
        return (
            None,
            grpc_utils.GrpcStatus(
                code=StatusCode.DEADLINE_EXCEEDED, details=f"{url} timed out"
            ),
        )
    except httpx.HTTPError as ex:
        logging.error(f"Failed to unfurl {url}: {ex}")
        return (
            None,
            grpc_utils.GrpcStatus(
                code=StatusCode.UNAVAILABLE, details=f"{url} failed. {ex}"
            ),
        )
    except ExtractError as ex:
        logging.error(f"Failed to unfurl {url}: {ex}")
        return (
            None,
            grpc_utils.GrpcStatus(
                code=StatusCode.INTERNAL, details=f"{url} failed to unfurl."
            ),
        )
    except httpx.DecodingError as ex:
        logging.error(f"Failed to unfurl {url} due to decoding error")
        return (
            None,
            grpc_utils.GrpcStatus(
                code=StatusCode.INTERNAL, details=f"{url} failed to unfurl."
            ),
        )
    except Exception as ex:
        logging.exception(f"Failed to unfurl {url} due to error")
        return (
            None,
            grpc_utils.GrpcStatus(
                code=StatusCode.INTERNAL, details=f"{url} failed to unfurl."
            ),
        )
