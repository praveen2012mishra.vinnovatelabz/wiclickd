from sanic import Blueprint
from weclikd.service import pubsub
from artemis.model import external_feed_model
from artemis.event import subscription

artemis_api = Blueprint("artemis", url_prefix="/artemis")

pubsub.add_push_route(
    blueprint=artemis_api,
    callback=external_feed_model.poll_all_feeds,
    subscription=subscription.poll_external_feed_subscription,
)

pubsub.add_push_route(
    blueprint=artemis_api,
    callback=external_feed_model.process_external_feed_item,
    subscription=subscription.process_external_feed_item_subscription,
)
