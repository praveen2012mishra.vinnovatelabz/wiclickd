import logging
from grpc import ServicerContext, StatusCode
from artemis.api.artemis_pb2_grpc import ArtemisServicer
from artemis.api.artemis_pb2 import (
    AddExternalFeedRequest,
    AddExternalFeedResponse,
    EditExternalFeedRequest,
    EditExternalFeedResponse,
    TestExternalFeedRequest,
    TestExternalFeedResponse,
    RemoveExternalFeedRequest,
    RemoveExternalFeedResponse,
    GetFeedsRequest,
    GetFeedsResponse,
    UnfurlRequest,
    UnfurlResponse,
    DownloadImageRequest,
    DownloadImageResponse,
    FollowFeedRequest,
    FollowFeedResponse,
    UnfollowFeedRequest,
    UnfollowFeedResponse,
    GetFeedsFollowedRequest,
    GetFeedsFollowedResponse,
    GetAllExternalFeedsRequest,
    GetAllExternalFeedsResponse,
)
from artemis.event.topic import (
    create_external_feed_topic,
    update_external_feed_topic,
    delete_external_feed_topic,
)
from artemis.api.external_feed_pb2 import ExternalFeedSource
from weclikd.utils import time_utils, url_normalize, weclikd_id
from artemis import extract_url, download_image
from artemis.event import topic
from artemis.event.external_feed_event_pb2 import (
    FollowExternalFeedEvent,
    UnfollowExternalFeedEvent,
    CreateExternalFeedEvent,
    EditExternalFeedEvent,
    DeleteExternalFeedEvent,
)
from artemis.model import external_feed_model


class Artemis(ArtemisServicer):
    async def AddExternalFeed(
        self, request: AddExternalFeedRequest, context: ServicerContext
    ) -> AddExternalFeedResponse:
        logging.info(f"AddExternalFeed: {request}")
        website = url_normalize.url_normalize(request.website)
        if not website.startswith("http"):
            context.set_code(StatusCode.INVALID_ARGUMENT)
            context.set_details(f"Feed website must start with http.")
            context.set_custom_status("INVALID_FEED")
            return AddExternalFeedResponse()

        feed = await external_feed_model.get_feeds(
            feed_ids=[], feed_names=[request.name]
        )
        if feed:
            context.set_code(StatusCode.ALREADY_EXISTS)
            context.set_details(f"{request.name} already exists")
            return AddExternalFeedResponse()
        feed = await external_feed_model.get_feeds(
            feed_ids=[
                weclikd_id.string_to_id(url_normalize.url_normalize(request.url))
            ],
            feed_names=[],
        )
        if feed:
            context.set_code(StatusCode.ALREADY_EXISTS)
            context.set_details(f"{request.url} already exists")
            return AddExternalFeedResponse()

        feed = ExternalFeedSource(
            name=request.name,
            type=request.type or ExternalFeedSource.Type.RSS,
            url=request.url,
            website=request.website,
            icon_url=request.icon_url,
            base_topic=request.base_topic if context.is_admin else "",
            summary_source=request.summary_source
            or ExternalFeedSource.SummarySource.RSS_DESCRIPTION,
            created=time_utils.get_current_timestamp_pb2(),
            submitted_by=context.current_user_id,
        )
        error = await external_feed_model.create(feed)
        if error:
            context.set_code(StatusCode.INVALID_ARGUMENT)
            context.set_details(f"{request.url} is not a valid RSS feed: {error}")
            return AddExternalFeedResponse()
        await create_external_feed_topic.publish(
            event=CreateExternalFeedEvent(feed=feed), session=context
        )
        return AddExternalFeedResponse(feed=feed)

    async def EditExternalFeed(
        self, request: EditExternalFeedRequest, context: ServicerContext
    ) -> EditExternalFeedResponse:
        logging.info(f"EditExternalFeed: {request}")
        website = url_normalize.url_normalize(request.website)
        if website and not website.startswith("http"):
            context.set_code(StatusCode.INVALID_ARGUMENT)
            context.set_details(f"Feed website must start with http.")
            return EditExternalFeedResponse()

        feed = await external_feed_model.retrieve(request.feed_id)
        if not feed:
            context.set_code(StatusCode.INVALID_ARGUMENT)
            context.set_details(f"Feed ID {request.feed_id} does not exist")
            return EditExternalFeedResponse()

        if feed.name != request.name:
            feed_with_same_name = await external_feed_model.get_feeds(
                feed_ids=[], feed_names=[request.name]
            )
            if feed_with_same_name:
                context.set_code(StatusCode.INVALID_ARGUMENT)
                context.set_details(f"{request.name} already exists")
                return EditExternalFeedResponse()

        modified_feed = ExternalFeedSource(
            name=request.name,
            type=request.type or ExternalFeedSource.Type.RSS,
            website=request.website,
            icon_url=request.icon_url,
            base_topic=request.base_topic if context.is_admin else "",
            summary_source=request.summary_source
            or ExternalFeedSource.SummarySource.RSS_DESCRIPTION,
        )
        upload_icon = len(request.icon_url) != 0
        feed.MergeFrom(modified_feed)
        error = await external_feed_model.update(feed, upload_icon)
        if error:
            context.set_code(StatusCode.INVALID_ARGUMENT)
            context.set_details(f"Feed ID {request.feed_id} is invalid: {error}")
            return EditExternalFeedResponse()

        await update_external_feed_topic.publish(
            event=EditExternalFeedEvent(feed=feed), session=context
        )
        return EditExternalFeedResponse(feed=feed)

    async def TestExternalFeed(
        self, request: TestExternalFeedRequest, context: ServicerContext
    ) -> TestExternalFeedResponse:
        logging.info(f"TestExternalFeed: {request}")
        feed = await external_feed_model.retrieve_from_url(request.url)
        if feed:
            context.set_code(StatusCode.ALREADY_EXISTS)
            context.set_custom_status("DUPLICATE_FEED")
            context.set_details(f"{request.url} already exists")
            return TestExternalFeedResponse(feed=feed)

        feed, error = await external_feed_model.test(url=request.url)
        if error:
            context.set_code(StatusCode.INVALID_ARGUMENT)
            context.set_details(f"Failed to parse feed {request.url}. Error = {error}")
            return TestExternalFeedResponse()

        context.set_custom_status("NEW_FEED")
        return TestExternalFeedResponse(feed=feed)

    async def RemoveExternalFeed(
        self, request: RemoveExternalFeedRequest, context: ServicerContext
    ) -> RemoveExternalFeedResponse:
        logging.info(f"RemoveExternalFeed: {request}")
        feed = await external_feed_model.delete(request.feed_id)
        if not feed:
            context.set_code(StatusCode.INVALID_ARGUMENT)
            context.set_details(f"External Feed {request.feed_id} does not exist")
            return RemoveExternalFeedResponse()
        await delete_external_feed_topic.publish(
            DeleteExternalFeedEvent(feed_id=request.feed_id), context
        )
        return RemoveExternalFeedResponse(feed=feed)

    async def GetFeeds(
        self, request: GetFeedsRequest, context: ServicerContext
    ) -> GetFeedsResponse:
        logging.info(f"GetExternalFeeds: {request}")
        feeds = await external_feed_model.get_feeds(
            feed_ids=request.feed_ids, feed_names=request.feed_names
        )
        return GetFeedsResponse(feeds=feeds)

    async def Unfurl(
        self, request: UnfurlRequest, context: ServicerContext
    ) -> UnfurlResponse:
        logging.info(f"Unfurl: {request}")
        response, status = await extract_url.unfurl(request.url)
        if status:
            context.set_code(status.code)
            context.set_details(status.details)
            return UnfurlResponse()
        return response

    async def DownloadImage(
        self, request: DownloadImageRequest, context: ServicerContext
    ) -> DownloadImageResponse:
        logging.info(f"DownloadImage: {request}")
        image_id, status = await download_image.add_picture_from_url(request.url)
        if status:
            context.set_code(status.code)
            context.set_details(status.details)
            return DownloadImageResponse()
        return DownloadImageResponse(image_id=image_id)

    async def FollowFeed(
        self, request: FollowFeedRequest, context: ServicerContext
    ) -> FollowFeedResponse:
        logging.info(f"Processing FollowFeed: {request}")
        external_feed = await external_feed_model.retrieve(request.feed_id)
        if not external_feed:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f"Feed {request.feed_id} does not exist")
            return FollowFeedResponse()

        success = await external_feed_model.follow(
            user_id=context.current_user_id,
            feed_id=request.feed_id,
            settings=request.settings,
        )
        if success:
            await topic.follow_external_feed_topic.publish(
                event=FollowExternalFeedEvent(
                    feed_id=request.feed_id,
                    settings=request.settings,
                    followers=external_feed.followers + 1,
                ),
                session=context,
            )
        return FollowFeedResponse()

    async def UnfollowFeed(
        self, request: UnfollowFeedRequest, context: ServicerContext
    ) -> UnfollowFeedResponse:
        logging.info(f"Processing UnfollowFeed: {request}")
        external_feed = await external_feed_model.retrieve(request.feed_id)
        if not external_feed:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f"Feed {request.feed_id} does not exist")
            return FollowFeedResponse()

        success = await external_feed_model.unfollow(
            user_id=context.current_user_id, feed_id=request.feed_id
        )
        if success:
            await topic.unfollow_external_feed_topic.publish(
                event=UnfollowExternalFeedEvent(
                    feed_id=request.feed_id,
                    followers=external_feed.followers - 1
                    if external_feed.followers
                    else 0,
                ),
                session=context,
            )
        return UnfollowFeedResponse()

    async def GetFeedsFollowed(
        self, request: GetFeedsFollowedRequest, context: ServicerContext
    ) -> GetFeedsFollowedResponse:
        logging.info(f"Get Topics Followed: {request}")

        return GetFeedsFollowedResponse(
            followed_feeds=await external_feed_model.retrieve_following(
                context.current_user_id
            )
        )

    async def GetAllExternalFeeds(
        self, request: GetAllExternalFeedsRequest, context: ServicerContext
    ) -> GetAllExternalFeedsResponse:
        logging.info(f"Get All External Feeds: {request}")
        feeds, cursor = await external_feed_model.retrieve_all(request.filter)
        return GetAllExternalFeedsResponse(feeds=feeds, end_cursor=cursor)
