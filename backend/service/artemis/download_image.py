import logging
import httpx
from typing import Optional, Tuple
from grpc import StatusCode, Status
from weclikd.service import storage
from weclikd import exceptions
from weclikd.utils import environment, grpc_utils

bucket = storage.get_bucket(environment.IMAGE_BUCKET)

MAX_CONTENT_SIZE = 2 * 1024 * 1024


async def http_get(url, timeout=5.0):
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36"
    }
    client = httpx.AsyncClient()
    async with client.stream("GET", url, headers=headers, timeout=10.0) as response:
        if int(response.headers.get("Content-Length", 0)) >= MAX_CONTENT_SIZE:
            logging.error(
                f"Content size too large from {url}: length={response.headers.get('Content-Length')}"
            )
            return None, None

        text = bytes()
        async for chunk in response.aiter_bytes():
            text += chunk

            if len(text) >= MAX_CONTENT_SIZE:
                logging.error(f"Content size too large from {url}")
                return None, None

        return text, response.headers


async def add_picture_from_url(url) -> Tuple[int, Optional[grpc_utils.GrpcStatus]]:
    try:
        content, headers = await http_get(url, timeout=10.0)
        if not content:
            return (
                0,
                grpc_utils.GrpcStatus(
                    code=StatusCode.UNAVAILABLE,
                    details=f"{url} has an image that is too large or small.",
                ),
            )
        return (
            int(
                await storage.add_picture_gcp(bucket, content, headers["content-type"])
            ),
            None,
        )

        return DownloadImageResponse(
            status=DownloadImageResponse.SUCCESS,
            image_id=int(
                await storage.add_picture_gcp(bucket, content, headers["content-type"])
            ),
        )
    except httpx.InvalidURL:
        logging.exception(f"Failed to download image from {url}")
        return (
            0,
            grpc_utils.GrpcStatus(
                code=StatusCode.INVALID_ARGUMENT, details=f"{url} is invalid"
            ),
        )
    except httpx.TimeoutException:
        logging.exception(f"Failed to download image from {url}")
        return (
            0,
            grpc_utils.GrpcStatus(
                code=StatusCode.DEADLINE_EXCEEDED, details=f"{url} timed out"
            ),
        )
    except httpx.HTTPError as ex:
        logging.exception(f"Failed to download image from {url}")
        return (
            0,
            grpc_utils.GrpcStatus(
                code=StatusCode.UNAVAILABLE, details=f"{url} failed. {ex}"
            ),
        )
    except exceptions.UnsupportedFormat:
        logging.exception(f"Failed to download image from {url}")
        return (
            0,
            grpc_utils.GrpcStatus(
                code=StatusCode.INVALID_ARGUMENT,
                details=f"{url} has an unsupported image format",
            ),
        )
    except Exception:
        logging.exception(f"Failed to download image from {url}")
        return (
            0,
            grpc_utils.GrpcStatus(
                code=StatusCode.UNKNOWN,
                details=f"{url} has an unsupported image format",
            ),
        )
