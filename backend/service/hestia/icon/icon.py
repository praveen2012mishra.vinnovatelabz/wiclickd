from PIL import Image, ImageDraw, ImageFont
import random
import os
import io
import re
from dataclasses import dataclass
from tempfile import NamedTemporaryFile
from weclikd.service import storage
from weclikd.utils import environment, weclikd_id

bucket = storage.get_bucket(environment.IMAGE_BUCKET)

current_path = os.path.abspath(os.path.dirname(__file__))


@dataclass
class FontSettings:
    path: str
    size: int  # 40-50
    height: int     # 0-100
    width_padding: int = 0

fonts = [
    FontSettings(
        path=os.path.abspath(os.path.join(current_path, "CarterOne-Regular.ttf")),
        size=50,
        height=12
    ),
    FontSettings(
        path=os.path.abspath(os.path.join(current_path, "Lobster-Regular.ttf")),
        size=48,
        height=19,
    ),
    FontSettings(
        path=os.path.abspath(os.path.join(current_path, "Monoton-Regular.ttf")),
        size=49,
        height=13
    ),
    FontSettings(
        path=os.path.abspath(os.path.join(current_path, "PressStart2P-Regular.ttf")),
        size=43,
        height=30,
        width_padding=2,
    ),
]


def camel_case_split(identifier):
    matches = re.finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', identifier)
    return [m.group(0) for m in matches]


async def create(clik_name, store_in_gcp=True):
    words = camel_case_split(clik_name)
    text = ""
    for word in words[:2]:
        text += word[0].upper()

    if len(text) == 1:
        text += word[1].upper()

    rgb = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
    img = Image.new('RGB', (100, 100), color=rgb)
    draw = ImageDraw.Draw(img)

    # Try to get the right font size so it takes up most of the space
    font_settings = random.choice(fonts)
    font = ImageFont.truetype(font_settings.path, font_settings.size)
    w, _ = draw.textsize(text, font=font)

    # Figure out font color based off of how dark the background color is
    if (rgb[0] + rgb[1] + rgb[2]) / 3 < 122:
        font_color = "white"
    else:
        font_color = "black"

    # Draw and store it in GCP
    draw.text(((100-w)/2 + font_settings.width_padding, font_settings.height), text, font=font, fill=font_color)
    img_byte_arr = io.BytesIO()
    img.save(img_byte_arr, format='PNG')
    img_byte_arr = img_byte_arr.getvalue()
    if store_in_gcp:
        return int(await storage.add_picture_gcp(bucket, img_byte_arr, content_type='image/png'))
    else:
        with open(f'/tmp/icons/{text}.png', 'wb') as f:
            f.write(img_byte_arr)
