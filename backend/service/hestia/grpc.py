import logging
from datetime import datetime, timedelta
from grpc import ServicerContext, StatusCode
from hestia.api.hestia_pb2_grpc import HestiaServicer
from hestia.api.hestia_pb2 import (
    GetTrendingCliksRequest,
    GetTrendingCliksResponse,
    IsClikMemberRequest,
    IsClikMemberResponse,
    GetClikInfosRequest,
    GetClikInfosResponse,
    JoinClikRequest,
    JoinClikResponse,
    CreateClikRequest,
    CreateClikResponse,
    InviteToClikRequest,
    InviteToClikResponse,
    GetMembersOfClikRequest,
    GetMembersOfClikResponse,
    GetApplicationsToClikRequest,
    GetApplicationsToClikResponse,
    ApproveClikRequest,
    ApproveClikResponse,
    GetCliksToApproveRequest,
    GetCliksToApproveResponse,
    DeleteClikRequest,
    DeleteClikResponse,
    ClikApplication,
    EditClikRequest,
    EditClikResponse,
    RejectClikMemberRequest,
    RejectClikMemberResponse,
    PromoteClikMemberRequest,
    PromoteClikMemberResponse,
    KickClikMemberRequest,
    KickClikMemberResponse,
    FollowClikRequest,
    FollowClikResponse,
    UnfollowClikRequest,
    UnfollowClikResponse,
    GetCliksFollowedRequest,
    GetCliksFollowedResponse,
    GetAllCliksRequest,
    GetAllCliksResponse,
    CreateClikInviteKeyRequest,
    CreateClikInviteKeyResponse,
    GetClikInviteKeyInfoRequest,
    GetClikInviteKeyInfoResponse,
)
from hestia.icon import icon
from hestia.api.clik_pb2 import ClikMember, Clik, ClikUserApplication
from weclikd.utils.session import Session
from weclikd.utils import weclikd_id, time_utils
from hestia.model import clik_model


class Hestia(HestiaServicer):
    async def GetClikInfos(
        selfself, request: GetClikInfosRequest, context: ServicerContext
    ) -> GetClikInfosResponse:
        logging.info(f"GetClikInfos: {request}")
        cliks = await clik_model.retrieve_many(request.clik_ids)
        return GetClikInfosResponse(cliks={clik.id: clik for clik in cliks})

    async def IsClikMember(
        self, request: IsClikMemberRequest, context: ServicerContext
    ) -> IsClikMemberResponse:
        logging.info(f"IsClikMember: {request}")
        response = IsClikMemberResponse()
        for clik_name in request.cliks:
            clik_id = weclikd_id.string_to_id(clik_name)
            member = await clik_model.get_member(
                user_id=context.current_user_id, clik_id=clik_id
            )
            response.clik_members[clik_name] = (
                member.type if member else ClikMember.UNKNOWN
            )

        return response

    async def GetTrendingCliks(
        self,
        request: GetTrendingCliksRequest,
        context: ServicerContext,
    ) -> GetTrendingCliksResponse:
        logging.info(f"GetTrendingCliks: {request}")
        return GetTrendingCliksResponse(cliks=await clik_model.get_trending())

    async def GetAllCliks(
        self, request: GetAllCliksRequest, context: ServicerContext
    ) -> GetAllCliksResponse:
        logging.info(f"GetAllCliks: {request}")
        cliks, cursor = await clik_model.retrieve_all(request.filter)
        return GetAllCliksResponse(cliks=cliks, end_cursor=cursor)

    async def CreateClik(
        self, request: CreateClikRequest, context: ServicerContext
    ) -> CreateClikResponse:
        clik = await clik_model.retrieve(weclikd_id.string_to_id(request.name))
        if clik:
            context.set_code(StatusCode.ALREADY_EXISTS)
            context.set_details(f"Clik {request.name} already exists")
            return CreateClikResponse()

        if not request.icon_picture_id:
            request.icon_picture_id = await icon.create(request.name)

        clik_application = ClikApplication(
            clik=Clik(
                id=weclikd_id.string_to_id(request.name),
                name=request.name,
                description=request.description,
                banner_picture_id=request.banner_picture_id,
                icon_picture_id=request.icon_picture_id,
                followers=0,
                members=0,
                max_members=request.max_members,
                qualifications=[each for each in request.qualifications],
                website=request.website,
                created=time_utils.get_current_timestamp_pb2(),
                invite_only=request.invite_only,
            ),
            qualification=request.my_qualification,
            user_id=context.current_user_id,
            invited_users=request.invited_users,
        )


        clik = await clik_model.create_clik(clik_application)
        return CreateClikResponse(clik=clik)

    async def InviteToClik(
        self, request: InviteToClikRequest, context: ServicerContext
    ) -> InviteToClikResponse:
        logging.info(f"InviteToClik: {request}")
        is_admin, member = await check_admin(context.current_user_id, request.clik_id)
        if not is_admin:
            context.set_code(StatusCode.PERMISSION_DENIED)
            context.set_details(f"Must be admin to invite other members")
            return InviteToClikResponse()

        for invited_user in request.invited_users:
            if (
                invited_user.member_type != ClikMember.ClikMemberType.SUPER_ADMIN
                and member.type <= invited_user.member_type
            ):
                logging.warning(
                    f"Must be super admin to invite other admins. user_id={context.current_user_id}"
                )
                context.set_code(StatusCode.PERMISSION_DENIED)
                context.set_details(f"Must be super admin to invite other admins")
                return InviteToClikResponse()

        for invited_user in request.invited_users:
            await clik_model.invite(
                clik_id=request.clik_id,
                invited_user=invited_user,
                invitor_user_id=context.current_user_id,
            )

    async def CreateClikInviteKey(
        self, request: CreateClikInviteKeyRequest, context: ServicerContext
    ) -> CreateClikInviteKeyResponse:
        logging.info(f"CreateClikInviteKey: {request}")
        is_admin, member = await check_admin(context.current_user_id, request.clik_id)
        if not is_admin:
            context.set_code(StatusCode.PERMISSION_DENIED)
            context.set_details(f"Must be admin to invite other members")
            return CreateClikInviteKeyResponse()

        if (
            member.type != ClikMember.ClikMemberType.SUPER_ADMIN
            and member.type <= request.member_type
        ):
            logging.warning(
                f"Must be super admin to invite other admins. user_id={context.current_user_id}"
            )
            context.set_code(StatusCode.PERMISSION_DENIED)
            context.set_details(f"Must be super admin to invite other admins")
            return CreateClikInviteKeyResponse()

        invite_key = await clik_model.create_invite_key(
            user_id=context.current_user_id,
            clik_id=request.clik_id,
            member_type=request.member_type,
        )
        return CreateClikInviteKeyResponse(invite_key=invite_key)

    async def GetClikInviteKeyInfo(
        self, request: GetClikInviteKeyInfoRequest, context: ServicerContext
    ) -> GetClikInviteKeyInfoResponse:
        logging.info(f"GetClikInviteKeyInfo: {request}")
        invite_info = await clik_model.get_invite_key(invite_key=request.invite_key)
        expired = datetime.now() - invite_info.created.ToDatetime() > timedelta(days=1)
        return GetClikInviteKeyInfoResponse(
            clik_id=invite_info.clik_id,
            member_type=invite_info.member_type,
            inviter_user_id=invite_info.invitor_user_id,
            created=invite_info.created,
            expired=expired,
        )

    async def JoinClik(
        self, request: JoinClikRequest, context: ServicerContext
    ) -> JoinClikResponse:
        logging.info(f"JoinClik: {request}")
        if not request.clik_id and not request.invite_key:
            context.set_details(StatusCode.INVALID_ARGUMENT)
            context.set_details(f"Must set either specify the key or the invite key")
            return JoinClikResponse()

        if request.invite_key:
            if request.invite_key.startswith("Clik:"):
                clik_id = request.invite_key[5:]
                clik_id = (
                    weclikd_id.string_to_id(clik_id)
                    if not clik_id.isnumeric()
                    else int(clik_id)
                )
                clik = await clik_model.retrieve(clik_id)
                if not clik:
                    context.set_details(StatusCode.NOT_FOUND)
                    context.set_details(f"{request.invite_key} does not exist.")
                    return JoinClikResponse()
                if clik.invite_only:
                    context.set_details(StatusCode.PERMISSION_DENIED)
                    context.set_details(
                        f"{request.invite_key} is invite only. Request an invite key from an admin."
                    )
                    return JoinClikResponse()
                clik_member = await clik_model.get_member(
                    user_id=context.current_user_id, clik_id=clik_id
                )
                if clik_member:
                    context.set_code(StatusCode.ALREADY_EXISTS)
                    context.set_details(f"Already joined clik {request.clik_id}")
                    return JoinClikResponse()

                clik_member = await clik_model.join_clik(
                    application=ClikUserApplication(
                        user_id=context.current_user_id,
                        clik_id=clik_id,
                    ),
                    invite_only=clik.invite_only,
                )
                if clik_member:
                    context.set_custom_status("JOINED")
                    return JoinClikResponse(member_type=clik_member.type)
            else:
                clik_invite = await clik_model.get_invite_key(request.invite_key)
                if not clik_invite:
                    context.set_details(StatusCode.NOT_FOUND)
                    context.set_details(f"Key {request.invite_key} does not exist.")
                    return JoinClikResponse()
                clik_invite.user_id = context.current_user_id
                expired = datetime.now() - clik_invite.created.ToDatetime() > timedelta(
                    days=1
                )
                if expired:
                    context.set_code(StatusCode.DEADLINE_EXCEEDED)
                    context.set_details(f"Invite Key expired")
                    return JoinClikResponse()
                clik_member = await clik_model.get_member(
                    user_id=context.current_user_id, clik_id=request.clik_id
                )
                if clik_member:
                    context.set_code(StatusCode.ALREADY_EXISTS)
                    context.set_details(f"Already joined clik {request.clik_id}")
                    return JoinClikResponse()

                clik_member = await clik_model.join_clik(
                    application=None, invite_only=False, clik_invite=clik_invite
                )
                context.set_custom_status("JOINED")
                return JoinClikResponse(member_type=clik_member.type)
        else:
            clik = await clik_model.retrieve(request.clik_id)
            if not clik:
                context.set_details(StatusCode.NOT_FOUND)
                context.set_details(f"Clik {request.clik_id} does not exist")
                return JoinClikResponse()

            clik_member = await clik_model.get_member(
                user_id=context.current_user_id, clik_id=request.clik_id
            )
            if clik_member:
                context.set_code(StatusCode.ALREADY_EXISTS)
                context.set_details(f"Already joined clik {request.clik_id}")
                return JoinClikResponse()

            clik_member = await clik_model.join_clik(
                application=ClikUserApplication(
                    user_id=context.current_user_id,
                    clik_id=request.clik_id,
                    qualification=request.qualification,
                    known_members=request.known_members,
                ),
                invite_only=clik.invite_only,
            )
            if clik_member:
                context.set_custom_status("JOINED")
                return JoinClikResponse(member_type=clik_member.type)

        context.set_custom_status("PENDING")
        return JoinClikResponse()

    async def GetMembersOfClik(
        self, request: GetMembersOfClikRequest, context: ServicerContext
    ) -> GetMembersOfClikResponse:
        members, cursor = await clik_model.get_members(
            request.clik_id, filter=request.filter
        )
        return GetMembersOfClikResponse(members=members, end_cursor=cursor)

    async def GetApplicationsToClik(
        self, request: GetApplicationsToClikRequest, context: ServicerContext
    ) -> GetApplicationsToClikResponse:
        session = Session.create_from_grpc(context)
        is_admin, member = await check_admin(session.current_user_id, request.clik_id)
        if not is_admin:
            return GetMembersOfClikResponse()

        applications, cursor = await clik_model.get_user_applications(
            clik_id=request.clik_id, filter=request.filter
        )
        return GetApplicationsToClikResponse(
            applications=applications, end_cursor=cursor
        )

    async def EditClik(
        self, request: EditClikRequest, context: ServicerContext
    ) -> EditClikResponse:
        logging.info(f"Edit Clik: {request}")
        is_admin, member = await check_admin(context.current_user_id, request.clik_id)
        if not is_admin:
            context.set_code(StatusCode.PERMISSION_DENIED)
            context.set_details(f"Must be admin to edit clik.")
            return GetMembersOfClikResponse()

        clik = await clik_model.retrieve(request.clik_id)
        modified_clik = Clik(
            description=request.description,
            banner_picture_id=request.banner_picture_id,
            icon_picture_id=request.icon_picture_id,
            website=request.website,
            qualifications=request.qualifications,
            max_members=request.max_members,
        )
        if modified_clik.qualifications:
            # merge from extends lists instead of replacing
            del clik.qualifications[:]

        clik.MergeFrom(modified_clik)
        clik.invite_only = request.invite_only
        await clik_model.update(clik)
        return EditClikResponse(clik=clik)

    async def DeleteClik(
        self, request: DeleteClikRequest, context: ServicerContext
    ) -> DeleteClikResponse:
        logging.info(f"Delete Clik: {request}")
        clik = await clik_model.retrieve(request.clik_id)
        if not clik:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f"Clik {request.clik_id} does not exist")
            return DeleteClikResponse()
        await clik_model.delete_clik(request.clik_id)
        return DeleteClikResponse(clik=clik)

    async def RejectClikMember(
        self,
        request: RejectClikMemberRequest,
        context: ServicerContext,
    ) -> RejectClikMemberResponse:
        is_admin, member = await check_admin(
            current_user_id=context.current_user_id, clik_id=request.clik_id
        )
        if not is_admin:
            context.set_code(StatusCode.PERMISSION_DENIED)
            context.set_details(f"Must be admin to reject clik members")
            return RejectClikMemberResponse()

        await clik_model.reject_user_application(
            clik_id=request.clik_id, user_id=request.user_id
        )
        return RejectClikMemberResponse()

    async def PromoteClikMember(
        self, request: PromoteClikMemberRequest, context: ServicerContext
    ) -> PromoteClikMemberResponse:
        if not await check_admin_and_user_permission(
            current_user_id=context.current_user_id,
            target_user_id=request.user_id,
            clik_id=request.clik_id,
        ):
            context.set_code(StatusCode.PERMISSION_DENIED)
            context.set_details(f"Must be higher status than member to promote")
            return PromoteClikMemberResponse()

        member = ClikMember(
            user_id=request.user_id, clik_id=request.clik_id, type=request.member_type
        )

        await clik_model.update_member(member)
        return PromoteClikMemberResponse()

    async def KickClikMember(
        self, request: KickClikMemberRequest, context: ServicerContext
    ) -> KickClikMemberResponse:
        clik = await clik_model.retrieve(request.clik_id)
        if not clik:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f"Clik {request.clik_id} does not exist")
            return KickClikMemberResponse()

        if not await check_admin_and_user_permission(
            current_user_id=context.current_user_id,
            target_user_id=request.user_id,
            clik_id=request.clik_id,
        ):
            context.set_code(StatusCode.PERMISSION_DENIED)
            context.set_details(f"Must be higher status than member to promote")
            return KickClikMemberResponse()

        await clik_model.remove_member(
            user_id=request.user_id, clik=clik, kicker_user_id=context.current_user_id
        )
        return KickClikMemberResponse()

    async def FollowClik(
        self, request: FollowClikRequest, context: ServicerContext
    ) -> FollowClikResponse:
        logging.info(f"Processing FollowClik: {request}")
        clik = await clik_model.retrieve(request.clik_id)
        if not clik:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f"Clik {request.clik_id} does not exist")
            return DeleteClikResponse()

        await clik_model.follow(
            user_id=context.current_user_id,
            clik=clik,
            settings=request.settings,
        )
        return FollowClikResponse()

    async def UnfollowClik(
        self, request: UnfollowClikRequest, context: ServicerContext
    ) -> UnfollowClikResponse:
        logging.info(f"Processing UnfollowClik: {request}")
        clik = await clik_model.retrieve(request.clik_id)
        if not clik:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f"Clik {request.clik_id} does not exist")
            return DeleteClikResponse()

        await clik_model.unfollow(user_id=context.current_user_id, clik=clik)
        return UnfollowClikResponse()

    async def GetCliksFollowed(
        self, request: GetCliksFollowedRequest, context: ServicerContext
    ) -> GetCliksFollowedResponse:
        logging.info(f"Get Clik Followed: {request}")
        followed_cliks = await clik_model.retrieve_following(context.current_user_id)
        return GetCliksFollowedResponse(followed_cliks=followed_cliks)


async def check_admin_and_user_permission(
    current_user_id: int, target_user_id: int, clik_id: int
) -> bool:
    member = await clik_model.get_member(user_id=target_user_id, clik_id=clik_id)
    admin = await clik_model.get_member(user_id=current_user_id, clik_id=clik_id)
    if not admin or admin.type < ClikMember.ClikMemberType.ADMIN:
        logging.warning(
            f"Access Denied: Member has no permissions for clik operation. user_id={current_user_id}"
        )
        return False

    if not member:
        logging.warning(
            f"Member does not exist. user_id={target_user_id} clik_id={clik_id}"
        )
        return False

    if member.type >= admin.type:
        logging.warning(
            f"Access Denied: Member has no permissions for clik operation. user_id={current_user_id}"
        )
        return False
    return True


async def check_admin(current_user_id: int, clik_id: int) -> bool:
    member = await clik_model.get_member(user_id=current_user_id, clik_id=clik_id)
    if not member or member.type < ClikMember.ClikMemberType.ADMIN:
        logging.warning(
            f"Access Denied: Member has no permissions for clik operation. user_id={current_user_id}"
        )
        return False, member

    return True, member
