from sqlalchemy import (
    Table,
    Column,
    Integer,
    ForeignKey,
    JSON,
    SMALLINT,
    VARCHAR,
    text as sqltext,
)
from sqlalchemy.dialects.mysql import TIMESTAMP, BIGINT
from weclikd.service.sql import metadata

UnsignedBigInt = BIGINT(unsigned=True)


ClikTable = Table(
    "clik",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True),
    Column("followers", Integer, nullable=False),
    Column("members", Integer, nullable=False),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

NewClikTable = Table(
    "new_clik",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)


ClikMemberTable = Table(
    "clik_member",
    metadata,
    Column("user_id", UnsignedBigInt, primary_key=True),
    Column("clik_id", UnsignedBigInt, ForeignKey("clik.id"), primary_key=True),
    Column("seniority", UnsignedBigInt, nullable=False),
    Column("type", SMALLINT, nullable=False),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

ClikInviteTable = Table(
    "clik_invite",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True, autoincrement=True),
    Column("clik_id", UnsignedBigInt, ForeignKey("clik.id")),
    Column("user_id", UnsignedBigInt, nullable=True),
    Column("invitor_user_id", UnsignedBigInt, nullable=False),
    Column("member_type", SMALLINT, nullable=False),
    Column("invite_key", VARCHAR(22), nullable=True),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

ClikJoinRequestTable = Table(
    "clik_join_request",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True, autoincrement=True),
    Column("clik_id", UnsignedBigInt, ForeignKey("clik.id")),
    Column("user_id", UnsignedBigInt),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

FollowClikTable = Table(
    "clik_follow",
    metadata,
    Column("user_id", UnsignedBigInt, primary_key=True),
    Column("clik_id", UnsignedBigInt, primary_key=True),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)
