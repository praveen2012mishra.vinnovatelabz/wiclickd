from weclikd.service.sql import SQLDb
from hestia.api.clik_pb2 import Clik, ClikMember, ClikUserApplication
from hestia.api.hestia_pb2 import (
    FollowedClik,
    ClikApplication,
    ClikUserInvitation,
)
from hestia.db.tables import (
    FollowClikTable,
    ClikTable,
    ClikMemberTable,
    NewClikTable,
    ClikJoinRequestTable,
    ClikInviteTable,
)
from google.protobuf.message import Message
from typing import Optional, List, Tuple
from weclikd.common.filter_pb2 import DefaultFilter
import sqlalchemy
import base64


class FollowClikSQLDb(SQLDb):
    def __init__(self):
        super().__init__(FollowClikTable, proto_clazz=FollowedClik)

    def to_proto(self, row, msg: Optional[Message] = None) -> FollowedClik:
        proto: FollowedClik = super().to_proto(row, msg)
        proto.clik.id = row.clik_id
        proto.settings.follow_type = row.data["follow_type"]
        return proto


class ClikDB(SQLDb):
    def __init__(self):
        super().__init__(ClikTable, proto_clazz=Clik, order_by=ClikTable.c.id)

    async def get_trending(self, filter: DefaultFilter) -> Tuple[List[Clik], str]:
        decoded_cursor = (
            base64.b64decode(filter.cursor).decode() if filter.cursor else None
        )
        num_followers, last_id = (
            decoded_cursor.split(",") if decoded_cursor else ("99999999999", 0)
        )

        rows = await self.sql_instance.db.fetch_all(
            sqlalchemy.select(
                [ClikTable],
                whereclause=sqlalchemy.or_(
                    ClikTable.c.followers < num_followers,
                    sqlalchemy.and_(
                        ClikTable.c.followers == num_followers, ClikTable.c.id > last_id
                    ),
                ),
                limit=filter.num_items,
            )
            .order_by(ClikTable.c.followers.desc())
            .order_by(ClikTable.c.id.asc())
        )
        cursor = (
            base64.b64encode(f"{rows[-1].followers},{rows[-1].id}".encode()).decode()
            if rows
            else ""
        )
        return [self.to_proto(row) for row in rows], cursor


clik_member_db = SQLDb(
    ClikMemberTable, proto_clazz=ClikMember, order_by=ClikMemberTable.c.seniority
)
clik_follow_db = FollowClikSQLDb()
clik_db = ClikDB()
clik_application_db = SQLDb(
    NewClikTable, proto_clazz=ClikApplication, order_by=NewClikTable.c.id
)
clik_user_application_db = SQLDb(
    ClikJoinRequestTable,
    proto_clazz=ClikUserApplication,
    order_by=ClikJoinRequestTable.c.id,
)
clik_user_invitation_db = SQLDb(
    ClikInviteTable, proto_clazz=ClikUserInvitation, order_by=ClikInviteTable.c.id
)
