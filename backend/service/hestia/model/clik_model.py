import time
import binascii
from typing import List, Tuple
from hestia.api.hestia_pb2 import (
    InvitedUser,
    FollowedClik,
    ClikApplication,
    ClikUserInvitation,
)
from hestia.api.clik_pb2 import Clik, ClikMember, ClikUserApplication
from weclikd.common.filter_pb2 import DefaultFilter
from hestia.db.clik_db import (
    clik_db,
    clik_follow_db,
    clik_member_db,
    clik_application_db,
    clik_user_application_db,
    clik_user_invitation_db,
)
from hestia.event.clik_pb2 import FollowClikEvent, UnfollowClikEvent
from hestia.event.clik_pb2 import (
    CreateClikEvent,
    DeleteClikEvent,
    EditClikEvent,
    CreateClikMemberEvent,
    DeleteClikMemberEvent,
    ClikInviteEvent,
    ClikJoinEvent,
)
from weclikd.common.follow_pb2 import FollowSettings
from weclikd.utils.session import Session
from weclikd.service.transaction import Transaction
from hestia.event import topic
import logging
import base64
import uuid


def _calculate_seniority(clik_member: ClikMember):
    seniority = int(time.time() * 1000) + binascii.crc32(
        str(clik_member.clik_id).encode()
    )

    if clik_member.type == ClikMember.ClikMemberType.SUPER_ADMIN:
        seniority += 1 << 62
    elif clik_member.type == ClikMember.ClikMemberType.ADMIN:
        seniority += 1 << 60
    elif clik_member.type == ClikMember.ClikMemberType.MEMBER:
        seniority += 1 << 58
    return seniority


async def retrieve(clik_id: int) -> List[Clik]:
    return await clik_db.get(id=clik_id)


async def retrieve_many(cliks: List[int]) -> List[Clik]:
    return await clik_db.get_many(field_name="id", values=cliks)


async def get_trending() -> List[Clik]:
    cliks, _ = await clik_db.paginate(filter=DefaultFilter(num_items=25))
    return cliks


async def retrieve_all(filter: DefaultFilter) -> Tuple[List[Clik], str]:
    if filter.sort == DefaultFilter.TRENDING:
        return await clik_db.get_trending(filter)

    return await clik_db.paginate(filter)


############################
# Member Related Functions
############################


async def follow(user_id: int, clik: Clik, settings: FollowSettings) -> bool:
    async with Transaction(sql=True):
        inserted = await clik_follow_db.upsert(
            user_id=user_id,
            clik_id=clik.id,
            data={"follow_type": settings.follow_type},
        )
        if inserted:
            await clik_db.increment("followers", id=clik.id)
            await topic.follow_clik_topic.publish(
                event=FollowClikEvent(
                    clik_id=clik.id, settings=settings, followers=clik.followers + 1
                ),
                session=Session(user_id=user_id),
            )


async def unfollow(user_id: int, clik: Clik) -> bool:
    async with Transaction(sql=True):
        await remove_member(user_id=user_id, clik=clik, kicker_user_id=user_id)

        deleted = await clik_follow_db.delete(user_id=user_id, clik_id=clik.id)
        if deleted:
            await clik_db.decrement("followers", id=clik.id)
            await topic.unfollow_clik_topic.publish(
                event=UnfollowClikEvent(
                    clik_id=clik.id,
                    followers=clik.followers - 1 if clik.followers else 0,
                ),
                session=Session(user_id=user_id),
            )


async def retrieve_following(user_id) -> List[FollowedClik]:
    async with Transaction(sql=True):
        followed_cliks = await clik_follow_db.query(user_id=user_id)
        members = await clik_member_db.query(user_id=user_id)
        clik_id_to_followed_clik = {each.clik.id: each for each in followed_cliks}
        for member in members:
            if member.clik_id in clik_id_to_followed_clik:
                clik_id_to_followed_clik[member.clik_id].member_type = member.type

        cliks = await clik_db.get_many(
            field_name="id",
            values=clik_id_to_followed_clik.keys(),
        )
        for clik in cliks:
            clik_id_to_followed_clik[clik.id].clik.CopyFrom(clik)
    return followed_cliks


############################
# Member Related Functions
############################


async def get_member(user_id: int, clik_id: int) -> ClikMember:
    return await clik_member_db.get(user_id=user_id, clik_id=clik_id)


async def add_member(
    clik_member: ClikMember,
    application: ClikUserApplication = None,
    inviter_user_id: int = 0,
) -> bool:
    logging.info(
        f"Adding Member clik_id={clik_member.clik_id} user_id={clik_member.user_id}"
    )
    async with Transaction(sql=True):
        clik = await retrieve(clik_member.clik_id)
        await clik_member_db.insert(
            message=clik_member,
            seniority=_calculate_seniority(clik_member),
            type=clik_member.type,
        )
        await clik_db.increment("members", id=clik_member.clik_id)
        await follow(
            user_id=clik_member.user_id,
            clik=clik,
            settings=FollowSettings(follow_type=FollowSettings.FollowType.FAVORITE),
        )

    await topic.create_clik_member_topic.publish(
        event=CreateClikMemberEvent(
            member=clik_member,
            application=application or ClikUserApplication(),
            inviter_user_id=inviter_user_id,
            members=clik.members + 1,
        ),
        session=Session(),
    )


async def remove_member(user_id: int, clik: Clik, kicker_user_id: int) -> bool:
    async with Transaction(sql=True):
        deleted = await clik_member_db.delete(user_id=user_id, clik_id=clik.id)
        if deleted:
            await clik_db.decrement("members", id=clik.id)
            await topic.delete_clik_member_topic.publish(
                event=DeleteClikMemberEvent(
                    member=ClikMember(clik_id=clik.id, user_id=user_id),
                    kicker_user_id=kicker_user_id,
                    members=clik.members - 1 if clik.members else 0,
                ),
                session=Session(),
            )
            return True
        return False


async def update_member(clik_member: ClikMember):
    await clik_member_db.upsert(
        user_id=clik_member.user_id,
        clik_id=clik_member.clik_id,
        type=clik_member.type,
        seniority=_calculate_seniority(clik_member),
        data={},
    )


async def get_members(
    clik_id: int, filter: DefaultFilter
) -> Tuple[List[ClikMember], str]:
    return await clik_member_db.paginate(clik_id=clik_id, filter=filter)


async def get_admins(clik_id: int) -> Tuple[List[ClikMember], str]:
    members, _ = await clik_member_db.paginate(
        clik_id=clik_id, filter=DefaultFilter(num_items=10)
    )
    return [member for member in members if member.type >= ClikMember.ADMIN]


############################
# Creating New Clik
############################


async def create_clik(clik_application: ClikApplication):
    async with Transaction(sql=True):
        await clik_db.insert(clik_application.clik)
        await clik_application_db.delete(id=clik_application.id)
        await add_member(
            clik_member=ClikMember(
                clik_id=clik_application.clik.id,
                user_id=clik_application.user_id,
                type=ClikMember.SUPER_ADMIN,
            )
        )
        for invited_user in clik_application.invited_users:
            await invite(
                clik_id=clik_application.clik.id,
                invited_user=invited_user,
                invitor_user_id=clik_application.user_id,
            )
        clik_application.clik.members = 1
        clik_application.clik.followers = 1
    await topic.create_clik_topic.publish(
        event=CreateClikEvent(clik=clik_application.clik),
        session=Session(user_id=clik_application.user_id),
    )
    return clik_application.clik


async def delete_clik(clik_id):
    await clik_db.delete(id=clik_id)
    await topic.delete_clik_topic.publish(
        event=DeleteClikEvent(clik_id=clik_id), session=Session()
    )


async def update(clik: Clik):
    await clik_db.update(clik, id=clik.id)

    await topic.update_clik_topic.publish(
        event=EditClikEvent(clik=clik), session=Session()
    )


############################
# Inviting / Joining Clik
############################


async def invite(clik_id: int, invited_user: InvitedUser, invitor_user_id: int):
    async with Transaction(sql=True):
        if invited_user.user_id:
            # check if user is already a member
            member = await get_member(invited_user.user_id, clik_id)
            if member and member.type >= ClikMember.ClikMemberType.MEMBER:
                logging.warning(f"User is already a member of the clik")
                return

            # check if user requested to join already
            application = await get_user_application(
                user_id=invited_user.user_id, clik_id=clik_id
            )
            if application:
                logging.info(f"Adding member {invited_user.user_id} to clik {clik_id}")
                await add_member(
                    clik_member=ClikMember(
                        clik_id=clik_id,
                        user_id=invited_user.user_id,
                        type=invited_user.member_type,
                    ),
                    inviter_user_id=invitor_user_id,
                    application=application,
                )

                await delete_user_application(
                    user_id=invited_user.user_id, clik_id=clik_id
                )
                return

            logging.info(f"Inviting member {invited_user.user_id} to clik {clik_id}")
            await clik_user_invitation_db.insert(
                ClikUserInvitation(
                    user_id=invited_user.user_id,
                    clik_id=clik_id,
                    member_type=invited_user.member_type,
                    invitor_user_id=invitor_user_id,
                )
            )
            await topic.invite_clik_topic.publish(
                event=ClikInviteEvent(
                    inviter_user_id=invitor_user_id,
                    invitee_user_id=invited_user.user_id,
                    clik_id=clik_id,
                )
            )


async def create_invite_key(user_id: int, clik_id: int, member_type: int) -> str:
    invite_key = base64.urlsafe_b64encode(uuid.uuid4().bytes).decode()[0:10]
    await clik_user_invitation_db.insert(
        ClikUserInvitation(
            clik_id=clik_id,
            member_type=member_type,
            invitor_user_id=user_id,
            invite_key=invite_key,
        )
    )
    return invite_key


async def get_invite_key(invite_key: str) -> ClikUserApplication:
    return await clik_user_invitation_db.get(invite_key=invite_key)


async def get_user_application(clik_id: int, user_id: int) -> ClikUserApplication:
    return await clik_user_application_db.get(clik_id=clik_id, user_id=user_id)


async def delete_user_application(clik_id: int, user_id: int):
    await clik_user_application_db.delete(clik_id=clik_id, user_id=user_id)


async def join_clik(
    application: ClikUserApplication,
    invite_only: bool,
    clik_invite: ClikUserInvitation = None,
) -> ClikMember:
    clik_invite = (
        await clik_user_invitation_db.get(
            clik_id=application.clik_id, user_id=application.user_id
        )
        if not clik_invite
        else clik_invite
    )

    if clik_invite or not invite_only:
        member = ClikMember(
            clik_id=application.clik_id if application else clik_invite.clik_id,
            user_id=application.user_id if application else clik_invite.user_id,
            type=clik_invite.member_type if clik_invite else ClikMember.MEMBER,
        )
        await add_member(
            clik_member=member,
            application=None,  # no need for application, already invited
            inviter_user_id=clik_invite.invitor_user_id if clik_invite else 0,
        )
        return member

    await clik_user_application_db.insert(application)
    admins = await get_admins(application.clik_id)
    await topic.join_clik_topic.publish(
        event=ClikJoinEvent(
            invitee_user_id=application.user_id,
            admins=[admin.user_id for admin in admins],
            clik_id=application.clik_id,
        ),
        session=Session(),
    )


async def delete_user_invitation(user_invitation: ClikUserInvitation):
    await clik_user_invitation_db.delete(
        clik_id=user_invitation.clik_id, user_id=user_invitation.user_id
    )


async def reject_user_application(user_id: int, clik_id: int):
    return await delete_user_application(user_id=user_id, clik_id=clik_id)


async def get_user_applications(
    clik_id: int, filter: DefaultFilter
) -> Tuple[List[ClikUserApplication], str]:
    return await clik_user_application_db.paginate(
        clik_id=clik_id, filter=filter, descending=False
    )
