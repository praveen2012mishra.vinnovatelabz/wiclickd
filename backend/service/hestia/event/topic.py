from weclikd.service import pubsub
from hestia.event import clik_pb2


create_clik_topic = pubsub.Topic("clik-create", clik_pb2.CreateClikEvent)
update_clik_topic = pubsub.Topic("clik-update", clik_pb2.EditClikEvent)
delete_clik_topic = pubsub.Topic("clik-delete", clik_pb2.DeleteClikEvent)
follow_clik_topic = pubsub.Topic("clik-follow", clik_pb2.FollowClikEvent)
unfollow_clik_topic = pubsub.Topic("clik-unfollow", clik_pb2.UnfollowClikEvent)

create_clik_member_topic = pubsub.Topic(
    "clik-member-create", clik_pb2.CreateClikMemberEvent
)
delete_clik_member_topic = pubsub.Topic(
    "clik-member-delete", clik_pb2.DeleteClikMemberEvent
)
invite_clik_topic = pubsub.Topic("clik-invite", clik_pb2.ClikInviteEvent)
join_clik_topic = pubsub.Topic("clik-join", clik_pb2.ClikJoinEvent)
