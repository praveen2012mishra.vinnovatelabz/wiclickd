from weclikd.service import pubsub
from eros.event import topic as eros_topic


follow_clik_subscription = pubsub.Subscription(eros_topic.follow_clik_topic, "hestia")
unfollow_clik_subscription = pubsub.Subscription(
    eros_topic.unfollow_clik_topic, "hestia"
)
