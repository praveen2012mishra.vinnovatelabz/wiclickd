from sqlalchemy import (
    Table,
    Column,
    Integer,
    ForeignKey,
    JSON,
    VARCHAR,
    BINARY,
    text as sqltext,
)
from sqlalchemy.dialects.mysql import TIMESTAMP
from weclikd.service.sql import UnsignedBigInt, metadata


AccountTable = Table(
    "account",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True),
    Column("data", JSON, nullable=False),
    Column("hashed_access_key", BINARY(32)),
    Column("firebase_id", VARCHAR(28)),
    Column("num_unread_notifications", Integer),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

ExternalAccountTable = Table(
    "external_account",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True),
    Column("account_id", UnsignedBigInt, ForeignKey("account.id")),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

UserTable = Table(
    "user",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True),
    Column("account_id", UnsignedBigInt, ForeignKey("account.id")),
    Column("username", VARCHAR(16), unique=True, nullable=False),
    Column("followers", Integer, default=0),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

FollowUserTable = Table(
    "user_follow",
    metadata,
    Column("user_id", UnsignedBigInt, ForeignKey("user.id"), primary_key=True),
    Column("followed_user_id", UnsignedBigInt, ForeignKey("user.id"), primary_key=True),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

NotificationTable = Table(
    "notification",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True, autoincrement=True),
    Column("user_id", UnsignedBigInt),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)
