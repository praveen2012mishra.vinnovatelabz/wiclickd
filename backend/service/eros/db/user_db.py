from eros.db.sql_tables import (
    UserTable,
    FollowUserTable,
    AccountTable,
    NotificationTable,
)
from eros.api.eros_pb2 import FollowedUser
from eros.api.account_pb2 import Account
from eros.api.profile_pb2 import Profile
from eros.api.notification_pb2 import Notification
from weclikd.service.sql import SQLDb
from weclikd.common.filter_pb2 import DefaultFilter
from google.protobuf.message import Message
from typing import Optional, Tuple, List
import sqlalchemy
import base64


class FollowUserSQLDb(SQLDb):
    def __init__(self):
        super().__init__(FollowUserTable, proto_clazz=FollowedUser)

    def to_proto(self, row, msg: Optional[Message] = None) -> FollowedUser:
        proto: FollowedUser = super().to_proto(row, msg)
        proto.user.id = row.followed_user_id
        proto.settings.follow_type = row.data["follow_type"]
        return proto


class UserDB(SQLDb):
    def __init__(self):
        super().__init__(UserTable, proto_clazz=Profile, order_by=UserTable.c.id)

    async def get_trending(self, filter: DefaultFilter) -> Tuple[List[Profile], str]:
        decoded_cursor = (
            base64.b64decode(filter.cursor).decode() if filter.cursor else None
        )
        num_followers, last_id = (
            decoded_cursor.split(",") if decoded_cursor else ("99999999999", 0)
        )

        rows = await self.sql_instance.db.fetch_all(
            sqlalchemy.select(
                [UserTable],
                whereclause=sqlalchemy.or_(
                    UserTable.c.followers < num_followers,
                    sqlalchemy.and_(
                        UserTable.c.followers == num_followers, UserTable.c.id > last_id
                    ),
                ),
                limit=filter.num_items,
            )
            .order_by(UserTable.c.followers.desc())
            .order_by(UserTable.c.id.asc())
        )
        cursor = (
            base64.b64encode(f"{rows[-1].followers},{rows[-1].id}".encode()).decode()
            if rows
            else ""
        )
        return [self.to_proto(row) for row in rows], cursor


user_follow_db = FollowUserSQLDb()
user_db = UserDB()
account_db = SQLDb(AccountTable, proto_clazz=Account, order_by=AccountTable.c.id)
notification_db = SQLDb(
    NotificationTable, proto_clazz=Notification, order_by=NotificationTable.c.id
)
