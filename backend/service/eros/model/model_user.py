from typing import Dict, Union, List, Tuple
import sqlalchemy
from sqlalchemy.sql.base import Executable
from eros.api.user_pb2 import User
from eros.api.profile_pb2 import Profile
from eros.api.eros_pb2 import (
    CreateUserRequest,
    GetProfilesRequest,
    EditProfileRequest,
    GetTrendingUsersRequest,
    FollowedUser,
)
from weclikd.utils.model import BaseModel
from weclikd.utils.session import Session
from weclikd.common.filter_pb2 import DefaultFilter
from weclikd.exceptions import MissingRequiredFieldException
from eros.db.user_db import user_db, user_follow_db
from eros.db.sql_tables import UserTable
from weclikd.common.follow_pb2 import FollowSettings
from weclikd.service.transaction import Transaction


async def follow(user_id: int, followed_user_id: int, settings: FollowSettings) -> bool:
    async with Transaction(sql=True):
        inserted = await user_follow_db.upsert(
            user_id=user_id,
            followed_user_id=followed_user_id,
            data={"follow_type": settings.follow_type},
        )
        if inserted:
            await user_db.increment("followers", id=followed_user_id)
            return True
        return False


async def unfollow(user_id: int, followed_user_id: int) -> bool:
    async with Transaction(sql=True):
        deleted = await user_follow_db.delete(
            user_id=user_id, followed_user_id=followed_user_id
        )
        if deleted:
            await user_db.decrement("followers", id=followed_user_id)
            return True
        return False


async def retrieve_following(user_id: int) -> List[FollowedUser]:
    async with Transaction(sql=True):
        followed_users = await user_follow_db.query(user_id=user_id)
        users = await user_db.get_many(
            field_name="id",
            values=[each.user.id for each in followed_users],
        )
    for followed_user in followed_users:
        for user in users:
            if followed_user.user.id == user.id:
                followed_user.user.CopyFrom(user)
                break
    return followed_users


async def retrieve_all(filter: DefaultFilter) -> Tuple[List[Profile], str]:
    if filter.sort == DefaultFilter.TRENDING:
        return await user_db.get_trending(filter)

    return await user_db.paginate(filter)


class UserModel(BaseModel):
    proto_class = User

    @classmethod
    def insert_stmt(cls, request: User, session: Session) -> Executable:
        return sqlalchemy.insert(
            UserTable,
            values={
                "id": request.profile.id,
                "account_id": session.account_id,
                "username": request.profile.username,
                "followers": 0,
                "data": ProfileModel.proto_to_data(
                    request.profile, remove_fields=["id", "created", "username"]
                ),
            },
        )

    @classmethod
    def request_to_proto(cls, request: CreateUserRequest, id: int) -> User:
        if not request.username:
            raise MissingRequiredFieldException("User", "username")

        return User(
            profile=Profile(
                id=id,
                username=request.username,
                full_name=request.full_name if request.full_name else request.username,
                type=Profile.ProfileType.PRIVATE,
                description=request.description
                if request.description
                else "Welcome to my profile.",
                banner_picture_id=request.banner_picture_id,
                profile_picture_id=request.profile_picture_id,
                created=BaseModel.get_current_timestamp(),
            ),
        )

    @classmethod
    def row_to_proto(cls, row: Dict) -> User:
        return User(profile=ProfileModel.row_to_proto(row))

    @classmethod
    def select_many_stmt(cls, account_id):
        return sqlalchemy.select([UserTable], UserTable.c.account_id == account_id)

    @classmethod
    def check_username(cls, username: str):
        return sqlalchemy.select(
            [UserTable.c.username], whereclause=UserTable.c.username == username
        )


class ProfileModel(BaseModel):
    proto_class = Profile

    @classmethod
    def select_stmt(cls, request: int, session: Session = None) -> Executable:
        return sqlalchemy.select([UserTable], UserTable.c.id == request)

    @classmethod
    def select_many_stmt(
        cls,
        request: Union[GetProfilesRequest, GetTrendingUsersRequest],
        session: Session = None,
    ) -> Executable:
        if isinstance(request, GetProfilesRequest):
            return sqlalchemy.select(
                [UserTable],
                sqlalchemy.sql.or_(
                    UserTable.c.id.in_(tuple(request.user_ids)),
                    UserTable.c.username.in_(tuple(request.usernames)),
                ),
            )
        else:
            return sqlalchemy.select([UserTable]).limit(25)

    @classmethod
    def select_ids_stmt(cls, usernames: List[str]):
        return sqlalchemy.select(
            [UserTable.c.id, UserTable.c.username],
            whereclause=UserTable.c.username.in_(tuple(usernames)),
        )

    @classmethod
    def row_to_proto(cls, row: Dict) -> Profile:
        proto = super(ProfileModel, cls).row_to_proto(row)
        proto.username = row["username"] if "username" in row else ""
        return proto

    @classmethod
    def update_stmt(
        cls, profile: Profile, request: EditProfileRequest, session: Session
    ) -> Executable:
        modified = ProfileModel.proto_to_data(request)
        profile.MergeFrom(ProfileModel.row_to_proto(modified))
        updated = ProfileModel.proto_to_data(
            profile, remove_fields=["id", "created", "username"]
        )
        return sqlalchemy.update(
            UserTable,
            whereclause=UserTable.c.id == session.current_user_id,
            values={"username": profile.username, "data": updated},
        )
