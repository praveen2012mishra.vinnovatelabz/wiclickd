import time
import sqlalchemy

from typing import Dict, Tuple, List
from google.protobuf.timestamp_pb2 import Timestamp
from eros.api.eros_pb2 import CreateAccountRequest, GetAccountRequest
from eros.api.account_pb2 import (
    PrivacySettings,
    EmailNotificationSettings,
    Account,
    AccountSettings,
)
from eros.api.notification_pb2 import Notification, CommentNotification
from weclikd.common.subscription_pb2 import Subscription
from weclikd.common.wrappers_pb2 import BoolEnum
from weclikd.common.filter_pb2 import DefaultFilter
from weclikd.utils.model import BaseModel
from sqlalchemy.sql.base import Executable
from eros.db.user_db import notification_db, account_db
from eros.db.sql_tables import AccountTable
from atlas.api.comment_pb2 import Comment


async def add_comment_notification(comment: Comment, parent_user_id: int):
    if parent_user_id == comment.content_info.user_id or not parent_user_id:
        # no notification if commented on your own content
        return

    notification = Notification(
        comment=CommentNotification(
            my_content_id=comment.thread_ids[-1],
            their_comment_id=comment.content_info.id,
            post_id=comment.thread_ids[0],
            thread_ids=comment.thread_ids[1:] + [comment.content_info.id],
        )
    )
    await notification_db.insert(message=notification, user_id=parent_user_id)
    await increment_unread_notification(parent_user_id)


async def add_notification(user_id: int, notification: Notification):
    await notification_db.insert(message=notification, user_id=user_id)
    await increment_unread_notification(user_id)


async def get_notification(user_id: int, notification_id: int):
    return await notification_db.get(id=notification_id, user_id=user_id)


async def delete_notification(user_id: int, notification_id: int):
    await notification_db.delete(id=notification_id, user_id=user_id)


async def increment_unread_notification(user_id: int):
    await account_db.increment(field_name="num_unread_notifications", id=user_id)


async def mark_notifications_as_read(user_id: int):
    await account_db.sql_instance.db.execute(
        sqlalchemy.update(
            AccountTable,
            whereclause=AccountTable.c.id == user_id,
            values={"num_unread_notifications": 0},
        )
    )


async def get_num_unread_notifications(user_id: int):
    row = await account_db.sql_instance.db.fetch_one(
        sqlalchemy.select(
            [AccountTable.c.num_unread_notifications],
            whereclause=AccountTable.c.id == user_id,
        )
    )
    if not row:
        return 0
    return row.num_unread_notifications


async def get_notifications(
    user_id: int, filter: DefaultFilter
) -> Tuple[List[Notification], str]:
    return await notification_db.paginate(filter=filter, user_id=user_id)


class AccountModel(BaseModel):
    proto_class = Account

    @classmethod
    def insert_stmt(cls, request: Account, firebase_id: str) -> Executable:
        return sqlalchemy.insert(
            AccountTable,
            values={
                "id": request.id,
                "data": BaseModel.proto_to_data(request),
                "firebase_id": firebase_id,
            },
        )

    @classmethod
    def select_stmt(cls, request: GetAccountRequest) -> Executable:
        return sqlalchemy.select([AccountTable]).where(
            AccountTable.c.id == request.account_id
        )

    @classmethod
    def update_stmt(self, account: Account) -> Executable:
        return sqlalchemy.update(
            AccountTable,
            whereclause=AccountTable.c.id == account.id,
            values={"data": BaseModel.proto_to_data(account)},
        )

    @classmethod
    def default_account_settings(cls, email=None):
        return AccountSettings(
            subscription=Subscription.SubscriptionType.BASIC,
            privacy=PrivacySettings(dm_settings=PrivacySettings.DMSettings.EVERYONE),
            email_notifications=EmailNotificationSettings(
                notification_email=email,
                monthly_earnings=BoolEnum.TRUE,
                clik_notifications=BoolEnum.TRUE,
                weclikd_updates=BoolEnum.TRUE,
            ),
        )

    @classmethod
    def row_to_proto(cls, row: Dict) -> Account:
        account = super(AccountModel, cls).row_to_proto(row)
        settings = row.data.get("settings", {})
        if not settings.get("privacy") or not settings.get("email_notifications"):
            default_settings = cls.default_account_settings(account.email)
            default_settings.subscription = 1
            account.settings.MergeFrom(default_settings)
        return account

    @classmethod
    def request_to_proto(cls, request: CreateAccountRequest, id: int) -> Account:
        return Account(
            id=id,
            email=request.email,
            first_name=request.first_name,
            last_name=request.last_name,
            settings=cls.default_account_settings(request.email),
            created=Timestamp(seconds=int(time.time())),
        )

    @classmethod
    def update_access_key(cls, account_id: int, hashed_access_key: bytes) -> Executable:
        return sqlalchemy.update(
            AccountTable,
            whereclause=AccountTable.c.id == account_id,
            values={"hashed_access_key": hashed_access_key},
        )

    @classmethod
    def get_account_from_access_key(cls, hashed_access_key: bytes) -> Executable:
        return sqlalchemy.select(
            [AccountTable],
            whereclause=AccountTable.c.hashed_access_key == hashed_access_key,
        )

    @classmethod
    def get_firebase_id(cls, account_id: int) -> Executable:
        return sqlalchemy.select(
            [AccountTable.c.firebase_id], whereclause=AccountTable.c.id == account_id
        )
