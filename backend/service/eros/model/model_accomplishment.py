from typing import List
from eros.api.accomplishment_pb2 import Accomplishment


async def add(accomplishment: Accomplishment):
    accomplishment.id = await table.insert(accomplishment)


async def get(accomplishment_id: int) -> Accomplishment:
    return await table.get(accomplishment_id)


async def remove(accomplishment: Accomplishment):
    await table.remove(accomplishment.id)


async def update(accomplishment: Accomplishment):
    await table.insert(accomplishment)


async def get_many(user_id: int) -> List[Accomplishment]:
    return await table.query(("user_ids", "=", user_id))
