from sanic import Blueprint
from weclikd.service import pubsub
from eros.event import subscription
from eros.api.notification_pb2 import (
    Notification,
    ClikJoinRequestNotification,
    ClikJoinAcceptedNotification,
    ClikInviteRequestNotification,
    ClikInviteAcceptedNotification,
)
from eros.model import model_account
from atlas.event.content_pb2 import CreateCommentEvent
from hestia.event.clik_pb2 import ClikJoinEvent, ClikInviteEvent, CreateClikMemberEvent

from weclikd.utils.session import Session

eros_api = Blueprint("eros", url_prefix="/eros")


async def create_comment_callback(event: CreateCommentEvent, session: Session):
    if event.HasField("parent_comment"):
        await model_account.add_comment_notification(
            comment=event.comment,
            parent_user_id=event.parent_comment.content_info.user_id,
        )
    else:
        await model_account.add_comment_notification(
            comment=event.comment,
            parent_user_id=event.post.content_info.user_id or event.post.sharer_id,
        )


async def join_clik_callback(event: ClikJoinEvent, session: Session):
    for invitor_user_id in event.admins:
        await model_account.add_notification(
            user_id=invitor_user_id,
            notification=Notification(
                clik_join_request=ClikJoinRequestNotification(
                    invitee_user_id=event.invitee_user_id, clik_id=event.clik_id
                )
            ),
        )


async def invite_clik_callback(event: ClikInviteEvent, session: Session):
    await model_account.add_notification(
        user_id=event.invitee_user_id,
        notification=Notification(
            clik_invite_request=ClikInviteRequestNotification(
                inviter_user_id=event.inviter_user_id, clik_id=event.clik_id
            )
        ),
    )


async def new_clik_member_callback(event: CreateClikMemberEvent, session: Session):
    if event.application.user_id:
        await model_account.add_notification(
            user_id=event.member.user_id,
            notification=Notification(
                clik_join_accepted=ClikJoinAcceptedNotification(
                    inviter_user_id=event.inviter_user_id, clik_id=event.member.clik_id
                )
            ),
        )
    if event.inviter_user_id:
        await model_account.add_notification(
            user_id=event.inviter_user_id,
            notification=Notification(
                clik_invite_accepted=ClikInviteAcceptedNotification(
                    invitee_user_id=event.member.user_id, clik_id=event.member.clik_id
                )
            ),
        )


# follow / unfollow
pubsub.add_push_route(
    blueprint=eros_api,
    callback=create_comment_callback,
    subscription=subscription.create_comment_subscription,
)

pubsub.add_push_route(
    blueprint=eros_api,
    callback=invite_clik_callback,
    subscription=subscription.invite_clik_subscription,
)

pubsub.add_push_route(
    blueprint=eros_api,
    callback=join_clik_callback,
    subscription=subscription.join_clik_subscription,
)

pubsub.add_push_route(
    blueprint=eros_api,
    callback=new_clik_member_callback,
    subscription=subscription.create_clik_member_subscription,
)
