import os
import sys
import logging

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from .micro_service import get_micro_service

logging.getLogger().setLevel(logging.DEBUG)

service = get_micro_service()
service.start()
