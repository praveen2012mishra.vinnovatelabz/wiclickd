from firebase_admin import auth
from weclikd.utils import async_utils
from weclikd.utils import environment
from typing import Any
import logging


@async_utils.wrap
def modify_claim(firebase_id: str, name: str, value: Any):
    return modify_claim_sync(firebase_id, name, value)


def modify_claim_sync(firebase_id: str, name: str, value: Any):
    if not environment.is_test_env():
        user = auth.get_user(firebase_id)
        claims = user.custom_claims or {}
        if value:
            # set claim
            claims[name] = value
        else:
            if name in claims:
                # remove claim if value is False or None
                del claims[name]

        auth.set_custom_user_claims(firebase_id, claims)
        return claims
    else:
        logging.info(f"Mock Setting User({firebase_id}) Claim {name} = {value}")
