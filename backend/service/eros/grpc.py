import logging
from grpc import ServicerContext, StatusCode
from eros.api.eros_pb2_grpc import ErosServicer
from eros.api.eros_pb2 import (
    CreateAccountRequest,
    CreateAccountResponse,
    CreateUserRequest,
    GetAccountRequest,
    GetAccountResponse,
    GetProfilesRequest,
    GetProfilesResponse,
    EditProfileRequest,
    EditProfileResponse,
    GetUsersFollowedRequest,
    GetUsersFollowedResponse,
    GetTrendingUsersRequest,
    GetTrendingUsersResponse,
    ChangeAccountSettingsRequest,
    ChangeAccountSettingsResponse,
    ChangeSubscriptionRequest,
    ChangeSubscriptionResponse,
    GetIdFromUsernameRequest,
    GetIdFromUsernameResponse,
    FollowUserRequest,
    FollowUserResponse,
    UnfollowUserRequest,
    UnfollowUserResponse,
    GetNotificationsRequest,
    GetNotificationsResponse,
    GetNumUnreadNotificationsRequest,
    GetNumUnreadNotificationsResponse,
    MarkNotificationsAsReadRequest,
    MarkNotificationsAsReadResponse,
    DeleteNotificationRequest,
    DeleteNotificationResponse,
    ChangeAccessKeyRequest,
    ChangeAccessKeyResponse,
    GetAccountIdFromAccessKeyRequest,
    GetAccountIdFromAccessKeyResponse,
    CheckUsernameRequest,
    CheckUsernameResponse,
    GetAllUsersRequest,
    GetAllUsersResponse,
)
from eros.api.account_pb2 import CustomClaims
from eros.event.account_pb2 import FollowUserEvent, UnfollowUserEvent
from weclikd.common.subscription_pb2 import Subscription
from weclikd.utils.session import Session
from weclikd.service.transaction import Transaction
from weclikd.service.sql import sql_instance as db, sql_instance_bk as db_bk
from eros.api.notification_pb2 import Notification, UserInviteAcceptedNotification
from eros.model.model_account import AccountModel
from eros.model.model_user import UserModel, ProfileModel
from eros.model import model_user, model_account
from eros import firebase
from eros.event import topic, account_pb2
import uuid
import base64
import hashlib
import re

username_check = re.compile("^[a-zA-Z0-9_]{3,15}$")


class Eros(ErosServicer):
    @staticmethod
    def check_username(username):
        return username_check.match(username)

    async def CreateAccount(
        self, request: CreateAccountRequest, context: ServicerContext
    ) -> CreateAccountResponse:
        logging.info(f"Create Account: {request}")

        await self.CheckUsername(
            request=CheckUsernameRequest(username=request.username), context=context
        )
        if context.grpc_code:
            return CreateAccountResponse()

        account = AccountModel.request_to_proto(request, id=context.current_user_id)
        async with Transaction(sql=True):
            await db.execute(AccountModel.insert_stmt(account, request.firebase_id))
            logging.info(f"Account ID: {context.current_user_id}")
            session = Session({"current_user_id": account.id})
            user_request = CreateUserRequest(username=request.username)
            user = UserModel.request_to_proto(user_request, id=context.current_user_id)
            await db.execute(UserModel.insert_stmt(user, session))
            account.my_users.append(user)

            await topic.create_user_topic.publish(
                event=account_pb2.CreateUserEvent(profile=user.profile), session=session
            )
            await topic.create_acccount_topic.publish(
                event=account_pb2.CreateAccountEvent(account=account), session=session
            )

        if request.inviter_username:
            inviter_user = await db.fetch_one(
                ProfileModel.select_many_stmt(
                    GetProfilesRequest(usernames=[request.inviter_username])
                )
            )
            if not inviter_user:
                return CreateAccountResponse(account=account)

            await model_account.add_notification(
                user_id=inviter_user.id,
                notification=Notification(
                    user_invite_accepted=UserInviteAcceptedNotification(
                        invitee_user_id=account.id
                    )
                ),
            )

        return CreateAccountResponse(account=account)

    async def ChangeAccessKey(
        self, request: ChangeAccessKeyRequest, context: ServicerContext
    ) -> ChangeAccessKeyResponse:
        logging.info(f"Change Access Key")
        random_bytes = uuid.uuid4().bytes
        access_key = f"wk_{base64.b64encode(random_bytes).decode()[:-2]}"
        hashed_access_key = hashlib.sha256(access_key.encode()).digest()
        await db.execute(
            AccountModel.update_access_key(context.account_id, hashed_access_key)
        )
        return ChangeAccessKeyResponse(access_key=access_key)

    async def GetAccountIdFromAccessKey(
        self, request: GetAccountIdFromAccessKeyRequest, context: ServicerContext
    ) -> GetAccountIdFromAccessKeyResponse:
        logging.info("Get Account Id From Access Key")
        hashed_access_key = hashlib.sha256(request.access_key.encode()).digest()
        await db_bk.attempt_to_reconnect()
        row = await db_bk.fetch_one(
            AccountModel.get_account_from_access_key(hashed_access_key)
        )
        if not row:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f'Access key "{request.access_key}" is invalid.')
            return GetAccountIdFromAccessKeyResponse()
        return GetAccountIdFromAccessKeyResponse(
            account_id=row.id,
            custom_claims=CustomClaims(
                admin=row.data.get("custom_claims", {}).get("admin")
            ),
        )

    async def CheckUsername(
        self, request: CheckUsernameRequest, context: ServicerContext
    ) -> CheckUsernameResponse:
        if not Eros.check_username(request.username):
            context.set_code(StatusCode.INVALID_ARGUMENT)
            context.set_details(f'Username "{request.username}" is invalid.')
            return CheckUsernameResponse()

        row = await db.fetch_one(UserModel.check_username(request.username))
        if not row:
            return CheckUsernameResponse()
        else:
            context.set_code(StatusCode.ALREADY_EXISTS)
            context.set_details(f'Username "{request.username}" is already taken.')
            return CheckUsernameResponse()

    async def GetAccount(
        self, request: GetAccountRequest, context: ServicerContext
    ) -> GetAccountResponse:
        logging.info(f"Get Account: {request}")

        row = await db.fetch_one(AccountModel.select_stmt(request))
        if not row:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f"Account {request.account_id} does not exist")
            return GetAccountResponse()

        account = AccountModel.row_to_proto(row)
        rows = await db.fetch_all(UserModel.select_many_stmt(account.id))
        account.my_users.extend([UserModel.row_to_proto(row) for row in rows])
        return GetAccountResponse(account=account)

    async def GetProfiles(
        self, request: GetProfilesRequest, context: ServicerContext
    ) -> GetProfilesResponse:
        logging.info(f"Get Profiles: {request}")

        rows = await db.fetch_all(ProfileModel.select_many_stmt(request))
        return GetProfilesResponse(
            profiles=[ProfileModel.row_to_proto(row) for row in rows]
        )

    async def GetIdFromUsername(
        self,
        request: GetIdFromUsernameRequest,
        context: ServicerContext,
    ) -> GetIdFromUsernameResponse:
        logging.info(f"GetIdFromUsername: {request}")

        rows = await db.fetch_all(ProfileModel.select_ids_stmt(request.usernames))
        username_to_id = {}
        for row in rows:
            username_to_id[row.username] = row.id

        return GetIdFromUsernameResponse(
            user_ids=[username_to_id.get(username, 0) for username in request.usernames]
        )

    async def EditProfile(
        self, request: EditProfileRequest, context: ServicerContext
    ) -> EditProfileResponse:
        logging.info(f"Edit Profile: {request}")

        session = Session.create_from_grpc(context)
        row = await db.fetch_one(
            ProfileModel.select_stmt(session.current_user_id, session)
        )
        profile = ProfileModel.row_to_proto(row)
        if request.username and row.username.lower() != request.username.lower():
            await self.CheckUsername(
                request=CheckUsernameRequest(username=request.username), context=context
            )
            if context.grpc_code:
                return EditProfileResponse()

        await db.execute(ProfileModel.update_stmt(profile, request, session=session))
        profile.id = context.current_user_id
        await topic.update_user_topic.publish(
            event=account_pb2.EditUserEvent(profile=profile), session=context
        )
        return EditProfileResponse(profile=profile)

    async def GetUsersFollowed(
        self, request: GetUsersFollowedRequest, context: ServicerContext
    ) -> GetUsersFollowedResponse:
        logging.info(f"Get Users Followed: {request}")
        return GetUsersFollowedResponse(
            followed_users=await model_user.retrieve_following(context.current_user_id)
        )

    async def GetTrendingUsers(
        self,
        request: GetTrendingUsersRequest,
        context: ServicerContext,
    ) -> GetTrendingUsersResponse:
        logging.info(f"Get Trending Users")

        rows = await db.fetch_all(
            ProfileModel.select_many_stmt(request, Session.create_from_grpc(context))
        )
        return GetTrendingUsersResponse(
            profiles=[ProfileModel.row_to_proto(row) for row in rows]
        )

    async def GetAllUsers(
        self, request: GetAllUsersRequest, context: ServicerContext
    ) -> GetAllUsersResponse:
        logging.info(f"Get All Users: {request}")
        users, cursor = await model_user.retrieve_all(request.filter)
        return GetAllUsersResponse(users=users, end_cursor=cursor)

    async def ChangeAccountSettings(
        self,
        request: ChangeAccountSettingsRequest,
        context: ServicerContext,
    ) -> ChangeAccountSettingsResponse:
        logging.info(f"ChangeAccountSettings: {request}")
        session = Session.create_from_grpc(context)
        row = await db.fetch_one(
            AccountModel.select_stmt(GetAccountRequest(account_id=session.account_id))
        )
        account = AccountModel.row_to_proto(row)
        account.settings.MergeFrom(request.settings)
        await db.execute(AccountModel.update_stmt(account))
        return ChangeAccountSettingsResponse()

    async def ChangeSubscription(
        self,
        request: ChangeSubscriptionRequest,
        context: ServicerContext,
    ) -> ChangeSubscriptionResponse:
        logging.info(f"ChangeSubscription: {request}")
        session = Session.create_from_grpc(context)
        row = await db.fetch_one(
            AccountModel.select_stmt(GetAccountRequest(account_id=session.account_id))
        )
        account = AccountModel.row_to_proto(row)
        account.settings.subscription = request.type

        row = await db.fetch_one(AccountModel.get_firebase_id(session.account_id))
        if not row:
            logging.error("Unable to get firebase id for account {account_id}")
            return

        if request.type == Subscription.SubscriptionType.BASIC:
            await firebase.modify_claim(row.firebase_id, "subscription", False)
        else:
            await firebase.modify_claim(row.firebase_id, "subscription", True)

        await db.execute(AccountModel.update_stmt(account))
        return ChangeSubscriptionResponse()

    async def FollowUser(
        self, request: FollowUserRequest, context: ServicerContext
    ) -> FollowUserResponse:
        logging.info(f"Processing FollowUser: {request}")
        success = await model_user.follow(
            user_id=context.current_user_id,
            followed_user_id=request.user_id,
            settings=request.settings,
        )
        if success:
            await topic.follow_user_topic.publish(
                event=FollowUserEvent(
                    user_id=request.user_id, settings=request.settings
                ),
                session=context,
            )
        return FollowUserResponse()

    async def UnfollowUser(
        self, request: UnfollowUserRequest, context: ServicerContext
    ) -> UnfollowUserResponse:
        logging.info(f"Processing UnfollowUser: {request}")
        success = await model_user.unfollow(
            user_id=context.current_user_id, followed_user_id=request.user_id
        )
        if success:
            await topic.unfollow_user_topic.publish(
                event=UnfollowUserEvent(user_id=request.user_id), session=context
            )
        return UnfollowUserResponse()

    async def GetNotifications(
        self, request: GetNotificationsRequest, context: ServicerContext
    ) -> GetNotificationsResponse:
        logging.info(f"Processing GetNotifications: {request}")
        notifications, cursor = await model_account.get_notifications(
            user_id=context.current_user_id, filter=request.filter
        )
        return GetNotificationsResponse(
            notifications=notifications,
            end_cursor=cursor,
        )

    async def GetNumUnreadNotifications(
        self, request: GetNumUnreadNotificationsRequest, context: ServicerContext
    ) -> GetNumUnreadNotificationsResponse:
        logging.info(f"Processing GetNumUnreadNotifications: {request}")
        num_unread_notifications = await model_account.get_num_unread_notifications(
            user_id=context.current_user_id
        )
        return GetNumUnreadNotificationsResponse(
            num_unread_notifications=num_unread_notifications
        )

    async def MarkNotificationsAsRead(
        self, request: MarkNotificationsAsReadRequest, context: ServicerContext
    ) -> MarkNotificationsAsReadResponse:
        logging.info(f"Processing MarkNotificationsAsRead")
        await model_account.mark_notifications_as_read(context.current_user_id)
        return MarkNotificationsAsReadResponse()

    async def DeleteNotification(
        self, request: DeleteNotificationRequest, context: ServicerContext
    ) -> DeleteNotificationRequest:
        logging.info(f"DeleteNotification")
        notification = await model_account.get_notification(
            user_id=context.current_user_id, notification_id=request.notification_id
        )
        if not notification:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f"Notification {request.id} not found")
            return DeleteNotificationResponse()
        await model_account.delete_notification(
            user_id=context.current_user_id, notification_id=request.notification_id
        )
        return DeleteNotificationResponse(notification=notification)
