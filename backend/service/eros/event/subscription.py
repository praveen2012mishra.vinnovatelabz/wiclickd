from weclikd.service import pubsub
import athena.event.topic as athena_topic
import atlas.event.topic as atlas_topic
import hestia.event.topic as hestia_topic

delete_topic_subscription = pubsub.Subscription(
    athena_topic.delete_topic_topic, component_name="eros"
)

create_comment_subscription = pubsub.Subscription(
    atlas_topic.create_comment_topic, component_name="eros"
)

create_clik_member_subscription = pubsub.Subscription(
    hestia_topic.create_clik_member_topic, component_name="eros"
)

invite_clik_subscription = pubsub.Subscription(
    hestia_topic.invite_clik_topic, component_name="eros"
)

join_clik_subscription = pubsub.Subscription(
    hestia_topic.join_clik_topic, component_name="eros"
)
