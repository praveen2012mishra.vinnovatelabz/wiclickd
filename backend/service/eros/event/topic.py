from weclikd.service import pubsub
from eros.event import account_pb2

create_acccount_topic = pubsub.Topic("account-create", account_pb2.CreateAccountEvent)
delete_acccount_topic = pubsub.Topic("account-delete", account_pb2.DeleteAccountEvent)

create_user_topic = pubsub.Topic("user-create", account_pb2.CreateUserEvent)
update_user_topic = pubsub.Topic("user-update", account_pb2.EditUserEvent)
delete_user_topic = pubsub.Topic("user-delete", account_pb2.DeleteAccountEvent)
follow_user_topic = pubsub.Topic("user-follow", account_pb2.FollowUserEvent)
unfollow_user_topic = pubsub.Topic("user-unfollow", account_pb2.UnfollowUserEvent)
