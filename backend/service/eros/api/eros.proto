syntax = "proto3";

package weclikd;

import "eros/api/account.proto";
import "eros/api/user.proto";
import "eros/api/profile.proto";
import "eros/api/notification.proto";
import "eros/api/accomplishment.proto";
import "weclikd/common/follow.proto";
import "weclikd/common/subscription.proto";
import "weclikd/common/filter.proto";
import "google/protobuf/timestamp.proto";

service Eros {
  rpc CreateAccount (CreateAccountRequest) returns (CreateAccountResponse) {}
  rpc CreateUser (CreateUserRequest) returns (CreateUserResponse) {}
  rpc ChangeAccessKey (ChangeAccessKeyRequest) returns (ChangeAccessKeyResponse) {}
  rpc CheckUsername (CheckUsernameRequest) returns (CheckUsernameResponse) {}

  rpc GetAccount (GetAccountRequest) returns (GetAccountResponse) {}
  rpc GetAccountIdFromAccessKey (GetAccountIdFromAccessKeyRequest) returns (GetAccountIdFromAccessKeyResponse) {}
  rpc GetProfiles (GetProfilesRequest) returns (GetProfilesResponse) {}
  rpc GetIdFromUsername (GetIdFromUsernameRequest) returns (GetIdFromUsernameResponse) {}

  rpc EditProfile (EditProfileRequest) returns (EditProfileResponse) {}
  rpc ChangeAccountSettings (ChangeAccountSettingsRequest) returns (ChangeAccountSettingsResponse) {}
  rpc ChangeSubscription (ChangeSubscriptionRequest) returns (ChangeSubscriptionResponse) {}

  rpc GetTrendingUsers (GetTrendingUsersRequest) returns (GetTrendingUsersResponse) {}
  rpc GetAllUsers (GetAllUsersRequest) returns (GetAllUsersResponse) {}

  rpc FollowUser (FollowUserRequest) returns (FollowUserResponse) {}
  rpc UnfollowUser (UnfollowUserRequest) returns (UnfollowUserResponse) {}
  rpc GetUsersFollowed (GetUsersFollowedRequest) returns (GetUsersFollowedResponse) {}

  rpc GetNotifications (GetNotificationsRequest) returns (GetNotificationsResponse) {}
  rpc DeleteNotification (DeleteNotificationRequest) returns (DeleteNotificationResponse) {}
  rpc GetNumUnreadNotifications (GetNumUnreadNotificationsRequest) returns (GetNumUnreadNotificationsResponse) {}
  rpc MarkNotificationsAsRead (MarkNotificationsAsReadRequest) returns (MarkNotificationsAsReadResponse) {}
}

// ChangeAccountSettings
message ChangeAccountSettingsRequest {
  AccountSettings settings = 1;
}
message ChangeAccountSettingsResponse {
}

// ChangeSubscription
message ChangeSubscriptionRequest {
  Subscription.SubscriptionType type = 1;
}
message ChangeSubscriptionResponse {
}

// ChangeAccessKey
message ChangeAccessKeyRequest {}
message ChangeAccessKeyResponse {
  string access_key = 1;
}

// CheckUsername
message CheckUsernameRequest {
  string username = 1;
}
message CheckUsernameResponse {
}

// GetAccountIdFromAccessKey
message GetAccountIdFromAccessKeyRequest {
  string access_key = 1;
}
message GetAccountIdFromAccessKeyResponse {
  uint64 account_id = 1;
  CustomClaims custom_claims = 2;
}

// CreateAccount
message CreateAccountRequest {
  string email = 1;
  string first_name = 2;
  string last_name = 3;
  string username = 4;
  string firebase_id = 5;
  string inviter_username = 6;
}
message CreateAccountResponse {
  Account account = 1;
}

// CreateUser
message CreateUserRequest {
  string username = 1;
  string full_name = 2;
  uint64 banner_picture_id = 3;
  uint64 profile_picture_id = 4;
  string description = 5;
}
message CreateUserResponse {
  User user = 1;
}

// EditProfile
message EditProfileRequest {
  string username = 1;
  string full_name = 2;
  Profile.ProfileType type = 3;
  uint64 banner_picture_id = 4;
  uint64 profile_picture_id = 5;
  string description = 6;
}
message EditProfileResponse {
  Profile profile = 1;
}

// GetAccount
message GetAccountRequest {
  oneof oneof_id {
    uint64 external_account_id = 1;
    uint64 account_id = 2;
  }
}
message GetAccountResponse {
    Account account = 1;
}

// GetProfiles
message GetProfilesRequest {
  repeated uint64 user_ids = 1;
  repeated string usernames = 2;
}
message GetProfilesResponse {
  // Profiles using user_ids are populated first
  // Profiles using usernames populated after
  repeated Profile profiles = 1;
}

// GetIdFromUsername
message GetIdFromUsernameRequest {
  repeated string usernames = 1;
}
message GetIdFromUsernameResponse {
  repeated uint64 user_ids = 2;
}


// GetTrendingUsers
message GetTrendingUsersRequest {
}
message GetTrendingUsersResponse {
  repeated Profile profiles = 1;
}

// FollowUser
message FollowUserRequest {
  uint64 user_id = 1;
  FollowSettings settings = 2;
}
message FollowUserResponse {
}

// UnfollowUser
message UnfollowUserRequest {
  uint64 user_id = 1;
}
message UnfollowUserResponse {
}

// GetUsersFollowed
message FollowedUser {
  Profile user = 1;
  FollowSettings settings = 2;
  google.protobuf.Timestamp created = 3;
}
message GetUsersFollowedRequest {
}
message GetUsersFollowedResponse {
  repeated FollowedUser followed_users = 1;
}


// GetNotification
message GetNotificationsRequest {
  DefaultFilter filter = 1;
}
message GetNotificationsResponse {
  repeated Notification notifications = 1;
  string end_cursor = 2;
}

// DeleteNotification
message DeleteNotificationRequest {
  uint64 notification_id = 1;
}
message DeleteNotificationResponse {
  Notification notification = 1;
}

// GetNumUnreadNotifications
message GetNumUnreadNotificationsRequest {
}
message GetNumUnreadNotificationsResponse {
  uint32 num_unread_notifications = 1;
}

// MarkNotificationsAsRead
message MarkNotificationsAsReadRequest {
}
message MarkNotificationsAsReadResponse {
}

// GetAllUsers
message GetAllUsersRequest {
  DefaultFilter filter = 1;
}
message GetAllUsersResponse {
  repeated Profile users = 1;
  string end_cursor = 2;
}
