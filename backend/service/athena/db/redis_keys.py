def profile(topic_id):
    return f"T{topic_id}-profile"


def followers(topic_id):
    return f"T{topic_id}-followers"


def alias(name):
    return f"T-{name}"
