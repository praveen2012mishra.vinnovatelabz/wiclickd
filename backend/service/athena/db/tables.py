from sqlalchemy import (
    Table,
    Column,
    Integer,
    JSON,
    VARCHAR,
    text as sqltext,
)
from sqlalchemy.dialects.mysql import TIMESTAMP
from weclikd.service.sql import UnsignedBigInt, metadata

TopicTable = Table(
    "topic",
    metadata,
    Column("id", UnsignedBigInt, primary_key=True),
    Column("parent_topic_id", UnsignedBigInt),
    Column("followers", Integer, nullable=False),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)

TopicAliasTable = Table(
    "topic_alias",
    metadata,
    Column("name", VARCHAR(32), primary_key=True),
    Column("topic_id", UnsignedBigInt),
)


FollowTopicTable = Table(
    "topic_follow",
    metadata,
    Column("user_id", UnsignedBigInt, primary_key=True),
    Column("topic_id", UnsignedBigInt, primary_key=True),
    Column("data", JSON, nullable=False),
    Column(
        "created",
        TIMESTAMP,
        nullable=False,
        server_default=sqltext("CURRENT_TIMESTAMP"),
    ),
)
