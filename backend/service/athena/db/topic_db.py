from weclikd.service.sql import SQLDb
from weclikd.service.transaction import Transaction
from weclikd.service import redis
from weclikd.common.filter_pb2 import DefaultFilter
from athena.api.athena_pb2 import FollowedTopic
from athena.api.topic_pb2 import Topic, TopicFamily
from athena.db.tables import TopicTable, FollowTopicTable, TopicAliasTable
from athena.db import redis_keys
from google.protobuf.message import Message
import sqlalchemy
import base64
import logging
from typing import Optional, List, Dict, Tuple
from aioredis import Redis


class TopicDb(SQLDb):
    def __init__(self):
        super().__init__(TopicTable, proto_clazz=Topic, order_by=TopicTable.c.id)

    async def get(self, id: int, check_cache=True) -> Topic:
        if check_cache:
            redis_cli = await redis.get()
            redis_pipeline: Redis = redis_cli.pipeline()
            redis_pipeline.get(redis_keys.profile(id))
            redis_pipeline.get(redis_keys.followers(id))
            topic_unserialized, followers = await redis_pipeline.execute()
            if topic_unserialized:
                topic = Topic()
                topic.ParseFromString(base64.b64decode(topic_unserialized.encode()))
                topic.followers = int(followers) if followers else 0
                return topic

        async with Transaction(sql=True):
            topic = await super().get(id=id)
            if not topic:
                return Topic()

            parent_rows = await self.sql_instance.db.fetch_all(
                sqlalchemy.sql.text(  # https://stackoverflow.com/questions/12948009/finding-all-parents-in-mysql-table-with-single-query-recursive-query
                    f"""
                    SELECT T2.id, T2.data
                    FROM (
                        SELECT
                            @r AS _id,
                            (SELECT @r := parent_topic_id FROM topic WHERE id = _id) AS parent_topic_id,
                            @l := @l + 1 AS lvl
                        FROM
                            (SELECT @r := {topic.id}, @l := 0) vars,
                            topic m
                        WHERE @r <> 0) T1
                    JOIN topic T2
                    ON T1._id = T2.id
                    ORDER BY T1.lvl DESC;
                    """
                )
            )
            if parent_rows:
                parent_rows = parent_rows[:-1]
            parents = [self.to_proto(row) for row in parent_rows]
            child_alias = TopicTable.alias("child_alias")
            children_rows = await self.sql_instance.db.fetch_all(
                sqlalchemy.select(
                    [child_alias],
                    from_obj=TopicTable.join(
                        child_alias, TopicTable.c.id == child_alias.c.parent_topic_id
                    ),
                    whereclause=child_alias.c.parent_topic_id == topic.id,
                )
            )
            children = [self.to_proto(row) for row in children_rows]
            aliases = await topic_aliases_db.query(topic_id=topic.id)

        topic.parents.extend([each.name for each in parents])
        topic.parent_topic_id = parents[-1].id if parents else 0
        topic.parent_topic_ids.extend([each.id for each in parents])
        topic.children.extend([each.name for each in children])
        topic.aliases.extend([each.name for each in aliases if each.name != topic.name])
        await self.cache([topic])
        return topic

    async def get_many(self, ids: List[int]) -> List[Topic]:
        if not ids:
            return []

        redis_cli = await redis.get()
        all_keys = []
        for each in ids:
            all_keys.append(redis_keys.profile(each))
            all_keys.append(redis_keys.followers(each))
        topic_values = await redis_cli.mget(*all_keys)
        topics = []
        for i, (topic_unserialized, followers) in enumerate(
            zip(topic_values[0::2], topic_values[1::2])
        ):
            if not topic_unserialized:
                topics.append(await self.get(ids[i]))
                continue
            topic = Topic()
            topic.ParseFromString(base64.b64decode(topic_unserialized.encode()))
            topic.followers = int(followers) if followers else 0
            topics.append(topic)
        return topics

    async def get_many_by_name(self, values: List[str]) -> List[Topic]:
        await self.sql_instance.attempt_to_reconnect()
        if not values:
            return []

        redis_cli = await redis.get()
        topic_values = await redis_cli.eval(
            script=(
                "local topic_ids = redis.call('MGET', unpack(KEYS));\n"
                "local r = {};\n"
                "for _, v in pairs(topic_ids) do\n"
                "    if not v then\n"
                "        r[#r+1] = false;\n"
                "        r[#r+1] = false;\n"
                "    else\n"
                "        r[#r+1] = redis.call('GET', 'T' .. v .. '-profile');\n"
                "        r[#r+1] = redis.call('GET', 'T' .. v .. '-followers');\n"
                "    end\n"
                "end\n"
                "return r;"
            ),
            keys=[redis_keys.alias(name.lower()) for name in values],
            args=[],
        )
        missing_indexes = dict()
        topics = []
        for i, (topic_unserialized, followers) in enumerate(
            zip(topic_values[0::2], topic_values[1::2])
        ):
            if not topic_unserialized:
                missing_indexes[values[i].lower()] = i
                topics.append(Topic())
                continue
            topic = Topic()
            topic.ParseFromString(base64.b64decode(topic_unserialized.encode()))
            topic.followers = int(followers) if followers else 0
            topics.append(topic)
        logging.info(f"Topics Cache Miss Rate: {len(missing_indexes)}/{len(values)}")
        if missing_indexes:
            rows = await self.sql_instance.db.fetch_all(
                sqlalchemy.select(
                    [TopicAliasTable],
                    whereclause=TopicAliasTable.c.name.in_(
                        [name for name in missing_indexes]
                    ),
                )
            )
            for row in rows:
                index = missing_indexes[row.name.lower()]
                topics[index] = await self.get(row.topic_id)
        return topics

    async def insert(self, message: Message = None, **kwargs) -> int:
        async with Transaction(sql=True):
            topic_names = list(message.aliases) + [message.name]
            await super().insert(message, **kwargs)
            await self.sql_instance.db.execute_many(
                sqlalchemy.insert(TopicAliasTable),
                values=[{"topic_id": message.id, "name": each} for each in topic_names],
            )
        await self.cache([message])

    async def update(
        self,
        message: Message = None,
        new_names: List[str] = None,
        deleted_names: List[str] = None,
    ):
        redis_pipeline = await redis.get_pipeline()

        async with Transaction(sql=True):
            await super().update(message, id=message.id)

            if new_names:
                await self.sql_instance.db.execute_many(
                    sqlalchemy.insert(TopicAliasTable),
                    values=[
                        {"topic_id": message.id, "name": each} for each in new_names
                    ],
                )
                for each in new_names:
                    redis_pipeline.set(redis_keys.alias(each), message.id)

            if deleted_names:
                await self.sql_instance.db.execute(
                    sqlalchemy.delete(
                        TopicAliasTable,
                        whereclause=TopicAliasTable.c.name.in_(deleted_names),
                    )
                )
                for each in deleted_names:
                    redis_pipeline.delete(redis_keys.alias(each))
                await redis_pipeline.execute()

            await self.cache([message])

    async def increment_followers(self, topic_id: int):
        await super().increment("followers", id=topic_id)
        redis_cli = await redis.get()
        redis_cli.incr(redis_keys.followers(topic_id))

    async def decrement_followers(self, topic_id: int):
        await super().decrement("followers", id=topic_id)
        redis_cli = await redis.get()
        redis_cli.decr(redis_keys.followers(topic_id))

    async def invalidate_cache(self, topics: List[Topic]):
        redis_pipeline: Redis = await redis.get_pipeline()
        for topic in topics:
            redis_pipeline.delete(redis_keys.profile(topic.id))
            redis_pipeline.delete(redis_keys.followers(topic.id))
            for alias in list(topic.aliases) + [topic.name]:
                redis_pipeline.delete(redis_keys.alias(alias))
        await redis_pipeline.execute()

    async def cache(self, topics: List[Topic]) -> Dict:
        redis_pipeline: Redis = await redis.get_pipeline()
        for topic in topics:
            redis_pipeline.set(
                redis_keys.profile(topic.id),
                base64.b64encode(topic.SerializeToString()),
            )
            redis_pipeline.set(redis_keys.followers(topic.id), topic.followers)
            for alias in list(topic.aliases) + [topic.name]:
                redis_pipeline.set(redis_keys.alias(alias), topic.id)
        await redis_pipeline.execute()

    async def get_trending(self, filter: DefaultFilter) -> Tuple[List[Topic], str]:
        decoded_cursor = (
            base64.b64decode(filter.cursor).decode() if filter.cursor else None
        )
        num_followers, last_id = (
            decoded_cursor.split(",") if decoded_cursor else ("99999999999", 0)
        )

        rows = await self.sql_instance.db.fetch_all(
            sqlalchemy.select(
                [TopicTable],
                whereclause=sqlalchemy.or_(
                    TopicTable.c.followers < num_followers,
                    sqlalchemy.and_(
                        TopicTable.c.followers == num_followers,
                        TopicTable.c.id > last_id,
                    ),
                ),
                limit=filter.num_items,
            )
            .order_by(TopicTable.c.followers.desc())
            .order_by(TopicTable.c.id.asc())
        )
        cursor = (
            base64.b64encode(f"{rows[-1].followers},{rows[-1].id}".encode()).decode()
            if rows
            else ""
        )
        return [self.to_proto(row) for row in rows], cursor

    def to_row(self, msg: Message) -> Dict:
        row = super().to_row(msg)
        # by default, protobuf sets the parent_topic_id field to 0
        # this is an issue since there's a constraint on that field, and there is no topic with an id of 0
        if not row.get("parent_topic_id") or row.get("parent_topic_id") == "0":
            row["parent_topic_id"] = None
        return row


class FollowTopicSQLDb(SQLDb):
    def __init__(self):
        super().__init__(FollowTopicTable, proto_clazz=FollowedTopic)

    def to_proto(self, row, msg: Optional[Message] = None) -> FollowedTopic:
        proto: FollowedTopic = super().to_proto(row, msg)
        proto.topic.id = row.topic_id
        proto.settings.follow_type = row.data["follow_type"]
        return proto


topic_follow_db = FollowTopicSQLDb()
topic_db = TopicDb()
topic_aliases_db = SQLDb(TopicAliasTable, proto_clazz=TopicFamily)
