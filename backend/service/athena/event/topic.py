from weclikd.service import pubsub
from athena.event import topic_pb2

create_topic_topic = pubsub.Topic("topic-create", topic_pb2.CreateTopicEvent)
update_topic_topic = pubsub.Topic("topic-update", topic_pb2.EditTopicEvent)
delete_topic_topic = pubsub.Topic("topic-delete", topic_pb2.DeleteTopicEvent)
follow_topic_topic = pubsub.Topic("topic-follow", topic_pb2)
unfollow_topic_topic = pubsub.Topic("topic-unfollow")
