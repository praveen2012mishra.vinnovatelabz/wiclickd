# Athena Design Doc

## Overview and Context

Athena, the goddess of knowledge, is the microservice responsible for topics.

Topics have a tree-like hierarchy with root topics that correspond to major fields of study such as Physics.
There are sub-topics which are children of other topics. For now, a topic can only have 0 or 1 parent.

## Goals

1) Topics can be retrieved efficiently, including its parents and children. 
2) Topics can be updated, including setting a new parent for the topic.
3) Efficiently retrieve the most popular topics.
4) Can ban specific topics to prevent pornography and other unwanted content

## Proposed Solution

## Interface

###gRPC (Synchronous)
* CRUD Topic
* ValidateTopics
* GetTrendingTopics 
* GetChildTopics
* ProposeTopic
* AcceptTopic
* BanTopic

###Published Events (Asynchronous)
* CUD Topic

###Subscribed Events
* Follow/Unfollow Topic to update # of followers

## Storage

Currently uses MySQL but Datastore is also a good choice. This database will be extremely read heavy
so caching is important.


### Tables

topic - stores all topics and its information, including its ancestors (root topic to this topic)

topic_relationship - relationship between child topics and parent topics

new_topic - new topics that are requested by users

banned_topic - topics that have been banned by Weclikd admins


### Caching

#### topics
Topics will be stored using the hash data type. This is so we can quickly update the follower count.
This will not take up a lot of space compared to other tables like posts.
It also does not have very many fields so retrieving all keys isn't a problem. Expiration: none

T<topic-id> hash: name, description, parents, num_followers, children


## Testability

## Monitoring and Alerting

