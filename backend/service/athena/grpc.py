import logging
import re
from typing import Optional, List
from grpc import ServicerContext, StatusCode
from athena.api.athena_pb2_grpc import AthenaServicer
from athena.api.athena_pb2 import (
    GetTopicsRequest,
    GetTopicsResponse,
    EditTopicRequest,
    EditTopicResponse,
    ProposeTopicRequest,
    ProposeTopicResponse,
    DeleteTopicRequest,
    DeleteTopicResponse,
    FollowTopicRequest,
    FollowRootTopicsRequest,
    FollowRootTopicsResponse,
    FollowTopicResponse,
    UnfollowTopicRequest,
    UnfollowTopicResponse,
    GetTopicsFollowedRequest,
    GetTopicsFollowedResponse,
    GetAllTopicsRequest,
    GetAllTopicsResponse,
)
from athena.model import topic_model
from athena.event import topic as pubsub_topic
from athena.event.topic_pb2 import FollowTopicEvent, UnfollowTopicEvent
from athena.api.topic_pb2 import Topic
from athena.event.topic_pb2 import CreateTopicEvent, EditTopicEvent, DeleteTopicEvent
from weclikd.service.transaction import Transaction
from weclikd.common.follow_pb2 import FollowSettings
from weclikd.utils.session import Session
from weclikd.utils import weclikd_id


topic_name_regex = re.compile("^[0-9a-zA-Z][0-9a-zA-Z\-]{0,30}[0-9a-zA-Z\-]$")


class Athena(AthenaServicer):
    async def GetTopics(
        self, request: GetTopicsRequest, context: ServicerContext
    ) -> GetTopicsResponse:
        logging.info(f"GetTopics: {request}")
        topics = await topic_model.retrieve_many(
            topic_ids=request.topic_ids,
            topic_names=request.topic_names,
            do_retrieve_parents=request.retrieve_parents,
        )
        return GetTopicsResponse(topics=topics)

    async def GetAllTopics(
        self, request: GetAllTopicsRequest, context: ServicerContext
    ) -> GetAllTopicsResponse:
        logging.info(f"Get All Topics: {request}")
        topics, cursor = await topic_model.retrieve_all(request.filter)
        return GetAllTopicsResponse(topics=topics, end_cursor=cursor)

    async def DeleteTopic(
        self, request: DeleteTopicRequest, context: ServicerContext
    ) -> DeleteTopicResponse:
        logging.info(f"DeleteTopic: {request}")

        async with Transaction(sql=True):
            # check topic exists
            topic = await topic_model.retrieve(topic_id=request.topic_id)
            if not topic:
                context.set_code(StatusCode.INVALID_ARGUMENT)
                context.set_details(f'Topic "{request.topic_id}" does not exist.')
                return DeleteTopicResponse()

            # check topic does not have children since that will orphan them
            if topic.children:
                context.set_code(StatusCode.INVALID_ARGUMENT)
                context.set_details(
                    f'Topic "{topic.name}" has children. Modify children before deleting this topic.'
                )
                return DeleteTopicResponse()

            # delete the topic
            await topic_model.delete(topic)

        # publish deletion event
        await pubsub_topic.delete_topic_topic.publish(
            event=DeleteTopicEvent(topic_id=request.topic_id), session=context
        )
        return DeleteTopicResponse()

    async def ProposeTopic(
        self, request: ProposeTopicRequest, context: ServicerContext
    ) -> ProposeTopicResponse:
        logging.info(f"ProposeTopic: {request}")

        # create new topic
        new_topic = Topic(
            id=weclikd_id.string_to_id(request.name),
            name=request.name,
            description=request.description or f"Anything about {request.name}",
            banner_picture_id=request.banner_picture_id or 4619524780044901376,
            aliases=request.aliases,
            keywords=request.keywords,
        )

        async with Transaction(sql=True):
            # check that new topic is valid
            error = await check_valid_topic(new_topic)
            if error:
                context.set_code(StatusCode.INVALID_ARGUMENT)
                context.set_details(error)
                return ProposeTopicResponse()

            # check for duplicate topics
            topic_names = list(new_topic.aliases) + [new_topic.name]
            error = await check_duplicate_topics(topic_names)
            if error:
                context.set_code(StatusCode.ALREADY_EXISTS)
                context.set_details(error)
                return ProposeTopicResponse()

            # check parent topic exists
            parent_topic = await topic_model.retrieve_by_name(request.parent)
            if request.parent and not parent_topic:
                context.set_code(StatusCode.NOT_FOUND)
                context.set_details(f'Parent topic "{request.parent}" does not exist.')
                return ProposeTopicResponse()

            # check maximum depth
            if parent_topic and len(parent_topic.parents) >= 4:
                context.set_code(StatusCode.INVALID_ARGUMENT)
                context.set_details(
                    f'Parent topic "{request.parent}" cannot have children.'
                )
                return ProposeTopicResponse()

            # create the topic
            await topic_model.create(topic=new_topic, parent_topic=parent_topic)

        # publish creation event
        await pubsub_topic.create_topic_topic.publish(
            event=CreateTopicEvent(topic=new_topic),
            session=Session.create_from_grpc(context),
        )
        return ProposeTopicResponse(topic=new_topic)

    async def EditTopic(
        self, request: EditTopicRequest, context: ServicerContext
    ) -> EditTopicResponse:
        logging.info(f"EditTopic: {request}")

        # create new topic
        new_topic = Topic(
            name=request.name.lower(),
            description=request.description,
            banner_picture_id=request.banner_picture_id,
            aliases=[each.lower() for each in request.aliases],
            keywords=request.keywords,
        )
        async with Transaction(sql=True):
            # check that the topic exists
            old_topic = await topic_model.retrieve(topic_id=request.topic_id)
            if not old_topic:
                context.set_code(StatusCode.NOT_FOUND)
                context.set_details(f"Topic {request.topic_id} does not exist")
                return EditTopicResponse()

            # check that new topic is valid
            new_topic.name = new_topic.name if new_topic.name else old_topic.name
            error = await check_valid_topic(new_topic)
            if error:
                context.set_code(StatusCode.INVALID_ARGUMENT)
                context.set_details(error)
                return EditTopicResponse()

            # figure out which names are new, which ones are old and should be deleted
            new_names = set(new_topic.aliases) - set(old_topic.aliases)
            old_names = (
                set(old_topic.aliases) - set(new_topic.aliases)
                if new_topic.aliases
                else set()
            )
            if new_topic.name and old_topic.name != new_topic.name:
                new_names.add(new_topic.name)
                old_names.add(old_topic.name)
                if new_topic.name in old_topic.aliases:
                    new_names.remove(new_topic.name)
                if old_topic.name in new_topic.aliases:
                    old_names.remove(old_topic.name)

            # check that the new names(aliases) are unique
            error = await check_duplicate_topics(new_names)
            if error:
                context.set_code(StatusCode.ALREADY_EXISTS)
                context.set_details(error)
                return EditTopicResponse()

            # check parent topic exists
            parent_topic = await topic_model.retrieve_by_name(request.parent)
            if request.parent and request.parent != "NONE" and not parent_topic:
                context.set_code(StatusCode.NOT_FOUND)
                context.set_details(f'Parent topic "{request.parent}" does not exist.')
                return ProposeTopicResponse()

            # set new topic's parent id
            if request.parent:
                if request.parent == "NONE":
                    new_topic.parent_topic_id = 0
                else:
                    new_topic.parent_topic_id = parent_topic.id
            else:
                new_topic.parent_topic_id = old_topic.parent_topic_id

            # check maximum depth
            if parent_topic and len(parent_topic.parents) >= 4:
                context.set_code(StatusCode.INVALID_ARGUMENT)
                context.set_details(
                    f'Parent topic "{request.parent}" cannot have children.'
                )
                return ProposeTopicResponse()

            # update the topic
            merged_topic = await topic_model.update(
                old_topic=old_topic,
                new_topic=new_topic,
                new_parent_topic=parent_topic,
                new_names=new_names,
                old_names=old_names,
            )

        # publish update event
        await pubsub_topic.update_topic_topic.publish(
            event=EditTopicEvent(topic=merged_topic),
            session=Session.create_from_grpc(context),
        )
        return EditTopicResponse(topic=merged_topic)

    async def FollowRootTopics(
        self, request: FollowRootTopicsRequest, context: ServicerContext
    ) -> FollowRootTopicsResponse:
        logging.info(f"FollowRootTopics: {request}")
        topics = await topic_model.retrieve_many(
            topic_ids=[],
            topic_names=["technology", "weclikd", "science", "humanities", "business"],
            do_retrieve_parents=False,
        )
        for topic in topics:
            if not topic.id:
                continue
            success = await topic_model.follow(
                user_id=context.current_user_id,
                topic_id=topic.id,
                settings=FollowSettings(follow_type=FollowSettings.FAVORITE),
            )
            if success:
                await pubsub_topic.follow_topic_topic.publish(
                    event=FollowTopicEvent(
                        topic_id=topic.id,
                        settings=FollowSettings(follow_type=FollowSettings.FAVORITE),
                        followers=topic.followers + 1,
                    ),
                    session=context,
                )
        return FollowRootTopicsResponse()

    async def FollowTopic(
        self, request: FollowTopicRequest, context: ServicerContext
    ) -> FollowTopicResponse:
        logging.info(f"FollowTopic: {request}")
        topic = await topic_model.retrieve(request.topic_id)
        if not topic:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f'Topic "{request.topic_id}" does not exist.')
            return FollowTopicResponse()

        success = await topic_model.follow(
            user_id=context.current_user_id,
            topic_id=request.topic_id,
            settings=request.settings,
        )
        if success:
            await pubsub_topic.follow_topic_topic.publish(
                event=FollowTopicEvent(
                    topic_id=request.topic_id,
                    settings=request.settings,
                    followers=topic.followers + 1,
                ),
                session=context,
            )
        return FollowTopicResponse()

    async def UnfollowTopic(
        self, request: UnfollowTopicRequest, context: ServicerContext
    ) -> UnfollowTopicResponse:
        logging.info(f"UnfollowTopic: {request}")

        topic = await topic_model.retrieve(request.topic_id)
        if not topic:
            context.set_code(StatusCode.NOT_FOUND)
            context.set_details(f'Topic "{request.topic_id}" does not exist.')
            return UnfollowTopicResponse()

        success = await topic_model.unfollow(
            user_id=context.current_user_id,
            topic_id=request.topic_id,
        )
        if success:
            await pubsub_topic.unfollow_topic_topic.publish(
                event=UnfollowTopicEvent(
                    topic_id=request.topic_id,
                    followers=topic.followers - 1 if topic.followers else 0,
                ),
                session=context,
            )
        return UnfollowTopicResponse()

    async def GetTopicsFollowed(
        self, request: GetTopicsFollowedRequest, context: ServicerContext
    ) -> GetTopicsFollowedResponse:
        logging.info(f"Get Topics Followed: {request}")

        return GetTopicsFollowedResponse(
            followed_topics=await topic_model.retrieve_following(
                context.current_user_id
            )
        )


async def check_valid_topic(topic: Topic) -> Optional[str]:
    # Check name matches regex
    names_to_check = list(topic.aliases) + [topic.name]
    for each in names_to_check:
        if not topic_name_regex.match(each):
            return f'Topic "{each}" is invalid. Must be 2-32 characters, alphanumeric, and words separate with hyphens.'

        if each.isnumeric():
            return f'Topic "{each}" is invalid. The topic name cannot be an integer.'

    # Check keywords
    for each in topic.keywords:
        if len(each.name) < 2:
            return f'Topic keyword "{each.name}" is invalid. Must be at least 2 characters.'


async def check_duplicate_topics(topic_names: List[str]) -> Optional[str]:
    # Check name does not exist already
    topics = await topic_model.retrieve_many(topic_names=list(topic_names))
    for topic_name, topic in zip(topic_names, topics):
        if topic.id:
            return f"Topic {topic_name} already exists"

    # Check topic is not banned
    # TODO
