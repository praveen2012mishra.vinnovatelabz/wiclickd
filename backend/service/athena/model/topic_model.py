from typing import List, Tuple, Optional
from athena.api.athena_pb2 import FollowedTopic
from athena.api.topic_pb2 import Topic
from athena.db.topic_db import topic_db, topic_follow_db
from weclikd.common.follow_pb2 import FollowSettings
from weclikd.common.filter_pb2 import DefaultFilter
from weclikd.service.transaction import Transaction
import logging


async def create(topic: Topic, parent_topic: Topic):
    logging.info(f"Adding New Topic: {topic}")
    _normalize_topic(topic)
    _link_topics(child_topic=topic, parent_topic=parent_topic)
    await topic_db.insert(topic)
    if parent_topic:
        await topic_db.update(parent_topic)


async def retrieve(topic_id: int) -> Topic:
    return await topic_db.get(id=topic_id)


async def retrieve_by_name(topic_name: str) -> Topic:
    if topic_name == "NONE" or not topic_name:
        return None

    topics = await retrieve_many(topic_names=[topic_name])
    if not topics or not topics[0].id:
        return None
    return topics[0]


async def retrieve_many(
    topic_ids: List[int] = None,
    topic_names: List[str] = None,
    do_retrieve_parents: bool = False,
) -> List[Topic]:
    topics = []
    async with Transaction(sql=True):
        if topic_ids:
            topics.extend(await topic_db.get_many(topic_ids))
        if topic_names:
            topics.extend(await topic_db.get_many_by_name(values=topic_names))
        if do_retrieve_parents:
            parent_topic_ids = set()
            for topic in topics:
                if not topic.id:
                    continue
                parent_topic_ids.update(topic.parent_topic_ids)
            topic_ids_set = set([each.id for each in topics])
            parent_topic_ids -= topic_ids_set

            topics.extend(await retrieve_many(topic_ids=list(parent_topic_ids)))
    return topics


async def retrieve_all(filter: DefaultFilter) -> Tuple[List[Topic], str]:
    if filter.sort == DefaultFilter.TRENDING:
        topics, cursor = await topic_db.get_trending(filter)
    else:
        topics, cursor = await topic_db.paginate(filter)
    topics = await retrieve_many([each.id for each in topics])
    return topics, cursor


async def retrieve_following(user_id) -> List[FollowedTopic]:
    async with Transaction(sql=True):
        followed_topics = await topic_follow_db.query(user_id=user_id)
        topics = await retrieve_many(
            topic_ids=[each.topic.id for each in followed_topics]
        )
    for followed_topic in followed_topics:
        for topic in topics:
            if followed_topic.topic.id == topic.id:
                followed_topic.topic.CopyFrom(topic)
                break
    return followed_topics


async def update(
    old_topic: Topic,
    new_topic: Topic,
    new_parent_topic: Optional[Topic],
    old_names: List[str],
    new_names: List[str],
) -> Topic:
    merged_topic = Topic()
    merged_topic.CopyFrom(old_topic)

    if new_topic.aliases:
        del merged_topic.aliases[:]
    if new_topic.keywords:
        del merged_topic.aliases[:]

    if (
        new_topic.parent_topic_id != old_topic.parent_topic_id
        or new_topic.parent_topic_id == 0
    ):
        merged_topic.parent_topic_id = 0
        del merged_topic.parents[:]
    merged_topic.MergeFrom(new_topic)

    _normalize_topic(merged_topic)
    _link_topics(child_topic=merged_topic, parent_topic=new_parent_topic)

    topics_to_invalidate_cache = []
    async with Transaction(sql=True):
        if new_parent_topic:
            await topic_db.update(new_parent_topic)
        await topic_db.update(
            merged_topic, new_names=new_names, deleted_names=old_names
        )

        if (
            new_parent_topic
            or new_topic.name != old_topic.name
            or (not new_parent_topic and old_topic.parents)
        ):
            topics_to_invalidate_cache = []
            if old_topic.parent_topic_id:
                topics_to_invalidate_cache.append(
                    await topic_db.get(old_topic.parent_topic_id)
                )

            await _recursively_get_all_children(old_topic, topics_to_invalidate_cache)
        await topic_db.invalidate_cache(topics_to_invalidate_cache)
    return merged_topic


async def delete(topic: Topic):
    # prerequisites: topic has no children
    await topic_db.delete(id=topic.id)
    await topic_follow_db.delete(topic_id=topic.id)
    await topic_db.invalidate_cache([topic])
    if topic.parents:
        parent_topic = await topic_db.get(id=topic.parent_topic_id)
        updated_children = []
        for child in parent_topic.children:
            if child != topic.name:
                updated_children.append(child)
        del parent_topic.children[:]
        parent_topic.children.extend(updated_children)
        await topic_db.cache([parent_topic])


async def follow(user_id: int, topic_id: int, settings: FollowSettings) -> bool:
    async with Transaction(sql=True):
        inserted = await topic_follow_db.upsert(
            user_id=user_id,
            topic_id=topic_id,
            data={"follow_type": settings.follow_type},
        )
        if inserted:
            await topic_db.increment_followers(topic_id)
            return True
        return False


async def unfollow(user_id: int, topic_id: int) -> bool:
    async with Transaction(sql=True):
        deleted = await topic_follow_db.delete(user_id=user_id, topic_id=topic_id)
        if deleted:
            await topic_db.decrement_followers(topic_id)
            return True
        return False


async def _recursively_get_all_children(
    topic: Topic, topics_to_invalidate: List[Topic]
):
    topics = await topic_db.get_many_by_name([child for child in topic.children])
    topics_to_invalidate.extend(topics)
    for each in topics:
        await _recursively_get_all_children(each, topics_to_invalidate)


def _normalize_topic(topic: Topic):
    topic.name = topic.name.lower()

    for i, alias in enumerate(topic.aliases):
        topic.aliases[i] = alias.lower()

    for i, keyword in enumerate(topic.keywords):
        topic.keywords[i] = keyword.lower()


def _link_topics(child_topic: Topic, parent_topic: Topic):
    if not parent_topic:
        return

    # modify parent
    parent_topic.children.append(child_topic.name)
    unique_children = list(set(parent_topic.children))
    unique_children.sort()
    del parent_topic.children[:]
    parent_topic.children.extend(unique_children)

    # modify child
    del child_topic.parents[:]
    child_topic.parents.extend(list(parent_topic.parents) + [parent_topic.name])
    child_topic.parent_topic_ids.extend(
        list(parent_topic.parent_topic_ids) + [parent_topic.id]
    )
    child_topic.parent_topic_id = parent_topic.id
    child_topic.banner_picture_id = (
        parent_topic.banner_picture_id
        if not child_topic.banner_picture_id
        else child_topic.banner_picture_id
    )
