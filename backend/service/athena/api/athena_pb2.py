# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: athena/api/athena.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from athena.api import topic_pb2 as athena_dot_api_dot_topic__pb2
from weclikd.common import filter_pb2 as weclikd_dot_common_dot_filter__pb2
from weclikd.common import follow_pb2 as weclikd_dot_common_dot_follow__pb2
from google.protobuf import timestamp_pb2 as google_dot_protobuf_dot_timestamp__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='athena/api/athena.proto',
  package='weclikd',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x17\x61thena/api/athena.proto\x12\x07weclikd\x1a\x16\x61thena/api/topic.proto\x1a\x1bweclikd/common/filter.proto\x1a\x1bweclikd/common/follow.proto\x1a\x1fgoogle/protobuf/timestamp.proto\"T\n\x10GetTopicsRequest\x12\x11\n\ttopic_ids\x18\x01 \x03(\x04\x12\x13\n\x0btopic_names\x18\x02 \x03(\t\x12\x18\n\x10retrieve_parents\x18\x03 \x01(\x08\"3\n\x11GetTopicsResponse\x12\x1e\n\x06topics\x18\x01 \x03(\x0b\x32\x0e.weclikd.Topic\"&\n\x12\x44\x65leteTopicRequest\x12\x10\n\x08topic_id\x18\x01 \x01(\x04\"\x15\n\x13\x44\x65leteTopicResponse\"\xad\x01\n\x10\x45\x64itTopicRequest\x12\x10\n\x08topic_id\x18\x01 \x01(\x04\x12\x0c\n\x04name\x18\x02 \x01(\t\x12\x13\n\x0b\x64\x65scription\x18\x03 \x01(\t\x12\x19\n\x11\x62\x61nner_picture_id\x18\x04 \x01(\x04\x12\x0e\n\x06parent\x18\x05 \x01(\t\x12(\n\x08keywords\x18\x06 \x03(\x0b\x32\x16.weclikd.Topic.Keyword\x12\x0f\n\x07\x61liases\x18\x07 \x03(\t\"2\n\x11\x45\x64itTopicResponse\x12\x1d\n\x05topic\x18\x01 \x01(\x0b\x32\x0e.weclikd.Topic\"\x9e\x01\n\x13ProposeTopicRequest\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x0e\n\x06parent\x18\x02 \x01(\t\x12\x13\n\x0b\x64\x65scription\x18\x03 \x01(\t\x12\x19\n\x11\x62\x61nner_picture_id\x18\x04 \x01(\x04\x12(\n\x08keywords\x18\x05 \x03(\x0b\x32\x16.weclikd.Topic.Keyword\x12\x0f\n\x07\x61liases\x18\x06 \x03(\t\"5\n\x14ProposeTopicResponse\x12\x1d\n\x05topic\x18\x01 \x01(\x0b\x32\x0e.weclikd.Topic\"Q\n\x12\x46ollowTopicRequest\x12\x10\n\x08topic_id\x18\x01 \x01(\x04\x12)\n\x08settings\x18\x02 \x01(\x0b\x32\x17.weclikd.FollowSettings\"\x15\n\x13\x46ollowTopicResponse\"\x19\n\x17\x46ollowRootTopicsRequest\"\x1a\n\x18\x46ollowRootTopicsResponse\"(\n\x14UnfollowTopicRequest\x12\x10\n\x08topic_id\x18\x01 \x01(\x04\"\x17\n\x15UnfollowTopicResponse\"\x86\x01\n\rFollowedTopic\x12\x1d\n\x05topic\x18\x01 \x01(\x0b\x32\x0e.weclikd.Topic\x12)\n\x08settings\x18\x02 \x01(\x0b\x32\x17.weclikd.FollowSettings\x12+\n\x07\x63reated\x18\x03 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\"\x1a\n\x18GetTopicsFollowedRequest\"L\n\x19GetTopicsFollowedResponse\x12/\n\x0f\x66ollowed_topics\x18\x01 \x03(\x0b\x32\x16.weclikd.FollowedTopic\"=\n\x13GetAllTopicsRequest\x12&\n\x06\x66ilter\x18\x01 \x01(\x0b\x32\x16.weclikd.DefaultFilter\"J\n\x14GetAllTopicsResponse\x12\x1e\n\x06topics\x18\x01 \x03(\x0b\x32\x0e.weclikd.Topic\x12\x12\n\nend_cursor\x18\x02 \x01(\t2\xd5\x05\n\x06\x41thena\x12\x44\n\tGetTopics\x12\x19.weclikd.GetTopicsRequest\x1a\x1a.weclikd.GetTopicsResponse\"\x00\x12M\n\x0cGetAllTopics\x12\x1c.weclikd.GetAllTopicsRequest\x1a\x1d.weclikd.GetAllTopicsResponse\"\x00\x12\x44\n\tEditTopic\x12\x19.weclikd.EditTopicRequest\x1a\x1a.weclikd.EditTopicResponse\"\x00\x12J\n\x0b\x44\x65leteTopic\x12\x1b.weclikd.DeleteTopicRequest\x1a\x1c.weclikd.DeleteTopicResponse\"\x00\x12Y\n\x10\x46ollowRootTopics\x12 .weclikd.FollowRootTopicsRequest\x1a!.weclikd.FollowRootTopicsResponse\"\x00\x12M\n\x0cProposeTopic\x12\x1c.weclikd.ProposeTopicRequest\x1a\x1d.weclikd.ProposeTopicResponse\"\x00\x12J\n\x0b\x46ollowTopic\x12\x1b.weclikd.FollowTopicRequest\x1a\x1c.weclikd.FollowTopicResponse\"\x00\x12P\n\rUnfollowTopic\x12\x1d.weclikd.UnfollowTopicRequest\x1a\x1e.weclikd.UnfollowTopicResponse\"\x00\x12\\\n\x11GetTopicsFollowed\x12!.weclikd.GetTopicsFollowedRequest\x1a\".weclikd.GetTopicsFollowedResponse\"\x00\x62\x06proto3'
  ,
  dependencies=[athena_dot_api_dot_topic__pb2.DESCRIPTOR,weclikd_dot_common_dot_filter__pb2.DESCRIPTOR,weclikd_dot_common_dot_follow__pb2.DESCRIPTOR,google_dot_protobuf_dot_timestamp__pb2.DESCRIPTOR,])




_GETTOPICSREQUEST = _descriptor.Descriptor(
  name='GetTopicsRequest',
  full_name='weclikd.GetTopicsRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='topic_ids', full_name='weclikd.GetTopicsRequest.topic_ids', index=0,
      number=1, type=4, cpp_type=4, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='topic_names', full_name='weclikd.GetTopicsRequest.topic_names', index=1,
      number=2, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='retrieve_parents', full_name='weclikd.GetTopicsRequest.retrieve_parents', index=2,
      number=3, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=151,
  serialized_end=235,
)


_GETTOPICSRESPONSE = _descriptor.Descriptor(
  name='GetTopicsResponse',
  full_name='weclikd.GetTopicsResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='topics', full_name='weclikd.GetTopicsResponse.topics', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=237,
  serialized_end=288,
)


_DELETETOPICREQUEST = _descriptor.Descriptor(
  name='DeleteTopicRequest',
  full_name='weclikd.DeleteTopicRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='topic_id', full_name='weclikd.DeleteTopicRequest.topic_id', index=0,
      number=1, type=4, cpp_type=4, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=290,
  serialized_end=328,
)


_DELETETOPICRESPONSE = _descriptor.Descriptor(
  name='DeleteTopicResponse',
  full_name='weclikd.DeleteTopicResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=330,
  serialized_end=351,
)


_EDITTOPICREQUEST = _descriptor.Descriptor(
  name='EditTopicRequest',
  full_name='weclikd.EditTopicRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='topic_id', full_name='weclikd.EditTopicRequest.topic_id', index=0,
      number=1, type=4, cpp_type=4, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='name', full_name='weclikd.EditTopicRequest.name', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='description', full_name='weclikd.EditTopicRequest.description', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='banner_picture_id', full_name='weclikd.EditTopicRequest.banner_picture_id', index=3,
      number=4, type=4, cpp_type=4, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='parent', full_name='weclikd.EditTopicRequest.parent', index=4,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='keywords', full_name='weclikd.EditTopicRequest.keywords', index=5,
      number=6, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='aliases', full_name='weclikd.EditTopicRequest.aliases', index=6,
      number=7, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=354,
  serialized_end=527,
)


_EDITTOPICRESPONSE = _descriptor.Descriptor(
  name='EditTopicResponse',
  full_name='weclikd.EditTopicResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='topic', full_name='weclikd.EditTopicResponse.topic', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=529,
  serialized_end=579,
)


_PROPOSETOPICREQUEST = _descriptor.Descriptor(
  name='ProposeTopicRequest',
  full_name='weclikd.ProposeTopicRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='weclikd.ProposeTopicRequest.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='parent', full_name='weclikd.ProposeTopicRequest.parent', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='description', full_name='weclikd.ProposeTopicRequest.description', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='banner_picture_id', full_name='weclikd.ProposeTopicRequest.banner_picture_id', index=3,
      number=4, type=4, cpp_type=4, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='keywords', full_name='weclikd.ProposeTopicRequest.keywords', index=4,
      number=5, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='aliases', full_name='weclikd.ProposeTopicRequest.aliases', index=5,
      number=6, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=582,
  serialized_end=740,
)


_PROPOSETOPICRESPONSE = _descriptor.Descriptor(
  name='ProposeTopicResponse',
  full_name='weclikd.ProposeTopicResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='topic', full_name='weclikd.ProposeTopicResponse.topic', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=742,
  serialized_end=795,
)


_FOLLOWTOPICREQUEST = _descriptor.Descriptor(
  name='FollowTopicRequest',
  full_name='weclikd.FollowTopicRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='topic_id', full_name='weclikd.FollowTopicRequest.topic_id', index=0,
      number=1, type=4, cpp_type=4, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='settings', full_name='weclikd.FollowTopicRequest.settings', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=797,
  serialized_end=878,
)


_FOLLOWTOPICRESPONSE = _descriptor.Descriptor(
  name='FollowTopicResponse',
  full_name='weclikd.FollowTopicResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=880,
  serialized_end=901,
)


_FOLLOWROOTTOPICSREQUEST = _descriptor.Descriptor(
  name='FollowRootTopicsRequest',
  full_name='weclikd.FollowRootTopicsRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=903,
  serialized_end=928,
)


_FOLLOWROOTTOPICSRESPONSE = _descriptor.Descriptor(
  name='FollowRootTopicsResponse',
  full_name='weclikd.FollowRootTopicsResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=930,
  serialized_end=956,
)


_UNFOLLOWTOPICREQUEST = _descriptor.Descriptor(
  name='UnfollowTopicRequest',
  full_name='weclikd.UnfollowTopicRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='topic_id', full_name='weclikd.UnfollowTopicRequest.topic_id', index=0,
      number=1, type=4, cpp_type=4, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=958,
  serialized_end=998,
)


_UNFOLLOWTOPICRESPONSE = _descriptor.Descriptor(
  name='UnfollowTopicResponse',
  full_name='weclikd.UnfollowTopicResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1000,
  serialized_end=1023,
)


_FOLLOWEDTOPIC = _descriptor.Descriptor(
  name='FollowedTopic',
  full_name='weclikd.FollowedTopic',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='topic', full_name='weclikd.FollowedTopic.topic', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='settings', full_name='weclikd.FollowedTopic.settings', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='created', full_name='weclikd.FollowedTopic.created', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1026,
  serialized_end=1160,
)


_GETTOPICSFOLLOWEDREQUEST = _descriptor.Descriptor(
  name='GetTopicsFollowedRequest',
  full_name='weclikd.GetTopicsFollowedRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1162,
  serialized_end=1188,
)


_GETTOPICSFOLLOWEDRESPONSE = _descriptor.Descriptor(
  name='GetTopicsFollowedResponse',
  full_name='weclikd.GetTopicsFollowedResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='followed_topics', full_name='weclikd.GetTopicsFollowedResponse.followed_topics', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1190,
  serialized_end=1266,
)


_GETALLTOPICSREQUEST = _descriptor.Descriptor(
  name='GetAllTopicsRequest',
  full_name='weclikd.GetAllTopicsRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='filter', full_name='weclikd.GetAllTopicsRequest.filter', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1268,
  serialized_end=1329,
)


_GETALLTOPICSRESPONSE = _descriptor.Descriptor(
  name='GetAllTopicsResponse',
  full_name='weclikd.GetAllTopicsResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='topics', full_name='weclikd.GetAllTopicsResponse.topics', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='end_cursor', full_name='weclikd.GetAllTopicsResponse.end_cursor', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1331,
  serialized_end=1405,
)

_GETTOPICSRESPONSE.fields_by_name['topics'].message_type = athena_dot_api_dot_topic__pb2._TOPIC
_EDITTOPICREQUEST.fields_by_name['keywords'].message_type = athena_dot_api_dot_topic__pb2._TOPIC_KEYWORD
_EDITTOPICRESPONSE.fields_by_name['topic'].message_type = athena_dot_api_dot_topic__pb2._TOPIC
_PROPOSETOPICREQUEST.fields_by_name['keywords'].message_type = athena_dot_api_dot_topic__pb2._TOPIC_KEYWORD
_PROPOSETOPICRESPONSE.fields_by_name['topic'].message_type = athena_dot_api_dot_topic__pb2._TOPIC
_FOLLOWTOPICREQUEST.fields_by_name['settings'].message_type = weclikd_dot_common_dot_follow__pb2._FOLLOWSETTINGS
_FOLLOWEDTOPIC.fields_by_name['topic'].message_type = athena_dot_api_dot_topic__pb2._TOPIC
_FOLLOWEDTOPIC.fields_by_name['settings'].message_type = weclikd_dot_common_dot_follow__pb2._FOLLOWSETTINGS
_FOLLOWEDTOPIC.fields_by_name['created'].message_type = google_dot_protobuf_dot_timestamp__pb2._TIMESTAMP
_GETTOPICSFOLLOWEDRESPONSE.fields_by_name['followed_topics'].message_type = _FOLLOWEDTOPIC
_GETALLTOPICSREQUEST.fields_by_name['filter'].message_type = weclikd_dot_common_dot_filter__pb2._DEFAULTFILTER
_GETALLTOPICSRESPONSE.fields_by_name['topics'].message_type = athena_dot_api_dot_topic__pb2._TOPIC
DESCRIPTOR.message_types_by_name['GetTopicsRequest'] = _GETTOPICSREQUEST
DESCRIPTOR.message_types_by_name['GetTopicsResponse'] = _GETTOPICSRESPONSE
DESCRIPTOR.message_types_by_name['DeleteTopicRequest'] = _DELETETOPICREQUEST
DESCRIPTOR.message_types_by_name['DeleteTopicResponse'] = _DELETETOPICRESPONSE
DESCRIPTOR.message_types_by_name['EditTopicRequest'] = _EDITTOPICREQUEST
DESCRIPTOR.message_types_by_name['EditTopicResponse'] = _EDITTOPICRESPONSE
DESCRIPTOR.message_types_by_name['ProposeTopicRequest'] = _PROPOSETOPICREQUEST
DESCRIPTOR.message_types_by_name['ProposeTopicResponse'] = _PROPOSETOPICRESPONSE
DESCRIPTOR.message_types_by_name['FollowTopicRequest'] = _FOLLOWTOPICREQUEST
DESCRIPTOR.message_types_by_name['FollowTopicResponse'] = _FOLLOWTOPICRESPONSE
DESCRIPTOR.message_types_by_name['FollowRootTopicsRequest'] = _FOLLOWROOTTOPICSREQUEST
DESCRIPTOR.message_types_by_name['FollowRootTopicsResponse'] = _FOLLOWROOTTOPICSRESPONSE
DESCRIPTOR.message_types_by_name['UnfollowTopicRequest'] = _UNFOLLOWTOPICREQUEST
DESCRIPTOR.message_types_by_name['UnfollowTopicResponse'] = _UNFOLLOWTOPICRESPONSE
DESCRIPTOR.message_types_by_name['FollowedTopic'] = _FOLLOWEDTOPIC
DESCRIPTOR.message_types_by_name['GetTopicsFollowedRequest'] = _GETTOPICSFOLLOWEDREQUEST
DESCRIPTOR.message_types_by_name['GetTopicsFollowedResponse'] = _GETTOPICSFOLLOWEDRESPONSE
DESCRIPTOR.message_types_by_name['GetAllTopicsRequest'] = _GETALLTOPICSREQUEST
DESCRIPTOR.message_types_by_name['GetAllTopicsResponse'] = _GETALLTOPICSRESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

GetTopicsRequest = _reflection.GeneratedProtocolMessageType('GetTopicsRequest', (_message.Message,), {
  'DESCRIPTOR' : _GETTOPICSREQUEST,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.GetTopicsRequest)
  })
_sym_db.RegisterMessage(GetTopicsRequest)

GetTopicsResponse = _reflection.GeneratedProtocolMessageType('GetTopicsResponse', (_message.Message,), {
  'DESCRIPTOR' : _GETTOPICSRESPONSE,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.GetTopicsResponse)
  })
_sym_db.RegisterMessage(GetTopicsResponse)

DeleteTopicRequest = _reflection.GeneratedProtocolMessageType('DeleteTopicRequest', (_message.Message,), {
  'DESCRIPTOR' : _DELETETOPICREQUEST,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.DeleteTopicRequest)
  })
_sym_db.RegisterMessage(DeleteTopicRequest)

DeleteTopicResponse = _reflection.GeneratedProtocolMessageType('DeleteTopicResponse', (_message.Message,), {
  'DESCRIPTOR' : _DELETETOPICRESPONSE,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.DeleteTopicResponse)
  })
_sym_db.RegisterMessage(DeleteTopicResponse)

EditTopicRequest = _reflection.GeneratedProtocolMessageType('EditTopicRequest', (_message.Message,), {
  'DESCRIPTOR' : _EDITTOPICREQUEST,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.EditTopicRequest)
  })
_sym_db.RegisterMessage(EditTopicRequest)

EditTopicResponse = _reflection.GeneratedProtocolMessageType('EditTopicResponse', (_message.Message,), {
  'DESCRIPTOR' : _EDITTOPICRESPONSE,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.EditTopicResponse)
  })
_sym_db.RegisterMessage(EditTopicResponse)

ProposeTopicRequest = _reflection.GeneratedProtocolMessageType('ProposeTopicRequest', (_message.Message,), {
  'DESCRIPTOR' : _PROPOSETOPICREQUEST,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.ProposeTopicRequest)
  })
_sym_db.RegisterMessage(ProposeTopicRequest)

ProposeTopicResponse = _reflection.GeneratedProtocolMessageType('ProposeTopicResponse', (_message.Message,), {
  'DESCRIPTOR' : _PROPOSETOPICRESPONSE,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.ProposeTopicResponse)
  })
_sym_db.RegisterMessage(ProposeTopicResponse)

FollowTopicRequest = _reflection.GeneratedProtocolMessageType('FollowTopicRequest', (_message.Message,), {
  'DESCRIPTOR' : _FOLLOWTOPICREQUEST,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.FollowTopicRequest)
  })
_sym_db.RegisterMessage(FollowTopicRequest)

FollowTopicResponse = _reflection.GeneratedProtocolMessageType('FollowTopicResponse', (_message.Message,), {
  'DESCRIPTOR' : _FOLLOWTOPICRESPONSE,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.FollowTopicResponse)
  })
_sym_db.RegisterMessage(FollowTopicResponse)

FollowRootTopicsRequest = _reflection.GeneratedProtocolMessageType('FollowRootTopicsRequest', (_message.Message,), {
  'DESCRIPTOR' : _FOLLOWROOTTOPICSREQUEST,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.FollowRootTopicsRequest)
  })
_sym_db.RegisterMessage(FollowRootTopicsRequest)

FollowRootTopicsResponse = _reflection.GeneratedProtocolMessageType('FollowRootTopicsResponse', (_message.Message,), {
  'DESCRIPTOR' : _FOLLOWROOTTOPICSRESPONSE,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.FollowRootTopicsResponse)
  })
_sym_db.RegisterMessage(FollowRootTopicsResponse)

UnfollowTopicRequest = _reflection.GeneratedProtocolMessageType('UnfollowTopicRequest', (_message.Message,), {
  'DESCRIPTOR' : _UNFOLLOWTOPICREQUEST,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.UnfollowTopicRequest)
  })
_sym_db.RegisterMessage(UnfollowTopicRequest)

UnfollowTopicResponse = _reflection.GeneratedProtocolMessageType('UnfollowTopicResponse', (_message.Message,), {
  'DESCRIPTOR' : _UNFOLLOWTOPICRESPONSE,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.UnfollowTopicResponse)
  })
_sym_db.RegisterMessage(UnfollowTopicResponse)

FollowedTopic = _reflection.GeneratedProtocolMessageType('FollowedTopic', (_message.Message,), {
  'DESCRIPTOR' : _FOLLOWEDTOPIC,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.FollowedTopic)
  })
_sym_db.RegisterMessage(FollowedTopic)

GetTopicsFollowedRequest = _reflection.GeneratedProtocolMessageType('GetTopicsFollowedRequest', (_message.Message,), {
  'DESCRIPTOR' : _GETTOPICSFOLLOWEDREQUEST,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.GetTopicsFollowedRequest)
  })
_sym_db.RegisterMessage(GetTopicsFollowedRequest)

GetTopicsFollowedResponse = _reflection.GeneratedProtocolMessageType('GetTopicsFollowedResponse', (_message.Message,), {
  'DESCRIPTOR' : _GETTOPICSFOLLOWEDRESPONSE,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.GetTopicsFollowedResponse)
  })
_sym_db.RegisterMessage(GetTopicsFollowedResponse)

GetAllTopicsRequest = _reflection.GeneratedProtocolMessageType('GetAllTopicsRequest', (_message.Message,), {
  'DESCRIPTOR' : _GETALLTOPICSREQUEST,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.GetAllTopicsRequest)
  })
_sym_db.RegisterMessage(GetAllTopicsRequest)

GetAllTopicsResponse = _reflection.GeneratedProtocolMessageType('GetAllTopicsResponse', (_message.Message,), {
  'DESCRIPTOR' : _GETALLTOPICSRESPONSE,
  '__module__' : 'athena.api.athena_pb2'
  # @@protoc_insertion_point(class_scope:weclikd.GetAllTopicsResponse)
  })
_sym_db.RegisterMessage(GetAllTopicsResponse)



_ATHENA = _descriptor.ServiceDescriptor(
  name='Athena',
  full_name='weclikd.Athena',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=1408,
  serialized_end=2133,
  methods=[
  _descriptor.MethodDescriptor(
    name='GetTopics',
    full_name='weclikd.Athena.GetTopics',
    index=0,
    containing_service=None,
    input_type=_GETTOPICSREQUEST,
    output_type=_GETTOPICSRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='GetAllTopics',
    full_name='weclikd.Athena.GetAllTopics',
    index=1,
    containing_service=None,
    input_type=_GETALLTOPICSREQUEST,
    output_type=_GETALLTOPICSRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='EditTopic',
    full_name='weclikd.Athena.EditTopic',
    index=2,
    containing_service=None,
    input_type=_EDITTOPICREQUEST,
    output_type=_EDITTOPICRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='DeleteTopic',
    full_name='weclikd.Athena.DeleteTopic',
    index=3,
    containing_service=None,
    input_type=_DELETETOPICREQUEST,
    output_type=_DELETETOPICRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='FollowRootTopics',
    full_name='weclikd.Athena.FollowRootTopics',
    index=4,
    containing_service=None,
    input_type=_FOLLOWROOTTOPICSREQUEST,
    output_type=_FOLLOWROOTTOPICSRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='ProposeTopic',
    full_name='weclikd.Athena.ProposeTopic',
    index=5,
    containing_service=None,
    input_type=_PROPOSETOPICREQUEST,
    output_type=_PROPOSETOPICRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='FollowTopic',
    full_name='weclikd.Athena.FollowTopic',
    index=6,
    containing_service=None,
    input_type=_FOLLOWTOPICREQUEST,
    output_type=_FOLLOWTOPICRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='UnfollowTopic',
    full_name='weclikd.Athena.UnfollowTopic',
    index=7,
    containing_service=None,
    input_type=_UNFOLLOWTOPICREQUEST,
    output_type=_UNFOLLOWTOPICRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='GetTopicsFollowed',
    full_name='weclikd.Athena.GetTopicsFollowed',
    index=8,
    containing_service=None,
    input_type=_GETTOPICSFOLLOWEDREQUEST,
    output_type=_GETTOPICSFOLLOWEDRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_ATHENA)

DESCRIPTOR.services_by_name['Athena'] = _ATHENA

# @@protoc_insertion_point(module_scope)
