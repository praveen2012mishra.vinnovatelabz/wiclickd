import base64
import json
import requests

# Set the webhook_url to the one provided by Slack when you create the webhook at https://my.slack.com/services/new/incoming-webhook/
SLACK_WEBHOOK_URL = "https://hooks.slack.com/services/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"

def build_slack_message(msg, status, color):
    if "repoSource" in msg["source"]:
        return {
            'text': f'Janus-{msg["source"]["repoSource"]["branchName"]} CloudBuild: *{status}*',
            'mrkdwn': True,
            'attachments': [
                {
                    'title': 'Build Logs',
                    'title_link': msg['logUrl'],
                    'color': color
                },
                {
                    'title': 'Github Commit',
                    'title_link': f"https://github.com/weclikd/Weclikd/commit/{msg['sourceProvenance']['resolvedRepoSource']['commitSha']}",
                    'color': color
                }
            ]
        }
    else:
        return {
            'text': f'Janus-{msg["substitutions"]["BRANCH_NAME"]} CloudBuild: *{status}*',
            'mrkdwn': True,
            'attachments': [
                {
                    'title': 'Build Logs',
                    'title_link': msg['logUrl'],
                    'color': color
                },
                {
                    'title': 'Github Commit',
                    'title_link': f"https://github.com/weclikd/Weclikd/commit/{msg['substitutions']['COMMIT_SHA']}",
                    'color': color
                }
            ]
        }

def slack_build_notification(event, context):
    """Triggered from a message on a Cloud Pub/Sub topic.
    Args:
         event (dict): Event payload.
         context (google.cloud.functions.Context): Metadata for the event.
    """
    msg = base64.b64decode(event['data']).decode('utf-8')
    print(msg)
    msg = json.loads(msg)
    
    status = msg['status']
    statuses = ['SUCCESS', 'FAILURE', 'INTERNAL_ERROR', 'TIMEOUT']
    if status not in statuses:
        # only care about these statuses
        return
    if 'source' not in msg:
    	# not related to code check-in
        return
    
    if status == 'SUCCESS':
        color = '#00ff00'
    else:
        color = '#ff0000'
        
    slack_msg = build_slack_message(msg, status, color)
     
    response = requests.post(SLACK_WEBHOOK_URL, json=slack_msg)
    
    if response.status_code != 200:
        raise ValueError(
            'Request to slack returned an error %s, the response is:\n%s'
            % (response.status_code, response.text)
        )