output "REDIS_HOST" {
  value = "${module.redis-instance.REDIS_HOST}"
}

output "vpc_access_connector" {
  value = "${module.vpc-firewall.vpc_access_connector}"
}

output "NETWORK_NAME" {
    value = "${module.vpc-firewall.vpc_name}"
}

output "DB_Instance_Private_IP_Address" {
    value = "${module.sql-instance.private_ip_address}"
}

output "root_User_Password" {
    value = "${module.sql-instance.db_instance_generated_user1_password}"
}

output "app_User_Password" {
    value = "${module.sql-instance.db_instance_generated_user2_password}"
}

output "Database_Name" {
    value = "${module.sql-instance.db_name}"
}

output "SQL_DB_APP_PASSWORD" {
    value = "${module.secret-manager.app_name}"
}

output "SQL_DB_ROOT_PASSWORD" {
    value = "${module.secret-manager.root_name}"
}