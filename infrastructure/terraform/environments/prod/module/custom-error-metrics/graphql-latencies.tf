resource "google_logging_metric" "logging_metric" {
  name   = "graphql-latencies"
  filter = "resource.type=\"gae_app\" AND resource.labels.module_id=\"janus\" AND logName=\"projects/weclikd-prod/logs/appengine.googleapis.com%2Fstdout\" OR \"projects/weclikd-prod/logs/appengine.googleapis.com%2Fstderr\" OR \"projects/weclikd-prod/logs/appengine.googleapis.com%2Fnginx.request\" AND \"GraphQL Query\""
  metric_descriptor {
    metric_kind = "DELTA"
    value_type  = "DISTRIBUTION"
    unit        = "1"
    labels {
      key         = "mass"
      value_type  = "STRING"
    }
    labels {
      key         = "sku"
      value_type  = "STRING"
    }
    display_name = "My metric"
  }
  value_extractor = "EXTRACT(textPayload)"
  label_extractors = {
    "mass" = "EXTRACT(([0-9.]+) ms)"
    "sku"  = "EXTRACT(Query:([^ ]*))"
  }
}