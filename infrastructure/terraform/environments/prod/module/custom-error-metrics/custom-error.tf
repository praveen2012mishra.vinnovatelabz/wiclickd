resource "google_logging_metric" "logging_metric" {
  name   = "custom-logs-error-janus"
  filter = "resource.type=\"gae_app\" AND logName=\"projects/weclikd-prod/logs/stdout\" OR \"projects/weclikd-prod/logs/stderr\" OR \"projects/weclikd-prod/logs/appengine.googleapis.com%2Fstdout\" OR \"projects/weclikd-prod/logs/appengine.googleapis.com%2Fstderr\" OR \"projects/weclikd-prod/logs/appengine.googleapis.com%2Fnginx.request\" OR \"projects/weclikd-prod/logs/appengine.googleapis.com%2Frequest_log\" AND resource.labels.module_id=\"janus\" AND textPayload:\"ERROR\""
  metric_descriptor {
     metric_kind = "DELTA"
     value_type  = "INT64"
  }
}