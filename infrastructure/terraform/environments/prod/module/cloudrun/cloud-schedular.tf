resource "google_cloud_scheduler_job" "job1" {
  name        = "refresh-trending"
  description = "refresh-trending"
  region      = "us-central1"
  schedule    = "*/15 * * * *"
  time_zone   = "America/Los_Angeles"

  pubsub_target {
    # topic.id is the topic's full resource name.
    topic_name = google_pubsub_topic.topic3.id
    data       = base64encode("{}")
  }
}

resource "google_cloud_scheduler_job" "job2" {
  name        = "external-feed-poll"
  description = "Poll all external feeds to index external content"
  region      = "us-central1"
  schedule    = "*/15 * * * *"
  time_zone   = "America/Los_Angeles"

  pubsub_target {
    # topic.id is the topic's full resource name.
    topic_name = google_pubsub_topic.topic1.id
    data       = base64encode("{}")
  }
}