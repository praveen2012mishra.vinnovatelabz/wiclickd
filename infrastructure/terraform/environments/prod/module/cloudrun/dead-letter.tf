resource "google_pubsub_topic" "dead1" {
  name = "artemis.external-feed-item-process-dlq"
}

resource "google_pubsub_subscription" "dead_sub1" {
  name  = "artemis.external-feed-item-process-dlq"
  topic = google_pubsub_topic.dead1.name

  # 7 Days
  message_retention_duration = "604800s"
//   retain_acked_messages      = false

  ack_deadline_seconds = 10

  expiration_policy {
    ttl = ""
  }
}


resource "google_pubsub_topic" "dead2" {
  name = "artemis.external-feed-poll-dlq"
}

resource "google_pubsub_subscription" "dead_sub2" {
  name  = "artemis.external-feed-poll-dlq"
  topic = google_pubsub_topic.dead2.name

  # 7 Days
  message_retention_duration = "604800s"
//   retain_acked_messages      = false

  ack_deadline_seconds = 10

  expiration_policy {
    ttl = ""
  }
}


resource "google_pubsub_topic" "dead3" {
  name = "atlas.refresh-trending-dlq"
}

resource "google_pubsub_subscription" "dead_sub3" {
  name  = "atlas.refresh-trending-dlq"
  topic = google_pubsub_topic.dead3.name

  # 7 Days
  message_retention_duration = "604800s"
//   retain_acked_messages      = false

  ack_deadline_seconds = 10

  expiration_policy {
    ttl = ""
  }
}