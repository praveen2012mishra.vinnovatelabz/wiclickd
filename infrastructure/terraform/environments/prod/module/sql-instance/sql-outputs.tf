output db_instance_name {
  description = "Name of the database instance"
  value       = "${google_sql_database_instance.instance.name}"
}

output db_instance_username1 {
  description = "Name of the database user"
  value       = "${var.db_user1_name}"
}

output db_instance_username2 {
  description = "Name of the database user"
  value       = "${var.db_user2_name}"
}

output db_instance_generated_user1_password {
  description = "The auto generated default user password if no input password was provided"
  value       = "${random_id.user1_password.hex}"
}

output db_instance_generated_user2_password {
  description = "The auto generated default user password if no input password was provided"
  value       = "${random_id.user2_password.hex}"
}

output private_ip_address {
  description = "Private IP against that instance"
  value       = "${google_sql_database_instance.instance.private_ip_address}"
}

output db_name {
    value = "${google_sql_database.sql.name}"
}