resource "google_compute_network" "vpc-2" {
 name                    = "${var.vpc_2_name}"
 auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "subnet-2" {
 name          = "${var.vpc_subnet_2_name}"
 ip_cidr_range = "${var.cidr_range_2}"
 network       = "${var.vpc_2_name}"
 depends_on    = ["google_compute_network.vpc-2"]
 region      = "us-central1"
}

