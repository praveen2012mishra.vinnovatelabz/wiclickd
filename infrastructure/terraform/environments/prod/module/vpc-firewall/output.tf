output vpc_name {
    value = "${google_compute_network.vpc-2.name}"
}

output vpc_subnetwork {
    value = "${google_compute_subnetwork.subnet.name}"
}

output vpc_access_connector {
   value = "${google_vpc_access_connector.connector.id}"
}

output private_vpc {
    value = "${google_compute_network.vpc.name}"
}

output vpc_self_link {
    value = "${google_compute_network.vpc.self_link}"
}


