resource "google_vpc_access_connector" "connector" {
  name          = "prod-vpc-connector"
  provider      = "google-beta"
  region        = "us-central1"
  ip_cidr_range = "10.8.0.0/28"
  network       = "${var.vpc_name}"
  min_throughput = "200"
  max_throughput = "300"
}
