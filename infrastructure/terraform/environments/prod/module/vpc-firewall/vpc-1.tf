resource "google_compute_network" "vpc" {
 name                    = "${var.vpc_name}"
 auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "subnet" {
 name          = "${var.vpc_subnet_name}"
 ip_cidr_range = "${var.cidr_range}"
 network       = "${var.vpc_name}"
 depends_on    = ["google_compute_network.vpc"]
 region      = "us-central1"
}

resource "google_compute_firewall" "terraform-ssh-firewall" {
  name    = "${var.ssh_rule}"
  network = "${google_compute_network.vpc.name}"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["35.235.240.0/20"]

  target_tags = []
}

resource "google_compute_firewall" "terraform-allow-internal" {
  name    = "${var.internal_rule}"
  network = "${google_compute_network.vpc.name}"
  allow {
    protocol = "icmp"
  }
  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }
  allow {
    protocol = "udp"
    ports    = ["0-65535"]
  }
  source_ranges = [
    "${var.cidr_range}"
  ]
}
