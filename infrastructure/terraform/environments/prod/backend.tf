terraform {
 backend "gcs" {
   bucket  = "weclikd-prod-terraform"
   prefix  = "tfstate"
 }
}
