output app_name {
    value = "${google_secret_manager_secret_version.secret-version-app-pwd.name}"
}

output root_name {
    value = "${google_secret_manager_secret_version.secret-version-root-pwd.name}"
}