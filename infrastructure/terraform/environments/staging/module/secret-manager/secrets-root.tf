resource "google_secret_manager_secret" "secret-root-pwd" {
  provider = google-beta

  secret_id = "SQL_ROOT_PASSWORD"

  replication {
    automatic = true
  }
}


resource "google_secret_manager_secret_version" "secret-version-root-pwd" {
  provider = google-beta

  secret = google_secret_manager_secret.secret-root-pwd.id

  secret_data = "${var.root_password}"
}