# GOOGLE STACKDRIVER using TERRAFORM

# To create the Monitoring alert using Terraform
1. Need 3 file to deploy the ploicy against separate project with separate environment variable value
2. Need following files are - alert_policy.tf + variables.tf + prod-terraform.tfvars / dev-terraform.tfvars
3. Steps to deploy policy -
   1. Create a directory & copy all the file into here
   2. terraform init
   3. terraform apply -var-file="prod-terraform.tfvars" {depend on project}
   4. rm -rf terraform.tfstate {if you want to go for another project or if you go in same project, then not require that step}  