variable "project" {
    default = "electric-block-241402"
}
variable "dbid" {
    default = "dev-sql-instance"
}
variable "critical" {
    default = "projects/electric-block-241402/notificationChannels/5995232713007715501"
}
variable "warning" {
    default = "projects/electric-block-241402/notificationChannels/6200475027715491450"
}
variable "app1" {
    default = "janus"
}
variable "app2" {
    default = "default"
}
variable "uptimeapp1" {
    default = "janus-health-check"
}
variable "uptimeapp2" {
    default = "aphrodite-uptime-checking"
}
