variable internal_rule {
    default = "allow-internal"
}

variable ssh_rule {
    default = "ssh-access"
}

variable cidr_range {
    default = "192.168.0.0/20"
}

variable vpc_access_connector_cidr_range {
    default = "10.8.0.0/28"
}

variable vpc_name {
    default = "vpc-1"
}

variable vpc_subnet_name {
    default = "tf-vpc-subnet-1"
}

variable vpc_2_name {
    default = "vpc-2"
}

variable vpc_subnet_2_name {
    default = "tf-vpc-subnet-2"
}

variable cidr_range_2 {
    default = "172.16.0.0/20"
}