data "archive_file" "py-init" {
  type        = "zip"
  source_dir = "../cloud-function-source-code/build_alert/"
  output_path = "./function-source-code-python.zip"
}

resource "google_storage_bucket_object" "py-archive" {
  name   = "function-source-code-python.zip"
  bucket = "electric-block-241402-terraform"
  source = "./function-source-code-python.zip"
}

resource "google_cloudfunctions_function" "py-functions" {
    name                      = "build_notification"
    runtime                   = "python37"
    entry_point               = "slack_build_notification"
    
    available_memory_mb       = 256
    timeout                   = 60
    region                    = "us-central1"

  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = "projects/electric-block-241402/topics/cloud-builds"
  }    

    source_archive_bucket     = "electric-block-241402-terraform"
    source_archive_object     = "${google_storage_bucket_object.py-archive.name}"

    # source_archive_bucket     = "${google_storage_bucket.bucket.name}"
    # source_archive_object     = "function-source-code-python.zip"

}

resource "google_cloudfunctions_function_iam_member" "py-invoker" {
  region         = google_cloudfunctions_function.py-functions.region
  cloud_function = google_cloudfunctions_function.py-functions.name

  role   = "roles/cloudfunctions.invoker"
  member = "serviceAccount:electric-block-241402@appspot.gserviceaccount.com"
}