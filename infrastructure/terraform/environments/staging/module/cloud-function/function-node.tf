data "archive_file" "node-init" {
  type        = "zip"
  source_dir = "../cloud-function-source-code/billing_alert/"
  output_path = "./function-source-code-nodejs.zip"
}

resource "google_storage_bucket_object" "node-archive" {
  name   = "function-source-code-nodejs.zip"
  bucket = "electric-block-241402-terraform"
  source = "./function-source-code-nodejs.zip"
}

resource "google_cloudfunctions_function" "node-functions" {
    name                      = "billing_notification"
    runtime                   = "nodejs10"
    entry_point               = "billing_alert"
    
    available_memory_mb       = 128
    timeout                   = 60
    region                    = "us-central1"

  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = "projects/electric-block-241402/topics/billing_alert"
  }    

    source_archive_bucket     = "electric-block-241402-terraform"    
    source_archive_object     = "${google_storage_bucket_object.node-archive.name}"

    # source_archive_bucket     = "${google_storage_bucket.bucket.name}"
    # source_archive_object     = "function-source-code-nodejs.zip"

  environment_variables = {
    SLACK_WEBHOOK_URL = "https://hooks.slack.com/services/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"
  }

}

resource "google_cloudfunctions_function_iam_member" "node-invoker" {
  region         = google_cloudfunctions_function.node-functions.region
  cloud_function = google_cloudfunctions_function.node-functions.name

  role   = "roles/cloudfunctions.invoker"
  member = "serviceAccount:electric-block-241402@appspot.gserviceaccount.com"
}