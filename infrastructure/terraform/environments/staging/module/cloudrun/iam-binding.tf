resource "google_project_iam_binding" "log_user" {
  role    = "roles/logging.logWriter"
  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_project_iam_binding" "cloudsql_editor" {
  role    = "roles/cloudsql.editor"
  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_project_iam_binding" "secret_accessor" {
  role    = "roles/secretmanager.secretAccessor"
  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_project_iam_binding" "datastore_owner" {
  role    = "roles/datastore.owner"
  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_project_iam_binding" "storage_admin" {
  role    = "roles/storage.admin"
  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_project_iam_binding" "pubsub_publisher" {
  role    = "roles/pubsub.publisher"
  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_project_iam_binding" "firebaseauth_admin" {
  role    = "roles/firebaseauth.admin"
  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_project_iam_binding" "servicedirectory_viewer" {
  role    = "roles/servicedirectory.viewer"
  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_project_iam_binding" "run_invoker" {
  role    = "roles/run.invoker"
  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_project_iam_binding" "redis_admin" {
  role    = "roles/redis.admin"
  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}

resource "google_project_iam_binding" "pubsub_admin" {
  role    = "roles/pubsub.admin"
  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
} 

resource "google_project_iam_binding" "iam_serviceAccountUser" {
  role    = "roles/iam.serviceAccountUser"
  members = [
    "serviceAccount:${google_service_account.service_account.email}"
  ]
}