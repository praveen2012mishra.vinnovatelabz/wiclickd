output "cloudrun" {
  value = "${google_cloud_run_service.janus.status[0].url}"
}


output "invoker_service_account" {
  value = "${google_service_account.service_account.email}"
}

output "pubsub_topic1_id" {
  value = "${google_pubsub_topic.topic1.id}"
}

output "pubsub_topic2_id" {
  value = "${google_pubsub_topic.topic2.id}"
}
