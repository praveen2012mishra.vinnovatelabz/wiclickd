resource "google_pubsub_topic" "topic1" {
  name = "${var.topic1_name}"

  provisioner "local-exec" {
    command = "gcloud --project $GCP_PROJECT_ID beta pubsub subscriptions create $SUBSCRIPTION --topic=$TOPIC --expiration-period=$EXPIRATION_PERIOD --ack-deadline=$ACK_DEADLINE --push-endpoint=$PUSH_ENDPOINT --dead-letter-topic=$DEAD_LETTER_TOPIC --max-delivery-attempts=$MAX_ATTEMPTS --push-auth-token-audience=$AUDIENCE --push-auth-service-account=$SERVICE_ACCOUNT"
    environment = {
      ACK_DEADLINE               = 600
      EXPIRATION_PERIOD          = "never"
      GCP_PROJECT_ID             = var.project_id
      PUSH_ENDPOINT              = "${google_cloud_run_service.janus.status[0].url}/artemis/external-feed-poll?token=vDXdmSNd1n"
      SERVICE_ACCOUNT            = "${google_service_account.service_account.email}"
      SUBSCRIPTION               = "${var.topic1_name}"
      TOPIC                      = "${google_pubsub_topic.topic1.name}"
      MAX_ATTEMPTS               = 5
      DEAD_LETTER_TOPIC          = "${google_pubsub_topic.dead2.name}"
      AUDIENCE                   = "weclikd.com"
    }
  }  
}

resource "google_pubsub_topic" "topic2" {
  name = "${var.topic2_name}"

  provisioner "local-exec" {
    command = "gcloud --project $GCP_PROJECT_ID beta pubsub subscriptions create $SUBSCRIPTION --topic=$TOPIC --expiration-period=$EXPIRATION_PERIOD --ack-deadline=$ACK_DEADLINE --push-endpoint=$PUSH_ENDPOINT --dead-letter-topic=$DEAD_LETTER_TOPIC --max-delivery-attempts=$MAX_ATTEMPTS --push-auth-token-audience=$AUDIENCE --push-auth-service-account=$SERVICE_ACCOUNT"
    environment = {
      ACK_DEADLINE               = 600
      EXPIRATION_PERIOD          = "never"
      GCP_PROJECT_ID             = var.project_id
      PUSH_ENDPOINT              = "${google_cloud_run_service.janus.status[0].url}/artemis/external-feed-item-process?token=vDXdmSNd1n"
      SERVICE_ACCOUNT            = "${google_service_account.service_account.email}"
      SUBSCRIPTION               = "${var.topic2_name}"
      TOPIC                      = "${google_pubsub_topic.topic2.name}"
      MAX_ATTEMPTS               = 5
      DEAD_LETTER_TOPIC          = "${google_pubsub_topic.dead1.name}"
      AUDIENCE                   = "weclikd.com"
    }
  }  
}

resource "google_pubsub_topic" "topic3" {
  name = "${var.topic3_name}"

  provisioner "local-exec" {
    command = "gcloud --project $GCP_PROJECT_ID beta pubsub subscriptions create $SUBSCRIPTION --topic=$TOPIC --expiration-period=$EXPIRATION_PERIOD --ack-deadline=$ACK_DEADLINE --push-endpoint=$PUSH_ENDPOINT --dead-letter-topic=$DEAD_LETTER_TOPIC --max-delivery-attempts=$MAX_ATTEMPTS --push-auth-token-audience=$AUDIENCE --push-auth-service-account=$SERVICE_ACCOUNT"
    environment = {
      ACK_DEADLINE               = 600
      EXPIRATION_PERIOD          = "never"
      GCP_PROJECT_ID             = var.project_id
      PUSH_ENDPOINT              = "${google_cloud_run_service.janus.status[0].url}/atlas/refresh-trending?token=vDXdmSNd1n"
      SERVICE_ACCOUNT            = "${google_service_account.service_account.email}"
      SUBSCRIPTION               = "${var.topic3_name}"
      TOPIC                      = "${google_pubsub_topic.topic3.name}"
      MAX_ATTEMPTS               = 5
      DEAD_LETTER_TOPIC          = "${google_pubsub_topic.dead3.name}"
      AUDIENCE                   = "weclikd.com"
    }
  }  
}



