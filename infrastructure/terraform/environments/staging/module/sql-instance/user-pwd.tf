# create user1
resource "random_id" "user1_password" {
  byte_length = 8
}

resource "google_sql_user" "user1" {
  name     = "${var.db_user1_name}"
  project  = "${var.project_id}"
  instance = "${google_sql_database_instance.instance.name}"
  host     = "${var.db_user_host}"
  password = "${var.db_user_password == "" ? random_id.user1_password.hex : var.db_user_password}"
}

# create user2
resource "random_id" "user2_password" {
  byte_length = 8
}


resource "google_sql_user" "user2" {
  name     = "${var.db_user2_name}"
  project  = "${var.project_id}"
  instance = "${google_sql_database_instance.instance.name}"
  host     = "${var.db_user_host}"
  password = "${var.db_user_password == "" ? random_id.user2_password.hex : var.db_user_password}"
}