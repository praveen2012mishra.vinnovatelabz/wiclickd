resource "google_compute_global_address" "private_ip_address" {
  provider = google-beta

  name          = "peer-private-ip"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = "${var.vpc_self_link}"
}

resource "google_service_networking_connection" "private_vpc_connection" {
  provider = google-beta

  network                 = "${var.vpc_self_link}"
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}


resource "google_sql_database_instance" "instance" {
        name = "staging-sql"
        region = "us-central1"
        database_version = "${var.db_version}"

        depends_on = [google_service_networking_connection.private_vpc_connection]
        
        settings {
                tier = "${var.db_tier}"
                activation_policy = "${var.db_activation_policy}"
                disk_autoresize = "${var.db_disk_autoresize}"
                disk_size = "${var.db_disk_size}"
                disk_type = "${var.db_disk_type}"
                pricing_plan = "${var.db_pricing_plan}"

                database_flags {
                        name  = "slow_query_log"
                        value = "on"
                }

                ip_configuration {
                        ipv4_enabled = "true"
                        private_network = "projects/${var.project_id}/global/networks/${var.vpc_name}"
                }
        }

        provisioner "local-exec" {
              command = "gcloud beta service-directory endpoints create master --address ${google_sql_database_instance.instance.private_ip_address} --port 3306 --service sql --namespace weclikd --location us-central1"
        } 


}


# create database
resource "google_sql_database" "sql" {
  name      = "${var.db_name}"
  project   = "${var.project_id}"
  instance  = "${google_sql_database_instance.instance.name}"
  charset   = "${var.db_charset}"
  collation = "${var.db_collation}"
}
