terraform {
 backend "gcs" {
   bucket  = "graphite-tesla-246507-terraform"
   prefix  = "tfstate"
 }
}