terraform {
  required_version = ">= 0.12"
}

provider "google" {
  project = "${var.project_id}"
}

provider "google-beta" {
  project = "${var.project_id}"
}


#module "storage" {
#  source = "./module/storage/"
      
#}

module "redis-instance" {
  source = "./module/redis-instance/"
  
  project = "${var.project_id}"
  network = "${module.vpc-firewall.vpc_name}"
  subnetwork = "${module.vpc-firewall.vpc_subnetwork}"  
}

module "sql-instance" {
  source = "./module/sql-instance/"

  vpc_name = "${module.vpc-firewall.private_vpc}"
  project_id = "${var.project_id}"
  vpc_self_link = "${module.vpc-firewall.vpc_self_link}"
}

module "vpc-firewall" {
  source = "./module/vpc-firewall/"
  
}

// module "stackdriver" {
//  source = "./module/stackdriver/"
// }

// module "cloud-functions" {
//  source = "./module/cloud-function"

// }

module "secret-manager" {
    source = "./module/secret-manager"

    sql_name = "${module.sql-instance.db_name}"
    app_password = "${module.sql-instance.db_instance_generated_user2_password}"
    root_password = "${module.sql-instance.db_instance_generated_user1_password}"
}

module "cloudrun" {
  source = "./module/cloudrun"

  connector_name = "${module.vpc-firewall.vpc_access_connector}"
}
