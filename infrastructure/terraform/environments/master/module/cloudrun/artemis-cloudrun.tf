resource "google_cloud_run_service" "janus" {
  name     = "${var.cloudrun_name}"
  location = "us-central1"
  autogenerate_revision_name = true
  
  template {
    spec {

      service_account_name = "${google_service_account.service_account.email}"

      containers {
        image = "gcr.io/electric-block-241402/janus"

        env {
          name = "PROJECT_ID"
          value = "${var.project_id}"
        }   

        resources {
          limits = {
            cpu    = "1000m"
            memory = "2048Mi"
          }
        }
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }

  provisioner "local-exec" {
    command = "gcloud beta run services update ${var.cloudrun_name} --platform=managed --region=us-central1 --vpc-connector ${var.connector_name}"
  } 

}

data "google_iam_policy" "auth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }

}

resource "google_cloud_run_service_iam_policy" "auth" {
  location    = google_cloud_run_service.janus.location
  project     = google_cloud_run_service.janus.project
  service     = google_cloud_run_service.janus.name

  policy_data = data.google_iam_policy.auth.policy_data
}
