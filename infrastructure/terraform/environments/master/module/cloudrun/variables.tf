variable "cloudrun_name" {
  default     = "janus"
}

variable "topic1_name" {
    default   = "external-feed-poll"
}

variable "topic2_name" {
    default   = "external-feed-item-process"
}

variable "topic3_name" {
    default   = "refresh-trending"
}

variable "service_name" {
    default   = "janus"
}

variable "project_id" {
    default   = "electric-block-241402"
}

variable "connector_name" {}

