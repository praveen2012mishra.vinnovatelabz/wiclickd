provider "google" {
  project = "${var.project}"
}  

locals {
  warning = "${var.warning}"
  critical = "${var.critical}"
}


resource "google_monitoring_alert_policy" "alert_policy0" {
  display_name = "Memory utilization alert against sql instance - ${var.dbid}"
  combiner = "OR"
  conditions {
    display_name = "Cloud SQL Database - Memory utilization (filtered) (grouped) [SUM]"
    condition_threshold {
      filter = "metric.type=\"cloudsql.googleapis.com/database/memory/utilization\" resource.type=\"cloudsql_database\" resource.label.database_id=\"${var.project}:${var.dbid}\"" 
      duration = "180s"
      comparison = "COMPARISON_GT"
      threshold_value = 0.90
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "60s"
        per_series_aligner = "ALIGN_MEAN"
        cross_series_reducer = "REDUCE_SUM"
      }
    }
  }
  documentation {
    content = "90% memory utilization crossed from 5 min against ${var.dbid}."
  }
  notification_channels = [
      "${local.critical}",
    ]
}

resource "google_monitoring_alert_policy" "alert_policy1" {
  display_name = "Disk Usage status for ${var.dbid}"
  combiner = "OR"
  conditions {
    display_name = "Bytes used (filtered) (grouped) [SUM] - sql instnace"
    condition_threshold {
      filter = "metric.type=\"cloudsql.googleapis.com/database/disk/bytes_used\" resource.type=\"cloudsql_database\" resource.label.project_id=\"${var.project}\" resource.label.database_id=\"${var.project}:${var.dbid}\""
      duration = "60s"
      comparison = "COMPARISON_GT"
      threshold_value = 100000000000
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "60s"
        per_series_aligner = "ALIGN_MEAN"
        cross_series_reducer = "REDUCE_SUM"
      }
    }
  }
  documentation {
    content = "Crossed 100Gb disk storage - ${var.dbid}."
  }

  notification_channels = [
      "${local.warning}",
    ]
}

resource "google_monitoring_alert_policy" "alert_policy2" {
  display_name = "${var.project} ${var.app1} response latency [99%]"
  combiner = "OR"
  conditions {
    display_name = "Response latency for ${var.project}, ${var.app1} by label.response_code [MIN]"
    condition_threshold {
      filter = "metric.type=\"appengine.googleapis.com/http/server/response_latencies\" resource.type=\"gae_app\" resource.label.project_id=\"${var.project}\" resource.label.module_id=\"${var.app1}\"" 
      duration = "60s"
      comparison = "COMPARISON_GT"
      threshold_value = 200
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "60s"
        per_series_aligner = "ALIGN_PERCENTILE_99"
        cross_series_reducer = "REDUCE_PERCENTILE_99"
      }
    }
  }
  documentation {
    content = "The rule trigger when response latency will cross 99% against ${var.app1} app-engine."
  }
  notification_channels = [
    "${local.warning}",
    ]  
}

resource "google_monitoring_alert_policy" "alert_policy3" {
  display_name = "${var.project} ${var.app2} response latency [99%]"
  combiner = "OR"
  conditions {
    display_name = "Response latency for ${var.project}, ${var.app2} by label.response_code [MIN]"
    condition_threshold {
      filter = "metric.type=\"appengine.googleapis.com/http/server/response_latencies\" resource.type=\"gae_app\" resource.label.project_id=\"${var.project}\" resource.label.module_id=\"${var.app2}\"" 
      duration = "60s"
      comparison = "COMPARISON_GT"
      threshold_value = 200
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "60s"
        per_series_aligner = "ALIGN_PERCENTILE_99"
        cross_series_reducer = "REDUCE_PERCENTILE_99"
      }
    }
  }
  documentation {
    content = "The rule trigger when response latency will cross 99% against ${var.app2} app-engine."
  }
  notification_channels = [
    "${local.warning}",
    ]  
}

resource "google_monitoring_alert_policy" "alert_policy4" {
  display_name = "${var.project} ${var.app2} response latency [50%]"
  combiner = "OR"
  conditions {
    display_name = "Response latency for ${var.project}, ${var.app2} by label.response_code [MIN]"
    condition_threshold {
      filter = "metric.type=\"appengine.googleapis.com/http/server/response_latencies\" resource.type=\"gae_app\" resource.label.project_id=\"${var.project}\" resource.label.module_id=\"${var.app2}\"" 
      duration = "60s"
      comparison = "COMPARISON_GT"
      threshold_value = 200
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "60s"
        per_series_aligner = "ALIGN_PERCENTILE_50"
        cross_series_reducer = "REDUCE_PERCENTILE_50"
      }
    }
  }
  documentation {
    content = "The rule trigger when response latency will cross 50% against ${var.app2} app-engine."
  }
  notification_channels = [
    "${local.warning}",
    ]  
}

resource "google_monitoring_alert_policy" "alert_policy5" {
  display_name = "${var.project} ${var.app1} response latency [50%]"
  combiner = "OR"
  conditions {
    display_name = "Response latency for ${var.project}, ${var.app1} by label.response_code [MIN]"
    condition_threshold {
      filter = "metric.type=\"appengine.googleapis.com/http/server/response_latencies\" resource.type=\"gae_app\" resource.label.project_id=\"${var.project}\" resource.label.module_id=\"${var.app1}\"" 
      duration = "60s"
      comparison = "COMPARISON_GT"
      threshold_value = 200
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "60s"
        per_series_aligner = "ALIGN_PERCENTILE_50"
        cross_series_reducer = "REDUCE_PERCENTILE_50"
      }
    }
  }
  documentation {
    content = "The rule trigger when response latency will cross 50% against ${var.app1} app-engine."
  }
  notification_channels = [
    "${local.warning}",
    ]  
}
resource "google_monitoring_alert_policy" "alert_policy6" {
  display_name = "Cloud storage alert - ${var.project}"
  combiner = "OR"
  conditions {
    display_name = "Total storage usage for ${var.project}"
    condition_threshold {
      filter = "metric.type=\"storage.googleapis.com/storage/total_bytes\" resource.type=\"gcs_bucket\" resource.label.project_id=\"${var.project}\"" 
      duration = "60s"
      comparison = "COMPARISON_GT"
      threshold_value = 100000000000
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "60s"
        per_series_aligner = "ALIGN_MEAN"
        cross_series_reducer = "REDUCE_NONE"
      }
    }
  }
  documentation {
    content = "100GB Cloud storage alert - ${var.project}."
  }
  notification_channels = [
    "${local.warning}",
    ]  
}

resource "google_monitoring_alert_policy" "alert_policy7" {
  display_name = "CPU utilization against ${var.dbid}"
  combiner = "OR"
  conditions {
    display_name = "CPU utilization by priority (filtered) (grouped) [MAX]"
    condition_threshold {
      filter = "metric.type=\"spanner.googleapis.com/instance/cpu/utilization_by_priority\" resource.type=\"spanner_instance\" resource.label.instance_id=\"${var.dbid}\" metric.label.priority=\"high\"" 
      duration = "180s"
      comparison = "COMPARISON_GT"
      threshold_value = 0.90
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "120s"
        per_series_aligner = "ALIGN_MEAN"
        cross_series_reducer = "REDUCE_MAX"
      }
    }
  }
  documentation {
    content = "90% CPU utilisation crossed from 3 min against ${var.dbid}."
  }
  notification_channels = [
    "${local.critical}",
    ]  
}

resource "google_monitoring_alert_policy" "alert_policy8" {
  display_name = "Response count above 299 in ${var.project}"
  combiner = "OR"
  conditions {
    display_name = "GAE Application - Response count (filtered)"
    condition_threshold {
      filter = "metric.type=\"appengine.googleapis.com/http/server/response_count\" resource.type=\"gae_app\" resource.label.project_id=\"${var.project}\" metric.label.response_code>\"299\"" 
      duration = "60s"
      comparison = "COMPARISON_GT"
      threshold_value = 10
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "60s"
        per_series_aligner = "ALIGN_RATE"
        cross_series_reducer = "REDUCE_NONE"
      }
    }
  }
  documentation {
    content = "Response count above 299 in ${var.project}."
  }
  notification_channels = [
    "${local.warning}",
    ]  
}

resource "google_monitoring_alert_policy" "alert_policy9" {
  display_name = "slow cloudsql query - ${var.project}"
  combiner = "OR"
  conditions {
    display_name = "logging/user/slow_mysql_query "
    condition_threshold {
      filter = "metric.type=\"logging.googleapis.com/user/slow_mysql_query\" resource.type=\"cloudsql_database\" resource.label.\"database_id\"=\"${var.project}:${var.dbid}\"" 
      duration = "60s"
      comparison = "COMPARISON_GT"
      threshold_value = 2
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "60s"
        per_series_aligner = "ALIGN_MEAN"
        cross_series_reducer = "REDUCE_NONE"
      }
    }
  }
  documentation {
    content = "Slow sql query."
  }
  notification_channels = [
    "${local.warning}",
    ]  
}

resource "google_monitoring_alert_policy" "alert_policy10" {
  display_name = "error logs ${var.app1} - ${var.project}"
  combiner = "OR"
  conditions {
    display_name = "logging/user/error_logs_janus [SUM]"
    condition_threshold {
      filter = "metric.type=\"logging.googleapis.com/user/error_logs_janus\" resource.type=\"gae_app\" resource.label.\"module_id\"=\"${var.app1}\"" 
      duration = "60s"
      comparison = "COMPARISON_GT"
      threshold_value = 10
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "60s"
        per_series_aligner = "ALIGN_MEAN"
        cross_series_reducer = "REDUCE_SUM"
      }
    }
  }
  documentation {
    content = "custom error logs ${var.app1} - ${var.project}."
  }
  notification_channels = [
    "${local.critical}",
    ]  
}

resource "google_monitoring_alert_policy" "alert_policy11" {
  display_name = "error logs ${var.app2} - ${var.project}"
  combiner = "OR"
  conditions {
    display_name = "logging/user/error_logs_aphrodite [SUM]"
    condition_threshold {
      filter = "metric.type=\"logging.googleapis.com/user/error_logs_aphrodite\" resource.type=\"gae_app\" resource.label.\"module_id\"=\"${var.app2}\"" 
      duration = "60s"
      comparison = "COMPARISON_GT"
      threshold_value = 10
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "60s"
        per_series_aligner = "ALIGN_MEAN"
        cross_series_reducer = "REDUCE_SUM"
      }
    }
  }
  documentation {
    content = "custom error logs ${var.app2} - ${var.project}."
  }
  notification_channels = [
    "${local.critical}",
    ]  
}

resource "google_monitoring_alert_policy" "alert_policy12" {
  display_name = "${var.app1} Health check - ${var.project}"
  combiner = "OR"
  conditions {
    display_name = "${var.app1} Health check - ${var.project}"
    condition_threshold {
      filter = "metric.type=\"monitoring.googleapis.com/uptime_check/check_passed\" resource.type=\"uptime_url\" metric.label.\"check_id\"=\"${var.uptimeapp1}\"" 
      duration = "60s"
      comparison = "COMPARISON_GT"
      threshold_value = 1
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "1200s"
        per_series_aligner = "ALIGN_NEXT_OLDER"
        cross_series_reducer = "REDUCE_COUNT_FALSE"
      }
    }
  }
  documentation {
    content = "${var.project} - ${var.uptimeapp1} > Down."
  }
  notification_channels = [
    "${local.critical}",
    ]  
}

resource "google_monitoring_alert_policy" "alert_policy13" {
  display_name = "Uptime check for ${var.app2} - ${var.project}"
  combiner = "OR"
  conditions {
    display_name = "${var.app2} Health check - ${var.project}"
    condition_threshold {
      filter = "metric.type=\"monitoring.googleapis.com/uptime_check/check_passed\" resource.type=\"gae_app\" metric.label.\"check_id\"=\"${var.uptimeapp2}\"" 
      duration = "60s"
      comparison = "COMPARISON_GT"
      threshold_value = 1
      trigger {
          count = 1
      }
      aggregations {
        alignment_period = "1200s"
        per_series_aligner = "ALIGN_NEXT_OLDER"
        cross_series_reducer = "REDUCE_COUNT_FALSE"
      }
    }
  }
  documentation {
    content = "${var.project} - ${var.uptimeapp2} > Down."
  }
  notification_channels = [
    "${local.critical}",
    ]  
}
