data "google_iam_policy" "viewer" {
  binding {
    role = "roles/storage.objectViewer"
    members = [
        "allUsers",
    ] 
  }
}

resource "google_storage_bucket_iam_policy" "editor" {
  bucket = "${google_storage_bucket.bucket.name}"
  policy_data = "${data.google_iam_policy.viewer.policy_data}"
}
resource "google_storage_bucket" "bucket" {
  name = "${var.project_id}-tf-test"
  storage_class = "standard"
  location = "US"
}
