output REDIS_HOST {
  description = "PRIVATE IP OF REDIS INSTANCE"
  value       = "${google_compute_instance.instance_with_ip.network_interface.0.network_ip}"
}
