resource "google_compute_instance" "instance_with_ip" {
  name         = "${var.project}-redis-vm"
  machine_type = "f1-micro"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    subnetwork = "${var.subnetwork}"
    access_config {
   }
  }

  metadata_startup_script = "sudo apt-get -y install redis-server && sudo sed -i 's/bind 127.0.0.1/bind 0.0.0.0/g' /etc/redis/redis.conf && sudo systemctl restart redis-server"

  provisioner "local-exec" {
    command = "gcloud beta service-directory endpoints create master --address ${google_compute_instance.instance_with_ip.network_interface.0.network_ip} --port 6379 --service redis --namespace weclikd --location us-central1"
    }  
}

