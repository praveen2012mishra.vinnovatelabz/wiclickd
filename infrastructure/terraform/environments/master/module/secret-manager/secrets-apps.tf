resource "google_secret_manager_secret" "secret-app-pwd" {
  provider = google-beta

  secret_id = "SQL_PASSWORD"

  replication {
    automatic = true
  }
}


resource "google_secret_manager_secret_version" "secret-version-app-pwd" {
  provider = google-beta

  secret = google_secret_manager_secret.secret-app-pwd.id

  secret_data = "${var.app_password}"
}