resource "google_pubsub_topic" "example" {
  name = "example-topic"
}

resource "google_pubsub_subscription" "example" {
  name  = "example-subscription"
  topic = google_pubsub_topic.example.name

  message_retention_duration = "604800s"
  retain_acked_messages      = true

  ack_deadline_seconds = 600

  expiration_policy {
    ttl = ""
  }
}