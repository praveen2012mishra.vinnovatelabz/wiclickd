// dev
//const GRAPHQL_URL = "https://janus-bgsfkvxhwq-uc.a.run.app/graphql";
// staging
const GRAPHQL_URL = "https://api.weclikd-beta.com/graphql";
//dev
//const WEBSITE = "https://localhost:19006/";
//staging
const WEBSITE = "https://www.weclikd-beta.com";

var plusCircleIcon = chrome.extension.getURL(
  "content/images/weclikd_icon_plus.png"
);
var editCircleIcon = chrome.extension.getURL("content/images/edit.png");
var heartCircleIcon = chrome.extension.getURL("content/images/heart1.png");
var chatCircleIcon = chrome.extension.getURL("content/images/chat-circle.png");
var ShareIcon = chrome.extension.getURL("content/images/share.png");
var wecklidLogo = chrome.extension.getURL("content/images/home.png");
var indexHtml = chrome.extension.getURL("index.html");

var like_type = ["NONE", "RED", "SILVER", "GOLD", "DIAMOND"];
var heartCount = 1;

const root = document.createElement("div");

root.className = "wecklid-extension-container";
var html = "";
html += `<div id="wecklid-discussion-div">
  <iframe src=${indexHtml} id="wecklid-discussion-iframe"></iframe>
  </div>`;
html += `<div id="wecklid-share-iframe">
  <div id="share-header">  
  <input type="button" value = "x" id="share-cross"/>
    <div style="width:395px;">
      <h4 id="share-title">Share Post</h4>
      </div>
  </div>
  <span class= 'usernameTopic'>
    <span class='usernameClass'>
      <p class='appendUsernameTxt'>@username</p>
      <input class= 'appendBtn' type='button' value ='x'/>
    </span>

    <span class='topicClass'>
      <p class='appendTopicTxt'>/topic</p>
      <input class= 'appendBtn' type='button' value ='x'/>
    </span>
  </span>

  <span id="addedSpan"></span>

  <div>
    <div id="clickSpan2"  class ="spanClass"></div>
  </div>
  <div class="searchBody">
    <input type="text" class="form-control" id="txtInput"    
    placeholder="Share to @users, /topics and #cliks" name="email"> 
  </div> 

  <div id="clickMain" class="clickMainDiv">
      <input type="submit" value = "#clik-1" id="clik-1" class='clik' />    
      <input type="submit" value = "#clik-2" id="clik-2" class='clik'/>
  </div>
  <div id="clickUsername" class="clickMainDiv">
      <input type="submit" value = "@username-1" id="username-1" class='username' />    
      <input type="submit" value = "@username-2" id="username-2" class='username'/>
  </div>
  <div id="clickTopic" class="clickMainDiv">
  <input type="submit" value = "/topic-1" id="topic-1" class='topic' />    
  <input type="submit" value = "/topic-1" id="topic-2" class='topic'/>
  </div>
  
  <input type="submit" id="sharebutton" value="Submit">
  </div>`;
html += `<iframe src=${WEBSITE} style="opacity: 0;width: 1px;height: 1px;position: absolute;"></iframe>`;
// circle menu html
/* <a class="floating-btn">
<i class="fa fa-plus"></i> 
</a> */

html += `<div id="circularMenu" class="circular-menu">
  <img src=${plusCircleIcon} class="floating-btn"/>
  <menu class="items-wrapper">
    <img class="wecklid-menu-item" src=${heartCircleIcon} id="heartLogo"/>
    <img class="wecklid-menu-item" src=${chatCircleIcon} id="chatLogo"/>
    <img class="wecklid-menu-item" src=${ShareIcon} id="ShareLogo"/>
    <img class="wecklid-menu-item" src=${wecklidLogo} id="wecklidLogo" />
  </menu>
</div>`;

root.innerHTML = html;
document.body.appendChild(root);
//-------------------------------------- handle main website login -----------------------------------
window.addEventListener(
  "message",
  event => {
    if (event.data.type == "wecklid_login") {
      chrome.storage.local.set({ isLoggedIn: true });
      chrome.runtime.sendMessage({
        type: "wecklid_login",
        userIdTokenFirebase: event.data.userIdTokenFirebase,
        UserName: event.data.UserName
      });
    }
    // if (event.data.type == "wecklid_logout") {
    //   chrome.storage.local.remove("isLoggedIn", () => {});
    //   chrome.runtime.sendMessage({
    //     type: "wecklid_logout"
    //   });
    // }
  },
  false
);
//--------------------------------------------------------------------------------
chrome.runtime.onMessage.addListener(function(action, sender, response) {
  if (action.type === "login") {
    $("#circularMenu").fadeIn(300);
  }
  if (action.type === "logout") {
    $("#circularMenu").fadeOut(300);
  }
  if (action.type === "show_icon") {
    $("#circularMenu").fadeIn(300);
  }
  if (action.type === "hide_icon") {
    $("#circularMenu").fadeOut(300);
  }
  if (action.type === "show_share_modal") {
    $("#wecklid-share-iframe").show(300);
  }
});
chrome.storage.onChanged.addListener(function(changes, namespace) {
  for (var key in changes) {
    var storageChange = changes[key];
    console.log(
      'Storage key "%s" in namespace "%s" changed. ' +
        'Old value was "%s", new value is "%s".',
      key,
      namespace,
      storageChange.oldValue,
      storageChange.newValue
    );
  }
});
// chrome.runtime.onMessageExternal.addListener(
//   function(request, sender, sendResponse) {
//     alert(request.myName);
//   });
$(document).ready(() => {
  // var like_type = ["NONE", "RED", "SILVER", "GOLD", "DIAMOND"];
  // var heartCount = 1;
  $("#heartLogo").click(() => {
    if (heartCount < 5) heartCount++;
    else heartCount = 1;
    let newUrl = chrome.extension.getURL(
      `content/images/heart${heartCount}.png`
    );
    $("#heartLogo").attr("src", newUrl);

    chrome.storage.local.get("userIdTokenFirebase", data => {
      if ("userIdTokenFirebase" in data) {
        let { userIdTokenFirebase } = data;
        let url = window.location.href;
        if (url.startsWith("https") == false) {
          alert("All links must start with https.");
          return false;
        }
        if (isUrl(url) == false) {
          alert("Not a valid URL");
          return false;
        }
        var query = `query GetUrlInfo($url: String!) {
        url_info(url: $url) {
            status
            post {
                  id
                  author {
                      id
                      username
                      profile_pic
                      banner_pic
                      full_name
                      description
                  }
                  text
                  likes_score
                  comments_score
                  reports_score
                  title
                  summary
                  created
                  thumbnail_pic
                  pictures
                  topics
                  cliks
                  link
                  user_like_type
                  external_feed {
                    icon_url
                    id
                  }
              
            }
            unfurl {
              title
              summary
              thumbnail_url
              normalized_url
            }
          }
        }
      `;
        try {
          fetch(GRAPHQL_URL, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              Accept: "application/json",
              Authorization: "Bearer " + userIdTokenFirebase
            },
            body: JSON.stringify({
              query,
              variables: { url }
            })
          })
            .then(r => r.json())
            .then(async res => {
              if (res.data.url_info.status == "UNFURL_SUCCESS") {
                let description = res.data.url_info.unfurl.summary;
                let image = res.data.url_info.unfurl.thumbnail_url;
                let title = res.data.url_info.unfurl.title;
                let url = res.data.url_info.unfurl.normalized_url;
                createPost(description, image, title, url);
              } else if (res.data.url_info.status == "ALREADY_SHARED") {
                console.log(res.data.url_info.post.id);
                likePost(
                  res.data.url_info.post.id,
                  like_type[heartCount == 1 ? 0 : heartCount - 1]
                );
              } else {
                let description = "";
                let image = "";
                let title = "";
                let url = link;
                createPost(description, image, url, url);
              }
            });
        } catch (e) {
          console.log(e);
          let description = "";
          let image = "";
          let title = "";
          let url = link;
          createPost(description, image, url, url);
        } finally {
          // toogleFloatingBtn();
        }
      }
    });
  });
  chrome.storage.local.get("isShowIcon", data => {
    if ("isShowIcon" in data) {
      if (data.isShowIcon) {
        $("#circularMenu").fadeIn(300);
      } else {
        $("#circularMenu").fadeOut(300);
      }
    }
  });

  chrome.storage.local.get("isLoggedIn", data => {
    if ("isLoggedIn" in data && data.isLoggedIn) {
      $("#circularMenu").fadeIn(300);
    } else {
      $("#circularMenu").fadeOut();
    }
  });

  $(".floating-btn").click(function() {
    chrome.storage.local.get("isLoggedIn", data => {
      if (data.isLoggedIn) {
        $("#circularMenu").toggleClass("active");
        if ($(".floating-btn").attr("src") === editCircleIcon) {
          getCurrentUrlInfo();
        } else {
          toogleFloatingBtn();
        }
      }
    });
  });

  // circle menu collapse handle when click outside of wecklid-extension-container
  $(document).click(e => {
    var container = $(".wecklid-extension-container");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      $("#circularMenu").removeClass("active");
      $(".floating-btn").attr("src", plusCircleIcon);
      $("#wecklid-discussion-iframe").hide(300);
      $("#wecklid-share-iframe").hide(300);
    }
  });

  // Go to wecklid page
  $("#wecklidLogo").click(() => {
    window.open("https://www.weclikd-beta.com/", "_blank");
  });

  // when user click chat logo then open discussion iframe
  $("#chatLogo").click(() => {
    //circle menu collapse
    $("#circularMenu").toggleClass("active");
    toogleFloatingBtn();

    //discussion iframe show
    $("#wecklid-discussion-iframe").show(300);
  });

  $("#ShareLogo").click(() => {
    //circle menu collapse
    $("#circularMenu").toggleClass("active");
    toogleFloatingBtn();
    //discussion iframe show
    $("#wecklid-share-iframe").show(300);
    $('#clickMain').hide(); 
    $('#clickUsername').hide(); 
    $('#clickTopic').hide();  
  });

  $("#sharebutton").click(() => {
    $("#wecklid-share-iframe").hide(300);
  });

  $("#share-cross").click(() => {    
    $("#wecklid-share-iframe").hide(300);
    
    $(document).find('#addedSpan').hide()
  });

  //------------show suggestions on typing-----------------

  $("#txtInput").on('input',() => {
    var str = $("#txtInput").val();
    if(str.charAt(0) == '#'){
      $('#clickMain').show()  
    } else if (str.charAt(0) == '@'){
      $('#clickUsername').show()  
    } else if (str.charAt(0) == '/'){
      $('#clickTopic').show()  
    } else if (str == ''){
      $('#clickMain').hide(); 
      $('#clickUsername').hide(); 
      $('#clickTopic').hide();  
    } 
  });

  //------------added append texts for clik -----------------
  $("#clik-1").click(() => {
    $("#txtInput").val("");        
  $("#addedSpan").append("<div class='appendClass' id='clik-span1'><p class='appendTxt'>#clik-1</p><input class= 'appendBtn' type='submit' value ='x' id='clik-1Btn'/></div>")
    $('#clickMain').hide(); 
    
  });
  $("#clik-2").click(() => {
    $("#txtInput").val("");
    $("#addedSpan").append(" <span class='appendClass' id='clik-span2'><p class='appendTxt'>#clik-2</p><input class= 'appendBtn' type='submit' value ='x' id='clik-2Btn'/></span>")
    $('#clickMain').hide(); 
  });

//------------on close for clik append-----------------

  $(document).on('click', '#clik-1Btn', function () {
    $("#clik-span1").hide();
  });
  $(document).on('click', '#clik-2Btn', function () {
    $("#clik-span2").hide();
  });

  //------------added append texts for username -----------------
  $("#username-1").click(() => {
    $("#txtInput").val("");    
    $("#addedSpan").append(" <span class='usernameClass' id='username-span1'><p class='appendUsernameTxt'>@username-1</p><input class= 'appendBtn' type='button' value ='x'/id='username-1Btn'></span>")    
    $('#clickUsername').hide();     
  });
  $("#username-2").click(() => {
    $("#txtInput").val("");
    $("#addedSpan").append(" <span class='usernameClass'  id='username-span2'><p class='appendUsernameTxt'>@username-2</p><input class= 'appendBtn' type='button' value ='x'/id='username-2Btn'></span>")
    $('#clickUsername').hide(); 
  });


  //------------on close for username append-----------------

  $(document).on('click', '#username-1Btn', function () {
    $("#username-span1").hide();
  });
  $(document).on('click', '#username-2Btn', function () {
    $("#username-span2").hide();
  });
  //------------added append texts for topic -----------------

  $("#topic-1").click(() => {
    $("#txtInput").val("");
    $("#addedSpan").append(" <span class='topicClass' id='topic-span1'><p class='appendTopicTxt'>/topic-1</p><input class= 'appendBtn' type='button' value ='x' id='topic-1Btn'/></span>")
    $('#clickTopic').hide(); 
  });
  $("#topic-2").click(() => {
    $("#txtInput").val("");
    $("#addedSpan").append(" <span class='topicClass' id='topic-span2'><p class='appendTopicTxt'>/topic-2</p><input class= 'appendBtn' type='button' value ='x' id='topic-2Btn'/></span>")
    $('#clickTopic').hide(); 
  });

  //------------on close for username append-----------------

  $(document).on('click', '#topic-1Btn', function () {
    $("#topic-span1").hide();
  });
  $(document).on('click', '#topic-2Btn', function () {
    $("#topic-span2").hide();
  });

  let sharebox = document.querySelector("input");
  let suggestionbox = document.getElementById("suggestion-box");
  sharebox.addEventListener("change", handleChange);

  function handleChange(e) {
    console.log(e);
    suggestionbox.textContent = e.target.value;
  }

  $(".floating-btn").mousedown(e => {
    e = e || window.event;
    e.preventDefault();
    $(document).mousemove(event => {
      event = event || window.event;
      event.preventDefault();

      $(".wecklid-extension-container").offset({
        top: event.pageY - 10,
        left: event.pageX - 15
      });
    });
  });
  $("#circularMenu").on("mouseup", e => {
    $(document).off("mousemove");
  });

  $("#share-header").mousedown(e => {
    e = e || window.event;
    e.preventDefault();
    $(document).mousemove(event => {
      event = event || window.event;
      event.preventDefault();

      $("#wecklid-share-iframe").offset({
        top: event.pageY - 10,
        left: event.pageX - 15
      });
    });
  });
  $("#share-header").on("mouseup", e => {
    $(document).off("mousemove");
  });

  $("#wecklid-discussion-iframe").mousedown(e => {
    e = e || window.event;
    e.preventDefault();
    $(document).mousemove(event => {
      event = event || window.event;
      event.preventDefault();

      $("#wecklid-discussion-div").offset({
        top: event.pageY - 10,
        left: event.pageX - 15
      });
    });
  });
  $("#wecklid-discussion-iframe").on("mouseup", e => {
    $(document).off("mousemove");
  });

  // var iframeBody = $("#wecklid-discussion-iframe").contentWindow.document;
  // console.log(iframeBody);
  // $(iframeBody).on("click", "#discussion-close-btn", function(event) {
  //   console.log("called");
  //   $("#wecklid-discussion-iframe").hide(300);
  // });
});
function isUrl(s) {
  var regexp = /(https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
  return regexp.test(s);
}
function getCurrentUrlInfo() {
  chrome.storage.local.get("userIdTokenFirebase", data => {
    if ("userIdTokenFirebase" in data) {
      let { userIdTokenFirebase } = data;
      let url = window.location.href;
      if (url.startsWith("https") == false) {
        alert("All links must start with https.");
        return false;
      }
      if (isUrl(url) == false) {
        alert("Not a valid URL");
        return false;
      }
      var query = `query GetUrlInfo($url: String!) {
      url_info(url: $url) {
          status
          post {
                id
                author {
                    id
                    username
                    profile_pic
                    banner_pic
                    full_name
                    description
                }
                text
                likes_score
                comments_score
                reports_score
                title
                summary
                created
                thumbnail_pic
                pictures
                topics
                cliks
                link
                user_like_type
                external_feed {
                  icon_url
                  id
                }
            
          }
          unfurl {
            title
            summary
            thumbnail_url
            normalized_url
          }
        }
      }
    `;
      try {
        fetch(GRAPHQL_URL, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: "Bearer " + userIdTokenFirebase
          },
          body: JSON.stringify({
            query,
            variables: { url }
          })
        })
          .then(r => r.json())
          .then(async res => {
            if (res.data.url_info.status == "UNFURL_SUCCESS") {
              let description = res.data.url_info.unfurl.summary;
              let image = res.data.url_info.unfurl.thumbnail_url;
              let title = res.data.url_info.unfurl.title;
              let url = res.data.url_info.unfurl.normalized_url;
              window.open(
                `https://www.weclikd-beta.com/post?description=${description}&image=${image}&title=${title}&url=${url}`,
                "_blank"
              );
            } else if (res.data.url_info.status == "ALREADY_SHARED") {
              console.log(res.data.url_info.post.id);
              window.open(
                `https://www.weclikd-beta.com/post/${res.data.url_info.post.id.replace(
                  "Post:",
                  ""
                )}`,
                "_blank"
              );
            } else {
              let description = "";
              let image = "";
              let title = "";
              let url = url;
              window.open(
                `https://www.weclikd-beta.com/post?description=${description}&image=${image}&title=${title}&url=${url}`,
                "_blank"
              );
            }
          });
      } catch (e) {
        console.log(e);
        let description = "";
        let image = "";
        let title = "";
        let url = url;
        window.open(
          `https://www.weclikd-beta.com/post?description=${description}&image=${image}&title=${title}&url=${url}`,
          "_blank"
        );
      } finally {
        toogleFloatingBtn();
      }
    }
  });
}
function toogleFloatingBtn() {
  if ($(".floating-btn").attr("src") === editCircleIcon) {
    $(".floating-btn").attr("src", plusCircleIcon);
  } else {
    $(".floating-btn").attr("src", editCircleIcon);
  }
}

function likePost(PostId, like_type) {
  chrome.storage.local.get("userIdTokenFirebase", data => {
    if ("userIdTokenFirebase" in data) {
      let { userIdTokenFirebase } = data;
      var query = `mutation LikeContent($content_id: ID!, $like_type: LikeType!) {
        content_like(like_input: { content_id: $content_id, type: $like_type }) {
          status
          remaining {
            gold
            diamond
          }
        }
      }
    `;
      try {
        fetch(GRAPHQL_URL, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: "Bearer " + userIdTokenFirebase
          },
          body: JSON.stringify({
            query,
            variables: { content_id: PostId, like_type: like_type }
          })
        })
          .then(r => r.json())
          .then(async res => {});
      } catch (e) {
      } finally {
        // toogleFloatingBtn();
      }
    }
  });
}
function createPost(description, image, title, url) {
  chrome.storage.local.get("userIdTokenFirebase", data => {
    if ("userIdTokenFirebase" in data) {
      let { userIdTokenFirebase } = data;
      var query = `mutation CreatePost(
        $title: String!
        $summary: String!
        $text: String
        $thumbnail_pic: String
        $thumbnail_pic_url: String
        $pictures: [String]
        $topics: [String]
        $cliks: [String]
        $link: String
      ) {
        post_create(
          post_input: {
            title: $title
            summary: $summary
            text: $text
            thumbnail_pic: $thumbnail_pic
            thumbnail_pic_url: $thumbnail_pic_url
            pictures: $pictures
            topics: $topics
            cliks: $cliks
            link: $link
          }
        ) 
          {
            id
            author {
                id
                username
                profile_pic
                banner_pic
                full_name
                description
            }
            text
            likes_score
            comments_score
            reports_score
            title
            summary
            created
            thumbnail_pic
            pictures
            topics
            cliks
            link
            user_like_type
            external_feed {
              icon_url
              id
            }
            
        }
      }
    `;
      try {
        fetch(GRAPHQL_URL, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: "Bearer " + userIdTokenFirebase
          },
          body: JSON.stringify({
            query,
            variables: {
              title: title,
              summary: description,
              text: "",
              thumbnail_pic: "",
              thumbnail_pic_url: image,
              pictures: [],
              topics: [],
              link: url,
              cliks: []
            }
          })
        })
          .then(r => r.json())
          .then(async res => {
            console.log(res);
            likePost(
              res.data.post_create.id,
              like_type[heartCount == 1 ? 0 : heartCount - 1]
            );
          });
      } catch (e) {
      } finally {
        // toogleFloatingBtn();
      }
    }
  });
}
