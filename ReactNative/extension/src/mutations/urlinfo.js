import gql from "graphql-tag";
import { postFields } from "./GraphQlFragment";

export const UrlInfoMutation = gql`
  query GetUrlInfo($url: String!) {
    url_info(url: $url) {
      status
      post {
        ...postFields
      }
      unfurl {
        title
        summary
        thumbnail_url
        normalized_url
      }
    }
  }
  ${postFields}
`;

export const UrlInfoVariables = {
  variables: {
    url: ""
  }
};
