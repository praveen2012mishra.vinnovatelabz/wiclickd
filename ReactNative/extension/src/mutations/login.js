import gql from "graphql-tag";
import {
  userFields,
  topicFields,
  clikFields,
  externalFeedFields
} from "./GraphQlFragment";

export const UserLoginMutation = gql`
  mutation Login {
    login {
      status {
        success
        status
        custom_status
        user_msg
      }
      account {
        id
        first_name
        last_name
        email
        settings {
          subscription
          privacy {
            dm_settings
          }
          email_notifications {
            notification_email
            monthly_earnings
            clik_notifications
            weclikd_updates
          }
        }
        created
        my_users {
          id
          user {
            ...userFields
          }
          users_followed {
            settings {
              follow_type
            }
            user {
              ...userFields
            }
            created
          }
          topics_followed {
            settings {
              follow_type
            }
            topic {
              ...topicFields
            }
            created
          }
          cliks_followed {
            settings {
              follow_type
            }
            member_type
            clik {
              ...clikFields
            }
            created
          }
          feeds_followed {
            settings {
              follow_type
            }
            feed {
              ...externalFeedFields
            }
            created
          }
        }
      }
    }
  }
  ${userFields}
  ${topicFields}
  ${clikFields}
  ${externalFeedFields}
`;
