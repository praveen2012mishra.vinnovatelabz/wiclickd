import gql from "graphql-tag";

export const __PageInfoField = gql`
  fragment __PageInfoField on CommentConnection {
    pageInfo {
      hasNextPage
      hasPreviousPage
      startCursor
      endCursor
    }
  }
`;

export const userFields = gql`
  fragment userFields on User {
    id
    username
    profile_pic
    banner_pic
    full_name
    description
  }
`;

export const clikFields = gql`
  fragment clikFields on Clik {
    id
    name
    banner_pic
    description
    num_followers
    num_members
    website
    qualifications
    max_members
  }
`;

export const postFields = gql`
  fragment postFields on Post {
    id
    author {
      ...userFields
    }
    text
    likes_score
    comments_score
    reports_score
    title
    summary
    created
    thumbnail_pic
    pictures
    topics
    cliks
    link
    user_like_type
    external_feed {
      icon_url
      id
    }
  }
  ${userFields}
`;

export const commentFields = gql`
  fragment commentFields on Comment {
    id
    author {
      ...userFields
    }
    text
    created
    likes_score
    comments_score
    reports_score
    clik
  }
  ${userFields}
`;

export const topicFields = gql`
  fragment topicFields on Topic {
    id
    name
    banner_pic
    description
    parents
    num_followers
  }
`;

export const statusFields = gql`
  fragment statusFields on WeclikdStatus {
    success
    status
    custom_status
    user_msg
    debug_error_msg
  }
`;

export const externalFeedFields = gql`
  fragment externalFeedFields on ExternalFeed {
    id
    name
    url
    icon_url
    base_topic
    created
  }
`;
