import gql from "graphql-tag";
import {commentFields } from "./GraphQlFragment";

export const CreateCommentMutation = gql`
  mutation CreateComment(
    $text: String
    $parent_content_id: ID!
    $clik: String
  ) {
    comment_create(
      input: { text: $text, parent_content_id: $parent_content_id, clik: $clik }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
      comment {
        ...commentFields
      }
    }
  }
  ${commentFields}
`;
