import React from "react";
import { Route, Redirect } from "react-router-dom";
import { isLoggedIn } from "../library/Helper";
export default function PrivateRoute({ children, ...rest }) {
    return (<Route {...rest} render={() => {
        return isLoggedIn() ? (children) : (<Redirect to="/" />)
    }} />)
}