import { ApolloClient, from, InMemoryCache } from "apollo-client-preset";
import { setContext } from "apollo-link-context";
import { createHttpLink } from "apollo-link-http";
import {
  getFirebaseToken,
  getMyUserId,
  getWeclikdToken
} from "./library/Helper";
import { IntrospectionFragmentMatcher } from "apollo-cache-inmemory";
import introspectionQueryResultData from "./fragmentTypes.json";
import fetch from "node-fetch";

const apiUrl = {
  //dev
  GRAPHQL_URL: "https://janus-bgsfkvxhwq-uc.a.run.app/graphql"
  // staging
  // GRAPHQL_URL: "https://api.weclikd-beta.com/graphql"
};

const httpLink = createHttpLink({
  uri: apiUrl.GRAPHQL_URL,
  fetch: fetch
});

const auth = async () => {
  const firebaseToken = await getFirebaseToken();
  const tempToken = await getWeclikdToken();
  const tempCurrentUserId = await getMyUserId();
  let weclikdToken = tempToken ? tempToken : null;
  let CurrentUserId = tempCurrentUserId ? tempCurrentUserId : null;
  return await {
    firebaseToken,
    weclikdToken,
    CurrentUserId
  };
};

const authMiddleware = setContext(operation =>
  auth().then(token => {
    let setHeader = {};
    if (token.firebaseToken) {
      setHeader = {
        ...setHeader,
        Authorization: "Bearer " + token.firebaseToken
      };
    }
    if (token.weclikdToken) {
      setHeader = {
        ...setHeader,
        "X-Weclikd-Access-Key": token.weclikdToken
      };
    }
    if (token.CurrentUserId) {
      setHeader = {
        ...setHeader,
        CurrentUserId: token.CurrentUserId
      };
    }
    return {
      headers: {
        ...setHeader
      }
    };
  })
);

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData
});

// const applloClient = new ApolloClient({
//   link: from([authMiddleware, httpLink]),
//   cache: new InMemoryCache(
//     {
//       dataIdFromObject: obj => obj.id,
//       addTypename: false,
//       fragmentMatcher: {
//         match: ({ id }, typeCond, context) => !!context.store.get(id)
//       }
//     }
//   ),
// });

const applloClient = new ApolloClient({
  link: from([authMiddleware, httpLink]),
  cache: new InMemoryCache({ fragmentMatcher })
});

export default applloClient;
