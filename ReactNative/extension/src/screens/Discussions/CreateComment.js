/*global chrome*/
import React, { Component } from 'react';
import {CreateCommentMutation} from '../../mutations/submitComment'
import applloClient from '../../client'
import { compose } from 'recompose';


const GRAPHQL_URL = "https://janus-bgsfkvxhwq-uc.a.run.app/graphql";
// staging
// const GRAPHQL_URL = "https://api.weclikd-beta.com/graphql";

class CreateComment extends Component {

    constructor(props){
        super(props)
        this.state={
            comment:""
        }
    }

    isUrl = (s) => {        
        var regexp = /(https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        return regexp.test(s);
      }
    
      getPostidFromURL = async() =>{ 
        let that = this   
        chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {            
      
        chrome.storage.local.get("userIdTokenFirebase", data => {      
          if ("userIdTokenFirebase" in data) {
            let { userIdTokenFirebase } = data;
            let url = url;
            if (tabs[0].url.startsWith("https") == false) {
              alert("All links must start with https.");
              return false;
            }
            if (that.isUrl(tabs[0].url) == false) {
              alert("Not a valid URL");
              return false;
            }
            var query = `query GetUrlInfo($url: String!) {
            url_info(url: $url) {
                status
                post {
                      id
                      author {
                          id
                          username
                          profile_pic
                          banner_pic
                          full_name
                          description
                      }
                      text
                      likes_score
                      comments_score
                      reports_score
                      title
                      summary
                      created
                      thumbnail_pic
                      pictures
                      topics
                      cliks
                      link
                      user_like_type
                      external_feed {
                        icon_url
                        id
                      }
                  
                }
                unfurl {
                  title
                  summary
                  thumbnail_url
                  normalized_url
                }
              }
            }
          `;
            try {
                let url = tabs[0].url                             
                fetch(GRAPHQL_URL, {
                    method: "POST",
                    headers: {
                    "Content-Type": "application/json",
                    Accept: "application/json",
                    Authorization: "Bearer " + userIdTokenFirebase
                    },
                    body: JSON.stringify({
                    query,
                    variables: { url }
                    })                
                })
                .then(r => 
                    r.json(),
                )
                .then(async res => {                  
                  if (res.data.url_info.status == "UNFURL_SUCCESS") {
                    let description = res.data.url_info.unfurl.summary;
                    let image = res.data.url_info.unfurl.thumbnail_url;
                    let title = res.data.url_info.unfurl.title;
                    let url = res.data.url_info.unfurl.normalized_url;
                    this.createPost(description, image, title, url);
                  } else if (res.data.url_info.status == "ALREADY_SHARED") {                                        
                    that.submitMainComment (res.data.url_info.post.id)
                  } else {
                    let description = "";
                    let image = "";
                    let title = "";
                    let url = tabs[0].url;
                    this.createPost(description, image, url, url);
                  }
                });
            } catch (e) {
            //   console.log(e);
              let description = "";
              let image = "";
              let title = "";
              let url = tabs[0].url;
              this.createPost(description, image, url, url);
            } finally {
              // toogleFloatingBtn();
            }
          }
        });
      })
      }
      createPost(description, image, title, url) {
          let that = this
        chrome.storage.local.get("userIdTokenFirebase", data => {
          if ("userIdTokenFirebase" in data) {
            let { userIdTokenFirebase } = data;
            var query = `mutation CreatePost(
              $title: String!
              $summary: String!
              $text: String
              $thumbnail_pic: String
              $thumbnail_pic_url: String
              $pictures: [String]
              $topics: [String]
              $cliks: [String]
              $link: String
            ) {
              post_create(
                post_input: {
                  title: $title
                  summary: $summary
                  text: $text
                  thumbnail_pic: $thumbnail_pic
                  thumbnail_pic_url: $thumbnail_pic_url
                  pictures: $pictures
                  topics: $topics
                  cliks: $cliks
                  link: $link
                }
              ) 
                {
                  id
                  author {
                      id
                      username
                      profile_pic
                      banner_pic
                      full_name
                      description
                  }
                  text
                  likes_score
                  comments_score
                  reports_score
                  title
                  summary
                  created
                  thumbnail_pic
                  pictures
                  topics
                  cliks
                  link
                  user_like_type
                  external_feed {
                    icon_url
                    id
                  }
                  
              }
            }
          `;
            try {
              fetch(GRAPHQL_URL, {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                  Accept: "application/json",
                  Authorization: "Bearer " + userIdTokenFirebase
                },
                body: JSON.stringify({
                  query,
                  variables: {
                    title: title,
                    summary: description,
                    text: "",
                    thumbnail_pic: "",
                    thumbnail_pic_url: image,
                    pictures: [],
                    topics: [],
                    link: url,
                    cliks: []
                  }
                })
              })
                .then(r => r.json())
                .then(async res => {                  
                  that.submitMainComment (res.data.post_create.id)
                });
            } catch (e) {
            } finally {
              // toogleFloatingBtn();
            }
          }
        });
      }

      
    submitMainComment = (parentid) =>{
        applloClient
            .query({
            query: CreateCommentMutation,
            variables: {
                text: this.state.comment,
                parent_content_id : parentid,
                clik: null
            },
            fetchPolicy: "no-cache"
            })
            .then(response => {
            //   console.log(response, "response");
              this.setState({comment:""})
            })
            .catch(e => {
            //   console.log(e);
        });
    }
    submitComment = async() =>{        
        if (this.state.comment && this.props.initial == "child" ) {                      
        await applloClient
        .query({
          query: CreateCommentMutation,
          variables: {
            text: this.state.comment,
            parent_content_id : this.props.parent_content_id,
            clik: null
          },
          fetchPolicy: "no-cache"
        })
        .then(response => {
        //   console.log(response, "response");
          this.setState({comment:""})
        })
        .catch(e => {
        //   console.log(e);
        });    
            this.props.modalVisibleFunc();  
        }
        if (this.state.comment && this.props.initial == "main"){
            this.getPostidFromURL()            
        }
    }

    render() {
        return (
            <div
                style={{
                    borderWidth: 1,
                    borderColor: "#e1e1e1",
                    borderRadius: 6,
                    margin: 8,
                    borderStyle: "solid",
                    marginTop: 20
                }}
            >
                <div
                    style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        marginTop: 10
                    }}
                >
                    <div
                        style={{
                            fontSize: 16,
                            fontWeight: "bold"
                        }}
                    >
                        Discuss With :{" "}
                    </div>
                    <div className="selectHead">
                        <select className="selectBox">
                            <option>Everyone</option>
                        </select>
                    </div>
                </div>
                <div style={{ padding: 8 }}>
                    <textarea
                        className="textarea"
                        placeholder="Write an insightful comment."
                        value= {this.state.comment}
                        onChange={(e) => this.setState({comment: e.target.value})}
                    ></textarea>
                </div>
                <div
                    style={{
                        display: "flex",
                        justifyContent: "space-between",
                        padding: 8
                    }}
                >
                    <div
                        style={{
                            fontSize: 16,
                            fontWeight: "bolder",
                            color: "#959FAA"
                        }}
                    >
                        @ {localStorage.getItem("UserName")}
                    </div>
                    <div>
                        <button className="btn-submit" onClick={()=>this.submitComment()}>Submit</button>
                    </div>
                </div>
            </div>
        )
    }
}
export default CreateComment