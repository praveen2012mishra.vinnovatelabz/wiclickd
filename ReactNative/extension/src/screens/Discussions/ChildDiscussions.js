import React, { PureComponent, lazy, Suspense } from 'react';
import PostDetailsCommentSwaper from './PostDetailsCommentSwaper';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'
import "./style.css";
import CreateComment from './CreateComment';

const params = {
  prevArrow: <div style={{width: "30px", marginRight: "-30px"}}>
    <img src={require('../../assets/left.png')} style={{ width: 20, height: 20 }}/>
    </div>,
  nextArrow: <div style={{width: "30px", marginLeft: "-30px"}}>
    <img src={require('../../assets/right.png')} style={{ width: 20, height: 20 }}/>
    </div>
}

class ChildDiscussions extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      setIndex: 0,
      modalVisible: false,
      parent_content_id: ""
    }
    this.swiperRef = React.createRef();
    this.slideRef = React.createRef();
    this.postDetailsCommentSwaperRef = {};

  }

  getcomment = () => {
    var comment = <div style ={{width: '100%'}}/>;
    if (this.props.commentData.length > 0) {
      comment = this.getSwiper();
    }
    return comment;
  };

  setModal = (id) =>{
    this.setState({modalVisible:!this.state.modalVisible}, ()=>{      
      this.setState({parent_content_id : id})
    })
  }

  getSwiper = () => {    
    
    var swiper = (
       
    <div style ={{width: '100%'}} >
      <Slide 
        {...params}
        arrows={true} 
        ref={this.slideRef} 
        autoplay={false} 
        onChange = { ()=>this.slideChange()}        
      >
      {this.props.commentData.map((item, i) => {
        return (            
            <PostDetailsCommentSwaper
              onRef={el =>
                (this.postDetailsCommentSwaperRef[
                  item.node.id.replace("Comment:", "")
                ] = el)
              }
              nativeID={item.node.id.replace("Comment:", "")}
              key={i}
              index={i}
              item={item}
              navigation={this.props.navigation}
              clickList={item.node.clik ? [item.node.clik] : null}
              parent_content_id={item.node.id}
              closeModalBySubmit={this.onSubmit}
              calHeight={this.calHeightModi}
              margin={this.props.isFirstComment == true ? 0 : 30}
              modalVisibleFunc={this.setModal}
              editmodalVisible={this.setEditModal}
              swiperRef={this.swiperRef}
              modalStatus={this.state.modalVisible}
              setGesture={this.setGesture}              
            />          
        );
      })
    }
    </Slide>    
    </div>    
    )
    return swiper        
  };

  slideChange = () => {        
    this.setState({setIndex: this.slideRef.current.state.index})
  }


  getrecursiveCommentThread = () => {
    var recursiveCommentThread = <div />;
    if (this.props.commentData.length > 0) {
      recursiveCommentThread = (
        <ChildDiscussions
          commentData={
            this.props.commentData[this.state.setIndex].comments.edges.length >
              0
              ? this.props.commentData[this.state.setIndex].comments.edges
              : []
          }
          currentIndex={this.state.setIndex}
          closeModalBySubmit={this.onSubmit}
          navigation={this.props.navigation}
          isFirstComment={false}
        />
      );
    }
    return recursiveCommentThread;
  };

  render() {    
    return (
      <div style ={{width: '100%'}} >
        {this.getcomment()}
        {this.state.modalVisible == false ? (
          this.getrecursiveCommentThread()
        ) : (
            <div>
              <CreateComment 
                initial="child" 
                parent_content_id={this.state.parent_content_id}
                modalVisibleFunc={this.setModal} /> 
            </div>
           )} 
      </div>
    )
  }
}
export default ChildDiscussions