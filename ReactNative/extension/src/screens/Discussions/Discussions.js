/*global chrome*/
import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimes,
  faComments,
  faHeart,
  faBug,
} from "@fortawesome/free-solid-svg-icons";
import "./style.css";
import $ from "jquery";
import ChildDiscussions from './ChildDiscussions';
import CreateComment from './CreateComment';

// dev
const API_URL = "https://janus-bgsfkvxhwq-uc.a.run.app/";
// staging
// const API_URL = "https://api.weclikd-beta.com/";

class Discussions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listCommentList: [],
      closeDiv: false,
      slideCount: 5,
      options: {
        navigation: true,
        pagination: true,
        paginationClickable: true,
        scrollBar: false,
        loop: false
      },
      
    };
  }
  closeFrame = () => {
    $(document).on('click', '#discussion-close-btn', function () {
      var data1 = document.getElementById("wecklid-discussion-iframe");
      var data2 = document.getElementById("wecklid-discussion-div");
      var iframes = document.getElementsByTagName("iframe");      
      $("#wecklid-discussion-iframe").hide(300);
      $("#wecklid-discussion-div").hide(300);
    });
  };
  componentDidMount = () => {
    $("#discussions-container").show(300);
    this.getCommentList();       
  };

  getCommentList = async () => {
    let that = this;
    chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {      
      fetch(API_URL + "v1/comments/?url=" + tabs[0].url)
        .then(response =>          
          response.json(),    
                
        ).then(async res => {
          let l1 = res.data.comments.edges;
          await that.setState({
            listCommentList: l1
          });
          
          // await this.setState({
          //   listCommentList: []
          // });

        });
    });
  };
  

  render() { 
    const { slides } = this.state  
    return (
      <div className="discussions-container" id="discussions-container">
        <header
          style={{
            position: "sticky",
            top: 0,
            left: 0,
            right: 0,
            zIndex: 10,
            overflow: "hidden",
            borderRadius: 15
          }}
        >
          <div
            style={{
              backgroundColor: "#000",
              display: "flex",
              justifyContent: 'center'
              // justifyContent: "space-between",
              // alignItems: "center"
            }}
          >
            {/* Left Header */}
            <div
              id="discussion-close-btn"
              onClick={() => this.closeFrame()}
              style={{ left: 15, position: 'absolute', paddingTop: 6, }}
            >
              <FontAwesomeIcon
                icon={faTimes}
                style={{
                  color: "white",
                  fontSize: 25,
                }}
              />
            </div>
            <h1 className="headerTitle">Discussions</h1>
            {/* </div> */}
          </div>
        </header>        
        <CreateComment initial="main"
          // parent_content_id={this.state.parent_content_id}
        />
        <div
          style={{
            // borderRadius: 6,            
            border: 'none',
            margin: 8,
          }}
        >
          <div>
            {this.state.listCommentList.map((data) => {
              return (
                <>
                  <div className="card-content">                    
                    <div style={{ borderTopWidth: 1}}>
                      <ChildDiscussions
                        commentData={[data]}
                        currentIndex={this.state.setindex}
                        closeModalBySubmit={this.onSubmit}
                        navigation={this.props.navigation}
                        isFirstComment={true} />
                    </div>
                  </div>


                </>
              )
            }
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Discussions;
