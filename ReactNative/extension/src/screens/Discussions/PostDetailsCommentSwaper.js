import React, { PureComponent } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faComments,
  faHeart,
  faBug,
} from "@fortawesome/free-solid-svg-icons";
import moment from 'moment';

class PostDetailsCommentSwaper extends PureComponent {
  constructor(props) {
    super(props)
    this.state={
      commentBoxOpen: false, 
           
    }
    moment.updateLocale("en", {
      relativeTime: {
        future: "in %s",
        past: "%s ago",
        s: "a few seconds",
        ss: "%d seconds",
        m: "1 minutes",
        mm: "%d minutes",
        h: "1 hour",
        hh: "%d hours",
        d: "1 day",
        dd: "%d days",
        w: "1 week",
        ww: "%d weeks",
        M: "1 month",
        MM: "%d months",
        y: "1 year",
        yy: "%d years"
      }
    });
  }

  openModal = () =>{        
    this.props.modalVisibleFunc(this.props.parent_content_id)
  }

  render() {    
    return (
      <div style={{ width: '100%' }}>
        <div className="child-card-content">
          <p className="child-comments"  style={{width:'86%'}}> {this.props.item.node.text}</p>
          <div className="child-card-footer" style={{width:'93%'}}>
            <div style={{ display: "flex" }}>
              <div
                style={{
                  fontSize: 13,
                  color: "#959faa"
                }}
              >
                @{this.props.item.node.author.username}
              </div>
              <div
                style={{
                  fontSize: 13,
                  color: "#959faa",
                  marginLeft: 8
                }}
              >
                {moment
                  .utc(this.props.item.node.created)
                  .local()
                  .fromNow()}
              </div>
            </div>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div style={{ paddingRight: 10 }}>
                <FontAwesomeIcon
                  icon={faBug}
                  // color="#4C8BF5"
                  color='#969faa'
                  style={{
                    fontSize: 16,
                    textAlign: "center"
                  }}
                />
              </div>
              <div style={{ paddingRight: 10 }} onClick ={()=>this.openModal()}>
                <FontAwesomeIcon
                  icon={faComments}
                  // color="#4C8BF5"
                  color='#969faa'
                  style={{
                    fontSize: 16,
                    textAlign: "center"
                  }}
                />
              </div>
              <div style={{ paddingRight: 10 }}>
                <FontAwesomeIcon
                  icon={faHeart}
                  // color="#4C8BF5"
                  color='#969faa'
                  style={{
                    fontSize: 16,
                    textAlign: "center"
                  }}
                />
              </div>
            </div>
          </div>
        </div>        
      </div>
    )
  }
}
export default PostDetailsCommentSwaper