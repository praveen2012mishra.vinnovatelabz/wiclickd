/*global chrome*/
import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faComments,
  faHeart,
  faPlusCircle,
  faSignOutAlt,
  faEdit,
  faUser,
  faUserCircle,
  faShare
} from "@fortawesome/free-solid-svg-icons";
import "./style.css";
import { Row, Spinner } from "react-bootstrap";
import $ from "jquery";
import Discussions from "../Discussions";
import { UrlInfoMutation, UrlInfoVariables } from "../../mutations/urlinfo";
import applloClient from "../../client";
import { withRouter } from "react-router";

const NavMenu = ({ history, isContentEditClick, setContentEditClick }) => {
  let [Icon, showIcon] = useState(true);
  let [Discussion, showDiscussion] = useState(false);
  let [Index, setIndex] = useState(0);
  let [ImageList, setImageList] = useState([
    "#000",
    "#eb3225",
    "#b0b0b0",
    "#ebca44",
    "#8bbaf9"
  ]);
  let [Loading, setLoading] = useState(false);

  const toggleMenu = () => {
    if (Icon == true) {
      showIcon(false);
      chrome.storage.local.set({ isShowIcon: false }, function() {});
    } else {
      showIcon(true);
      chrome.storage.local.set({ isShowIcon: true }, function() {});
    }
    chrome.tabs.query(
      {
        active: true,
        currentWindow: true
      },
      tabs => {
        chrome.tabs.sendMessage(
          tabs[0].id,
          { type: !Icon ? "show_icon" : "hide_icon" },
          () => {}
        );
      }
    );
  };

  const showShareModal = () => {
    chrome.tabs.query(
      {
        active: true,
        currentWindow: true
      },
      tabs => {
        chrome.tabs.sendMessage(
          tabs[0].id,
          { type: "show_share_modal" },
          () => {}
        );
      }
    );
  };
  useEffect(() => {
    chrome.storage.local.get("isShowIcon", data => {
      if ("isShowIcon" in data) {
        if (data.isShowIcon) {
          showIcon(true);
        } else {
          showIcon(false);
        }
      }
    });
  }, []);
  useEffect(() => {
    if (isContentEditClick) getUrl();
  }, [isContentEditClick]);
  const isUrl = s => {
    var regexp = /(https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return regexp.test(s);
  };

  const getUrl = () => {
    chrome.tabs.query({ currentWindow: true, active: true }, function(tabs) {
      let link = tabs[0].url;
      if (link.startsWith("https") == false) {
        alert("All links must start with https.");
        return false;
      }
      if (isUrl(link) == false) {
        alert("Not a valid URL");
        return false;
      }
      UrlInfoVariables.variables.url = link;
      setLoading(true);
      try {
        applloClient
          .query({
            query: UrlInfoMutation,
            ...UrlInfoVariables,
            fetchPolicy: "no-cache"
          })
          .then(async res => {
            if (res.data.url_info.status == "UNFURL_SUCCESS") {
              let description = res.data.url_info.unfurl.summary;
              let image = res.data.url_info.unfurl.thumbnail_url;
              let title = res.data.url_info.unfurl.title;
              let url = res.data.url_info.unfurl.normalized_url;
              window.open(
                `https://www.weclikd-beta.com/post?description=${description}&image=${image}&title=${title}&url=${url}`,
                "_blank"
              );
              setLoading(false);
            } else if (res.data.url_info.status == "ALREADY_SHARED") {
              setContentEditClick(false);
              window.open(
                `https://www.weclikd-beta.com/post/${res.data.url_info.post.id.replace(
                  "Post:",
                  ""
                )}`,
                "_blank"
              );
              setLoading(false);
            } else {
              let description = "";
              let image = "";
              let title = "";
              let url = link;
              window.open(
                `https://www.weclikd-beta.com/post?description=${description}&image=${image}&title=${title}&url=${url}`,
                "_blank"
              );
              setLoading(false);
            }
          });
      } catch (e) {
        console.log(e);
        let description = "";
        let image = "";
        let title = "";
        let url = link;
        window.open(
          `https://www.weclikd-beta.com/post?description=${description}&image=${image}&title=${title}&url=${url}`,
          "_blank"
        );
        setLoading(false);
      }
    });
  };
  const handleSignout = () => {
    chrome.storage.local.remove("isLoggedIn", () => {});
    localStorage.removeItem("userIdTokenFirebase");
    chrome.tabs.query(
      {
        active: true,
        currentWindow: true
      },
      tabs => {
        chrome.tabs.sendMessage(tabs[0].id, { type: "logout" }, () => {});
      }
    );
    history.push("/");
  };
  const changeHeart = () => {
    if (Index + 1 === ImageList.length) {
      setIndex(0);
    } else {
      setIndex(Index + 1);
    }
  };
  return (
    <div>
      {Loading == false ? (
        <div class="menu-container">
          <div class="menu-header">
            <FontAwesomeIcon
              icon={faUserCircle}
              color="black"
              className="user-icon"
            />
            <div id="user-name">{localStorage.getItem("UserName")}</div>
          </div>
          <nav class="navigation">
            <div
              class="nav-item"
              onClick={() =>
                window.open("https://www.weclikd-beta.com", "_blank")
              }
            >
              <img
                alt="logo"
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAilBMVEUAAAD////m5uZMTEz29vb8/Pypqanh4eF6enqEhIQ0NDS+vr4vLy/z8/Pv7+/5+fnT09PIyMjd3d1UVFQICAi3t7eQkJAdHR1fX18YGBhDQ0OhoaFoaGjX19dycnKvr6/ExMQnJyeLi4tGRkaZmZl1dXUqKiqioqJiYmJQUFA8PDwiIiIzMzMSEhIDs6ucAAALuklEQVR4nO2d2VLjOhBAQ7DJQuzsGyQkrIEA//97F2YGSKQ+sjxqa3KrrJepYiTZx7HUrd7caMRu582zqC06YE1YE9aENWFNWBPWhDVhTVgT1oQ1YU1YE9aENWFNWBPWhDXhV5t0x6t2ezV+/UvCZDTcTqfbgdKT0Adc7LajPEny9aZz+zeEl3cvy89+3Zvd+hQJlxet9GvufHBTmrA5ezrouktOjnCyTQ9nzy5KEo5WR32Xq/zECCdbc/7LUoStZ7P3Png1qgK+W4BnZ5sShHnbnnMc+iuqEraFu8k6/oRTcdL+6RB2h9IVBle+hM2xNOtenPXfELYz6QqJ8SMyob1mf7W7lAbEJlxeyJfYzP0I+5Js+WiL1qkQXg3kSwyPX1MkTN7keQNfU0XCZxDPoxc/wjXMe3t5KoQ3cAlfQliGjdeTIbyDSww831JB//nVTuYtfX+AS2y7foTXMPE5rO/4hDu4xPT4iIGEsJWezl46n8IlLiZ+hC8w8SpMNdUjfBvBJR6O+yEhCAtQJP4B4R5U5PTOk/AdJu6EKaaKhHAjfU+tLaOJaQeLTngOV8gf/QibMO/k+lQIF3CFkXFiIMIBzDvfnAohqTS+pydSaZ5oj45O2IErDHt+hDuYlxT6+ISo0hwfnpBQsGD87k9SKDqhYKP51UzTBBGuxFkbjXGgLUqPkB61aYoiwnuY9zlM4OsRTuBG+oZKg4RdcdpG4zEMUJEQLpAZKg0R5j1x2g+l7VQIn+ACpsAnwjU4ciazUyEklaZlmgiBcAhqKdm34hOu4AIjcwcBwg0QBtowFAlJ4I/MHQQIryfitI3bQIGvR0gC31I3gdDckL5aN+yEr0hIj3rrSWhuSF9tHygO9QjpUVveFiC0/Gpf3QMdM3qEdMK3bIQyYSJ6ZRp8KItPCI/a9MsQYfMc5qVDWXTCHsyfWTZCmXB9Jc3a4D06OuEY5s+tt08mHOxh4sATfgRC685lwg0obY3AE74eITktmtady4TXZEsMPP/qEZL2aFvQZMIZTRwq8NUISXu0nYIyIak0y1CBr0ZIHjAhnqbVT8zWT8hKc5vYvQ/GpcU+/qI7Xy46DxfFbUdRL4Od2XXWkRptpcs7sftXu3uYFplxCvhmw2b28aCK2llGzzI7M7tePrmvWbK9jjvuzcg5ujfyVQqb1NH++yWZK/669Zwy0zXyxj/gCgntrVCf0B035Rh2V0Krx7c0DqHLx8iDSrlecUuzX4NKCCfXeAc4plQERL8JF0jsv1dC6PBu4JBOmfBcJBQ0kmoIGzO6XxrQK2fionUYjxBDbWlAucBVVC2Ey1ZEiJ4h6l/uaJ3TKyL8vSrCB3jK1L/c0RoJI/6GK7gH6D6h815JwnjrsDEuR1jyN+z7C/zKCBclCR/LhB6nGag/acR1+FiScFFmL01zIJQmqYrwouROU0qlQULJBFER4YSSpHBEKcckrcOIhJg/hCPOSySO9f1Ph1URvuI7x2M6/jagnJ6fNEU1hHy3PGZ54X0+zGh6SVeshPCZLRmOUbfeHnR8S2Otw4VjSbnGvc78REaKAj+StGi7zMbuoYupl+DPQNiKg9UJx5fOHaNg9Py8PV3nSZ5kWfKZ3Pv73+zjL9nPH7MmEDaz330O+iaJEbof1pbj2aBgR1S5DkUptCTTqGDVp9DSuUaqswohZVoMpZ/LJkwptHQf7HhSInwlH99m6UXYJ7fMQiMlX4PwjQwID5JT0CbMFjBxCZN0tYRXpDGJv41ASKGlJdSqaglfaLmILjObMCdfzUNoLI0WIS4XMVdLIISQtsYuLANYj/AGXiZ5fQmENDEdaqMTksVDzj63CUcw7zI4DkOLkKxWa3EHsQnFQgMf7SkwE1+NEOOUt2Jwuk1IyaOhuSRqhHM6ZclRQDahFfr2p72cSn2aW1ouOzEKyCakXJLA5FE9widSaeTAZpuQwhJLOfiqJNzTcpHjnGxCEocnQ3gPL1NfjgKyCWnimYY41CAcg8DP5fRzb0JMf49OuCCBL+8gFmEL5g1NHtUjJGfqWt5BLMIhzBuaPKpHSAJ/KCfYW4Qo8FVUGg1CyrW+lE1OFiEJ/NDkUT1Cepmmkg1DIKRMi9DkUTXCCYlDq44ZEJLAJ8d8dMI5GJxTM3mUCKkexo2KOFQg7FF6LIRuW4Tyy6yQaaFFuIdHbZWiA8IMovSDk0fVCK9g5iZU1DEJB6CWvgfWw9Aj9CyAhYRo0tdRaRQIKbZoDTuISUgFsN5UTvgahHTCH87l/iYh5ZKE1/XUIqTz7xb6m4SUaXGlYfBWISRjClnQTEJyWrxoGLxVCOFlspNHgZCSR591BL4CIfiHElpfBmGflLbQehhqhK+gPWZYb+aYsEnJo+XiPysk3MPEOdkIDUKzBNF3UxL44YSUa92kXC2DcFpZ8qgWIRVXaYE4NAkvSPFWEvjhhBSHsfYkfAC1lCr6xCck7ZHsSyYh2TDelYRFOKF/8qhImFI9DKroE5+QVBqwYViEVLWUDmXRCV9JpSEb4XEecL9JStsideT/fjcFwvnNdLBuORpsCOkI+jc352/dn9ajPPx516ftx7MiJ6Ob72VTpP7i/2Owj3IecKPRcwdQuobO28VvAaXlSZkWfwgriC91HSUd4544L7OYMOPf8GRihOfXPuuYEg/jEjaeOUqYB3V8wubwZUxRJakmD5jvFsf4BUJglD7vUBXlW2DOKw7xs8fmRMhvTUU5M9L3UZyEGH5w3PolEg+rJXwnXxwNWPjZgUrlIVRKiOdJ6u9ZU5OS1lgcVkZIJSCwvx8h5SEk8X/Dsjmknq4t+g0T1jL+Z3nAtAwTPqFXRUgfgaH+fg7YBGuaxF+HVMGF+r94+UWynKKFeExVhJuSecB+4ToJETqSySoi7NElcYRXrkMqVGf5R4R3dL84oktes8OGqZUOrb0aQtbBeMyNx0okxdsh8KshfN+hDuYYhL/7TyPFux/7N0S923nGnxSXwqK3tB95L6WclgLCDzWhIHYuxTz8uG+pM5i4YOzjIHGU3ENhkTjK8+na2ibzrtMOVWwvfR93rMqHP402sJGjxOKs/d0eyTv6ump7ttmmqPxn2BOkSBOKUvhs9webAnnClQIvwwnxy4TkMvtsB34Lu4zyn6aTS6JAiIlJ5DIzCO0yyn+aY/OPSzimNUC/jUlInnCV5NEqCfsUpWAQtqj4gFLQXjjhDUWakMvMIKTPdGFV4uiEbTod0qcMDUL0hJ8MIX55lNaXQbiFLnOdTItwwpLJozYhecI9zdHVE+K3VZ3lSw4ISagU6cPRCDF5FKOADELKJdEU+EGE8rfUzxwl5A1CUks1BX4Q4R4ccOZ3OZGQQtp0UisVCOkLapA8ao3CL49qhSUGE45Bt0KF2iCkL49iRZ/ohCsyJbpUmgNC+pj6UlNpCyKkOOW1S6U5IKzqy6N6hOS9GVBFHYOwoo+pKxKSwB+6VJoDQioPda+p0oQQvpN+PKVYNYOQNiQ8dsYmnNOjpuJkJiHp51iKNDYhBl5C8qhJmBJh6Hc51QhvwdCc0PoyCDNSaZSSR8MJKXk0d6o0P4RNMsgpJY+GE9JnuqBahEVI5aEmgZ+L1yPEL4+SjdAgJCsNHspOhhA/g2cQ0pb7pirwQwjpBCBWvBQIaUNSqXipQkgv09Y97JuQMi2ook98QniZUsy0MAjxY+qa598gQlDarE+NEyG6ZVQBQwhhU8/JvmQSkjVHV1hUIA9HLkNbo1haoLobn1DWS7EosEUIRU3udQH1rRjNokCEH8Kt+P+60rACi7DTVnpMmEq7qfI+E+iZObefN7qTBMKzlq3fKQvDUMLG2Ax+27q8Thbh2cC0yr1ov6PB+YdXF0eHxGkx4LEdudU5OkKN9QHDMyyft9/7zbrjEwx0bCnPLn8W43LW0lVnVAg/GM/vLgd5f3Tx7BfsZPoC+oPVp21uOX6gPLh/TVi2kbejqlYT1oQ1YU1YE9aENWFNWBPWhDVhTVgT1oQ1YU1YE9aEcdp/oB3WxrhAvKEAAAAASUVORK5CYII="
                style={{ height: 25, width: 25 }}
              ></img>
              <div class="title">Go to wecklid</div>
            </div>
            <div class="nav-item" onClick={() => showDiscussion(true)}>
              <FontAwesomeIcon
                icon={faComments}
                color="black"
                className="icon"
              />
              <div class="title">Discuss</div>
            </div>
            <div class="nav-item">
              <FontAwesomeIcon
                icon={faHeart}
                color={ImageList[Index]}
                className="icon"
                onClick={() => changeHeart()}
              />
              <div class="title">Like</div>
            </div>
            <div class="nav-item" onClick={() => getUrl()}>
              <FontAwesomeIcon icon={faEdit} color="black" className="icon" />
              <div class="title">Create Post</div>
            </div>
            <div
              class="nav-item"
              onClick={() => {
                showShareModal();
              }}
            >
              <FontAwesomeIcon icon={faShare} color="black" className="icon" />
              <div class="title">Share Post</div>
            </div>
            <div
              class="nav-item"
              onClick={() => {
                toggleMenu();
              }}
            >
              <FontAwesomeIcon
                icon={faPlusCircle}
                color="black"
                className="icon"
              />
              <div class="title">
                {Icon == true ? "Hide Icon" : "Show Icon"}
              </div>
            </div>
            <div class="nav-item" onClick={handleSignout}>
              <FontAwesomeIcon
                icon={faSignOutAlt}
                color="black"
                className="icon"
              />
              <div class="title">Sign Out</div>
            </div>
          </nav>
        </div>
      ) : (
        // <Discussions />
        <div
          style={{
            flex: 1,
            justifyContent: "center",
            width: 300,
            height: 300,
            marginLeft: 150,
            marginTop: 150
          }}
        >
          <Spinner animation="border" />
        </div>
      )}
    </div>
  );
};

export default withRouter(NavMenu);
