/*global chrome*/
// import * as firebase from "firebase";
import firebase from "../../Firebase.js";
import React, { Component } from "react";
import "./style.css";
import { Row, Button, InputGroup, FormControl, Spinner } from "react-bootstrap";
import { setLocalStorage, isLoggedIn } from "../../library/Helper";
import { withRouter } from "react-router";
import { graphql } from "react-apollo";
import { UserLoginMutation } from "../../mutations/login";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UserEmail: "",
      UserPassword: "",
      loading: false,
      access_key: ""
    };
  }
  componentDidMount() {
    if (isLoggedIn()) {
      this.props.history.push("/navMenu");
    }
  }
  userLogin = async () => {
    // window.parent.postMessage({type:"wecklid_login",userIdTokenFirebase:"abc"}, '*');
    let __self = this;
    this.setState({
      loading: true
    });
    const { UserEmail, UserPassword } = this.state;

    await firebase
      .auth()
      .signInWithEmailAndPassword(UserEmail, UserPassword)
      .then(async res => {
        if (res.user.emailVerified == true) {
          return await res.user.getIdToken().then(async function(idToken) {
            chrome.storage.local.set(
              { userIdTokenFirebase: idToken },
              function() {}
            );
            return await setLocalStorage("userIdTokenFirebase", idToken);
          });
        } else {
          alert("Verify Email Please!");
          return false;
        }
      })
      .then(async res => {
        if (res) {
          return await __self.props.LoginMutation();
        }
      })
      .then(async res => {
        if (res) {
          __self.setState({
            loading: false
          });
          await localStorage.setItem("userLoginId", res.data.login.account.id);
          await localStorage.setItem(
            "MyUserUserId",
            res.data.login.account.my_users[0].id
          );
          await localStorage.setItem(
            "userIdTokenWeclikd",
            __self.state.access_key
          );
          await localStorage.setItem(
            "UserId",
            res.data.login.account.my_users[0].user.id
          );
          await localStorage.setItem(
            "UserName",
            res.data.login.account.my_users[0].user.username
          );
          chrome.storage.local.set({ isLoggedIn: true }, function() {});
          chrome.tabs.query(
            {
              active: true,
              currentWindow: true
            },
            tabs => {
              chrome.tabs.sendMessage(tabs[0].id, { type: "login" }, () => {});
            }
          );

          this.props.history.push("/navMenu");
        }
      })
      .catch(error => {
        __self.setState({
          loading: false
        });
        console.log(error);
        alert("Invalid email or password");
        return false;
      });
  };

  googleLogin = async () => {
    let __self = this;
    this.setState({
      loading: true
    });
    chrome.identity.getAuthToken({ interactive: true }, async token => {
      var credential = firebase.auth.GoogleAuthProvider.credential(null, token);
      await firebase
        .auth()
        .signInWithCredential(credential)
        .then(async user => {
          await firebase.auth().onAuthStateChanged(async res => {
            if (res) {
              return await res
                .getIdToken(true)
                .then(async function(idToken) {
                  await setLocalStorage("userIdTokenFirebase", idToken);
                  chrome.storage.local.set(
                    { userIdTokenFirebase: idToken },
                    function() {}
                  );
                  return idToken;
                })
                .then(async res => {
                  if (res) {
                    return await __self.props.LoginMutation();
                  }
                })
                .then(async res => {
                  if (res) {
                    this.setState({
                      loading: false
                    });
                    await localStorage.setItem(
                      "userLoginId",
                      res.data.login.account.id
                    );
                    await localStorage.setItem(
                      "MyUserUserId",
                      res.data.login.account.my_users[0].id
                    );
                    // await localStorage.setItem(
                    //   "userIdTokenWeclikd",
                    //   res.data.login.account.weclikd_jwt
                    // );
                    await localStorage.setItem(
                      "UserId",
                      res.data.login.account.my_users[0].user.id
                    );

                    await localStorage.setItem(
                      "UserName",
                      res.data.login.account.my_users[0].user.username
                    );
                    chrome.storage.local.set(
                      { isLoggedIn: true },
                      function() {}
                    );
                    chrome.tabs.query(
                      {
                        active: true,
                        currentWindow: true
                      },
                      tabs => {
                        chrome.tabs.sendMessage(
                          tabs[0].id,
                          { type: "login" },
                          () => {}
                        );
                      }
                    );

                    this.props.history.push("/navMenu");
                  }
                })
                .catch(error => {
                  console.log(error);
                  this.setState({
                    loading: false
                  });
                  alert("Invalid email or password");
                  return false;
                });
            }
          });
        })
        .catch(error => {
          this.setState({
            loading: false
          });
          console.log(error.message);
        });
    });
  };
  render() {
    return (
      <div style={{ margin: 10, width: 300 }}>
        {this.state.loading == false ? (
          <div
            style={{
              width: "100%",
              alignItems: "center",
              justifyContent: "center",
              alignSelf: "center"
            }}
          >
            <Row
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignSelf: "center",
                height: 50,
                borderWidth: 1,
                borderColor: "red"
              }}
              onClick={() => this.googleLogin()}
            >
              <img
                alt="logo"
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABSlBMVEX////qQzU0qFNChfT7vAXm7v07gvSAqffqQTP7ugDpNCLqPi/7uAA1f/Qtpk7pOSggo0a70PrqPCz50c43gPTpMBz8wQD5/foopUv++Pj87Ovr9u4do0X74d/4x8TrUkb86OfucmnpOzb+8tbR4Pz//PJSs2vd7+F9w45BgvvsXVLrTUDpLBftY1nxjIXwg3v2vLjylo/0rajzop34y8j7xj/x9f7/+ORNi/SMsvixyvrM59Igp1eow/mdvPiXz6RFrmC738Rqu37G2PvE48zubmT1sq3we0j957Xyhif3ox7tWzTxeSz1mCT80Wn5rhrvajH93pj7wi7tXjP7y1X+7sf803n95rFyovf92oz7zFFhl/T7xTmetUN0slHcvCm7ukOOtUvPvDiJyJlasFap1rQ+lMRMpJ5Cp3xJlNNNoLRFpYxCq2pIj99uwY8pAAALl0lEQVR4nO2c7XfaRhbGhUwcvWBJFZYRRhhoYkHANrZx0qQkMRDbm+5u0002abNNX3brZZt2u///15UQxghmpJmRZjTk8HzISXJOLP24d+5z78wQQVhrrbXWWmst3lQOlPVrpKzdevG4d9K9bHckxTqYSNFztcPGUe90r7q72rjVvV73sKNYim6aquQpN5X3W1U1dV2xzNrl0VVxN+s3JVD5rHfZMT00dYYFlIdqKopU6x5Xs35lDJWLvbaXiaoUDRfiNBVL7x6vRCx3j7s5S0dmm8f0KNu9It8Ls3zckBSTBG8qVVdqJ/zm615DV2KWHVooOz0e07XeU63keFNI/aBxljXQgs4aOtHag0q1alccrcjjdqLFB5ak5E44YbySFDVtvIBRt044WJBXHSX18M0x6lnHca9NkS9gzJ1myFc9pJSfIUaltpcR3+7RAX2+CeNBo54F4FVOZ8LnyzR7zPnqh5QXYFiS0mbcy50yWIBhqQrLMDIOYCBJOayzAjxWWQcwkKoeswHsZhDAQJJyxICv3mFXQpel16i3cXt6Nhl6I1WiPFb1MsvQG0k61S6ukTmgh2jRW4y77SyX4K2UBqV5o1ozs2abSr+kUm+qUrY1Zk6SQqOFK3KwBKeSzCIFwLN0N5qSSNI/dUCLhiMWLX4AlU8d0KQBWE1rLzu5JIXGGqzzZBM0AMs5fgCpVNHyIS+dDCUfFLp89KI5WinqjUtZg92IEuDeQYqvKKk3ktCP+Gf/mk6K1tNpZfz7FpaSqx1eNnxdHrY7phV/TyP0I6gUGa/KJC+jkqpYUuPkqlifuxxULu9W93pHh4qlo1FSSlHhKGGVkVRdUaNuypSLp5cWwukqLcDjZItQ0vV2L36QK58ddWIgKa1BYTfRRKha6NdGymfdqGsAtCIoXJJbvWQeXJ5hbabs9lTYB0oN8IrYCSVT6RJsM1zlgIyUqqhnFMS3K0y9Wyd75mlnOVdprUFBaBDmqGQ1yLeJyifWgj9RS1FhzyLj06Vkh+7V8NUHeoBlshxVlZPEjz6dOxmhl6LCCZHX67U0Xqg621qnVmS8h5CEULKO0tltLx8F+0L0UpSszKhKeie0Vz4ixQgKRYIyY3bS3GkvehMWvTUoCG38kSLtw5Jqx6IISOAUSjftl6hTBBT+9Dk2IIsbBOnp5c5XmIgpuCBTPZR3/pzDYVw1wHeFfH6n8Bd0xBVLUUH4Xs572vkrKmL6RYay7hXyE+18jQZoNrJ+Y1y9kvNTxG9QMlWtcXKfHln70xBO9LdYREmvZ/3GuHo9TxhvG1ZWV7DJ9UbOzyPG2Maq+YQQWEUIsfD3CES1nfX74uuVnF9QhG1IOr/fo4NpP79E6NsGZBxmevM6Jb0sLAHCbUOqZf26BFpO0gjbOKA539ASBBBoG+aqdWu+FitplG1Iaj3r1yXQt7AYAmxDXz0r9ASopLBMlSQOvvuIrXvQJA0Q521jNUP4Oppw3jakg1UbKSZ6G5WkgW4aHHPV5vqJQA3NcqYGiNbq9WtC7DK8yVTfNtSVG+wniluGU8SC1+CkeETBUrCWbYnxq887K1lnSm8QCb0GZyXrjLCPCpjPy+/IH/PoDnXBHg1vSpe1T0742dYGXW1uPYI8Gq3QTEL4ihyQPuHG1n3IoyPa7gUVvuOb8EvIo39AJ0yQpAwIN59DHo3Kl5d3EgCyIHwPfvI++jL8lm/CjcfgJyP1bEGSJlmGLAi3wE8GbrOBCe/xTgi2i+/Q7TBJoWFCCLaLf6CWUvlhiXfCF8Ano/bdyfyeDeFnwCd/j0yYqJSyINx+BnwysuEXXvNOuPkE+GTk2anwknvCuwkJE5kFE0JwU4OwDRVI5p/wA+jB+4h8K0EIbNvQCfNf8E4IbkzXhGvCNWH2hCVkQP5rKWQE/oQcf00YS8h9XwohfMhotvgyM0KEA+BA/M+HEELkLe+EMz6DGII7b/RjC/mHRPs0LAjB0xP6bmKyvTYWhOAJGP1wLZldMCDcBu9iYOx5JyqmLAghRzOMzi0YEMKO11ABE549sSAE7wijW36+kGS6YEEIOQRG3tZPdvjEghByVwH9aEZ+yzkh5NHoxTQvJ3BEBoSQE1Kc+zSFBPdpnm1tEgoVEHbKjX5yIcs/npMTvrhLqA+oiJBjCwG595bf/PR0kKg1JdPzbURC6G0TxFIj/zwQRaPPki3Qe9QYQm8MoZUa+Z9PRVHUWizZAiGvQ+itL2E/fiNDzv/LBxRFu8ISztd95BoMK6UCwpgvv/l3ACjaCWoNmZ6hLkPI7DRR3EKUfxFnMtixBUIupbDJwte9yBh6JvF0jnDMDs7XI/RlCOm7J4o6JZXzP80BemJrGBitEPQGrRDZfMs/i2ExXonofg/ehpoKvpMRmERYLMvpHdQ6s7EdUWgE6BQ8M4lQEK8Z0fl6gkwIuS50I3DjdmsSITlNRnhenUHl82IYtQwhaTpvEvPSXEZ8/kCCTBjh976A37n4ERRAto6BbBWwncRbLaXpkkmEosio2DxHD2GkG/paNP0lk8giT9HdHr6DcavwjhvIJEJ5ysQU76ITwuf7meZ7U6BJhOUwGBTvIztFrFdMdBtDiEkwX4qPMZJ0Ezob3mrWucm/xPP5S5F2f/oEY3MOIUkF4YsgTcOTRITsEV3AFzi7jyhJOr3wHWkSYRlUu7c7GHyeEJI02K6JNokFOTQLKvJM4SvW7qd6K8eZxGIU6SG+x6ijUbtsYb2LNwlWiDhVBnZzFiRXwwOklqgY3dokhEh1xtfQwSUUHRrlBhNwYwP9R+MHUTRGqfsiXop6IYzYZFtUHz+Ioj1Iubu5i30MFz37hjXCD6Ko2Rcp8j36gAuIahWBmjY+oV9vUsvU+xs4PjjRJk4IBeGaCNFwU9q6wa4xuCEUhBJBmop+pqZhG333Pw+wCeEnThAROEYQRjHpxFhpOZrzK/qhNlkIBbJiM5EzSpKqpbHoLxDn4wesfg2x5w6pQkropWqL2DiGAyP4IfbgN5xM3QJ/qTJaY4MUUbSNFkkcvfgZs89Vc35/gH79ArkjDYk4TyfvN+pjWkfz2gh/ps5/kfcvYvcQwaqQB3HCODhHD2Rl7DpLBmX/8T+0TEXavACJtJ7O3tB2kSCbY1czQAmjaWi2ge0UM7WIfH/+FW1Daw0j6k6pPx7Zhg1dD86vCM0N/AJNrEoEQ8YypeGIo+txv1IpzVZmqVSp9MfnrYHhwOkCxI+P42yDOEd9kfWnAErbcAxx4LqjiVx3oDmOFzqEDzDeNjAb0gUlXYoLoDPh/CM72jYS5OhE54kKajpyPkYsxu0kOTpR4mqTgjzbgC3GzcfEdfRGqVSbpNJEmG1sI24gRiKKHCCKkGkj6SIMVMEqDLRkgKaNLfyZCag+D4Qg29iOvB2EhZiqZ5BqadpIocrMdMEFoj9tzGXqJtb2YZyGHHiGuDBtpFFG53TBgfOLoWkD9aAJWXysxdtpg3DojVKTC9PwEP/wbWM7fUAPccAHoi3+9oBCBH1VXD7qjWb8TgfQ04iLeqOJFO998jBM2S7VW0rDmF0H+qJwFhtWU8x2MVK92zJVK8NM1ewhfUAvUzNzxtQOKONUcbNpcJxrdl9jGQP3qOnK1tK8KBCrist4NWpGi/UXV8dMi6o9YBrAQP5xNCM+jeUKnFfTZbIcNSPRwXkyXbjU46gZbgYJOqfhgK5zOAMmHh/NKFLLVc0Qs+fzNaSzHr385IPPV79lp+0dtt3K4P81iFDlXEyx6GiOeM78K//x6o/sVLJVM+wRX+G7VWk4Slp2bEMcDbOxd0RVhi0x7voBNHi2M7i+4DA7F1Vqjl0Hk3Jyl2E0bnIdvbD649FAQ7luoXlwtuheD7PrzMhV6Y9bHqdj2La9cAHD+6P3l4ZjD9zW+cUqhW5ZpUrzYnx+3Rq5Az8XDQ9XEwfuqHV9Pr5oVlaaDaDS7Je11lprrbXW4kb/BxxPxTS82rvyAAAAAElFTkSuQmCC"
                style={{ height: 30, width: 30 }}
              ></img>
              <div
                style={{ alignSelf: "center", marginTop: 10, marginLeft: 10 }}
              >
                <h5 style={{ textAlign: "center" }}>Login With Google</h5>
              </div>
            </Row>
            <Row className="devider">
              <div className="hr"></div>
              <div className="title-or">or</div>
              <div className="hr"></div>
            </Row>

            <div className="my-3 mx-2">
              <InputGroup className="mb-3">
                <FormControl
                  aria-label="Default"
                  aria-describedby="inputGroup-sizing-default"
                  value={this.state.UserEmail}
                  placeholder="Email"
                  onChange={event =>
                    this.setState({ UserEmail: event.target.value })
                  }
                />
              </InputGroup>

              <InputGroup className="mb-3">
                <FormControl
                  aria-label="Default"
                  aria-describedby="inputGroup-sizing-default"
                  placeholder="Password"
                  value={this.state.UserPassword}
                  onChange={event =>
                    this.setState({ UserPassword: event.target.value })
                  }
                  type="password"
                />
              </InputGroup>
              <div id="signupContent">
                No Account?{" "}
                <a
                  href="#"
                  onClick={() =>
                    window.open("https://www.weclikd-beta.com/", "_blank")
                  }
                >
                  Sign Up
                </a>
              </div>
            </div>
            {/* <Row>No Account?  Sign Up</Row> */}
            <Row
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignSelf: "center",
                height: 50
              }}
              className="mb-3"
            >
              <div
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              >
                <Button
                  style={{ backgroundColor: "black" }}
                  variant="secondary"
                  size="lg"
                  onClick={() => this.userLogin()}
                >
                  Login
                </Button>
                {/* <Button
                style={{ backgroundColor: "black" }}
                variant="secondary"
                size="lg"
                onClick={() => {
                  window.parent.postMessage({type:"wecklid_logout",}, '*');
                }}
              >
                Logout
              </Button> */}
              </div>
            </Row>
          </div>
        ) : (
          <div
            style={{
              flex: 1,
              justifyContent: "center",
              width: 300,
              height: 300,
              marginLeft: 150,
              marginTop: 150
            }}
          >
            <Spinner animation="border" />
          </div>
        )}
      </div>
    );
  }
}

const LoginMutationWrapper = graphql(UserLoginMutation, {
  name: "LoginMutation",
  options: { fetchPolicy: "no-cache" }
})(Login);

export default withRouter(LoginMutationWrapper);
