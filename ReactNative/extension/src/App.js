/*global chrome*/
import React, { useEffect, useState } from "react";
import Discussions from "./screens/Discussions";
import logo from "./logo.svg";
import "./App.css";
import NavMenu from "./screens/NavMenu";
import "bootstrap/dist/css/bootstrap.min.css";
import Login from "./screens/Login";
import { Switch, Route, withRouter } from "react-router-dom";
import PrivateRoute from "./components/PrivateRoute";


function App(props) {
  const [isContentEditClick, setContentEditClick] = useState(false);
  useEffect(() => {
    if (window.location != window.parent.location) {
      props.history.push("/discussions")
    }
    if (chrome.runtime.onMessage && chrome.runtime.onMessage.addListener) {
      chrome.runtime.onMessage.addListener(function (action, sender, response) {
        if (action.type === "wecklid_login") {
          localStorage.setItem("userIdTokenFirebase", action.userIdTokenFirebase);
          localStorage.setItem("UserName", action.UserName);
          props.history.push("/navMenu")
        }
        if (action.type === "wecklid_logout") {
          localStorage.removeItem("userIdTokenFirebase");
          localStorage.removeItem("UserName");
          props.history.push("/")
        }
        if (action.type === "content_edit_click") {
          setContentEditClick(true)
        }
      })
    }

  }, [])

  return (
    <div >
      <Switch>
        <Route exact path="/">
          <Login />
        </Route>
        <Route path="/navMenu">
          <NavMenu
            isContentEditClick={isContentEditClick}
            setContentEditClick={setContentEditClick}
          />
        </Route>
        <Route path="/discussions">
          <Discussions/>
        </Route>
      </Switch>
    </div>
  );
}

export default withRouter(App);