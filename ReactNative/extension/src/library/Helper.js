import * as firebase from "firebase";
import React from "react";

let firebasetoken;
let weclikdtoken;
let CurrentUserId;

export const getFirebaseToken = async () => {
  if (firebase.auth().currentUser) {
    await firebase
      .auth()
      .currentUser.getIdToken(true)
      .then(async idToken => {
        localStorage.setItem("userIdTokenFirebase", idToken);
        firebasetoken = idToken;
      })
      .catch(error => {
        console.log(error);
      });
  } else {
    firebasetoken = await localStorage.getItem("userIdTokenFirebase");
  }
  return firebasetoken;
};

export const getWeclikdToken = async () => {
  weclikdtoken = await localStorage.getItem("userIdTokenWeclikd");
  return weclikdtoken;
};

export const getMyUserId = async () => {
  CurrentUserId = await localStorage.getItem("MyUserUserId");
  return CurrentUserId;
};

export const getLocalStorage = async key => {
  if (!key) {
    return "Please send local storage key name";
  }
  const data = await localStorage.getItem(key);
  return data;
};

export const setLocalStorage = async (key, value = null) => {
  if (!key) {
    return "Please send local storage key name";
  }
  await localStorage.setItem(key, value);
  return true;
};

export const dataURItoBlob = async dataURI => {
  let byteString;
  if (dataURI.split(",")[0].indexOf("base64") >= 0)
    byteString = await atob(dataURI.split(",")[1]);
  else byteString = unescape(dataURI.split(",")[1]);
  let mimeString = dataURI
    .split(",")[0]
    .split(":")[1]
    .split(";")[0];
  let ia = new Uint8Array(byteString.length);
  for (let i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }
  return await new Blob([ia], { type: mimeString });
};

export const capitalizeFirstLetter = string => {
  if (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
};

export const isLoggedIn =()=>{
  return localStorage.getItem("userIdTokenFirebase")
}
