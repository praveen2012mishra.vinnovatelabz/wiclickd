import * as firebase from "firebase";

// dev
const firebaseConfig = {
  apiKey: "AIzaSyBNvlsGvHp-KPCj1KMTswQjLOsjHiy3_Q8",
  authDomain: "electric-block-241402.firebaseapp.com",
  databaseURL: "https://electric-block-241402.firebaseio.com",
  projectId: "electric-block-241402",
  storageBucket: "electric-block-241402.appspot.com",
  messagingSenderId: "519921550901",
  appId: "1:519921550901:web:cd515d8bfb9a52c4f2537c",
  measurementId: "G-QZMNJ3HRLG"
};

// staging
// const firebaseConfig = {
//   apiKey: "AIzaSyDw7qUzCmzCWIBGZQmzkiRsX-Aj1130C0M",
//   authDomain: "graphite-tesla-246507.firebaseapp.com",
//   databaseURL: "https://graphite-tesla-246507.firebaseio.com",
//   projectId: "graphite-tesla-246507",
//   storageBucket: "graphite-tesla-246507.appspot.com",
//   messagingSenderId: "746359675799",
//   appId: "1:746359675799:web:e990d5faefe22a613e02d2",
//   appDynamicLink: "https://weclikdalpha.page.link/links",
//   measurementId: "G-7NVE1NMR1B"
// };

firebase.initializeApp(firebaseConfig);

export default firebase;
