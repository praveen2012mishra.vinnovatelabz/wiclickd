import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router,MemoryRouter } from "react-router-dom";
import { ApolloProvider } from "react-apollo";
import applloClient from "./client";
// window.addEventListener('DOMContentLoaded', (event) => {
//   if(localStorage.getItem("userIdTokenFirebase"))
//   window.parent.postMessage({type:"wecklid_login",userIdTokenFirebase:localStorage.getItem("userIdTokenFirebase")}, '*');
//   else
//   window.parent.postMessage({type:"wecklid_logout",}, '*');
// });
ReactDOM.render(
  <React.StrictMode>
    <MemoryRouter>
      <ApolloProvider client={applloClient}>
        <App />
      </ApolloProvider>
    </MemoryRouter>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
