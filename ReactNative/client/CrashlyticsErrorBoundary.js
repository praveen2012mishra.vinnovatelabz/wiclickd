import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  Image,
  TouchableHighlight
} from "react-native";
import { Button } from "react-native-elements";
import ConstantFontFamily from "./constants/FontFamily";
//import ButtonStyle from "../constants/ButtonStyle";
import ButtonStyle from "./constants/ButtonStyle";
import NavigationService from "./library/NavigationService";

class CrashlyticsErrorBoundary extends Component {
  componentDidMount = async () => {
    window.onerror = function(message, source, lineno, colno, error) {
      if (error || error.name === "ChunkLoadError") {
        window.location.reload(true);
      }
    };
  };
  render() {
    return (
      <ScrollView
        style={{
          flex: 1
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            marginVertical: 20,
            minHeight: 530
          }}
        >
          {/* <Text
            style={{
              marginVertical: 10
            }}
          >
            Something went wrong!
          </Text>
          <Text
            style={{
              marginVertical: 10
            }}
          >
            {this.props.error && this.props.error.toString()}
          </Text>
          <Button onPress={this.props.resetError} title={"Try again"} /> */}
          <View style={{ width: "60%" }}>
            <Text
              style={{
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontWeight: "bold",
                fontSize: 27,
                textAlign: "center"
              }}
            >
              This application may have updated or crashed.
            </Text>
            <Text
              style={{
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontWeight: "bold",
                fontSize: 27,
                textAlign: "center"
              }}
            >
              Please refresh your browser to
            </Text>
            <Text
              style={{
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontWeight: "bold",
                fontSize: 27,
                textAlign: "center"
              }}
            >
              see the latest content
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              width: "100%"
            }}
          >
            <Button
              onPress={() => {
                NavigationService.navigate("home"), window.location.reload();
              }}
              color="#000"
              title="Refresh"
              titleStyle={ButtonStyle.titleStyle}
              buttonStyle={ButtonStyle.backgroundStyle}
              containerStyle={ButtonStyle.containerStyle}
            />
          </View>

          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Image
              source={require("./assets/image/logo.png")}
              style={{
                height: 30,
                width: 30,
                justifyContent: "flex-start"
              }}
              resizeMode={"contain"}
            />
            <Text
              style={{
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontWeight: "bold",
                fontSize: 22,
                textAlign: "center",
                paddingLeft:2
              }}
            >
              weclikd
            </Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default CrashlyticsErrorBoundary;
