import React, { Component, lazy, Suspense } from "react";
import {
  Animated,
  Dimensions,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Icon, Tooltip } from "react-native-elements";
import { TabBar, TabView } from "react-native-tab-view";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import applloClient from "../client";
import CreateCommentCard from "../components/CreateCommentCard";
import PostDetailsComment from "../components/PostDetailsComment";
import SEOMetaData from "../components/SEOMetaData";
import ShadowSkeleton from "../components/ShadowSkeleton";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import getEnvVars from "../environment";
import {
  AdminPostQuery,
  PostQuery
} from "../graphqlSchema/graphqlMutation/PostMutation";
import {
  SearchClikMutation,
  SearchFeedMutation,
  SearchTopicMutation,
  SearchUserMutation
} from "../graphqlSchema/graphqlMutation/SearchMutation";
import { PostQueryVariables } from "../graphqlSchema/graphqlVariables/PostVariables";
import {
  SearchClikVariables,
  SearchFeedVariables,
  SearchTopicVariables,
  SearchUserVariables
} from "../graphqlSchema/graphqlVariables/SearchVariables";
import { capitalizeFirstLetter } from "../library/Helper";
import { retry } from "../library/Helper";
import HeaderRight from "../components/HeaderRight";
import ButtonStyle from "../constants/ButtonStyle";
import Overlay from "react-native-modal-overlay";
import Modal from "modal-enhanced-react-native-web";
import CommentDetailScreen from "./CommentDetailScreen";
import BottomScreenForDiscussion from "../components/BottomScreenForDiscussion";
import NavigationService from "../library/NavigationService";

const PostDetailsCard = lazy(() =>
  retry(() => import("../components/PostDetailsCard"))
);

const initRoutes = [
  { key: "first", title: "Trending" },
  { key: "second", title: "New" },
  { key: "third", title: "Bookmarks" }
];
var keys = { 37: 1, 38: 1, 39: 1, 40: 1 };
var wheelOpt = { passive: false };
var wheelEvent =
  "onwheel" in document.createElement("div") ? "wheel" : "mousewheel";

var timer = null;
//const $body = document.querySelector('body');
const $body = Platform.OS == "web" && document.querySelector("body");
let scrollPosition = 0;
class PostDetailsScreen extends Component {
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.timer;
    this.Pagescrollview = null;
    this.state = {
      listCommentList: [],
      PostDetails: null,
      sortby: "",
      parentEndCursor: "",
      hasNextPage: true,
      cliksPostListItem: "",
      modalVisible: false,
      setItems: undefined,
      items: [],
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      mainCommentShow: this.props.setComment,
      page: 1,
      refreshing: false,
      loadingMore: false,
      apiCall: true,
      postId: "",
      showSearchDiscussionsTooltip: false,
      routes: [...initRoutes],
      index: 0,
      term: "",
      TopicSuggestList: [],
      ClikSuggestList: [],
      UserSuggestList: [],
      FeedSuggestList: [],
      scrollY: 0,
      postDetailsViewHeight: 0,
      topDownNavTrack: {},
      gestureStatus: false,
      preventScroll: false,
      feedY: 0
    };
  }

  // ============

  preventDefault(e) {
    if (e.cancelable) {
      e.preventDefault();
    }
  }

  preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
      this.preventDefault(e);
      return false;
    }
  }

  // modern Chrome requires { passive: false } when adding event
  // try {
  //   window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
  //     get: function () { supportsPassive = true; }
  //   }));
  // } catch (e) { }

  // call this to Disable
  disableScroll = () => {
    document.addEventListener("DOMMouseScroll", this.preventDefault, false); // older FF
    document.addEventListener(wheelEvent, this.preventDefault, wheelOpt); // modern desktop
    document.addEventListener("touchmove", this.preventDefault, wheelOpt); // mobile
    document.addEventListener(
      "keydown",
      this.preventDefaultForScrollKeys,
      false
    );

    // let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    // let scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;

    // // if any scroll is attempted, set this to the previous value
    // window.onscroll = function () {
    //   window.scrollTo(scrollLeft, scrollTop);
    // };

    // $body.style.removeProperty('overflow');
    // $body.style.removeProperty('position');
    // $body.style.removeProperty('top');
    // $body.style.removeProperty('width');
    // window.scrollTo(0, scrollPosition);

    // $body.style.removeProperty('overflow');
  };

  // call this to Enable
  enableScroll = () => {
    document.removeEventListener("DOMMouseScroll", this.preventDefault, false);
    document.removeEventListener(wheelEvent, this.preventDefault, wheelOpt);
    document.removeEventListener("touchmove", this.preventDefault);
    document.removeEventListener(
      "keydown",
      this.preventDefaultForScrollKeys,
      false
    );

    // window.onscroll = function () {

    // };

    // scrollPosition = window.pageYOffset;
    // $body.style.overflow = 'hidden';
    // $body.style.position = 'fixed';
    // $body.style.top = `-${scrollPosition}px`;
    // $body.style.width = '100%';

    // $body.style.overflow = 'hidden';
  };
  // ===========
  handalListMode = async (position, data) => {
    await this.props.setPostCommentDetails({
      id: this.state.postId.replace("%3A", ":")
    });
  };

  setNewBorderColor = async (selectComment, isNavTo) => {
    const prevId = this.props.selectComment.prevId;
    setTimeout(async () => {
      let prevElementOfView = document.getElementById(prevId);
      let slectElementOfView = document.getElementById(selectComment);
      let elementOfTouchableOpacity = document.getElementById(
        "Comment:" + selectComment
      );
      if (slectElementOfView) {
        if (prevElementOfView) prevElementOfView.style.borderColor = "white";
        slectElementOfView.style.borderColor = "red";
        let prevPosition = prevElementOfView
          ? prevElementOfView.getBoundingClientRect().top
          : 0;
        let currentPosition = slectElementOfView.getBoundingClientRect().top;
        if (prevPosition != currentPosition) {
          slectElementOfView.scrollIntoView({
            block: "center",
            behavior: "smooth"
          });
        }
        if (elementOfTouchableOpacity) {
          elementOfTouchableOpacity.focus();
          elementOfTouchableOpacity.style.outline = "none";
        }
      }
    }, 0);
  };
  getFlatCommentList = items => {
    let item, output, clone;
    output = [];
    for (item of items) {
      clone = [
        ...output,
        { ...item },
        [...this.getFlatCommentList(item.comments.edges)]
      ];
      output = clone;
    }
    return output;
  };
  handleKeyup = e => {
    if (e.target.tagName !== "TEXTAREA") {
      const commentList = this.getFlatCommentList(
        this.props.PostCommentDetails
      ).flat(Infinity);
      let comment = commentList.find(
        comment =>
          "Comment:" + this.props.selectComment.currentId == comment.node.id
      );
      if ((event.keyCode == 32 || event.keyCode == 83) && !comment) {
        this.setState({ isStartNav: true });
        this.props.setActiveId(commentList[0].node.id.replace("Comment:", ""));
        this.setNewBorderColor(commentList[0].node.id.replace("Comment:", ""));
      }
      if (
        [39, 68, 37, 65, 83, 40, 87, 38].indexOf(event.keyCode) !== -1 &&
        this.props.selectComment.currentId
      ) {
        let nextSelectId = null;
        let isNavTo = false;
        if (comment) {
          let currentActiveElement = document.getElementById(
            this.props.selectComment.currentId
          );
          if (event.keyCode == 39 || event.keyCode == 68) {
            nextSelectId = comment.right;
            if (currentActiveElement) {
              currentActiveElement.setAttribute("navTo", "right");
              currentActiveElement.click();
              isNavTo = true;
            }
          }
          if (event.keyCode == 37 || event.keyCode == 65) {
            nextSelectId = comment.left;
            if (currentActiveElement) {
              currentActiveElement.setAttribute("navTo", "left");
              currentActiveElement.click();
              isNavTo = true;
            }
          }
          if (event.keyCode == 83 || event.keyCode == 40) {
            if (
              this.state.topDownNavTrack[this.props.selectComment.currentId] &&
              this.state.topDownNavTrack[this.props.selectComment.currentId]
                .down
            )
              nextSelectId = this.state.topDownNavTrack[
                this.props.selectComment.currentId
              ].down;
            else if (comment.down) {
              nextSelectId = comment.down;
              this.setState(prevState => {
                return {
                  topDownNavTrack: {
                    ...prevState.topDownNavTrack,
                    [nextSelectId.replace("Comment:", "")]: {
                      top: this.props.selectComment.currentId
                    }
                  }
                };
              });
              isNavTo = false;
            }
          }
          if (event.keyCode == 87 || event.keyCode == 38) {
            if (comment.up) {
              nextSelectId = comment.up;
              this.setState(prevState => {
                let setKeyObj = prevState.topDownNavTrack[
                  nextSelectId.replace("Comment:", "")
                ]
                  ? {
                    ...prevState.topDownNavTrack[
                    nextSelectId.replace("Comment:", "")
                    ],
                    down: this.props.selectComment.currentId
                  }
                  : { down: this.props.selectComment.currentId };
                return {
                  topDownNavTrack: {
                    ...prevState.topDownNavTrack,
                    [nextSelectId.replace("Comment:", "")]: setKeyObj
                  }
                };
              });
            } else if (
              this.state.topDownNavTrack[this.props.selectComment.currentId] &&
              this.state.topDownNavTrack[this.props.selectComment.currentId].top
            )
              nextSelectId = this.state.topDownNavTrack[
                this.props.selectComment.currentId
              ].top;
            isNavTo = false;
          }

          if (nextSelectId && nextSelectId != "null") {
            this.props.setActiveId(nextSelectId.replace("Comment:", ""));
            this.setNewBorderColor(
              nextSelectId.replace("Comment:", ""),
              isNavTo
            );
          }
        }
      }
    }
  };

  async componentDidMount() {
    this.props.setActiveId("");
    this.props.searchOpenBarStatus(false);
    // document.body.addEventListener("keyup", this.handleKeyup);
    this.roleWiseTabUpdate();
    this.props.openCreateComment(false)
    const { navigation } = this.props;
    const itemId = "Post:" + navigation.getParam("id", "NO-ID");

    // if (this.props.postId){
    //   this.setState({
    //     postId: itemId
    //   });
    // } else {
    //   this.setState({
    //     postId: this.props.TrendingHomeFeed[0] && this.props.TrendingHomeFeed[0].node.id
    //   });
    // }

    await this.goToPostDetailsScreen(itemId.replace("%3A", ":"));
    this.timer = setTimeout(() => {
      this.setState({
        favColor: "red"
      });
    }, 1000);
    //window.addEventListener('touchstart', this.touchStart);
    //window.addEventListener('touchmove', this.preventTouch, {passive: false});
  }

  setGestureState = event => {
    window.addEventListener("touchstart", this.touchStart);
    window.addEventListener("touchmove", this.preventTouch, { passive: false });
  };

  componentWillUnmount() {
    window.removeEventListener("touchstart", e => this.touchStart(e));
    window.removeEventListener("touchmove", e => this.preventTouch(e), {
      passive: false
    });
  }

  touchStart = e => {
    this.firstClientX = e.touches[0].clientX;
    this.firstClientY = e.touches[0].clientY;
  };

  preventTouch = e => {
    const minValue = 5; // threshold
    this.clientX = e.touches[0].clientX;
    this.clientY = e.touches[0].clientY;
    Math.abs(this.clientX) > minValue;
    //this.setState({gestureStatus:Math.abs(this.clientX) > minValue})
    //console.log('------------------------->',Math.abs(this.clientX) > minValue);
  };

  roleWiseTabUpdate = () => {
    if (this.props.isAdmin && this.props.isAdminView) {
      this.setState({
        routes: [{ key: "zero", title: "Reported" }, ...initRoutes]
      });
    } else if (this.state.routes.length === 4) {
      this.setState({ routes: initRoutes });
    }
  };

  componentWillUnmount() {
    this._isMounted = false;
    clearTimeout(this.timer);
    document.body.removeEventListener("keyup", this.handleKeyup);
  }

  goToPostDetailsScreen = async id => {
    let __self = this;
    PostQueryVariables.variables.id = id;
    PostQueryVariables.variables.first = 3;
    applloClient
      .query({
        query: this.props.isAdmin == true ? AdminPostQuery : PostQuery,
        ...PostQueryVariables,
        fetchPolicy: "cache-first"
      })
      .then(async res => {
        let title = "Home";
        if (__self.props.navigation.state.routeName == "profile") {
          title = "@" + res.data.post.author.username;
        } else if (__self.props.navigation.state.routeName == "cliksprofile") {
          title = "#" + res.data.post.cliks[0];
        } else {
          title = "Home";
        }
        await __self.props.setPostDetails({
          title: title,
          id: id,
          navigate: true
        });
        await __self.props.setPostCommentDetails({
          id: id,
          title: res.data.post.title,
          loading: true
        });
        return true;
      })
      .then(async res => {
        if (res) {
          await this.getTrandingCliks();
        }
      });
  };

  getTrandingCliks = async () => {
    let data = [
      {
        label: "Everyone",
        value: "",
        key: null
      }
    ];
    if (this.props.PostDetails) {
      if (this.props.PostDetails.post.cliks != null) {
        await this.props.PostDetails.post.cliks.map((item, index) => {
          data.push({
            label: "#" + capitalizeFirstLetter(item),
            value: item,
            key: index
          });
        });
      }
    }
    await this.setState({
      items: data
    });
  };

  onClose = () => {
    this.setState({
      modalVisible: false
    });
  };

  clickEvent = () => {
    this.setState({
      mainCommentShow: !this.state.mainCommentShow
    });
  };

  componentDidUpdate(prevProps) {
    if (this.props.getHasScrollTop == true && this.Pagescrollview) {
      this.Pagescrollview.scrollTo({ x: 0, y: 0, animated: true });
      this.props.setHASSCROLLEDACTION(false);
    }
    if (prevProps.isAdminView !== this.props.isAdminView) {
      this.roleWiseTabUpdate();
    }
  }

  onClose = () => {
    this.setState({
      mainCommentShow: !this.state.mainCommentShow
    });
  };

  listScroll = value => {
    this.setState({
      feedY: value
    });
  };

  getRespectiveSuggestions = async value => {
    var TotalTerm = value.split(" ");
    for (var i = 0; i < TotalTerm.length; i++) {
      if (TotalTerm[i].startsWith("#") == true || this.state.index == 2) {
        await this.setState({
          TopicSuggestList: [],
          UserSuggestList: [],
          FeedSuggestList: []
        });
        await this.getClikSuggestion(TotalTerm[i].replace("#", ""));
      } else if (
        TotalTerm[i].startsWith("/") == true ||
        this.state.index == 1
      ) {
        await this.setState({
          ClikSuggestList: [],
          UserSuggestList: [],
          FeedSuggestList: []
        });
        await this.getTopicSuggestion(TotalTerm[i].replace("/", ""));
      } else if (
        TotalTerm[i].startsWith("@") == true ||
        this.state.index == 3
      ) {
        await this.setState({
          TopicSuggestList: [],
          ClikSuggestList: [],
          FeedSuggestList: []
        });
        await this.getUserSuggestion(TotalTerm[i].replace("@", ""));
      } else if (
        TotalTerm[i].startsWith("%") == true ||
        this.state.index == 4
      ) {
        await this.setState({
          TopicSuggestList: [],
          ClikSuggestList: [],
          UserSuggestList: []
        });
        await this.getFeedSuggestion(TotalTerm[i].replace("%", ""));
      } else {
        await this.setState({
          TopicSuggestList: [],
          ClikSuggestList: [],
          UserSuggestList: [],
          FeedSuggestList: []
        });
      }
    }
  };

  getTopicSuggestion = value => {
    SearchTopicVariables.variables.prefix = value;
    applloClient
      .query({
        query: SearchTopicMutation,
        ...SearchTopicVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        let data = res.data.search.topics;
        this.setState({
          TopicSuggestList: data,
          ClikSuggestList: [],
          UserSuggestList: [],
          FeedSuggestList: []
        });
      });
  };

  getClikSuggestion = value => {
    SearchClikVariables.variables.prefix = value;
    applloClient
      .query({
        query: SearchClikMutation,
        ...SearchClikVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        let data = res.data.search.cliks;
        this.setState({
          ClikSuggestList: data,
          TopicSuggestList: [],
          UserSuggestList: [],
          FeedSuggestList: []
        });
      });
  };

  getUserSuggestion = value => {
    SearchUserVariables.variables.prefix = value;
    applloClient
      .query({
        query: SearchUserMutation,
        ...SearchUserVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        let data = res.data.search.users;
        this.setState({
          UserSuggestList: data,
          TopicSuggestList: [],
          ClikSuggestList: [],
          FeedSuggestList: []
        });
      });
  };

  getFeedSuggestion = value => {
    SearchFeedVariables.variables.prefix = value.replace("-", " ");
    applloClient
      .query({
        query: SearchFeedMutation,
        ...SearchFeedVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        let data = res.data.search.feeds;
        this.setState({
          FeedSuggestList: data,
          TopicSuggestList: [],
          ClikSuggestList: [],
          UserSuggestList: []
        });
      });
  };
  toggleTooltip = stateName => {
    this.setState(prevState => ({ [stateName]: !prevState[stateName] }));
  };
  _renderLazyPlaceholder = ({ route }) => <ShadowSkeleton />;
  _renderTabBar = props => (
    Dimensions.get("window").width >= 750 &&
    <View
      style={
        this.state.scrollY >= this.state.postDetailsViewHeight
          ? [
            styles.header,
            {
              top:
                this.state.scrollY >= this.state.postDetailsViewHeight
                  ? this.state.scrollY
                  : 0
            }
          ]
          : null
      }
    >
      <View
        style={{
          flexDirection: "row",
          width: "100%",
          backgroundColor: "#fff",
          height: 30,
          justifyContent: "space-between"
        }}
      >
        <View
          style={{
            width: "2%"
          }}
        ></View>
        <TabBar
          {...props}
          indicatorStyle={{
            backgroundColor: "#009B1A",
            height: 3,
            borderRadius: 6
          }}
          style={{
            backgroundColor: "transparent",
            minWidth: "90%",
            shadowColor: "transparent",
            height: 30,
            justifyContent: 'center'
          }}
          labelStyle={{
            color: "#000",
            fontFamily: ConstantFontFamily.MontserratBoldFont,
            fontSize: 13,
            fontWeight: "bold"
          }}
          renderIndicator={({ route, focused, color }) => null}
        />
        <TouchableOpacity
          style={{
            justifyContent: "center",
            alignItems: "center"
          }}
        ></TouchableOpacity>
      </View>

      <View>
        {/* {this.state.mainCommentShow == true && ( */}
        <View
          style={{
            flexDirection: "row",
            borderWidth: 1,
            // borderColor: "#e1e1e1",
            borderColor: "red",
            borderRadius: 6,
            marginVertical: 10
          }}
        >
          <CreateCommentCard
            onClose={this.onClose}
            // parent_content_id={this.state.postId}
            parent_content_id={
              this.props.PostId &&
              this.props.PostId.replace("Trending", "")
                .replace("New", "")
                .replace("Discussion", "")
                .replace("Search", "")
            }
            closeModalBySubmit={this.clickEvent}
            clickList={
              this.props.PostDetails ? this.props.PostDetails.post.cliks : null
            }
            initial="main"
            topComment={this.props.PostDetails && this.props.PostDetails.post}
            navigation={this.props.navigation}
          />
        </View>
        {/* )} */}
      </View>
    </View>
  );
  stopScrolling = prvntScroll => {
    if (prvntScroll) {
      this.disableScroll();
    } else {
      this.enableScroll();
    }
    // this.setState({ preventScroll: true })
  };
  _handleIndexChange = index => this.setState({ index });
  _renderScene = ({ route }) => {
    switch (route.key) {
      case "third":
        return (
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={{ marginTop: 10 }}
          >
            <PostDetailsComment
              item={this.props.PostCommentDetails}
              navigation={this.props.navigation}
              closeModalhandalListMode={this.handalListMode}
              clickList={
                this.props.PostCommentDetails
                  ? this.props.PostCommentDetails
                  : null
              }
              PostId={
                this.props.PostDetails ? this.props.PostDetails.post.id : null
              }
            />
            <View style={{ height: 100 }}></View>
            {this.props.PostCommentDetails.length == 0 && (
              <View
                style={{
                  flexDirection: "column",
                  height: 100
                }}
              >
                <View
                  style={{
                    width: "100%",
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center"
                    }}
                    reverse
                    name="comments"
                    type="font-awesome"
                    size={20}
                    containerStyle={{
                      alignSelf: "center"
                    }}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    width: "100%",
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.defaultFont,
                      color: "#000",
                      alignSelf: "center"
                    }}
                  >
                    No Discussions
                  </Text>
                </View>
              </View>
            )}
          </ScrollView>
        );
      case "second":
        return (
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={{ marginTop: 10 }}
          >
            <PostDetailsComment
              item={this.props.PostCommentDetails}
              navigation={this.props.navigation}
              closeModalhandalListMode={this.handalListMode}
              clickList={
                this.props.PostCommentDetails
                  ? this.props.PostCommentDetails
                  : null
              }
              PostId={
                this.props.PostDetails ? this.props.PostDetails.post.id : null
              }
            />
            <View style={{ height: 100 }}></View>
            {this.props.PostCommentDetails.length == 0 && (
              <View
                style={{
                  flexDirection: "column",
                  height: 100
                }}
              >
                <View
                  style={{
                    width: "100%",
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center"
                    }}
                    reverse
                    name="comments"
                    type="font-awesome"
                    size={20}
                    containerStyle={{
                      alignSelf: "center"
                    }}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    width: "100%",
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.defaultFont,
                      color: "#000",
                      alignSelf: "center"
                    }}
                  >
                    No Discussions
                  </Text>
                </View>
              </View>
            )}
          </ScrollView>
        );
      default:
        return (
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={{
              marginTop: 10,
              height: Dimensions.get("window").height - 200
            }}
          >
            <PostDetailsComment
              item={this.props.PostCommentDetails}
              navigation={this.props.navigation}
              closeModalhandalListMode={this.handalListMode}
              clickList={
                this.props.PostCommentDetails
                  ? this.props.PostCommentDetails
                  : null
              }
              PostId={
                this.props.PostDetails ? this.props.PostDetails.post.id : null
              }
            />

            <View style={{ height: 100 }}></View>

            {this.props.PostCommentDetails.length == 0 && (
              <View
                style={{
                  flexDirection: "column",
                  height: 100
                }}
              >
                <View
                  style={{
                    width: "100%",
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center"
                    }}
                    reverse
                    name="comments"
                    type="font-awesome"
                    size={20}
                    containerStyle={{
                      alignSelf: "center"
                    }}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    width: "100%",
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.defaultFont,
                      color: "#000",
                      alignSelf: "center"
                    }}
                  >
                    No Discussions
                  </Text>
                </View>
              </View>
            )}
          </ScrollView>
        );
    }
  };

  onCloseCreateComment = () => {
    this.props.openCreateComment(false)
  }
  render() {
    return (
      <View style={styles.container}>
        {/* {this.props.getCreateCommentModalStatus &&
            <Modal
            isVisible={this.props.getCreateCommentModalStatus}
            navigation={this.props.navigation}
            onBackdropPress={this.onCloseCreateComment}
            style={{
              marginHorizontal:
                Dimensions.get("window").width > 750 ? "30%" : 0,
              padding: 0
            }}              
            >
                <CreateCommentCard
                  onClose={this.onClose}
                  parent_content_id={
                    this.props.PostId &&
                    this.props.PostId.replace("Trending", "")
                      .replace("New", "")
                      .replace("Discussion", "")
                      .replace("Search", "")
                  }
                  closeModalBySubmit={this.clickEvent}
                  clickList={
                    this.props.PostDetails ? this.props.PostDetails.post.cliks : null
                  }
                  initial="main"
                  topComment={this.props.PostDetails && this.props.PostDetails.post}
                  navigation={this.props.navigation}
                />

            </Modal>
          } */}
        {Platform.OS == "web" && (
          <SEOMetaData
            title={
              this.props.PostDetails ? this.props.PostDetails.post.title : ""
            }
            description={
              this.props.PostDetails ? this.props.PostDetails.post.summary : ""
            }
            image={
              this.props.PostDetails
                ? this.props.PostDetails.post.thumbnail_pic
                : ""
            }
          />
        )}
        {this.state.term != "" && (
          <View
            style={{
              zIndex: 5,
              alignSelf: "flex-start",
              width: "31.6%",
              marginRight: 20,
              position: "absolute",
              backgroundColor: "#FEFEFA",
              top: 60,
              right: "2.1%",
              padding:
                this.state.TopicSuggestList.length > 0 ||
                  this.state.ClikSuggestList.length > 0 ||
                  this.state.UserSuggestList.length > 0 ||
                  this.state.FeedSuggestList.length > 0
                  ? 10
                  : 0,
              borderWidth:
                this.state.TopicSuggestList.length > 0 ||
                  this.state.ClikSuggestList.length > 0 ||
                  this.state.UserSuggestList.length > 0 ||
                  this.state.FeedSuggestList.length > 0
                  ? 1
                  : 0,
              borderColor: "#000",
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6
            }}
          >
            {this.state.TopicSuggestList.map((item, index) => {
              return (
                <View
                  key={item.name}
                  style={{
                    backgroundColor: "#FEFEFA",
                    width: "95%",
                    padding: 5,
                    justifyContent: "space-between",
                    alignSelf: "flex-start",
                    flexDirection: "row",
                    height: 30
                  }}
                >
                  <View
                    style={{
                      padding: 5,
                      backgroundColor: item.parents ? "#e3f9d5" : "#e3f9d5",
                      borderRadius: 6,
                      alignSelf: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        color: item.parents ? "#009B1A" : "#009B1A",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        fontWeight: "bold"
                      }}
                    >
                      /{item.name.toLowerCase()}
                    </Text>
                  </View>
                </View>
              );
            })}

            {this.state.ClikSuggestList.map((item, index) => {
              return (
                <View
                  key={item.name}
                  style={{
                    backgroundColor: "#FEFEFA",
                    width: "95%",
                    padding: 5,
                    justifyContent: "space-between",
                    alignSelf: "flex-start",
                    flexDirection: "row",
                    height: 30
                  }}
                >
                  <View
                    style={{
                      padding: 5,
                      backgroundColor: "#E8F5FA",
                      borderRadius: 6,
                      alignSelf: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        color: "#4169e1",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        fontWeight: "bold"
                      }}
                    >
                      #{item.name}
                    </Text>
                  </View>
                </View>
              );
            })}

            {this.state.UserSuggestList.map((item, index) => {
              return (
                <View
                  key={item.username}
                  style={{
                    backgroundColor: "#FEFEFA",
                    width: "95%",
                    padding: 5,
                    justifyContent: "space-between",
                    alignSelf: "flex-start",
                    flexDirection: "row",
                    height: 30
                  }}
                >
                  <View
                    style={{
                      padding: 5,
                      backgroundColor: "#E8F5FA",
                      borderRadius: 6,
                      alignSelf: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        color: "#000",
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontWeight: "bold"
                      }}
                    >
                      @{item.username}
                    </Text>
                  </View>
                </View>
              );
            })}

            {this.state.FeedSuggestList.map((item, index) => {
              return (
                <View
                  key={item.id}
                  style={{
                    backgroundColor: "#FEFEFA",
                    width: "95%",
                    padding: 5,
                    justifyContent: "space-between",
                    alignSelf: "flex-start",
                    flexDirection: "row",
                    height: 30
                  }}
                >
                  <View style={{ flexDirection: "row", alignSelf: "center" }}>
                    <Text
                      style={{
                        alignSelf: "center",
                        textAlign: "center",
                        color: "#000",
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontWeight: "bold",
                        marginLeft: 10
                      }}
                    >
                      {item.name}
                    </Text>
                  </View>
                </View>
              );
            })}
          </View>
        )}
        {//Platform.OS !== "web"
          Dimensions.get("window").width <= 750 ? (
            <Animated.View
              style={{
                left: 0,
                right: 0,
                zIndex: 10,
                overflow: "hidden",
                borderRadius: Dimensions.get("window").width <= 750 ? 0 : 6
              }}
            >
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <View
                  style={{
                    width: "100%",
                    height: 50,
                    flexDirection: "row",
                    alignItems: "center",
                    marginBottom: Dimensions.get("window").width <= 750 ? 0 : 10,
                    backgroundColor: "#000",
                    borderRadius: Dimensions.get("window").width <= 750 ? 0 : 6
                  }}
                >
                  <TouchableOpacity
                    style={[ButtonStyle.headerBackStyle, {
                      width: "5%",
                      flexDirection: "row",
                      justifyContent: "flex-start",
                      alignSelf: "center",
                    }]}
                    onPress={() => {
                      let nav = this.props.navigation.dangerouslyGetParent()
                        .state;
                      if (nav.routes.length > 1) {
                        this.props.navigation.goBack();
                        return;
                      } else {
                        this.props.navigation.navigate("home");
                      }
                    }}
                  >
                    <Icon
                      color={"#fff"}
                      name="angle-left"
                      type="font-awesome"
                      size={40}
                    //iconStyle={{ paddingLeft: 6 }}
                    />
                  </TouchableOpacity>
                  {!this.props.getsearchBarStatus &&
                    <TouchableOpacity
                      style={{
                        flex: 1,
                        justifyContent: "center",
                        // marginHorizontal: 10,
                      }}
                    >
                      <Text
                        style={{
                          //textAlign: 'center',
                          color: "#fff",
                          fontWeight: "bold",
                          fontSize: 18,
                          fontFamily: ConstantFontFamily.MontserratBoldFont
                        }}
                      >
                        Discussions
                  </Text>
                    </TouchableOpacity>
                  }
                  <View
                    style={ButtonStyle.headerRightStyle}
                  >
                    <HeaderRight navigation={this.props.navigation} />
                  </View>
                </View>
              </View>
            </Animated.View>
          ) : null}
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            width: "100%",
            justifyContent: "center"
          }}
        >
          <ScrollView
            ref={scrollview => {
              this.Pagescrollview = scrollview;
            }}
            showsVerticalScrollIndicator={false}
            onLayout={event => {
              let { x, y, width, height } = event.nativeEvent.layout;
              if (width < 1024) {
                this.setState({
                  showsVerticalScrollIndicatorView: true,
                  currentScreentWidth: width
                });
              } else {
                this.setState({
                  showsVerticalScrollIndicatorView: false,
                  currentScreentWidth: width
                });
              }
            }}
            style={{ flex: 1 }}
            contentContainerStyle={{
              width:
                Platform.OS == "web" && Dimensions.get("window").width >= 750
                  ? 450
                  : "100%"
            }}
            onScroll={event => {
              this.setState({
                scrollY: event.nativeEvent.contentOffset.y
              });
              if (timer !== null) {
                clearTimeout(timer);
                //console.log("false");
                this.setState({ gestureStatus: false });
              }
              let _self = this;
              timer = setTimeout(function () {
                // do something
                //console.log("true");
                _self.setState({ gestureStatus: true });
              }, 150);
            }}
            scrollEventThrottle={16}
          >
            <View
              style={{
                borderRadius: 6,
                marginBottom: 10,
                width: '100%'
              }}
              onLayout={event => {
                let { x, y, width, height } = event.nativeEvent.layout;
                if (width > 0) {
                  this.setState({ postDetailsViewHeight: height });
                }
              }}
            >
              {this.props.PostDetails != null && (
                <Suspense fallback={<ShadowSkeleton />}>
                  <PostDetailsCard
                    item={this.props.PostDetails}
                    closeModalhandalListMode={this.handalListMode}
                    navigation={this.props.navigation}
                    clickEvent={this.clickEvent}
                    modalStatus={this.state.mainCommentShow}
                  />
                </Suspense>
              )}
              {/* <View
              style={{
                height: 1,
                backgroundColor: "#fff",
                marginTop: 10
              }}
            ></View> */}
            </View>
            {/* <View
            style={{
              flexDirection: "row",
              borderWidth: 1,
              borderColor: "#e1e1e1",
              borderRadius: 6,
              marginVertical: 10
            }}
          > */}
            {Dimensions.get('window').width >= 750 || this.state.mainCommentShow || this.props.getShareLinkModalStatus &&
              <CreateCommentCard
                onClose={this.onClose}
                // parent_content_id={this.state.postId}
                parent_content_id={
                  this.props.PostId &&
                  this.props.PostId.replace("Trending", "")
                    .replace("New", "")
                    .replace("Discussion", "")
                    .replace("Search", "")
                }
                closeModalBySubmit={this.clickEvent}
                clickList={
                  this.props.PostDetails
                    ? this.props.PostDetails.post.cliks
                    : null
                }
                initial="main"
                topComment={this.props.PostDetails && this.props.PostDetails.post}
                navigation={this.props.navigation}
              />
            }
            { Dimensions.get('window').width <= 750 && this.props.getCreateCommentModalStatus &&
              <CreateCommentCard
                onClose={this.onClose}
                // parent_content_id={this.state.postId}
                parent_content_id={
                  this.props.PostId &&
                  this.props.PostId.replace("Trending", "")
                    .replace("New", "")
                    .replace("Discussion", "")
                    .replace("Search", "")
                }
                closeModalBySubmit={this.clickEvent}
                clickList={
                  this.props.PostDetails
                    ? this.props.PostDetails.post.cliks
                    : null
                }
                initial="main"
                topComment={this.props.PostDetails && this.props.PostDetails.post}
                navigation={this.props.navigation}
              />
            }
            {/* </View> */}
            <ScrollView
              removeClippedSubview={false}
              showsVerticalScrollIndicator={false}
              style={{
                display:
                  Dimensions.get("window").width <= 750
                    ? "flex"
                    : "none",
                marginHorizontal: Dimensions.get('window').width <= 750 && 5
              }}
            >
              <PostDetailsComment
                gestureStatus={this.state.gestureStatus}
                right={false}
                item={
                  this.props.PostCommentDetails.length > 0
                    ? this.props.PostCommentDetails
                    : []
                }
                navigation={this.props.navigation}
                closeModalhandalListMode={this.handalListMode}
                clickList={
                  this.props.PostCommentDetails
                    ? this.props.PostCommentDetails
                    : null
                }
                PostId={
                  this.props.PostDetails ? this.props.PostDetails.post.id : null
                }
                stopScrolling={this.stopScrolling}
              />

              <View style={{ height: 100 }}></View>
              {this.props.PostCommentDetails.length == 0 && (
                <View
                  style={{
                    flexDirection: "column",
                    height: 100
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      flexDirection: "row",
                      justifyContent: "center"
                    }}
                  >
                    <Icon
                      color={"#000"}
                      iconStyle={{
                        color: "#fff",
                        justifyContent: "center",
                        alignItems: "center",
                        alignSelf: "center"
                      }}
                      reverse
                      name="comments"
                      type="font-awesome"
                      size={20}
                      containerStyle={{
                        alignSelf: "center"
                      }}
                    />
                  </View>
                  <View
                    style={{
                      flex: 1,
                      width: "100%",
                      flexDirection: "row",
                      justifyContent: "center"
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.defaultFont,
                        color: "#000",
                        alignSelf: "center"
                      }}
                    >
                      No Discussions
                    </Text>
                  </View>
                </View>
              )}
            </ScrollView>
            {/* <TabView
            swipeEnabled={false}
            lazy
            navigationState={this.state}
            style={{
              marginRight: 1,
              flex: 1
            }}
            renderScene={this._renderScene}
            renderLazyPlaceholder={this._renderLazyPlaceholder}
            renderTabBar={this._renderTabBar}
            onIndexChange={this._handleIndexChange}
          /> */}
          </ScrollView>
          <View
            style={[
              //ButtonStyle.borderStyle,
              {
                width: 450,
                marginLeft: 20,
                marginBottom: 5,
                display:
                  Dimensions.get("window").width >= 1100 && Platform.OS == "web"
                    ? "flex"
                    : "none"
              }
            ]}
          >
            <CommentDetailScreen
              navigation={this.props.navigation}
              postId={this.props.PostDetails && this.props.PostDetails.post.id}
              listScroll={this.listScroll}
              showScrollIntoView={this.showScrollIntoView}
              ActiveTab={this.state.routes[this.state.index].title}
            />
          </View>
        </View>
        {
          Dimensions.get("window").width <= 750 &&
          <BottomScreenForDiscussion navigation={NavigationService} />
        }
      </View>
    );
  }
}

const mapStateToProps = state => ({
  PostDetails: state.PostDetailsReducer.get("PostDetails"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  loginStatus: state.UserReducer.get("loginStatus"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  PostCommentDetails: state.PostCommentDetailsReducer.get("PostCommentDetails"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  selectComment: state.PostCommentDetailsReducer.get("selectComment"),
  setComment: state.PostDetailsReducer.get("setComment"),
  TrendingHomeFeed: state.HomeFeedReducer.get("TrendingHomeFeedList"),
  PostId: state.PostCommentDetailsReducer.get("PostId"),
  getCreateCommentModalStatus: state.AdminReducer.get("openCreateCommentModal"),
  getShareLinkModalStatus: state.ShareLinkModalReducer.get("modalStatus"),
  getsearchBarStatus: state.AdminReducer.get("searchBarOpenStatus"),
});

const mapDispatchToProps = dispatch => ({
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload)),
  setActiveId: payload => dispatch({ type: "SET_ACTIVE_ID", payload }),
  openCreateComment: payload => dispatch({ type: "OPEN_CREATE_COMMENT", payload }),
  searchOpenBarStatus: payload => dispatch({ type: "SEARCHBAR_STATUS", payload })
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  PostDetailsScreen
);

const styles = StyleSheet.create({
  header: {
    position: Platform.OS == "web" ? "fixed" : null,
    left: 0,
    right: 0,
    zIndex: 10,
    overflow: "hidden"
  },
  container: {
    backgroundColor: ConstantColors.customeBackgroundColor,
    paddingTop: Dimensions.get("window").width <= 750 ? 0 : 10,
    paddingHorizontal: Dimensions.get("window").width <= 750 ? 0 : 10,
    flex: 1
    // Platform.OS == "web" ? 10 : 5
  }
});