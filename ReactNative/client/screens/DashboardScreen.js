import React, { createRef } from "react";
import { graphql } from "react-apollo";
import {
  Animated,
  Dimensions,
  Platform,
  StyleSheet,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  YellowBox,
  AsyncStorage,
  Image
} from "react-native";
import { Icon } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger
} from "react-native-popup-menu";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { ScrollPager, TabBar, TabView } from "react-native-tab-view";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setAdminView } from "../actionCreator/AdminAction";
import { setCreateAccount } from "../actionCreator/CreateAccountAction";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingExternalFeeds } from "../actionCreator/TrendingExternalFeedsAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import { getTrendingUsers } from "../actionCreator/TrendingUsersAction";
import {
  saveUserLoginDaitails,
  setLoginStatus
} from "../actionCreator/UserAction";
import { setUserApproachAction } from "../actionCreator/UserApproachAction";
import { setUSERNAMEMODALACTION } from "../actionCreator/UsernameModalAction";
import DiscussionHomeFeed from "../components/DiscussionHomeFeed";
import HeaderRight from "../components/HeaderRight";
import NewHomeFeed from "../components/NewHomeFeed";
import SEOMetaData from "../components/SEOMetaData";
import ShadowSkeleton from "../components/ShadowSkeleton";
import TrendingHomeFeed from "../components/TrendingHomeFeed";
import AppHelper from "../constants/AppHelper";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import CommentDetailScreen from "./CommentDetailScreen";
import { setScreenLoadingModalAction } from "../actionCreator/ScreenLoadingModalAction";
import CreateCommentCard from "../components/CreateCommentCard";
import firebase from "firebase/app";
import "firebase/auth";
import { setLocalStorage } from "../library/Helper";
import { setAdminStatus } from "../actionCreator/AdminAction";
import jwt_decode from "jwt-decode";
import ButtonStyle from "../constants/ButtonStyle";
import { CONTACTS } from "expo-permissions";
import NavigationService from "../library/NavigationService";
import applloClient from "../client";
import {
  GetNumUnreadNotificationsMutation,
  MarkNotificationsAsReadMutation
} from "../graphqlSchema/graphqlMutation/Notification";
import SearchInputWeb from "../components/SearchInputWeb";
import { GetAccountNotificationsMutation } from "../graphqlSchema/graphqlMutation/Notification";
// import TrendingHomeFeed from "../components/TrendingHomeFeed";
// import NewHomeFeed from "../components/NewHomeFeed";
// import DiscussionHomeFeed from "../components/DiscussionHomeFeed";
import BottomScreen from '../components/BottomScreen';

const USER_VIEW = "USER_VIEW";
const ADMIN_VIEW = "ADMIN_VIEW";
const roleWiseViewOption = [
  {
    label: "Admin",
    value: ADMIN_VIEW,
    itemKey: 0
  },
  {
    label: "User",
    value: USER_VIEW,
    itemKey: 1
  }
];
const initRoutes = [
  {
    key: "first",
    title: "Trending",
    icon: "fire",
    type: "simple-line-icon"
  },
  { key: "second", title: "New", icon: "clock-o", type: "font-awesome" },
  {
    key: "third",
    title: "Bookmarks",
    icon: "bookmark",
    type: "font-awesome"
  }
];
const adminRoutes = [{
  key: "first",
  title: "Trending",
  icon: "fire",
  type: "simple-line-icon"
},
{ key: "second", title: "New", icon: "clock-o", type: "font-awesome" },
{
  key: "third",
  title: "Bookmarks",
  icon: "bookmark",
  type: "font-awesome"
},
{
  key: "zero",
  title: "Reported",
  icon: "flag",
  type: "font-awesome"
},
];

let lastTap = null;
class DashboardScreen extends React.PureComponent {
  state = {
    modalVisible: false,
    showsVerticalScrollIndicatorView: false,
    currentScreentWidth: 0,
    data: [],
    page: 1,
    loading: true,
    loadingMore: false,
    refreshing: false,
    pageInfo: null,
    error: null,
    apiCall: true,
    routes: [...initRoutes],
    // this.props.isAdmin  ? [...adminRoutes] : [...initRoutes],
    index: 0,
    ViewMode: "Default",
    userType: this.props.isAdminView ? ADMIN_VIEW : USER_VIEW,
    prevFeedList: {
      trending: {},
      discussion: {},
      new: {}
    },
    feedY: 0,
    UnreadNotifications: 0,
    showSearchIcon: true,
  };

  constructor(props) {
    super(props);
    this.Pagescrollview = null;
    this.circleAnimatedValue = new Animated.Value(0);
    YellowBox.ignoreWarnings(["VirtualizedLists should never be nested"]);
    this.onEndReachedCalledDuringMomentum = true;
    this.inputRefs = {};
    this.flatListRefNew = createRef();
    this.flatListRefDiscussion = createRef();
    this.flatListRefTrending = createRef();
  }
  setPrevList = (res, name) => {
    this.setState(prevState => ({
      prevFeedList: { ...prevState.prevFeedList, [name]: res }
    }));
  };

  roleWiseTabUpdate = () => {
    if (this.props.isAdmin && this.props.isAdminView) {
      this.setState(prevState => ({
        routes: [
          {
            key: "zero",
            title: "Reported",
            icon: "flag",
            type: "font-awesome"
          },
          ...initRoutes
        ],
        index: prevState.index + 1
      }));
    } else if (this.state.routes.length === 4) {
      this.setState(prevState => ({
        index: prevState.index > 0 ? prevState.index - 1 : prevState.index,
        routes: initRoutes
      }));
    }
  };

  componentDidMount = async () => {
    this.props.searchOpenBarStatus(false)
    // this.roleWiseTabUpdate();
    let __self = this;
    // this.props.setSignUpModalStatus(false);
    // this.props.setLoginModalStatus(false);
    // this.props.setUsernameModalStatus(false);
  };

  getIcon = () => {
    switch (this.state.ViewMode) {
      case "Card":
        return "dashboard";
      case "Text":
        return "line-weight";
      case "Compact":
        return "reorder";
      default:
        return "list";
    }
  };

  componentDidUpdate(prevProps) {
    if (this.props.getHasScrollTop == true && this.Pagescrollview) {
      this.Pagescrollview.scrollToOffset({ animated: true, offset: 0 });
      this.props.setHASSCROLLEDACTION(false);
    }
    // if (prevProps.isAdminView !== this.props.isAdminView) {
    //   this.roleWiseTabUpdate();
    // }
  }

  goToScrollTop = () => {
    this.props.setHASSCROLLEDACTION(true);
  };

  doScroll = (value, name) => {
    if (name == "new") {
      this.flatListRefNew = value;
    } else if (name == "trending") {
      this.flatListRefTrending = value;
    } else if (name == "discussion") {
      this.flatListRefDiscussion = value;
    }
  };

  scrollFunc = () => {
    {
      this.flatListRefNew.current &&
        this.flatListRefNew.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true
        }),
        this.flatListRefTrending.current &&
        this.flatListRefTrending.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true
        }),
        this.flatListRefDiscussion.current &&
        this.flatListRefDiscussion.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true
        });
    }
  };

  handleDoubleTap = () => {
    if (lastTap !== null) {
      this.scrollFunc();
      clearTimeout(lastTap);
      lastTap = null;
    } else {
      lastTap = setTimeout(() => {
        clearTimeout(lastTap);
        lastTap = null;
      }, 1000);
    }
  };

  _renderLazyPlaceholder = ({ route }) => <ShadowSkeleton />;

  _handleIndexChange = index => {
    this.setState({ index });
    this.props.setPostCommentReset({
      payload: [],
      postId: "",
      title: "",
      loading: true
    });
  };

  _renderTabBar = props =>
    Dimensions.get("window").width >= 750 && (
      <View
        style={[
          ButtonStyle.shadowStyle, ButtonStyle.borderStyle,
          {
            flexDirection: "row",
            width: "100%",
            // height: Dimensions.get("window").width <= 750 ? 50 : 60,
            height: 70,
            backgroundColor:
              Dimensions.get("window").width <= 750 ? "#000" : "#fff",
            alignItems: "center",
            paddingHorizontal: 10,
            borderWidth:0,
            paddingBottom: 10,
            paddingTop:5,
            marginTop:12,
            marginLeft: 4,
            width:'98%'
            // paddingBottom: Dimensions.get("window").width <= 750 ? 0 : 10,
            // borderRadius: Dimensions.get("window").width <= 750 ? 0 : 20
          }
        ]}
      >

        <TabBar
          onTabPress={() => this.handleDoubleTap()}
          {...props}
          indicatorStyle={{
            backgroundColor: "transparent",
            height: 3,
            borderRadius: 6
          }}
          style={{
            backgroundColor: 'transparent',
            width: "100%",
            height: 60,
            shadowColor: "transparent",
            justifyContent: 'center',

          }}
          labelStyle={{
            color: "#000",
            fontFamily: ConstantFontFamily.MontserratBoldFont,
            fontSize: 13,
            fontWeight: "bold",
            // justifyContent:'center',
            // alignItems:'center'
          }}
           renderIcon={({ route, focused, color }) =>
            Dimensions.get("window").width >= 750 && (
              <Icon
                name={route.icon}
                type={route.type}
                color={focused ? "#009B1A" : "#D3D3D3"}
              />
            )
          }
          renderLabel={({ route, focused, color }) => (
            <Text
              style={{
                color: focused ? "#009B1A" : "#D3D3D3",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                flexWrap: "wrap",
                flex: 1,
                width: "100%"
              }}
            >
              {route.title}
            </Text>
          )}
        // renderIndicsator={({ route, focused, color }) => null}
        />
        {/* <TouchableOpacity
        style={{
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Menu>
          <MenuTrigger>
            <Icon
              type="material"
              name={this.getIcon()}
              containerStyle={{
                alignSelf: "center",
                justifyContent: "center"
              }}
            />
          </MenuTrigger>
          <MenuOptions
            optionsContainerStyle={{
              borderRadius: 6,
              borderWidth: 1,
              borderColor: "#e1e1e1",
              shadowColor: "transparent"
            }}
            customStyles={{
              optionsContainer: {
                minHeight: 100,
                marginTop: hp("3%"),
                marginLeft: wp("-0.5%")
              }
            }}
          >
            <MenuOption
              onSelect={() => {
                this.setState({
                  ViewMode: "Card"
                });
              }}
            >
              <Hoverable>
                {isHovered => (
                  <Text
                    style={{
                      textAlign: "center",
                      color: isHovered == true ? "#009B1A" : "#000",
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }}
                  >
                    Default View
                  </Text>
                )}
              </Hoverable>
            </MenuOption>
            <MenuOption
              onSelect={() => {
                this.setState({
                  ViewMode: "Default"
                });
              }}
            >
              <Hoverable>
                {isHovered => (
                  <Text
                    style={{
                      textAlign: "center",
                      color: isHovered == true ? "#009B1A" : "#000",
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }}
                  >
                    Card View
                  </Text>
                )}
              </Hoverable>
            </MenuOption>
            <MenuOption
              onSelect={() => {
                this.setState({
                  ViewMode: "Text"
                });
              }}
            >
              <Hoverable>
                {isHovered => (
                  <Text
                    style={{
                      textAlign: "center",
                      color: isHovered == true ? "#009B1A" : "#000",
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }}
                  >
                    Text View
                  </Text>
                )}
              </Hoverable>
            </MenuOption>
            <MenuOption
              onSelect={() => {
                this.setState({
                  ViewMode: "Compact"
                });
              }}
            >
              <Hoverable>
                {isHovered => (
                  <Text
                    style={{
                      textAlign: "center",
                      color: isHovered == true ? "#009B1A" : "#000",
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }}
                  >
                    Compact View
                  </Text>
                )}
              </Hoverable>
            </MenuOption>
          </MenuOptions>
        </Menu>
      </TouchableOpacity> */}
      </View>
    );

  _renderScene = ({ route }) => {
    switch (route.key) {
      case "third":
        return this.props.loginStatus == 1 ? (
          <DiscussionHomeFeed
            navigation={this.props.navigation}
            listType={"Home"}
            ViewMode={this.state.ViewMode}
            setPrevList={this.setPrevList}
            prevFeedList={this.state.prevFeedList}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        ) : (
            <View
              style={{
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Icon
                color={"#000"}
                iconStyle={{
                  color: "#fff",
                  justifyContent: "center",
                  alignItems: "center",
                  alignSelf: "center"
                }}
                reverse
                name="sticky-note"
                type="font-awesome"
                size={20}
                containerStyle={{
                  alignSelf: "center"
                }}
              />
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  color: "#000",
                  alignSelf: "center"
                }}
              >
                <Text
                  onPress={() => this.loginHandle()}
                  style={{
                    textDecorationLine: "underline",
                    fontFamily: ConstantFontFamily.defaultFont
                  }}
                >
                  Login
              </Text>{" "}
              to see Discussion posts
            </Text>
            </View>
          );
      case "second":
        return (
          <NewHomeFeed
            navigation={this.props.navigation}
            listType={"Home"}
            ViewMode={this.state.ViewMode}
            setPrevList={this.setPrevList}
            prevFeedList={this.state.prevFeedList}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        );
      case "first":
        return (
          <TrendingHomeFeed
            navigation={this.props.navigation}
            listType={"Home"}
            ViewMode={this.state.ViewMode}
            setPrevList={this.setPrevList}
            prevFeedList={this.state.prevFeedList}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        );
      case "zero":
        return (
          <NewHomeFeed
            navigation={this.props.navigation}
            listType={"Home"}
            ViewMode={this.state.ViewMode}
            setPrevList={this.setPrevList}
            prevFeedList={this.state.prevFeedList}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        );
    }
  };

  renderTabViewForMobile = () => {
    if (this.props.getTabView == "Bookmarks") {
      return (
        this.props.loginStatus == 1 ? (
          <View style={{ flex: 1 }}>
          <DiscussionHomeFeed
            navigation={this.props.navigation}
            listType={"Home"}
            ViewMode={this.state.ViewMode}
            setPrevList={this.setPrevList}
            prevFeedList={this.state.prevFeedList}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
          </View>
        ) : (
            <View
              style={{
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Icon
                color={"#000"}
                iconStyle={{
                  color: "#fff",
                  justifyContent: "center",
                  alignItems: "center",
                  alignSelf: "center"
                }}
                reverse
                name="sticky-note"
                type="font-awesome"
                size={20}
                containerStyle={{
                  alignSelf: "center"
                }}
              />
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  color: "#000",
                  alignSelf: "center"
                }}
              >
                <Text
                  onPress={() => this.loginHandle()}
                  style={{
                    textDecorationLine: "underline",
                    fontFamily: ConstantFontFamily.defaultFont
                  }}
                >
                  Login
                </Text>{" "}
                to see Discussion posts
              </Text>
            </View>
          ))
    }

    else if (this.props.getTabView == "New") {
      return (
        <View style={{ flex: 1 }}>
        < NewHomeFeed
          navigation={this.props.navigation}
          listType={"Home"}
          ViewMode={this.state.ViewMode}
          setPrevList={this.setPrevList}
          prevFeedList={this.state.prevFeedList}
          ActiveTab={this.state.routes[this.state.index].title}
          doScroll={this.doScroll}
        />
        </View>)

    }
    else if (this.props.getTabView == "Trending") {
      return (
        <View style={{ flex: 1 }}>
          <TrendingHomeFeed
            navigation={this.props.navigation}
            listType={"Home"}
            ViewMode={this.state.ViewMode}
            setPrevList={this.setPrevList}
            prevFeedList={this.state.prevFeedList}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
       </View>
        )
    }
    else {
      return (
        // <View style={{ flex: 1 }}>
          <TrendingHomeFeed
            navigation={this.props.navigation}
            listType={"Home"}
            ViewMode={this.state.ViewMode}
            setPrevList={this.setPrevList}
            prevFeedList={this.state.prevFeedList}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
          // </View>
          )
    }
  }


  loginHandle = () => {
    this.props.setLoginModalStatus(true);
  };

  listScroll = value => {
    this.setState({
      feedY: value
    });
  };

  setMarkAsRead = () => {
    applloClient
      .query({
        query: MarkNotificationsAsReadMutation,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        //console.log(res,'===================================>');
        this.setState({
          UnreadNotifications: 0
        });
        this.getUnreadNotifications();
      });
  };

  getUnreadNotifications = () => {
    applloClient
      .query({
        query: GetNumUnreadNotificationsMutation,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        this.setState({
          UnreadNotifications: res.data.account.num_unread_notifications
        });
        let timer = setTimeout(() => {
          if (this.props.loginStatus == 1) {
            this.getUnreadNotifications();
          }
        }, 60000);
      });
  };

  getAccountNotifications = () => {
    applloClient
      .query({
        query: GetAccountNotificationsMutation,
        variables: {
          first: 100,
          after: null
        },
        fetchPolicy: "no-cache"
      })
      .then(async res => { });
  };

  render() {
    return (
      <View style={styles.container}>
        {}
        {Platform.OS == "web" && (
          <SEOMetaData title={"Weclikd"} description={""} image={""} />
        )}
        {//Platform.OS != "web" &&
          Dimensions.get("window").width <= 750 && (
            <Animated.View
              style={{
                position: Platform.OS == "web" ? "sticky" : null,
                top: 0,
                left: 0,
                right: 0,
                zIndex: 10,
                overflow: "hidden",
                borderRadius: 0
              }}
            >
              <View
                style={{
                  alignItems: Platform.OS == "web" ? "center" : null,
                  justifyContent: "center",
                  width: "100%",
                  flexDirection: "row",
                  backgroundColor: Dimensions.get('window').width <= 750 ? "#000" : "#000",
                  height: Dimensions.get('window').width <= 750 ? 50 : 42
                }}
              >
                {/* {this.props.isAdmin && (
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "flex-start",
                    alignSelf: "center",
                    marginLeft: 10,
                  }}
                >
                  <RNPickerSelect
                    placeholder={{}}
                    items={roleWiseViewOption}
                    onValueChange={(itemValue, itemIndex) => {
                      this.setState({ userType: itemValue });
                      this.props.changeAdminView(itemValue === ADMIN_VIEW);
                    }}
                    onUpArrow={() => {
                      this.inputRefs.name.focus();
                    }}
                    onDownArrow={() => {
                      this.inputRefs.picker2.togglePicker();
                    }}
                    style={{ ...styles }}
                    // value={this.props.isAdminView ? ADMIN_VIEW : USER_VIEW}
                    value={this.state.userType}
                    ref={el => {
                      this.inputRefs.picker = el;
                    }}
                  />
                </View>
                  )} */}
                <View>
                  {!this.state.showSearchIcon &&
                    Dimensions.get("window").width <= 1100 && (
                      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent:'space-between' }}>
                        <TouchableOpacity
                          onPress={this.goToScrollTop}
                          style={{
                            flex: 1,
                            flexDirection: "row",
                            //justifyContent: this.props.loginStatus == 1 ? "center" : null,
                            backgroundColor: Dimensions.get('window').width <= 750 ? "f4f4f4" : "#000",
                            height: 50,
                            marginLeft: 5,
                            marginRight: 30
                          }}
                        >
                          <Image
                            source={require("../assets/image/logo.png")}
                            style={{
                              height: 30,
                              width: 30,
                              //marginVertical: 8,
                              marginRight: 5,
                              alignSelf: "center"
                            }}
                            resizeMode={"contain"}
                          />
                        </TouchableOpacity>
                        <SearchInputWeb
                          navigation={this.props.navigation}
                          refs={ref => {
                            this.input = ref;
                          }}
                          displayType={"web"}
                          press={status => {
                            this.setState({ showSearchIcon: status });
                          }}
                        />
                      </View>
                    )}
                </View>

                {this.state.showSearchIcon && (<TouchableOpacity
                  onPress={this.goToScrollTop}
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    //justifyContent: this.props.loginStatus == 1 ? "center" : null,
                    backgroundColor: Dimensions.get('window').width <= 750 ? "f4f4f4" : "#000",
                    height: 50,
                    marginLeft: 5
                  }}
                >
                  <Image
                    source={require("../assets/image/logo.png")}
                    style={{
                      height: 30,
                      width: 30,
                      //marginVertical: 8,
                      marginRight: 5,
                      alignSelf: "center"
                    }}
                    resizeMode={"contain"}
                  />
                  {!this.props.getsearchBarStatus &&
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        fontWeight: "bold",
                        fontSize: 22,
                        textAlign: "center",
                        color: Dimensions.get('window').width <= 750 ? "#fff" : "#fff",
                        alignSelf: 'center'
                      }}
                    >
                      weclikd
                </Text>}
                </TouchableOpacity>)}
                {this.props.loginStatus == 1 ?
                  <HeaderRight navigation={this.props.navigation} />
                  :
                  // <Icon
                  //   color={Dimensions.get('window').width <= 750 ? "#fff" : "#fff"}
                  //   iconStyle={{ paddingLeft: 10, marginRight: 10 }}
                  //   onPress={() => this.props.leftPanelModalFunc(true)}
                  //   name="bars"
                  //   type="font-awesome"
                  //   size={22}
                  // />
                  this.state.showSearchIcon && <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginRight: 10 }}>
                    <Icon
                      name="search"
                      size={25}
                      type="feather"
                      iconStyle={styles.actionButtonIcon}
                      iconStyle={{
                        color: Dimensions.get('window').width <= 750 ? "#fff" : "#fff",
                        marginRight: Dimensions.get('window').width <= 750 ? 19 : 30,
                      }}
                      color="#fff"
                      underlayColor="#000"
                      onPress={() => {
                        this.props.setSearchBarStatus(true);
                        this.setState({ showSearchIcon: false });
                      }}
                    />

                    {Dimensions.get('window').width <= 1100 && (<View>
                      <Menu>
                        <MenuTrigger testID="ProfileIcon" onPress={() => { this.props.leftPanelModalFunc(true) }}>
                          <Image
                            source={require("../assets/image/default-image.png")}
                            style={{
                              height: 27,
                              width: 27,
                              borderRadius: 13,
                              //bottom: 4,
                              borderWidth: 1,
                              alignSelf: 'center',
                              borderColor: Dimensions.get('window').width <= 750 ? "#fff" : "#fff",
                            }}
                            navigation={this.props.navigation}
                          />
                        </MenuTrigger>
                      </Menu>
                    </View>

                    )}
                  </View>
                }
                {/* )} */}
              </View>
            </Animated.View>
          )}
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            // width: "100%",
            justifyContent: "center",
            marginLeft: Dimensions.get("window").width < 1100 && Dimensions.get("window").width >= 750 ? 5 : 0
          }}
        >
          {
            Dimensions.get('window').width <= 750 ?
              // this.props.getTabView ?
              <View
                style={{
                  flex: 1,
                  width: "100%",
                  // padding: 10
                }}
              // contentContainerStyle={{flex:1}}
              >
                {this.renderTabViewForMobile()}
              </View>
              :
              <View
                style={{
                  // flex: 1,
                  // width:
                  //   Platform.OS == "web" && Dimensions.get("window").width >= 750
                  //     ? 450
                  //     : "100%",
                  // padding: 10
                }}
              // showsVerticalScrollIndicator ={false}
              // contentContainerStyle={{flex:1}}
              >
                <TabView
                  swipeEnabled={false}
                  lazy
                  navigationState={this.state}
                  renderScene={this._renderScene}
                  renderLazyPlaceholder={this._renderLazyPlaceholder}
                  renderTabBar={this._renderTabBar}
                  onIndexChange={this._handleIndexChange}
                  removeClippedSubviews={true}
                  style={{
                    width:
                      Platform.OS == "web" && Dimensions.get("window").width >= 750
                        ? 450
                        : "100%",
                  }}
                />
              </View>
          }

          <View
            style={{
              width: 450,
              marginLeft: 20,
              marginBottom: 5,
              marginRight: Dimensions.get("window").width < 1100 && Dimensions.get("window").width >= 750 ? 10 : 0,
              display:
                Dimensions.get("window").width >= 750 && Platform.OS == "web"
                  ? "flex"
                  : "none",
              marginTop: 10
            }}
          >
            <CommentDetailScreen
              navigation={this.props.navigation}
              postId={this.props.PostDetails && this.props.PostDetails.post.id}
              listScroll={this.listScroll}
              showScrollIntoView={this.showScrollIntoView}
              ActiveTab={this.state.routes[this.state.index].title}
            />
          </View>
        </View>
        { Dimensions.get("window").width <= 750 &&
          <BottomScreen navigation={NavigationService} />
        }
      </View>
    );
  }
}

const mapStateToProps = state => ({
  listHomeFeed: state.HomeFeedReducer.get("HomefeedList"),
  homefeedListPagination: state.HomeFeedReducer.get("HomefeedListPagination"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  getCreateAccount: state.CreateAccountReducer.get("setCreateAccountData"),
  getUserApproach: state.UserApproachReducer.get("setUserApproach"),
  loginStatus: state.UserReducer.get("loginStatus"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),

  // For New Part
  PostDetails: state.PostDetailsReducer.get("PostDetails"),
  PostCommentDetails: state.PostCommentDetailsReducer.get("PostCommentDetails"),
  selectComment: state.PostCommentDetailsReducer.get("selectComment"),
  setComment: state.PostDetailsReducer.get("setComment"),
  getUsernameModalStatus: state.UsernameModalReducer.get("modalStatus"),
  getsearchBarStatus: state.AdminReducer.get("searchBarOpenStatus"),
  getTabView: state.AdminReducer.get("tabType"),
});

const mapDispatchToProps = dispatch => ({
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingUsers: payload => dispatch(getTrendingUsers(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setUsernameModalStatus: payload => dispatch(setUSERNAMEMODALACTION(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  changeLoginStatus: payload => dispatch(setLoginStatus(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  setCreateAccount: payload => dispatch(setCreateAccount(payload)),
  setUserApproachAction: payload => dispatch(setUserApproachAction(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  getTrendingExternalFeeds: payload =>
    dispatch(getTrendingExternalFeeds(payload)),
  changeAdminView: payload => dispatch(setAdminView(payload)),
  setScreenLoadingModalAction: payload =>
    dispatch(setScreenLoadingModalAction(payload)),
  changeAdminStatus: payload => dispatch(setAdminStatus(payload)),
  setSearchBarStatus: payload =>
    dispatch({ type: "SEARCH_BAR_STATUS", payload }),
  setPostCommentReset: payload =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload }),
  leftPanelModalFunc: payload => dispatch({ type: 'LEFT_PANEL_OPEN', payload }),
  searchOpenBarStatus: payload => dispatch({ type: "SEARCHBAR_STATUS", payload })
});

const DashboardScreenContainerWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(DashboardScreen);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  DashboardScreenContainerWrapper
);

const styles = StyleSheet.create({
  inputIOS: {
    width: Platform.OS != "web" && 100,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 10,
    backgroundColor: "white",
    color: "black",
    fontSize: 16,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  inputAndroid: {
    width: Platform.OS != "web" && 100,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#fff",
    borderRadius: 10,
    color: "#000",
    backgroundColor: "white",
    fontSize: 16,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  container: {
    flex: 1,
    width: "100%",
    borderRadius: 8,
    paddingTop: 0,
    // Dimensions.get("window").width <= 750 ? 0 : 10,
    paddingHorizontal:
      Platform.OS == "web" && Dimensions.get("window").width >= 1100
        ? 15
        : 0,
    // Dimensions.get("window").width <= 750
    //   ? 0
    //   : 5,
    height: "100%",
    backgroundColor: ConstantColors.customeBackgroundColor
  }
  // inputIOS: {
  //   paddingTop: 13,
  //   paddingHorizontal: 10,
  //   paddingBottom: 12,
  //   borderWidth: 1,
  //   borderColor: "gray",
  //   borderRadius: 4,
  //   backgroundColor: "white",
  //   color: "black",
  //   fontSize: 16,
  //   fontWeight: "bold",
  //   fontFamily: ConstantFontFamily.MontserratBoldFont
  // },
  // inputAndroid: {
  //   paddingHorizontal: 10,
  //   paddingVertical: 8,
  //   borderWidth: 0.5,
  //   borderColor: "#fff",
  //   borderRadius: 8,
  //   color: "#000",
  //   backgroundColor: "white",
  //   fontSize: 16,
  //   fontWeight: "bold",
  //   fontFamily: ConstantFontFamily.MontserratBoldFont
  // }
});