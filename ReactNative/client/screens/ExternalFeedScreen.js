import { openBrowserAsync } from "expo-web-browser";
import { List } from "immutable";
import React, { Component, createRef } from "react";
import { graphql } from "react-apollo";
import {
  Animated,
  Dimensions,
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from "react-native";
import { Icon } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger,
} from "react-native-popup-menu";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { TabBar, TabView } from "react-native-tab-view";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { editFeed } from "../actionCreator/FeedEditAction";
import { getFeedProfileDetails } from "../actionCreator/FeedProfileAction";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { getTrendingTopicsProfileDetails } from "../actionCreator/TrendingTopicsProfileAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import DiscussionHomeFeed from "../components/DiscussionHomeFeed";
import ExternalFeedStar from "../components/ExternalFeedStar";
import NewHomeFeed from "../components/NewHomeFeed";
import SEOMetaData from "../components/SEOMetaData";
import ShadowSkeleton from "../components/ShadowSkeleton";
import TrendingHomeFeed from "../components/TrendingHomeFeed";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import { rootTopics } from "../constants/RootTopics";
import { DeleteExternalFeedMutation } from "../graphqlSchema/graphqlMutation/FeedMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { DeleteExternalFeedVariables } from "../graphqlSchema/graphqlVariables/LikeContentVariables";
import CommentDetailScreen from "./CommentDetailScreen";
import HeaderRight from "../components/HeaderRight";
import ButtonStyle from "../constants/ButtonStyle";
import {
  FeedFollowVariables,
  FeedUnFollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import {
  FeedFollowMutation,
  FeedUnFollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import BottomScreen from '../components/BottomScreen';
import NavigationService from "../library/NavigationService";

let lastTap = null;

function truncate(input) {
  if (input.length > 10) {
    return input.substring(0, 10) + "...";
  }
  return input;
}

class ExternalFeedScreen extends Component {
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.state = {
      modalVisible: false,
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      items: [
        {
          label: "Profile",
          value: "FEED",
          key: 0,
        },
      ],
      cliksselectItem: this.props.navigation
        .getParam("type", "NO-ID")
        .toUpperCase(),
      showIcon: "#fff",
      routes: [
        {
          key: "first",
          title: "Trending",
          icon: "fire",
          type: "simple-line-icon",
        },
        { key: "second", title: "New", icon: "clock-o", type: "font-awesome" },
        {
          key: "third",
          title: "Bookmarks",
          icon: "bookmark",
          type: "font-awesome",
        },
      ],
      index: 0,
      wikiVisible: false,
      id: this.props.navigation.getParam("title", "NO-ID")
        ? this.props.navigation.getParam("title", "NO-ID")
        : "",
      ViewMode: "Default",
      scrollY: 0,
      ProfileHeight: 0,
      feedY: 0,
      menu: false
    };
    this.flatListRefNew = createRef();
    this.flatListRefDiscussion = createRef();
    this.flatListRefTrending = createRef();
  }

  doScroll = (value, name) => {
    if (name == "new") {
      this.flatListRefNew = value;
    } else if (name == "trending") {
      this.flatListRefTrending = value;
    } else if (name == "discussion") {
      this.flatListRefDiscussion = value;
    }
  };

  scrollFunc = () => {
    {
      this.flatListRefNew.current &&
        this.flatListRefNew.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true,
        }),
        this.flatListRefTrending.current &&
        this.flatListRefTrending.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true,
        }),
        this.flatListRefDiscussion.current &&
        this.flatListRefDiscussion.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true,
        });
    }
  };

  handleDoubleTap = () => {
    if (lastTap !== null) {
      this.scrollFunc();
      clearTimeout(lastTap);
      lastTap = null;
    } else {
      lastTap = setTimeout(() => {
        clearTimeout(lastTap);
        lastTap = null;
      }, 1000);
    }
  };
  _renderLazyPlaceholder = ({ route }) => <ShadowSkeleton />;

  _handleIndexChange = (index) => {
    this.setState({ index });
    this.props.setPostCommentReset({
      payload: [],
      postId: "",
      title: "",
      loading: true,
    });
  };

  _renderTabBar = (props) => (
    Dimensions.get("window").width >= 750 &&
    <View>
      <View
        style={[
          ButtonStyle.shadowStyle, ButtonStyle.borderStyle,
          {
          flexDirection: "row",
          // width: "100%",
          height: 70,
          // height: Dimensions.get("window").width <= 750 ? 50 : 60,
          borderWidth: 0,
          // borderColor: "#C5C5C5",
          backgroundColor:
            Dimensions.get("window").width <= 750 ? "#000" : "#fff",
          alignItems: "center",
          paddingHorizontal: 10,
          marginLeft:4
          // paddingBottom: Dimensions.get("window").width <= 750 ? 0 : 10,
          // borderRadius: Dimensions.get("window").width <= 750 ? 0 : 10,
        }]}
      >
        <TabBar
          onTabPress={() => this.handleDoubleTap()}
          {...props}
          indicatorStyle={{
            backgroundColor: "transparent",
            height: 3,
            borderRadius: 6,
          }}
          style={{
            backgroundColor: "transparent",
            width: "100%",
            // height: Dimensions.get("window").width <= 750 ? 55 : 60,
            shadowColor: "transparent",
            height: 60,
            justifyContent: 'center'
          }}
          labelStyle={{
            color: "#000",
            fontFamily: ConstantFontFamily.MontserratBoldFont,
          }}
          renderIcon={({ route, focused, color }) =>
            Dimensions.get("window").width >= 750 && (
              <Icon
                name={route.icon}
                type={route.type}
                color={focused ? "#009B1A" : "#D3D3D3"}
              />
            )
          }
          renderLabel={({ route, focused, color }) => (
            <Text
              style={{
                color: focused ? "#009B1A" : "#D3D3D3",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
              }}
            >
              {route.title}
            </Text>
          )}
          renderIndicator={({ route, focused, color }) => null}
        />
      </View>
    </View>
  );

  getIcon = () => {
    switch (this.state.ViewMode) {
      case "Card":
        return "dashboard";
      case "Text":
        return "line-weight";
      case "Compact":
        return "reorder";
      default:
        return "list";
    }
  };

  showIcon = (data) => {
    this.setState({
      showIcon: data,
    });
  };

  async componentDidMount() {
    this.props.searchOpenBarStatus(false);
    let itemId = await this.props.navigation.getParam("title", "NO-ID");
    this.setState({
      id: itemId,
    });
    if (itemId) {
      this.props.setFeedDetails({
        id: itemId,
        type: this.props.navigation.getParam("type", "NO-ID"),
      });
    }
    const index = this.props.getUserFollowFeedList.findIndex(
      (i) =>
        i
          .getIn(["feed", "name"])
          .toLowerCase()
          .replace("ExternalFeed:", "") ==
        itemId
          .replace("%2F", "")
          .toLowerCase()
          .replace("ExternalFeed:", "")
    );
    if (index != -1) {
      if (
        this.props.getUserFollowFeedList.getIn([
          index,
          "settings",
          "follow_type",
        ]) == "FAVORITE"
      ) {
        this.showIcon("#FADB4A");
      }
      if (
        this.props.getUserFollowFeedList.getIn([
          index,
          "settings",
          "follow_type",
        ]) == "FOLLOW"
      ) {
        this.showIcon("#E1E1E1");
      }
    } else {
      this.showIcon("#fff");
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.getHasScrollTop == true && this.UserProfilescrollview) {
      this.UserProfilescrollview.scrollTo({ x: 0, y: 0, animated: true });
      this.props.setHASSCROLLEDACTION(false);
    }
    if (
      prevProps.navigation.getParam("title", "NO-ID").toUpperCase() !=
      this.props.navigation.getParam("title", "NO-ID").toUpperCase()
    ) {
      this.setState({
        scrollY: 0,
      });
      this.FeedProfilescrollview.scrollTo({ x: 0, y: 0, animated: true });
    }
  }

  loginHandle = () => {
    this.props.setLoginModalStatus(true);
  };

  goToTopicProfile = (id) => {
    this.props.topicId({
      id: id,
      type: "feed",
    });
  };

  openWindow = async (link) => {
    await openBrowserAsync(link);
  };

  showAlert = () => {
    if (Platform.OS === "web") {
      var result = confirm(
        "Are you sure you want to delete " +
        this.props.feedDetails.getIn(["data", "external_feed"]).get("name")
      );
      if (result == true) {
        DeleteExternalFeedVariables.variables.feed_id = this.props.feedDetails
          .getIn(["data", "external_feed"])
          .get("id");
        applloClient
          .query({
            query: DeleteExternalFeedMutation,
            ...DeleteExternalFeedVariables,
            fetchPolicy: "no-cache",
          })
          .then(async (res) => {
            let resDataLogin = await this.props.Login();
            await this.props.saveLoginUser(resDataLogin.data.login);
          });
      } else {
      }
    } else {
      Alert.alert(
        "Are you sure you want to delete " +
        this.props.feedDetails.getIn(["data", "external_feed"]).get("name")[
        ({
          text: "NO",
          onPress: () => console.warn("NO Pressed"),
          style: "cancel",
        },
        {
          text: "YES",
          onPress: () => {
            DeleteExternalFeedVariables.variables.feed_id = this.props.feedDetails
              .getIn(["data", "external_feed"])
              .get("id");
            applloClient
              .query({
                query: DeleteExternalFeedMutation,
                ...DeleteExternalFeedVariables,
                fetchPolicy: "no-cache",
              })
              .then(async (res) => {
                let resDataLogin = await this.props.Login();
                await this.props.saveLoginUser(resDataLogin.data.login);
              });
          },
        })
        ]
      );
    }
  };

  listScroll = (value) => {
    this.setState({
      feedY: value,
    });
  };

  _renderScene = ({ route }) => {
    switch (route.key) {
      case "third":
        return (
          <View>
            {this.props.loginStatus == 1 ? (
              <DiscussionHomeFeed
                navigation={this.props.navigation}
                listType={"Feed"}
                data={this.props.navigation.getParam("title", "NO-ID")}
                ViewMode={this.state.ViewMode}
                listScroll={this.listScroll}
                ActiveTab={this.state.routes[this.state.index].title}
                doScroll={this.doScroll}
              />
            ) : (
              <View>
                <View
                  style={{
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center",
                    }}
                    reverse
                    name="sticky-note"
                    type="font-awesome"
                    size={20}
                    containerStyle={{
                      alignSelf: "center",
                    }}
                  />
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center",
                    }}
                  >
                    <Text
                      onPress={() => this.loginHandle()}
                      style={{
                        textDecorationLine: "underline",
                        fontFamily: ConstantFontFamily.defaultFont,
                      }}
                    >
                      Login
                    </Text>{" "}
                    to see Discussion posts
                  </Text>
                </View>
              </View>
            )}
          </View>
        );
      case "second":
        return (
          <NewHomeFeed
            navigation={this.props.navigation}
            listType={"Feed"}
            data={this.props.navigation.getParam("title", "NO-ID")}
            ViewMode={this.state.ViewMode}
            listScroll={this.listScroll}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        );
      case "first":
        return (
          <TrendingHomeFeed
            navigation={this.props.navigation}
            listType={"Feed"}
            data={this.props.navigation.getParam("title", "NO-ID")}
            ViewMode={this.state.ViewMode}
            listScroll={this.listScroll}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        );
      default:
        return (
          <NewHomeFeed
            navigation={this.props.navigation}
            listType={"Feed"}
            data={this.props.navigation.getParam("title", "NO-ID")}
            ViewMode={this.state.ViewMode}
            listScroll={this.listScroll}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        );
    }
  };

  renderTabViewForMobile = () => {
    if (this.props.getTabView == "Bookmarks") {
      return (
        <View>
          {this.props.loginStatus == 1 ? (
            <DiscussionHomeFeed
              navigation={this.props.navigation}
              listType={"Feed"}
              data={this.props.navigation.getParam("title", "NO-ID")}
              ViewMode={this.state.ViewMode}
              listScroll={this.listScroll}
              ActiveTab={this.state.routes[this.state.index].title}
              doScroll={this.doScroll}
            />
          ) : (
            <View>
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Icon
                  color={"#000"}
                  iconStyle={{
                    color: "#fff",
                    justifyContent: "center",
                    alignItems: "center",
                    alignSelf: "center",
                  }}
                  reverse
                  name="sticky-note"
                  type="font-awesome"
                  size={20}
                  containerStyle={{
                    alignSelf: "center",
                  }}
                />
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: "bold",
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    color: "#000",
                    alignSelf: "center",
                  }}
                >
                  <Text
                    onPress={() => this.loginHandle()}
                    style={{
                      textDecorationLine: "underline",
                      fontFamily: ConstantFontFamily.defaultFont,
                    }}
                  >
                    Login
                  </Text>{" "}
                  to see Discussion posts
                </Text>
              </View>
            </View>
          )}
        </View>
      );
    }

    else if (this.props.getTabView == "New") {
      return (
        <NewHomeFeed
          navigation={this.props.navigation}
          listType={"Feed"}
          data={this.props.navigation.getParam("title", "NO-ID")}
          ViewMode={this.state.ViewMode}
          listScroll={this.listScroll}
          ActiveTab={this.state.routes[this.state.index].title}
          doScroll={this.doScroll}
        />
      );
    }
    else if (this.props.getTabView == "Trending") {
      return (
        <TrendingHomeFeed
          navigation={this.props.navigation}
          listType={"Feed"}
          data={this.props.navigation.getParam("title", "NO-ID")}
          ViewMode={this.state.ViewMode}
          listScroll={this.listScroll}
          ActiveTab={this.state.routes[this.state.index].title}
          doScroll={this.doScroll}
        />
      );
    }
    else {
      return (
        // <View style={{ flex: 1 }}>
          <TrendingHomeFeed
            navigation={this.props.navigation}
            listType={"Feed"}
            data={this.props.navigation.getParam("title", "NO-ID")}
            ViewMode={this.state.ViewMode}
            listScroll={this.listScroll}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        // </View>
        )
    }
  };

  followFeed = feedid => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    FeedFollowVariables.variables.feed_id = feedid;
    FeedFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: FeedFollowMutation,
        ...FeedFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowFeed = async Id => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    FeedUnFollowVariables.variables.feed_id = Id;
    applloClient
      .query({
        query: FeedUnFollowMutation,
        ...FeedUnFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  favroiteFeed = async Id => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    FeedFollowVariables.variables.feed_id = Id;
    FeedFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: FeedFollowMutation,
        ...FeedFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  render() {
    return (
      <View style={styles.container}>
        {Platform.OS == "web" && (
          <SEOMetaData
            title={
              this.props.feedDetails.getIn(["data", "external_feed"])
                ? this.props.feedDetails
                  .getIn(["data", "external_feed"])
                  .get("name")
                : ""
            }
            description={
              this.props.feedDetails.getIn(["data", "external_feed"])
                ? this.props.feedDetails
                  .getIn(["data", "external_feed"])
                  .get("description")
                : ""
            }
            image={
              this.props.feedDetails.getIn(["data", "external_feed"])
                ? this.props.feedDetails
                  .getIn(["data", "external_feed"])
                  .get("icon_url")
                : ""
            }
          />
        )}
        {// Platform.OS != "web" &&
          Dimensions.get("window").width <= 750 && (
            <Animated.View
              style={{
                position: Platform.OS == "web" ? "sticky" : null,
                top: 0,
                left: 0,
                right: 0,
                zIndex: 10,
                overflow: "hidden",
                borderRadius: Dimensions.get("window").width <= 750 ? 0 : 6,
              }}
            >
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <View
                  style={{
                    width: "100%",
                    flexDirection: "row",
                    marginBottom: Dimensions.get("window").width <= 750 ? 0 : 10,
                    backgroundColor: "#000",
                    borderRadius: Dimensions.get("window").width <= 750 ? 0 : 6,
                    height: 50,
                  }}
                >
                  <TouchableOpacity
                    style={ButtonStyle.headerBackStyle}
                    onPress={() => {
                      let nav = this.props.navigation.dangerouslyGetParent()
                        .state;
                      if (nav.routes.length > 1) {
                        this.props.navigation.goBack();
                        return;
                      } else {
                        this.props.navigation.navigate("home");
                      }
                    }}
                  >
                    <Icon
                      color={"#fff"}
                      name="angle-left"
                      type="font-awesome"
                      size={40}
                    />
                  </TouchableOpacity>
                  {this.props.feedDetails.size != 0 &&
                    !this.props.getsearchBarStatus &&
                    (
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          alignSelf: "center",
                          alignItems: "center",
                        }}
                        onPress={() =>
                          this.FeedProfilescrollview.scrollTo({
                            x: 0,
                            y: 0,
                            animated: true,
                          })
                        }
                      >
                        {/* <Image
                        source={{
                          uri:
                            this.props.feedDetails.getIn([
                              "data",
                              "external_feed"
                            ]) &&
                              this.props.feedDetails
                                .getIn(["data", "external_feed"])
                                .get("icon_url") != null
                              ? this.props.feedDetails
                                .getIn(["data", "external_feed"])
                                .get("icon_url")
                              : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAhFBMVEX/pQD/////ngD/oQD/owD/oAD/+fT/nQD///3/7NP/pgD/4r7//vr/+vH/t1D/vF3/+O7/79r/zo//5MP/xnr/16P/2Kj/9OX/rSv/xXf/uVb/zIn/6c3/267/qRP/05v/s0L/wGn/sDb/wW3/tkv/z5T/yYH/qyD/sDf/37b/x4T/ryzcdsqXAAAM+ElEQVR4nN1daXfqOgx0bKUQCIQd2rKWbtz+//93E6ALEI/kLJAw57xP9yXNYFmSpbGtPA6dYDHYdr+WLVUltJZf3e1gEXTY71fwX8PhaKa1MUR0a0oXiL/JGK0/RsMwM8P2RvumetROQcbXUTsLw85A6aqz+wZpGljN1cKwN9KVH72/IKMfew4Mm8/a3PqbnWH0c1PKsE3145fAUNp8vGQYRrWZf+cgHV361QuGgarnAB5gVMAxHDfqOoAHUGOMGY70rT8xN/QIMYz8W39fAfAjO8OnOk/BX5gnG8PoPgjGFKN0hqN7MNED/FEaw3H9ncwv9PiSYdC49VcVikZwzjBU9Y6D5yAVnjG8Gy/zjR9vc2TYvqdJeIBu/2XYrGCVIi+Imn8YPt+bjSYwz78Me/dnowl074fh4z0OYTyIo2+GnfscwngQO0eGg/scwngQB0eGd+hIDyB1YNi2Ztz6CN83ppqFbwZJTIwZRrYP19NObzrtB/N5e7J6e4zWrYRtQvWqn5kDtEkYhlY/ox+8c3T6w8lzNDN7otf81ozQYcxwaDfSS4ZHNKfD1fbQs7nm97rDH8YMR1ZPamd45NlfvL/4lS7/xyFReTPrB3IM9wiD1ZOpLEuaeQqEexHDPfrjqrLUDyoogmFisvPBrIIkdaAW9oTGiWGC6aSr/WqRNAsFUjZnhjHCXeRXiaQZqK39c7IwTEguXhuVMVfaqm7hDGN0xrOK9Fipq77KYBijP6qEtdJMLe3/mothjMVLBXqtSwWEQHkZxgO5vXkAgUKn/AwT0QpVwVgtKIJhjMmyAsaajoIYet5uVlGOhTH0vHY1ORbI0POGVeRYKMPYVpeV8zkFM4x9jqlGovODwhlWTjBXPEPP61VKU1YGQ8+bz6qjiCiHoeeN/aqYalkMvU5VTLU0hnF0bFViGHWq5rYYNB+rIIKkaLPZbB/f38aL9rzfKZjvfFmBYSTab2qI4Wutzcd6O9gFFsF4BmyrMIwnSOjGVP314yQoZIoOqyooP+xV2U76uSmG3Qo32BOafnd1Iat2xLgiccMGMpqiSa6Z2a+Cw8GIWc4GOYayGVXYUr8Rk2z9y06y6pZ6QExyOcjqeoKaKANiki8LvBfQhs5L5SfjEeT722zWuqnBZDwi9juLLBTf6kMxHkgaZEh52rXwN0fEM3I0dabYr4m/OcLojTPH3rJWFJVpOHMMa+NSjzB665rQvdaMYrI31zFA1m/3nDHnOx4ZbGtHkfyPuRPFxxoFxiNIR07T8b1+FJXxnUy1jhSVP3NZd9SSIumBA8XH2rmbBP6HwzDWz6MmIP0mp1jT3YL+Wu5Ur5PdmMNGi2SnRTE5MRl0Vs4pXq6Qhptde7cYr97+bbqfh60W+YnqRynD8AorjdPu2jRYDDb7nRa5/rJ5kVpqr/z1Ylr/MOzvntc6jx6PjDSL65ceFu0d0v44oswsqbESUix9dzLuAfdX60ZGCZDeCCmWXZ5iu9ydXWQykTSfwmXjptyYIenjh+0oi7lSS1jhKDdmCJUKncmnu9KJjKxy3CnVoTrsCho572IjvRO9GuzpyQ8XtUnorgRuTERvXpVI0VFPM1w7chRm4iUm4c6KoaDrpq7Qz5K3NstL3zJoooKu0zjqd8lLy8ttMqm+gk8XjjKKpR1/lFHX1l46LNBlFLslTcXMyj0XeaVoLoYlRcUce9c2clMVedRhOadY5VFfOujytCQubkux03z60mfxMDYEpY1yQkZOBW3/Q/q7a0GOOi/DTnNrhN+FCQD5gpVGGccE5VdBz4XySmrx68Vmq3g7LUDnLZVX0if/rmHxcb8QJfubzOGYDf+q4lPwYrT6c9nKUfPlqU7hzYyCdiP0ZqLfvsEXGcdFUyxsv8WT5MvI8KVi+zksGRlmExqmQNTypBf2PfOCnY1ZTSaL3bA/zc9UtP4xI/Y9RTub/U6LZKsFzaLnxZy/LcMOkS5Ps+lbeecfHm7LaG3GQdadM3MJRX4qlnyGZcxTm+44m9i5L4gatObe0rxC4zSRrb+7iYAOmAoo+uxicXKVDv+epPtISihq9rXXkmjGJD/Grj5WQJE+uJfsrie2IeM/OipI+7y78Vndzcc1dbam8eQmzReEbNZOr3yqM+lXJ47859GMe0fRuRv7Rbrr4nT47IaV+V3/aG7SW4d055119z4X9689iCqRFknFB57gFg0TMW+4ojv9hf8hn47sEGguobjJtgU6v9/Fjh6nbmCD4nUSmwuYlnQY2ZjBOpsbba+Rq2QHzBgQMenSzQ7o9tdCp8ppKw3TkbrdOflEMktl22WaiRibm22RIi2T5XNtCMOoNEsVoTAQKkjfGTvVTFJ/DYGtDX5XRJFZInBF8MUtte5mJlk4cgqLBh7E5k3V/PQhcalMUYkbxNteykFKInVmci9mJt7S16ikISigyKQ2XIH4qmv9S5ASGCqjj9X4FeMSzfRw8hDehEFL3t0w7TKDS4u9ku5RI6P9WXf7/m9/3wXYmsBXIzxvBSkS4adfSzBT8k3092ihzhAcNW8EcRE7Gx+LbIsPiaSf2pdti+lAWdZ7Pp/d4JILYwYPBTMk+xkCC4tWT5CjfsJBZIQ2T4WaqV6jitoqvdTLS4FwxDBb+HCRZkqc+Kz3mvbXiNiYAU69j+FDh/xQXNA3S758P0hz3ny7DN+d6eOTYArzpuaV5eclip6Uv8e3IeBXMr39SUFBn61fHhGkUWSnIs4vcXI6LcZMTy8Qdv1YanFPrdEgGmwDheSmEsnZN9opk4pVWEC9Gi3hs0W09YlcdENpZULWTmFig58uQmDDVthPkeI3+Ao2GghcVyxgpS9Q8ZwgrZDJtW1CxJAxUxxOBSDlqp9JWy74TNy3X4ymuJ5w7kUiE3HTkDKrmOQLF6WwN827Y4gxkVSkJYtcbx4VP5kFRs4xNLItgqdIueGbmKUi9DW4wJ9zIposqsS0vhATMeBttvhnXuUaxOSSS3ekpVLEZLZopUcwacwXEX35OR1/kdbFZgZxh+IaLNfYLxmVoJFNOptmpoST2xAxxL9OnhaNS0b6F6k3mzJtCOuFtirRKqMnYTBlYES7Hy+R6jeYhiAyUzz+4AJHnmGWWJEgdf8O9ssdtNT30ZN5Yr6fRSebIHXFx/xeqOgGE4ZmDoa8mtWC1EnF5CZvwNgMzB1zaMA4tYAV6a09/IOhYgbOa8EtnCzDrHtm/qUyNP/gQyjmwxVmjqwm8ximqxCYig2KF7Bsar92m2foftrxARa7weUCtNKDMT9HwU2yPTcVlnwfx1c4EZEfzrEFI2NaavVujJkihrCWkt2Z4mwJwDYxsDcFdVPcHMjegsLLFjusWQY+UxjVPg16MN11yyhmY2jNFPFSH6Wm0K3n6F5kTGrsft+gyh1KMKHTa2cPF9kmIvBt8ENR2RR29HPk3hI5xSXAT4onImizwJJiHjltJjMF1S9c+AGqWIKZaQ6GWUpRKMXAERGIt3G4yHOeRIbEDeqToZALrNZx7o0FHRjuIRFPe+hqYN6GvDBK2pm7x52ba0xHF69lkX3DlRySmrbUEjN07VxMGEUl6gaiegT0eWj3xVJ9MTbM9Y1Owa1ksNWndDx+GCLzBmtg+lJsZ0O7NNi4LVdYPQKKu3CdA9I26iq+yuEwFfljxmGqC35tOIFBjkFbJdhBJF4JC06tgKeJgEADs6E5yKIGSlAyJuEoslsnFRNgwTII6oVB/8kslEjyLjk07uFTsoiBPhHUTGEFBHDQgYLNyd//kfWoc9mWQFhHBz4Rtj1AlqEflLDKYZbQUpsj4Ql7cBkEym0waNljVLw6UtLuFOnIPoUW4mtvoU9EiekmE0MzihmKC6oxx1SnGk5a8mPLYV0Q1DEgQ/sC0R/GDB3axKRnqzNPEbY3TgddQ4YoriGGdl+imzFDJjM/+0PJ9birdjDtPfT688n7p+vR+mYVPlixA1YaNe3PWa00+V2U+yEM+6ur9f5GjwznzRttB9S3gefsQ9jeM7zV1u/yQQm75L+bbf0uG/v6lYITtebY10wShjfejlkaDv0cheNJrXFoBewZln3O221wrJgcGDbv0J0SNf8wvMHBRKXj+xDGI8O63uRmx89Gn2+GIahz1RGkwjOGzB6y2qHxswz6YVjeJRK3wJ/dq78MvVEt741Mhf9Hu/GH4f14m5PthH8ZisqBNcDpdsIThjW8FTsF/mlv5JRhPW/iPcX5uXFnDL2x241NlQM1zsv/5wy9QNV5Mhp1UQ68YOiFkesFapUB6eiy8XPJME7DxfXdasFQWpMxjaHXfHa/0PDmMPo5Vc2QytDzOqM818VeH2T0yCLuszCMOQ5UbeYjaRpYhTpWhjHam7zXG18BSX06Qk1+xDD2q8P3ma+5I6FuAzrcZvAxGuLN2Jhhgodg8bbtzpaMtOjKaC2/utvBIuBPFPsPHJSoM14DLo8AAAAASUVORK5CYII="
                        }}
                        style={{
                          width: 30,
                          height: 30,
                          borderRadius: 15,
                          marginLeft: 5
                        }}
                      /> */}
                        {/* <View
                        style={{
                          padding: 5,
                          alignItems: "center",
                          justifyContent: "center",
                          flexDirection:'row'
                        }}
                      > */}
                        <Text
                          style={{
                            color: "#fff",
                            fontWeight: "bold",
                            fontSize: 18,
                            fontFamily: ConstantFontFamily.MontserratBoldFont,
                            flex: 1,
                            flexWrap: "wrap",
                            flexShrink: 1,
                          }}
                        >
                          {this.props.navigation.state.params.title.replace(
                            "%2F",
                            "/"
                          )}
                        </Text>
                        {/* </View> */}

                        {/* {this.props.loginStatus == 1 && (
                        <View
                          style={{
                            flexDirection: "row",
                            position: "absolute",
                            right: 0,
                            marginRight: 20
                          }}
                        >
                          <ExternalFeedStar
                            FeedName={
                              this.props.feedDetails.getIn([
                                "data",
                                "external_feed"
                              ]) &&
                              this.props.feedDetails
                                .getIn(["data", "external_feed"])
                                .get("name")
                            }
                            FeedId={
                              this.props.feedDetails.getIn([
                                "data",
                                "external_feed"
                              ]) &&
                              this.props.feedDetails
                                .getIn(["data", "external_feed"])
                                .get("id")
                            }
                            ContainerStyle={{}}
                            ImageStyle={{
                              height: 20,
                              width: 20,
                              alignSelf: "center",
                              marginLeft: 5,
                              marginBottom: 5
                            }}
                          />
                        </View>
                      )} */}
                      </TouchableOpacity>
                    )}
                  <View style={ButtonStyle.headerRightStyle}>
                    <HeaderRight navigation={this.props.navigation} />
                  </View>
                </View>
              </View>
            </Animated.View>
          )}
        {this.state.cliksselectItem == "FEED" && (
          <ScrollView
            contentContainerStyle={{ height: "100%" }}
            ref={(ref) => {
              this.FeedProfilescrollview = ref;
            }}
            showsVerticalScrollIndicator={false}
            onScroll={(event) => {
              this.setState({
                scrollY: event.nativeEvent.contentOffset.y,
              });
            }}
            scrollEventThrottle={16}
          >
            <Animated.View
              onLayout={(event) => {
                let { x, y, width, height } = event.nativeEvent.layout;
                if (width > 0) {
                  this.setState({ ProfileHeight: height });
                }
              }}
              style={[
                Dimensions.get('window').width <= 750 ? ButtonStyle.cardBorderStyle : ButtonStyle.borderStyle,
                ButtonStyle.profileShadowStyle,
                {
                  overflow: "hidden",
                  marginBottom: Dimensions.get("window").width >= 750 ? 10 : 0,
                  marginTop: Dimensions.get("window").width <= 750 ? 0 : 10,
                  borderWidth: 0,
                  // maxHeight:
                  //   Dimensions.get("window").height / 2 - this.state.feedY,
                  marginHorizontal: Dimensions.get("window").width <= 750 ? 0 : 10,
                  // width: Dimensions.get("window").width <= 750 ? "97%" : "100%",
                }]}
            >
              {/* <View
                style={{
                  height:
                    Dimensions.get("window").height / 4 - this.state.feedY,
                  marginHorizontal:
                    Dimensions.get("window").width <= 750 ? 5 : 0,
                }}
              >
                {this.props.feedDetails.getIn(["data", "external_feed"]) &&
                  this.props.feedDetails
                    .getIn(["data", "external_feed"])
                    .get("icon_url") != null && (
                    <TouchableHighlight
                      style={{
                        overflow: "visible",
                      }}
                    >
                      <ImageBackground
                        style={{
                          width:
                            this.state.ProfileHeight - this.state.feedY < 0
                              ? 0
                              : "100%",
                          height:
                            Dimensions.get("window").height / 4 -
                            this.state.feedY,
                        }}
                        imageStyle={{
                          borderRadius:
                            Dimensions.get("window").width <= 750 ? 0 : 10,
                        }}
                        source={{
                          uri:
                            this.props.feedDetails.getIn([
                              "data",
                              "external_feed",
                            ]) &&
                            this.props.feedDetails
                              .getIn(["data", "external_feed"])
                              .get("topic")
                              .get("banner_pic"),
                        }}
                        resizeMode={"cover"}
                      ></ImageBackground>
                    </TouchableHighlight>
                  )}
                {this.props.isAdmin == true && (
                  <View
                    style={{
                      flexDirection: "column",
                      justifyContent: "flex-end",
                      flex: 1,
                      paddingHorizontal: 5,
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        width: "100%",
                        justifyContent: "flex-end",
                      }}
                    >
                      <Hoverable>
                        {(isHovered) => (
                          <Icon
                            color={"#000"}
                            iconStyle={{
                              color: "#fff",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                            reverse
                            name="trash"
                            type="font-awesome"
                            size={isHovered == true ? 18 : 16}
                            containerStyle={{
                              alignSelf: "flex-end",
                              justifyContent: "flex-end",
                            }}
                            onPress={() => this.showAlert()}
                          />
                        )}
                      </Hoverable>

                      <Hoverable>
                        {(isHovered) => (
                          <Icon
                            color={"#000"}
                            iconStyle={{
                              color: "#fff",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                            reverse
                            name="edit"
                            type="font-awesome"
                            size={isHovered == true ? 18 : 16}
                            containerStyle={{
                              alignSelf: "flex-end",
                              justifyContent: "flex-end",
                            }}
                            onPress={async () => {
                              await this.props.showFeedDetails({
                                name:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("name"),
                                feed_id:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("id"),
                                website:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("website"),
                                base_topic:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("base_topic"),
                                icon_url:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("icon_url"),
                                feedurl:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("url"),
                              });
                              this.props.navigation.navigate("editfeed");
                            }}
                          />
                        )}
                      </Hoverable>
                    </View>
                  </View>
                )}
              </View> */}

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingVertical: 15,
                  paddingRight: 20,
                  paddingLeft: 15,
                  alignItems: 'flex-start'
                }}
              >
                <View style={{ flexDirection: 'row', width: '85%' }} >
                  <Image
                    source={{
                      uri:
                        this.props.feedDetails.getIn([
                          "data",
                          "external_feed",
                        ]) &&
                          this.props.feedDetails
                            .getIn(["data", "external_feed"])
                            .get("icon_url") != null
                          ? this.props.feedDetails
                            .getIn(["data", "external_feed"])
                            .get("icon_url")
                          : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAhFBMVEX/pQD/////ngD/oQD/owD/oAD/+fT/nQD///3/7NP/pgD/4r7//vr/+vH/t1D/vF3/+O7/79r/zo//5MP/xnr/16P/2Kj/9OX/rSv/xXf/uVb/zIn/6c3/267/qRP/05v/s0L/wGn/sDb/wW3/tkv/z5T/yYH/qyD/sDf/37b/x4T/ryzcdsqXAAAM+ElEQVR4nN1daXfqOgx0bKUQCIQd2rKWbtz+//93E6ALEI/kLJAw57xP9yXNYFmSpbGtPA6dYDHYdr+WLVUltJZf3e1gEXTY71fwX8PhaKa1MUR0a0oXiL/JGK0/RsMwM8P2RvumetROQcbXUTsLw85A6aqz+wZpGljN1cKwN9KVH72/IKMfew4Mm8/a3PqbnWH0c1PKsE3145fAUNp8vGQYRrWZf+cgHV361QuGgarnAB5gVMAxHDfqOoAHUGOMGY70rT8xN/QIMYz8W39fAfAjO8OnOk/BX5gnG8PoPgjGFKN0hqN7MNED/FEaw3H9ncwv9PiSYdC49VcVikZwzjBU9Y6D5yAVnjG8Gy/zjR9vc2TYvqdJeIBu/2XYrGCVIi+Imn8YPt+bjSYwz78Me/dnowl074fh4z0OYTyIo2+GnfscwngQO0eGg/scwngQB0eGd+hIDyB1YNi2Ztz6CN83ppqFbwZJTIwZRrYP19NObzrtB/N5e7J6e4zWrYRtQvWqn5kDtEkYhlY/ox+8c3T6w8lzNDN7otf81ozQYcxwaDfSS4ZHNKfD1fbQs7nm97rDH8YMR1ZPamd45NlfvL/4lS7/xyFReTPrB3IM9wiD1ZOpLEuaeQqEexHDPfrjqrLUDyoogmFisvPBrIIkdaAW9oTGiWGC6aSr/WqRNAsFUjZnhjHCXeRXiaQZqK39c7IwTEguXhuVMVfaqm7hDGN0xrOK9Fipq77KYBijP6qEtdJMLe3/mothjMVLBXqtSwWEQHkZxgO5vXkAgUKn/AwT0QpVwVgtKIJhjMmyAsaajoIYet5uVlGOhTH0vHY1ORbI0POGVeRYKMPYVpeV8zkFM4x9jqlGovODwhlWTjBXPEPP61VKU1YGQ8+bz6qjiCiHoeeN/aqYalkMvU5VTLU0hnF0bFViGHWq5rYYNB+rIIKkaLPZbB/f38aL9rzfKZjvfFmBYSTab2qI4Wutzcd6O9gFFsF4BmyrMIwnSOjGVP314yQoZIoOqyooP+xV2U76uSmG3Qo32BOafnd1Iat2xLgiccMGMpqiSa6Z2a+Cw8GIWc4GOYayGVXYUr8Rk2z9y06y6pZ6QExyOcjqeoKaKANiki8LvBfQhs5L5SfjEeT722zWuqnBZDwi9juLLBTf6kMxHkgaZEh52rXwN0fEM3I0dabYr4m/OcLojTPH3rJWFJVpOHMMa+NSjzB665rQvdaMYrI31zFA1m/3nDHnOx4ZbGtHkfyPuRPFxxoFxiNIR07T8b1+FJXxnUy1jhSVP3NZd9SSIumBA8XH2rmbBP6HwzDWz6MmIP0mp1jT3YL+Wu5Ur5PdmMNGi2SnRTE5MRl0Vs4pXq6Qhptde7cYr97+bbqfh60W+YnqRynD8AorjdPu2jRYDDb7nRa5/rJ5kVpqr/z1Ylr/MOzvntc6jx6PjDSL65ceFu0d0v44oswsqbESUix9dzLuAfdX60ZGCZDeCCmWXZ5iu9ydXWQykTSfwmXjptyYIenjh+0oi7lSS1jhKDdmCJUKncmnu9KJjKxy3CnVoTrsCho572IjvRO9GuzpyQ8XtUnorgRuTERvXpVI0VFPM1w7chRm4iUm4c6KoaDrpq7Qz5K3NstL3zJoooKu0zjqd8lLy8ttMqm+gk8XjjKKpR1/lFHX1l46LNBlFLslTcXMyj0XeaVoLoYlRcUce9c2clMVedRhOadY5VFfOujytCQubkux03z60mfxMDYEpY1yQkZOBW3/Q/q7a0GOOi/DTnNrhN+FCQD5gpVGGccE5VdBz4XySmrx68Vmq3g7LUDnLZVX0if/rmHxcb8QJfubzOGYDf+q4lPwYrT6c9nKUfPlqU7hzYyCdiP0ZqLfvsEXGcdFUyxsv8WT5MvI8KVi+zksGRlmExqmQNTypBf2PfOCnY1ZTSaL3bA/zc9UtP4xI/Y9RTub/U6LZKsFzaLnxZy/LcMOkS5Ps+lbeecfHm7LaG3GQdadM3MJRX4qlnyGZcxTm+44m9i5L4gatObe0rxC4zSRrb+7iYAOmAoo+uxicXKVDv+epPtISihq9rXXkmjGJD/Grj5WQJE+uJfsrie2IeM/OipI+7y78Vndzcc1dbam8eQmzReEbNZOr3yqM+lXJ47859GMe0fRuRv7Rbrr4nT47IaV+V3/aG7SW4d055119z4X9689iCqRFknFB57gFg0TMW+4ojv9hf8hn47sEGguobjJtgU6v9/Fjh6nbmCD4nUSmwuYlnQY2ZjBOpsbba+Rq2QHzBgQMenSzQ7o9tdCp8ppKw3TkbrdOflEMktl22WaiRibm22RIi2T5XNtCMOoNEsVoTAQKkjfGTvVTFJ/DYGtDX5XRJFZInBF8MUtte5mJlk4cgqLBh7E5k3V/PQhcalMUYkbxNteykFKInVmci9mJt7S16ikISigyKQ2XIH4qmv9S5ASGCqjj9X4FeMSzfRw8hDehEFL3t0w7TKDS4u9ku5RI6P9WXf7/m9/3wXYmsBXIzxvBSkS4adfSzBT8k3092ihzhAcNW8EcRE7Gx+LbIsPiaSf2pdti+lAWdZ7Pp/d4JILYwYPBTMk+xkCC4tWT5CjfsJBZIQ2T4WaqV6jitoqvdTLS4FwxDBb+HCRZkqc+Kz3mvbXiNiYAU69j+FDh/xQXNA3S758P0hz3ny7DN+d6eOTYArzpuaV5eclip6Uv8e3IeBXMr39SUFBn61fHhGkUWSnIs4vcXI6LcZMTy8Qdv1YanFPrdEgGmwDheSmEsnZN9opk4pVWEC9Gi3hs0W09YlcdENpZULWTmFig58uQmDDVthPkeI3+Ao2GghcVyxgpS9Q8ZwgrZDJtW1CxJAxUxxOBSDlqp9JWy74TNy3X4ymuJ5w7kUiE3HTkDKrmOQLF6WwN827Y4gxkVSkJYtcbx4VP5kFRs4xNLItgqdIueGbmKUi9DW4wJ9zIposqsS0vhATMeBttvhnXuUaxOSSS3ekpVLEZLZopUcwacwXEX35OR1/kdbFZgZxh+IaLNfYLxmVoJFNOptmpoST2xAxxL9OnhaNS0b6F6k3mzJtCOuFtirRKqMnYTBlYES7Hy+R6jeYhiAyUzz+4AJHnmGWWJEgdf8O9ssdtNT30ZN5Yr6fRSebIHXFx/xeqOgGE4ZmDoa8mtWC1EnF5CZvwNgMzB1zaMA4tYAV6a09/IOhYgbOa8EtnCzDrHtm/qUyNP/gQyjmwxVmjqwm8ximqxCYig2KF7Bsar92m2foftrxARa7weUCtNKDMT9HwU2yPTcVlnwfx1c4EZEfzrEFI2NaavVujJkihrCWkt2Z4mwJwDYxsDcFdVPcHMjegsLLFjusWQY+UxjVPg16MN11yyhmY2jNFPFSH6Wm0K3n6F5kTGrsft+gyh1KMKHTa2cPF9kmIvBt8ENR2RR29HPk3hI5xSXAT4onImizwJJiHjltJjMF1S9c+AGqWIKZaQ6GWUpRKMXAERGIt3G4yHOeRIbEDeqToZALrNZx7o0FHRjuIRFPe+hqYN6GvDBK2pm7x52ba0xHF69lkX3DlRySmrbUEjN07VxMGEUl6gaiegT0eWj3xVJ9MTbM9Y1Owa1ksNWndDx+GCLzBmtg+lJsZ0O7NNi4LVdYPQKKu3CdA9I26iq+yuEwFfljxmGqC35tOIFBjkFbJdhBJF4JC06tgKeJgEADs6E5yKIGSlAyJuEoslsnFRNgwTII6oVB/8kslEjyLjk07uFTsoiBPhHUTGEFBHDQgYLNyd//kfWoc9mWQFhHBz4Rtj1AlqEflLDKYZbQUpsj4Ql7cBkEym0waNljVLw6UtLuFOnIPoUW4mtvoU9EiekmE0MzihmKC6oxx1SnGk5a8mPLYV0Q1DEgQ/sC0R/GDB3axKRnqzNPEbY3TgddQ4YoriGGdl+imzFDJjM/+0PJ9birdjDtPfT688n7p+vR+mYVPlixA1YaNe3PWa00+V2U+yEM+6ur9f5GjwznzRttB9S3gefsQ9jeM7zV1u/yQQm75L+bbf0uG/v6lYITtebY10wShjfejlkaDv0cheNJrXFoBewZln3O221wrJgcGDbv0J0SNf8wvMHBRKXj+xDGI8O63uRmx89Gn2+GIahz1RGkwjOGzB6y2qHxswz6YVjeJRK3wJ/dq78MvVEt741Mhf9Hu/GH4f14m5PthH8ZisqBNcDpdsIThjW8FTsF/mlv5JRhPW/iPcX5uXFnDL2x241NlQM1zsv/5wy9QNV5Mhp1UQ68YOiFkesFapUB6eiy8XPJME7DxfXdasFQWpMxjaHXfHa/0PDmMPo5Vc2QytDzOqM818VeH2T0yCLuszCMOQ5UbeYjaRpYhTpWhjHam7zXG18BSX06Qk1+xDD2q8P3ma+5I6FuAzrcZvAxGuLN2Jhhgodg8bbtzpaMtOjKaC2/utvBIuBPFPsPHJSoM14DLo8AAAAASUVORK5CYII=",
                    }}
                    style={{
                      height: 80,
                      width: 80,
                      padding: 0,
                      margin: 0,
                      borderRadius: 10,
                      borderWidth: 1,
                      borderColor: "#fff",
                    }}
                  />

                  <View style={{ marginLeft: 10,}}>
                    <View style={{marginBottom: 10}}>
                      <Text
                        style={{
                          justifyContent: "flex-start",
                          width: Dimensions.get("window").width <= 750 ? '60%' : "100%",
                          color: "#000",
                          fontWeight: "bold",
                          fontFamily: ConstantFontFamily.MontserratBoldFont,
                          fontSize: 16,
                          flexWrap: 'wrap',
                          flex: 1,
                          
                        }}
                      >
                        {this.props.feedDetails.getIn([
                          "data",
                          "external_feed",
                        ]) &&
                          this.props.feedDetails
                            .getIn(["data", "external_feed"])
                            .get("name")}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        marginTop:Dimensions.get("window").width <= 750 ? 10 : 0
                      }}
                    >
                      <Text
                        style={{
                          textAlign: "center",
                          alignSelf: "center",
                          color: "grey",
                          fontSize: 12,
                          fontFamily: ConstantFontFamily.defaultFont,
                        }}
                      >
                        {this.props.feedDetails.getIn([
                          "data",
                          "external_feed",
                        ]) &&
                          this.props.feedDetails
                            .getIn(["data", "external_feed"])
                            .get("followers") != null
                          ? this.props.feedDetails
                            .getIn(["data", "external_feed"])
                            .get("followers")
                          : 0}{" "}
                                Followers{" "}
                      </Text>
                      <Hoverable>
                        {(isHovered) => (
                          <TouchableOpacity
                            style={{
                              // justifyContent: "flex-start",
                              // alignContent: "flex-start",
                              flexDirection: "row",
                            }}
                            onPress={() =>
                              this.openWindow(
                                this.props.feedDetails.getIn([
                                  "data",
                                  "external_feed",
                                ]) &&
                                this.props.feedDetails
                                  .getIn(["data", "external_feed"])
                                  .get("website")
                              )
                            }
                          >
                            <View
                                style={{
                                  alignSelf: "center",
                                  marginLeft: 10,
                                }}
                              >
                                <Icon
                                  name="link"
                                  type="font-awesome"
                                  color="#6D757F"
                                  size={13}
                                />
                              </View>
                            <Text
                              numberOfLines={1}
                              style={{
                                // textAlign: "center",
                                alignSelf: "center",
                                color: "grey",
                                fontSize: 13,
                                fontFamily: ConstantFontFamily.defaultFont,
                                textDecorationLine:
                                  isHovered == true ? "underline" : "none",
                              }}
                            >
                              {" "}
                              {this.props.feedDetails.getIn([
                                "data",
                                "external_feed",
                              ]) &&
                                Dimensions.get("window").width <= 750 &&
                                truncate(
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("website")
                                    .replace("http://", "")
                                    .replace("https://", "")
                                    .replace("www.", "")
                                    .split(/[/?#]/)[0]
                                )}
                              {this.props.feedDetails.getIn([
                                "data",
                                "external_feed",
                              ]) &&
                                Dimensions.get("window").width >= 750 &&
                                this.props.feedDetails
                                  .getIn(["data", "external_feed"])
                                  .get("website")
                                  .replace("http://", "")
                                  .replace("https://", "")
                                  .replace("www.", "")
                                  .split(/[/?#]/)[0]}
                            </Text>
                          </TouchableOpacity>
                        )}
                      </Hoverable>
                    </View>

                  </View>
                </View>

                <View style={{ flexDirection: 'row',alignItems:'center' }}>
                  <TouchableOpacity
                    onMouseEnter={() => this.setState({ menu: true })}
                    onMouseLeave={() => this.setState({ menu: false })}
                  >
                    <Menu>
                      <MenuTrigger>
                        <Image
                          source={require("../assets/image/menu.png")}
                          style={{
                            height: 16,
                            width: 16,
                            marginTop: 0,
                            marginRight: Dimensions.get('window').width <= 750 ? 10 : 20
                          }}
                        />
                      </MenuTrigger>
                      <MenuOptions
                        optionsContainerStyle={{
                          borderRadius: 6,
                          borderWidth: 1,
                          borderColor: "#e1e1e1",
                          shadowColor: "transparent"
                        }}
                        customStyles={{
                          optionsContainer: {
                            minHeight: 50,
                            width: 150,
                            marginTop: 15,
                          }
                        }}
                      >
                        <MenuOption
                          onSelect={() => {
                            this.followFeed(this.props.feedDetails.getIn([
                              "data",
                              "external_feed",
                            ]) &&
                              this.props.feedDetails
                                .getIn(["data", "external_feed"])
                                .get("id"));
                          }}
                        >
                          <Hoverable>
                            {isHovered => (
                              <Text
                                style={{
                                  textAlign: "center",
                                  color: isHovered == true ? "#009B1A" : "#000",
                                  fontFamily: ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                Follow
                              </Text>
                            )}
                          </Hoverable>
                        </MenuOption>
                        <MenuOption
                          onSelect={() => {
                            this.favroiteFeed(this.props.feedDetails.getIn([
                              "data",
                              "external_feed",
                            ]) &&
                              this.props.feedDetails
                                .getIn(["data", "external_feed"])
                                .get("id"));
                          }}
                        >
                          <Hoverable>
                            {isHovered => (
                              <Text
                                style={{
                                  textAlign: "center",
                                  color: isHovered == true ? "#009B1A" : "#000",
                                  fontFamily: ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                Favourite
                              </Text>
                            )}
                          </Hoverable>
                        </MenuOption>
                        <MenuOption
                          onSelect={() => {
                            this.unfollowFeed(this.props.feedDetails.getIn([
                              "data",
                              "external_feed",
                            ]) &&
                              this.props.feedDetails
                                .getIn(["data", "external_feed"])
                                .get("id"));
                          }}
                        >
                          <Hoverable>
                            {isHovered => (
                              <Text
                                style={{
                                  textAlign: "center",
                                  color: isHovered == true ? "#009B1A" : "#000",
                                  fontFamily: ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                Unfollow
                              </Text>
                            )}
                          </Hoverable>
                        </MenuOption>

                        <MenuOption>
                          <Hoverable>
                            {isHovered => (
                              <Text
                                style={{
                                  textAlign: "center",
                                  color: isHovered == true ? "#009B1A" : "#000",
                                  fontFamily: ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                Report
                              </Text>
                            )}
                          </Hoverable>
                        </MenuOption>
                        {this.props.isAdmin == true &&
                          <MenuOption
                            onSelect={async () => {
                              await this.props.showFeedDetails({
                                name:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("name"),
                                feed_id:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("id"),
                                website:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("website"),
                                base_topic:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("base_topic"),
                                icon_url:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("icon_url"),
                                feedurl:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("url"),
                              });
                              this.props.navigation.navigate("editfeed");
                            }}
                          >
                            <Hoverable>
                              {isHovered => (
                                <Text
                                  style={{
                                    textAlign: "center",
                                    color: isHovered == true ? "#009B1A" : "#000",
                                    fontFamily:
                                      ConstantFontFamily.MontserratBoldFont
                                  }}
                                >
                                  Edit
                                </Text>
                              )}
                            </Hoverable>
                          </MenuOption>
                        }
                        {this.props.isAdmin == true &&
                          <MenuOption onSelect={() => this.showAlert()}>
                            <Hoverable>
                              {isHovered => (
                                <Text
                                  style={{
                                    textAlign: "center",
                                    color: isHovered == true ? "#009B1A" : "#000",
                                    fontFamily:
                                      ConstantFontFamily.MontserratBoldFont
                                  }}
                                >
                                  Delete
                                </Text>
                              )}
                            </Hoverable>
                          </MenuOption>
                        }
                      </MenuOptions>
                    </Menu>
                  </TouchableOpacity>
                  <ExternalFeedStar
                    FeedName={
                      this.props.feedDetails.getIn([
                        "data",
                        "external_feed",
                      ]) &&
                      this.props.feedDetails
                        .getIn(["data", "external_feed"])
                        .get("name")
                    }
                    FeedId={
                      this.props.feedDetails.getIn([
                        "data",
                        "external_feed",
                      ]) &&
                      this.props.feedDetails
                        .getIn(["data", "external_feed"])
                        .get("id")
                    }
                    ContainerStyle={{}}
                    ImageStyle={{
                      height: 20,
                      width: 20,
                      alignSelf: "center",
                      marginLeft: 5,
                    }}
                  />
                </View>


                {/* <View
                  style={{
                    flexDirection: "column",
                    // justifyContent: 'center'
                  }}
                > */}
                {/* <View
                    style={{
                      flexDirection: "row",
                      width: "100%",
                      alignItems: "center",
                      marginTop: 5,
                    }}
                  >                     */}


                {/* <Hoverable>
                        {(isHovered) => (
                          <Icon
                            color={"#000"}
                            iconStyle={{
                              color: "#fff",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                            reverse
                            name="trash"
                            type="font-awesome"
                            size={isHovered == true ? 18 : 16}
                            containerStyle={{
                              alignSelf: "flex-end",
                              justifyContent: "flex-end",
                            }}
                            onPress={() => this.showAlert()}
                          />
                        )}
                      </Hoverable>

                      <Hoverable>
                        {(isHovered) => (
                          <Icon
                            color={"#000"}
                            iconStyle={{
                              color: "#fff",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                            reverse
                            name="edit"
                            type="font-awesome"
                            size={isHovered == true ? 18 : 16}
                            containerStyle={{
                              alignSelf: "flex-end",
                              justifyContent: "flex-end",
                            }}
                            onPress={async () => {
                              await this.props.showFeedDetails({
                                name:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("name"),
                                feed_id:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("id"),
                                website:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("website"),
                                base_topic:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("base_topic"),
                                icon_url:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("icon_url"),
                                feedurl:
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("url"),
                              });
                              this.props.navigation.navigate("editfeed");
                            }}
                          />
                        )}
                      </Hoverable> */ }


                {/* </View> */}

                {/* <View
                    style={{
                      width: "100%",
                      flexDirection: "row",
                    }}
                  >
                    {Dimensions.get('window').width <= 750 &&
                      <View
                        style={{
                          width: "95%",
                          flexDirection: "row",
                          marginTop: 10,
                        }}
                      >
                        <Text
                          style={{
                            textAlign: "center",
                            alignSelf: "center",
                            color: "grey",
                            fontSize: 12,
                            fontFamily: ConstantFontFamily.defaultFont,
                          }}
                        >
                          {this.props.feedDetails.getIn([
                            "data",
                            "external_feed",
                          ]) &&
                            this.props.feedDetails
                              .getIn(["data", "external_feed"])
                              .get("followers") != null
                            ? this.props.feedDetails
                              .getIn(["data", "external_feed"])
                              .get("followers")
                            : 0}{" "}
                        Followers{" "}
                        </Text>
                        <Hoverable>
                          {(isHovered) => (
                            <TouchableOpacity
                              style={{
                                justifyContent: "flex-start",
                                alignContent: "flex-start",
                                flexDirection: "row",
                                width: "48%",
                              }}
                              onPress={() =>
                                this.openWindow(
                                  this.props.feedDetails.getIn([
                                    "data",
                                    "external_feed",
                                  ]) &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("website")
                                )
                              }
                            >
                              <Text
                                numberOfLines={1}
                                style={{
                                  flex: 1,
                                  // textAlign: "center",
                                  alignSelf: "center",
                                  color: "grey",
                                  fontSize: 13,
                                  fontFamily: ConstantFontFamily.defaultFont,
                                  textDecorationLine:
                                    isHovered == true ? "underline" : "none",
                                }}
                              >
                                <View
                                  style={{
                                    alignSelf: "center",
                                    marginLeft: 10,
                                  }}
                                >
                                  <Icon
                                    name="link"
                                    type="font-awesome"
                                    color="#6D757F"
                                    size={13}
                                  />
                                </View>{" "}
                                {this.props.feedDetails.getIn([
                                  "data",
                                  "external_feed",
                                ]) &&
                                  Dimensions.get("window").width <= 750 &&
                                  truncate(
                                    this.props.feedDetails
                                      .getIn(["data", "external_feed"])
                                      .get("website")
                                      .replace("http://", "")
                                      .replace("https://", "")
                                      .replace("www.", "")
                                      .split(/[/?#]/)[0]
                                  )}
                                {this.props.feedDetails.getIn([
                                  "data",
                                  "external_feed",
                                ]) &&
                                  Dimensions.get("window").width >= 750 &&
                                  this.props.feedDetails
                                    .getIn(["data", "external_feed"])
                                    .get("website")
                                    .replace("http://", "")
                                    .replace("https://", "")
                                    .replace("www.", "")
                                    .split(/[/?#]/)[0]}
                              </Text>
                            </TouchableOpacity>
                          )}
                        </Hoverable>
                      </View>}
                  </View> */}
                {/* </View> */}
              </View>
            </Animated.View>
            <View
              style={
                Dimensions.get("window").width >= 750 &&
                this.state.scrollY >= this.state.ProfileHeight
                  ? [
                    styles.header,
                    {
                      flexDirection: "row",
                      height: "100%",
                      top:
                        this.state.scrollY >= this.state.ProfileHeight
                          ? this.state.scrollY
                          : 0,
                      marginHorizontal: Dimensions.get("window").width >= 750 ? 5 : 0,
                      width: Dimensions.get("window").width >= 750 ? "99%" : '100%',
                    },
                  ]
                  : 
                {
                  flexDirection: "row",
                  height: "100%",
                  marginHorizontal: Dimensions.get("window").width >= 750 ? 5 : 0,
                  width: Dimensions.get("window").width >= 750 ? "99%" : '100%',
                }
              }
            >
              {Dimensions.get("window").width <= 750
                // && this.props.getTabView 
                ? (
                  <View
                    style={{
                      flex: 1,
                      width: "100%",
                      height: '100%',
                      // padding: 10
                    }}
                  // contentContainerStyle={{flex:1}}
                  >
                    {this.renderTabViewForMobile()}
                  </View>
                ) : (
                  <View
                    style={{
                      width:
                        Platform.OS == "web" &&
                          Dimensions.get("window").width >= 750
                          ? 450
                          : "100%",
                      flex: 1,
                    }}
                  >
                    <TabView
                      swipeEnabled={false}
                      lazy
                      navigationState={this.state}
                      renderScene={this._renderScene}
                      renderLazyPlaceholder={this._renderLazyPlaceholder}
                      renderTabBar={this._renderTabBar}
                      onIndexChange={this._handleIndexChange}
                      style={{
                        marginRight: 1,
                      }}
                    />
                  </View>
                )}
              <View
                style={{
                  width: 450,
                  height: "100%",
                  marginLeft: 20,
                  marginBottom: 5,
                  flex: 1,
                  display:
                    Dimensions.get("window").width >= 750 &&
                      Platform.OS == "web"
                      ? "flex"
                      : "none",
                }}
              >
                <CommentDetailScreen
                  navigation={this.props.navigation}
                  postId={
                    this.props.PostDetails && this.props.PostDetails.post.id
                  }
                  listScroll={this.listScroll}
                  ProfileHeight={this.state.ProfileHeight}
                  scrollY={this.state.scrollY}
                  ActiveTab={this.state.routes[this.state.index].title}
                />
              </View>
            </View>
          </ScrollView>
        )}
        {Dimensions.get("window").width <= 750 &&
          <BottomScreen navigation={NavigationService} />
        }
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  TopicsFeedList: state.TopicsFeedReducer.getIn(["TopicsFeedList"]),
  topicsDetails: state.TrendingTopicsProfileReducer.get(
    "getTrendingTopicsProfileDetails"
  ),
  feedDetails: state.FeedProfileReducer.get("getFeedProfileDetails"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getUserFollowFeedList: state.LoginUserDetailsReducer.get("userFollowFeedList")
    ? state.LoginUserDetailsReducer.get("userFollowFeedList")
    : List(),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getTabView: state.AdminReducer.get("tabType"),
  getsearchBarStatus: state.AdminReducer.get("searchBarOpenStatus"),
});

const mapDispatchToProps = (dispatch) => ({
  setHASSCROLLEDACTION: (payload) => dispatch(setHASSCROLLEDACTION(payload)),
  topicId: (payload) => dispatch(getTrendingTopicsProfileDetails(payload)),
  saveLoginUser: (payload) => dispatch(saveUserLoginDaitails(payload)),
  setSignUpModalStatus: (payload) => dispatch(setSIGNUPMODALACTION(payload)),
  setLoginModalStatus: (payload) => dispatch(setLOGINMODALACTION(payload)),
  setFeedDetails: (payload) => dispatch(getFeedProfileDetails(payload)),
  showFeedDetails: (payload) => dispatch(editFeed(payload)),
  setPostCommentReset: (payload) =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload }),
  searchOpenBarStatus: payload => dispatch({ type: "SEARCHBAR_STATUS", payload })
});

const ExternalFeedScreenWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" },
})(ExternalFeedScreen);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  ExternalFeedScreenWrapper
);

const styles = StyleSheet.create({
  header: {
    position: Platform.OS == "web" ? "fixed" : null,
    left: 0,
    right: 0,
    zIndex: 10,
  },
  container: {
    flex: 1,
    backgroundColor: ConstantColors.customeBackgroundColor,
    paddingTop: Dimensions.get("window").width <= 1100 ? 0 : 10,
    paddingHorizontal: Dimensions.get("window").width <= 1100 ? 0 : 10,
    //  Platform.OS == "web" ? 10 : 5,
    height: "100%",
  },
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont,
    fontSize: 16,
  },
  inputAndroid: {
    paddingHorizontal: Dimensions.get("window").width > 750 ? 10 : 3,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#fff",
    borderRadius: 8,
    color: "#000",
    backgroundColor: "white",
    paddingRight: Dimensions.get("window").width > 750 ? 30 : 0,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont,
    fontSize: 16,
  },
  actionButtonIcon: {
    fontSize: 20,
    color: "white",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
  },
});
