import { launchImageLibraryAsync } from "expo-image-picker";
import { askAsync, CAMERA_ROLL } from "expo-permissions";
import { List } from "immutable";
import React, { Component } from "react";
import { graphql } from "react-apollo";
import {
  Animated,
  AsyncStorage,
  Dimensions,
  ImageBackground,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Button, Icon, Tooltip } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import BottomFloatingButton from "../components/BottomFloatingButton";
import NewHeader from "../components/NewHeader";
import TataStructure from "../components/TataStructure";
import AppHelper from "../constants/AppHelper";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import ConstantTooltip from "../constants/Tooltip";
import {
  CreateTopicMutation,
  EditTopicMutation
} from "../graphqlSchema/graphqlMutation/PostMutation";
import {
  GetChildTopicsMutation,
  TopicQuery
} from "../graphqlSchema/graphqlMutation/TrendingMutation";
import {
  SearchTopicMutation,
  SearchUserMutation
} from "../graphqlSchema/graphqlMutation/SearchMutation";
import {
  UserLoginMutation,
  UserQueryMutation
} from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  CreateTopicVariables,
  EditTopicVariables
} from "../graphqlSchema/graphqlVariables/PostVariables";
import {
  SearchTopicVariables,
  SearchUserVariables
} from "../graphqlSchema/graphqlVariables/SearchVariables";
import { UserDetailsVariables } from "../graphqlSchema/graphqlVariables/UserVariables";
import { uploadBannerImageAsync } from "../services/UserService";
import ButtonStyle from "../constants/ButtonStyle";
import BottomScreen from "../components/BottomScreen";
import NavigationService from "../library/NavigationService";

class EditTopicScreen extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.Pagescrollview = null;
    this.state = {
      value: 50,
      showSlider: false,
      textStyle: "WHITE",
      uploading: false,
      profilePic: "",
      title: "",
      text: "",
      items: [],
      selectText: "Cliks Name",
      uploadMutipleImagePost: [],
      changeBackPicEnable: this.props.topicdetails.get("changeBackPicEnable"),
      setBackPic: this.props.topicdetails.get("setBackPic"),
      gradientColor: "rgba(0,0,0,0.9)",
      brightnessvalue: 0.5,
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      conditionIcon: false,
      focusDesc:false,
      focusParentTopic: false,
      focusTopicname: false,
      summary: "",
      RoleItems: [
        {
          label: "Member",
          value: "Member",
          key: 0
        },
        {
          label: "Admin",
          value: "Admin",
          key: 1
        },
        {
          label: "Super Admin",
          value: "Super Admin",
          key: 2
        }
      ],
      SelectRoleItems: "Member",
      topicName: this.props.topicdetails.get("topicName"),
      rtopicName: this.props.topicdetails.get("rtopicName"),
      description: this.props.topicdetails.get("description"),
      topic_id: this.props.topicdetails.get("topic_id"),
      MutipleUser: [],
      MutipleUserList: [],
      showError: false,
      getImage: "",
      showcliktooltip: false,
      showaddmembertooltip: false,
      MutipleQualification: [],
      bannerHover: false,
      clikHover: false,
      descriptionHover: false,
      joinclikHover: false,
      website: "",
      qualification: "",
      modalVisible: false,
      TopicList: [],
      TopicListHierarchy: [],
      UserList: [],
      showDeleteIcon: false,
      inputParentName: false,
      rtopicParents: "#000",
      rtopicselected: "",
      parentsOfSelectedTopic: []
    };
    this.changeBannerImage = "";
    this.baseState = this.state;
  }

  componentDidMount = async () => {
    this._isMounted = true;
    const profileData = this.props.profileData;
    let userProfilePic = profileData
      .getIn(["my_users", "0", "user"])
      .getIn(["profile_pic"]);
    this.setState({
      profilePic: {
        uri: userProfilePic
      }
    });
    let joined = [];
    this.props.listTrending_cliks.map(async (value, index) => {
      await joined.push({
        id: value.node.id,
        name: value.node.name
      });
    });
    this.setState({ items: joined });
    if (this.state.rtopicName) {
      this.renderTopicListHierarchy(this.state.rtopicName);
      this.getTopicProfileDetails(this.state.rtopicName);
    }
  };

  componentDidUpdate() {
    if (this.props.getHasScrollTop == true && this.Pagescrollview) {
      this.Pagescrollview.scrollTo({ x: 0, y: 0, animated: true });
      this.props.setHASSCROLLEDACTION(false);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  _askPermission = async (type, failureMessage) => {
    const { status, permissions } = await askAsync(type);
    if (status === "denied") {
      alert(failureMessage);
    }
  };

  _pickBannerImage = async () => {
    await this._askPermission(
      CAMERA_ROLL,
      "We need the camera-roll permission to read pictures from your phone..."
    );
    let pickerResult = await launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    });
    this._handleBannerImagePicked(pickerResult);
  };

  _handleBannerImagePicked = async pickerResult => {
    let uploadResponse, uploadResult;
    try {
      this.setState({ uploading: true });
      if (!pickerResult.cancelled) {
        uploadResponse = await uploadBannerImageAsync(pickerResult.uri);
        uploadResult = await uploadResponse.json();
        this.setState({
          setBackPic: pickerResult.uri
        });
        this.setState({
          changeBackPicEnable: uploadResult.id,
          showDeleteIcon: true
        });
      }
    } catch (e) {
      alert("Upload failed, sorry :(" + e + ")");
    } finally {
      this.setState({ uploading: false });
    }
  };

  editTopic = async () => {
    let __self = this;
    EditTopicVariables.variables.topic_id = this.state.topic_id;
    EditTopicVariables.variables.name = this.state.topicName;
    EditTopicVariables.variables.description = this.state.description;
    EditTopicVariables.variables.banner_pic = this.state.changeBackPicEnable;
    EditTopicVariables.variables.parent = this.state.rtopicName;
    try {
      let id = await AsyncStorage.getItem("UserId");
      await applloClient
        .query({
          query: EditTopicMutation,
          ...EditTopicVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState(this.baseState);
          let resDataLogin = await __self.props.Login();
          await __self.props.saveLoginUser(resDataLogin.data.login);
          if (resDataLogin) {
            await __self.props.getTrendingTopics({
              currentPage: AppHelper.PAGE_LIMIT
            });
            __self.props.navigation.navigate("home");
          }
        });
    } catch (e) {
      console.log(e);
    }
  };

  checkTopicname = async name => {
    var letters = /^[0-9a-zA-Z-]+$/;
    if (name.match(letters) || name == "") {
      if (name[0] == "-") {
        alert("- not allowed at initial of clik name");
        return false;
      }
      this.setState({ topicName: name });
      return true;
    } else {
      alert("Please input alphanumeric characters only");
      return false;
    }
  };

  checkRTopicname = async name => {
    var letters = /^[0-9a-zA-Z-]+$/;
    if (name.match(letters) || name == "") {
      if (name[0] == "-") {
        alert("- not allowed at initial of clik name");
        return false;
      }
      this.setState(
        {
          rtopicName: name,
          TopicListHierarchy: [],
          rtopicParents: "#000"
        },
        () => {
          this.customRenderTopicSuggestion(name);
        }
      );
      return true;
    } else {
      alert("Please input alphanumeric characters only");
      return false;
    }
  };

  customRenderTopicSuggestion = value => {
    SearchTopicVariables.variables.prefix = value;
    applloClient
      .query({
        query: SearchTopicMutation,
        ...SearchTopicVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        this.setState({
          TopicList: res.data.search.topics,
          TopicListHierarchy: []
        });
      });
  };

  getTopicProfileDetails = value => {
    applloClient
      .query({
        query: TopicQuery,
        variables: {
          id: "Topic:" + value
        },
        fetchPolicy: "no-cache"
      })
      .then(res => {
        this.setState({
          parentsOfSelectedTopic: res.data.topic.parents
            ? [...res.data.topic.parents, value]
            : [value]
        });
      });
  };
  getChildStructure(arr, selectTopic, childrenOfSelectTopic) {
    let output = [];
    let i = 0;
    if (arr.length > 0)
      output[i] = {
        name: arr[i],
        children:
          selectTopic === arr[i]
            ? [...childrenOfSelectTopic]
            : this.getChildStructure(
                arr.slice(i + 1),
                selectTopic,
                childrenOfSelectTopic
              )
      };

    return output;
  }
  renderTopicListHierarchy = value => {
    this.setState({
      TopicListHierarchy: []
    });
    if (value) {
      applloClient
        .query({
          query: GetChildTopicsMutation,
          variables: {
            id: "Topic:" + value
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          let { children } = res.data.topic;
          this.setState({
            TopicListHierarchy: this.getChildStructure(
              this.state.parentsOfSelectedTopic,
              value,
              [{ name: "dynamicText" }, ...children]
            )
          });
        });
    }
  };

  handleTopicSelectInput = (topic, parents, item) => {
    this.setState(
      {
        rtopicName: topic,
        TopicList: [],
        TopicListHierarchy: []
      },
      () => {
        if (parents) {
          this.colorCondition(parents);
        } else {
          this.colorCondition("");
        }
        if (topic != "") {
          if (item.banner_pic) {
            let v = item.banner_pic.split("/");
            let k = v[v.length - 1].substring(0, v[v.length - 1].indexOf("."));
            this.setState({
              setBackPic: item.banner_pic,
              showDeleteIcon: true,
              changeBackPicEnable: k
            });
          }

          this.getTopicProfileDetails(topic);
          this.renderTopicListHierarchy(topic);
        }
        if (this.state.rtopicName == "") {
          this.setState({
            rtopicParents: "",
            TopicList: []
          });
        }
      }
    );
  };

  colorCondition = parent => {
    if (parent.length > 0) {
      this.setState({
        rtopicParents: "#009B1A"
      });
    } else if (parent == "") {
      this.setState({
        rtopicParents: "#009B1A"
      });
    } else if (parent == null) {
      this.setState({
        rtopicParents: "#009B1A"
      });
    }
  };

  render() {
    const { setBackPic } = this.state;
    const textStyle = styles.usertext;
    return (
      <View
        style={[
          //ButtonStyle.borderStyle,
          {
            flex: 1,
            backgroundColor: ConstantColors.customeBackgroundColor,
            width: "100%",
            // paddingTop: Dimensions.get("window").width <= 1100 ? 0 : 10,
            paddingHorizontal: Dimensions.get("window").width <= 1100 ? 0 : 10
          }
        ]}
      >
        <ScrollView
          ref={scrollview => {
            this.Pagescrollview = scrollview;
          }}
          showsVerticalScrollIndicator={false}
          onLayout={event => {
            let { x, y, width, height } = event.nativeEvent.layout;
            if (width < 1024) {
              this.setState({
                showsVerticalScrollIndicatorView: true,
                currentScreentWidth: width
              });
            } else {
              this.setState({
                showsVerticalScrollIndicatorView: false,
                currentScreentWidth: width
              });
            }
          }}
          style={{
            height:
              Platform.OS !== "web"
                ? null
                : Dimensions.get("window").height - 80
          }}
        >
          <View>
            {Dimensions.get("window").width <= 750 && (
              <Animated.View
                style={{
                  position: Platform.OS == "web" ? "sticky" : null,
                  top: 0,
                  left: 0,
                  right: 0,
                  zIndex: 10,
                  overflow: "hidden",
                  // borderRadius: 20
                }}
              >
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      flexDirection: "row",
                      backgroundColor: "#000",
                      // borderRadius: 20,
                      height: 50
                    }}
                  >
                    <TouchableOpacity
                      style={ButtonStyle.headerBackStyle}
                      onPress={() => {
                        let nav = this.props.navigation.dangerouslyGetParent()
                          .state;
                        if (nav.routes.length > 1) {
                          this.props.navigation.goBack();
                          return;
                        } else {
                          this.props.navigation.navigate("home");
                        }
                      }}
                    >
                      <Icon
                        color={"#fff"}
                        name="angle-left"
                        type="font-awesome"
                        size={40}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[ButtonStyle.headerTitleStyle, {backgroundColor:"#000"}]}
                    >
                      <Text
                        style={{
                          color: "#fff",
                          textAlign: "center",
                          fontWeight: "bold",
                          fontSize: 18,
                          fontFamily: ConstantFontFamily.MontserratBoldFont
                        }}
                      >
                        Edit Topic
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Animated.View>
            )}

            <View
              style={[
                // ButtonStyle.borderStyle,
                {
                  backgroundColor: "#fff",
                  padding: 10,
                 // marginTop: 10
                }
              ]}
            >
              {/* <View style={{ flexDirection: "row", width: "100%" }}>
                <View
                  style={{
                    width: "70%",
                    justifyContent: "flex-start",
                    flexDirection: "row"
                  }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      marginTop: 10,
                      marginRight: 20,
                      fontSize: 16,
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }}
                  >
                    Banner Picture
                  </Text>
                </View>
                <TouchableOpacity
                  style={{
                    width: "30%",
                    justifyContent: "flex-end",
                    alignItems: "flex-end"
                  }}
                  onMouseEnter={() => this.setState({ bannerHover: true })}
                  onMouseLeave={() => this.setState({ bannerHover: false })}
                >
                  {setBackPic == null && (
                    <Icon
                      color={"#f80403"}
                      iconStyle={{
                        marginTop: 10,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      name="times"
                      type="font-awesome"
                      size={16}
                    />
                  )}
                  {this.state.bannerHover == true && Platform.OS == "web" ? (
                    <Tooltip
                      backgroundColor={"#d3d3d3"}
                      withPointer={false}
                      withOverlay={false}
                      toggleOnPress={true}
                      containerStyle={{
                        left: -60
                      }}
                      popover={
                        <Text
                          style={{
                            fontFamily: ConstantFontFamily.defaultFont
                          }}
                        >
                          Upload a Banner picture
                        </Text>
                      }
                    />
                  ) : null}
                </TouchableOpacity>
              </View> */}
              {/* <View
                style={{
                  height: Dimensions.get("window").height / 3,
                  // borderColor: "#e1e1e1",
                  // borderWidth: 1,
                  borderRadius: Platform.OS == "web" ? 5 : null
                }}
              >
                <ImageBackground
                  style={{
                    width: "100%",
                    height: "100%",
                    backgroundColor: "#fff"
                  }}
                  source={{
                    uri: setBackPic
                  }}
                  resizeMode={"cover"}
                >
                  {this.state.showDeleteIcon == true && (
                    <View style={{ height: 50 }}>
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          color: "#fff",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                        reverse
                        name="trash"
                        type="font-awesome"
                        size={16}
                        containerStyle={{
                          flex: 1,
                          position: "absolute",
                          top: 3,
                          right: 3
                        }}
                        onPress={() =>
                          this.setState({
                            setBackPic: null,
                            changeBackPicEnable: null,
                            showDeleteIcon: false
                          })
                        }
                      />
                    </View>
                  )}
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    reverse
                    name="camera"
                    type="font-awesome"
                    size={20}
                    containerStyle={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                      flex: 1
                    }}
                    onPress={this._pickBannerImage}
                  />
                </ImageBackground>
              </View> */}

              <View style={{ flexDirection: "row", width: "100%" }}>
                <View
                  style={{
                    width: "90%",
                    justifyContent: "flex-start",
                    flexDirection: "row"
                  }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      marginTop: 10,
                      marginRight: 20,
                      fontSize: 16,
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }}
                  >
                    Topic Name
                  </Text>
                </View>
                <TouchableOpacity
                  style={{
                    width: "10%",
                    justifyContent: "flex-end",
                    alignItems: "flex-end"
                  }}
                  onMouseEnter={() => this.setState({ clikHover: true })}
                  onMouseLeave={() => this.setState({ clikHover: false })}
                >
                  {(this.state.topicName.length < 3 ||
                    this.state.topicName.length > 24 ||
                    this.state.topicName[this.state.topicName.length - 1] ==
                      "-") && (
                    <Icon
                      color={"#f80403"}
                      iconStyle={{
                        marginTop: 10,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      name="times"
                      type="font-awesome"
                      size={16}
                    />
                  )}
                  {this.state.clikHover == true && Platform.OS == "web" ? (
                    <Tooltip
                      backgroundColor={"#d3d3d3"}
                      withPointer={false}
                      withOverlay={false}
                      toggleOnPress={true}
                      containerStyle={{
                        left: -60,
                        top: -60
                      }}
                      popover={
                        <Text>Topic Name should be 3 to 24 Characters</Text>
                      }
                    />
                  ) : null}
                </TouchableOpacity>
              </View>

              <View style={{ flexDirection: "row", width: "100%" }}>
                <TextInput
                  value={this.state.topicName}
                  placeholder="2-31 characters, only alphanumeric and underscores."
                  placeholderTextColor="#6D757F"
                  style={[
                    textStyle,
                    this.state.focusTopicname ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,                    
                    {
                      width: "100%",
                      borderRadius: 5,
                      // borderColor: "#e1e1e1",
                      borderWidth: 0,
                      marginTop: 5,
                      height: 45,
                      paddingVertical: 5,
                      paddingHorizontal: 10,
                      fontFamily: ConstantFontFamily.defaultFont,
                      outline: 'none',                      
                    }
                  ]}
                  onChangeText={topicName => this.checkTopicname(topicName)}
                  onFocus = {()=> this.setState({focusTopicname: true})}
                  onBlur = {()=> this.setState({focusTopicname: false})}
                />
              </View>

              <View style={{ flexDirection: "row", width: "100%" }}>
                <View
                  style={{
                    width: "90%",
                    justifyContent: "flex-start",
                    flexDirection: "row"
                  }}
                >
                  <View
                    style={{
                      width: "70%",
                      justifyContent: "flex-start",
                      flexDirection: "row"
                    }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: 10,
                        marginRight: 20,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Parent Topic Name
                    </Text>
                  </View>
                </View>
                <TouchableOpacity
                  style={{
                    width: "10%",
                    justifyContent: "flex-end",
                    alignItems: "flex-end"
                  }}
                  onMouseEnter={() => this.setState({ clikHover: true })}
                  onMouseLeave={() => this.setState({ clikHover: false })}
                >
                  {(this.state.rtopicName.length < 3 ||
                    this.state.rtopicName.length > 24 ||
                    this.state.rtopicName[this.state.rtopicName.length - 1] ==
                      "-") && (
                    <Icon
                      color={"#f80403"}
                      iconStyle={{
                        marginTop: 10,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      name="times"
                      type="font-awesome"
                      size={16}
                    />
                  )}
                  {this.state.clikHover == true && Platform.OS == "web" ? (
                    <Tooltip
                      backgroundColor={"#d3d3d3"}
                      withPointer={false}
                      withOverlay={false}
                      toggleOnPress={true}
                      containerStyle={{
                        left: -60,
                        top: -60
                      }}
                      popover={
                        <Text>Topic1 name should be 3 to 24 Characters</Text>
                      }
                    />
                  ) : null}
                </TouchableOpacity>
              </View>

              <View style={{ flexDirection: "row", width: "100%" }}>
                <View
                  style={{
                    width: "100%",
                    marginVertical: 5,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <TextInput
                    value={this.state.rtopicName}
                    placeholder="Type and select an existing topic"
                    placeholderTextColor="#6D757F"
                    onChangeText={rtopicName => {
                      this.checkRTopicname(rtopicName);
                    }}
                    style={[
                      textStyle,
                      this.state.focusParentTopic ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                      {
                        width: "100%",
                        borderRadius: 5,
                        // borderColor: "#e1e1e1",
                        borderWidth: 0,
                        // marginTop: 5,
                        height: 45,
                        paddingVertical: 5,
                        paddingHorizontal: 10,
                        fontFamily: ConstantFontFamily.defaultFont,
                        outline:'none'
                      }
                    ]}
                    onFocus={e => {
                      this.setState({
                        inputParentName: true
                      });
                    }}
                    onFocus = {()=> this.setState({focusParentTopic: true})}
                    onBlur = {()=> this.setState({focusParentTopic: false})}
                  />
                  {this.state.inputParentName == true &&
                    this.state.TopicList.map((item, index) => {
                      return (
                        <View
                          key={item.name}
                          style={{
                            backgroundColor: "#FEFEFA",
                            width: "100%",
                            padding: 5
                          }}
                        >
                          <View
                            style={{
                              padding: 5,
                              backgroundColor: item.parents
                                ? "#e3f9d5"
                                : "#e3f9d5",
                              borderRadius: 5,
                              alignSelf: "flex-start",
                              alignItems: "center"
                            }}
                          >
                            <Text
                              style={{
                                color: item.parents ? "#009B1A" : "#009B1A",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                fontWeight: "bold"
                              }}
                              onPress={() =>
                                this.handleTopicSelectInput(
                                  item.name,
                                  item.parents,
                                  item
                                )
                              }
                            >
                              /{item.name.toLowerCase()}
                            </Text>
                          </View>
                        </View>
                      );
                    })}
                </View>
              </View>
              {this.state.TopicListHierarchy.length > 0 && (
                <View>
                  <View style={{ flexDirection: "row", width: "100%" }}>
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginVertical: 10,
                        marginRight: 20,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Topic Hierarchy
                    </Text>
                  </View>
                  <View
                    style={{
                      // borderWidth: 1,
                      // borderColor: "#e1e1e1",
                      borderRadius: 5,
                      padding: 10
                    }}
                  >
                    <TataStructure
                      isRoot={true}
                      item={this.state.TopicListHierarchy[0]}
                      topicName={this.state.topicName}
                    />
                  </View>
                </View>
              )}

              <View style={{ flexDirection: "row", width: "100%" }}>
                <View
                  style={{
                    width: "70%",
                    justifyContent: "flex-start",
                    flexDirection: "row"
                  }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      marginTop: 10,
                      marginRight: 20,
                      fontSize: 16,
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }}
                  >
                    Description
                  </Text>
                </View>
                <TouchableOpacity
                  style={{
                    width: "30%",
                    justifyContent: "flex-end",
                    alignItems: "flex-end"
                  }}
                  onMouseEnter={() => this.setState({ descriptionHover: true })}
                  onMouseLeave={() =>
                    this.setState({ descriptionHover: false })
                  }
                >
                  {(this.state.description.length < 50 ||
                    this.state.description.length > 300) && (
                    <Icon
                      color={"#f80403"}
                      iconStyle={{
                        marginTop: 10,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      name="times"
                      type="font-awesome"
                      size={16}
                    />
                  )}
                  {this.state.descriptionHover == true &&
                  Platform.OS == "web" ? (
                    <Tooltip
                      backgroundColor={"#d3d3d3"}
                      withPointer={false}
                      withOverlay={false}
                      toggleOnPress={true}
                      containerStyle={{
                        left: -60,
                        top: -60
                      }}
                      popover={
                        <Text
                          style={{
                            fontFamily: ConstantFontFamily.defaultFont
                          }}
                        >
                          Description should be 50 to 300 Characters
                        </Text>
                      }
                    />
                  ) : null}
                </TouchableOpacity>
              </View>

              <TextInput
                value={this.state.description}
                multiline={true}
                numberOfLines={5}
                placeholder="What the topic is about. Can paraphrase Wikipedia."
                placeholderTextColor="#6D757F"
                style={[
                  textStyle,
                  this.state.focusDesc ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                  {
                    width: "100%",
                    borderRadius: 5,
                    // borderColor: "#e1e1e1",
                    borderWidth: 0,
                    marginTop: 5,
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    height: Platform.OS == "ios" ? 100 : null,
                    fontFamily: ConstantFontFamily.defaultFont,
                    outline:'none'
                  }
                ]}
                onChangeText={description => this.setState({ description })}
                onFocus = {()=> this.setState({focusDesc: true})}
                onBlur = {()=> this.setState({focusDesc: false})}
              />

              <View
                style={{
                  marginTop: 20,
                  alignSelf: "center"
                }}
              >
                <Button
                  title="Edit Topic"
                  titleStyle={ButtonStyle.wtitleStyle}
                  buttonStyle={ButtonStyle.gbackgroundStyle}
                  containerStyle={ButtonStyle.containerStyle}
                  disabled={
                    this.state.topicName.length < 3 ||
                    this.state.topicName.length > 24 ||
                    this.state.topicName[this.state.topicName.length - 1] ==
                      "-" ||
                    this.state.description.length < 50 ||
                    this.state.description.length > 300 
                    // setBackPic == null
                      ? true
                      : false
                  }
                  onPress={() => this.editTopic()}
                />
              </View>
            </View>
          </View>
        </ScrollView>
        {Dimensions.get("window").width <= 750 &&
          <BottomScreen navigation={NavigationService} />
        }
      </View>
    );
  }
}

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  listTrending_cliks: !state.TrendingCliksReducer.getIn(["Trending_cliks_List"])
    ? List()
    : state.TrendingCliksReducer.getIn(["Trending_cliks_List"]),
  link: state.LinkPostReducer.get("link"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  topicdetails: state.EditTopicReducer.get("topic")
});

const mapDispatchToProps = dispatch => ({
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload))
});

const EditTopicScreenContainerWrapper = compose(
  graphql(UserLoginMutation, {
    name: "Login",
    options: { fetchPolicy: "no-cache" }
  })
)(EditTopicScreen);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  EditTopicScreenContainerWrapper
);

const styles = StyleSheet.create({
  usertext: {
    color: "#000",
    fontSize: 14,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  }
});
