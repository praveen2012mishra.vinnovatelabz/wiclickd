import { List } from "immutable";
import React, { Component } from "react";
import { graphql } from "react-apollo";
import {
  Animated,
  Dimensions,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Button, Icon, Tooltip } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";
import { getFeedProfileDetails } from "../actionCreator/FeedProfileAction";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { getTrendingExternalFeeds } from "../actionCreator/TrendingExternalFeedsAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import NewHeader from "../components/NewHeader";
import ProgressBar from "../components/ProgressBar";
import TataStructure from "../components/TataStructure";
import AppHelper from "../constants/AppHelper";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import {
  AddExternalFeedMutation,
  ExternalFeedTestMutation
} from "../graphqlSchema/graphqlMutation/FeedMutation";
import {
  SearchTopicMutation,
  SearchUserMutation
} from "../graphqlSchema/graphqlMutation/SearchMutation";
import {
  GetChildTopicsMutation,
  TopicQuery
} from "../graphqlSchema/graphqlMutation/TrendingMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  AddExternalFeedVariables,
  ExternalFeedVariables
} from "../graphqlSchema/graphqlVariables/PostVariables";
import {
  SearchTopicVariables,
  SearchUserVariables
} from "../graphqlSchema/graphqlVariables/SearchVariables";
import { uploadBannerImageAsync } from "../services/UserService";
import ButtonStyle from "../constants/ButtonStyle";
import HeaderRight from "../components/HeaderRight";

class AddFeedScreen extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.Pagescrollview = null;
    this.state = {
      value: 100,
      showSlider: false,
      textStyle: "WHITE",
      uploading: false,
      profilePic: "",
      title: "",
      text: "",
      items: [],
      selectText: "Cliks Name",
      uploadMutipleImagePost: [],
      changeBackPicEnable: null,
      setBackPic: null,
      gradientColor: "rgba(0,0,0,0.9)",
      brightnessvalue: 0.5,
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      conditionIcon: false,
      focusWeburl: false,
      focusTopic:false,
      focusName: false,
      focusIconurl: false,
      focusFeedUrl: false,
      summary: "",
      RoleItems: [
        {
          label: "Member",
          value: "Member",
          key: 0
        },
        {
          label: "Admin",
          value: "Admin",
          key: 1
        },
        {
          label: "Super Admin",
          value: "Super Admin",
          key: 2
        }
      ],
      SelectRoleItems: "Member",
      topicName: "",
      rtopicName: "",
      description: "",
      MutipleUser: [],
      MutipleUserList: [],
      showError: false,
      getImage: "",
      showcliktooltip: false,
      showaddmembertooltip: false,
      MutipleQualification: [],
      bannerHover: false,
      clikHover: false,
      descriptionHover: false,
      joinclikHover: false,
      qualification: "",
      modalVisible: false,
      TopicList: [],
      TopicListHierarchy: [],
      UserList: [],
      showDeleteIcon: false,
      inputParentName: false,
      rtopicParents: "#000",
      rtopicselected: "",
      parentsOfSelectedTopic: [],
      showContent: false,
      ErrorMessage: "",
      feedurl: "",
      base_topic: "",
      icon_url: "",
      id: "",
      name: "",
      website: "",
      loading: false,
      textColor: "#000",
      UserMessage: ""
    };
    this.changeBannerImage = "";
    this.baseState = this.state;
  }

  componentDidMount = async () => {
    this.props.searchOpenBarStatus(false)
    this._isMounted = true;
    const profileData = this.props.profileData;
    let userProfilePic = profileData
      .getIn(["my_users", "0", "user"])
      .getIn(["profile_pic"]);
    this.setState({
      profilePic: {
        uri: userProfilePic
      }
    });
    let joined = [];
    this.props.listTrending_cliks.map(async (value, index) => {
      await joined.push({
        id: value.node.id,
        name: value.node.name
      });
    });
    this.setState({ items: joined });
  };

  componentDidUpdate() {
    if (this.props.getHasScrollTop == true && this.Pagescrollview) {
      this.Pagescrollview.scrollTo({ x: 0, y: 0, animated: true });
      this.props.setHASSCROLLEDACTION(false);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
    this.setState(this.baseState);
  }

  createFeed = async () => {
    let __self = this;
    this.setState({
      loading: true
    });
    AddExternalFeedVariables.variables.name = this.state.name;
    AddExternalFeedVariables.variables.url = this.state.feedurl;
    AddExternalFeedVariables.variables.website = this.state.website;
    AddExternalFeedVariables.variables.base_topic = this.state.rtopicName.replace(
      "/",
      ""
    );
    AddExternalFeedVariables.variables.summary_source = "RSS_DESCRIPTION";
    AddExternalFeedVariables.variables.icon_url = this.state.icon_url;
    try {
      await applloClient
        .query({
          query: AddExternalFeedMutation,
          ...AddExternalFeedVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.goToProfile(res.data.external_feed_create.external_feed.name);
          alert("Posts will populate in a few minutes");
          let resDataLogin = await __self.props.Login();
          await __self.props.saveLoginUser(resDataLogin.data.login);
          if (resDataLogin) {
            await __self.props.getTrendingExternalFeeds({
              currentPage: AppHelper.PAGE_LIMIT
            });
            this.setState({
              loading: false
            });
            this.setState(this.baseState);
          }
        });
    } catch (e) {
      this.setState({
        loading: false
      });
      console.log(e);
    }
  };

  checkRTopicname = async name => {
    var letters = /^[0-9a-zA-Z-]+$/;
    if (name.match(letters) || name == "") {
      if (name[0] == "-") {
        alert("- not allowed at initial of clik name");
        return false;
      }
      this.setState(
        {
          rtopicName: name,
          TopicListHierarchy: [],
          rtopicParents: "#000"
        },
        () => {
          this.customRenderTopicSuggestion(name);
        }
      );
      return true;
    } else {
      alert("Please input alphanumeric characters only");
      return false;
    }
  };

  customRenderTopicSuggestion = value => {
    SearchTopicVariables.variables.prefix = value;
    applloClient
      .query({
        query: SearchTopicMutation,
        ...SearchTopicVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        this.setState({
          TopicList: res.data.search.topics,
          TopicListHierarchy: []
        });
      });
  };

  getTopicProfileDetails = value => {
    applloClient
      .query({
        query: TopicQuery,
        variables: {
          id: "Topic:" + value
        },
        fetchPolicy: "no-cache"
      })
      .then(res => {
        this.setState({
          parentsOfSelectedTopic: res.data.topic.parents
            ? [...res.data.topic.parents, value]
            : [value]
        });
      });
  };
  getChildStructure(arr, selectTopic, childrenOfSelectTopic) {
    let output = [];
    let i = 0;
    if (arr.length > 0)
      output[i] = {
        name: arr[i],
        children:
          selectTopic === arr[i]
            ? [...childrenOfSelectTopic]
            : this.getChildStructure(
              arr.slice(i + 1),
              selectTopic,
              childrenOfSelectTopic
            )
      };

    return output;
  }
  renderTopicListHierarchy = value => {
    this.setState({
      TopicListHierarchy: []
    });
    if (value) {
      applloClient
        .query({
          query: GetChildTopicsMutation,
          variables: {
            id: "Topic:" + value
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          let { children } = res.data.topic;
          this.setState({
            TopicListHierarchy: this.getChildStructure(
              this.state.parentsOfSelectedTopic,
              value,
              [{ name: "dynamicText" }, ...children]
            )
          });
        });
    }
  };

  handleTopicSelectInput = (topic, parents, item) => {
    this.setState(
      {
        textColor: "#009B1A",
        rtopicName: "/" + topic,
        TopicList: [],
        TopicListHierarchy: []
      },
      () => {
        if (parents) {
          this.colorCondition(parents);
        } else {
          this.colorCondition("");
        }
        if (topic != "") {
          if (item.banner_pic) {
            let v = item.banner_pic.split("/");
            let k = v[v.length - 1].substring(0, v[v.length - 1].indexOf("."));
            this.setState({
              setBackPic: item.banner_pic,
              showDeleteIcon: true,
              changeBackPicEnable: k
            });
          }

          this.getTopicProfileDetails(topic);
          this.renderTopicListHierarchy(topic);
        }
        if (this.state.rtopicName == "") {
          this.setState({
            rtopicParents: "",
            TopicList: []
          });
        }
      }
    );
  };

  colorCondition = parent => {
    if (parent.length > 0) {
      this.setState({
        rtopicParents: "#009B1A"
      });
    } else if (parent == "") {
      this.setState({
        rtopicParents: "#009B1A"
      });
    } else if (parent == null) {
      this.setState({
        rtopicParents: "#009B1A"
      });
    }
  };

  getTestExternalFeed = url => {
    ExternalFeedVariables.variables.url = url;
    applloClient
      .query({
        query: ExternalFeedTestMutation,
        ...ExternalFeedVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        if (res.data.external_feed_test.status.custom_status == "NEW_FEED") {
          this.setState({
            rtopicName:
              res.data.external_feed_test.external_feed.base_topic != null
                ? res.data.external_feed_test.external_feed.base_topic
                : "",
            icon_url:
              res.data.external_feed_test.external_feed.icon_url != null
                ? res.data.external_feed_test.external_feed.icon_url
                : "",
            id: res.data.external_feed_test.external_feed.id,
            name: res.data.external_feed_test.external_feed.name,
            website: res.data.external_feed_test.external_feed.website,
            showContent: true,
            ErrorMessage: "NEW"
          });
        } else if (
          res.data.external_feed_test.status.custom_status == "DUPLICATE_FEED"
        ) {
          this.setState({
            rtopicName:
              res.data.external_feed_test.external_feed.base_topic != null
                ? res.data.external_feed_test.external_feed.base_topic
                : "",
            icon_url:
              res.data.external_feed_test.external_feed.icon_url != null
                ? res.data.external_feed_test.external_feed.icon_url
                : "",
            id: res.data.external_feed_test.external_feed.id,
            name: res.data.external_feed_test.external_feed.name,
            website: res.data.external_feed_test.external_feed.website,
            showContent: false,
            ErrorMessage: "DUPLICATE"
          });
        } else {
          this.setState({
            rtopicName: "",
            icon_url: "",
            id: "",
            name: "",
            website: "",
            showContent: false,
            ErrorMessage: "INVALID",
            UserMessage: res.data.external_feed_test.status.user_msg
          });
        }
      });
  };

  goToProfile = async id => {
    this.props.setFeedDetails({
      id: id,
      type: "feed"
    });
  };

  render() {
    const textStyle = styles.usertext;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: ConstantColors.customeBackgroundColor,
          width: "100%",
          // paddingTop: Platform.OS == "web" ? 10 : 5,
          paddingHorizontal: Dimensions.get("window").width <= 750 ? 0 : 10
        }}
      >
        <ScrollView
          ref={scrollview => {
            this.Pagescrollview = scrollview;
          }}
          showsVerticalScrollIndicator={false}
          onLayout={event => {
            let { x, y, width, height } = event.nativeEvent.layout;
            if (width < 1024) {
              this.setState({
                showsVerticalScrollIndicatorView: true,
                currentScreentWidth: width
              });
            } else {
              this.setState({
                showsVerticalScrollIndicatorView: false,
                currentScreentWidth: width
              });
            }
          }}
          style={[
            // ButtonStyle.borderStyle,
            {
              backgroundColor: "#fff",
              marginTop: Dimensions.get("window").width <= 750 ? 0 : 10,
              marginBottom: 30,
              // paddingHorizontal: 10,
              height:
                Platform.OS !== "web"
                  ? null
                  : Dimensions.get("window").height - 80
            }
          ]}
        >
          <View>
            {Dimensions.get("window").width <= 750 && (
              <Animated.View
                style={{
                  position: Platform.OS == "web" ? "sticky" : null,
                  top: 0,
                  left: 0,
                  right: 0,
                  zIndex: 10,
                  overflow: "hidden",
                  // borderRadius: 20
                }}
              >
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      flexDirection: "row",
                      backgroundColor: "#000",
                      // borderRadius: 20,
                      height: 50
                    }}
                  >
                    <TouchableOpacity
                      style={ButtonStyle.headerBackStyle}
                      onPress={() => {
                        let nav = this.props.navigation.dangerouslyGetParent()
                          .state;
                        if (nav.routes.length > 1) {
                          this.props.navigation.goBack();
                          return;
                        } else {
                          this.props.navigation.navigate("home");
                        }
                      }}
                    >
                      <Icon
                        color={"#fff"}
                        name="angle-left"
                        type="font-awesome"
                        size={40}
                      />
                    </TouchableOpacity>
                    {!this.props.getsearchBarStatus &&
                      <TouchableOpacity
                        style={[ButtonStyle.headerTitleStyle, { backgroundColor: "#000" }]}
                      >
                        <Text
                          style={{
                            color: "white",
                            textAlign: "center",
                            fontWeight: "bold",
                            fontSize: 18,
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          Add Feed
                      </Text>
                      </TouchableOpacity>
                    }
                    <View
                      style={ButtonStyle.headerRightStyle}
                    >
                      <HeaderRight navigation={this.props.navigation} />
                    </View>
                  </View>
                </View>
              </Animated.View>
            )}
            <ProgressBar
              progress={this.state.loading}
              value={this.state.value}
            />
            <View style={{paddingLeft:5}}>
            <View style={{ flexDirection: "row", width: "100%", marginHorizontal: Dimensions.get('window').width <= 1100 ? 5 : 0 }}>
              <View
                style={{
                  width: "80%",
                  justifyContent: "flex-start",
                  flexDirection: "row"
                }}
              >
                <Text
                  style={{
                    fontWeight: "bold",
                    marginTop: 10,
                    marginRight: 20,
                    fontSize: 16,
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  RSS Feed URL
                </Text>
              </View>
            </View>

            <View style={{ flexDirection: "row", width: "100%", marginHorizontal: Dimensions.get('window').width <= 750 && 5 }}>
              <TextInput
                value={this.state.feedurl}
                placeholder="Copy RSS or Atom Feed URL"
                placeholderTextColor="#6D757F"
                style={[
                  textStyle,
                  this.state.focusFeedUrl ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                  {
                    width: Dimensions.get('window').width<=750 ?'97%' :"99%",
                    borderRadius: 5,
                    // borderColor: "#e1e1e1",
                    // borderWidth: 1,
                    marginTop: 5,
                    height: 45,
                    padding: 5,
                    fontFamily: ConstantFontFamily.defaultFont,
                    outline:'none',
                    marginLeft: Dimensions.get('window').width<=750 ?  0 : 2
                  }
                ]}
                onChangeText={feedurl =>
                  this.setState({
                    feedurl: feedurl
                  })
                }
                editable={!this.state.showContent}
                onFocus = {()=> this.setState({focusFeedUrl: true})}
                onBlur = {()=> this.setState({focusFeedUrl: false})}
              />
            </View>
            {this.state.ErrorMessage == "INVALID" && (
              <Text
                style={{
                  marginTop: 5,
                  fontSize: 14,
                  fontFamily: ConstantFontFamily.defaultFont,
                  color: "#eb3225"
                }}
              >
                {this.state.UserMessage}
              </Text>
            )}
            {this.state.ErrorMessage == "DUPLICATE" && (
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    marginTop: 5,
                    fontSize: 14,
                    fontFamily: ConstantFontFamily.defaultFont
                  }}
                >
                  This feed already exists.
                </Text>
                <Image
                  source={{
                    uri: this.state.icon_url
                  }}
                  style={{
                    width: 30,
                    height: 30,
                    alignSelf: "center",
                    marginLeft: 20
                  }}
                />
                <Text
                  style={{
                    marginLeft: 10,
                    marginTop: 5,
                    fontSize: 14,
                    fontFamily: ConstantFontFamily.defaultFont,
                    textDecorationLine: "underline"
                  }}
                  onPress={() => this.goToProfile(this.state.name)}
                >
                  {this.state.name}
                </Text>
              </View>
            )}

            {this.state.showContent == false && (
              <View
                style={{
                  alignSelf: "center",
                  marginTop: 20
                }}
              >
                <Button
                  title="Check Feed"
                  titleStyle={ButtonStyle.wtitleStyle}
                  buttonStyle={ButtonStyle.gbackgroundStyle}
                  containerStyle={ButtonStyle.containerStyle}
                  disabled={this.state.feedurl == "" ? true : false}
                  onPress={() => this.getTestExternalFeed(this.state.feedurl)}
                />
              </View>
            )}

            {this.state.showContent == true && (
              <View>
                <View style={{ flexDirection: "row", width: "100%" }}>
                  <View
                    style={{
                      width: "90%",
                      justifyContent: "flex-start",
                      flexDirection: "row",
                      marginLeft: Dimensions.get('window').width<=750 ? 5 :2,
                    }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: 10,
                        marginRight: 20,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Feed Name
                    </Text>
                  </View>
                </View>

                <View style={{ flexDirection: "row", width: "100%" }}>
                  <TextInput
                    value={this.state.name}
                    placeholderTextColor="#6D757F"
                    style={[
                      textStyle,
                      this.state.focusName ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                      {
                        width: Dimensions.get('window').width<=750 ?'97%' :"99%",
                        borderRadius: 5,
                        // borderColor: "#e1e1e1",
                        // borderWidth: 1,
                        marginTop: 5,
                        marginLeft: Dimensions.get('window').width<=750 ? 5 :2,
                        height: 45,
                        padding: 5,
                        fontFamily: ConstantFontFamily.defaultFont,
                        outline: 'none'
                      }
                    ]}
                    onChangeText={name =>
                      this.setState({
                        name: name
                      })
                    }
                    onFocus = {()=> this.setState({focusName: true})}
                    onBlur = {()=> this.setState({focusName: false})}
                  />
                </View>

                <View style={{ flexDirection: "row", width: "100%" }}>
                  <View
                    style={{
                      width: "90%",
                      justifyContent: "flex-start",
                      flexDirection: "row",
                      marginLeft: Dimensions.get('window').width<=750 ? 5 :2,
                    }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: 10,
                        marginRight: 20,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Website
                    </Text>
                  </View>
                </View>

                <View style={{ flexDirection: "row", width: "100%" }}>
                  <TextInput
                    value={this.state.website}
                    placeholderTextColor="#6D757F"
                    style={[
                      textStyle,
                      this.state.focusWeburl ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                      {
                        width: Dimensions.get('window').width<=750 ?'97%' :"99%",
                        marginTop: 5,
                        marginLeft: Dimensions.get('window').width<=750 ? 5 :2,
                        height: 45,
                        padding: 5,
                        fontFamily: ConstantFontFamily.defaultFont,
                      }
                    ]}
                    onChangeText={website =>
                      this.setState({
                        website: website
                      })
                    }
                    onFocus = {()=> this.setState({focusWeburl: true})}
                    onBlur = {()=> this.setState({focusWeburl: false})}
                  />
                </View>

                <View style={{ flexDirection: "row", width: "100%" }}>
                  <View
                    style={{
                      width: "80%",
                      justifyContent: "flex-start",
                      flexDirection: "row",
                      marginLeft: Dimensions.get('window').width<=750 ? 5 :2,
                    }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: 10,
                        marginRight: 20,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Favicon URL
                    </Text>
                  </View>
                </View>

                <View style={{ flexDirection: "row", width: "99%", justifyContent:'space-between' }}>
                  <TextInput
                    value={this.state.icon_url}
                    placeholderTextColor="#6D757F"
                    style={[
                      textStyle,
                      this.state.focusIconurl ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                      {
                        width: "90%",
                        borderRadius: 5,
                        // borderColor: "#e1e1e1",
                        // borderWidth: 1,
                        marginTop: 5,
                        marginLeft: Dimensions.get('window').width<=750 ? 5 :2,
                        height: 45,
                        padding: 5,
                        fontFamily: ConstantFontFamily.defaultFont,
                        outline:'none'
                      }
                    ]}
                    onChangeText={icon_url =>
                      this.setState({
                        icon_url: icon_url
                      })
                    }
                    onFocus = {()=> this.setState({focusIconurl: true})}
                    onBlur = {()=> this.setState({focusIconurl: false})}
                  />
                  <Image
                    source={{
                      uri:
                        this.state.icon_url == ""
                          ? "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAhFBMVEX/pQD/////ngD/oQD/owD/oAD/+fT/nQD///3/7NP/pgD/4r7//vr/+vH/t1D/vF3/+O7/79r/zo//5MP/xnr/16P/2Kj/9OX/rSv/xXf/uVb/zIn/6c3/267/qRP/05v/s0L/wGn/sDb/wW3/tkv/z5T/yYH/qyD/sDf/37b/x4T/ryzcdsqXAAAM+ElEQVR4nN1daXfqOgx0bKUQCIQd2rKWbtz+//93E6ALEI/kLJAw57xP9yXNYFmSpbGtPA6dYDHYdr+WLVUltJZf3e1gEXTY71fwX8PhaKa1MUR0a0oXiL/JGK0/RsMwM8P2RvumetROQcbXUTsLw85A6aqz+wZpGljN1cKwN9KVH72/IKMfew4Mm8/a3PqbnWH0c1PKsE3145fAUNp8vGQYRrWZf+cgHV361QuGgarnAB5gVMAxHDfqOoAHUGOMGY70rT8xN/QIMYz8W39fAfAjO8OnOk/BX5gnG8PoPgjGFKN0hqN7MNED/FEaw3H9ncwv9PiSYdC49VcVikZwzjBU9Y6D5yAVnjG8Gy/zjR9vc2TYvqdJeIBu/2XYrGCVIi+Imn8YPt+bjSYwz78Me/dnowl074fh4z0OYTyIo2+GnfscwngQO0eGg/scwngQB0eGd+hIDyB1YNi2Ztz6CN83ppqFbwZJTIwZRrYP19NObzrtB/N5e7J6e4zWrYRtQvWqn5kDtEkYhlY/ox+8c3T6w8lzNDN7otf81ozQYcxwaDfSS4ZHNKfD1fbQs7nm97rDH8YMR1ZPamd45NlfvL/4lS7/xyFReTPrB3IM9wiD1ZOpLEuaeQqEexHDPfrjqrLUDyoogmFisvPBrIIkdaAW9oTGiWGC6aSr/WqRNAsFUjZnhjHCXeRXiaQZqK39c7IwTEguXhuVMVfaqm7hDGN0xrOK9Fipq77KYBijP6qEtdJMLe3/mothjMVLBXqtSwWEQHkZxgO5vXkAgUKn/AwT0QpVwVgtKIJhjMmyAsaajoIYet5uVlGOhTH0vHY1ORbI0POGVeRYKMPYVpeV8zkFM4x9jqlGovODwhlWTjBXPEPP61VKU1YGQ8+bz6qjiCiHoeeN/aqYalkMvU5VTLU0hnF0bFViGHWq5rYYNB+rIIKkaLPZbB/f38aL9rzfKZjvfFmBYSTab2qI4Wutzcd6O9gFFsF4BmyrMIwnSOjGVP314yQoZIoOqyooP+xV2U76uSmG3Qo32BOafnd1Iat2xLgiccMGMpqiSa6Z2a+Cw8GIWc4GOYayGVXYUr8Rk2z9y06y6pZ6QExyOcjqeoKaKANiki8LvBfQhs5L5SfjEeT722zWuqnBZDwi9juLLBTf6kMxHkgaZEh52rXwN0fEM3I0dabYr4m/OcLojTPH3rJWFJVpOHMMa+NSjzB665rQvdaMYrI31zFA1m/3nDHnOx4ZbGtHkfyPuRPFxxoFxiNIR07T8b1+FJXxnUy1jhSVP3NZd9SSIumBA8XH2rmbBP6HwzDWz6MmIP0mp1jT3YL+Wu5Ur5PdmMNGi2SnRTE5MRl0Vs4pXq6Qhptde7cYr97+bbqfh60W+YnqRynD8AorjdPu2jRYDDb7nRa5/rJ5kVpqr/z1Ylr/MOzvntc6jx6PjDSL65ceFu0d0v44oswsqbESUix9dzLuAfdX60ZGCZDeCCmWXZ5iu9ydXWQykTSfwmXjptyYIenjh+0oi7lSS1jhKDdmCJUKncmnu9KJjKxy3CnVoTrsCho572IjvRO9GuzpyQ8XtUnorgRuTERvXpVI0VFPM1w7chRm4iUm4c6KoaDrpq7Qz5K3NstL3zJoooKu0zjqd8lLy8ttMqm+gk8XjjKKpR1/lFHX1l46LNBlFLslTcXMyj0XeaVoLoYlRcUce9c2clMVedRhOadY5VFfOujytCQubkux03z60mfxMDYEpY1yQkZOBW3/Q/q7a0GOOi/DTnNrhN+FCQD5gpVGGccE5VdBz4XySmrx68Vmq3g7LUDnLZVX0if/rmHxcb8QJfubzOGYDf+q4lPwYrT6c9nKUfPlqU7hzYyCdiP0ZqLfvsEXGcdFUyxsv8WT5MvI8KVi+zksGRlmExqmQNTypBf2PfOCnY1ZTSaL3bA/zc9UtP4xI/Y9RTub/U6LZKsFzaLnxZy/LcMOkS5Ps+lbeecfHm7LaG3GQdadM3MJRX4qlnyGZcxTm+44m9i5L4gatObe0rxC4zSRrb+7iYAOmAoo+uxicXKVDv+epPtISihq9rXXkmjGJD/Grj5WQJE+uJfsrie2IeM/OipI+7y78Vndzcc1dbam8eQmzReEbNZOr3yqM+lXJ47859GMe0fRuRv7Rbrr4nT47IaV+V3/aG7SW4d055119z4X9689iCqRFknFB57gFg0TMW+4ojv9hf8hn47sEGguobjJtgU6v9/Fjh6nbmCD4nUSmwuYlnQY2ZjBOpsbba+Rq2QHzBgQMenSzQ7o9tdCp8ppKw3TkbrdOflEMktl22WaiRibm22RIi2T5XNtCMOoNEsVoTAQKkjfGTvVTFJ/DYGtDX5XRJFZInBF8MUtte5mJlk4cgqLBh7E5k3V/PQhcalMUYkbxNteykFKInVmci9mJt7S16ikISigyKQ2XIH4qmv9S5ASGCqjj9X4FeMSzfRw8hDehEFL3t0w7TKDS4u9ku5RI6P9WXf7/m9/3wXYmsBXIzxvBSkS4adfSzBT8k3092ihzhAcNW8EcRE7Gx+LbIsPiaSf2pdti+lAWdZ7Pp/d4JILYwYPBTMk+xkCC4tWT5CjfsJBZIQ2T4WaqV6jitoqvdTLS4FwxDBb+HCRZkqc+Kz3mvbXiNiYAU69j+FDh/xQXNA3S758P0hz3ny7DN+d6eOTYArzpuaV5eclip6Uv8e3IeBXMr39SUFBn61fHhGkUWSnIs4vcXI6LcZMTy8Qdv1YanFPrdEgGmwDheSmEsnZN9opk4pVWEC9Gi3hs0W09YlcdENpZULWTmFig58uQmDDVthPkeI3+Ao2GghcVyxgpS9Q8ZwgrZDJtW1CxJAxUxxOBSDlqp9JWy74TNy3X4ymuJ5w7kUiE3HTkDKrmOQLF6WwN827Y4gxkVSkJYtcbx4VP5kFRs4xNLItgqdIueGbmKUi9DW4wJ9zIposqsS0vhATMeBttvhnXuUaxOSSS3ekpVLEZLZopUcwacwXEX35OR1/kdbFZgZxh+IaLNfYLxmVoJFNOptmpoST2xAxxL9OnhaNS0b6F6k3mzJtCOuFtirRKqMnYTBlYES7Hy+R6jeYhiAyUzz+4AJHnmGWWJEgdf8O9ssdtNT30ZN5Yr6fRSebIHXFx/xeqOgGE4ZmDoa8mtWC1EnF5CZvwNgMzB1zaMA4tYAV6a09/IOhYgbOa8EtnCzDrHtm/qUyNP/gQyjmwxVmjqwm8ximqxCYig2KF7Bsar92m2foftrxARa7weUCtNKDMT9HwU2yPTcVlnwfx1c4EZEfzrEFI2NaavVujJkihrCWkt2Z4mwJwDYxsDcFdVPcHMjegsLLFjusWQY+UxjVPg16MN11yyhmY2jNFPFSH6Wm0K3n6F5kTGrsft+gyh1KMKHTa2cPF9kmIvBt8ENR2RR29HPk3hI5xSXAT4onImizwJJiHjltJjMF1S9c+AGqWIKZaQ6GWUpRKMXAERGIt3G4yHOeRIbEDeqToZALrNZx7o0FHRjuIRFPe+hqYN6GvDBK2pm7x52ba0xHF69lkX3DlRySmrbUEjN07VxMGEUl6gaiegT0eWj3xVJ9MTbM9Y1Owa1ksNWndDx+GCLzBmtg+lJsZ0O7NNi4LVdYPQKKu3CdA9I26iq+yuEwFfljxmGqC35tOIFBjkFbJdhBJF4JC06tgKeJgEADs6E5yKIGSlAyJuEoslsnFRNgwTII6oVB/8kslEjyLjk07uFTsoiBPhHUTGEFBHDQgYLNyd//kfWoc9mWQFhHBz4Rtj1AlqEflLDKYZbQUpsj4Ql7cBkEym0waNljVLw6UtLuFOnIPoUW4mtvoU9EiekmE0MzihmKC6oxx1SnGk5a8mPLYV0Q1DEgQ/sC0R/GDB3axKRnqzNPEbY3TgddQ4YoriGGdl+imzFDJjM/+0PJ9birdjDtPfT688n7p+vR+mYVPlixA1YaNe3PWa00+V2U+yEM+6ur9f5GjwznzRttB9S3gefsQ9jeM7zV1u/yQQm75L+bbf0uG/v6lYITtebY10wShjfejlkaDv0cheNJrXFoBewZln3O221wrJgcGDbv0J0SNf8wvMHBRKXj+xDGI8O63uRmx89Gn2+GIahz1RGkwjOGzB6y2qHxswz6YVjeJRK3wJ/dq78MvVEt741Mhf9Hu/GH4f14m5PthH8ZisqBNcDpdsIThjW8FTsF/mlv5JRhPW/iPcX5uXFnDL2x241NlQM1zsv/5wy9QNV5Mhp1UQ68YOiFkesFapUB6eiy8XPJME7DxfXdasFQWpMxjaHXfHa/0PDmMPo5Vc2QytDzOqM818VeH2T0yCLuszCMOQ5UbeYjaRpYhTpWhjHam7zXG18BSX06Qk1+xDD2q8P3ma+5I6FuAzrcZvAxGuLN2Jhhgodg8bbtzpaMtOjKaC2/utvBIuBPFPsPHJSoM14DLo8AAAAASUVORK5CYII="
                          : this.state.icon_url
                    }}
                    style={{
                      width: 30,
                      height: 30,
                      alignSelf: "center",
                      // marginLeft: 20
                    }}
                  />
                </View>
                {this.props.isAdmin == true && (
                  <View style={{ flexDirection: "row", width: "100%" }}>
                    <View
                      style={{
                        width: "90%",
                        justifyContent: "flex-start",
                        flexDirection: "row",
                        marginLeft: Dimensions.get('window').width<=750 ? 5 :2,
                      }}
                    >
                      <Text
                        style={{
                          fontWeight: "bold",
                          marginTop: 10,
                          marginRight: 20,
                          fontSize: 16,
                          fontFamily: ConstantFontFamily.MontserratBoldFont
                        }}
                      >
                        Base Topic
                      </Text>
                    </View>
                    <TouchableOpacity
                      style={{
                        width: "10%",
                        justifyContent: "flex-end",
                        alignItems: "flex-end"
                      }}
                      onMouseEnter={() => this.setState({ clikHover: true })}
                      onMouseLeave={() => this.setState({ clikHover: false })}
                    >
                      {/* {this.state.rtopicName == "" && (
                        <Icon
                          color={"#f80403"}
                          iconStyle={{
                            marginTop: 10,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                          name="times"
                          type="font-awesome"
                          size={16}
                        />
                      )} */}
                      {this.state.clikHover == true && Platform.OS == "web" ? (
                        <Tooltip
                          backgroundColor={"#d3d3d3"}
                          withPointer={false}
                          withOverlay={false}
                          toggleOnPress={true}
                          containerStyle={{
                            left: -60,
                            top: -60
                          }}
                          popover={
                            <Text
                              style={{
                                fontFamily: ConstantFontFamily.defaultFont
                              }}
                            >
                              Base Topic is Mandatory!
                            </Text>
                          }
                        />
                      ) : null}
                    </TouchableOpacity>
                  </View>
                )}
                {this.props.isAdmin == true && (
                  <View style={{ flexDirection: "row", width: "100%" }}>
                    <View
                      style={{
                        width: "100%",
                        // marginVertical: 5,
                        justifyContent: "center",
                        // alignItems: "center"
                      }}
                    >
                      <TextInput
                        value={this.state.rtopicName}
                        placeholder="Type and select an existing topic"
                        placeholderTextColor="#6D757F"
                        onChangeText={rtopicName => {
                          this.checkRTopicname(rtopicName);
                        }}
                        style={[
                          this.state.focusTopic ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                          {
                          color: this.state.textColor,
                          fontSize: 14,
                          fontWeight: "bold",
                          width: Dimensions.get('window').width<=750 ?'97%' :"99%",
                          height: 45,
                          padding: 5,
                          fontFamily: ConstantFontFamily.defaultFont,
                        }]}
                        onFocus={e => {
                          this.setState({
                            inputParentName: true,
                            focusTopic: true
                          });
                        }}
                        onBlur = {()=> this.setState({focusTopic: false})}
                      />
                      {/* <View style={styles.container}>
                        <View style={styles.textContainer}>
                          <TextInput
                            style={styles.paragraph}
                            value={"Hello world"}
                          />
                        </View>
                      </View> */}
                      {this.state.inputParentName == true &&
                        this.state.TopicList.map((item, index) => {
                          return (
                            <View
                              key={item.name}
                              style={{
                                backgroundColor: "#FEFEFA",
                                width: "100%",
                                padding: 5
                              }}
                            >
                              <View
                                style={{
                                  padding: 5,
                                  backgroundColor: item.parents
                                    ? "#e3f9d5"
                                    : "#e3f9d5",
                                  borderRadius: 5,
                                  alignSelf: "flex-start",
                                  alignItems: "center"
                                }}
                              >
                                <Text
                                  style={{
                                    color: item.parents ? "#009B1A" : "#009B1A",
                                    fontFamily:
                                      ConstantFontFamily.MontserratBoldFont,
                                    fontWeight: "bold"
                                  }}
                                  onPress={() =>
                                    this.handleTopicSelectInput(
                                      item.name.toLowerCase(),
                                      item.parents,
                                      item
                                    )
                                  }
                                >
                                  /{item.name.toLowerCase()}
                                </Text>
                              </View>
                            </View>
                          );
                        })}
                    </View>
                  </View>
                )}
                {this.state.TopicListHierarchy.length > 0 && (
                  <View>
                    <View style={{ flexDirection: "row", width: "100%" }}>
                      <Text
                        style={{
                          fontWeight: "bold",
                          marginVertical: 10,
                          marginRight: 20,
                          fontSize: 16,
                          fontFamily: ConstantFontFamily.MontserratBoldFont
                        }}
                      >
                        Topic Hierarchy
                      </Text>
                    </View>
                    <View
                      style={{
                        // borderWidth: 1,
                        // borderColor: "#e1e1e1",
                        borderRadius: 5,
                        paddingHorizontal: 10,
                        paddingBottom: 10
                      }}
                    >
                      <TataStructure
                        isRoot={true}
                        item={this.state.TopicListHierarchy[0]}
                        topicName={this.state.topicName}
                      />
                    </View>
                  </View>
                )}

                <View
                  style={{
                    marginTop: 20,
                    alignSelf: "center"
                  }}
                >
                  <Button
                    title={
                      this.state.loading == false
                        ? "Add Feed"
                        : "Getting posts from feed..."
                    }
                    titleStyle={ButtonStyle.wtitleStyle}
                    buttonStyle={ButtonStyle.gbackgroundStyle}
                    containerStyle={ButtonStyle.containerStyle}
                    disabled={
                      this.state.loading == true ||
                        // this.state.rtopicName == "" ||
                        this.state.name == "" ||
                        this.state.website == "" ||
                        this.state.feedurl == ""
                        ? true
                        : false
                    }
                    onPress={() => this.createFeed()}
                  />
                </View>
              </View>
            )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  listTrending_cliks: !state.TrendingCliksReducer.getIn(["Trending_cliks_List"])
    ? List()
    : state.TrendingCliksReducer.getIn(["Trending_cliks_List"]),
  link: state.LinkPostReducer.get("link"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  getsearchBarStatus: state.AdminReducer.get("searchBarOpenStatus"),
});

const mapDispatchToProps = dispatch => ({
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  setFeedDetails: payload => dispatch(getFeedProfileDetails(payload)),
  getTrendingExternalFeeds: payload =>
    dispatch(getTrendingExternalFeeds(payload)),
  searchOpenBarStatus: payload => dispatch({ type: "SEARCHBAR_STATUS", payload })
});

const AddFeedScreenWrapper = compose(
  graphql(UserLoginMutation, {
    name: "Login",
    options: { fetchPolicy: "no-cache" }
  })
)(AddFeedScreen);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  AddFeedScreenWrapper
);

const styles = StyleSheet.create({
  usertext: {
    color: "#000",
    fontSize: 14,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  }
});
