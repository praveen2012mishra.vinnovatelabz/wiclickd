import { List } from "immutable";
import React, { Component, lazy, Suspense } from "react";
import {
  Animated,
  Dimensions,
  FlatList,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Clipboard
} from "react-native";
import { Icon } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger
} from "react-native-popup-menu";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { TabBar, TabView } from "react-native-tab-view";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { listClikMembers } from "../actionCreator/ClikMembersAction";
import { listClikUserRequest } from "../actionCreator/ClikUserRequestAction";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import ClikMembersList from "../components/ClikMembersList";
import ClikProfileUserList from "../components/ClikProfileUserList";
import ClikStar from "../components/ClikStar";
import DiscussionHomeFeed from "../components/DiscussionHomeFeed";
import InviteUserModal from "../components/InviteUserModal";
import NewHomeFeed from "../components/NewHomeFeed";
import SEOMetaData from "../components/SEOMetaData";
import ShadowSkeleton from "../components/ShadowSkeleton";
import TrendingHomeFeed from "../components/TrendingHomeFeed";
import AppHelper from "../constants/AppHelper";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import { retry } from "../library/Helper";
import CommentDetailScreen from "./CommentDetailScreen";
import HeaderRight from "../components/HeaderRight";
import CliksProfileFeedScreen from "./CliksProfileFeedScreen";
import ButtonStyle from "../constants/ButtonStyle";
import { Button } from "react-native-elements";
import QualificationToJoin from "../components/QualificationToJoin";
import ApplyToJoin from "../components/ApplyToJoin";
import JoinWithInviteKey from "../components/JoinWithInviteKey";
import InviteViaLink from "../components/InviteViaLink";
import InviteLinkPrivate from "../components/InviteLinkPrivate";
import applloClient from "../client";
import {
  ClikJoinMutation,
  InviteKeyClikProfileMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { ClikJoinVariables } from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import Modal from "modal-enhanced-react-native-web";
import Overlay from "react-native-modal-overlay";
import InviteKeyJoinModal from "../components/InviteKeyJoinModal";
import { Alert } from "react-native-web";
import { setUSERNAMEMODALACTION } from "../actionCreator/UsernameModalAction";
import BottomScreenProfile from '../components/BottomScreenProfile';
import NavigationService from "../library/NavigationService";

const CliksProfileCard = lazy(() =>
  retry(() => import("../components/CliksProfileCard"))
);

class CliksProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.state = {
      modalVisible: false,
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      memberValue: "",
      items: [
        {
          label: "Profile",
          value: "FEED",
          key: 0
        },
        {
          label: "Members",
          value: "USERS",
          key: 1
        },
        {
          label: "Applications",
          value: "APPLICATIONS",
          key: 2
        }
      ],
      itemsDefult: [
        {
          label: "Feed",
          value: "FEED",
          key: 0
        },
        {
          label: "Profile",
          value: "WIKI",
          key: 1
        }
      ],
      cliksselectItem: this.props.navigation
        .getParam("type", "NO-ID")
        .toUpperCase(),
      listUserRequest: [],
      showIcon: "#fff",
      routes: [
        { key: "first", title: "Feeds" },
        { key: "second", title: "Members" },
        { key: "third", title: "Applications" }
      ],
      index: 0,
      wikiVisible: false,
      id: "",
      ViewMode: "Default",
      scrollY: 0,
      ProfileHeight: 0,
      feedY: 0,
      showInviteLink: false,
      showCopiedText: false,
      qualification: "",
      enteredQualification: false,
      type: "",
      member: "",
      qualification: "",
      invite_key: "",
      status: "Default",
      member_type: "",
      user_msg: "",
      InviteClik: {},
      showInviteKeyJoinModal: false,
      InviteType: "",
      MemberType: ""
    };
    this.userPermision = false;
    this.userApprovePermision = false;
  }

  _renderLazyPlaceholder = ({ route }) => <ShadowSkeleton />;

  _handleIndexChange = index => this.setState({ index });
  handleIndexForMember = index => this.setState({ index });

  enterQualification = text => {
    this.setState({ qualification: text });
  };
  _renderTabBar = props => (
    Dimensions.get("window").width >= 750 &&
    <View>
      <View
        style={[ButtonStyle.profileShadowStyle, {
          flexDirection: "row",
          width: Dimensions.get("window").width >= 750 ? "98%" : "100%",
          backgroundColor: "#fff",
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          marginBottom: 10,
          // borderWidth: 1,
          borderBottomColor: "#D7D7D7",
          borderTopColor: "transparent",
          borderLeftColor: "#D7D7D7",
          borderRightColor: "#D7D7D7",
          paddingHorizontal: 10,
          height: 30,
          marginHorizontal: Dimensions.get("window").width <= 750 ? 0 : 10,
          marginTop: Dimensions.get("window").width <= 750 ? 0 :
            Dimensions.get("window").width >= 1100 ? 0 : 10,
          marginTop: 0,
        }]}
      >
        <TabBar
          {...props}
          indicatorStyle={{
            backgroundColor: "transparent",
            height: 3,
            borderRadius: 6
          }}
          style={{
            backgroundColor: "transparent",
            width: "100%",
            shadowColor: "transparent",
            height: 30,
            justifyContent: 'center'
          }}
          labelStyle={{
            color: "#4169e1",
            fontFamily: ConstantFontFamily.MontserratBoldFont
          }}
          renderLabel={({ route, focused, color }) => (
            <Text
              style={{
                color: focused ? "#4169e1" : "#808080",
                fontFamily: ConstantFontFamily.MontserratBoldFont
              }}
            >
              {route.title}
            </Text>
          )}
        />

        {/* <TouchableOpacity
          style={{
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Menu>
            <MenuTrigger>
              <Icon
                type="material"
                name={this.getIcon()}
                containerStyle={{
                  alignSelf: "center",
                  justifyContent: "center"
                }}
              />
            </MenuTrigger>
            <MenuOptions
              optionsContainerStyle={{
                borderRadius: 6,
                borderWidth: 1,
                borderColor: "#e1e1e1",
                shadowColor: "transparent"
              }}
              customStyles={{
                optionsContainer: {
                  minHeight: 100,
                  marginTop: hp("3%"),
                  marginLeft: wp("-0.5%")
                }
              }}
            >
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Card"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Default View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Default"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Card View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Text"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Text View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Compact"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Compact View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
            </MenuOptions>
          </Menu>
        </TouchableOpacity> */}
      </View>
    </View>
  );

  getIcon = () => {
    switch (this.state.ViewMode) {
      case "Card":
        return "dashboard";
      case "Text":
        return "line-weight";
      case "Compact":
        return "reorder";
      default:
        return "list";
    }
  };

  _renderUserItem = item => {
    return (
      <ClikProfileUserList
        item={item}
        navigation={this.props.navigation}
        ClikInfo={this.props.cliksDetails}
      />
    );
  };

  _renderClikMembersItem = item => {
    return (
      <ClikMembersList
        item={item}
        navigation={this.props.navigation}
        ClikInfo={this.props.cliksDetails}
        userApprovePermision={this.userApprovePermision}
      />
    );
  };

  showIcon = data => {
    this.setState({
      showIcon: data
    });
  };

  members = () => {
    this.setState({
      cliksselectItem: "FEED"
    });
  };

  showMembers = type => {
    this.setState({
      index: 1,
      InviteType: type
    });
  };

  showInviteLink = (value, type, member) => {
    this.setState({
      showInviteLink: value,
      type: type,
      member: member
    });
  };

  onClose = () => {
    this.setState({
      modalVisible: false,
      showInviteKeyJoinModal: false
    });
  };

  requestInvite = async () => {
    ClikJoinVariables.variables.clik_id = this.props.navigation.getParam(
      "id",
      "NO-ID"
    );
    ClikJoinVariables.variables.qualification = "";
    ClikJoinVariables.variables.known_members = [];
    ClikJoinVariables.variables.invite_key = this.state.invite_key;
    try {
      await applloClient
        .query({
          query: ClikJoinMutation,
          ...ClikJoinVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          if (res.data.clik_join.status.custom_status == "JOINED") {
            let member_type = res.data.clik_join.member_type;
            this.setState({
              status: "Success",
              member_type: member_type
            });
          } else if (res.data.clik_join.status.custom_status == "PENDING") {
            this.setState({
              status: "Pending"
            });
          } else {
            let user_msg = res.data.clik_join.status.user_msg;
            this.setState({
              status: "Failure",
              user_msg: user_msg
            });
          }
        });
    } catch (e) {
      console.log(e);
    }
  };

  requestJoin = async () => {
    this.setState({
      index: 1
    });
    if (
      this.props.cliksDetails.getIn(["data", "clik"]).get("invite_only") == true
    ) {
      this.getKeyProfile(this.props.navigation.getParam("key", ""));
    }
  };

  getKeyProfile = async key => {
    try {
      await applloClient
        .query({
          query: InviteKeyClikProfileMutation,
          variables: {
            invite_key: key
          },
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState({
            InviteClik: res.data.clik_invite_key,
            invite_key: key,
            showInviteKeyJoinModal: true
          });
        });
    } catch (e) {
      console.log(e);
    }
  };

  async componentDidMount() {
    this.props.searchOpenBarStatus(false);
    this.getStaticData();
    this.props.SetProfileTabView("FEED");
  }

  setSignupData = async itemId => {
    await this.props.setUsernameModalStatus(true);
    await this.props.setInviteUserDetail({
      clikName: itemId,
      inviteKey: this.props.navigation.getParam("key", ""),
      userName: ""
    });
  };

  async getStaticData() {
    let itemId = await this.props.navigation.getParam("id", "NO-ID");
    this.setState({
      id: itemId
    });
    if (
      this.props.navigation.getParam("type", "NO-ID") == "join" ||
      this.props.navigation.getParam("type", "NO-ID") == "invite"
    ) {
      this.props.loginStatus == 1
        ? this.requestJoin()
        : this.setSignupData(itemId);
    }
    if (itemId) {
      await this.props.userId({
        id: itemId,
        type: this.props.navigation.getParam("type", "NO-ID")
      });
      await this.props.setClikUserRequest({
        id: itemId,
        currentPage: AppHelper.PAGE_LIMIT
      });
      await this.props.setClikMembers({
        id: itemId
      });

      const index = this.props.getUserFollowCliksList.findIndex(
        i =>
          i
            .getIn(["clik", "name"])
            .toLowerCase()
            .replace("clik:", "") ==
          itemId
            .replace("%3A", ":")
            .toLowerCase()
            .replace("clik:", "")
      );
      if (index != -1) {
        if (
          this.props.getUserFollowCliksList.getIn([index, "member_type"]) ==
          "SUPER_ADMIN"
        ) {
          this.setState({
            showIcon: require("../assets/image/YBadge.png"),
            MemberType: "SUPER_ADMIN"
          });
        } else if (
          this.props.getUserFollowCliksList.getIn([index, "member_type"]) ==
          "ADMIN"
        ) {
          this.setState({
            showIcon: require("../assets/image/SBadge.png"),
            MemberType: "ADMIN"
          });
        } else if (
          this.props.getUserFollowCliksList.getIn([index, "member_type"]) ==
          "MEMBER"
        ) {
          this.setState({
            showIcon: require("../assets/image/badge.png"),
            MemberType: "MEMBER"
          });
        } else {
          this.getIconColour(index);
        }
      } else {
        this.getIconColour(index);
      }
    }
  }

  getIconColour = index => {
    if (index != -1) {
      if (
        this.props.getUserFollowCliksList.getIn([
          index,
          "settings",
          "follow_type"
        ]) == "FAVORITE"
      ) {
        this.setState({
          showIcon: "#FADB4A"
        });
      }
      if (
        this.props.getUserFollowCliksList.getIn([
          index,
          "settings",
          "follow_type"
        ]) == "FOLLOW"
      ) {
        this.setState({
          showIcon: "#E1E1E1"
        });
      }
    } else {
      this.setState({
        showIcon: "#fff"
      });
    }
  };

  componentDidUpdate(prevProps) {
    if (this.props.getHasScrollTop == true && this.UserProfilescrollview) {
      this.UserProfilescrollview.scrollTo({ x: 0, y: 0, animated: true });
      this.props.setHASSCROLLEDACTION(false);
    }
    if (
      prevProps.navigation.getParam("id", "NO-ID").toUpperCase() !=
      this.props.navigation.getParam("id", "NO-ID").toUpperCase()
    ) {
      this.getStaticData();
      this.setState({
        scrollY: 0
      });
      this.CliksProfilescrollview.scrollTo({ x: 0, y: 0, animated: true });
    }
    if (this.props.loginStatus == 1) {
      this.getUserPermision();
      this.getUserApprovePermision();
    }
  }

  getUserPermision = () => {
    const index = this.props.listClikMembers.findIndex(
      i =>
        i.node.user.id ==
        this.props.profileData.getIn(["my_users", "0", "user", "id"])
    );
    if (index != -1) {
      this.userPermision = true;
    } else {
      this.userPermision = false;
    }
  };

  getUserApprovePermision = () => {
    const index = this.props.listClikMembers.findIndex(
      i =>
        i.node.user.id ==
        this.props.profileData.getIn(["my_users", "0", "user", "id"]) &&
        (i.node.type == "SUPER_ADMIN" || i.node.type == "ADMIN")
    );
    if (index != -1) {
      this.userApprovePermision = true;
    } else {
      this.userApprovePermision = false;
    }
  };

  loginHandle = () => {
    this.props.setLoginModalStatus(true);
  };

  listScroll = value => {
    this.setState({
      feedY: value
    });
  };

  writeToClipboard = async () => {
    await Clipboard.setString(
      "https://www.weclikd.com/clik/" +
      this.props.cliksDetails.getIn(["data", "clik"]).get("name") +
      "/join"
    );
    this.setState({
      showCopiedText: true
    });
    let timer = setTimeout(() => {
      this.setState({
        showCopiedText: false
      });
    }, 2000);
  };

  _renderScene = ({ route }) => {
    switch (route.key) {
      case "third":
        return (
          <View>
            <View
              style={{
                borderWidth: 1,
                borderColor: "#C5C5C5",
                borderRadius: 10,
                backgroundColor: "#fff",
                marginBottom: 10
              }}
            >
              <Text
                style={{
                  textAlign: "left",
                  color: "#000",
                  fontSize: 16,
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  marginVertical: 10,
                  marginHorizontal: 10
                }}
              >
                Qualifications
              </Text>
              {this.props.cliksDetails
                .getIn(["data", "clik"])
                .get("qualifications") == null && (
                  <Text
                    style={{
                      textAlign: "center",
                      color: "#000",
                      fontFamily: ConstantFontFamily.defaultFont,
                      fontSize: 16,
                      fontWeight: "bold",
                      margin: 10
                    }}
                  >
                    No prerequisites to join this clik
                  </Text>
                )}
              {this.props.cliksDetails
                .getIn(["data", "clik"])
                .get("qualifications") != null &&
                this.props.cliksDetails
                  .getIn(["data", "clik"])
                  .get("qualifications")
                  .map((item, index) => {
                    return (
                      <Text
                        key={index}
                        style={{
                          color: "#000",
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 16,
                          fontWeight: "bold",
                          marginBottom: 20,
                          marginLeft: 30
                        }}
                      >
                        {"\u25CF"} {item}
                      </Text>
                    );
                  })}
            </View>
            {this.userApprovePermision == true && (
              <InviteUserModal
                onClose={() => this.onClose()}
                {...this.props}
                ClikInfo={this.props.cliksDetails.getIn(["data", "clik"])}
                showHeader={false}
              />
            )}
            <View
              style={{
                borderWidth: 1,
                borderColor: "#C5C5C5",
                borderRadius: 10,
                backgroundColor: "#fff",
                marginBottom: 10
              }}
            >
              <Text
                style={{
                  textAlign: "left",
                  color: "#000",
                  fontSize: 16,
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  marginVertical: 10,
                  marginHorizontal: 10
                }}
              >
                Member Applications
              </Text>
              {this.props.listClikUserRequest.length > 0 &&
                this.userApprovePermision == true ? (
                <View>
                  <FlatList
                    extraData={this.state}
                    data={this.props.listClikUserRequest}
                    keyExtractor={item => item.node.user.id}
                    renderItem={this._renderUserItem}
                  />
                </View>
              ) : (
                this.userApprovePermision == true && (
                  <View
                    style={{
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                      height: hp("15%")
                    }}
                  >
                    <Icon
                      color={"#000"}
                      iconStyle={{
                        color: "#fff",
                        justifyContent: "center",
                        alignItems: "center",
                        alignSelf: "center"
                      }}
                      reverse
                      name="user-times"
                      type="font-awesome"
                      size={20}
                      containerStyle={{
                        alignSelf: "center"
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.defaultFont,
                        color: "#000",
                        alignSelf: "center"
                      }}
                    >
                      No Applications
                    </Text>
                  </View>
                )
              )}
            </View>
          </View>
        );
      case "second":
        return (
          <ScrollView containerStyle={{ marginBottom: 50, flex: 1, width: '100%' }}>
            {this.props.cliksDetails.getIn(["data", "clik"]) && (
              <QualificationToJoin type={this.state.type} navigation={this.props.navigation} />
            )}

            {/* ================================================================================= */}
            {/* {this.state.type == "Apply" &&
              this.props.cliksDetails
                .getIn(["data", "clik"])
                .get("invite_only") == true && <ApplyToJoin />} */}

            {/* {this.state.type == "Apply" &&
              this.props.cliksDetails
                .getIn(["data", "clik"])
                .get("invite_only") == true && <JoinWithInviteKey />} */}

            {/* ================================================================ */}

            {this.state.showInviteLink == true &&
              this.props.cliksDetails
                .getIn(["data", "clik"])
                .get("invite_only") == false && <InviteViaLink />}

            {this.state.showInviteLink == true &&
              this.props.cliksDetails
                .getIn(["data", "clik"])
                .get("invite_only") == true && (
                <InviteLinkPrivate InviteType={this.state.InviteType} />
              )}

            {this.userApprovePermision == true && (
              <InviteUserModal
                onClose={() => this.onClose()}
                {...this.props}
                ClikInfo={this.props.cliksDetails.getIn(["data", "clik"])}
                showHeader={false}
              />
            )}
            <View
              style={[ButtonStyle.cardShadowStyle, {
                borderWidth: 0,
                // borderColor: "#C5C5C5",
                borderRadius: 10,
                backgroundColor: "#fff",
                marginBottom: 50
              }]}
            >
              <Text
                style={{
                  textAlign: "left",
                  color: "#000",
                  fontSize: 16,
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  marginVertical: 10,
                  marginHorizontal: 10
                }}
              >
                Current Members
              </Text>
              {this.props.listClikMembers.length > 0 ? (
                <View
                  style={{
                    paddingLeft: 10, width: '100%', paddingRight: 10,
                  }}
                >
                  <FlatList
                    extraData={this.state}
                    data={this.props.listClikMembers}
                    keyExtractor={item => item.node.user.id}
                    renderItem={this._renderClikMembersItem}
                  />
                </View>
              ) : (
                <View
                  style={{
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    height: hp("15%")
                  }}
                >
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center"
                    }}
                    reverse
                    name="user-times"
                    type="font-awesome"
                    size={20}
                    containerStyle={{
                      alignSelf: "center"
                    }}
                  />
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.defaultFont,
                      color: "#000",
                      alignSelf: "center"
                    }}
                  >
                    No Member found
                  </Text>
                </View>
              )}
            </View>
          </ScrollView>
        );
      case "first":
        return (
          <View>
            <CliksProfileFeedScreen
              navigation={this.props.navigation}
              listScroll={this.listScroll}
              ProfileHeight={this.state.ProfileHeight}
              scrollY={this.state.scrollY}
            />
          </View>
        );
      default:
        return (
          <View>
            <CliksProfileFeedScreen
              navigation={this.props.navigation}
              listScroll={this.listScroll}
              ProfileHeight={this.state.ProfileHeight}
              scrollY={this.state.scrollY}
            />
          </View>
        );
    }
  };


  renderTabViewForMobile = () => {
    if (this.props.getProfileTabView == "FEED") {
      return (
        <CliksProfileFeedScreen
          navigation={this.props.navigation}
          listScroll={this.listScroll}
          ProfileHeight={this.state.ProfileHeight}
          scrollY={this.state.scrollY}
        />
      )
    }
    else if (this.props.getProfileTabView == "USERS") {
      return (
        <ScrollView containerStyle={{ marginBottom: 50, flex: 1}}>
          {this.props.cliksDetails.getIn(["data", "clik"]) && (
            <QualificationToJoin type={this.state.type} navigation={this.props.navigation} />
          )}

          {/* ================================================================================= */}
          {/* {this.state.type == "Apply" &&
              this.props.cliksDetails
                .getIn(["data", "clik"])
                .get("invite_only") == true && <ApplyToJoin />} */}

          {/* {this.state.type == "Apply" &&
              this.props.cliksDetails
                .getIn(["data", "clik"])
                .get("invite_only") == true && <JoinWithInviteKey />} */}

          {/* ================================================================ */}

          {this.state.showInviteLink == true &&
            this.props.cliksDetails
              .getIn(["data", "clik"])
              .get("invite_only") == false && <InviteViaLink />}

          {this.state.showInviteLink == true &&
            this.props.cliksDetails
              .getIn(["data", "clik"])
              .get("invite_only") == true && (
              <InviteLinkPrivate InviteType={this.state.InviteType} />
            )}

          {this.userApprovePermision == true && (
            <InviteUserModal
              onClose={() => this.onClose()}
              {...this.props}
              ClikInfo={this.props.cliksDetails.getIn(["data", "clik"])}
              showHeader={false}
            />
          )}
          <View
            style={[ButtonStyle.cardShadowStyle, {
              borderWidth: 0,
              // borderColor: "#C5C5C5",
              borderRadius: 10,
              backgroundColor: "#fff",
              marginBottom: 50
            }]}
          >
            <Text
              style={{
                textAlign: "left",
                color: "#000",
                fontSize: 16,
                fontWeight: "bold",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                marginVertical: 10,
                marginHorizontal: 10
              }}
            >
              Current Members
              </Text>
            {this.props.listClikMembers.length > 0 ? (
              <View
                style={{
                  paddingLeft: 10, width: '100%', paddingRight: 10,
                }}
              >
                <FlatList
                  extraData={this.state}
                  data={this.props.listClikMembers}
                  keyExtractor={item => item.node.user.id}
                  renderItem={this._renderClikMembersItem}
                />
              </View>
            ) : (
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  height: hp("15%")
                }}
              >
                <Icon
                  color={"#000"}
                  iconStyle={{
                    color: "#fff",
                    justifyContent: "center",
                    alignItems: "center",
                    alignSelf: "center"
                  }}
                  reverse
                  name="user-times"
                  type="font-awesome"
                  size={20}
                  containerStyle={{
                    alignSelf: "center"
                  }}
                />
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: "bold",
                    fontFamily: ConstantFontFamily.defaultFont,
                    color: "#000",
                    alignSelf: "center"
                  }}
                >
                  No Member found
                  </Text>
              </View>
            )}
          </View>
        </ScrollView>
      )

    }
    else if (this.props.getProfileTabView == "APPLICATIONS") {
      return (
        <View>
          <View
            style={[ButtonStyle.cardShadowStyle, {
              borderWidth: 0,
              // borderColor: "#C5C5C5",
              borderRadius: 10,
              backgroundColor: "#fff",
              marginVertical: 10
            }]}
          >
            <Text
              style={{
                textAlign: "left",
                color: "#000",
                fontSize: 16,
                fontWeight: "bold",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                marginVertical: 10,
                marginHorizontal: 10
              }}
            >
              Qualifications
              </Text>
            {this.props.cliksDetails
              .getIn(["data", "clik"])
              .get("qualifications") == null && (
                <Text
                  style={{
                    textAlign: "center",
                    color: "#000",
                    fontFamily: ConstantFontFamily.defaultFont,
                    fontSize: 16,
                    fontWeight: "bold",
                    margin: 10
                  }}
                >
                  No prerequisites to join this clik
                </Text>
              )}
            {this.props.cliksDetails
              .getIn(["data", "clik"])
              .get("qualifications") != null &&
              this.props.cliksDetails
                .getIn(["data", "clik"])
                .get("qualifications")
                .map((item, index) => {
                  return (
                    <Text
                      key={index}
                      style={{
                        color: "#000",
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 16,
                        fontWeight: "bold",
                        marginBottom: 20,
                        marginLeft: 30
                      }}
                    >
                      {"\u25CF"} {item}
                    </Text>
                  );
                })}
          </View>
          {this.userApprovePermision == true && (
            <InviteUserModal
              onClose={() => this.onClose()}
              {...this.props}
              ClikInfo={this.props.cliksDetails.getIn(["data", "clik"])}
              showHeader={false}
            />
          )}
          <View
            style={[ButtonStyle.cardShadowStyle, {
              borderWidth: 0,
              // borderColor: "#C5C5C5",
              borderRadius: 10,
              backgroundColor: "#fff",
              marginBottom: 10
            }]}
          >
            <Text
              style={{
                textAlign: "left",
                color: "#000",
                fontSize: 16,
                fontWeight: "bold",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                marginVertical: 10,
                marginHorizontal: 10
              }}
            >
              Member Applications
              </Text>
            {this.props.listClikUserRequest.length > 0 &&
              this.userApprovePermision == true ? (
              <View>
                <FlatList
                  extraData={this.state}
                  data={this.props.listClikUserRequest}
                  keyExtractor={item => item.node.user.id}
                  renderItem={this._renderUserItem}
                />
              </View>
            ) : (
              this.userApprovePermision == true && (
                <View
                  style={{
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    height: hp("15%")
                  }}
                >
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center"
                    }}
                    reverse
                    name="user-times"
                    type="font-awesome"
                    size={20}
                    containerStyle={{
                      alignSelf: "center"
                    }}
                  />
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.defaultFont,
                      color: "#000",
                      alignSelf: "center"
                    }}
                  >
                    No Applications
                    </Text>
                </View>
              )
            )}
          </View>
        </View>
      )

    }
    else {
      return (
        <View style={{ flex: 1 }}>
          <CliksProfileFeedScreen
            navigation={this.props.navigation}
            listScroll={this.listScroll}
            ProfileHeight={this.state.ProfileHeight}
            scrollY={this.state.scrollY}
          />
        </View>)
    }
  }

  selectMember = memberValue => {
    this.setState({ memberValue: memberValue });
  };

  render() {
    const { cliksselectItem } = this.state;
    return (
      <View style={styles.container}>
        {this.state.showInviteKeyJoinModal == true ? (
          Platform.OS !== "web" ? (
            <Overlay
              animationType="zoomIn"
              visible={this.state.showInviteKeyJoinModal}
              onClose={() => this.onClose()}
              closeOnTouchOutside
              children={
                <InviteKeyJoinModal
                  onClose={() => this.onClose()}
                  {...this.props}
                  InviteClik={this.state.InviteClik}
                  invite_key={this.state.invite_key}
                />
              }
              childrenWrapperStyle={{
                padding: 0,
                margin: 0
              }}
            />
          ) : (
            <Modal
              isVisible={this.state.showInviteKeyJoinModal}
              onBackdropPress={() => this.onClose()}
              style={{
                marginHorizontal:
                  Dimensions.get("window").width > 750 ? "30%" : 10,
                padding: 0
              }}
            >
              <InviteKeyJoinModal
                onClose={() => this.onClose()}
                {...this.props}
                InviteClik={this.state.InviteClik}
                invite_key={this.state.invite_key}
              />
            </Modal>
          )
        ) : null}

        {Platform.OS == "web" && (
          <SEOMetaData
            title={
              this.props.cliksDetails.getIn(["data", "clik"])
                ? this.props.cliksDetails.getIn(["data", "clik"]).get("name")
                : ""
            }
            description={
              this.props.cliksDetails.getIn(["data", "clik"])
                ? this.props.cliksDetails
                  .getIn(["data", "clik"])
                  .getIn(["description"])
                : ""
            }
            image={
              this.props.cliksDetails.getIn(["data", "clik"])
                ? this.props.cliksDetails
                  .getIn(["data", "clik"])
                  .getIn(["banner_pic"])
                : ""
            }
          />
        )}
        {//Platform.OS != "web" &&
          Dimensions.get("window").width <= 750 && (
            <Animated.View
              style={{
                position: Platform.OS == "web" ? "sticky" : null,
                top: 0,
                left: 0,
                right: 0,
                zIndex: 10,
                overflow: "hidden",
                borderRadius: Dimensions.get("window").width <= 750 ? 0 : 6
              }}
            >
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <View
                  style={{
                    width: "100%",
                    flexDirection: "row",
                    marginBottom: Dimensions.get("window").width <= 750 ? 0 : 10,
                    backgroundColor: "#000",
                    borderRadius: Dimensions.get("window").width <= 750 ? 0 : 6,
                    height: 50
                  }}
                >
                  <TouchableOpacity
                    style={ButtonStyle.headerBackStyle}
                    onPress={() => {
                      let nav = this.props.navigation.dangerouslyGetParent()
                        .state;
                      if (nav.routes.length > 1) {
                        if (this.state.cliksselectItem == "USERS") {
                          this.setState({
                            cliksselectItem: "FEED"
                          });
                        } else {
                          this.props.navigation.goBack();
                        }
                      } else {
                        this.props.navigation.navigate("home");
                      }
                    }}
                  >
                    <Icon
                      color={"#fff"}
                      name="angle-left"
                      type="font-awesome"
                      size={40}
                    />
                  </TouchableOpacity>

                  {this.props.cliksDetails.size > 0 &&
                    !this.props.getsearchBarStatus &&
                    (
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          alignSelf: "center",
                          alignItems: "center",
                          marginRight: 5
                        }}
                      >
                        {this.props.loginStatus == 1 &&
                          this.state.showIcon.toString().startsWith("#") == (
                            <Image
                              source={this.state.showIcon}
                              style={{
                                width: 22,
                                height: 22
                              }}
                            />
                          )}
                        <TouchableOpacity
                          style={{
                            paddingVertical: 5,
                            alignItems: "center",
                            justifyContent: "center"
                          }}
                          onPress={() =>
                            this.CliksProfilescrollview.scrollTo({
                              x: 0,
                              y: 0,
                              animated: true
                            })
                          }
                        >
                          <View
                            style={{
                              alignItems: "center",
                              justifyContent: "center",
                              height: 30,
                              paddingVertical: 5,
                              backgroundColor: "#000",
                              borderRadius: 6,
                              alignSelf: "flex-start"
                            }}
                          >
                            <Text
                              style={{
                                color: "#fff",
                                fontWeight: "bold",
                                fontSize: 18,
                                fontFamily: ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              #
                          {this.props.navigation.state.params.id
                                .replace("Clik%3A", "#")
                                .replace("Clik:", "#")}
                            </Text>
                          </View>
                        </TouchableOpacity>

                        {/* {this.props.loginStatus == 1 && (
                        <View
                          style={{
                            flexDirection: "row",
                            position: "absolute",
                            right: 0,
                            marginRight: 20
                          }}
                        >
                          <ClikStar
                            ClikName={this.props.cliksDetails
                              .getIn(["data", "clik"])
                              .get("name")}
                            ContainerStyle={{}}
                            ImageStyle={{
                              height: 20,
                              width: 20,
                              alignSelf: "center",
                              marginLeft: 5
                            }}
                            MemberType={this.state.MemberType}
                          />
                        </View>
                      )} */}
                      </TouchableOpacity>
                    )}
                  <View style={ButtonStyle.headerRightStyle}>
                    <HeaderRight navigation={this.props.navigation} />
                  </View>
                </View>
              </View>
            </Animated.View>
          )}
        <ScrollView
          contentContainerStyle={{ height: "100%" }}
          ref={ref => {
            this.CliksProfilescrollview = ref;
          }}
          showsVerticalScrollIndicator={false}
          onScroll={event => {
            this.setState({
              scrollY: event.nativeEvent.contentOffset.y
            });
          }}
          scrollEventThrottle={16}
        // style={{
        //   // marginHorizontal: Dimensions.get("window").width <= 750 ? 0 : 5,
        //   // marginTop: Dimensions.get("window").width <= 750 ? 0 :
        //   Dimensions.get("window").width >= 1100 ? 0 : 10,}}
        >
          <Animated.View
            onLayout={event => {
              let { x, y, width, height } = event.nativeEvent.layout;
              if (width > 0) {
                this.setState({ ProfileHeight: height });
              }
            }}
          >
            <Suspense fallback={<ShadowSkeleton />}>
              {this.props.cliksDetails.getIn(["data", "clik"]) && (
                <CliksProfileCard
                  item={this.props.cliksDetails}
                  navigation={this.props.navigation}
                  icon={this.showIcon}
                  members={this.members}
                  showMembers={this.showMembers}
                  feedY={this.state.feedY}
                  ProfileHeight={this.state.ProfileHeight}
                  showIcon={this.state.showIcon}
                  showInviteLink={this.showInviteLink}
                  MemberType={this.state.MemberType}
                />
              )}
            </Suspense>
          </Animated.View>
          {Dimensions.get('window').width <= 750 ?
            // this.props.getProfileTabView ?
            <View
              style={{
                flex: 1,
                width: "100%",
                // padding: 10
              }}
            // contentContainerStyle={{flex:1}}
            >
              {this.renderTabViewForMobile()}
            </View>
            :
            <View
              style={
                Dimensions.get("window").width >= 750 &&
                this.state.index == 0 &&
                  this.state.scrollY >= this.state.ProfileHeight + 50
                  ? [
                    styles.header,
                    {
                      flexDirection: "row",
                      width: '100%',
                      // Platform.OS == "web" && Dimensions.get("window").width >= 750
                      //   ? 450
                      //   : "100%",
                      top:
                        this.state.scrollY >= this.state.ProfileHeight + 50
                          ? this.state.scrollY - 60
                          : 0
                    }
                  ]
                  :
                  {
                    flexDirection: "row",
                    width: '100%',
                    height: "100%",
                    // marginHorizontal: Dimensions.get("window").width >= 750 ? 5 : 0,
                  }
              }
            >
              <TabView
                lazy
                navigationState={this.state}
                renderScene={this._renderScene}
                renderLazyPlaceholder={this._renderLazyPlaceholder}
                renderTabBar={this._renderTabBar}
                onIndexChange={this._handleIndexChange}
                style={{
                  flex: 1
                }}
              />
            </View>
          }
        </ScrollView>
        {Dimensions.get("window").width <= 750 &&
          <BottomScreenProfile navigation={NavigationService} />
        }
      </View>
    );
  }
}

const mapStateToProps = state => ({
  cliksDetails: state.TrendingCliksProfileReducer.get(
    "getTrendingCliksProfileDetails"
  ),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  listClikUserRequest: !state.ClikUserRequestReducer.get("ClikUserRequestList")
    ? List()
    : state.ClikUserRequestReducer.get("ClikUserRequestList"),
  listClikMembers: !state.ClikMembersReducer.get("clikMemberList")
    ? List()
    : state.ClikMembersReducer.get("clikMemberList"),
  loginStatus: state.UserReducer.get("loginStatus"),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List(),
  getsearchBarStatus: state.AdminReducer.get("searchBarOpenStatus"),
  getProfileTabView: state.AdminReducer.get("profileTabType"),
  getTabView: state.AdminReducer.get("tabType"),
});

const mapDispatchToProps = dispatch => ({
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  userId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  setClikUserRequest: payload => dispatch(listClikUserRequest(payload)),
  setClikMembers: payload => dispatch(listClikMembers(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  setUsernameModalStatus: payload => dispatch(setUSERNAMEMODALACTION(payload)),
  setInviteUserDetail: payload =>
    dispatch({ type: "SET_INVITE_USER_DETAIL", payload }),
  searchOpenBarStatus: payload => dispatch({ type: "SEARCHBAR_STATUS", payload }),
  SetProfileTabView: (payload) => dispatch({ type: "SET_PROFILE_TAB_VIEW", payload }),
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  CliksProfileScreen
);

const styles = StyleSheet.create({
  header: {
    position: Platform.OS == "web" ? "fixed" : null,
    left: 0,
    right: 0,
    zIndex: 10
  },
  container: {
    flex: 1,
    backgroundColor: ConstantColors.customeBackgroundColor,
    // paddingTop: Dimensions.get("window").width <= 750 ? 0 : 10,
    // paddingHorizontal: Dimensions.get("window").width <= 750 ? 0 : 10
    // Platform.OS == "web" ? 10 : 5
  },
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 6,
    backgroundColor: "white",
    color: "black",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  inputAndroid: {
    paddingHorizontal: Dimensions.get("window").width > 750 ? 10 : 3,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#fff",
    borderRadius: 6,
    color: "#000",
    backgroundColor: "white",
    paddingRight: Dimensions.get("window").width > 750 ? 30 : 0,
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  actionButtonIcon: {
    fontSize: 20,
    // height: 20,
    color: "white",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10
  },
  indicator: {
    backgroundColor: "#009B1A",
    height: 4,
    borderRadius: 6
    // marginHorizontal: "2%"
  },
  tab: {
    backgroundColor: "transparent",
    flex: 1,
    width: "90%",
    height: 30,
    marginLeft: "auto",
    marginRight: "auto"
  },
  label: {
    fontSize: 16,
    fontWeight: "500",
    fontFamily: "Montserrat-Medium",
    letterSpacing: 1,
    width: "auto"
  },
  labelStyle: {
    color: "#000",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  }
});
