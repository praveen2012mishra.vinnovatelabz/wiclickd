import { launchImageLibraryAsync } from "expo-image-picker";
import { askAsync, CAMERA_ROLL } from "expo-permissions";
import { List } from "immutable";
import React, { Component } from "react";
import { graphql } from "react-apollo";
import {
  Animated,
  AsyncStorage,
  Dimensions,
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Button, Icon, Tooltip } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import NewHeader from "../components/NewHeader";
import AppHelper from "../constants/AppHelper";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import ConstantTooltip from "../constants/Tooltip";
import { CreateClikMutation } from "../graphqlSchema/graphqlMutation/PostMutation";
import { SearchUserMutation } from "../graphqlSchema/graphqlMutation/SearchMutation";
import {
  UserLoginMutation,
  UserQueryMutation
} from "../graphqlSchema/graphqlMutation/UserMutation";
import { CreateClikVariables } from "../graphqlSchema/graphqlVariables/PostVariables";
import { SearchUserVariables } from "../graphqlSchema/graphqlVariables/SearchVariables";
import { UserDetailsVariables } from "../graphqlSchema/graphqlVariables/UserVariables";
import {
  uploadBannerImageAsync,
  uploadProfileImageAsync
} from "../services/UserService";
import { nullableTypeAnnotation } from "@babel/types";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import ButtonStyle from "../constants/ButtonStyle";
import HeaderRight from "../components/HeaderRight";

class CreateClikScreen extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.Pagescrollview = null;
    this.state = {
      switchOn: false,
      textInput: [],
      value: 50,
      showSlider: false,
      textStyle: "WHITE",
      uploading: false,
      profilePic: "",
      title: "",
      text: "",
      items: [],
      selectText: "Cliks Name",
      getMemberDetails: [],
      uploadMutipleImagePost: [],
      changeBackPicEnable: null,
      setBackPic: null,
      setIcon: null,
      gradientColor: "rgba(0,0,0,0.9)",
      brightnessvalue: 0.5,
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      conditionIcon: false,
      focusnameInput: false,
      focusUsername: false,
      focusdescInput: false,
      
      summary: "",
      RoleItems: [
        {
          label: "Member",
          value: "MEMBER",
          key: 0
        },
        {
          label: "Admin",
          value: "ADMIN",
          key: 1
        },
        {
          label: "Super Admin",
          value: "SUPER_ADMIN",
          key: 2
        }
      ],
      SelectRoleItems: "Member",
      clikName: "",
      description: "",
      MutipleUser: [],
      MutipleUserList: [],
      showError: false,
      getImage: "",
      showcliktooltip: false,
      showaddmembertooltip: false,
      MutipleQualification: [],
      bannerHover: false,
      clikHover: false,
      descriptionHover: false,
      joinclikHover: false,
      website: "",
      qualification: "",
      modalVisible: false,
      UserList: [],
      switchValue: true,
      textInput: [],
      inputData: [],
      testArray: [{ text: "", index: 0 }],
      customQualificationArray: [{ text: "" }],
      customAddMemberArray: [
        {
          username: "",
          pic: "",
          showUserModal: false,
          roleName: "Member",
          member_type: "MEMBER"
        }
      ],
      MoreOptions: false,
      changeIconPicEnable: null
    };
    this.changeBannerImage = "";
    this.baseState = this.state;
  }

  componentDidMount = async () => {
    this.props.searchOpenBarStatus(false);
    this._isMounted = true;
    const profileData = this.props.profileData;
    let userProfilePic = profileData
      .getIn(["my_users", "0", "user"])
      .getIn(["profile_pic"]);
    this.setState({
      profilePic: {
        uri: userProfilePic
      }
    });
    let joined = [];
    this.props.listTrending_cliks.map(async (value, index) => {
      await joined.push({
        id: value.node.id,
        name: value.node.name
      });
    });
    this.setState({ items: joined });
  };

  componentDidUpdate() {
    if (this.props.getHasScrollTop == true && this.Pagescrollview) {
      this.Pagescrollview.scrollTo({ x: 0, y: 0, animated: true });
      this.props.setHASSCROLLEDACTION(false);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  _askPermission = async (type, failureMessage) => {
    const { status, permissions } = await askAsync(type);
    if (status === "denied") {
      alert(failureMessage);
    }
  };

  _pickBannerImage = async () => {
    await this._askPermission(
      CAMERA_ROLL,
      "We need the camera-roll permission to read pictures from your phone..."
    );
    let pickerResult = await launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    });
    this._handleBannerImagePicked(pickerResult);
  };

  _handleBannerImagePicked = async pickerResult => {
    let uploadResponse, uploadResult;
    try {
      this.setState({ uploading: true });
      if (!pickerResult.cancelled) {
        uploadResponse = await uploadBannerImageAsync(pickerResult.uri);
        uploadResult = await uploadResponse.json();
        this.setState({
          setBackPic: pickerResult.uri
        });
        this.setState({ changeBackPicEnable: uploadResult.id });
      }
    } catch (e) {
      alert("Upload failed, sorry :(" + e + ")");
    } finally {
      this.setState({ uploading: false });
    }
  };

  // ============upload icon===================
  pickIcon = async () => {
    await this._askPermission(
      CAMERA_ROLL,
      "We need the camera-roll permission to read pictures from your phone..."
    );
    let pickerResult = await launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    });
    let size = pickerResult.uri.length * (3 / 4) - 2;
    if (size <= 20000) {
      this._handleIconImagePicked(pickerResult);
    } else {
      alert("choose an image less than 20kb");
    }
  };

  _handleIconImagePicked = async pickerResult => {
    let uploadResponse, uploadResult;
    try {
      this.setState({ uploading: true });
      if (!pickerResult.cancelled) {
        uploadResponse = await uploadProfileImageAsync(pickerResult.uri);
        uploadResult = await uploadResponse.json();
        this.setState({
          setIcon: pickerResult.uri
        });
        this.setState({ changeIconPicEnable: uploadResult.id });
      }
    } catch (e) {
      alert("Upload failed, sorry :(" + e + ")");
    } finally {
      this.setState({ uploading: false });
    }
  };

  checkSelectedUser = async (value, index) => {
    UserDetailsVariables.variables.id = "User:" + value;
    try {
      await applloClient
        .query({
          query: UserQueryMutation,
          ...UserDetailsVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState({
            MutipleUserList: this.state.MutipleUserList.concat([
              {
                name: res.data.user.username,
                pic: res.data.user.profile_pic,
                role: this.state.SelectRoleItems
              }
            ])
          });
          //-----set userdetails on the custom field
          let inputArray = [...this.state.customAddMemberArray];
          // inputArray[index].username = res.data.user.username;
          // inputArray[index].showUserModal = false;
          // inputArray[index].pic = res.data.user.profile_pic;

          inputArray[this.state.customAddMemberArray.length - 1].username =
            res.data.user.username;
          inputArray[
            this.state.customAddMemberArray.length - 1
          ].showUserModal = false;
          inputArray[this.state.customAddMemberArray.length - 1].pic =
            res.data.user.profile_pic;

          this.setState({
            showError: false,
            SelectRoleItems: "Member",
            title: ""
          });
        });
    } catch (e) {
      console.log(e);
      this.setState({
        showError: true
      });
    }
  };

  createCliks = async () => {
    let __self = this;
    let MemberArray = [];
    for (let i = 0; i < this.state.customAddMemberArray.length; i++) {
      MemberArray.push({
        userid_or_email: this.state.customAddMemberArray[i].username,
        member_type: this.state.customAddMemberArray[i].member_type
      });
    }
    CreateClikVariables.variables.name = this.state.clikName;
    CreateClikVariables.variables.description = this.state.description;
    CreateClikVariables.variables.banner_pic = this.state.changeBackPicEnable;
    CreateClikVariables.variables.icon_pic = this.state.changeIconPicEnable;
    CreateClikVariables.variables.qualifications =
      this.state.customQualificationArray[0].text != ""
        ? this.state.customQualificationArray.map(value => value.text)
        : [];
    CreateClikVariables.variables.website = this.state.website;
    CreateClikVariables.variables.my_qualification = "auto approve";
    CreateClikVariables.variables.invite_only = this.state.switchValue;
    CreateClikVariables.variables.invited_users =
      MemberArray[0].userid_or_email != "" ? MemberArray : [];
    try {
      let id = await AsyncStorage.getItem("UserId");
      await applloClient
        .query({
          query: CreateClikMutation,
          ...CreateClikVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState(this.baseState);
          let resDataLogin = await __self.props.Login();
          await __self.props.saveLoginUser(resDataLogin.data.login);
          if (resDataLogin) {
            await __self.props.getTrendingClicks({
              currentPage: AppHelper.PAGE_LIMIT
            });
            let clikId = res.data.clik_create && res.data.clik_create.clik.name;
            this.props.userId({
              id: clikId,
              type: "feed"
            });
            this.props.setPostCommentReset({
              payload: [],
              postId: "",
              title: "",
              loading: true
            });
          }
        });
    } catch (e) {
      console.log(e);
    }
  };

  tooglecliktooltip = () => {
    if (this.state.showcliktooltip == false) {
      this.setState({ showcliktooltip: true });
    } else {
      this.setState({ showcliktooltip: false });
    }
  };

  toogletooltip = () => {
    if (this.state.showtooltip == false) {
      this.setState({ showtooltip: true });
    } else {
      this.setState({ showtooltip: false });
    }
  };

  checkClikname = async name => {
    var letters = /^[0-9a-zA-Z-]+$/;
    if (name.match(letters) || name == "") {
      if (name[0] == "-") {
        alert("- not allowed at initial of clik name");
        return false;
      }
      this.setState({ clikName: name });
      return true;
    } else {
      alert("Please input alphanumeric characters only");
      return false;
    }
  };

  customRenderUserSuggestion = (value, index) => {
    console.log(value);
    let inputArray = [...this.state.customAddMemberArray];
    inputArray[index].username = value;
    inputArray[index].showUserModal = true;
    this.setState({ customAddMemberArray: inputArray });
    SearchUserVariables.variables.prefix = value;
    applloClient
      .query({
        query: SearchUserMutation,
        ...SearchUserVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        this.setState({
          UserList: res.data.search.users,
          getMemberDetails: Object.assign([], inputArray)
        });
      });
  };

  onPress = () => {
    this.setState({ switchOn: !this.state.switchOn });
  };

  // custom qualification ============================================
  addcustomQualificationArray = e => {
    this.setState({
      customQualificationArray: [
        ...this.state.customQualificationArray,
        { text: "" }
      ]
    });
  };

  handleCustomQualification = (value, index) => {
    let inputArray = [...this.state.customQualificationArray];
    inputArray[index].text = value;
    this.setState({ customQualificationArray: inputArray }, () => { });
  };

  removecustomQualificationArray = (index, e) => {
    this.state.customQualificationArray.splice(index, 1);
    this.setState({
      customQualificationArray: this.state.customQualificationArray
    });
  };
  //---------------add custom member
  addcustomMember() {
    this.setState({
      customAddMemberArray: [
        ...this.state.customAddMemberArray,
        {
          username: "",
          pic: "",
          showUserModal: false,
          roleName: "Member",
          member_type: "MEMBER"
        }
      ]
    });
  }
  removeCustomMember = index => {
    this.state.customAddMemberArray.splice(index, 1);
    this.setState({ customAddMemberArray: this.state.customAddMemberArray });
  };
  setRoleType = (value, name, index) => {
    let inputArray = [...this.state.customAddMemberArray];
    inputArray[index].roleName = name;
    inputArray[index].member_type = value;
    this.setState({ customAddMemberArray: inputArray });
  };

  render() {
    const { setBackPic, setIcon } = this.state;
    const textStyle = styles.usertext;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: ConstantColors.customeBackgroundColor,
          width: "100%",
          paddingTop: Dimensions.get("window").width <= 1100 ? 0 : 10,
          paddingHorizontal: Dimensions.get("window").width <= 1100 ? 0 : 10
        }}
      >
        <ScrollView
          ref={scrollview => {
            this.Pagescrollview = scrollview;
          }}
          showsVerticalScrollIndicator={false}
          onLayout={event => {
            let { x, y, width, height } = event.nativeEvent.layout;
            if (width < 1024) {
              this.setState({
                showsVerticalScrollIndicatorView: true,
                currentScreentWidth: width
              });
            } else {
              this.setState({
                showsVerticalScrollIndicatorView: false,
                currentScreentWidth: width
              });
            }
          }}
          style={{
            height:
              Platform.OS !== "web"
                ? null
                : Dimensions.get("window").height - 80,
          }}
        >
          <View>
            {Dimensions.get("window").width <= 750 && (
              <Animated.View
                style={{
                  position: Platform.OS == "web" ? "sticky" : null,
                  top: 0,
                  left: 0,
                  right: 0,
                  zIndex: 10,
                  overflow: "hidden"
                }}
              >
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      flexDirection: "row",
                      backgroundColor: "#000",
                      height: 50
                    }}
                  >
                    <TouchableOpacity
                      style={ButtonStyle.headerBackStyle}
                      onPress={() => {
                        let nav = this.props.navigation.dangerouslyGetParent()
                          .state;
                        if (nav.routes.length > 1) {
                          this.props.navigation.goBack();
                          return;
                        } else {
                          this.props.navigation.navigate("home");
                        }
                      }}
                    >
                      <Icon
                        color={"#fff"}
                        name="angle-left"
                        type="font-awesome"
                        size={40}
                      />
                    </TouchableOpacity>
                    {!this.props.getsearchBarStatus &&
                      <TouchableOpacity
                        style={[ButtonStyle.headerTitleStyle, { backgroundColor: "#000" }]}
                      >
                        <Text
                          style={{
                            color: "white",
                            textAlign: "center",
                            fontWeight: "bold",
                            fontSize: 18,
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          Create Clik
                      </Text>
                      </TouchableOpacity>}
                    <View
                      style={ButtonStyle.headerRightStyle}
                    >
                      <HeaderRight navigation={this.props.navigation} />
                    </View>
                  </View>
                </View>
              </Animated.View>
            )}
            <View style={{
              marginVertical: Dimensions.get("window").width <= 1100 ? 10 : 0,
              marginHorizontal: Dimensions.get("window").width <= 1100 ? 5 : 0, alignItems: 'center'
            }}>
              <View
                style={[
                  ButtonStyle.borderStyle, ButtonStyle.textAreaShadowStyle,
                  {
                    width: "99%",
                    borderWidth:0,
                    borderRadius:5,
                    marginLeft: 2,
                    backgroundColor: "#fff",
                    padding: Dimensions.get("window").width <= 750 ? 5 : 10,
                  }
                ]}
              >
                <Text
                  numberOfLines={2}
                  style={{
                    fontWeight: "bold",
                    fontSize: 16,
                    textAlign: "center",
                    alignSelf: "center",
                    justifyContent: "center",
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  {/* A clik is a discussion group. You can share posts to each other
                  and also start a discussion with only members of your clik . */}
                  A clik is a discussion group. Share and discuss posts exclusively among members of your clik.
              </Text>
              </View>

              <View
                style={[
                  // ButtonStyle.borderStyle,
                  {
                    backgroundColor: "#fff",
                    paddingVertical: 10,
                    paddingHorizontal: 5,
                    // marginTop: 10,
                    width:'100%'
                  }
                ]}
              >
                {/* <View style={{ flexDirection: "row", width: "100%" }}>
                  <View
                    style={{
                      width: "70%",
                      justifyContent: "flex-start",
                      flexDirection: "row"
                    }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: 10,
                        marginRight: 20,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Banner
                  </Text>
                  </View>
                  <TouchableOpacity
                    style={{
                      width: "30%",
                      justifyContent: "flex-end",
                      alignItems: "flex-end"
                    }}
                    onMouseEnter={() => this.setState({ bannerHover: true })}
                    onMouseLeave={() => this.setState({ bannerHover: false })}
                  >
                    {setBackPic == null && (
                      <Icon
                        color={"#f80403"}
                        iconStyle={{
                          marginTop: 10,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                        name="times"
                        type="font-awesome"
                        size={16}
                      />
                    )}
                    {this.state.bannerHover == true && Platform.OS == "web" ? (
                      <Tooltip
                        backgroundColor={"#d3d3d3"}
                        withPointer={false}
                        withOverlay={false}
                        toggleOnPress={true}
                        containerStyle={{
                          left: -60
                        }}
                        popover={
                          <Text
                            style={{ fontFamily: ConstantFontFamily.defaultFont }}
                          >
                            Upload a Banner picture
                        </Text>
                        }
                      />
                    ) : null}
                  </TouchableOpacity>
                </View> */}
                {/* <View
                  style={{
                    height: Dimensions.get("window").height / 4,
                    borderColor: "#C5C5C5",
                    borderWidth: 1,
                    backgroundColor: "#fff",
                    borderRadius: Platform.OS == "web" ? 20 : null
                  }}
                >
                  <ImageBackground
                    style={styles.image}
                    imageStyle={{
                      borderRadius: 20
                    }}
                    source={{
                      uri: setBackPic
                    }}
                    resizeMode={"cover"}
                  >
                    <Icon
                      color={"#000"}
                      iconStyle={{
                        color: "#fff",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      reverse
                      name="camera"
                      type="font-awesome"
                      size={20}
                      containerStyle={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        flex: 1
                      }}
                      onPress={this._pickBannerImage}
                    />
                  </ImageBackground>
                </View> */}
                <Text
                  style={{
                    fontWeight: "bold",
                    marginTop: 5,
                    marginRight: 20,
                    fontSize: 16,
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  Add Icon (Less than 20kb)
              </Text>
                <View
                  style={[ ButtonStyle.shadowStyle,{
                    height: 120,
                    borderWidth: 1,
                    borderColor: "#C5C5C5",
                    borderWidth: 1,
                    backgroundColor: "#fff",
                    borderRadius: Platform.OS == "web" ? 5 : null,
                    width: 120,
                  }]}
                >
                  <ImageBackground
                    style={styles.image}
                    imageStyle={{
                      borderRadius: 5
                    }}
                    source={{
                      uri: setIcon
                    }}
                    resizeMode={"cover"}
                  >
                    <Icon
                      color={"#000"}
                      iconStyle={{
                        color: "#fff",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      reverse
                      name="camera"
                      type="font-awesome"
                      size={16}
                      containerStyle={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        flex: 1
                      }}
                      onPress={this.pickIcon}
                    />
                  </ImageBackground>
                </View>

                <View style={{ flexDirection: "row", width: "100%", marginTop:10 }}>
                  <View
                    style={{
                      width: "70%",
                      justifyContent: "flex-start",
                      flexDirection: "row",                     
                    }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: 10,
                        marginRight: 10,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Clik Name
                    </Text>
                    {Platform.OS == "web" ? (
                      <View>
                        <Icon
                          color={"#000"}
                          iconStyle={{
                            marginTop: 10,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                          name="info-circle"
                          type="font-awesome"
                          size={16}
                          onPress={() => this.tooglecliktooltip()}
                        />
                        {this.state.showcliktooltip == true && (
                          <Tooltip
                            withPointer={false}
                            withOverlay={false}
                            toggleOnPress={true}
                            containerStyle={{
                              left: -40,
                              top: -60,
                              width: 100
                            }}
                            popover={
                              <Text
                                style={{
                                  fontFamily: ConstantFontFamily.defaultFont
                                }}
                              >
                                {ConstantTooltip.CreateClik[0].ClikTooltip}
                              </Text>
                            }
                          />
                        )}
                      </View>
                    ) : (
                        <Tooltip
                          withOverlay={false}
                          popover={
                            <Text
                              style={{ fontFamily: ConstantFontFamily.defaultFont }}
                            >
                              {ConstantTooltip.CreateClik[0].ClikTooltip}
                            </Text>
                          }
                        >
                          <Icon
                            color={"#000"}
                            iconStyle={{
                              marginTop: 10,
                              justifyContent: "center",
                              alignItems: "center"
                            }}
                            name="info-circle"
                            type="font-awesome"
                            size={16}
                          />
                        </Tooltip>
                      )}
                  </View>
                  <TouchableOpacity
                    style={{
                      width: "30%",
                      justifyContent: "flex-end",
                      alignItems: "flex-end"
                    }}
                    onMouseEnter={() => this.setState({ clikHover: true })}
                    onMouseLeave={() => this.setState({ clikHover: false })}
                  >
                    {(this.state.clikName.length < 3 ||
                      this.state.clikName.length > 24 ||
                      this.state.clikName[this.state.clikName.length - 1] ==
                      "-") && (
                        <Icon
                          color={"#f80403"}
                          iconStyle={{
                            marginTop: 10,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                          name="times"
                          type="font-awesome"
                          size={16}
                        />
                      )}
                    {this.state.clikHover == true && Platform.OS == "web" ? (
                      <Tooltip
                        backgroundColor={"#d3d3d3"}
                        withPointer={false}
                        withOverlay={false}
                        toggleOnPress={true}
                        containerStyle={{
                          left: -60,
                          top: -60
                        }}
                        popover={
                          <Text
                            style={{ fontFamily: ConstantFontFamily.defaultFont }}
                          >
                            ClikName should be 3 to 24 Characters
                        </Text>
                        }
                      />
                    ) : null}
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row"}}>
                  <TextInput
                    value={this.state.clikName}
                    placeholder="EnterNameWithOutSpaces"
                    placeholderTextColor="#6D757F"
                    style={ [
                      textStyle,
                      this.state.focusnameInput ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                      {                       
                        height: 45,
                        paddingHorizontal: 5,
                        fontFamily: ConstantFontFamily.defaultFont,
                      }
                    ]}
                    onChangeText={clikName => this.checkClikname(clikName)}
                    onFocus = {()=> this.setState({focusnameInput: true})}
                    onBlur = {()=> this.setState({focusnameInput: false})}
                  />
                </View>

                <View style={{ flexDirection: "row", width: "100%", marginTop:10, alignItems:'flex-end'}}>
                  <View
                    style={{
                      width: "70%",
                      justifyContent: "flex-start",
                      flexDirection: "row"
                    }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginRight: 20,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Description
                  </Text>
                  </View>
                  <TouchableOpacity
                    style={{
                      width: "30%",
                      justifyContent: "flex-end",
                      alignItems: "flex-end"
                    }}
                    onMouseEnter={() => this.setState({ descriptionHover: true })}
                    onMouseLeave={() =>
                      this.setState({ descriptionHover: false })
                    }
                  >
                    {(this.state.description.length < 50 ||
                      this.state.description.length > 300) && (
                        <Icon
                          color={"#f80403"}
                          iconStyle={{
                            marginTop: 10,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                          name="times"
                          type="font-awesome"
                          size={16}
                        />
                      )}
                    {this.state.descriptionHover == true &&
                      Platform.OS == "web" ? (
                        <Tooltip
                          backgroundColor={"#d3d3d3"}
                          withPointer={false}
                          withOverlay={false}
                          toggleOnPress={true}
                          containerStyle={{
                            left: -60,
                            top: -60
                          }}
                          popover={
                            <Text
                              style={{ fontFamily: ConstantFontFamily.defaultFont }}
                            >
                              Description should be 50 to 300 Characters
                        </Text>
                          }
                        />
                      ) : null}
                  </TouchableOpacity>
                </View>
                <TextInput
                  value={this.state.description}
                  multiline={true}
                  numberOfLines={5}
                  placeholder="Enter a description"
                  placeholderTextColor="#6D757F"
                  style={[
                    textStyle, 
                    this.state.focusdescInput ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                    {                   
                      paddingHorizontal: 5,
                      marginVertical:5,
                      paddingTop:12,
                      height: Platform.OS == "ios" ? 100 : 45,
                      fontFamily: ConstantFontFamily.defaultFont,
                    }
                  ]}
                  onChangeText={description => this.setState({ description })}
                  onFocus = {()=> this.setState({focusdescInput: true})}
                  onBlur = {()=> this.setState({focusdescInput: false})}
                />

                <View
                  style={{ flexDirection: "row", width: "100%", marginTop: 10 }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      marginTop: 5,
                      marginRight: 20,
                      fontSize: 16,
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }}
                  >
                    Invite Only
                </Text>
                  <View
                    style={{
                      alignItems: "center",
                      flexDirection: "row",
                      alignSelf: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Button
                      title="Yes"
                      buttonStyle={{
                        backgroundColor:
                          this.state.switchValue == true ? "#009B1A" : "#fff",
                        borderColor: "#e1e1e1",
                        borderWidth: 1,
                        height: 30,
                        borderBottomLeftRadius: 6,
                        borderTopLeftRadius: 6
                      }}
                      titleStyle={{
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        paddingHorizontal: 10,
                        fontSize: 14,
                        color: this.state.switchValue == true ? "#fff" : "#e1e1e1"
                      }}
                      onPress={() => this.setState({ switchValue: true })}
                    />
                    <Button
                      title="No"
                      buttonStyle={{
                        backgroundColor:
                          this.state.switchValue == false ? "#009B1A" : "#fff",
                        borderColor: "#e1e1e1",
                        borderWidth: 1,
                        height: 30,
                        borderTopRightRadius: 6,
                        borderBottomRightRadius: 6
                      }}
                      titleStyle={{
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        paddingHorizontal: 10,
                        fontSize: 14,
                        color:
                          this.state.switchValue == false ? "#fff" : "#e1e1e1"
                      }}
                      onPress={() => this.setState({ switchValue: false })}
                    />
                  </View>
                </View>

                <View
                  style={{
                    width: "100%",
                    marginTop: 10,
                    flexDirection: "row",
                    justifyContent: "space-between"
                  }}
                >
                  <View
                    style={{
                      width: "70%",
                      justifyContent: "flex-start",
                      flexDirection: "row",
                      paddingBottom: 5
                    }}
                  >
                    <Text
                      style={{
                        color: "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        fontSize: 16,
                        fontWeight: "bold",
                        marginTop: 10
                      }}
                    >
                      Add Members
                  </Text>
                    {Platform.OS == "web" ? (
                      <View>
                        <Icon
                          color={"#000"}
                          iconStyle={{
                            marginTop: 10,
                            justifyContent: "center",
                            alignItems: "center",
                            marginLeft: 10
                          }}
                          name="info-circle"
                          type="font-awesome"
                          size={16}
                          onPress={() => this.toogletooltip()}
                        />
                        {this.state.showtooltip == true && (
                          <Tooltip
                            withPointer={false}
                            withOverlay={false}
                            toggleOnPress={true}
                            containerStyle={{
                              left: -20,
                              width: 100
                            }}
                            popover={
                              <Text
                                style={{
                                  fontFamily: ConstantFontFamily.defaultFont
                                }}
                              >
                                Info here
                            </Text>
                            }
                          />
                        )}
                      </View>
                    ) : (
                        <Tooltip
                          withOverlay={false}
                          popover={
                            <Text
                              style={{ fontFamily: ConstantFontFamily.defaultFont }}
                            >
                              Info here
                        </Text>
                          }
                        >
                          <Icon
                            color={"#000"}
                            iconStyle={{
                              marginTop: 10,
                              justifyContent: "center",
                              alignItems: "center",
                              marginLeft: 20
                            }}
                            name="info-circle"
                            type="font-awesome"
                            size={16}
                          />
                        </Tooltip>
                      )}
                  </View>
                  <TouchableOpacity
                    style={{
                      width: "30%",
                      alignSelf: "center"
                    }}
                    onMouseEnter={() => this.setState({ joinclikHover: true })}
                    onMouseLeave={() => this.setState({ joinclikHover: false })}
                  >
                    <Icon
                      name="plus"
                      type="font-awesome"
                      size={18}
                      cursor="pointer"
                      containerStyle={{
                        marginLeft: "auto",
                        justifyContent: "center",
                        alignItems: "center",
                        cursor: "pointer"
                      }}
                      onPress={() => this.addcustomMember()}
                    />
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    flexDirection: "column"
                  }}
                >
                  {this.state.customAddMemberArray.map((item, index) => {
                    return (
                      <View style={{ flexDirection: "column", marginTop: 5 }}>
                        <View
                          style={{
                            flexDirection: "row",
                            width: "100%",
                            justifyContent: "space-between"
                          }}
                        >
                          <View
                            style={{
                              width: "60%",
                              justifyContent: "flex-start",
                              flexDirection: "row"
                            }}
                          >
                            {item.pic == "" || item.pic == null ? (
                              <Image
                                source={require("../assets/image/default-image.png")}
                                style={{
                                  width: 40,
                                  height: 40,
                                  padding: 0,
                                  margin: 5,
                                  borderRadius: 5
                                }}
                              />
                            ) : (
                                <Image
                                  source={{ uri: item.pic }}
                                  style={{
                                    width: 40,
                                    height: 40,
                                    padding: 0,
                                    margin: 5,
                                    borderRadius: 5
                                  }}
                                />
                              )}
                            <TextInput
                              value={item.username}
                              placeholder="Username or Email"
                              placeholderTextColor="#6D757F"
                              style={[
                                this.state.focusUsername ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                                {
                                  color: "#000",
                                  fontSize: 14,
                                  fontWeight: "bold",
                                  fontFamily: ConstantFontFamily.defaultFont
                                },
                                {                                  
                                  height: 45,
                                  padding: 5,
                                  outline: 'none',
                                }
                              ]}
                              onChangeText={username => {
                                this.setState({ username: username }, () => {
                                  this.customRenderUserSuggestion(
                                    this.state.username,
                                    index
                                  );
                                });
                              }}
                              onFocus={()=>this.setState({focusUsername: true})}
                              onBlur = {()=> this.setState({focusUsername: false})}
                            />
                          </View>
                          <View
                            style={[ButtonStyle.shadowStyle, {
                              width: "30%",
                              justifyContent: "flex-end",
                              borderRadius: 5,
                              borderColor: "#e1e1e1",
                              borderWidth: 0,
                              marginLeft: 5,
                              height: 45,
                              marginTop: 5,
                              alignSelf: "center",
                              marginTop:0
                            }]}
                          >
                            <RNPickerSelect
                              placeholder={{}}
                              items={this.state.RoleItems}
                              onValueChange={(itemValue, itemIndex) => {
                                this.setRoleType(
                                  itemValue,
                                  this.state.RoleItems[itemIndex].label,
                                  index
                                );
                                this.setState(
                                  {
                                    SelectRoleItems: itemValue
                                  },
                                  () => console.log(this.state.SelectRoleItems)
                                );
                              }}
                              onUpArrow={() => {
                                this.inputRefs.name.focus();
                              }}
                              onDownArrow={() => {
                                this.inputRefs.picker2.togglePicker();
                              }}
                              style={{ ...styles }}
                              ref={el => {
                                this.inputRefs.picker = el;
                              }}
                            />
                          </View>
                          <View
                            style={{
                              width: "5%",
                              alignSelf: "center",
                              justifyContent: "flex-end"
                            }}
                          >
                            <Icon
                              color={"#000"}
                              name="trash"
                              type="font-awesome"
                              size={18}
                              iconStyle={{
                                justifyContent: "center",
                                alignItems: "center",
                                marginLeft: 20
                              }}
                              onPress={() => this.removeCustomMember()}
                            />
                          </View>
                        </View>
                      </View>
                    );
                  })}
                  {// item.showUserModal &&
                    this.state.UserList.map((item, index) => {
                      return (
                        <View
                          key={item.username}
                          style={{
                            backgroundColor: "#FEFEFA",
                            width: "100%",
                            padding: 5,
                            marginLeft: 40
                          }}
                        >
                          <Text
                            style={{
                              color: "#000",
                              fontFamily: ConstantFontFamily.defaultFont,
                              fontWeight: "bold"
                            }}
                            onPress={() =>
                              this.checkSelectedUser(item.username, index)
                            }
                          >
                            {item.username}
                          </Text>
                        </View>
                      );
                    })}
                </View>
                {this.state.MoreOptions == false && (
                  <TouchableOpacity
                    style={{
                      marginTop: 5
                    }}
                    onPress={() =>
                      this.setState({
                        MoreOptions: true
                      })
                    }
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: 10,
                        marginRight: 20,
                        fontSize: 14,
                        fontFamily: ConstantFontFamily.defaultFont,
                        textDecorationLine: "underline",
                        fontStyle: "italic"
                      }}
                    >
                      Clik Here For More Options
                  </Text>
                  </TouchableOpacity>
                )}
                {this.state.MoreOptions == true && (
                  <View>
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: 10,
                        marginRight: 20,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Max Number Of Members
                  </Text>
                    <TextInput
                      placeholder="100"
                      placeholderTextColor="#000"
                      style={[
                        textStyle,
                        this.state.focusMaxNum ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                        {
                          height: 45,
                          padding: 5,
                          fontFamily: ConstantFontFamily.defaultFont,
                        }
                      ]}
                      onChangeText={summary => this.setState({ summary })}
                      onFocus = {()=> this.setState({focusMaxNum: true})}
                      onBlur = {()=> this.setState({focusMaxNum: false})}
                    />

                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: 10,
                        marginRight: 20,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Website
                  </Text>
                    <TextInput
                      value={this.state.website}
                      placeholder="Optional"
                      style={[
                        textStyle,
                        this.state.focusWeb ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                        {
                          height: 45,
                          padding: 5,
                          outline: 'none',
                        }
                      ]}
                      onChangeText={website => this.setState({ website })}
                      onFocus = {()=> this.setState({focusWeb: true})}
                      onBlur = {()=> this.setState({focusWeb: false})}
                    />

                    <View style={{ flexDirection: "row", width: "100%" }}>
                      <View
                        style={{
                          width: "70%",
                          justifyContent: "flex-start",
                          flexDirection: "row"
                        }}
                      >
                        <Text
                          style={{
                            fontWeight: "bold",
                            marginTop: 10,
                            marginRight: 20,
                            fontSize: 16,
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          Prerequisites to Join Clik
                      </Text>
                      </View>
                      <TouchableOpacity
                        style={{
                          width: "30%",
                          justifyContent: "flex-end",
                          alignItems: "flex-end"
                        }}
                        onMouseEnter={() =>
                          this.setState({ joinclikHover: true })
                        }
                        onMouseLeave={() =>
                          this.setState({ joinclikHover: false })
                        }
                      >
                        <Icon
                          name="plus"
                          type="font-awesome"
                          size={18}
                          cursor="pointer"
                          containerStyle={{
                            marginLeft: 5,
                            justifyContent: "center",
                            alignItems: "center",
                            cursor: "pointer"
                          }}
                          onPress={() => this.addcustomQualificationArray()}
                        />
                      </TouchableOpacity>
                    </View>

                    {this.state.customQualificationArray &&
                      this.state.customQualificationArray.map((item, index) => {
                        return (
                          <View
                            style={{
                              width: "100%",
                              flexDirection: "row",
                              alignItems: "center"
                            }}
                          >
                            <Text style={{ width: "5%" }}>{index + 1})</Text>
                            <TextInput
                              style={[
                                textStyle,
                                this.state.focusQualification ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                                {                              
                                height: 45,
                                padding: 5,
                                fontFamily: ConstantFontFamily.defaultFont,
                              }]}
                              value={item.text}
                              placeholder="Enter a qualification"
                              onChangeText={text =>
                                this.handleCustomQualification(text, index)
                              }
                              onFocus = {()=> this.setState({focusQualification: true})}
                              onBlur = {()=> this.setState({focusQualification: false})}
                            />
                            <TouchableOpacity
                              style={{ width: "5%" }}
                              onPress={() =>
                                this.removecustomQualificationArray(index)
                              }
                            >
                              <Icon
                                name="trash"
                                type="font-awesome"
                                size={18}
                                cursor="pointer"
                                containerStyle={{
                                  marginLeft: "auto",
                                  justifyContent: "center",
                                  alignItems: "center",
                                  cursor: "pointer"
                                }}
                              />
                            </TouchableOpacity>
                          </View>
                        );
                      })}
                  </View>
                )}

                <View
                  style={{
                    marginTop: 10,
                    alignSelf: "center"
                  }}
                >
                  <Button
                    title="Create Clik"
                    titleStyle={ButtonStyle.wtitleStyle}
                    buttonStyle={ButtonStyle.gbackgroundStyle}
                    containerStyle={ButtonStyle.containerStyle}
                    disabled={
                      this.state.clikName.length < 3 ||
                        this.state.clikName.length > 24 ||
                        this.state.clikName[this.state.clikName.length - 1] ==
                        "-" ||
                        this.state.description.length < 50 ||
                        this.state.description.length > 300 
                        // setBackPic == null
                        ? // this.state.MutipleQualification.length == 0
                        true
                        : false
                    }
                    onPress={() => this.createCliks()}
                  />
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  listTrending_cliks: !state.TrendingCliksReducer.getIn(["Trending_cliks_List"])
    ? List()
    : state.TrendingCliksReducer.getIn(["Trending_cliks_List"]),
  link: state.LinkPostReducer.get("link"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getsearchBarStatus: state.AdminReducer.get("searchBarOpenStatus"),
});

const mapDispatchToProps = dispatch => ({
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  userId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  setPostCommentReset: payload =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload }),
  searchOpenBarStatus: payload => dispatch({ type: "SEARCHBAR_STATUS", payload })
});

const CreateClikScreenContainerWrapper = compose(
  graphql(UserLoginMutation, {
    name: "Login",
    options: { fetchPolicy: "no-cache" }
  })
)(CreateClikScreen);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  CreateClikScreenContainerWrapper
);

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: "100%"
  },
  usertext: {
    color: "#000",
    fontSize: 14,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  },
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 20,
    backgroundColor: "white",
    color: "black",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  },
  inputAndroid: {
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#fff",
    borderRadius: 20,
    color: "#000",
    backgroundColor: "white",
    paddingRight: 30,
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  }
});