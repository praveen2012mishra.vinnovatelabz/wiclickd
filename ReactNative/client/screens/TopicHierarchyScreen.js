import { List } from "immutable";
import React, { Component } from "react";
import { graphql } from "react-apollo";
import {
  Animated,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { Button, Icon } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import { getTrendingTopicsProfileDetails } from "../actionCreator/TrendingTopicsProfileAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import TopicStructure from "../components/TopicStructure";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import {
  TopicFollowMutation,
  TopicUnFollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import {
  GetChildTopicsMutation,
  TopicQuery
} from "../graphqlSchema/graphqlMutation/TrendingMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  TopicFollowVariables,
  TopicUnFollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import NavigationService from "../library/NavigationService";
import ButtonStyle from "../constants/ButtonStyle";

class TopicHierarchyScreen extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.Pagescrollview = null;
    this.state = {
      value: 50,
      showSlider: false,
      textStyle: "WHITE",
      uploading: false,
      profilePic: "",
      title: "",
      text: "",
      items: [],
      selectText: "Cliks Name",
      uploadMutipleImagePost: [],
      changeBackPicEnable: null,
      setBackPic: null,
      gradientColor: "rgba(0,0,0,0.9)",
      brightnessvalue: 0.5,
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      conditionIcon: false,
      summary: "",
      RoleItems: [
        {
          label: "Member",
          value: "Member",
          key: 0
        },
        {
          label: "Admin",
          value: "Admin",
          key: 1
        },
        {
          label: "Super Admin",
          value: "Super Admin",
          key: 2
        }
      ],
      SelectRoleItems: "Member",
      topicName: "",
      rtopicName: "",
      description: "",
      MutipleUser: [],
      MutipleUserList: [],
      showError: false,
      getImage: "",
      showcliktooltip: false,
      showaddmembertooltip: false,
      MutipleQualification: [],
      bannerHover: false,
      clikHover: false,
      descriptionHover: false,
      joinclikHover: false,
      qualification: "",
      modalVisible: false,
      TopicList: [],
      TopicListHierarchy: [],
      UserList: [],
      showDeleteIcon: false,
      inputParentName: false,
      rtopicParents: "#000",
      rtopicselected: "",
      parentsOfSelectedTopic: [],
      childrenByTopics: {},
      openTopics: []
    };
    this.changeBannerImage = "";
    this.baseState = this.state;
  }

  componentDidMount = async () => {
    this._isMounted = true;
    if (this.props.loginStatus == 1) {
      const profileData = this.props.profileData;
      let userProfilePic = profileData
        .getIn(["my_users", "0", "user"])
        .getIn(["profile_pic"]);
      this.setState({
        profilePic: {
          uri: userProfilePic
        }
      });
    }
    let joined = [];
    this.props.listTrending_cliks.map(async (value, index) => {
      await joined.push({
        id: value.node.id,
        name: value.node.name
      });
    });
    this.setState({ items: joined });
  };

  componentDidUpdate() {
    if (this.props.getHasScrollTop == true && this.Pagescrollview) {
      this.Pagescrollview.scrollTo({ x: 0, y: 0, animated: true });
      this.props.setHASSCROLLEDACTION(false);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getTopicProfileDetails = value => {
    applloClient
      .query({
        query: TopicQuery,
        variables: {
          id: "Topic:" + value
        },
        fetchPolicy: "no-cache"
      })
      .then(res => {
        this.setState({
          parentsOfSelectedTopic: res.data.topic.parents
            ? [...res.data.topic.parents, value]
            : [value]
        });
      });
  };
  getChildStructure(arr, selectTopic, childrenOfSelectTopic) {
    let output = [];
    let i = 0;
    if (arr.length > 0)
      output[i] = {
        name: arr[i],
        children:
          selectTopic === arr[i]
            ? [...childrenOfSelectTopic]
            : this.getChildStructure(
                arr.slice(i + 1),
                selectTopic,
                childrenOfSelectTopic
              )
      };

    return output;
  }
  renderTopicListHierarchy = (value, topicId) => {
    this.setState({
      TopicListHierarchy: []
    });
    // if not exist topic in `childrenByTopics` then call api
    if (!this.state.childrenByTopics[topicId]) {
      applloClient
        .query({
          query: GetChildTopicsMutation,
          variables: {
            id: "Topic:" + value
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          let { children } = res.data.topic;
          this.state.TopicListHierarchy;
          this.setState(({ childrenByTopics, openTopics }) => ({
            childrenByTopics: { ...childrenByTopics, [topicId]: children },
            openTopics: this.handleToggleArr(openTopics, topicId)
          }));

          this.setState({
            TopicListHierarchy: this.getChildStructure(
              this.state.parentsOfSelectedTopic,
              value,
              [{ name: "dynamicText" }, ...children]
            )
          });
        });
    } else {
      this.setState(({ openTopics }) => ({
        openTopics: this.handleToggleArr(openTopics, topicId)
      }));
    }
  };
  handleToggleArr = (inputArr, value) => {
    let arr = [...inputArr];
    arr.indexOf(value) === -1
      ? arr.push(value)
      : arr.splice(arr.indexOf(value), 1);
    return arr;
  };
  handleTopicSelectInput = (topic, parents, item) => {
    this.setState(
      {
        rtopicName: topic,
        TopicList: [],
        TopicListHierarchy: []
      },
      () => {
        if (parents) {
          this.colorCondition(parents);
        } else {
          this.colorCondition("");
        }
        if (topic != "") {
          if (item.banner_pic) {
            let v = item.banner_pic.split("/");
            let k = v[v.length - 1].substring(0, v[v.length - 1].indexOf("."));
            this.setState({
              setBackPic: item.banner_pic,
              showDeleteIcon: true,
              changeBackPicEnable: k
            });
          }

          this.getTopicProfileDetails(topic);
          this.renderTopicListHierarchy(topic, item.id);
        }
        if (this.state.rtopicName == "") {
          this.setState({
            rtopicParents: "",
            TopicList: []
          });
        }
      }
    );
  };

  colorCondition = parent => {
    if (parent.length > 0) {
      this.setState({
        rtopicParents: "#FEC236"
      });
    } else if (parent == "") {
      this.setState({
        rtopicParents: "#009B1A"
      });
    } else if (parent == null) {
      this.setState({
        rtopicParents: "#009B1A"
      });
    }
  };

  goToProfile = async id => {
    this.props.topicId({
      id: id,
      type: "feed"
    });
  };

  getTopicStar = TopicName => {
    let index = 0;
    index = this.props.getUserFollowTopicList.findIndex(
      i =>
        i
          .getIn(["topic", "name"])
          .toLowerCase()
          .replace("topic:", "") ==
        TopicName.toLowerCase().replace("topic:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowTopicList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  followTopics = topicId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    TopicFollowVariables.variables.topic_id = topicId;
    TopicFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowTopics = async topicId => {
    TopicUnFollowVariables.variables.topic_id = topicId;
    applloClient
      .query({
        query: TopicUnFollowMutation,
        ...TopicUnFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  favroiteTopics = async topicId => {
    TopicFollowVariables.variables.topic_id = topicId;
    TopicFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  render() {
    const { openTopics, childrenByTopics, topicName } = this.state;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: ConstantColors.customeBackgroundColor,
          width: "100%",
          padding: 10
        }}
      >
        <View>
          {Platform.OS !== "web" ? (
            <Animated.View
              style={{
                position: Platform.OS == "web" ? "sticky" : null,
                top: 0,
                left: 0,
                right: 0,
                zIndex: 10,
                overflow: "hidden",
                borderRadius: 6
              }}
            >
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <View
                  style={{
                    width: "100%",
                    flexDirection: "row",
                    backgroundColor: "#000",
                    borderRadius: 6,
                    height: 42
                  }}
                >
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      justifyContent: "flex-start",
                      alignSelf: "center",
                      paddingLeft: 10 
                    }}
                    onPress={() => {
                      let nav = this.props.navigation.dangerouslyGetParent()
                        .state;
                      if (nav.routes.length > 1) {
                        this.props.navigation.goBack();
                        return;
                      } else {
                        this.props.navigation.navigate("home");
                      }
                    }}
                  >
                    <Icon
                      color={"#fff"}
                      name="angle-left"
                      type="font-awesome"
                      size={40}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: "100%",
                      flex: 1,
                      backgroundColor: "#000",
                      justifyContent: "center",
                      borderRadius: 6
                    }}
                  >
                    <Text
                      style={{
                        color: "white",
                        textAlign: "center",
                        fontWeight: "bold",
                        fontSize: 18,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Topic Hierarchy
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Animated.View>
          ) : null
          // (
          //     <TouchableOpacity
          //       style={{
          //         width: "100%",
          //         justifyContent: "center",
          //         borderRadius: 6,
          //         height: 40
          //       }}
          //     >
          //       <Text
          //         style={{
          //           color: "black",
          //           textAlign: "center",
          //           fontWeight: "bold",
          //           fontSize: 18,
          //           fontFamily: ConstantFontFamily.MontserratBoldFont
          //         }}
          //       >
          //         Topic Hierarchy
          //     </Text>
          //     </TouchableOpacity>
          //   )
          }
        </View>

        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            backgroundColor: "#fff",
            paddingTop: 0,
            border: "1px solid #c5c5c5",
            borderRadius: 10
          }}
        >
          {this.props.getTrending_Topics_List.map((item, i) => {
            if (!item.node.parents) {
              return (
                <TopicStructure
                  isRoot={true}
                  item={item.node}
                  childrenByTopics={childrenByTopics}
                  openTopics={openTopics}
                  topicName={topicName}
                  handleTopicSelectInput={this.handleTopicSelectInput}
                  getTopicStar={this.getTopicStar}
                  goToProfile={this.goToProfile}
                  followTopics={this.followTopics}
                  unfollowTopics={this.unfollowTopics}
                  favroiteTopics={this.favroiteTopics}
                />
              );
            }
          })}
        </ScrollView>

        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignSelf: "center",
            alignItems: "center"
          }}
        >
          <Button
            onPress={() => {
              return NavigationService.navigate("createtopic");
            }}
            title="Propose Topic"
            titleStyle={ButtonStyle.titleStyle}
            buttonStyle={ButtonStyle.backgroundStyle}
            containerStyle={ButtonStyle.containerStyle}
          />

          <Button
            onPress={() => {
              return NavigationService.navigate("faq");
            }}
            title="Topic FAQ"
            titleStyle={ButtonStyle.titleStyle}
            buttonStyle={ButtonStyle.backgroundStyle}
            containerStyle={ButtonStyle.containerStyle}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  listTrending_cliks: !state.TrendingCliksReducer.getIn(["Trending_cliks_List"])
    ? List()
    : state.TrendingCliksReducer.getIn(["Trending_cliks_List"]),
  loginStatus: state.UserReducer.get("loginStatus"),
  getTrending_Topics_List: state.TrendingTopicsReducer.get(
    "Trending_Topics_List"
  )
    ? state.TrendingTopicsReducer.get("Trending_Topics_List")
    : List(),
  link: state.LinkPostReducer.get("link"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  getUserFollowTopicList: state.LoginUserDetailsReducer.get(
    "userFollowTopicsList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowTopicsList")
    : List(),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension")
});

const mapDispatchToProps = dispatch => ({
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  topicId: payload => dispatch(getTrendingTopicsProfileDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload))
});

const TopicHierarchyScreenWrapper = compose(
  graphql(UserLoginMutation, {
    name: "Login",
    options: { fetchPolicy: "no-cache" }
  })
)(TopicHierarchyScreen);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  TopicHierarchyScreenWrapper
);
