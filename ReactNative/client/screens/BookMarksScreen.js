import React, { Component } from "react";
import {
  Animated,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import ConstantFontFamily from "../constants/FontFamily";
import NavigationService from "../library/NavigationService";
import DrawerScreen from "../components/DrawerScreens";
import TrendingTabs from "../components/TrendingTabs";
import ButtonStyle from '../constants/ButtonStyle';
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Dimensions } from "react-native";

class BookMarksScreen extends Component {
  totalHeigth = 0;
  constructor(props) {
    super(props);
    this.state = {
      getHeight: 100,
      randomItemEvent: 0,
      primaryIndex: 0,
      isSearched: false,
      searchedWord: "",
      isActive: false,
      searchedFollowText: ""
    };
  }

  calHeightLeftPannelMian = height => {
    if (height > 0 && height != null) {
      this.totalHeigth = height;
    }
  };

  activeIndexAction = index => {
    this.setState({
      primaryIndex: index,
      searchedWord: ""
    });
  };

  activeTrendingIndexAction = index => {
    this.setState({
      searchedFollowText: ""
    });
  };

  changeColor = isHovered => {
    if (isHovered == true) {
      switch (this.state.primaryIndex) {
        case 0:
          return "#009B1A";
        case 1:
          return "#4C82B6";
        case 2:
          return "#FEC236";
        default:
          return "#F34225";
      }
    }
  };

  getType = () => {
    switch (this.state.primaryIndex) {
      case 0:
        return "TOPICS";
      case 1:
        return "CLIKS";
      case 2:
        return "USERS";
      default:
        return "FEEDS";
    }
  };

  // click on the search icon
  searchClicked = () => {
    this.setState({ isSearched: true });
  };

  searchParaChanged = () => {
    this.setState({ searchedWord: "" });
  };
  // type on the search field

  searchTextField = value => {
    this.setState({ searchedWord: value });
  };

  render() {
    return (
      <View
        style={{
          padding: Dimensions.get('window').width<=750 ? 0 : 10,
          flex: 1
        }}
      >
        <Animated.View
          style={{
            position: Platform.OS == "web" ? "sticky" : null,
            top: 0,
            left: 0,
            right: 0,
            zIndex: 10,
            overflow: "hidden"
          }}
        >
          <View
            style={{
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <View
              style={{
                width: "100%",
                flexDirection: "row",
                backgroundColor: "#000",
                borderRadius: Dimensions.get('window').width<=750 ? 0 : 6,
                height: 42
              }}
            >
              <TouchableOpacity
                style={ButtonStyle.headerBackStyle}
                onPress={() => {
                  let nav = this.props.navigation.dangerouslyGetParent().state;
                  if (nav.routes.length > 1) {
                    this.props.navigation.goBack();
                    return;
                  } else {
                    this.props.navigation.navigate("home");
                  }
                }}
              >
                <Icon
                  color={"#fff"}
                  name="angle-left"
                  type="font-awesome"
                  size={40}
                />
              </TouchableOpacity>
              <View
                style={{
                  width: "100%",
                  flex: 1,
                  backgroundColor: "#000",
                  justifyContent: "center",
                  alignItems: "center",
                  flexDirection: "row"
                }}
              >
                <Text
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontWeight: "bold",
                    fontSize: 18,
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    paddingRight: 45,
                    marginLeft: 20
                  }}
                >
                  Favorites
                </Text>
              </View>
            </View>
          </View>
        </Animated.View>  
        <View
          style={[ButtonStyle.borderStyle,{
            paddingHorizontal: 10,
            flex: 1,
            height: hp('50%'),
            // borderWidth: 1,
            // borderColor: "#c5c5c5",
            // borderRadius: 10,
            backgroundColor: "#fff",
            marginTop: 10
          }]}
        >
          {this.props.loginStatus == 1 ? (
            <DrawerScreen
              calHeightLeftPannelSend={this.calHeightLeftPannelMian}
              navigation={this.props.navigation}
              activeIndex={this.activeIndexAction}
              searchParam={this.searchParaChanged}
              searchedWord={this.state.searchedWord}
            />
          ) : (
            <TrendingTabs
              navigation={this.props.navigation}
              randomItemEvent={this.state.randomItemEvent}
              searchedFollowText={this.state.searchedWord}
              activeIndex={this.activeTrendingIndexAction}
              onchangetext={query => this.searchTextField(query)}
              onpress={() => this.setState({ searchedFollowText: "" })}
            />
          )}
        </View>      
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus")
});

const mapDispatchToProps = dispatch => ({
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  BookMarksScreen
);
