import React, { Component } from "react";
import {
  Animated,
  Dimensions,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import NewHeader from "../components/NewHeader";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";

class PrivacyPolicyScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          ref={scrollview => {
            this.Pagescrollview = scrollview;
          }}
          showsVerticalScrollIndicator={false}
          onLayout={event => {
            let { x, y, width, height } = event.nativeEvent.layout;
            if (width < 1024) {
              this.setState({
                showsVerticalScrollIndicatorView: true,
                currentScreentWidth: width
              });
            } else {
              this.setState({
                showsVerticalScrollIndicatorView: false,
                currentScreentWidth: width
              });
            }
          }}
          style={{
            height:
              Platform.OS !== "web"
                ? null
                : Dimensions.get("window").height - 80
          }}
        >
          <View>
            {Dimensions.get('window').width <= 750 ? (
              <Animated.View
                style={{
                  position: Platform.OS == "web" ? "sticky" : null,
                  top: 0,
                  left: 0,
                  right: 0,
                  zIndex: 10,
                  overflow: "hidden",
                  borderRadius: 6
                }}
              >
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      flexDirection: "row",
                      marginBottom: 10,
                      backgroundColor: "#000",
                      borderRadius: 6,
                      height: 42
                    }}
                  >
                    <TouchableOpacity
                      style={{
                        flexDirection: "row",
                        justifyContent: "flex-start",
                        alignSelf: "center"
                      }}
                      onPress={() => {
                        let nav = this.props.navigation.dangerouslyGetParent()
                          .state;
                        if (nav.routes.length > 1) {
                          this.props.navigation.goBack();
                          return;
                        } else {
                          this.props.navigation.navigate("home");
                        }
                      }}
                    >
                      <Icon
                        color={"#fff"}
                        name="angle-left"
                        type="font-awesome"
                        size={40}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        flex: 1,
                        backgroundColor: "#000",
                        justifyContent: "center",
                        marginLeft: 20,
                        borderRadius: 6
                      }}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontWeight: "bold",
                          fontSize: 18,
                          fontFamily: ConstantFontFamily.MontserratBoldFont
                        }}
                      >
                        Privacy policy
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Animated.View>
            ) : (
              null
              // <TouchableOpacity
              //   style={{
              //     width: "100%",
              //     justifyContent: "center",
              //     borderRadius: 6,
              //     height: 40
              //   }}
              // >
              //   <Text
              //     style={{
              //       color: "black",
              //       textAlign: "center",
              //       fontWeight: "bold",
              //       fontSize: 18,
              //       fontFamily: ConstantFontFamily.MontserratBoldFont
              //     }}
              //   >
              //      Privacy policy
              //   </Text>
              // </TouchableOpacity>
            )}
            <ScrollView showsVerticalScrollIndicator={false}>
              <View
                style={{
                  borderRadius: 6
                }}
              ></View>
              <View>
                <View
                  style={{
                    marginVertical: Platform.OS != "web" ? 10 : 15,
                    paddingHorizontal: Platform.OS != "web" ? 5 : 0
                  }}
                >
                  {Platform.OS === "web" ? (
                    <iframe
                      src={require("../assets/privacy.pdf")}
                      style={{ height: 600, width: "100%" }}
                    />
                  ) : null}
                  {/* <Text>
                    Online Privacy Policy What information do we collect? We
                    collect information from you when you respond to a survey.
                    Google, as a third party vendor, uses cookies to serve ads
                    on your site. Google's use of the DART cookie enables it to
                    serve ads to your users based on their visit to your sites
                    and other sites on the Internet. Users may opt out of the
                    use of the DART cookie by visiting the Google ad and content
                    network privacy policy.. What do we use your information
                    for? Any of the information we collect from you may be used
                    in one of the following ways: To improve our website (we
                    continually strive to improve our website offerings based on
                    the information and feedback we receive from you) Do we use
                    cookies? Yes (Cookies are small files that a site or its
                    service provider transfers to your computers hard drive
                    through your Web browser (if you allow) that enables the
                    sites or service providers systems to recognize your browser
                    and capture and remember certain information We use cookies
                    to compile aggregate data about site traffic and site
                    interaction so that we can offer better site experiences and
                    tools in the future. We may contract with third-party
                    service providers to assist us in better understanding our
                    site visitors. These service providers are not permitted to
                    use the information collected on our behalf except to help
                    us conduct and improve our business. Do we disclose any
                    information to outside parties? We do not sell, trade, or
                    otherwise transfer to outside parties your personally
                    identifiable information. This does not include trusted
                    third parties who assist us in operating our website,
                    conducting our business, or servicing you, so long as those
                    parties agree to keep this information confidential. We may
                    also release your information when we believe release is
                    appropriate to comply with the law, enforce our site
                    policies, or protect ours or others rights, property, or
                    safety. However, non-personally identifiable visitor
                    information may be provided to other parties for marketing,
                    advertising, or other uses. Third party links Occasionally,
                    at our discretion, we may include or offer third party
                    products or services on our website. These third party sites
                    have separate and independent privacy policies. We therefore
                    have no responsibility or liability for the content and
                    activities of these linked sites. Nonetheless, we seek to
                    protect the integrity of our site and welcome any feedback
                    about these sites. California Online Privacy Protection Act
                    Compliance Because we value your privacy we have taken the
                    necessary precautions to be in compliance with the
                    California Online Privacy Protection Act. We therefore will
                    not distribute your personal information to outside parties
                    without your consent. Childrens Online Privacy Protection
                    Act Compliance We are in compliance with the requirements of
                    COPPA (Childrens Online Privacy Protection Act), we do not
                    collect any information from anyone under 13 years of age.
                    Our website, products and services are all directed to
                    people who are at least 13 years old or older. Online
                    Privacy Policy Only This online privacy policy applies only
                    to information collected through our website and not to
                    information collected offline. Terms and Conditions Please
                    also visit our Terms and Conditions section establishing the
                    use, disclaimers, and limitations of liability governing the
                    use of our website at terms and conditions. Your Consent By
                    using our site, you consent to our privacy policy. Changes
                    to our Privacy Policy If we decide to change our privacy
                    policy, we will post those changes on this page, and/or
                    update the Privacy Policy modification date below. This
                    policy was last modified on 2013-12-16 Contacting Us If
                    there are any questions regarding this privacy policy you
                    may contact us using the information below.
                    generator.lorem-ipsum.info 126 Electricov St. Kiev, Kiev
                    04176 Ukraine contact@lorem-ipsum.info
                  </Text> */}
                </View>
              </View>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  PostDetails: state.PostDetailsReducer.get("PostDetails"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  loginStatus: state.UserReducer.get("loginStatus"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension")
});

const mapDispatchToProps = dispatch => ({
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  PrivacyPolicyScreen
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ConstantColors.customeBackgroundColor,
    paddingTop: Dimensions.get('window').width <= 750? 5 : 0,
    paddingHorizontal: Platform.OS == "web" ? 10 : 5
  }
});
