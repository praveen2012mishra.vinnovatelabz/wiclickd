import React, { Component } from "react";
import { Dimensions, Image, View, Platform } from "react-native";
import ButtonStyle from "../constants/ButtonStyle";

class NotFound extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          paddingTop: 10
        }}
      >
        <Image
          resizeMode={"contain"}
          source={require("../assets/image/404.jpg")}
          style={{
            height: 550,
            width: "100%",
            borderRadius: 10,
            borderWidth: 1,
            borderColor: "#C5C5C5",
            alignSelf: "center",
            justifyContent: "center"
          }}
        />
      </View>
    );
  }
}

export default NotFound;
