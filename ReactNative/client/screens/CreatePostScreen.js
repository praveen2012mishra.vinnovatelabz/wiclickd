import { launchImageLibraryAsync } from "expo-image-picker";
import { askAsync, CAMERA_ROLL } from "expo-permissions";
import { openBrowserAsync } from "expo-web-browser";
import { fromJS, List } from "immutable";
import React, { Component } from "react";
import { graphql } from "react-apollo";
import {
  ActivityIndicator,
  Animated,
  Dimensions,
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View
} from "react-native";
import { Button, Icon, Tooltip } from "react-native-elements";
import Slider from "react-native-slider";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import applloClient from "../client";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import { rootTopics } from "../constants/RootTopics";
import { PostCreateMutation } from "../graphqlSchema/graphqlMutation/PostMutation";
import {
  SearchClikMutation,
  SearchTopicMutation
} from "../graphqlSchema/graphqlMutation/SearchMutation";
import { PostCreateVariables } from "../graphqlSchema/graphqlVariables/PostVariables";
import {
  SearchClikVariables,
  SearchTopicVariables
} from "../graphqlSchema/graphqlVariables/SearchVariables";
import { uploadPostImageAsync } from "../services/UserService";
import ButtonStyle from "../constants/ButtonStyle";
import { postLink } from "../actionCreator/LinkPostAction";
import HeaderRight from "../components/HeaderRight";

const SearchParam = new URLSearchParams(window.location.search);

class CreatePostScreen extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.Pagescrollview = null;
    this.state = {
      listHomeFeed: [],
      value: 50,
      showSlider: false,
      textStyle: "WHITE",
      uploading: false,
      profilePic: "",
      selectedItems: [],
      title:
        Platform.OS == "web"
          ? SearchParam.has("title")
            ? SearchParam.get("title")
            : this.props.link.get("title")
          : this.props.link.get("title"),
      text: "",
      items: [],
      selectText: "Cliks Name",
      uploadMutipleImagePost: [],
      changeBackPicEnable: {
        uri:
          Platform.OS == "web"
            ? SearchParam.has("image")
              ? SearchParam.get("image")
              : this.props.link.get("image")
            : this.props.link.get("image")
      },
      gradientColor: "rgba(0,0,0,0.9)",
      brightnessvalue: 0.5,
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      conditionIcon: false,
      cliksText: "",
      summary:
        Platform.OS == "web"
          ? SearchParam.has("description")
            ? SearchParam.get("description")
            : this.props.link.get("description")
          : this.props.link.get("description"),
      showtopictooltip: false,
      showcliktooltip: false,
      typedClikname: "",
      selectedTopics: [],
      ClikInputwidth: 400,
      TopicInputwidth: 400,
      topicHover: false,
      titleHover: false,
      summaryHover: false,
      clikHover: false,
      focusTopic: false,
      focusTitle: false,
      focusSummary: false,
      topic: "",
      TopicList: [],
      clik: "",
      ClikList: [],
      selectedCliks: [],
      url:
        Platform.OS == "web"
          ? SearchParam.has("url")
            ? SearchParam.get("url")
            : this.props.link.get("url")
          : this.props.link.get("url"),
      quickTopicArray: [],
      quickClikArray: [],
      loading: false
    };
    this.items = [];
    this.changeBannerImage = "";
    this.Topicitems = [];
    this.baseState = this.state;
  }

  componentDidMount = async () => {
    this.props.searchOpenBarStatus(false);
    this._isMounted = true;
    const profileData = this.props.profileData;
    let userProfilePic = profileData
      .getIn(["my_users", "0", "user"])
      .getIn(["profile_pic"]);
    this.setState({
      profilePic: {
        uri: userProfilePic
      }
    });
    let joined = [];
    this.props.getUserFollowCliksList.map(async (value, index) => {
      if (value.getIn(["member_type"]) == "SUPER_ADMIN") {
        await joined.push({
          id: value.getIn(["clik", "id"]),
          name: value.getIn(["clik", "name"])
        });
      }
    });
    this.items = joined;
  };

  componentDidUpdate = async prevProps => {
    if (
      prevProps.link.get("description") !== this.props.link.get("description")
    ) {
      this.setState({
        summary: this.props.link.get("description"),
        selectedTopics: [],
        selectedCliks: [],
        topic: ""
      });
    }

    if (prevProps.link.get("title") !== this.props.link.get("title")) {
      this.setState({
        title: this.props.link.get("title"),
        selectedTopics: [],
        selectedCliks: [],
        topic: ""
      });
    }

    if (prevProps.link.get("url") !== this.props.link.get("url")) {
      this.setState({
        url: this.props.link.get("url"),
        selectedTopics: [],
        selectedCliks: [],
        topic: ""
      });
    }

    if (prevProps.link.get("image") !== this.props.link.get("image")) {
      this.setState({
        changeBackPicEnable: { uri: this.props.link.get("image") },
        selectedTopics: [],
        selectedCliks: [],
        topic: ""
      });
    }
  };

  componentWillUnmount() {
    this._isMounted = false;
    this.setState(this.baseState);
  }

  _askPermission = async (type, failureMessage) => {
    const { status, permissions } = await askAsync(type);
    if (status === "denied") {
      alert(failureMessage);
    }
  };

  _topPickBannerImageSingle = async () => {
    await this._askPermission(
      CAMERA_ROLL,
      "We need the camera-roll permission to read pictures from your phone..."
    );
    let pickerResult = await launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    });
    this.__topHandleBannerImagePicked(pickerResult);
  };

  __topHandleBannerImagePicked = async pickerResult => {
    let uploadResponse, uploadResult;
    try {
      this.setState({ uploading: true });
      if (!pickerResult.cancelled) {
        uploadResponse = await uploadPostImageAsync(pickerResult.uri);
        uploadResult = await uploadResponse.json();
        this.setState({
          changeBackPicEnable: {
            id: uploadResult.id,
            uri: pickerResult.uri,
            setPic: true
          }
        });
        this.props.postlink({
          description: this.state.summary,
          image: pickerResult.uri,
          title: this.state.title,
          url: this.state.url,
          withoutUrl: this.props.link.get("withoutUrl")
        });
        this.setState({
          conditionIcon: true
        });
      }
    } catch (e) {
      alert("Upload failed, sorry :(" + e + ")");
    } finally {
      this.setState({ uploading: false });
    }
  };

  onSave = async () => {
    let __self = this;
    this.setState({
      loading: true
    });
    new Promise(async (resole, reject) => {
      let uploadImageTotal = [];
      if (this.state.uploadMutipleImagePost.length > 0) {
        this.state.uploadMutipleImagePost.forEach(async (element, index) => {
          uploadImageTotal.push(element.id);
        });
      }
      PostCreateVariables.variables.title = this.state.title;
      PostCreateVariables.variables.text = this.state.text;
      PostCreateVariables.variables.summary = this.state.summary;
      PostCreateVariables.variables.link = this.state.url;
      PostCreateVariables.variables.thumbnail_pic_url = this.state
        .changeBackPicEnable.id
        ? ""
        : this.props.link.get("image");
      if (this.state.changeBackPicEnable) {
        PostCreateVariables.variables.thumbnail_pic = this.state
          .changeBackPicEnable.id
          ? this.state.changeBackPicEnable.id
          : "";
      }
      if (PostCreateVariables.variables.pictures.length == 0) {
        PostCreateVariables.variables.pictures = uploadImageTotal;
      }
      if (this.state.selectedCliks.length > 0) {
        let newUpdateArray = [];
        this.state.selectedCliks.forEach(async (element, index) => {
          await newUpdateArray.push(element.name);
        });
        PostCreateVariables.variables.cliks = newUpdateArray;
      }
      if (this.state.selectedTopics.length > 0) {
        let newUpdateTopicArray = [];
        this.state.selectedTopics.forEach(async (element, index) => {
          await newUpdateTopicArray.push(element.name);
        });
        PostCreateVariables.variables.topics = newUpdateTopicArray;
      }
      this.changeBannerImage = null;
      //this.setState(this.baseState);
      let resData = await this.props.PostCreate(PostCreateVariables);
      resole(resData);
    })
      .then(async r => {
        if (r) {
          this.setState({
            loading: false
          });
          await __self.props.setPostDetails({
            id: r.data.post_create.post.id,
            title: "Home",
            navigate: true
          });
          await __self.props.setPostCommentDetails({
            id: r.data.post_create.post.id,
            title: r.data.post_create.post.title
          });
        }
      })
      .catch(e => {
        console.log(e);
        this.setState({
          loading: false
        });
      });
  };

  changeBrightness = value => {
    this.setState({
      value: value
    });
    this.setState({
      brightnessvalue: value / 100
    });
  };

  openWindow = async link => {
    await openBrowserAsync(link);
  };

  handleDelete = index => {
    let tagsSelected = this.state.selectedItems;
    tagsSelected.splice(index, 1);
    this.setState({ selectedItems: tagsSelected });
  };

  handleTopicSelectInput = topic => {
    if (this.state.selectedTopics.length < 3) {
      let index = this.state.selectedTopics.findIndex(i => i.name == topic);
      if (index != -1) {
        alert("topic name already selected");
      } else {
        this.setState({
          selectedTopics: this.state.selectedTopics.concat([{ name: topic }]),
          topic: "",
          TopicList: [],
          ClikList: []
        });
      }
    } else {
      alert("You can only choose Maximum 3 Topics to Tag");
      this.setState({
        topic: ""
      });
    }
  };

  handleTopicDelete = index => {
    let tagsSelected = this.state.selectedTopics;
    tagsSelected.splice(index, 1);
    this.setState({ selectedTopics: tagsSelected });
  };

  customRenderTopicTags = tags => {
    return (
      <View
        style={{
          flexDirection: "row",
          flexWrap: "wrap",
          alignItems: "flex-start",
          backgroundColor: "#fff",
          width: "100%",
          padding: 5
        }}
      >
        {this.state.selectedTopics.map((t, i) => {
          return (
            <View
              key={t.name}
              style={{
                flexDirection: "row",
                backgroundColor: "#e3f9d5",
                // rootTopics.includes(t.name) == false ? "#e3f9d5" : "#FEF6D1",
                justifyContent: "center",
                alignItems: "center",
                height: 30,
                marginLeft: 10,
                marginTop: 3,
                borderRadius: 10,
                padding: 3
              }}
            >
              <TouchableHighlight
                key={i}
                onPress={() => this.handleTopicDelete(i)}
              >
                <Text
                  style={{
                    color: "#009B1A",
                    // rootTopics.includes(t.name) == false
                    //   ? "#009B1A"
                    //   : "#FEC236",
                    fontWeight: "bold",
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  /{t.name.toLowerCase()}
                </Text>
              </TouchableHighlight>
              <Icon
                color={
                  "#009B1A"
                  // rootTopics.includes(t.name) == false ? "#009B1A" : "#FEC236"
                }
                name="times"
                type="font-awesome"
                size={12}
                iconStyle={{ marginLeft: 5 }}
                onPress={() => this.handleTopicDelete(i)}
              />
            </View>
          );
        })}
      </View>
    );
  };

  customRenderTopicSuggestion = async value => {
    let topicsearchData = [];
    let cliksearchData = [];
    this.setState({ topic: value, clik: value });
    if (value.charAt(0) == "/") {
      SearchTopicVariables.variables.prefix = value;
      applloClient
        .query({
          query: SearchTopicMutation,
          ...SearchTopicVariables,
          fetchPolicy: "no-cache"
        })
        .then(res => {
          res.data.search.topics.forEach(async (element, index) => {
            await topicsearchData.push({ topic: element });
          });
          this.setState({ TopicList: fromJS(topicsearchData) });
        });
    } else if (value.charAt(0) == "#") {
      SearchClikVariables.variables.prefix = value;
      applloClient
        .query({
          query: SearchClikMutation,
          ...SearchClikVariables,
          fetchPolicy: "no-cache"
        })
        .then(res1 => {
          res1.data.search.cliks.forEach(async (element, index) => {
            await cliksearchData.push({ clik: element });
          });
          this.setState({ ClikList: fromJS(cliksearchData) });
        });
    } else if (value == "") {
      this.setState({ TopicList: [], ClikList: [] });
    } else {
      SearchTopicVariables.variables.prefix = value;
      applloClient
        .query({
          query: SearchTopicMutation,
          ...SearchTopicVariables,
          fetchPolicy: "no-cache"
        })
        .then(res => {
          res.data.search.topics.forEach(async (element, index) => {
            await topicsearchData.push({ topic: element });
          });
          this.setState({ TopicList: fromJS(topicsearchData) });
        });
      SearchClikVariables.variables.prefix = value;
      applloClient
        .query({
          query: SearchClikMutation,
          ...SearchClikVariables,
          fetchPolicy: "no-cache"
        })
        .then(res1 => {
          res1.data.search.cliks.forEach(async (element, index) => {
            await cliksearchData.push({ clik: element });
          });
          this.setState({ ClikList: fromJS(cliksearchData) });
        });
    }
  };

  handleClikSelectInput = clik => {
    if (this.state.selectedCliks.length < 3) {
      let index = this.state.selectedCliks.findIndex(i => i.name == clik);
      if (index != -1) {
        alert("clik name already selected");
      } else {
        this.setState({
          selectedCliks: this.state.selectedCliks.concat([{ name: clik }]),
          clik: "",
          topic: "",
          ClikList: [],
          TopicList: []
        });
      }
    } else {
      alert("You can only choose Maximum 3 Cliks to Tag");
      this.setState({
        clik: ""
      });
    }
  };

  handleClikDelete = index => {
    let tagsSelected = this.state.selectedCliks;
    tagsSelected.splice(index, 1);
    this.setState({ selectedCliks: tagsSelected });
  };

  customRenderClikTags = tags => {
    return (
      <View
        style={{
          flexDirection: "row",
          flexWrap: "wrap",
          alignItems: "flex-start",
          backgroundColor: "#fff",
          width: "100%",
          padding: 5
        }}
      >
        {this.state.selectedCliks.map((t, i) => {
          return (
            <View
              key={t.name}
              style={{
                flexDirection: "row",
                backgroundColor: "#E8F5FA",
                justifyContent: "center",
                alignItems: "center",
                height: 30,
                marginLeft: 10,
                marginTop: 3,
                borderRadius: 10,
                padding: 3
              }}
            >
              <TouchableHighlight
                key={i}
                onPress={() => this.handleClikDelete(i)}
              >
                <Text
                  style={{
                    color: "#4169e1",
                    fontWeight: "bold",
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  #{t.name}
                </Text>
              </TouchableHighlight>
              <Icon
                color={"#4C82B6"}
                name="times"
                type="font-awesome"
                size={12}
                iconStyle={{ marginLeft: 5 }}
                onPress={() => this.handleClikDelete(i)}
              />
            </View>
          );
        })}
      </View>
    );
  };

  render() {
    const { changeBackPicEnable } = this.state;
    const textStyle = styles.usertext;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: ConstantColors.customeBackgroundColor,
          width: "100%",                    
          paddingHorizontal: Dimensions.get("window").width <= 1100 ? 0 : 10,
          // borderColor:'#c5c5c5',
          // borderWidth:1,
          // borderRadius:10,
          // marginTop:10
        }}
      >
        {Dimensions.get("window").width <= 750 && (
          <Animated.View
            style={{
              position: Platform.OS == "web" ? "sticky" : null,
              top: 0,
              left: 0,
              right: 0,
              zIndex: 10,
              overflow: "hidden",
              // borderRadius: 6
            }}
          >
            <View
              style={{
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <View
                style={{
                  width: "100%",
                  flexDirection: "row",
                  backgroundColor: "#000",
                  // borderRadius: 6,
                  height: 50
                }}
              >
                <TouchableOpacity
                  style={ButtonStyle.headerBackStyle}
                  onPress={() => {
                    let nav = this.props.navigation.dangerouslyGetParent()
                      .state;
                    if (nav.routes.length > 1) {
                      this.props.navigation.goBack();
                      return;
                    } else {
                      this.props.navigation.navigate("home");
                    }
                  }}
                >
                  <Icon
                    color={"#fff"}
                    name="angle-left"
                    type="font-awesome"
                    size={40}
                  />
                </TouchableOpacity>

                {
                !this.props.getsearchBarStatus &&
                  <TouchableOpacity
                    style={[ButtonStyle.headerTitleStyle, { backgroundColor: "#000" }]}
                  >
                    <Text
                      style={{
                        color: "#fff",
                        textAlign: "center",
                        fontWeight: "bold",
                        fontSize: 18,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Create Post
                  </Text>
                  </TouchableOpacity>}
                {
                // !this.props.getsearchBarStatus &&
                  <View
                    style={ButtonStyle.headerRightStyle}
                  >
                    <HeaderRight navigation={this.props.navigation} />
                  </View>
                }
              </View>

            </View>
          </Animated.View>
        )
          // : null
          // <TouchableOpacity
          //   style={{
          //     width: "100%",
          //     justifyContent: "center",
          //     borderRadius: 6,
          //     height: 40
          //   }}
          // >
          //   <Text
          //     style={{
          //       color: "black",
          //       textAlign: "center",
          //       fontWeight: "bold",
          //       fontSize: 18,
          //       fontFamily: ConstantFontFamily.MontserratBoldFont
          //     }}
          //   >
          //     Create Post
          //   </Text>
          // </TouchableOpacity>
        }
        <ScrollView
          ref={scrollview => {
            this.Pagescrollview = scrollview;
          }}
          showsVerticalScrollIndicator={false}
          onLayout={event => {
            let { x, y, width, height } = event.nativeEvent.layout;
            if (width < 1024) {
              this.setState({
                showsVerticalScrollIndicatorView: true,
                currentScreentWidth: width
              });
            } else {
              this.setState({
                showsVerticalScrollIndicatorView: false,
                currentScreentWidth: width
              });
            }
          }}
          style={[
            // ButtonStyle.borderStyle
            {
              height:
                Platform.OS !== "web"
                  ? null
                  : Dimensions.get("window").height - 80,
              paddingHorizontal: 10,
              marginLeft: 2,
              backgroundColor: "#fff",
              // marginTop: Dimensions.get("window").width >= 1100 ? 0 : 10,
              // marginHorizontal: Dimensions.get("window").width <= 1100 ? 10 : 0
            }
          ]}
        >
          {/* <Text
            style={{
              marginBottom: 10,
              fontSize: 16,
              fontWeight: "bold",
              fontFamily: ConstantFontFamily.MontserratBoldFont
            }}
          >
            Thumbnail Picture
          </Text>
          <View
            style={[ButtonStyle.shadowStyle, {
              height: Dimensions.get("window").height / 4,
              // borderColor: "#e1e1e1",
              // borderWidth: 1,
              borderRadius: Dimensions.get('window').width >= 750 ? 5 : 0,
            }]}
          >
            <ImageBackground
              style={styles.image}
              source={{
                uri:
                  changeBackPicEnable != null ? changeBackPicEnable.uri : null
              }}
              resizeMode={"cover"}
            >
              <Icon
                color={"#000"}
                iconStyle={{
                  color: "#fff",
                  justifyContent: "center",
                  alignItems: "center"
                }}
                reverse
                name="camera"
                type="font-awesome"
                size={20}
                containerStyle={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                  flex: 1
                }}
                onPress={this._topPickBannerImageSingle}
              />
            </ImageBackground>
          </View>
          {this.state.showSlider && (
            <View style={{ alignItems: "stretch", justifyContent: "center" }}>
              <Slider
                value={this.state.value}
                maximumValue={100}
                minimumValue={0}
                onValueChange={value => this.changeBrightness(value)}
              />
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Text style={{ color: "#000", fontFamily: ConstantFontFamily.defaultFont }}>
                  {" "}
                  Slide to change the brightness{" "}
                </Text>
              </View>
            </View>
          )} */}

          {/* {this.state.url == "" ? (
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                marginVertical: 5
              }}
            >
              {this.props.profileData
                .getIn(["my_users", "0", "user"])
                .getIn(["profile_pic"]) ? (
                <Image
                  source={{
                    uri: this.props.profileData
                      .getIn(["my_users", "0", "user"])
                      .getIn(["profile_pic"])
                      ? this.props.profileData
                          .getIn(["my_users", "0", "user"])
                          .getIn(["profile_pic"])
                      : null
                  }}
                  style={{
                    width: 40,
                    height: 40,
                    padding: 0,
                    marginRight: 5,
                    borderRadius: 20
                  }}
                />
              ) : (
                <Image
                  source={require("../assets/image/default-image.png")}
                  style={{
                    width: 40,
                    height: 40,
                    padding: 0,
                    margin: 5,
                    borderRadius: 20
                  }}
                />
              )}
              <View style={{ padding: 5 }}>
                {this.props.profileData
                  .getIn(["my_users", "0", "user"])
                  .getIn(["first_name"]) ||
                this.props.profileData
                  .getIn(["my_users", "0", "user"])
                  .getIn(["last_name"]) ? (
                  <Text
                    style={{
                      color: "#000",
                      fontWeight: "bold",
                      fontSize: 15,
                      fontFamily: ConstantFontFamily.defaultFont
                    }}
                  >
                    {" "}
                    {this.props.profileData
                      .getIn(["my_users", "0", "user"])
                      .getIn(["first_name"])}{" "}
                    {this.props.profileData
                      .getIn(["my_users", "0", "user"])
                      .getIn(["last_name"])}
                  </Text>
                ) : null}
                <Text
                  style={{
                    color: "#000",
                    fontSize: 13,
                    fontFamily: ConstantFontFamily.defaultFont
                  }}
                >
                  {" "}
                  @
                  {this.props.profileData
                    .getIn(["my_users", "0", "user"])
                    .getIn(["username"])}
                </Text>
              </View>
            </View>
          ) : null} */}

          {this.state.url != "" && (
            <View>
              <Text
                style={{
                  fontWeight: "bold",
                  marginTop: 10,
                  fontSize: 16,
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
              >
                URL
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  padding: 5
                }}
              >
                <View
                  style={{
                    justifyContent: "flex-start",
                    alignContent: "center",
                    justifyContent: "center",
                    flexDirection: "row"
                  }}
                >
                  <View
                    style={{
                      alignContent: "center",
                      justifyContent: "center"
                    }}
                  >
                    {this.state.url != "" ? (
                      <Icon
                        name="link"
                        type="font-awesome"
                        color="#000"
                        size={20}
                      />
                    ) : null}
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      this.state.url ? this.openWindow(this.state.url) : null
                    }
                  >
                    <Text
                      style={{
                        alignSelf: "center",
                        color: "#000",
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.defaultFont,
                        margin: 10
                      }}
                    >
                      {this.state.url.length > 30
                        ? this.state.url
                          .replace("http://", "")
                          .replace("HTTP://", "")
                          .replace("https://", "")
                          .replace("HTTPS://", "")
                          .replace("www.", "")
                          .replace("WWW.", "")
                          .replace(/(^\w+:|^)\/\//, "")
                          .substring(0, 30) + "..."
                        : this.state.url}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
          <View style={{ flexDirection: "row", width: "100%" }}>
            <View
              style={{
                width: "70%",
                justifyContent: "flex-start",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontWeight: "bold",
                  marginTop: 10,
                  marginRight: 20,
                  fontSize: 16,
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
              >
                Title
              </Text>
            </View>
            <View
              style={{
                width: "30%",
                justifyContent: "flex-end",
                alignItems: "flex-end",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  color:
                    this.state.title.length < 10 ||
                      this.state.title.length > 150
                      ? "#de5246"
                      : "#009B1A",
                  fontSize: 13,
                  fontFamily: ConstantFontFamily.defaultFont,
                  marginTop: 2,
                  marginRight: 3
                }}
              >
                {this.state.title.length < 10
                  ? 10 - this.state.title.length + " characters more"
                  : this.state.title.length > 10 &&
                  150 - this.state.title.length + " characters left"}
              </Text>
              {(this.state.title.length < 10 ||
                this.state.title.length > 150) && (
                  <TouchableOpacity
                    onMouseEnter={() => this.setState({ titleHover: true })}
                    onMouseLeave={() => this.setState({ titleHover: false })}
                  >
                    <Icon
                      color={"#f80403"}
                      iconStyle={{
                        marginTop: 10,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      name="times"
                      type="font-awesome"
                      size={16}
                    />
                  </TouchableOpacity>
                )}
              {this.state.titleHover == true && Platform.OS == "web" ? (
                <Tooltip
                  backgroundColor={"#d3d3d3"}
                  withPointer={false}
                  withOverlay={false}
                  toggleOnPress={true}
                  containerStyle={{
                    left: -150,
                    top: -50
                  }}
                  popover={<Text>Between 10 to 150 Characters</Text>}
                />
              ) : null}
            </View>
          </View>
          <TextInput
            value={this.state.title}
            multiline={true}
            numberOfLines={2}
            maxLength={150}
            placeholder={
              this.props.link.get("withoutUrl")
                ? "What are we discussing today?"
                : "Enter a title."
            }
            placeholderTextColor="#6D757F"
            style={[
              textStyle,
              this.state.focusTitle ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
              {
                width: "100%",
                borderRadius: 5,
                // borderColor: "#e1e1e1",
                // borderWidth: 1,
                marginTop: 5,
                paddingHorizontal: 5,
                paddingTop:10,
                height: Platform.OS == "ios" ? 100 : 40,
                outline: 'none',
                textAlignVertical:'center'
              }
            ]}
            onChangeText={title => {
              this.setState({ title }),
                this.props.postlink({
                  description: this.state.summary,
                  image: this.state.changeBackPicEnable.uri,
                  title: title,
                  url: this.state.url,
                  withoutUrl: this.props.link.get("withoutUrl")
                });
            }}
            onFocus = {()=>this.setState({focusTitle : true})}
            onBlur = {()=> this.setState({focusTitle: false})}
          />
          <View style={{ flexDirection: "row", width: "100%" }}>
            <View
              style={{
                width: "70%",
                justifyContent: "flex-start",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  fontWeight: "bold",
                  marginTop: 15,
                  marginRight: 20,
                  fontSize: 16,
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
              >
                Summary
              </Text>
            </View>
            <View
              style={{
                width: "30%",
                justifyContent: "flex-end",
                alignItems: "flex-end",
                flexDirection: "row"
              }}
            >
              <Text
                style={{
                  color:
                    this.state.summary.length < 50 ||
                      this.state.summary.length > 300
                      ? "#de5246"
                      : "#009B1A",
                  fontSize: 13,
                  fontFamily: ConstantFontFamily.defaultFont,
                  marginTop: 2,
                  marginRight: 3
                }}
              >
                {this.state.summary.length < 50
                  ? 50 - this.state.summary.length + " characters more"
                  : this.state.summary.length > 50 &&
                  300 - this.state.summary.length + " characters left"}
              </Text>
              {(this.state.summary.length < 50 ||
                this.state.summary.length > 300) && (
                  <TouchableOpacity
                    onMouseEnter={() => this.setState({ summaryHover: true })}
                    onMouseLeave={() => this.setState({ summaryHover: false })}
                  >
                    <Icon
                      color={"#f80403"}
                      iconStyle={{
                        marginTop: 10,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      name="times"
                      type="font-awesome"
                      size={16}
                    />
                  </TouchableOpacity>
                )}
              {this.state.summaryHover == true && Platform.OS == "web" ? (
                <Tooltip
                  backgroundColor={"#d3d3d3"}
                  withPointer={false}
                  withOverlay={false}
                  toggleOnPress={true}
                  containerStyle={{
                    left: -150,
                    top: -50
                  }}
                  popover={<Text>Between 50 to 300 Characters</Text>}
                />
              ) : null}
            </View>
          </View>
          <TextInput
            value={this.state.summary}
            multiline={true}
            numberOfLines={5}
            maxLength={300}
            placeholder={
              this.props.link.get("withoutUrl")
                ? "Only write about this discussion's context here. Create a separate comment on the next screen for analysis, opinions, or anecdotes."
                : "Enter a concise summary."
            }
            placeholderTextColor="#6D757F"
            style={[
              textStyle, 
              this.state.focusSummary ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
              {
                width: "100%",
                borderRadius: 5,
                // borderColor: "#e1e1e1",
                // borderWidth: 1,
                marginTop: 5,
                paddingHorizontal: 5,
                paddingTop:10,
                height: Platform.OS == "ios" ? 100 : null,
                outline: 'none'
              }
            ]}
            onChangeText={summary => {
              this.props.postlink({
                description: summary,
                image: this.state.changeBackPicEnable.uri,
                title: this.state.title,
                url: this.state.url,
                withoutUrl: this.props.link.get("withoutUrl")
              });
              this.setState({ summary });
            }}
            onFocus = {()=>this.setState({focusSummary : true})}
            onBlur = {()=> this.setState({focusSummary: false})}
          />
          {/* {this.state.url == "" && (
            <View style={{ flexDirection: "row", width: "100%" }}>
              <View
                style={{
                  width: "70%",
                  justifyContent: "flex-start",
                  flexDirection: "row"
                }}
              >
                <Text
                  style={{
                    fontWeight: "bold",
                    marginTop: 10,
                    marginRight: 20,
                    fontSize: 16,
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  Body
                </Text>
              </View>
            </View>
          )}
          {this.state.url == "" && (
            <TextInput
              value={this.state.text}
              multiline={true}
              numberOfLines={4}
              placeholder="Write a thought provoking piece...(Optional)"
              placeholderTextColor="#6D757F"
              style={[
                textStyle,
                {
                  width: "100%",
                  borderColor: "#e1e1e1",
                  borderWidth: 1,
                  borderRadius: Platform.OS == "web" ? 6 : 0,
                  color: "#000",
                  marginTop: 5,
                  padding: 5,
                  height: Platform.OS == "ios" ? 100 : null
                }
              ]}
              onChangeText={text => this.setState({ text })}
            />
          )} */}
          <View
            style={{ flexDirection: "row", justifyContent: "space-between", alignItems:'flex-end' }}
          >
            <Text
              style={{
                fontWeight: "bold",
                marginTop: 15,
                // marginBottom:5,
                marginRight: 10,
                fontSize: 16,
                fontFamily: ConstantFontFamily.MontserratBoldFont
              }}
            >
              Share To
            </Text>

            {this.state.selectedTopics.length == 0 &&
              this.state.selectedCliks.length == 0 && (
                <TouchableOpacity>
                  <Icon
                    color={"#f80403"}
                    iconStyle={{
                      marginTop: 10,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    name="times"
                    type="font-awesome"
                    size={16}
                  />
                </TouchableOpacity>
              )}
          </View>
          <View style={{ width: "100%" }}>
            <View
              style={{
                width: "100%",
                flexDirection: "row"
              }}
            >
              <View
                style={ {
                  // padding: 10,
                  // marginVertical: 10,
                  width: "100%",
                  // borderColor: "#e1e1e1",
                  // borderWidth: 1,
                  borderRadius: Dimensions.get('window').width >= 750 ? 5 : 0
                }}
              >
                <View
                  style={{
                    // marginVertical: 5
                  }}
                >
                  {this.state.selectedTopics.length > 0 &&
                    this.customRenderTopicTags()}

                  {this.state.selectedCliks.length > 0 &&
                    this.customRenderClikTags()}
                </View>
                <View
                  style={[this.state.focusTopic ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle ,
                    {
                    width: "100%",
                    height: 40,
                    justifyContent: "flex-start",
                    flexDirection: "row",
                    alignItems: "center",
                    // borderColor: "#e1e1e1",
                    // borderWidth: 1,
                    borderRadius: 5
                  }]}
                >                  
                  <View style={{ width: "5%", marginRight: "auto" }}>
                    <Icon name="search" size={18} type="font-awesome" />
                  </View>
                  <TextInput
                    value={this.state.topic}
                    autoFocus={false}
                    placeholder="share to /topics #clicks"
                    onChangeText={topic =>
                      // this.setState({ topic: topic, clik: topic }, () =>
                      //   this.customRenderTopicSuggestion(topic)
                      // )
                      this.customRenderTopicSuggestion(topic)
                    }
                    style={{                    
                      width: "95%",
                      //border: "none",
                      outline: "none",
                      fontFamily: ConstantFontFamily.defaultFont
                    }}
                    onFocus = {()=>this.setState({focusTopic : true})}
                    onBlur = {()=> this.setState({focusTopic: false})}
                  />
                </View>
                {this.state.topic.length > 0 && (
                  <ScrollView
                    nestedScrollEnabled
                    style={{ maxHeight: 200 }}
                    showsVerticalScrollIndicator={false}
                  >
                    {this.state.TopicList.map((item, index) => {
                      return (
                        <View
                          key={item.name}
                          style={{
                            backgroundColor: "#FEFEFA",
                            width: "100%",
                            padding: 5
                          }}
                        >
                          <View
                            style={{
                              padding: 5,
                              backgroundColor: item.getIn(["topic", "parents"])
                                ? "#e3f9d5"
                                : "#e3f9d5",
                              borderRadius: 10,
                              alignSelf: "flex-start",
                              alignItems: "center"
                            }}
                          >
                            <Text
                              style={{
                                color: item.getIn(["topic", "parents"])
                                  ? "#009B1A"
                                  : "#009B1A",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                fontWeight: "bold"
                              }}
                              onPress={() =>
                                this.handleTopicSelectInput(
                                  item.getIn(["topic", "name"])
                                )
                              }
                            >
                              /{item.getIn(["topic", "name"]).toLowerCase()}
                            </Text>
                          </View>
                        </View>
                      );
                    })}
                    {this.state.ClikList.map((item, index) => {
                      let indexx = this.props.getUserFollowCliksList.findIndex(
                        i =>
                          i.getIn(["clik", "name"]) ==
                          item.getIn(["clik", "name"])
                      );
                      if (
                        indexx != -1 &&
                        (this.props.getUserFollowCliksList.getIn([
                          index,
                          "member_type"
                        ]) == "SUPER_ADMIN" ||
                          this.props.getUserFollowCliksList.getIn([
                            index,
                            "member_type"
                          ]) == "ADMIN" ||
                          this.props.getUserFollowCliksList.getIn([
                            index,
                            "member_type"
                          ]) == "MEMBER")
                      ) {
                        return (
                          <View
                            key={item.name}
                            style={{
                              backgroundColor: "#FEFEFA",
                              width: "100%",
                              padding: 5
                            }}
                          >
                            <View
                              style={{
                                padding: 5,
                                backgroundColor: "#E8F5FA",
                                borderRadius: 10,
                                alignSelf: "flex-start",
                                alignItems: "center"
                              }}
                            >
                              <Text
                                style={{
                                  color: "#4169e1",
                                  fontFamily: ConstantFontFamily.defaultFont,
                                  fontWeight: "bold"
                                }}
                                onPress={() =>
                                  this.handleClikSelectInput(
                                    item.getIn(["clik", "name"])
                                  )
                                }
                              >
                                #{item.getIn(["clik", "name"])}
                              </Text>
                            </View>
                          </View>
                        );
                      }
                    })}
                  </ScrollView>
                )}

                <Text
                  style={{
                    fontWeight: "bold",
                    marginTop: 10,
                    marginRight: 10,
                    fontSize: 16,
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  Quick Access
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    // paddingHorizontal: "10%",
                    justifyContent: "space-between"
                  }}
                >
                  <View style={{ marginVertical: 10, maxHeight: 350 }}>
                    {this.props.getUserFollowTopicList
                      .slice(0, 5)
                      .map((item, i) => {
                        return (
                          <TouchableOpacity
                            onPress={() =>
                              this.handleTopicSelectInput(
                                item.getIn(["topic", "name"])
                              )
                            }
                            style={{
                              marginTop: 10,
                              marginLeft: 5,
                              alignSelf: "center",
                              padding: 5,
                              backgroundColor: item.getIn(["topic", "parents"])
                                ? "#e3f9d5"
                                : "#e3f9d5",
                              borderRadius: 10
                            }}
                            key={i}
                          >
                            <Hoverable>
                              {isHovered => (
                                <Text
                                  style={{
                                    width: "100%",
                                    color: item.getIn(["topic", "parents"])
                                      ? "#009B1A"
                                      : "#009B1A",
                                    fontSize: 15,
                                    fontWeight: "bold",
                                    fontFamily:
                                      ConstantFontFamily.MontserratBoldFont,
                                    textDecorationLine:
                                      isHovered == true ? "underline" : "none"
                                  }}
                                >
                                  /{item.getIn(["topic", "name"]).toLowerCase()}
                                </Text>
                              )}
                            </Hoverable>
                          </TouchableOpacity>
                        );
                      })}
                  </View>
                  <View style={{ marginVertical: 10, maxHeight: 350 }}>
                    {this.props.getUserFollowCliksList
                      .slice(0, 5)
                      .map((item, i) => {
                        return (
                          <TouchableOpacity
                            key={i}
                            onPress={() =>
                              this.handleClikSelectInput(
                                item.getIn(["clik", "name"])
                              )
                            }
                            style={{
                              marginLeft: 5,
                              margin: 5,
                              alignSelf: "center",
                              padding: 5,
                              backgroundColor: "#E8F5FA",
                              borderRadius: 10,
                              maxWidth: "100%"
                            }}
                          >
                            <Hoverable>
                              {isHovered => (
                                <Text
                                  style={{
                                    width: "100%",
                                    textAlign: "left",
                                    color: "#4169e1",
                                    fontSize: 15,
                                    fontWeight: "bold",
                                    fontFamily:
                                      ConstantFontFamily.MontserratBoldFont,
                                    textDecorationLine:
                                      isHovered == true ? "underline" : "none"
                                  }}
                                >
                                  #{item.getIn(["clik", "name"])}
                                </Text>
                              )}
                            </Hoverable>
                          </TouchableOpacity>
                        );
                      })}
                  </View>
                </View>
              </View>
            </View>
          </View>
          {/* <Text
            style={{
              textAlign: "right",
              fontSize: 14,
              fontFamily: ConstantFontFamily.MontserratBoldFont
            }}
          >
            Can't find a topic?&nbsp;
            <Text
              style={{
                textAlign: "right",
                fontSize: 14,
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                textDecorationLine: "underline"
              }}
              onPress={() => this.props.navigation.navigate("createtopic")}
            >
              Create
            </Text>
            <Text
              style={{
                textAlign: "right",
                fontSize: 14,
                fontFamily: ConstantFontFamily.MontserratBoldFont
              }}
              onPress={() => this.props.navigation.navigate("createtopic")}
            >
              &nbsp;one
            </Text>
          </Text> */}

          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              marginBottom: 10
            }}
          >
            {this.state.loading == true && (
              <ActivityIndicator animating size="large" color="#000" />
            )}
            <Button
              title="Submit"
              titleStyle={ButtonStyle.wtitleStyle}
              buttonStyle={ButtonStyle.gbackgroundStyle}
              containerStyle={ButtonStyle.containerStyle}
              disabled={
                this.state.title.length < 10 ||
                  this.state.title.length > 150 ||
                  this.state.summary.length < 50 ||
                  this.state.summary.length > 300 ||
                  (this.state.selectedTopics.length == 0 &&
                    this.state.selectedCliks.length == 0)
                  ? true
                  : false
              }
              onPress={this.onSave}
            />
          </View>
        </ScrollView>
      </View >
    );
  }
}

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  listTrending_cliks: !state.TrendingCliksReducer.getIn(["Trending_cliks_List"])
    ? List()
    : state.TrendingCliksReducer.getIn(["Trending_cliks_List"]),
  link: state.LinkPostReducer.get("link"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List(),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getUserFollowTopicList: state.LoginUserDetailsReducer.get(
    "userFollowTopicsList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowTopicsList")
    : List(),
  getsearchBarStatus: state.AdminReducer.get("searchBarOpenStatus"),
});

const mapDispatchToProps = dispatch => ({
  postlink: payload => dispatch(postLink(payload)),
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload)),
  searchOpenBarStatus: payload => dispatch({ type: "SEARCHBAR_STATUS", payload })
});

const CreatePostScreenContainerWrapper = graphql(PostCreateMutation, {
  name: "PostCreate"
})(CreatePostScreen);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  CreatePostScreenContainerWrapper
);

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: "100%"
  },
  usertext: {
    color: "#000",
    fontSize: 14,
    // fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  }
});
