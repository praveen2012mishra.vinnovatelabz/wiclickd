import React, { Component } from "react";
import {
  Animated,
  Dimensions,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { Icon } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";
import { connect } from "react-redux";
import { compose } from "recompose";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import ButtonStyle from "../constants/ButtonStyle";
import { ScrollPager, TabBar, TabView } from "react-native-tab-view";
import ShadowSkeleton from "../components/ShadowSkeleton";
import AnalyticsGraph from "../components/AnalyticsGraph";
import StripePayment from "../components/StripePayment";
import HeaderRight from "../components/HeaderRight";
import BottomScreenAnalytics from '../components/BottomScreenAnalytics';
import NavigationService from "../library/NavigationService";

const initRoutes = [
  {
    key: "first",
    title: "Summary",
    icon: "bars",
    type: "font-awesome"
  },
  { key: "second", title: "Graphs", icon: "bar-chart", type: "font-awesome" },
  {
    key: "third",
    title: "Top Comments",
    icon: "comment-o",
    type: "font-awesome"
  },
  { key: "fourth", title: "Stripe", icon: "dollar", type: "font-awesome" }
];

class AnalyticsScreen extends Component {
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.Pagescrollview = null;
    this.state = {
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      durationDropdownItems: [
        {
          label: "Current Month",
          value: "Current Month"
        }
      ],
      index: 0,
      routes: [...initRoutes],
      showInfoText: false,
      key: {
        label: "Summary",
        value: "Summary",
      }
    };
    this.changeBannerImage = "";
    this.baseState = this.state;
  }

  onClose = () => {
    this.props.setEarningModalStatus(false);
  };

  _renderLazyPlaceholder = ({ route }) => <ShadowSkeleton />;

  _handleIndexChange = index => {
    this.setState({ index });
  };

  _renderScene = ({ route }) => {
    switch (route.key) {
      case "fourth":
        return <StripePayment />;
      case "third":
        return (
          <View
            style={[
              ButtonStyle.shadowStyle,
              {
                borderRadius: 5,
                marginTop: 10,
                paddingVertical: 10,
                marginHorizontal: Dimensions.get('window').width <= 1100 ? 5 : 5,
                backgroundColor: "#fff",
                borderRadius:10,
                // marginLeft: 2,
                width: '99%'
              }
            ]}
          >
            <View style={[{ margin: 5, paddingHorizontal: 20 }]}>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: ConstantFontFamily.defaultFont,
                  fontWeight: "bold",
                  textAlign: "center",
                  paddingHorizontal: 20
                }}
              >
                Post: Section 1.10.32 of "de Finibus Bonorum et Malorum",
                written by Cicero in 45 BC
              </Text>
            </View>
            <View style={{ margin: 10 }}>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: ConstantFontFamily.defaultFont
                }}
              >
                Comment: Lorem ipsum dolor sit amet, consectetur adipiscing
                elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                aute irure dolor in reprehenderit in voluptate velit esse cillum
                dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa qui officia deserunt
                mollit anim id est laborum
              </Text>
            </View>

            <View
              style={{
                marginHorizontal: 10,
                flexDirection: "row",
                width: 'auto'
              }}
            >
              <Text
                style={{
                  width: "70%",
                  fontWeight: "bold",
                  color: "#009B1A",
                  fontSize: 14,
                  fontFamily: ConstantFontFamily.defaultFont
                }}
              >
                $215.08
              </Text>

              <View
                style={{
                  width:Dimensions.get('window').width >= 1100 && "30%",
                  flexDirection: "row",
                  justifyContent: "flex-end"
                }}
              >
                <View style={{ flexDirection: "row" }}>
                  <Text
                    style={{
                      fontFamily: ConstantFontFamily.defaultFont,
                      fontWeight: "bold",
                      paddingRight: 5,
                      marginTop: 4
                    }}
                  >
                    100
                  </Text>
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      padding: 2,
                      justifyContent: "center",
                      alignItems: "center",
                      color: "red",
                      paddingRight: 10
                    }}
                    name="heart"
                    type="font-awesome"
                    size={20}
                  />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text
                    style={{
                      fontFamily: ConstantFontFamily.defaultFont,
                      fontWeight: "bold",
                      paddingRight: 5,
                      marginTop: 4
                    }}
                  >
                    75
                  </Text>
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      padding: 2,
                      justifyContent: "center",
                      alignItems: "center",
                      color: "grey",
                      paddingRight: 10
                    }}
                    name="heart"
                    type="font-awesome"
                    size={20}
                  />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text
                    style={{
                      fontFamily: ConstantFontFamily.defaultFont,
                      fontWeight: "bold",
                      paddingRight: 5,
                      marginTop: 4
                    }}
                  >
                    25
                  </Text>
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      padding: 2,
                      justifyContent: "center",
                      alignItems: "center",
                      color: "#fff44f",
                      paddingRight: 10
                    }}
                    name="heart"
                    type="font-awesome"
                    size={20}
                  />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text
                    style={{
                      fontFamily: ConstantFontFamily.defaultFont,
                      fontWeight: "bold",
                      paddingRight: 5,
                      marginTop: 4
                    }}
                  >
                    10
                  </Text>

                  <Icon
                    color={"#000"}
                    iconStyle={{
                      padding: 2,
                      justifyContent: "center",
                      alignItems: "center",
                      color: "#4169e1",
                      paddingRight: 10
                    }}
                    name="heart"
                    type="font-awesome"
                    size={20}
                  />
                </View>
              </View>
            </View>
          </View>
        );
      case "second":
        return <AnalyticsGraph />;
      case "first":
        return (
          <>
            <View style={{ alignItems: 'center' }}>
              <View
                style={[
                  ButtonStyle.shadowStyle,
                  {
                    marginTop: 10,
                    paddingVertical: 10,
                    backgroundColor: "#fff",
                    borderRadius: 10,
                    // marginLeft: 2,
                    width: Dimensions.get('window').width <= 1100 ? '97%' : '99%',
                    paddingHorizontal: Dimensions.get('window').width <= 1100 && 5,
                    marginHorizontal: Dimensions.get('window').width <= 1100 ? 5 : 2,
                  }
                ]}
              >
                <View style={{ flex: 1, alignItems: "center", marginBottom: 20 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      fontSize: 16
                    }}
                  >
                    Current Month
                </Text>
                </View>

                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14
                        }}
                      >
                        Estimated Earnings{" "}
                      </Text>
                      <Icon
                        color={"#000"}
                        name="info-circle"
                        type="font-awesome"
                        size={18}
                        onPress={() => {
                          this.props.setEarningModalStatus(true);
                        }}
                      />
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      $518
                  </Text>
                  </View>
                </View>

                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      Coments Written
                  </Text>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      20
                  </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 5
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      alignItems: "center"
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "red"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>

                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      100
                  </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 5
                  }}
                >
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "grey"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      75
                  </Text>
                  </View>
                </View>
                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "#fff44f"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      25
                  </Text>
                  </View>
                </View>
                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "#4169e1"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      10
                  </Text>
                  </View>
                </View>
              </View>

              <View
                style={[
                  ButtonStyle.shadowStyle,
                  {
                    marginTop: 10,
                    paddingVertical: 10,
                    backgroundColor: "#fff",
                    paddingHorizontal: Dimensions.get('window').width <= 1100 && 5,
                    marginHorizontal: Dimensions.get('window').width <= 1100 ? 5 : 2,
                    borderRadius: 10,
                    width: Dimensions.get('window').width <= 1100 ? '97%' : '99%',
                  }
                ]}
              >
                <View style={{ flex: 1, alignItems: "center", marginBottom: 20 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      fontSize: 16
                    }}
                  >
                    Last Month
                </Text>
                </View>

                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      Earnings
                  </Text>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      $518
                  </Text>
                  </View>
                </View>

                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      Coments Written
                  </Text>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      20
                  </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 5
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      alignItems: "center"
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "red"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>

                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      100
                  </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 5
                  }}
                >
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "grey"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      75
                  </Text>
                  </View>
                </View>
                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "#fff44f"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      25
                  </Text>
                  </View>
                </View>
                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "#4169e1"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      10
                  </Text>
                  </View>
                </View>
              </View>
            </View>
          </>
        );
    }
  };

  renderTabViewForMobile = ( route ) => {
    switch (route.value) {
      case "Stripe":
        return <StripePayment />;
      case "Top Comments":
        return (
          <View
            style={[
              ButtonStyle.shadowStyle,
              {
                borderRadius: 5,
                //marginTop: 10,
                //paddingVertical: 10,
                //marginHorizontal: Dimensions.get('window').width <= 1100 ? 5 : 5,
                backgroundColor: "#fff",
                // borderRadius:5,
                // marginLeft: 2,
                width: '100%'
              }
            ]}
          >
            <View style={[{ margin: 5, paddingHorizontal: 20 }]}>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: ConstantFontFamily.defaultFont,
                  fontWeight: "bold",
                  textAlign: "center",
                  paddingHorizontal: 20
                }}
              >
                Post: Section 1.10.32 of "de Finibus Bonorum et Malorum",
                written by Cicero in 45 BC
              </Text>
            </View>
            <View style={{ margin: 10 }}>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: ConstantFontFamily.defaultFont
                }}
              >
                Comment: Lorem ipsum dolor sit amet, consectetur adipiscing
                elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                aute irure dolor in reprehenderit in voluptate velit esse cillum
                dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa qui officia deserunt
                mollit anim id est laborum
              </Text>
            </View>

            <View
              style={{
                paddingHorizontal: 10,
                flexDirection: "row",
                width: "100%"
              }}
            >
              <Text
                style={{
                  width: "70%",
                  fontWeight: "bold",
                  color: "#009B1A",
                  fontSize: 14,
                  fontFamily: ConstantFontFamily.defaultFont
                }}
              >
                $215.08
              </Text>

              <View
                style={{
                  //width: "50%",
                  flexDirection: "row",
                  justifyContent: "flex-end"
                }}
              >
                <View style={{ flexDirection: "row" }}>
                  <Text
                    style={{
                      fontFamily: ConstantFontFamily.defaultFont,
                      fontWeight: "bold",
                      paddingRight: 5,
                      marginTop: 4
                    }}
                  >
                    100
                  </Text>
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      padding: 2,
                      justifyContent: "center",
                      alignItems: "center",
                      color: "red",
                      paddingRight: 10
                    }}
                    name="heart"
                    type="font-awesome"
                    size={20}
                  />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text
                    style={{
                      fontFamily: ConstantFontFamily.defaultFont,
                      fontWeight: "bold",
                      paddingRight: 5,
                      marginTop: 4
                    }}
                  >
                    75
                  </Text>
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      padding: 2,
                      justifyContent: "center",
                      alignItems: "center",
                      color: "grey",
                      paddingRight: 10
                    }}
                    name="heart"
                    type="font-awesome"
                    size={20}
                  />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text
                    style={{
                      fontFamily: ConstantFontFamily.defaultFont,
                      fontWeight: "bold",
                      paddingRight: 5,
                      marginTop: 4
                    }}
                  >
                    25
                  </Text>
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      padding: 2,
                      justifyContent: "center",
                      alignItems: "center",
                      color: "#fff44f",
                      paddingRight: 10
                    }}
                    name="heart"
                    type="font-awesome"
                    size={20}
                  />
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Text
                    style={{
                      fontFamily: ConstantFontFamily.defaultFont,
                      fontWeight: "bold",
                      paddingRight: 5,
                      marginTop: 4
                    }}
                  >
                    10
                  </Text>

                  <Icon
                    color={"#000"}
                    iconStyle={{
                      padding: 2,
                      justifyContent: "center",
                      alignItems: "center",
                      color: "#4169e1",
                      paddingRight: 10
                    }}
                    name="heart"
                    type="font-awesome"
                    size={20}
                  />
                </View>
              </View>
            </View>
          </View>
        );
      case "Graphs":
        return <AnalyticsGraph />;
      case "Summary":
        return (
          <>
            <View style={{ alignItems: 'center' }}>
              <View
                style={[
                  ButtonStyle.shadowStyle,
                  {
                    //marginTop: 10,
                    paddingVertical: 10,
                    backgroundColor: "#fff",
                    borderRadius: 5,
                    // marginLeft: 2,
                    width: Dimensions.get('window').width <= 1100 ? '100%' : '99%',
                    paddingHorizontal: Dimensions.get('window').width <= 1100 && 10,
                    //marginHorizontal: Dimensions.get('window').width <= 1100 ? 5 : 2,
                  }
                ]}
              >
                <View style={{ flex: 1, alignItems: "center", marginVertical: 10 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      fontSize: 16
                    }}
                  >
                    Current Month
                </Text>
                </View>

                <View style={{ flexDirection: "row", marginBottom: 10 }}>
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14
                        }}
                      >
                        Estimated Earnings{" "}
                      </Text>
                      <Icon
                        color={"#000"}
                        name="info-circle"
                        type="font-awesome"
                        size={18}
                        onPress={() => {
                          this.props.setEarningModalStatus(true);
                        }}
                      />
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      $518
                  </Text>
                  </View>
                </View>

                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      Coments Written
                  </Text>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      20
                  </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 5
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      alignItems: "center"
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "red"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>

                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      100
                  </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 5
                  }}
                >
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "grey"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      75
                  </Text>
                  </View>
                </View>
                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "#fff44f"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      25
                  </Text>
                  </View>
                </View>
                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "#4169e1"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      10
                  </Text>
                  </View>
                </View>
              </View>

              <View
                style={[
                  ButtonStyle.shadowStyle,
                  {
                    marginTop: 10,
                    paddingVertical: 10,
                    backgroundColor: "#fff",
                    paddingHorizontal: Dimensions.get('window').width <= 1100 && 5,
                    marginHorizontal: Dimensions.get('window').width <= 1100 ? 5 : 2,
                    borderRadius: 5,
                    width: Dimensions.get('window').width <= 1100 ? '100%' : '99%',
                  }
                ]}
              >
                <View style={{ flex: 1, alignItems: "center", marginBottom: 20 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      fontSize: 16
                    }}
                  >
                    Last Month
                </Text>
                </View>

                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      Earnings
                  </Text>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      $518
                  </Text>
                  </View>
                </View>

                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      Coments Written
                  </Text>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      20
                  </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 5
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      alignItems: "center"
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "red"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>

                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      100
                  </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 5
                  }}
                >
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "grey"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      75
                  </Text>
                  </View>
                </View>
                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "#fff44f"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      25
                  </Text>
                  </View>
                </View>
                <View style={{ flexDirection: "row", marginBottom: 5 }}>
                  <View style={{ flex: 1, alignItems: "center" }}>
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          padding: 2,
                          justifyContent: "center",
                          alignItems: "center",
                          color: "#4169e1"
                        }}
                        name="heart"
                        type="font-awesome"
                        size={20}
                      />
                      <Text
                        style={{
                          fontFamily: ConstantFontFamily.defaultFont,
                          fontSize: 14,
                          alignSelf: "center"
                        }}
                      >
                        {" "}
                      Received
                    </Text>
                    </View>
                  </View>
                  <View
                    style={{ flex: 1, alignItems: "center", alignSelf: "center" }}
                  >
                    <Text
                      style={{
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 14
                      }}
                    >
                      10
                  </Text>
                  </View>
                </View>
              </View>
            </View>
          </>
        );
    }
  };

  _renderTabBar = props => (
    Dimensions.get("window").width >= 750 &&
    <View
      style={[
        // ButtonStyle.borderStyle,
        {
          flexDirection: "row",
          width: "100%",
          //marginTop: -10,
          height: 30,
          backgroundColor:
            Dimensions.get("window").width <= 750 ? "#000" : "#fff",
          alignItems: "center",
          paddingHorizontal: 10,
          paddingBottom: Dimensions.get("window").width <= 1100 ? 0 : 10,
          marginTop: 10,
          // borderRadius: Dimensions.get("window").width <= 1100 ? 0 : 20
        }
      ]}
    >
      <TabBar
        {...props}
        indicatorStyle={{
          backgroundColor: "#009B1A",
          height: 3,
          borderRadius: 6,
          marginBottom: 10,
        }}
        style={{
          backgroundColor: "transparent",
          width: "100%",
          height: 60,
          shadowColor: "transparent"
        }}
        labelStyle={{
          color: "#000",
          fontFamily: ConstantFontFamily.MontserratBoldFont,
          fontSize: 13,
          fontWeight: "bold",
          height: 30,
          justifyContent: 'center'
        }}
        renderLabel={({ route, focused, color }) => (
          <Text
            style={{
              paddingTop: 10,
              color: focused ? "#009B1A" : "#D3D3D3",
              fontFamily: ConstantFontFamily.MontserratBoldFont
            }}
          >
            {route.title}
          </Text>
        )}
      // renderIcon={({ route, focused, color }) =>
      //   Dimensions.get("window").width >= 750 && (
      //     <Icon
      //       name={route.icon}
      //       type={route.type}
      //       color={focused ? "#009B1A" : "#D3D3D3"}
      //     />
      //   )
      // }
      //renderIndicator={({ route, focused, color }) => null}
      />
    </View>
  );

  render() {
    return (
      <>
        <View
          style={[
            // ButtonStyle.borderStyle,
            {
              flex: 1,
              backgroundColor: ConstantColors.customeBackgroundColor,
              width: "100%",
              height: '100%'
              // paddingTop: Platform.OS == "web" ? 10 : 5,

            }
          ]}
        >
          <View>
            {// Platform.OS !== "web" ? (
              Dimensions.get("window").width <= 750 ? (
                <Animated.View
                  style={{
                    position: Platform.OS == "web" ? "sticky" : null,
                    top: 0,
                    left: 0,
                    right: 0,
                    zIndex: 10,
                    overflow: "hidden",
                    // borderRadius: 6
                  }}
                >
                  <View
                    style={{
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <View
                      style={{
                        width: "100%",
                        flexDirection: "row",
                        backgroundColor: "#000",
                        // borderRadius: 6,
                        height: 42
                      }}
                    >
                      <TouchableOpacity
                        style={ButtonStyle.headerBackStyle}
                        onPress={() => {
                          let nav = this.props.navigation.dangerouslyGetParent()
                            .state;
                          if (nav.routes.length > 1) {
                            this.props.navigation.goBack();
                            return;
                          } else {
                            this.props.navigation.navigate("home");
                          }
                        }}
                      >
                        <Icon
                          color={"#fff"}
                          name="angle-left"
                          type="font-awesome"
                          size={40}
                        />
                      </TouchableOpacity>
                      {!this.props.getsearchBarStatus &&
                        <TouchableOpacity
                          style={[ButtonStyle.headerTitleStyle, { backgroundColor: '#000' }]}
                        >
                          <Text
                            style={{
                              color: "#fff",
                              textAlign: "center",
                              fontWeight: "bold",
                              fontSize: 18,
                              fontFamily: ConstantFontFamily.MontserratBoldFont
                            }}
                          >
                            Analytics
                    </Text>
                        </TouchableOpacity>
                      }
                      <View style={{ alignItems: "flex-end" }}>
                        <HeaderRight navigation={this.props.navigation} />
                      </View>
                    </View>
                  </View>
                </Animated.View>
              ) : // <TouchableOpacity
                //   style={{
                //     width: "100%",
                //     justifyContent: "center",
                //     borderRadius: 6,
                //     height: 40
                //   }}
                // >
                //   <Text
                //     style={{
                //       color: "black",
                //       textAlign: "center",
                //       fontWeight: "bold",
                //       fontSize: 18,
                //       fontFamily: ConstantFontFamily.MontserratBoldFont
                //     }}
                //   >
                //     Analytics
                //   </Text>
                // </TouchableOpacity>
                null}

            <View>
              {
                Dimensions.get('window').width <= 750 ?
                  // this.props.getTabView ?
                  <View
                    style={{
                      flex: 1,
                      width: "100%",
                      justifyContent:'flex-start',
                       padding: 10
                    }}
                  >                   
                    {this.renderTabViewForMobile(this.state.key)}
                  </View>
                  :
                  <View
                    style={{
                      flex: 1,
                      width: "100%",
                      justifyContent:'flex-start',
                       padding: 10
                    }}
                  >
                    <TabView
                      lazy
                      navigationState={this.state}
                      renderScene={this._renderScene}
                      renderLazyPlaceholder={this._renderLazyPlaceholder}
                      renderTabBar={this._renderTabBar}
                      onIndexChange={this._handleIndexChange}
                      removeClippedSubviews={true}
                    />
                  </View>
              }
            </View>
          </View>

        </View>
        {Dimensions.get("window").width <= 750 &&
          <BottomScreenAnalytics navigation={NavigationService} call={(id) => {
            this.setState({ key: id });
          }} />
        }
      </>
    );
  }
}

const mapStateToProps = state => ({
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getsearchBarStatus: state.AdminReducer.get("searchBarOpenStatus"),
  getAnalyticsTabView: state.AdminReducer.get("analyticsTabType"),
});
const mapDispatchToProps = dispatch => ({
  setEarningModalStatus: payload =>
    dispatch({ type: "EARNINGMODALSTATUS", payload })
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  AnalyticsScreen
);

const styles = StyleSheet.create({
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 5,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
    fontSize: 16,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  inputAndroid: {
    paddingHorizontal: 3,
    paddingVertical: 5,
    borderWidth: 0.5,
    borderColor: "grey",
    borderRadius: 8,
    color: "#000",
    backgroundColor: "white",
    paddingRight: 10,
    fontSize: 16,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  }
});
