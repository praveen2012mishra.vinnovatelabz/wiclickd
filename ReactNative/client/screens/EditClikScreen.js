import { launchImageLibraryAsync } from "expo-image-picker";
import { askAsync, CAMERA_ROLL } from "expo-permissions";
import { List } from "immutable";
import React, { Component } from "react";
import { graphql } from "react-apollo";
import {
  Animated,
  AsyncStorage,
  Dimensions,
  ImageBackground,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Button, Icon, Tooltip } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import NewHeader from "../components/NewHeader";
import AppHelper from "../constants/AppHelper";
import ButtonStyle from "../constants/ButtonStyle";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import ConstantTooltip from "../constants/Tooltip";
import { SearchUserMutation } from "../graphqlSchema/graphqlMutation/SearchMutation";
import { EditClikMutation } from "../graphqlSchema/graphqlMutation/TrendingMutation";
import {
  UserLoginMutation,
  UserQueryMutation
} from "../graphqlSchema/graphqlMutation/UserMutation";
import { EditClikVariables } from "../graphqlSchema/graphqlVariables/PostVariables";
import { SearchUserVariables } from "../graphqlSchema/graphqlVariables/SearchVariables";
import { UserDetailsVariables } from "../graphqlSchema/graphqlVariables/UserVariables";
import {
  uploadBannerImageAsync,
  uploadProfileImageAsync
} from "../services/UserService";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import BottomScreen from "../components/BottomScreen";
import NavigationService from "../library/NavigationService";

class CreateClikScreen extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.Pagescrollview = null;
    this.state = {
      switchOn: false,
      value: 50,
      showSlider: false,
      textStyle: "WHITE",
      uploading: false,
      profilePic: "",
      title: "",
      text: "",
      items: [],
      selectText: "Cliks Name",
      uploadMutipleImagePost: [],
      changeBackPicEnable: this.props.clikdetails.get("changeBackPicEnable"),
      setBackPic: this.props.clikdetails.get("banner_pic"),
      setIcon: this.props.clikdetails.get("icon_pic"),
      changeIconPicEnable: this.props.clikdetails.get("changeIconPicEnable"),
      gradientColor: "rgba(0,0,0,0.9)",
      brightnessvalue: 0.5,
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      conditionIcon: false,
      summary: "",
      focusSummary: false,
      focusClikname: false,
      focusDesc: false,
      focusWeburl: false,
      focusQualification: false,
      RoleItems: [
        {
          label: "Member",
          value: "Member",
          key: 0
        },
        {
          label: "Admin",
          value: "Admin",
          key: 1
        },
        {
          label: "Super Admin",
          value: "Super Admin",
          key: 2
        }
      ],
      SelectRoleItems: "Member",
      clikName: this.props.clikdetails.get("cliksName").toString(),
      description: this.props.clikdetails.get("description").toString(),
      MutipleUser: [],
      MutipleUserList: [],
      showError: false,
      getImage: "",
      showcliktooltip: false,
      showaddmembertooltip: false,
      MutipleQualification: this.props.clikdetails.get("qualifications"),
      bannerHover: false,
      clikHover: false,
      descriptionHover: false,
      joinclikHover: false,
      website: this.props.clikdetails.get("website"),
      qualification: "",
      modalVisible: false,
      UserList: [],
      clikId: this.props.clikdetails.get("clikId"),
      switchValue: this.props.clikdetails.get("invite_only"),
      textInput: [],
      inputData: [],
      testArray: [],
      customQualificationArray: [{ text: "" }]
    };
    this.changeBannerImage = "";
    this.baseState = this.state;
  }

  componentDidMount = async () => {
    let testArray = [];
    let qualificationArray = [];
    qualificationArray =
      this.props.clikdetails.get("qualifications") &&
      this.props.clikdetails.get("qualifications").toArray();
    if (qualificationArray) {
      for (let i = 0; i < qualificationArray.length; i++) {
        testArray.push({ text: qualificationArray[i] });
      }
    }
    this.setState({ customQualificationArray: testArray });
    this._isMounted = true;
    const profileData = this.props.profileData;
    let userProfilePic = profileData
      .getIn(["my_users", "0", "user"])
      .getIn(["profile_pic"]);
    this.setState({
      profilePic: {
        uri: userProfilePic
      }
    });
    let joined = [];
    this.props.listTrending_cliks.map(async (value, index) => {
      await joined.push({
        id: value.node.id,
        name: value.node.name
      });
    });
    this.setState({ items: joined });
  };

  componentDidUpdate() {
    if (this.props.getHasScrollTop == true && this.Pagescrollview) {
      this.Pagescrollview.scrollTo({ x: 0, y: 0, animated: true });
      this.props.setHASSCROLLEDACTION(false);
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  _askPermission = async (type, failureMessage) => {
    const { status, permissions } = await askAsync(type);
    if (status === "denied") {
      alert(failureMessage);
    }
  };

  _pickBannerImage = async () => {
    await this._askPermission(
      CAMERA_ROLL,
      "We need the camera-roll permission to read pictures from your phone..."
    );
    let pickerResult = await launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    });
    this._handleBannerImagePicked(pickerResult);
  };

  _handleBannerImagePicked = async pickerResult => {
    let uploadResponse, uploadResult;
    try {
      this.setState({ uploading: true });
      if (!pickerResult.cancelled) {
        uploadResponse = await uploadBannerImageAsync(pickerResult.uri);
        uploadResult = await uploadResponse.json();
        this.setState({
          setBackPic: pickerResult.uri
        });
        this.setState({ changeBackPicEnable: uploadResult.id });
      }
    } catch (e) {
      alert("Upload failed, sorry :(" + e + ")");
    } finally {
      this.setState({ uploading: false });
    }
  };

  // ============upload icon===================
  pickIcon = async () => {
    await this._askPermission(
      CAMERA_ROLL,
      "We need the camera-roll permission to read pictures from your phone..."
    );
    let pickerResult = await launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    });
    let size = pickerResult.uri.length * (3 / 4) - 2;
    if (size <= 4000) {
      this._handleIconImagePicked(pickerResult);
    } else {
      alert("choose an image less than 4kb");
    }
  };

  _handleIconImagePicked = async pickerResult => {
    let uploadResponse, uploadResult;
    try {
      this.setState({ uploading: true });
      if (!pickerResult.cancelled) {
        uploadResponse = await uploadProfileImageAsync(pickerResult.uri);
        uploadResult = await uploadResponse.json();
        this.setState({
          setIcon: pickerResult.uri
        });
        this.setState({ changeIconPicEnable: uploadResult.id });
      }
    } catch (e) {
      alert("Upload failed, sorry :(" + e + ")");
    } finally {
      this.setState({ uploading: false });
    }
  };

  checkUser = async () => {
    let value =
      this.state.title.charAt(0) == "@"
        ? this.state.title.substr(1)
        : this.state.title;
    UserDetailsVariables.variables.id = "User:" + value;
    try {
      await applloClient
        .query({
          query: UserQueryMutation,
          ...UserDetailsVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState({
            MutipleUserList: this.state.MutipleUserList.concat([
              {
                name: res.data.user.username,
                pic: res.data.user.profile_pic,
                role: this.state.SelectRoleItems
              }
            ])
          });
          this.setState({
            showError: false,
            SelectRoleItems: "Member",
            title: ""
          });
        });
    } catch (e) {
      console.log(e);
      this.setState({
        showError: true
      });
    }
  };

  checkSelectedUser = async value => {
    UserDetailsVariables.variables.id = "User:" + value;
    try {
      await applloClient
        .query({
          query: UserQueryMutation,
          ...UserDetailsVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState({
            MutipleUserList: this.state.MutipleUserList.concat([
              {
                name: res.data.user.username,
                pic: res.data.user.profile_pic,
                role: this.state.SelectRoleItems
              }
            ])
          });
          this.setState({
            showError: false,
            SelectRoleItems: "Member",
            title: ""
          });
        });
    } catch (e) {
      console.log(e);
      this.setState({
        showError: true
      });
    }
  };

  deleteUser = async index => {
    let updatedList = this.state.MutipleUserList;
    updatedList.splice(index, 1);
    this.setState({ MutipleUserList: updatedList });
  };

  editCliks = async () => {
    let __self = this;
    EditClikVariables.variables.clik_id = this.state.clikId;
    EditClikVariables.variables.description = this.state.description;
    EditClikVariables.variables.banner_pic = this.state.changeBackPicEnable;
    EditClikVariables.variables.icon_pic = this.state.changeIconPicEnable;
    (EditClikVariables.variables.qualifications = this.props.clikdetails.get(
      "qualifications"
    )),
      // this.state.customQualificationArray &&  this.state.customQualificationArray[0].text != ""
      //     ? this.state.customQualificationArray.map(value => value.text)
      //     : [];
      (EditClikVariables.variables.website = this.state.website);
    EditClikVariables.variables.my_qualification = "auto approve";
    EditClikVariables.variables.invite_only = this.state.switchValue;
    try {
      let id = await AsyncStorage.getItem("UserId");
      await applloClient
        .query({
          query: EditClikMutation,
          ...EditClikVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.goToProfile(this.state.clikName);
          this.setState(this.baseState);
          let resDataLogin = await __self.props.Login();
          await __self.props.saveLoginUser(resDataLogin.data.login);
          if (resDataLogin) {
            await __self.props.getTrendingClicks({
              currentPage: AppHelper.PAGE_LIMIT
            });
            //__self.props.navigation.navigate("home");
          }
        });
    } catch (e) {
      console.log(e);
    }
  };

  goToProfile = id => {
    this.props.ClikId({
      id: id,
      type: "feed"
    });
  };

  tooglecliktooltip = () => {
    if (this.state.showcliktooltip == false) {
      this.setState({ showcliktooltip: true });
    } else {
      this.setState({ showcliktooltip: false });
    }
  };

  toogleaddmembertooltip = () => {
    if (this.state.showaddmembertooltip == false) {
      this.setState({ showaddmembertooltip: true });
    } else {
      this.setState({ showaddmembertooltip: false });
    }
  };

  addQualification = async () => {
    await this.setState({
      MutipleQualification: this.state.MutipleQualification.concat([
        this.state.qualification
      ])
    });
    await this.setState({
      qualification: ""
    });
  };

  deleteQualification = async index => {
    let updatedList = this.state.MutipleQualification;
    updatedList.splice(index, 1);
    this.setState({ MutipleQualification: updatedList });
  };

  checkClikname = async name => {
    var letters = /^[0-9a-zA-Z-]+$/;
    if (name.match(letters) || name == "") {
      if (name[0] == "-") {
        alert("- not allowed at initial of clik name");
        return false;
      }
      this.setState({ clikName: name });
      return true;
    } else {
      alert("Please input alphanumeric characters only");
      return false;
    }
  };

  customRenderUserSuggestion = value => {
    SearchUserVariables.variables.prefix = value;
    applloClient
      .query({
        query: SearchUserMutation,
        ...SearchUserVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        this.setState({ UserList: res.data.search.users });
      });
  };

  // custom qualification ============================================
  addcustomQualificationArray = e => {
    this.setState({
      customQualificationArray: [
        ...this.state.customQualificationArray,
        { text: "" }
      ]
    });
  };

  handleCustomQualification = (value, index) => {
    let inputArray = [...this.state.customQualificationArray];
    inputArray[index].text = value;
    this.setState({ customQualificationArray: inputArray }, () => { });
  };

  removecustomQualificationArray = (index, e) => {
    this.state.customQualificationArray.splice(index, 1);
    this.setState({
      customQualificationArray: this.state.customQualificationArray
    });
  };

  render() {
    const { setBackPic, setIcon } = this.state;
    const textStyle = styles.usertext;
    return (
      <View
        style={[
          {
            flex: 1,
            backgroundColor: ConstantColors.customeBackgroundColor,
            width: "100%",
            paddingTop: Dimensions.get("window").width <= 1100 ? 0 : 10,
            paddingHorizontal: Dimensions.get("window").width <= 1100 ? 0 : 10
          }
        ]}
      >
        <ScrollView
          ref={scrollview => {
            this.Pagescrollview = scrollview;
          }}
          showsVerticalScrollIndicator={false}
          onLayout={event => {
            let { x, y, width, height } = event.nativeEvent.layout;
            if (width < 1024) {
              this.setState({
                showsVerticalScrollIndicatorView: true,
                currentScreentWidth: width
              });
            } else {
              this.setState({
                showsVerticalScrollIndicatorView: false,
                currentScreentWidth: width
              });
            }
          }}
          style={{
            height:
              Platform.OS !== "web"
                ? null
                : Dimensions.get("window").height - 40
          }}
        >
          <View>
            {Dimensions.get("window").width <= 750 && (
              <Animated.View
                style={{
                  position: Platform.OS == "web" ? "sticky" : null,
                  top: 0,
                  left: 0,
                  right: 0,
                  zIndex: 10,
                  overflow: "hidden"
                }}
              >
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      flexDirection: "row",
                      backgroundColor: "#000",
                      // borderRadius: 20,
                      height: 50
                    }}
                  >
                    <TouchableOpacity
                      style={ButtonStyle.headerBackStyle}
                      onPress={() => {
                        let nav = this.props.navigation.dangerouslyGetParent()
                          .state;
                        if (nav.routes.length > 1) {
                          this.props.navigation.goBack();
                          return;
                        } else {
                          this.props.navigation.navigate("home");
                        }
                      }}
                    >
                      <Icon
                        color={"#fff"}
                        name="angle-left"
                        type="font-awesome"
                        size={40}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[ButtonStyle.headerTitleStyle, { backgroundColor: "#000" }]}
                    >
                      <Text
                        style={{
                          color: "white",
                          textAlign: "center",
                          fontWeight: "bold",
                          fontSize: 18,
                          fontFamily: ConstantFontFamily.MontserratBoldFont
                        }}
                      >
                        Edit Clik
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Animated.View>
            )}
            <View style={{
              marginVertical: Dimensions.get("window").width <= 1100 ? 10 : 0,
              marginHorizontal: Dimensions.get("window").width <= 1100 ? 5 : 0, 
              alignItems: 'center',
              backgroundColor: '#fff'
            }}>
              <View
                style={[
                  ButtonStyle.borderStyle, ButtonStyle.shadowStyle,
                  {
                    width: "99%",
                    borderRadius: 5,
                    backgroundColor: "#fff",
                    padding: Dimensions.get("window").width <= 750 ? 0 : 10,
                    marginHorizontal: 10,
                    borderWidth:0,
                  }
                ]}
              >
                <Text
                  style={{
                    marginTop: 10,
                    fontWeight: "bold",
                    fontSize: 16,
                    textAlign: "center",
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                    // padding: 10
                  }}
                >
                  A clik is a discussion group. You can share posts to each other
                  and also start a discussion with only members of your clik .
              </Text>
              </View>

              <View
                style={[
                  // ButtonStyle.borderStyle,
                  {
                    backgroundColor: "#fff",
                    // padding: 10,
                    marginTop: 10,
                    paddingHorizontal: 5,
                    width:'100%'
                  }
                ]}
              >
                {/* <View style={{ flexDirection: "row", width: "100%" }}>
                  <View
                    style={{
                      width: "70%",
                      justifyContent: "flex-start",
                      flexDirection: "row"
                    }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginRight: 20,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Banner
                </Text>
                  </View>
                  <TouchableOpacity
                    style={{
                      width: "30%",
                      justifyContent: "flex-end",
                      alignItems: "flex-end"
                    }}
                    onMouseEnter={() => this.setState({ bannerHover: true })}
                    onMouseLeave={() => this.setState({ bannerHover: false })}
                  >
                    {setBackPic == null && (
                      <Icon
                        color={"#f80403"}
                        iconStyle={{
                          marginTop: 10,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                        name="times"
                        type="font-awesome"
                        size={16}
                      />
                    )}
                    {this.state.bannerHover == true && Platform.OS == "web" ? (
                      <Tooltip
                        backgroundColor={"#d3d3d3"}
                        withPointer={false}
                        withOverlay={false}
                        toggleOnPress={true}
                        containerStyle={{
                          left: -60
                        }}
                        popover={
                          <Text
                            style={{
                              fontFamily: ConstantFontFamily.defaultFont
                            }}
                          >
                            Upload a Banner picture
                      </Text>
                        }
                      />
                    ) : null}
                  </TouchableOpacity>
                </View> */}
                {/* <View
                  style={{
                    height: Dimensions.get("window").height / 4,
                    borderColor: "#e1e1e1",
                    // borderWidth: 1,
                    borderRadius: Platform.OS == "web" ? 20 : null
                  }}
                >
                  <ImageBackground
                    style={styles.image}
                    imageStyle={{
                      borderRadius: 5
                    }}
                    source={{
                      uri: setBackPic
                    }}
                    resizeMode={"cover"}
                  >
                    <Icon
                      color={"#000"}
                      iconStyle={{
                        color: "#fff",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      reverse
                      name="camera"
                      type="font-awesome"
                      size={20}
                      containerStyle={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        flex: 1
                      }}
                      onPress={this._pickBannerImage}
                    />
                  </ImageBackground>
                </View> */}

                <Text
                  style={{
                    fontWeight: "bold",
                    marginTop: 15,
                    marginBottom: 5,
                    marginRight: 20,
                    fontSize: 16,
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  Add Icon (Less than 4kb)
            </Text>
                <View
                  style={{
                    height: 120,
                    borderWidth: 1,
                    borderColor: "#C5C5C5",
                    // borderWidth: 1,
                    backgroundColor: "#fff",
                    borderRadius: Platform.OS == "web" ? 20 : null,
                    width: 120
                  }}
                >
                  <ImageBackground
                    style={styles.image}
                    imageStyle={{
                      borderRadius: 5
                    }}
                    source={{
                      uri: setIcon
                    }}
                    resizeMode={"cover"}
                  >
                    <Icon
                      color={"#000"}
                      iconStyle={{
                        color: "#fff",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      reverse
                      name="camera"
                      type="font-awesome"
                      size={16}
                      containerStyle={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        flex: 1
                      }}
                      onPress={this.pickIcon}
                    />
                  </ImageBackground>
                </View>
                <View style={{ flexDirection: "row", width: "100%" }}>
                  <View
                    style={{
                      width: "70%",
                      justifyContent: "flex-start",
                      flexDirection: "row",
                      marginTop: 5
                    }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: 10,
                        marginRight: 20,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Clik Name
                </Text>
                    {Platform.OS == "web" ? (
                      <View>
                        <Icon
                          color={"#000"}
                          iconStyle={{
                            marginTop: 10,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                          name="info-circle"
                          type="font-awesome"
                          size={16}
                          onPress={() => this.tooglecliktooltip()}
                        />
                        {this.state.showcliktooltip == true && (
                          <Tooltip
                            withPointer={false}
                            withOverlay={false}
                            toggleOnPress={true}
                            containerStyle={{
                              left: -40,
                              top: -60,
                              width: 100
                            }}
                            popover={
                              <Text
                                style={{
                                  fontFamily: ConstantFontFamily.defaultFont
                                }}
                              >
                                {ConstantTooltip.CreateClik[0].ClikTooltip}
                              </Text>
                            }
                          />
                        )}
                      </View>
                    ) : (
                        <Tooltip
                          withOverlay={false}
                          popover={
                            <Text
                              style={{ fontFamily: ConstantFontFamily.defaultFont }}
                            >
                              {ConstantTooltip.CreateClik[0].ClikTooltip}
                            </Text>
                          }
                        >
                          <Icon
                            color={"#000"}
                            iconStyle={{
                              marginTop: 10,
                              justifyContent: "center",
                              alignItems: "center"
                            }}
                            name="info-circle"
                            type="font-awesome"
                            size={16}
                          />
                        </Tooltip>
                      )}
                  </View>
                  <TouchableOpacity
                    style={{
                      width: "30%",
                      justifyContent: "flex-end",
                      alignItems: "flex-end"
                    }}
                    onMouseEnter={() => this.setState({ clikHover: true })}
                    onMouseLeave={() => this.setState({ clikHover: false })}
                  >
                    {(this.state.clikName.length < 3 ||
                      this.state.clikName.length > 24 ||
                      this.state.clikName[this.state.clikName.length - 1] ==
                      "-") && (
                        <Icon
                          color={"#f80403"}
                          iconStyle={{
                            marginTop: 10,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                          name="times"
                          type="font-awesome"
                          size={16}
                        />
                      )}
                    {this.state.clikHover == true && Platform.OS == "web" ? (
                      <Tooltip
                        backgroundColor={"#d3d3d3"}
                        withPointer={false}
                        withOverlay={false}
                        toggleOnPress={true}
                        containerStyle={{
                          left: -60,
                          top: -60
                        }}
                        popover={
                          <Text
                            style={{ fontFamily: ConstantFontFamily.defaultFont }}
                          >
                            ClikName should be 3 to 24 Characters
                      </Text>
                        }
                      />
                    ) : null}
                  </TouchableOpacity>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <TextInput
                    value={this.state.clikName}
                    placeholder="EnterNameWithOutSpaces"
                    placeholderTextColor="#6D757F"
                    style={[
                      textStyle,
                      this.state.focusClikname ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle,
                      {                        
                        height: 45,
                        paddingVertical: 5,
                        paddingHorizontal: 10,
                        fontFamily: ConstantFontFamily.defaultFont,
                        outline: 'none'
                      }
                    ]}
                    onChangeText={clikName => this.checkClikname(clikName)}
                    onFocus={() => this.setState({ focusClikname: true })}
                    onBlur={() => this.setState({ focusClikname: false })}
                  />
                </View>
                <View
                  style={{ flexDirection: "row", width: "100%", marginTop: 10 }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      // marginTop: 5,
                      marginRight: 20,
                      fontSize: 16,
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }}
                  >
                    Invite Only
              </Text>
                  <View
                    style={{
                      alignItems: "center",
                      flexDirection: "row",
                      alignSelf: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Button
                      title="Yes"
                      buttonStyle={{
                        backgroundColor:
                          this.state.switchValue == true ? "#009B1A" : "#fff",
                        borderColor: "#e1e1e1",
                        borderWidth: 1,
                        height: 30,
                        borderBottomLeftRadius: 6,
                        borderTopLeftRadius: 6
                      }}
                      titleStyle={{
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        paddingHorizontal: 10,
                        fontSize: 14,
                        color: this.state.switchValue == true ? "#fff" : "#e1e1e1"
                      }}
                      onPress={() => this.setState({ switchValue: true })}
                    />
                    <Button
                      title="No"
                      buttonStyle={{
                        backgroundColor:
                          this.state.switchValue == false ? "#009B1A" : "#fff",
                        borderColor: "#e1e1e1",
                        borderWidth: 1,
                        height: 30,
                        borderTopRightRadius: 6,
                        borderBottomRightRadius: 6
                      }}
                      titleStyle={{
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        paddingHorizontal: 10,
                        fontSize: 14,
                        color: this.state.switchValue == false ? "#fff" : "#e1e1e1"
                      }}
                      onPress={() => this.setState({ switchValue: false })}
                    />
                  </View>
                </View>
                <Text
                  style={{
                    fontWeight: "bold",
                    marginTop: 15,
                    marginRight: 20,
                    fontSize: 16,
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  Website
            </Text>
                <TextInput
                  value={this.state.website}
                  placeholder="Optional"
                  style={[
                    textStyle,
                    this.state.focusWeburl ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle,
                    {                     
                      height: 45,
                      paddingVertical: 5,
                      paddingHorizontal: 10,
                      fontFamily: ConstantFontFamily.defaultFont,
                      outline: 'none'
                    }
                  ]}
                  onChangeText={website => this.setState({ website })}
                  onFocus={() => this.setState({ focusWeburl: true })}
                  onBlur={() => this.setState({ focusWeburl: false })}
                />
                <View style={{ flexDirection: "row", width: "100%" }}>
                  <View
                    style={{
                      width: "100%",
                      justifyContent: "flex-start",
                      flexDirection: "row",
                      marginTop: 5
                    }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: 10,
                        marginRight: 20,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Description
                </Text>
                  </View>
                  <TouchableOpacity
                    style={{
                      width: "30%",
                      justifyContent: "flex-end",
                      alignItems: "flex-end"
                    }}
                    onMouseEnter={() => this.setState({ descriptionHover: true })}
                    onMouseLeave={() => this.setState({ descriptionHover: false })}
                  >
                    {(this.state.description.length < 50 ||
                      this.state.description.length > 300) && (
                        <Icon
                          color={"#f80403"}
                          iconStyle={{
                            marginTop: 10,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                          name="times"
                          type="font-awesome"
                          size={16}
                        />
                      )}
                    {this.state.descriptionHover == true && Platform.OS == "web" ? (
                      <Tooltip
                        backgroundColor={"#d3d3d3"}
                        withPointer={false}
                        withOverlay={false}
                        toggleOnPress={true}
                        containerStyle={{
                          left: -60,
                          top: -60
                        }}
                        popover={
                          <Text
                            style={{
                              fontFamily: ConstantFontFamily.defaultFont
                            }}
                          >
                            Description should be 50 to 300 Characters
                      </Text>
                        }
                      />
                    ) : null}
                  </TouchableOpacity>
                </View>
                <TextInput
                  value={this.state.description}
                  multiline={true}
                  numberOfLines={5}
                  placeholder="Enter a description"
                  placeholderTextColor="#6D757F"
                  style={[
                    textStyle,
                    this.state.focusDesc ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle,
                    {                      
                      paddingVertical: 5,
                      paddingHorizontal: 10,
                      fontFamily: ConstantFontFamily.defaultFont,
                      height: Platform.OS == "ios" ? 100 : null,
                      outline: 'none'
                    }
                  ]}
                  onChangeText={description => this.setState({ description })}
                  onFocus={() => this.setState({ focusDesc: true })}
                  onBlur={() => this.setState({ focusDesc: false })}
                />
                <View style={{ flexDirection: "row", width: "100%" }}>
                  <View
                    style={{
                      width: "70%",
                      justifyContent: "flex-start",
                      flexDirection: "row"
                    }}
                  >
                    <Text
                      style={{
                        fontWeight: "bold",
                        marginTop: 15,
                        marginRight: 20,
                        fontSize: 16,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Prerequisites to Join Clik
                </Text>
                  </View>
                  <TouchableOpacity
                    style={{
                      width: "30%",
                      justifyContent: "flex-end",
                      alignItems: "flex-end"
                    }}
                    onMouseEnter={() => this.setState({ joinclikHover: true })}
                    onMouseLeave={() => this.setState({ joinclikHover: false })}
                  >
                    <Icon
                      name="plus"
                      type="font-awesome"
                      size={18}
                      cursor="pointer"
                      containerStyle={{
                        marginLeft: 5,
                        justifyContent: "center",
                        alignItems: "center",
                        cursor: "pointer"
                      }}
                      onPress={() => this.addcustomQualificationArray()}
                    />
                  </TouchableOpacity>
                </View>
                {this.state.customQualificationArray &&
                  this.state.customQualificationArray.map((item, index) => {
                    return (
                      <View
                        style={{
                          width: "100%",
                          flexDirection: "row",
                          alignItems: "center"
                        }}
                      >
                        <Text
                          style={{
                            width: "5%",
                            fontFamily: ConstantFontFamily.defaultFont
                          }}
                        >
                          {index + 1})
                    </Text>
                        <TextInput
                          style={[
                            this.state.focusQualification ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle,
                            {                              
                              height: 45,
                              paddingVertical: 5,
                              paddingHorizontal: 10,
                              outline: 'none'
                            }]}
                          placeholder="Enter a qualification"
                          value={item.text}
                          onChangeText={text =>
                            this.handleCustomQualification(text, index)
                          }
                          onFocus={() => this.setState({ focusQualification: true })}
                          onBlur={() => this.setState({ focusQualification: false })}
                        />
                        <TouchableOpacity
                          style={{ width: "5%" }}
                          onPress={() => this.removecustomQualificationArray(index)}
                        >
                          <Icon
                            name="trash"
                            type="font-awesome"
                            size={18}
                            cursor="pointer"
                            containerStyle={{
                              marginLeft: "auto",
                              justifyContent: "center",
                              alignItems: "center",
                              cursor: "pointer"
                            }}
                          />
                        </TouchableOpacity>
                      </View>
                    );
                  })}


                <Text
                  style={{
                    fontWeight: "bold",
                    marginTop: 15,
                    marginRight: 20,
                    fontSize: 16,
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  Max Number Of Members
            </Text>
                <TextInput
                  placeholder="100"
                  placeholderTextColor="#000"
                  style={[
                    textStyle,
                    this.state.focusSummary ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle,
                    {
                      height: 45,
                      paddingVertical: 5,
                      paddingHorizontal: 10,
                      outline: 'none',
                      fontFamily: ConstantFontFamily.defaultFont
                    }
                  ]}
                  onChangeText={summary => this.setState({ summary })}
                  onFocus={() => this.setState({ focusSummary: true })}
                  onBlur={() => this.setState({ focusSummary: false })}
                />

                <View
                  style={{
                    marginTop: 20,
                    alignSelf: "center"
                  }}
                >
                  <Button
                    title="Submit"
                    titleStyle={ButtonStyle.wtitleStyle}
                    buttonStyle={ButtonStyle.gbackgroundStyle}
                    containerStyle={ButtonStyle.containerStyle}
                    // disabled={
                    //   this.state.clikName.length < 3 ||
                    //   this.state.clikName.length > 24 ||
                    //   this.state.clikName[this.state.clikName.length - 1] == "-" ||
                    //   this.state.description.length < 50 ||
                    //   this.state.description.length > 300 ||
                    //   setBackPic == null ||
                    //   this.state.MutipleQualification.length == 0
                    //     ? true
                    //     : false
                    // }
                    onPress={() => this.editCliks()}
                  />
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
        {Dimensions.get("window").width <= 750 &&
          <BottomScreen navigation={NavigationService} />
        }
      </View>
    );
  }
}

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  listTrending_cliks: !state.TrendingCliksReducer.getIn(["Trending_cliks_List"])
    ? List()
    : state.TrendingCliksReducer.getIn(["Trending_cliks_List"]),
  link: state.LinkPostReducer.get("link"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  clikdetails: state.EditClikReducer.get("clik")
});

const mapDispatchToProps = dispatch => ({
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  ClikId: payload => dispatch(getTrendingCliksProfileDetails(payload))
});

const CreateClikScreenContainerWrapper = compose(
  graphql(UserLoginMutation, {
    name: "Login",
    options: { fetchPolicy: "no-cache" }
  })
)(CreateClikScreen);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  CreateClikScreenContainerWrapper
);

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: "100%"
  },
  usertext: {
    color: "#000",
    fontSize: 14,
    //fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  }
});
