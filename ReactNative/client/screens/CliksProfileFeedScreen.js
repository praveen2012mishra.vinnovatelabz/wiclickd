import { List } from "immutable";
import React, { Component, lazy, Suspense, createRef } from "react";
import {
  Animated,
  Dimensions,
  FlatList,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { Icon } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger,
} from "react-native-popup-menu";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { TabBar, TabView } from "react-native-tab-view";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { listClikMembers } from "../actionCreator/ClikMembersAction";
import { listClikUserRequest } from "../actionCreator/ClikUserRequestAction";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import ClikMembersList from "../components/ClikMembersList";
import ClikProfileUserList from "../components/ClikProfileUserList";
import ClikStar from "../components/ClikStar";
import DiscussionHomeFeed from "../components/DiscussionHomeFeed";
import InviteUserModal from "../components/InviteUserModal";
import NewHomeFeed from "../components/NewHomeFeed";
import SEOMetaData from "../components/SEOMetaData";
import ShadowSkeleton from "../components/ShadowSkeleton";
import TrendingHomeFeed from "../components/TrendingHomeFeed";
import AppHelper from "../constants/AppHelper";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import { retry } from "../library/Helper";
import CommentDetailScreen from "./CommentDetailScreen";
import HeaderRight from "../components/HeaderRight";
import ButtonStyle from "../constants/ButtonStyle";

const CliksProfileCard = lazy(() =>
  retry(() => import("../components/CliksProfileCard"))
);
let lastTap = null;

class CliksProfileFeedScreen extends Component {
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.state = {
      modalVisible: false,
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      memberValue: "",
      items: [
        {
          label: "Profile",
          value: "FEED",
          key: 0,
        },
        {
          label: "Members",
          value: "USERS",
          key: 1,
        },
        {
          label: "Applications",
          value: "APPLICATIONS",
          key: 2,
        },
      ],
      itemsDefult: [
        {
          label: "Feed",
          value: "FEED",
          key: 0,
        },
        {
          label: "Profile",
          value: "WIKI",
          key: 1,
        },
      ],
      cliksselectItem: this.props.navigation
        .getParam("type", "NO-ID")
        .toUpperCase(),
      listUserRequest: [],
      showIcon: "#fff",
      routes: [
        {
          key: "first",
          title: "Trending",
          icon: "fire",
          type: "simple-line-icon",
        },
        { key: "second", title: "New", icon: "clock-o", type: "font-awesome" },
        {
          key: "third",
          title: "Bookmarks",
          icon: "bookmark",
          type: "font-awesome",
        },
      ],
      index: 0,
      wikiVisible: false,
      id: "",
      ViewMode: "Default",
      scrollY: 0,
      ProfileHeight: 0,
      feedY: 0,
    };
    this.userPermision = false;
    this.userApprovePermision = false;
    this.flatListRefNew = createRef();
    this.flatListRefDiscussion = createRef();
    this.flatListRefTrending = createRef();
  }

  _renderLazyPlaceholder = ({ route }) => <ShadowSkeleton />;

  _handleIndexChange = (index) => {
    this.setState({ index });
    this.props.setPostCommentReset({
      payload: [],
      postId: "",
      title: "",
      loading: true,
    });
  };
  handleIndexForMember = (index) => this.setState({ index });

  _renderTabBar = (props) => (
    Dimensions.get("window").width >= 750 &&
    <View>
      <View
        style={[ButtonStyle.shadowStyle,ButtonStyle.borderStyle,{
          flexDirection: "row",
          // width: "100%",
          // height: Dimensions.get("window").width <= 750 ? 50 : 60,
          // borderWidth: 1,
          // borderColor: "#C5C5C5",
          // borderRadius: Dimensions.get("window").width <= 750 ? 0 : 10,
          backgroundColor:"#fff",
          alignItems: "center",
          paddingHorizontal: 10,
          height:70,
          marginLeft: 4,
          borderWidth: 0
          // paddingBottom: Dimensions.get("window").width <= 750 ? 0 : 10,
        }]}
      >
        <TabBar
          onTabPress={() => this.handleDoubleTap()}
          {...props}
          indicatorStyle={{
            backgroundColor: "transparent",
            height: 3,
            borderRadius: 6,
          }}
          style={{
            backgroundColor: "transparent",
            width: "100%",
            // height: Dimensions.get("window").width <= 750 ? 55 : 60,
            shadowColor: "transparent",
            height:60,
            justifyContent:'center'
          }}
          labelStyle={{
            color: "#000",
            fontFamily: ConstantFontFamily.MontserratBoldFont,
          }}
          renderIcon={({ route, focused, color }) =>
            Dimensions.get("window").width >= 750 && (
              <Icon
                name={route.icon}
                type={route.type}
                color={focused ? "#009B1A" : "#D3D3D3"}
              />
            )
          }
          renderLabel={({ route, focused, color }) => (
            <Text
              style={{
                color: focused ? "#009B1A" : "#D3D3D3",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
              }}
            >
              {route.title}
            </Text>
          )}
          // renderIndicator={({ route, focused, color }) => null}
        />

        {/* <TouchableOpacity
          style={{
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Menu>
            <MenuTrigger>
              <Icon
                type="material"
                name={this.getIcon()}
                containerStyle={{
                  alignSelf: "center",
                  justifyContent: "center"
                }}
              />
            </MenuTrigger>
            <MenuOptions
              optionsContainerStyle={{
                borderRadius: 6,
                borderWidth: 1,
                borderColor: "#e1e1e1",
                shadowColor: "transparent"
              }}
              customStyles={{
                optionsContainer: {
                  minHeight: 100,
                  marginTop: hp("3%"),
                  marginLeft: wp("-0.5%")
                }
              }}
            >
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Card"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Default View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Default"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Card View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Text"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Text View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Compact"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Compact View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
            </MenuOptions>
          </Menu>
        </TouchableOpacity> */}
      </View>
    </View>
  );

  getIcon = () => {
    switch (this.state.ViewMode) {
      case "Card":
        return "dashboard";
      case "Text":
        return "line-weight";
      case "Compact":
        return "reorder";
      default:
        return "list";
    }
  };

  _renderUserItem = (item) => {
    return (
      <ClikProfileUserList
        item={item}
        navigation={this.props.navigation}
        ClikInfo={this.props.cliksDetails}
      />
    );
  };

  _renderClikMembersItem = (item) => {
    return (
      <ClikMembersList
        item={item}
        navigation={this.props.navigation}
        ClikInfo={this.props.cliksDetails}
        userApprovePermision={this.userApprovePermision}
      />
    );
  };

  showIcon = (data) => {
    this.setState({
      showIcon: data,
    });
  };

  members = () => {
    this.setState({
      cliksselectItem: "USERS",
    });
  };

  onClose = () => {
    this.setState({
      modalVisible: false,
    });
  };

  async getStaticData() {
    let itemId = await this.props.navigation.getParam("id", "NO-ID");
    this.setState({
      id: itemId,
    });
    if (itemId) {
      await this.props.userId({
        id: itemId,
        type: this.props.navigation.getParam("type", "NO-ID"),
      });
      await this.props.setClikUserRequest({
        id: itemId,
        currentPage: AppHelper.PAGE_LIMIT,
      });
      await this.props.setClikMembers({
        id: itemId,
      });

      const index = this.props.getUserFollowCliksList.findIndex(
        (i) =>
          i
            .getIn(["clik", "name"])
            .toLowerCase()
            .replace("clik:", "") ==
          itemId
            .replace("%3A", ":")
            .toLowerCase()
            .replace("clik:", "")
      );
      if (index != -1) {
        if (
          this.props.getUserFollowCliksList.getIn([index, "member_type"]) ==
          "SUPER_ADMIN"
        ) {
          this.setState({
            showIcon: require("../assets/image/YBadge.png"),
          });
        } else if (
          this.props.getUserFollowCliksList.getIn([index, "member_type"]) ==
          "ADMIN"
        ) {
          this.setState({
            showIcon: require("../assets/image/SBadge.png"),
          });
        } else if (
          this.props.getUserFollowCliksList.getIn([index, "member_type"]) ==
          "MEMBER"
        ) {
          this.setState({
            showIcon: require("../assets/image/badge.png"),
          });
        } else {
          this.getIconColour(index);
        }
      } else {
        this.getIconColour(index);
      }
    }
  }

  getIconColour = (index) => {
    if (index != -1) {
      if (
        this.props.getUserFollowCliksList.getIn([
          index,
          "settings",
          "follow_type",
        ]) == "FAVORITE"
      ) {
        this.setState({
          showIcon: "#FADB4A",
        });
      }
      if (
        this.props.getUserFollowCliksList.getIn([
          index,
          "settings",
          "follow_type",
        ]) == "FOLLOW"
      ) {
        this.setState({
          showIcon: "#E1E1E1",
        });
      }
    } else {
      this.setState({
        showIcon: "#fff",
      });
    }
  };

  componentDidUpdate(prevProps) {
    if (this.CliksProfilescrollview) {
      this.CliksProfilescrollview.scrollTo({ x: 0, y: 0, animated: true });
    }
    if (this.props.getHasScrollTop == true && this.UserProfilescrollview) {
      this.UserProfilescrollview.scrollTo({ x: 0, y: 0, animated: true });
      this.props.setHASSCROLLEDACTION(false);
    }
    if (this.props.loginStatus == 1) {
      this.getUserPermision();
      this.getUserApprovePermision();
    }
  }

  getUserPermision = () => {
    const index = this.props.listClikMembers.findIndex(
      (i) =>
        i.node.user.id ==
        this.props.profileData.getIn(["my_users", "0", "user", "id"])
    );
    if (index != -1) {
      this.userPermision = true;
    } else {
      this.userPermision = false;
    }
  };

  getUserApprovePermision = () => {
    const index = this.props.listClikMembers.findIndex(
      (i) =>
        i.node.user.id ==
          this.props.profileData.getIn(["my_users", "0", "user", "id"]) &&
        (i.node.type == "SUPER_ADMIN" || i.node.type == "ADMIN")
    );
    if (index != -1) {
      this.userApprovePermision = true;
    } else {
      this.userApprovePermision = false;
    }
  };

  loginHandle = () => {
    this.props.setLoginModalStatus(true);
  };

  listScroll = (value) => {
    this.setState({
      feedY: value,
    });
    this.props.listScroll(value);
  };
  doScroll = (value, name) => {
    if (name == "new") {
      this.flatListRefNew = value;
    } else if (name == "trending") {
      this.flatListRefTrending = value;
    } else if (name == "discussion") {
      this.flatListRefDiscussion = value;
    }
  };

  scrollFunc = () => {
    {
      this.flatListRefNew.current &&
        this.flatListRefNew.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true,
        }),
        this.flatListRefTrending.current &&
          this.flatListRefTrending.current.scrollToOffset({
            x: 0,
            y: 0,
            animated: true,
          }),
        this.flatListRefDiscussion.current &&
          this.flatListRefDiscussion.current.scrollToOffset({
            x: 0,
            y: 0,
            animated: true,
          });
    }
  };

  handleDoubleTap = () => {
    if (lastTap !== null) {
      this.scrollFunc();
      clearTimeout(lastTap);
      lastTap = null;
    } else {
      lastTap = setTimeout(() => {
        clearTimeout(lastTap);
        lastTap = null;
      }, 1000);
    }
  };

  _renderScene = ({ route }) => {
    switch (route.key) {
      case "third":
        return (
          <View>
            {this.props.loginStatus == 1 ? (
              <View>
                {this.props.cliksDetails
                  .getIn(["data", "clik"])
                  .get("invite_only") == true ? (
                  <DiscussionHomeFeed
                    navigation={this.props.navigation}
                    listType={"Clik"}
                    data={this.props.navigation
                      .getParam("id", "NO-ID")
                      .toLowerCase()}
                    ViewMode={this.state.ViewMode}
                    listScroll={this.listScroll}
                    ActiveTab={this.state.routes[this.state.index].title}
                    doScroll={this.doScroll}
                  />
                ) : (
                  <DiscussionHomeFeed
                    navigation={this.props.navigation}
                    listType={"Clik"}
                    data={this.props.navigation
                      .getParam("id", "NO-ID")
                      .toLowerCase()}
                    ViewMode={this.state.ViewMode}
                    listScroll={this.listScroll}
                    ActiveTab={this.state.routes[this.state.index].title}
                    doScroll={this.doScroll}
                  />
                )}
              </View>
            ) : (
              <View>
                <View
                  style={{
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center",
                    }}
                    reverse
                    name="sticky-note"
                    type="font-awesome"
                    size={20}
                    containerStyle={{
                      alignSelf: "center",
                    }}
                  />
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center",
                    }}
                  >
                    <Text
                      onPress={() => this.loginHandle()}
                      style={{
                        textDecorationLine: "underline",
                        fontFamily: ConstantFontFamily.defaultFont,
                      }}
                    >
                      Login
                    </Text>{" "}
                    to see Discussion posts
                  </Text>
                </View>
              </View>
            )}
          </View>
        );
      case "second":
        return (
          <View>
            {this.props.cliksDetails
              .getIn(["data", "clik"])
              .get("invite_only") == true ? (
              this.props.loginStatus == 1 ? (
                <NewHomeFeed
                  navigation={this.props.navigation}
                  listType={"Clik"}
                  data={this.props.navigation
                    .getParam("id", "NO-ID")
                    .toLowerCase()}
                  ViewMode={this.state.ViewMode}
                  listScroll={this.listScroll}
                  ActiveTab={this.state.routes[this.state.index].title}
                  doScroll={this.doScroll}
                />
              ) : (
                <View>
                  <View
                    style={{
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Icon
                      color={"#000"}
                      iconStyle={{
                        color: "#fff",
                        justifyContent: "center",
                        alignItems: "center",
                        alignSelf: "center",
                      }}
                      reverse
                      name="sticky-note"
                      type="font-awesome"
                      size={20}
                      containerStyle={{
                        alignSelf: "center",
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center",
                      }}
                    >
                      <Text
                        onPress={() => this.loginHandle()}
                        style={{
                          textDecorationLine: "underline",
                          fontFamily: ConstantFontFamily.defaultFont,
                        }}
                      >
                        Login
                      </Text>{" "}
                      to see New posts
                    </Text>
                  </View>
                </View>
              )
            ) : (
              <NewHomeFeed
                navigation={this.props.navigation}
                listType={"Clik"}
                data={this.props.navigation
                  .getParam("id", "NO-ID")
                  .toLowerCase()}
                ViewMode={this.state.ViewMode}
                listScroll={this.listScroll}
                ActiveTab={this.state.routes[this.state.index].title}
                doScroll={this.doScroll}
              />
            )}
          </View>
        );
      case "first":
        return (
          <View>
            {this.props.cliksDetails.getIn(["data", "clik"]) &&
            this.props.cliksDetails
              .getIn(["data", "clik"])
              .get("invite_only") == true ? (
              this.props.loginStatus == 1 ? (
                <TrendingHomeFeed
                  navigation={this.props.navigation}
                  listType={"Clik"}
                  data={this.props.navigation
                    .getParam("id", "NO-ID")
                    .toLowerCase()}
                  ViewMode={this.state.ViewMode}
                  listScroll={this.listScroll}
                  ActiveTab={this.state.routes[this.state.index].title}
                  doScroll={this.doScroll}
                />
              ) : (
                <View>
                  <View
                    style={{
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Icon
                      color={"#000"}
                      iconStyle={{
                        color: "#fff",
                        justifyContent: "center",
                        alignItems: "center",
                        alignSelf: "center",
                      }}
                      reverse
                      name="sticky-note"
                      type="font-awesome"
                      size={20}
                      containerStyle={{
                        alignSelf: "center",
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center",
                      }}
                    >
                      <Text
                        onPress={() => this.loginHandle()}
                        style={{
                          textDecorationLine: "underline",
                          fontFamily: ConstantFontFamily.defaultFont,
                        }}
                      >
                        Login
                      </Text>{" "}
                      to see Trending posts
                    </Text>
                  </View>
                </View>
              )
            ) : (
              <TrendingHomeFeed
                navigation={this.props.navigation}
                listType={"Clik"}
                data={this.props.navigation
                  .getParam("id", "NO-ID")
                  .toLowerCase()}
                ViewMode={this.state.ViewMode}
                listScroll={this.listScroll}
                ActiveTab={this.state.routes[this.state.index].title}
                doScroll={this.doScroll}
              />
            )}
          </View>
        );
      default:
        return (
          <View>
            {this.props.cliksDetails
              .getIn(["data", "clik"])
              .get("invite_only") == true ? (
              this.props.loginStatus == 1 ? (
                <NewHomeFeed
                  navigation={this.props.navigation}
                  listType={"Clik"}
                  data={this.props.navigation
                    .getParam("id", "NO-ID")
                    .toLowerCase()}
                  ViewMode={this.state.ViewMode}
                  listScroll={this.listScroll}
                  ActiveTab={this.state.routes[this.state.index].title}
                  doScroll={this.doScroll}
                />
              ) : (
                <View>
                  <View
                    style={{
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Icon
                      color={"#000"}
                      iconStyle={{
                        color: "#fff",
                        justifyContent: "center",
                        alignItems: "center",
                        alignSelf: "center",
                      }}
                      reverse
                      name="sticky-note"
                      type="font-awesome"
                      size={20}
                      containerStyle={{
                        alignSelf: "center",
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center",
                      }}
                    >
                      <Text
                        onPress={() => this.loginHandle()}
                        style={{
                          textDecorationLine: "underline",
                          fontFamily: ConstantFontFamily.defaultFont,
                        }}
                      >
                        Login
                      </Text>{" "}
                      to see New posts
                    </Text>
                  </View>
                </View>
              )
            ) : (
              <NewHomeFeed
                navigation={this.props.navigation}
                listType={"Clik"}
                data={this.props.navigation
                  .getParam("id", "NO-ID")
                  .toLowerCase()}
                ViewMode={this.state.ViewMode}
                listScroll={this.listScroll}
                ActiveTab={this.state.routes[this.state.index].title}
                doScroll={this.doScroll}
              />
            )}
          </View>
        );
    }
  };

  renderTabViewForMobile = () => {
    if (this.props.getTabView == "Bookmarks") {
      return (
        <View>
          {this.props.loginStatus == 1 ? (
            <View>
              {this.props.cliksDetails
                .getIn(["data", "clik"])
                .get("invite_only") == true ? (
                <DiscussionHomeFeed
                  navigation={this.props.navigation}
                  listType={"Clik"}
                  data={this.props.navigation
                    .getParam("id", "NO-ID")
                    .toLowerCase()}
                  ViewMode={this.state.ViewMode}
                  listScroll={this.listScroll}
                  ActiveTab={this.state.routes[this.state.index].title}
                  doScroll={this.doScroll}
                />
              ) : (
                <DiscussionHomeFeed
                  navigation={this.props.navigation}
                  listType={"Clik"}
                  data={this.props.navigation
                    .getParam("id", "NO-ID")
                    .toLowerCase()}
                  ViewMode={this.state.ViewMode}
                  listScroll={this.listScroll}
                  ActiveTab={this.state.routes[this.state.index].title}
                  doScroll={this.doScroll}
                />
              )}
            </View>
          ) : (
            <View>
              <View
                style={{
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Icon
                  color={"#000"}
                  iconStyle={{
                    color: "#fff",
                    justifyContent: "center",
                    alignItems: "center",
                    alignSelf: "center",
                  }}
                  reverse
                  name="sticky-note"
                  type="font-awesome"
                  size={20}
                  containerStyle={{
                    alignSelf: "center",
                  }}
                />
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: "bold",
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    color: "#000",
                    alignSelf: "center",
                  }}
                >
                  <Text
                    onPress={() => this.loginHandle()}
                    style={{
                      textDecorationLine: "underline",
                      fontFamily: ConstantFontFamily.defaultFont,
                    }}
                  >
                    Login
                  </Text>{" "}
                  to see Discussion posts
                </Text>
              </View>
            </View>
          )}
        </View>
      );
    }

    else if (this.props.getTabView == "New") {
      return (
        <View>
          {this.props.cliksDetails.getIn(["data", "clik"]).get("invite_only") ==
          true ? (
            this.props.loginStatus == 1 ? (
              <NewHomeFeed
                navigation={this.props.navigation}
                listType={"Clik"}
                data={this.props.navigation
                  .getParam("id", "NO-ID")
                  .toLowerCase()}
                ViewMode={this.state.ViewMode}
                listScroll={this.listScroll}
                ActiveTab={this.state.routes[this.state.index].title}
                doScroll={this.doScroll}
              />
            ) : (
              <View>
                <View
                  style={{
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center",
                    }}
                    reverse
                    name="sticky-note"
                    type="font-awesome"
                    size={20}
                    containerStyle={{
                      alignSelf: "center",
                    }}
                  />
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center",
                    }}
                  >
                    <Text
                      onPress={() => this.loginHandle()}
                      style={{
                        textDecorationLine: "underline",
                        fontFamily: ConstantFontFamily.defaultFont,
                      }}
                    >
                      Login
                    </Text>{" "}
                    to see New posts
                  </Text>
                </View>
              </View>
            )
          ) : (
            <NewHomeFeed
              navigation={this.props.navigation}
              listType={"Clik"}
              data={this.props.navigation.getParam("id", "NO-ID").toLowerCase()}
              ViewMode={this.state.ViewMode}
              listScroll={this.listScroll}
              ActiveTab={this.state.routes[this.state.index].title}
              doScroll={this.doScroll}
            />
          )}
        </View>
      );
    }
    else if (this.props.getTabView == "Trending") {
      return (
        <View>
          {this.props.cliksDetails.getIn(["data", "clik"]) &&
          this.props.cliksDetails.getIn(["data", "clik"]).get("invite_only") ==
            true ? (
            this.props.loginStatus == 1 ? (
              <TrendingHomeFeed
                navigation={this.props.navigation}
                listType={"Clik"}
                data={this.props.navigation
                  .getParam("id", "NO-ID")
                  .toLowerCase()}
                ViewMode={this.state.ViewMode}
                listScroll={this.listScroll}
                ActiveTab={this.state.routes[this.state.index].title}
                doScroll={this.doScroll}
              />
            ) : (
              <View>
                <View
                  style={{
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center",
                    }}
                    reverse
                    name="sticky-note"
                    type="font-awesome"
                    size={20}
                    containerStyle={{
                      alignSelf: "center",
                    }}
                  />
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center",
                    }}
                  >
                    <Text
                      onPress={() => this.loginHandle()}
                      style={{
                        textDecorationLine: "underline",
                        fontFamily: ConstantFontFamily.defaultFont,
                      }}
                    >
                      Login
                    </Text>{" "}
                    to see Trending posts
                  </Text>
                </View>
              </View>
            )
          ) : (
            <TrendingHomeFeed
              navigation={this.props.navigation}
              listType={"Clik"}
              data={this.props.navigation.getParam("id", "NO-ID").toLowerCase()}
              ViewMode={this.state.ViewMode}
              listScroll={this.listScroll}
              ActiveTab={this.state.routes[this.state.index].title}
              doScroll={this.doScroll}
            />
          )}
        </View>
      );
    }
    else {
      return (
        // <View style={{ flex: 1 }}>
          <TrendingHomeFeed
              navigation={this.props.navigation}
              listType={"Clik"}
              data={this.props.navigation.getParam("id", "NO-ID").toLowerCase()}
              ViewMode={this.state.ViewMode}
              listScroll={this.listScroll}
              ActiveTab={this.state.routes[this.state.index].title}
              doScroll={this.doScroll}
            />
          // </View>
          )
    }
  };

  render() {
    const { cliksselectItem } = this.state;
    return (
      <View
        style={{
          // flex:1,
          // flexDirection: "row",
          // width: "100%",
          // height: "100%",
          
            flexDirection: "row",
            width: Dimensions.get("window").width >= 750 ? "99%" : '100%',
            height: "100%",
            marginHorizontal: Dimensions.get("window").width >= 750 ? 5 : 0,
          
        }}
      >
        
        {Dimensions.get("window").width <= 750
        //  && this.props.getTabView 
         ? (
          <View
          style={{
            flex: 1,
            width: "100%",
            height:'100%'
            // padding: 10
          }}
          // contentContainerStyle={{flex:1}}
          >
            {this.renderTabViewForMobile()}
          </View>
        ) : (
          <View
            style={{
              width:
                Platform.OS == "web" && Dimensions.get("window").width >= 750
                  ? 450
                  : "100%",
                  flex: 1
            }}
          >
            <TabView
              swipeEnabled={false}
              lazy
              navigationState={this.state}
              renderScene={this._renderScene}
              renderLazyPlaceholder={this._renderLazyPlaceholder}
              renderTabBar={this._renderTabBar}
              onIndexChange={this._handleIndexChange}
              style={{
                marginRight: 1,
                
              }}
            />
          </View>
         )} 

        <View
          style={{
            width: 450,
            height: "100%",
            marginLeft: 20,
            marginBottom: 5,
            flex: 1,
            display:
              Dimensions.get("window").width >= 750 && Platform.OS == "web"
                ? "flex"
                : "none",
          }}
        >
          <CommentDetailScreen
            navigation={this.props.navigation}
            postId={this.props.PostDetails && this.props.PostDetails.post.id}
            listScroll={this.listScroll}
            ProfileHeight={this.props.ProfileHeight}
            scrollY={this.props.scrollY}
            ActiveTab={this.state.routes[this.state.index].title}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  cliksDetails: state.TrendingCliksProfileReducer.get(
    "getTrendingCliksProfileDetails"
  ),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  listClikUserRequest: !state.ClikUserRequestReducer.get("ClikUserRequestList")
    ? List()
    : state.ClikUserRequestReducer.get("ClikUserRequestList"),
  listClikMembers: !state.ClikMembersReducer.get("clikMemberList")
    ? List()
    : state.ClikMembersReducer.get("clikMemberList"),
  loginStatus: state.UserReducer.get("loginStatus"),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List(),
  getTabView: state.AdminReducer.get("tabType"),
});

const mapDispatchToProps = (dispatch) => ({
  setHASSCROLLEDACTION: (payload) => dispatch(setHASSCROLLEDACTION(payload)),
  userId: (payload) => dispatch(getTrendingCliksProfileDetails(payload)),
  setClikUserRequest: (payload) => dispatch(listClikUserRequest(payload)),
  setClikMembers: (payload) => dispatch(listClikMembers(payload)),
  setSignUpModalStatus: (payload) => dispatch(setSIGNUPMODALACTION(payload)),
  setLoginModalStatus: (payload) => dispatch(setLOGINMODALACTION(payload)),
  saveLoginUser: (payload) => dispatch(saveUserLoginDaitails(payload)),
  setPostCommentReset: (payload) =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload }),
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  CliksProfileFeedScreen
);

const styles = StyleSheet.create({
  header: {
    position: Platform.OS == "web" ? "fixed" : null,
    left: 0,
    right: 0,
    zIndex: 10,
  },
  container: {
    flex: 1,
    backgroundColor: ConstantColors.customeBackgroundColor,
    paddingTop: Platform.OS == "web" ? 10 : 5,
    paddingHorizontal: Platform.OS == "web" ? 10 : 5,
  },
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 6,
    backgroundColor: "white",
    color: "black",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont,
  },
  inputAndroid: {
    paddingHorizontal: Dimensions.get("window").width > 750 ? 10 : 3,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#fff",
    borderRadius: 6,
    color: "#000",
    backgroundColor: "white",
    paddingRight: Dimensions.get("window").width > 750 ? 30 : 0,
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont,
  },
  actionButtonIcon: {
    fontSize: 20,
    // height: 20,
    color: "white",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
  },
  indicator: {
    backgroundColor: "#009B1A",
    height: 4,
    borderRadius: 6,
    // marginHorizontal: "2%"
  },
  tab: {
    backgroundColor: "transparent",
    flex: 1,
    width: "90%",
    height: 30,
    marginLeft: "auto",
    marginRight: "auto",
  },
  label: {
    fontSize: 16,
    fontWeight: "500",
    fontFamily: "Montserrat-Medium",
    letterSpacing: 1,
    width: "auto",
  },
  labelStyle: {
    color: "#000",
    fontFamily: ConstantFontFamily.MontserratBoldFont,
  },
});
