import React, { Component } from "react";
import {
  Animated,
  Dimensions,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import NewHeader from "../components/NewHeader";
import ButtonStyle from "../constants/ButtonStyle";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";

class FAQScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      headingName: ''
    };
  }

  async componentDidMount() {
    this.setState({
      data: [
        {
          title: "About",
          data: [1, 2, 3, 4, 5, 6]
        },
        {
          title: "Basics",
          data: ["A", "B", "C", "D", "E"]
        },
        {
          title: "Membership",
          data: ["A", "B", "C", "D", "E"]
        },
        {
          title: "Monetization",
          data: ["A", "B", "C", "D", "E"]
        },
        {
          title: "Contact",
          data: ["A", "B", "C", "D", "E"]
        },
        {
          title: "Beta testers",
          data: ["A", "B", "C", "D", "E"]
        }
      ]
    });
  }

  _renderItem = item => {
    switch (item) {
      case "Beta testers":
        return (<View style={{ padding: 25 }}>
          <Text style={{
            fontSize: 25,
            fontWeight: 'bold',
            paddingBottom: 15,
            //justifyContent:'center',
            //alignItems:'center'
            textAlign: 'center'
          }}>Beta Testers</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>We host a constant, open beta to try out new features and would love to have your feedback and for you to share our app via word of mouth! If your quality suggestions and feedback are implemented and/or each member you invite, we award you with gold membership each month.</Text>
          <Hoverable>
            {isHovered => (
              <TouchableOpacity
                style={{
                  fontSize: 15,
                  color: 'blue'
                }}
                onPress={() => {
                  this.setState({
                    headingName: 'Beta testers'
                  })
                }}
              >
                <Text>https://docs.google.com/document/d/1Bsy28x6vjaBM_ESO_A-cD_6SkhQVxhzE5IQpvqhWyd4/edit?usp=sharing</Text>
              </TouchableOpacity>
            )}
          </Hoverable>
          <Text>Update each category as outlined
          12:41
          ie) contact with contact,
          12:41
          beta testers with beta testers
          12:41
thanks!</Text>
        </View>);
      case "Contact":
        return (<View style={{ padding: 25 }}>
          <Text style={{
            fontSize: 25,
            fontWeight: 'bold',
            paddingBottom: 15,
            //justifyContent:'center',
            //alignItems:'center'
            textAlign: 'center'
          }}>Extras</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>Mail us anytime and reach our executive office directly at info@weclikd.com. We’re always looking forward to and actively reaching out to members, content leaders and new potential collabs. Please feel free to reach out to our CEO on Linkedin or follow @weclikd on Twitter.</Text>
        </View>);
      case "Monetization":
        return (<View style={{ padding: 25 }}>
          <Text style={{
            fontSize: 25,
            fontWeight: 'bold',
            paddingBottom: 15,
            //justifyContent:'center',
            //alignItems:'center'
            textAlign: 'center'
          }}>Monetization/Hearts</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>The core of #weclikd is you are investing in yourself while using our platform to learn. We gather the intellectual content of other social platforms, blogs, and thought leaders across the web and rely on our users to complement the posts with quality discussions.</Text>
          <Text style={{ fontSize: 15 }}>There are four heart tiers, red, silver, gold, and blue.</Text>
          <View style={{
            paddingBottom: 15,
          }}>
            <Text style={{ fontSize: 15 }}>These hearts serve multiple purposes.</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 15 }}>{'1'}{' '}</Text><Text style={{ fontSize: 15 }}> Bookmark the content. Posts with hearts will appear in your “Bookmarks” section of your home feed.</Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 15 }}>{'2'}{' '}</Text><Text style={{ fontSize: 15 }}> Your vote in the quality of the content. Blue is the highest quality, which means you have learned something valuable. Red should be used to express support for the content, but it was not something you learned. Silver & Gold are somewhere in between red and blue hearts.</Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 15 }}>{'3'}{' '}</Text><Text style={{ fontSize: 15 }}> Award the author with a portion of your paid subscription. Currently, only comments are monetized, not posts.</Text></View>

          </View>
          <Text style={{
            fontSize: 25,
            fontWeight: 'bold',
            paddingBottom: 15,
          }}>In detail:</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>Given Alice (you) is willing to create x and Bob is agreed upon payment for her content.</Text>

        </View>);
      case "Extras":
        return (<View style={{ padding: 25 }}>
          <Text style={{
            fontSize: 25,
            fontWeight: 'bold',
            paddingBottom: 15,
            //justifyContent:'center',
            //alignItems:'center'
            textAlign: 'center'
          }}>Extras</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>Find extra boosts, merch, widgets, and limited edition items here!! </Text>
        </View>);
      case "Profile":
        return (<View style={{ padding: 25 }}>
          <Text style={{
            fontSize: 25,
            fontWeight: 'bold',
            paddingBottom: 15,
            //justifyContent:'center',
            //alignItems:'center'
            textAlign: 'center'
          }}>Your Profile</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>Your profile is a personal safe space where you can check your analytics via earnings, edit settings and how you appear to the world wide web. It is a space to tell others what you are passionate about. </Text>
          <View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Analytics: optimize content promotion and earnings</Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Settings: edit your subscription options, notifications, and earn free membership and parks when you invite a friend to our platform</Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Edit: Change your profile </Text></View>
          </View>
        </View>);
      case "Membership":
        return (<View style={{ padding: 25 }}>
          <Text style={{
            fontSize: 25,
            fontWeight: 'bold',
            paddingBottom: 15,
            //justifyContent:'center',
            //alignItems:'center'
            textAlign: 'center'
          }}>Membership</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>Basic membership is free. However for the added premium features, the ability to fully monetize your earnings potential and for the hosting of your servers, we charge a base of $5.00 per month. </Text>

          <View>
            <Text style={{ fontSize: 15 }}>Additional packages to earn or invest in content will be released on a rolling out and seasonal basis. Future packages may allow users to invest and generate revenue by not only contributing valuable content but also via liking. This is to build the community of #weclikd itself upon celebration of thoughtful holidays and the meaning of days marked within our time. ((More details apply.))</Text>
          </View>
        </View>);
      case "Basics":
        return (<View style={{ padding: 25 }}>
          <Text style={{
            fontSize: 25,
            fontWeight: 'bold',
            paddingBottom: 15,
            //justifyContent:'center',
            //alignItems:'center'
            textAlign: 'center'
          }}>Basics</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>Home is where you can view the
          most popular trending feeds, newly indexed content, and
           your bookmarks that you liked, discussed, and indexed in.</Text>

          <View>
            <Text style={{ fontSize: 15 }}>Choose from four views:</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Cards</Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Compact</Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Text </Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Compact view</Text></View>
          </View>
          {/* <Text style={{
            fontSize: 25,
            fontWeight: 'bold',
            paddingBottom: 15,
          }}>Your Profile</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>Your profile is a personal safe space where you can check your analytics via earnings, edit settings and how you appear to the world wide web. It is a space to tell others what you are passionate about.</Text>
          <View>
            <Text style={{ fontSize: 15 }}>Choose from four views:</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Analytics: optimize content promotion and earnings</Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Settings: edit your subscription options, notifications, and earn free membership and parks when you invite a friend to our platform</Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Edit: Change your profile </Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Compact view</Text></View>
          </View> */}
          {/* <Text style={{ fontSize: 15, paddingBottom: 20 }}>Each user has their own feed, where you can see their trending content, new content, and also any common posts that you have discussed with the user.</Text>
          <Text style={{
            fontSize: 25,
            fontWeight: 'bold',
            paddingBottom: 15,
          }}>Topics</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>Topics relate to a specific subject. It is open to all - no community owns a specific topic.
          Topics are organized in a tree-like structure.
Users with enough reputation can propose new topics and can also volunteer to moderate.</Text>
          <Text style={{
            fontSize: 25,
            fontWeight: 'bold',
            paddingBottom: 15,
          }}>Cliks</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>Cliks are groups of people with a common background or shared interest. There is an application process to create a clik to prevent hate groups and also to prevent similar cliks from being created. All cliks are invite only and they may require some minimum qualifications or degrees to join. While both topics and cliks can be followed by anyone, they serve different purposes. Anyone can follow and discuss posts in /astrophysics, but the clik #astrophysicsphd may have their own locked esoteric discussions about posts in /astrophysics or other related fields. </Text>

          <Text style={{ fontSize: 15, paddingBottom: 20 }}>You may click the “+” icon to find new topics, cliks, users, or feeds to follow. Unlike other platforms that boost content and ads based off of popularity and who you follow, we improve quality experience through relevant topics, groups, and content.</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>Topics and cliks are groups of individuals coming together for a mutual interest. Some examples for cliks which are private groups are #uclajudo or #uclaphysics whereas topics are more generalized such as /computer-science. </Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>Root topics are gold topics that all other topics, cliks, and feeds must tie into. Topics are denoted in green font.
Cliks are denoted in blue font.</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>It is our philosophical nature at #weclikd to be curious: to question what is true grue.
To create a topic or clik, there is a quick approval process via submission of a document. Topics or cliks that are not yet approved do not appear on indexed content.</Text> */}
        </View>);
      case "About":
        return (<View style={{ padding: 25 }}>
          <Text style={{
            fontSize: 25,
            fontWeight: 'bold',
            paddingBottom: 15,
            //justifyContent:'center',
            //alignItems:'center'
            textAlign: 'center'
          }}>About</Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>Weclikd inc. is a platform to help you monetize and promote quality intellectual discourse across the web. We ascertain to help you discover novel ideas with our topics and quality content across the web. Our goal is to amplify your voice across the web and recapture the loss value of your time.</Text>

          <View>
            <Text style={{ fontSize: 15 }}>Users on our platform can:</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Endorse quality discussions with your subscription</Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Get visibility & traffic to your community and/or site</Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Monetize off your insightful comments</Text></View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}><Text style={{ fontSize: 25 }}>{'\u2022'}{' '}</Text><Text style={{ fontSize: 15 }}> Most of all, have fun learning with other users</Text></View>
          </View>
        </View>);
        case "Topics & cliks":
          return (<View style={{ padding: 25 }}>
            <Text style={{
              fontSize: 25,
              fontWeight: 'bold',
              paddingBottom: 15,
              //justifyContent:'center',
              //alignItems:'center'
              textAlign: 'center'
            }}>Topics </Text>
            <Text style={{ fontSize: 15, paddingBottom: 20 }}>Topics relate to a specific subject. It is open to all - no community owns a specific topic.
Topics are organized in a tree-like structure. 
Users with enough reputation can propose new topics and can also volunteer to moderate.</Text>
<Text style={{
              fontSize: 25,
              fontWeight: 'bold',
              paddingBottom: 15,
              //justifyContent:'center',
              //alignItems:'center'
              textAlign: 'center'
            }}>Cliks </Text>
            <Text style={{ fontSize: 15, paddingBottom: 20 }}>Cliks are groups of people with a common background or shared interest. There is an application process to create a clik to prevent hate groups and also to prevent similar cliks from being created. All cliks are invite only and they may require some minimum qualifications or degrees to join. While both topics and cliks can be followed by anyone, they serve different purposes. Anyone can follow and discuss posts in /astrophysics, but the clik #astrophysicsphd may have their own locked esoteric discussions about posts in /astrophysics or other related fields. </Text>
            <Text style={{ fontSize: 15, paddingBottom: 20 }}>You may click the “+” icon to find new topics, cliks, users, or feeds to follow. Unlike other platforms that boost content and ads based off of popularity and who you follow, we improve quality experience through relevant topics, groups, and content.  </Text>
            <Text style={{ fontSize: 15, paddingBottom: 20 }}>Topics and cliks are groups of individuals coming together for a mutual interest. Some examples for cliks which are private groups are #uclajudo or #uclaphysics whereas topics are more generalized such as /computer-science.  </Text>
          <Text style={{ fontSize: 15, paddingBottom: 20 }}>Root topics are gold topics that all other topics, cliks, and feeds must tie into. Topics are denoted in green font.
Cliks are denoted in blue font.
 
It is our philosophical nature at #weclikd to be curious: to question what is true grue.
To create a topic or clik, there is a quick approval process via submission of a document. Topics or cliks that are not yet approved do not appear on indexed content.</Text>
          </View>);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          ref={scrollview => {
            this.Pagescrollview = scrollview;
          }}
          showsVerticalScrollIndicator={false}
          onLayout={event => {
            let { x, y, width, height } = event.nativeEvent.layout;
            if (width < 1024) {
              this.setState({
                showsVerticalScrollIndicatorView: true,
                currentScreentWidth: width
              });
            } else {
              this.setState({
                showsVerticalScrollIndicatorView: false,
                currentScreentWidth: width
              });
            }
          }}
          style={{
            height:
              Platform.OS !== "web"
                ? null
                : Dimensions.get("window").height - 80
          }}
        >
          <View>
            {Dimensions.get('window').width <= 750 ? (
              <Animated.View
                style={{
                  position: Platform.OS == "web" ? "sticky" : null,
                  top: 0,
                  left: 0,
                  right: 0,
                  zIndex: 10,
                  overflow: "hidden",
                  //borderRadius: 6
                }}
              >
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      flexDirection: "row",
                      marginBottom: 10,
                      backgroundColor: "#000",
                      //borderRadius: 6,
                      paddingLeft:10,
                      height: 42
                    }}
                  >
                    <TouchableOpacity
                      style={{
                        flexDirection: "row",
                        justifyContent: "flex-start",
                        alignSelf: "center"
                      }}
                      onPress={() => {
                        let nav = this.props.navigation.dangerouslyGetParent()
                          .state;
                        if (nav.routes.length > 1) {
                          this.props.navigation.goBack();
                          return;
                        } else {
                          this.props.navigation.navigate("home");
                        }
                      }}
                    >
                      <Icon
                        color={"#fff"}
                        name="angle-left"
                        type="font-awesome"
                        size={40}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        flex: 1,
                        backgroundColor: "#000",
                        justifyContent: "center",
                        marginLeft: 20,
                        borderRadius: 6
                      }}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontWeight: "bold",
                          fontSize: 18,
                          fontFamily: ConstantFontFamily.MontserratBoldFont
                        }}
                      >
                        FAQ
                    </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Animated.View>
            ) : (
              null
              // <TouchableOpacity
              //   style={{
              //     width: "100%",
              //     justifyContent: "center",
              //     borderRadius: 6,
              //     height: 40
              //   }}
              // >
              //   <Text
              //     style={{
              //       color: "black",
              //       textAlign: "center",
              //       fontWeight: "bold",
              //       fontSize: 18,
              //       fontFamily: ConstantFontFamily.MontserratBoldFont
              //     }}
              //   >
              //     FAQ
              // </Text>
              // </TouchableOpacity>
            )}
            <ScrollView showsVerticalScrollIndicator={false}>
              {this._renderItem(this.state.headingName)}
              <View>
                <View
                  style={{
                    flexDirection: "row",
                  }}
                >
                  <View
                    style={{
                      width: Dimensions.get('window').width <= 750?"30%":'31%',
                      height: 200,
                      margin: 10
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <TouchableOpacity
                          style={[ButtonStyle.backgroundStyle, this.state.headingName == 'About' ? ButtonStyle.selectShadowStyle : ButtonStyle.shadowStyle, {
                            flexDirection: "column",
                            height: 200,
                          }]}
                          onPress={() => {
                            this.setState({
                              headingName: 'About'
                            })
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              justifyContent: "center",
                              alignItems: "center",
                              height: 200
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row"
                              }}
                            >
                              <Image
                                style={{
                                  width: 60,
                                  height: 60
                                }}
                                source={require("../../client/assets/image/logo.png")}
                              />
                            </View>
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                marginTop: 10
                              }}
                            >
                              About
                            </Text>
                          </View>
                        </TouchableOpacity>
                      )}
                    </Hoverable>
                  </View>

                  <View
                    style={{
                      width: Dimensions.get('window').width <= 750?"30%":'31%',
                      height: 200,
                      margin: 10
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <TouchableOpacity
                          style={[ButtonStyle.backgroundStyle, this.state.headingName == 'Basics' ? ButtonStyle.selectShadowStyle : ButtonStyle.shadowStyle, {
                            flexDirection: "column",
                            height: 200,
                          }]}
                          onPress={() => {
                            this.setState({
                              headingName: 'Basics'
                            })
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              justifyContent: "center",
                              alignItems: "center",
                              height: 200
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row"
                              }}
                            >
                              <Image
                                style={{
                                  width: 60,
                                  height: 60
                                }}
                                source={require("../../client/assets/image/books_1f4da.png")}
                              />
                            </View>
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                marginTop: 10
                              }}
                            >
                              Basics
                            </Text>
                          </View>
                        </TouchableOpacity>
                      )}
                    </Hoverable>
                  </View>

                  <View
                    style={{
                      width: Dimensions.get('window').width <= 750?"30%":'31%',
                      height: 200,
                      margin: 10
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <TouchableOpacity
                          style={[ButtonStyle.backgroundStyle, this.state.headingName == 'Membership' ? ButtonStyle.selectShadowStyle : ButtonStyle.shadowStyle, {
                            flexDirection: "column",
                            height: 200,
                          }]}
                          onPress={() => {
                            this.setState({
                              headingName: 'Membership'
                            })
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              justifyContent: "center",
                              alignItems: "center",
                              height: 200
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row"
                              }}
                            >
                              <Image
                                style={{
                                  width: 60,
                                  height: 60
                                }}
                                source={require("../../client/assets/image/sparkles_2728.png")}
                              />
                            </View>
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                marginTop: 10
                              }}
                            >
                              Membership
                            </Text>
                          </View>
                        </TouchableOpacity>
                      )}
                    </Hoverable>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: "row"
                  }}
                >
                  <View
                    style={{
                      width: Dimensions.get('window').width <= 750?"30%":'31%',
                      height: 200,
                      margin: 10
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <TouchableOpacity
                          style={[ButtonStyle.backgroundStyle, this.state.headingName == 'Profile' ? ButtonStyle.selectShadowStyle : ButtonStyle.shadowStyle, {
                            flexDirection: "column",
                            height: 200,
                          }]}
                          onPress={() => {
                            this.setState({
                              headingName: 'Profile'
                            })
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              justifyContent: "center",
                              alignItems: "center",
                              height: 200
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row"
                              }}
                            >
                              <Image
                                style={{
                                  width: 60,
                                  height: 60
                                }}
                                source={require("../../client/assets/image/smiling-face-with-sunglasses_1f60e.png")}
                              />
                            </View>
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                marginTop: 10
                              }}
                            >
                              Profile
                            </Text>
                          </View>
                        </TouchableOpacity>
                      )}
                    </Hoverable>
                  </View>

                  <View
                    style={{
                      width: Dimensions.get('window').width <= 750?"30%":'31%',
                      height: 200,
                      margin: 10
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <TouchableOpacity
                          style={[ButtonStyle.backgroundStyle, this.state.headingName == 'Topics & cliks' ? ButtonStyle.selectShadowStyle : ButtonStyle.shadowStyle, {
                            flexDirection: "column",
                            height: 200,
                          }]}
                          onPress={() => {
                            this.setState({
                              headingName: 'Topics & cliks'
                            })
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              justifyContent: "center",
                              alignItems: "center",
                              height: 200
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row"
                              }}
                            >
                              <Image
                                style={{
                                  width: 60,
                                  height: 60
                                }}
                                source={require("../../client/assets/image/rocket_1f680.png")}
                              />
                            </View>
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                marginTop: 10
                              }}
                            >
                              Topics & cliks
                            </Text>
                          </View>
                        </TouchableOpacity>
                      )}
                    </Hoverable>
                  </View>

                  <View
                    style={{
                      width: Dimensions.get('window').width <= 750?"30%":'31%',
                      height: 200,
                      margin: 10
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <TouchableOpacity
                          style={[ButtonStyle.backgroundStyle, this.state.headingName == 'Extras' ? ButtonStyle.selectShadowStyle : ButtonStyle.shadowStyle, {
                            flexDirection: "column",
                            height: 200,
                          }]}
                          onPress={() => {
                            this.setState({
                              headingName: 'Extras'
                            })
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              justifyContent: "center",
                              alignItems: "center",
                              height: 200
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row"
                              }}
                            >
                              <Image
                                style={{
                                  width: 60,
                                  height: 60
                                }}
                                source={require("../../client/assets/image/shopping-trolley_1f6d2.png")}
                              />
                            </View>
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                marginTop: 10
                              }}
                            >
                              Extras
                            </Text>
                          </View>
                        </TouchableOpacity>
                      )}
                    </Hoverable>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: "row",

                  }}
                >
                  <View
                    style={{
                      width: Dimensions.get('window').width <= 750?"30%":'31%',
                      height: 200,
                      margin: 10
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <TouchableOpacity
                          style={[ButtonStyle.backgroundStyle, this.state.headingName == 'Monetization' ? ButtonStyle.selectShadowStyle : ButtonStyle.shadowStyle, {
                            flexDirection: "column",
                            height: 200,
                          }]}
                          onPress={() => {
                            this.setState({
                              headingName: 'Monetization'
                            })
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              justifyContent: "center",
                              alignItems: "center",
                              height: 200
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row"
                              }}
                            >
                              <Image
                                style={{
                                  width: 60,
                                  height: 60
                                }}
                                source={require("../../client/assets/image/currency-exchange_1f4b1.png")}
                              />
                            </View>
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                marginTop: 10
                              }}
                            >
                              Monetization
                            </Text>
                          </View>
                        </TouchableOpacity>
                      )}
                    </Hoverable>
                  </View>

                  <View
                    style={{
                      width: Dimensions.get('window').width <= 750?"30%":'31%',
                      height: 200,
                      margin: 10
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <TouchableOpacity
                          style={[ButtonStyle.backgroundStyle, this.state.headingName == 'Contact' ? ButtonStyle.selectShadowStyle : ButtonStyle.shadowStyle, {
                            flexDirection: "column",
                            height: 200,
                          }]}
                          onPress={() => {
                            this.setState({
                              headingName: 'Contact'
                            })
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              justifyContent: "center",
                              alignItems: "center",
                              height: 200
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row"
                              }}
                            >
                              <Image
                                style={{
                                  width: 60,
                                  height: 60
                                }}
                                source={require("../../client/assets/image/speech-balloon_1f4ac.png")}
                              />
                            </View>
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                marginTop: 10
                              }}
                            >
                              Contact
                            </Text>
                          </View>
                        </TouchableOpacity>
                      )}
                    </Hoverable>
                  </View>

                  <View
                    style={{
                      width: Dimensions.get('window').width <= 750?"30%":'31%',
                      height: 200,
                      margin: 10
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <TouchableOpacity
                          style={[ButtonStyle.backgroundStyle, this.state.headingName == 'Beta testers' ? ButtonStyle.selectShadowStyle : ButtonStyle.shadowStyle, {
                            flexDirection: "column",
                            height: 200,
                          }]}
                          onPress={() => {
                            this.setState({
                              headingName: 'Beta testers'
                            })
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              justifyContent: "center",
                              alignItems: "center",
                              height: 200
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row"
                              }}
                            >
                              <Image
                                style={{
                                  width: 60,
                                  height: 60
                                }}
                                source={require("../../client/assets/image/crossed-swords_2694.png")}
                              />
                            </View>
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                marginTop: 10
                              }}
                            >
                              Beta testers
                            </Text>
                          </View>
                        </TouchableOpacity>
                      )}
                    </Hoverable>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  PostDetails: state.PostDetailsReducer.get("PostDetails"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  loginStatus: state.UserReducer.get("loginStatus"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension")
});

const mapDispatchToProps = dispatch => ({
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(FAQScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ConstantColors.customeBackgroundColor,
    paddingTop: Dimensions.get('window').width <= 750 ? 5 : 0,
    paddingHorizontal: Platform.OS == "web" ? 10 : 5
  }
});
