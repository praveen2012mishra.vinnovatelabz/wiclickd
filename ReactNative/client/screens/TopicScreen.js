import { List } from "immutable";
import React, { lazy, Suspense, createRef } from "react";
import {
  Animated,
  Dimensions,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  Image,
} from "react-native";
import { Icon } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger,
} from "react-native-popup-menu";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { TabBar, TabView } from "react-native-tab-view";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { getTrendingTopicsProfileDetails } from "../actionCreator/TrendingTopicsProfileAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import DiscussionHomeFeed from "../components/DiscussionHomeFeed";
import NewHomeFeed from "../components/NewHomeFeed";
import SEOMetaData from "../components/SEOMetaData";
import ShadowSkeleton from "../components/ShadowSkeleton";
import TopicStar from "../components/TopicStar";
import TrendingHomeFeed from "../components/TrendingHomeFeed";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import { retry } from "../library/Helper";
import CommentDetailScreen from "./CommentDetailScreen";
import HeaderRight from "../components/HeaderRight";
import ButtonStyle from "../constants/ButtonStyle";
import BottomScreen from '../components/BottomScreen';
import NavigationService from "../library/NavigationService";

let lastTap = null;

const TopicProfileCard = lazy(() =>
  retry(() => import("../components/TopicProfileCard"))
);

class TopicScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.offset = new Animated.Value(0);
    this.state = {
      modalVisible: false,
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      items: [
        {
          label: "Profile",
          value: "FEED",
          key: 0,
        },
      ],
      cliksselectItem: this.props.navigation
        .getParam("type", "NO-ID")
        .toUpperCase(),
      showIcon: "#fff",
      routes: [
        {
          key: "first",
          title: "Trending",
          icon: "fire",
          type: "simple-line-icon",
        },
        { key: "second", title: "New", icon: "clock-o", type: "font-awesome" },
        {
          key: "third",
          title: "Bookmarks",
          icon: "bookmark",
          type: "font-awesome",
        },
      ],
      index: 0,
      wikiVisible: false,
      id: this.props.navigation.getParam("title", "NO-ID").toUpperCase()
        ? this.props.navigation.getParam("title", "NO-ID").toUpperCase()
        : "",
      ViewMode: "Default",
      scrollY: 0,
      ProfileHeight: 0,
      feedY: 0,
    };
    this.flatListRefNew = createRef();
    this.flatListRefDiscussion = createRef();
    this.flatListRefTrending = createRef();
  }
  doScroll = (value, name) => {
    if (name == "new") {
      this.flatListRefNew = value;
    } else if (name == "trending") {
      this.flatListRefTrending = value;
    } else if (name == "discussion") {
      this.flatListRefDiscussion = value;
    }
  };

  scrollFunc = () => {
    {
      this.flatListRefNew.current &&
        this.flatListRefNew.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true,
        }),
        this.flatListRefTrending.current &&
        this.flatListRefTrending.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true,
        }),
        this.flatListRefDiscussion.current &&
        this.flatListRefDiscussion.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true,
        });
    }
  };

  handleDoubleTap = () => {
    if (lastTap !== null) {
      this.scrollFunc();
      clearTimeout(lastTap);
      lastTap = null;
    } else {
      lastTap = setTimeout(() => {
        clearTimeout(lastTap);
        lastTap = null;
      }, 1000);
    }
  };
  _renderLazyPlaceholder = ({ route }) => <ShadowSkeleton />;

  _handleIndexChange = (index) => {
    this.setState({ index });
    this.props.setPostCommentReset({
      payload: [],
      postId: "",
      title: "",
      loading: true,
    });
  };

  _renderTabBar = (props) => (
    Dimensions.get("window").width >= 750 &&
    <View>
      <View
        style={[
          ButtonStyle.shadowStyle,ButtonStyle.borderStyle,
          {
            flexDirection: "row",
            // width: "100%",
            backgroundColor: "#fff",
            height: 70,
            // height: Dimensions.get("window").width <= 750 ? 50 : 60,
            backgroundColor:
              Dimensions.get("window").width <= 750 ? "#000" : "#fff",
            alignItems: "center",
            paddingHorizontal: 10,
            marginLeft:4,
            borderWidth: 0
            // paddingBottom: Dimensions.get("window").width <= 750 ? 0 : 10,
          },
        ]}
      >
        <TabBar
          onTabPress={() => this.handleDoubleTap()}
          {...props}
          indicatorStyle={{
            backgroundColor: "transparent",
            height: 3,
            borderRadius: 6,
          }}
          style={{
            backgroundColor: "transparent",
            width: "100%",
            height: 60,
            // height: Dimensions.get("window").width <= 750 ? 55 : 60,
            shadowColor: "transparent",
            justifyContent: 'center',
          }}
          labelStyle={{
            color: "#000",
            fontFamily: ConstantFontFamily.MontserratBoldFont,
          }}
          renderIcon={({ route, focused, color }) =>
            Dimensions.get("window").width >= 750 && (
              <Icon
                name={route.icon}
                type={route.type}
                color={focused ? "#009B1A" : "#D3D3D3"}
              />
            )
          }
          renderLabel={({ route, focused, color }) => (
            <Text
              style={{
                color: focused ? "#009B1A" : "#D3D3D3",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
              }}
            >
              {route.title}
            </Text>
          )}
          renderIndicator={({ route, focused, color }) => null}
        />

        {/* <TouchableOpacity
          style={{
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Menu>
            <MenuTrigger>
              <Icon
                type="material"
                name={this.getIcon()}
                containerStyle={{
                  alignSelf: "center",
                  justifyContent: "center"
                }}
              />
            </MenuTrigger>
            <MenuOptions
              optionsContainerStyle={{
                borderRadius: 6,
                borderWidth: 1,
                borderColor: "#e1e1e1",
                shadowColor: "transparent"
              }}
              customStyles={{
                optionsContainer: {
                  minHeight: 100,
                  marginTop: hp("3%"),
                  marginLeft: wp("-0.5%")
                }
              }}
            >
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Card"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Default View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Default"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Card View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Text"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Text View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Compact"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Compact View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
            </MenuOptions>
          </Menu>
        </TouchableOpacity> */}
      </View>
    </View>
  );

  getIcon = () => {
    switch (this.state.ViewMode) {
      case "Card":
        return "dashboard";
      case "Text":
        return "line-weight";
      case "Compact":
        return "reorder";
      default:
        return "list";
    }
  };

  showIcon = (data) => {
    this.setState({
      showIcon: data,
    });
  };

  async componentDidMount() {
    this.offset = new Animated.Value(0);
    this.getStaticData();
  }

  async getStaticData() {
    let itemId = await this.props.navigation.getParam("title", "NO-ID");
    this.setState({
      id: itemId,
    });
    if (itemId) {
      this.props.topicId({
        id: itemId,
        type: this.props.navigation.getParam("type", "NO-ID"),
      });

      const index = this.props.getUserFollowTopicList.findIndex(
        (i) =>
          i
            .getIn(["topic", "name"])
            .toLowerCase()
            .replace("topic:", "") ==
          itemId
            .replace("%2F", "")
            .toLowerCase()
            .replace("topic:", "")
      );
      if (index != -1) {
        if (
          this.props.getUserFollowTopicList.getIn([
            index,
            "settings",
            "follow_type",
          ]) == "FAVORITE"
        ) {
          this.setState({
            showIcon: "#FADB4A",
          });
        }
        if (
          this.props.getUserFollowTopicList.getIn([
            index,
            "settings",
            "follow_type",
          ]) == "FOLLOW"
        ) {
          this.setState({
            showIcon: "#E1E1E1",
          });
        }
      } else {
        this.setState({
          showIcon: "#fff",
        });
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.getHasScrollTop == true && this.UserProfilescrollview) {
      this.UserProfilescrollview.scrollTo({ x: 0, y: 0, animated: true });
      this.props.setHASSCROLLEDACTION(false);
    }
    if (
      prevProps.navigation.getParam("title", "NO-ID").toUpperCase() !=
      this.props.navigation.getParam("title", "NO-ID").toUpperCase()
    ) {
      this.getStaticData();
      // this.setState({
      //   scrollY: 0,
      // });
      this.TopicsProfilescrollview.scrollTo({ x: 0, y: 0, animated: true });
    }
  }

  loginHandle = () => {
    this.props.setLoginModalStatus(true);
  };

  listScroll = (value) => {
    this.setState({
      feedY: value,
    });
  };

  _renderScene = ({ route }) => {
    switch (route.key) {
      case "third":
        return (
          <View>
            {this.props.loginStatus == 1 ? (
              <DiscussionHomeFeed
                navigation={this.props.navigation}
                listType={"Topic"}
                data={this.props.navigation
                  .getParam("title", "NO-ID")
                  .toLowerCase()}
                ViewMode={this.state.ViewMode}
                listScroll={this.listScroll}
                ActiveTab={this.state.routes[this.state.index].title}
                doScroll={this.doScroll}
              />
            ) : (
                <View>
                  <View
                    style={{
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Icon
                      color={"#000"}
                      iconStyle={{
                        color: "#fff",
                        justifyContent: "center",
                        alignItems: "center",
                        alignSelf: "center",
                      }}
                      reverse
                      name="sticky-note"
                      type="font-awesome"
                      size={20}
                      containerStyle={{
                        alignSelf: "center",
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center",
                      }}
                    >
                      <Text
                        onPress={() => this.loginHandle()}
                        style={{ textDecorationLine: "underline" }}
                      >
                        Login
                    </Text>{" "}
                    to see Discussion posts
                  </Text>
                  </View>
                </View>
              )}
          </View>
        );
      case "second":
        return (
          <NewHomeFeed
            navigation={this.props.navigation}
            listType={"Topic"}
            data={this.props.navigation
              .getParam("title", "NO-ID")
              .toLowerCase()}
            ViewMode={this.state.ViewMode}
            listScroll={this.listScroll}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        );
      case "first":
        return (
          <TrendingHomeFeed
            navigation={this.props.navigation}
            listType={"Topic"}
            data={this.props.navigation
              .getParam("title", "NO-ID")
              .toLowerCase()}
            ViewMode={this.state.ViewMode}
            listScroll={this.listScroll}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        );
      default:
        return (
          <NewHomeFeed
            navigation={this.props.navigation}
            listType={"Topic"}
            data={this.props.navigation
              .getParam("title", "NO-ID")
              .toLowerCase()}
            ViewMode={this.state.ViewMode}
            listScroll={this.listScroll}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        );
    }
  };

  renderTabViewForMobile = () => {
    if (this.props.getTabView == "Bookmarks") {
      return (
        <View>
          {this.props.loginStatus == 1 ? (
            <DiscussionHomeFeed
              navigation={this.props.navigation}
              listType={"Topic"}
              data={this.props.navigation
                .getParam("title", "NO-ID")
                .toLowerCase()}
              ViewMode={this.state.ViewMode}
              listScroll={this.listScroll}
              ActiveTab={this.state.routes[this.state.index].title}
              doScroll={this.doScroll}
            />
          ) : (
              <View>
                <View
                  style={{
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center",
                    }}
                    reverse
                    name="sticky-note"
                    type="font-awesome"
                    size={20}
                    containerStyle={{
                      alignSelf: "center",
                    }}
                  />
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center",
                    }}
                  >
                    <Text
                      onPress={() => this.loginHandle()}
                      style={{ textDecorationLine: "underline" }}
                    >
                      Login
                  </Text>{" "}
                  to see Discussion posts
                </Text>
                </View>
              </View>
            )}
        </View>
      );
    }

    else if (this.props.getTabView == "New") {
      return (
        <NewHomeFeed
          navigation={this.props.navigation}
          listType={"Topic"}
          data={this.props.navigation.getParam("title", "NO-ID").toLowerCase()}
          ViewMode={this.state.ViewMode}
          listScroll={this.listScroll}
          ActiveTab={this.state.routes[this.state.index].title}
          doScroll={this.doScroll}
        />
      );
    }
    else if (this.props.getTabView == "Trending") {
      return (
        // <View style={{ flex: 1 }}>
        <TrendingHomeFeed
          navigation={this.props.navigation}
          listType={"Topic"}
          data={this.props.navigation.getParam("title", "NO-ID").toLowerCase()}
          ViewMode={this.state.ViewMode}
          listScroll={this.listScroll}
          ActiveTab={this.state.routes[this.state.index].title}
          doScroll={this.doScroll}
        />
        // </View>
      );
    }
    else {
      return (
        // <View style={{ flex: 1 }}>
          <TrendingHomeFeed
            navigation={this.props.navigation}
            listType={"Topic"}
            data={this.props.navigation.getParam("title", "NO-ID").toLowerCase()}
            ViewMode={this.state.ViewMode}
            listScroll={this.listScroll}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        // </View>
        )
    }
  };

  render() {
    return (
      <View style={styles.container}>
        {Platform.OS == "web" && (
          <SEOMetaData
            title={
              this.props.topicsDetails.getIn(["data", "topic"])
                ? this.props.topicsDetails.getIn(["data", "topic"]).get("name")
                : ""
            }
            description={
              this.props.topicsDetails.getIn(["data", "topic"])
                ? this.props.topicsDetails
                  .getIn(["data", "topic"])
                  .getIn(["description"])
                : ""
            }
            image={
              this.props.topicsDetails.getIn(["data", "topic"])
                ? this.props.topicsDetails
                  .getIn(["data", "topic"])
                  .getIn(["banner_pic"])
                : ""
            }
          />
        )}
        {//Platform.OS != "web" &&
          Dimensions.get("window").width <= 750 && (
            <Animated.View
              style={{
                position: Platform.OS == "web" ? "sticky" : null,
                top: 0,
                left: 0,
                right: 0,
                zIndex: 10,
                overflow: "hidden",
                borderRadius: Dimensions.get("window").width <= 750 ? 0 : 6,
                paddingTop: Dimensions.get("window").width <= 750 ? 0 : 10,
              }}
            >
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <View
                  style={{
                    width: "100%",
                    flexDirection: "row",
                    backgroundColor: "#000",
                    borderRadius: Dimensions.get("window").width <= 750 ? 0 : 6,
                    marginBottom: Dimensions.get("window").width <= 750 ? 0 : 10,
                    height: 50,
                  }}
                >
                  <TouchableOpacity
                    style={ButtonStyle.headerBackStyle}
                    onPress={() => {
                      let nav = this.props.navigation.dangerouslyGetParent()
                        .state;
                      if (nav.routes.length > 1) {
                        this.props.navigation.goBack();
                        return;
                      } else {
                        this.props.navigation.navigate("home");
                      }
                    }}
                  >
                    <Icon
                      color={"#fff"}
                      name="angle-left"
                      type="font-awesome"
                      size={40}
                    />
                  </TouchableOpacity>

                  {this.props.topicsDetails.size != 0 &&
                    // !this.props.getsearchBarStatus &&
                    (
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          alignSelf: "center",
                          alignItems: "center",
                        }}
                      >
                        <View
                          style={{
                            paddingVertical: 5,
                            alignItems: "center",
                            justifyContent: "center",
                          }}
                        >
                          <TouchableOpacity
                            style={{
                              height: 30,
                              alignSelf: "flex-start",
                              paddingVertical: 5,
                              backgroundColor: this.props.topicsDetails
                                .getIn(["data", "topic"])
                                .get("parents")
                                ? "#e3f9d5"
                                : "#e3f9d5",
                              borderRadius: 6,
                              alignItems: "center",
                              justifyContent: "center",
                              marginRight: 5,
                            }}
                            onPress={() =>
                              this.TopicsProfilescrollview.scrollTo({
                                x: 0,
                                y: 0,
                                animated: true,
                              })
                            }
                          >
                            <Hoverable>
                              {(isHovered) => (
                                <Text
                                  style={{
                                    color: this.props.topicsDetails
                                      .getIn(["data", "topic"])
                                      .get("parents")
                                      ? "#009B1A"
                                      : "#009B1A",
                                    fontSize: 18,
                                    fontWeight: "bold",
                                    fontFamily:
                                      ConstantFontFamily.MontserratBoldFont,
                                    textDecorationLine:
                                      isHovered == true ? "underline" : "none",
                                  }}
                                >
                                  {"/"}
                                  {this.props.navigation.state.params.title
                                    .replace("%2F", "/")
                                    .toLowerCase()}
                                </Text>
                              )}
                            </Hoverable>
                          </TouchableOpacity>
                        </View>

                        {/* {this.props.loginStatus == 1 && (
                      <View
                        style={{
                          flexDirection: "row",
                          position: "absolute",
                          right: 0,
                          marginRight: 20
                        }}
                      >
                        <TopicStar
                          TopicName={this.props.topicsDetails
                            .getIn(["data", "topic"])
                            .get("name")}
                          ContainerStyle={{}}
                          ImageStyle={{
                            height: 20,
                            width: 20,
                            alignSelf: "center",
                            marginLeft: 5
                          }}
                        />
                      </View>
                    )} */}
                      </TouchableOpacity>
                    )}
                  <View style={ButtonStyle.headerRightStyle}>
                    <HeaderRight navigation={this.props.navigation} />
                  </View>
                </View>
              </View>
            </Animated.View>
          )}
        <ScrollView
          contentContainerStyle={{ height: "100%" }}
          ref={(ref) => {
            this.TopicsProfilescrollview = ref;
          }}
          showsVerticalScrollIndicator={false}
          onScroll={(event) => {
            this.setState({
              scrollY: event.nativeEvent.contentOffset.y,
            });
          }}
          scrollEventThrottle={16}
        >
          <Animated.View
            onLayout={(event) => {
              let { x, y, width, height } = event.nativeEvent.layout;
              if (width > 0) {
                this.setState({ ProfileHeight: height });
              }
            }}
          >
            <Suspense fallback={<ShadowSkeleton />}>
              <TopicProfileCard
                item={this.props.topicsDetails}
                navigation={this.props.navigation}
                icon={this.showIcon}
                feedY={this.state.feedY}
                ProfileHeight={this.state.ProfileHeight}
              />
            </Suspense>
          </Animated.View>
          <View
            style={
              Dimensions.get("window").width >= 750 &&
              this.state.scrollY >= this.state.ProfileHeight
                ? [
                  styles.header,
                  {
                    flexDirection: "row",
                    width: Dimensions.get("window").width >= 750 ? "99%" : '100%',
                    height: "100%",
                    marginHorizontal: Dimensions.get("window").width >= 750 ? 5 : 0,
                    top:
                      this.state.scrollY >= this.state.ProfileHeight
                        ? this.state.scrollY
                        : 0,
                  },
                ]
                : 
                {
                  flexDirection: "row",
                  width: Dimensions.get("window").width >= 750 ? "99%" : '100%',
                  height: "100%",
                  marginHorizontal: Dimensions.get("window").width >= 750 ? 5 : 0,
                }
            }
          >
            {Dimensions.get("window").width <= 750
              // && this.props.getTabView 
              ? 
                <View
                  style={{
                    flex: 1,
                    width: "100%",
                    height: '100%'
                    // padding: 10
                  }}
                // contentContainerStyle={{flex:1}}
                >
                  {this.renderTabViewForMobile()}
                </View>
               : 
                <View
                  style={{
                    width:
                      Platform.OS == "web" &&
                        Dimensions.get("window").width >= 750
                        ? 450
                        : "100%",
                    flex: 1,
                  }}
                >
                  <TabView
                    swipeEnabled={false}
                    lazy
                    navigationState={this.state}
                    renderScene={this._renderScene}
                    renderLazyPlaceholder={this._renderLazyPlaceholder}
                    renderTabBar={this._renderTabBar}
                    onIndexChange={this._handleIndexChange}
                    style={{
                      marginRight: 1,
                    }}
                  />
                </View>
              }
            <View
              style={{
                width: 450,
                height: "100%",
                marginLeft: 20,
                marginBottom: 5,
                flex: 1,
                display:
                  Dimensions.get("window").width >= 750 && Platform.OS == "web"
                    ? "flex"
                    : "none",
              }}
            >
              <CommentDetailScreen
                navigation={this.props.navigation}
                postId={
                  this.props.PostDetails && this.props.PostDetails.post.id
                }
                listScroll={this.listScroll}
                ProfileHeight={this.state.ProfileHeight}
                scrollY={this.state.scrollY}
                ActiveTab={this.state.routes[this.state.index].title}
              />
            </View>
          </View>
        </ScrollView>
        {Dimensions.get("window").width <= 750 &&
          <BottomScreen navigation={NavigationService} />
        }
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  TopicsFeedList: state.TopicsFeedReducer.getIn(["TopicsFeedList"]),
  topicsDetails: state.TrendingTopicsProfileReducer.get(
    "getTrendingTopicsProfileDetails"
  ),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getUserFollowTopicList: state.LoginUserDetailsReducer.get(
    "userFollowTopicsList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowTopicsList")
    : List(),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getTabView: state.AdminReducer.get("tabType"),
  getsearchBarStatus: state.AdminReducer.get("searchBarOpenStatus"),
});

const mapDispatchToProps = (dispatch) => ({
  setHASSCROLLEDACTION: (payload) => dispatch(setHASSCROLLEDACTION(payload)),
  topicId: (payload) => dispatch(getTrendingTopicsProfileDetails(payload)),
  saveLoginUser: (payload) => dispatch(saveUserLoginDaitails(payload)),
  setSignUpModalStatus: (payload) => dispatch(setSIGNUPMODALACTION(payload)),
  setLoginModalStatus: (payload) => dispatch(setLOGINMODALACTION(payload)),
  setPostCommentReset: (payload) =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload }),
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  TopicScreen
);

const styles = StyleSheet.create({
  header: {
    position: Platform.OS == "web" ? "fixed" : null,
    left: 0,
    right: 0,
    zIndex: 10,
  },
  container: {
    flex: 1,
    backgroundColor: ConstantColors.customeBackgroundColor,
    paddingTop: Dimensions.get("window").width <= 1100 ? 0 : 10,
    paddingHorizontal: Dimensions.get("window").width <= 1100 ? 0 : 10,
    // Platform.OS == "web" ? 10 : 5,
    height: "100%",
  },
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 6,
    backgroundColor: "white",
    color: "black",
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont,
    fontSize: 16,
  },
  inputAndroid: {
    paddingHorizontal: Dimensions.get("window").width > 750 ? 10 : 3,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#fff",
    borderRadius: 6,
    color: "#000",
    backgroundColor: "white",
    paddingRight: Dimensions.get("window").width > 750 ? 30 : 0,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont,
    fontSize: 16,
  },
  actionButtonIcon: {
    fontSize: 20,
    // height: 20,
    color: "white",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
  },
});
