import React, { Component, lazy, Suspense } from "react";
import {
  Animated,
  Dimensions,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Icon, Tooltip } from "react-native-elements";
import { TabBar, TabView } from "react-native-tab-view";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import applloClient from "../client";
import CreateCommentCard from "../components/CreateCommentCard";
import PostDetailsComment from "../components/PostDetailsComment";
import SEOMetaData from "../components/SEOMetaData";
import ShadowSkeleton from "../components/ShadowSkeleton";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { retry } from "../library/Helper";
const PostDetailsCard = lazy(() =>
  retry(() => import("../components/PostDetailsCard"))
);
import ShadowSkeletonComment from "../components/ShadowSkeletonComment";
import { calendarFormat } from "moment";
import ButtonStyle from "../constants/ButtonStyle";

class CommentDetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mainCommentShow: this.props.setComment,
      listScroll: 0,
      commentDetails: <ShadowSkeletonComment />
    };
  }

  async componentDidMount() {
    this.props.setPostCommentReset({
      payload: [],
      postId: "",
      title: "",
      loading: true
    });
  }

  onClose = () => {
    this.setState({
      modalVisible: false
    });
  };

  // componentDidUpdate = async prevProps => {
  //   if (this.props.PostCommentDetails.length > 0) {
  //     let Data = <ShadowSkeletonComment />;
  //     let CData = (
  //       <PostDetailsComment
  //         item={this.props.PostCommentDetails}
  //         navigation={this.props.navigation}
  //         closeModalhandalListMode={this.handalListMode}
  //         clickList={
  //           this.props.PostCommentDetails ? this.props.PostCommentDetails : null
  //         }
  //         PostId={
  //           this.props.PostDetails ? this.props.PostDetails.post.id : null
  //         }
  //       />
  //     );
  //     setTimeout(() => {
  //       this.setState({
  //         commentDetails: CData
  //       });
  //     }, 3000);
  //   }
  // };

  render() {
    return (
      <View
        style={{
          width: "100%",
          height: "100%"
        }}
      >
        {/* <TouchableOpacity
          style={[
            ButtonStyle.borderStyle,
            {
              justifyContent: "center",
              backgroundColor: "#fff",
              height: 60
            }
          ]}
          onClick={() => {
            let tempPostId = "";
            if (this.props.PostId.includes("Post:")) {
              tempPostId = this.props.PostId.replace("Post:", "");
            } else {
              tempPostId = this.props.PostId;
            }
            document.getElementById(
              this.props.ActiveTab + "Post:" + tempPostId
            ) &&
              document
                .getElementById(this.props.ActiveTab + "Post:" + tempPostId)
                .scrollIntoView();
          }}
        >
          <Text
            numberOfLines={2}
            style={{
              textAlign: "center",
              alignSelf: "center",
              justifyContent: "center",
              fontFamily: ConstantFontFamily.MontserratBoldFont,
              fontSize: 15,
              paddingHorizontal: 10
            }}
          >
            {this.props.Title}
          </Text>
        </TouchableOpacity> */}

        <CreateCommentCard
          onClose={this.onClose}
          parent_content_id={
            this.props.PostId &&
            this.props.PostId.replace("Trending", "")
              .replace("New", "")
              .replace("Discussion", "")
              .replace("Search", "")
          }
          clickList={
            this.props.PostDetails ? this.props.PostDetails.post.cliks : null
          }
          initial="main"
          topComment={this.props.PostDetails && this.props.PostDetails.post}
          navigation={this.props.navigation}
          title={this.props.Title}
        />
        <ScrollView
          ref="_scrollView"
          showsVerticalScrollIndicator={false}
          style={{
            height: hp("100%") - 50,
            // paddingBottom: 300,
            // overflowY:
            //   this.props.scrollY <= this.props.ProfileHeight
            //     ? "hidden"
            //     : "scroll"
          }}
          onScroll={event => {
            this.props.listScroll(event.nativeEvent.contentOffset.y);
            this.setState({
              listScroll: event.nativeEvent.contentOffset.y
            });
          }}
          scrollEventThrottle={16}
          scrollEnabled={
            this.state.listScroll >= this.props.ProfileHeight ? true : false
          }
        >


          {/* {this.props.PostDetails &&
            this.props.PostDetails.post.comments_score > 0 &&
            this.props.PostCommentDetails.length == 0 && (
              <ShadowSkeletonComment />
            )} */}

          {this.props.Loading == true && <ShadowSkeletonComment />}
          {this.props.PostCommentDetails.length > 0 && (
            <PostDetailsComment
              right={true}
              item={this.props.PostCommentDetails}
              navigation={this.props.navigation}
              closeModalhandalListMode={this.handalListMode}
              clickList={
                this.props.PostCommentDetails
                  ? this.props.PostCommentDetails
                  : null
              }
              PostId={
                this.props.PostDetails ? this.props.PostDetails.post.id : null
              }
              stopScrolling={() => { }}
            />
          )}

          {this.props.PostCommentDetails.length == 0 && (
            <View
              style={{
                flexDirection: "column",
                height: 100
              }}
            >
              <View
                style={{
                  width: "100%",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              >
                <Icon
                  color={"#000"}
                  iconStyle={{
                    color: "#fff",
                    justifyContent: "center",
                    alignItems: "center",
                    alignSelf: "center"
                  }}
                  reverse
                  name="comments"
                  type="font-awesome"
                  size={20}
                  containerStyle={{
                    alignSelf: "center"
                  }}
                />
              </View>
              <View
                style={{
                  flex: 1,
                  width: "100%",
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: "bold",
                    fontFamily: ConstantFontFamily.defaultFont,
                    color: "#000",
                    alignSelf: "center"
                  }}
                >
                  No Discussions
                </Text>
              </View>
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = state => ({
  PostDetails: state.PostDetailsReducer.get("PostDetails"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  loginStatus: state.UserReducer.get("loginStatus"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  PostCommentDetails: state.PostCommentDetailsReducer.get("PostCommentDetails"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  selectComment: state.PostCommentDetailsReducer.get("selectComment"),
  setComment: state.PostDetailsReducer.get("setComment"),
  TrendingHomeFeed: state.HomeFeedReducer.get("TrendingHomeFeedList"),
  TrendingTopicHomeFeed: state.HomeFeedReducer.get("TrendingTopicHomeFeedList"),
  TrendingClikHomeFeed: state.HomeFeedReducer.get("TrendingClikHomeFeedList"),
  TrendingUserHomeFeed: state.HomeFeedReducer.get("TrendingUserHomeFeedList"),
  TrendingExternalHomeFeed: state.HomeFeedReducer.get(
    "TrendingExternalFeedHomeFeedList"
  ),
  PostId: state.PostCommentDetailsReducer.get("PostId"),
  Title: state.PostCommentDetailsReducer.get("Title"),
  Loading: state.PostCommentDetailsReducer.get("Loading")
});

const mapDispatchToProps = dispatch => ({
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload)),
  setActiveId: payload => dispatch({ type: "SET_ACTIVE_ID", payload }),
  setPostCommentReset: payload =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload })
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  CommentDetailScreen
);