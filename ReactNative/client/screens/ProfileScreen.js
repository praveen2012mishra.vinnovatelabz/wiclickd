import { List } from "immutable";
import React, { Component, lazy, Suspense, createRef } from "react";
import {
  Animated,
  Dimensions,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import { Icon } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger,
} from "react-native-popup-menu";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { TabBar, TabView } from "react-native-tab-view";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import DiscussionHomeFeed from "../components/DiscussionHomeFeed";
import NewHomeFeed from "../components/NewHomeFeed";
import SEOMetaData from "../components/SEOMetaData";
import ShadowSkeleton from "../components/ShadowSkeleton";
import TrendingHomeFeed from "../components/TrendingHomeFeed";
import UserStar from "../components/UserStar";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import { retry } from "../library/Helper";
import CommentDetailScreen from "./CommentDetailScreen";
import HeaderRight from "../components/HeaderRight";
import ButtonStyle from "../constants/ButtonStyle";
import BottomScreen from '../components/BottomScreen';
import NavigationService from "../library/NavigationService";

const UserProfileCard = lazy(() =>
  retry(() => import("../components/UserProfileCard"))
);
let lastTap = null;
class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.UserProfilescrollview = null;
    this.state = {
      listHomeFeed: [],
      modalVisible: false,
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      id: "",
      items: [
        {
          label: "Profile",
          value: "FEED",
          key: 0,
        },
      ],
      userselectItem: this.props.navigation
        .getParam("type", "NO-ID")
        .toUpperCase(),
      showIcon: "#fff",
      publicationVisible: false,
      routes: [
        {
          key: "first",
          title: "Trending",
          icon: "fire",
          type: "simple-line-icon",
        },
        { key: "second", title: "New", icon: "clock-o", type: "font-awesome" },
        {
          key: "third",
          title: "Bookmarks",
          icon: "bookmark",
          type: "font-awesome",
        },
      ],
      index: 0,
      ViewMode: "Default",
      scrollY: 0,
      ProfileHeight: 0,
      feedY: 0,
    };
    this.flatListRefNew = createRef();
    this.flatListRefDiscussion = createRef();
    this.flatListRefTrending = createRef();
  }

  showIcon = (data) => {
    this.setState({
      showIcon: data,
    });
  };

  componentDidUpdate(prevProps) {
    if (this.props.getHasScrollTop == true && this.UserProfilescrollview) {
      this.UserProfilescrollview.scrollTo({ x: 0, y: 0, animated: true });
      this.props.setHASSCROLLEDACTION(false);
    }
    if (
      prevProps.navigation.getParam("username", "NO-ID").toUpperCase() !=
      this.props.navigation.getParam("username", "NO-ID").toUpperCase()
    ) {
      // this.setState({
      //   scrollY: 0,
      // });
      this.UserProfilescrollview.scrollTo({ x: 0, y: 0, animated: true });
    }
  }

  async componentDidMount() {
    this.props.searchOpenBarStatus(false);
    const { navigation } = this.props;
    const itemId = navigation.getParam("username", "NO-ID");

    this.setState({
      id: itemId,
    });
    if (itemId) {
      this.props.userId({
        username: itemId,
        type: this.props.navigation.getParam("type", "NO-ID"),
      });
    }
  }

  doScroll = (value, name) => {
    if (name == "new") {
      this.flatListRefNew = value;
    } else if (name == "trending") {
      this.flatListRefTrending = value;
    } else if (name == "discussion") {
      this.flatListRefDiscussion = value;
    }
  };

  scrollFunc = () => {
    {
      this.flatListRefNew.current &&
        this.flatListRefNew.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true,
        }),
        this.flatListRefTrending.current &&
        this.flatListRefTrending.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true,
        }),
        this.flatListRefDiscussion.current &&
        this.flatListRefDiscussion.current.scrollToOffset({
          x: 0,
          y: 0,
          animated: true,
        });
    }
  };

  handleDoubleTap = () => {
    if (lastTap !== null) {
      this.scrollFunc();
      clearTimeout(lastTap);
      lastTap = null;
    } else {
      lastTap = setTimeout(() => {
        clearTimeout(lastTap);
        lastTap = null;
      }, 1000);
    }
  };

  __renderLazyPlaceholder = ({ route }) => <ShadowSkeleton />;

  _handleIndexChange = (index) => {
    this.setState({ index });
    this.props.setPostCommentReset({
      payload: [],
      postId: "",
      title: "",
      loading: true,
    });
  };

  _renderTabBar = (props) => (
    Dimensions.get("window").width >= 750 ?
    <View>
      <View
        style={[
          ButtonStyle.shadowStyle,ButtonStyle.borderStyle,
          {
            flexDirection: "row",
            // width: "100%",
            // height: Dimensions.get("window").width <= 750 ? 50 : 60,
            backgroundColor:
              Dimensions.get("window").width <= 750 ? "#000" : "#fff",
            alignItems: "center",
            paddingHorizontal: 10,
            height: 70,
            marginLeft:4,
            borderWidth: 0
            // paddingBottom: Dimensions.get("window").width <= 750 ? 0 : 10,
          },
        ]}
      >
        <TabBar
          onTabPress={() => this.handleDoubleTap()}
          {...props}
          indicatorStyle={{
            backgroundColor: "transparent",
            height: 3,
            borderRadius: 6,
          }}
          style={{
            backgroundColor: "transparent",
            width: "100%",
            height: 60,
            // height: Dimensions.get("window").width <= 750 ? 55 : 60,
            shadowColor: "transparent",
            justifyContent: 'center'
          }}
          labelStyle={{
            color: "#000",
            fontFamily: ConstantFontFamily.MontserratBoldFont,
          }}
          renderIcon={({ route, focused, color }) =>
            Dimensions.get("window").width >= 750 && (
              <Icon
                name={route.icon}
                type={route.type}
                color={focused ? "#009B1A" : "#D3D3D3"}
              />
            )
          }
          renderLabel={({ route, focused, color }) => (
            <Text
              style={{
                color: focused ? "#009B1A" : "#D3D3D3",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
              }}
            >
              {route.title}
            </Text>
          )}
          renderIndicator={({ route, focused, color }) => null}
        />

        {/* <TouchableOpacity
          style={{
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Menu>
            <MenuTrigger>
              <Icon
                type="material"
                name={this.getIcon()}
                containerStyle={{
                  alignSelf: "center",
                  justifyContent: "center"
                }}
              />
            </MenuTrigger>
            <MenuOptions
              optionsContainerStyle={{
                borderRadius: 6,
                borderWidth: 1,
                borderColor: "#e1e1e1",
                shadowColor: "transparent"
              }}
              customStyles={{
                optionsContainer: {
                  minHeight: 100,
                  marginTop: hp("3%"),
                  marginLeft: wp("-0.5%")
                }
              }}
            >
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Card"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Default View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Default"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Card View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Text"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Text View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
              <MenuOption
                onSelect={() =>
                  this.setState({
                    ViewMode: "Compact"
                  })
                }
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        textAlign: "center",
                        color: isHovered == true ? "#009B1A" : "#000",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Compact View
                    </Text>
                  )}
                </Hoverable>
              </MenuOption>
            </MenuOptions>
          </Menu>
        </TouchableOpacity> */}
      </View>
    </View>
    : null
  );

  getIcon = () => {
    switch (this.state.ViewMode) {
      case "Card":
        return "dashboard";
      case "Text":
        return "line-weight";
      case "Compact":
        return "reorder";
      default:
        return "list";
    }
  };

  listScroll = (value) => {
    this.setState({
      feedY: value,
    });
  };

  _renderScene = ({ route }) => {
    switch (route.key) {
      case "third":
        return (
          <View>
            {this.props.loginStatus == 1 ? (
              <DiscussionHomeFeed
                navigation={this.props.navigation}
                listType={"User"}
                data={this.props.navigation.getParam("username", "NO-ID")}
                //data={this.props.userDetails.getIn(["user", "username"])}
                ViewMode={this.state.ViewMode}
                listScroll={this.listScroll}
                ActiveTab={this.state.routes[this.state.index].title}
                doScroll={this.doScroll}
              />
            ) : (
                <View>
                  <View
                    style={{
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Icon
                      color={"#000"}
                      iconStyle={{
                        color: "#fff",
                        justifyContent: "center",
                        alignItems: "center",
                        alignSelf: "center",
                      }}
                      reverse
                      name="sticky-note"
                      type="font-awesome"
                      size={20}
                      containerStyle={{
                        alignSelf: "center",
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center",
                      }}
                    >
                      <Text
                        onPress={() => this.loginHandle()}
                        style={{
                          textDecorationLine: "underline",
                          fontFamily: ConstantFontFamily.defaultFont,
                        }}
                      >
                        Login
                    </Text>{" "}
                    to see Discussion posts
                  </Text>
                  </View>
                </View>
              )}
          </View>
        );
      case "second":
        return (
          <NewHomeFeed
            navigation={this.props.navigation}
            listType={"User"}
            data={this.props.navigation.getParam("username", "NO-ID")}
            //data={this.props.userDetails.getIn(["user", "username"])}
            ViewMode={this.state.ViewMode}
            listScroll={this.listScroll}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        );
      case "first":
        return (
          <TrendingHomeFeed
            navigation={this.props.navigation}
            listType={"User"}
            data={this.props.navigation.getParam("username", "NO-ID")}
            //data={this.props.userDetails.getIn(["user", "username"])}
            ViewMode={this.state.ViewMode}
            listScroll={this.listScroll}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        );
      default:
        return (
          <NewHomeFeed
            navigation={this.props.navigation}
            listType={"User"}
            data={this.props.navigation.getParam("username", "NO-ID")}
            //data={this.props.userDetails.getIn(["user", "username"])}
            ViewMode={this.state.ViewMode}
            listScroll={this.listScroll}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        );
    }
  };

  loginHandle = () => {
    this.props.setLoginModalStatus(true);
  };

  renderTabViewForMobile = () => {
    if (this.props.getTabView == "Bookmarks") {
      return (
        <View>
          {this.props.loginStatus == 1 ? (
            <DiscussionHomeFeed
              navigation={this.props.navigation}
              listType={"User"}
              data={this.props.navigation.getParam("username", "NO-ID")}
              //data={this.props.userDetails.getIn(["user", "username"])}
              ViewMode={this.state.ViewMode}
              listScroll={this.listScroll}
              ActiveTab={this.state.routes[this.state.index].title}
              doScroll={this.doScroll}
            />
          ) : (
              <View>
                <View
                  style={{
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center",
                    }}
                    reverse
                    name="sticky-note"
                    type="font-awesome"
                    size={20}
                    containerStyle={{
                      alignSelf: "center",
                    }}
                  />
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center",
                    }}
                  >
                    <Text
                      onPress={() => this.loginHandle()}
                      style={{
                        textDecorationLine: "underline",
                        fontFamily: ConstantFontFamily.defaultFont,
                      }}
                    >
                      Login
                  </Text>{" "}
                  to see Discussion posts
                </Text>
                </View>
              </View>
            )}
        </View>
      );
    }

    else if (this.props.getTabView == "New") {
      return (
        <NewHomeFeed
          navigation={this.props.navigation}
          listType={"User"}
          data={this.props.navigation.getParam("username", "NO-ID")}
          //data={this.props.userDetails.getIn(["user", "username"])}
          ViewMode={this.state.ViewMode}
          listScroll={this.listScroll}
          ActiveTab={this.state.routes[this.state.index].title}
          doScroll={this.doScroll}
        />
      );
    }
    else if (this.props.getTabView == "Trending") {
      return (
        <TrendingHomeFeed
          navigation={this.props.navigation}
          listType={"User"}
          data={this.props.navigation.getParam("username", "NO-ID")}
          //data={this.props.userDetails.getIn(["user", "username"])}
          ViewMode={this.state.ViewMode}
          listScroll={this.listScroll}
          ActiveTab={this.state.routes[this.state.index].title}
          doScroll={this.doScroll}
        />
      );
    }
    else {
      return (
        // <View style={{ flex: 1 }}>
          <TrendingHomeFeed
            navigation={this.props.navigation}
            listType={"User"}
            data={this.props.navigation.getParam("username", "NO-ID")}
            //data={this.props.userDetails.getIn(["user", "username"])}
            ViewMode={this.state.ViewMode}
            listScroll={this.listScroll}
            ActiveTab={this.state.routes[this.state.index].title}
            doScroll={this.doScroll}
          />
        // </View>
        )
    }
  };

  render() {
    return (
      <View style={styles.container}>
        {Platform.OS == "web" ? (
          <SEOMetaData
            title={
              this.props.userDetails.getIn(["user", "full_name"])
                ? this.props.userDetails.getIn(["user", "full_name"])
                : ""
            }
            description={
              this.props.userDetails.getIn(["user", "description"])
                ? this.props.userDetails.getIn(["user", "description"])
                : ""
            }
            image={
              this.props.userDetails.getIn(["user", "banner_pic"])
                ? this.props.userDetails.getIn(["user", "banner_pic"])
                : ""
            }
          />
        ):null}
        {//Platform.OS != "web" &&
          Dimensions.get("window").width <= 750 ? (
            <Animated.View
              style={{
                position: Platform.OS == "web" ? "sticky" : null,
                top: 0,
                left: 0,
                right: 0,
                zIndex: 10,
                overflow: "hidden",
                borderRadius: Dimensions.get("window").width <= 750 ? 0 : 6,
              }}
            >
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <View
                  style={{
                    width: "100%",
                    flexDirection: "row",
                    marginBottom: Dimensions.get("window").width <= 750 ? 0 : 10,
                    backgroundColor: "#000",
                    borderRadius: Dimensions.get("window").width <= 750 ? 0 : 6,
                    height: 50,
                  }}
                >
                  <TouchableOpacity
                    style={ButtonStyle.headerBackStyle}
                    onPress={() => {
                      let nav = this.props.navigation.dangerouslyGetParent()
                        .state;
                      if (nav.routes.length > 1) {
                        this.props.navigation.goBack();
                        return;
                      } else {
                        this.props.navigation.navigate("home");
                      }
                    }}
                  >
                    <Icon
                      color={"#fff"}
                      name="angle-left"
                      type="font-awesome"
                      size={40}
                    />
                  </TouchableOpacity>
                  {
                    !this.props.getsearchBarStatus ?
                    <TouchableOpacity
                      style={{
                        flex: 1,
                        justifyContent: "center",
                      }}
                      onPress={() =>
                        this.UserProfilescrollview.scrollTo({
                          x: 0,
                          y: 0,
                          animated: true,
                        })
                      }
                    >
                      <View
                        style={{
                          alignSelf: "flex-start",
                          paddingVertical: 5,
                          borderRadius: 6,
                          flexDirection: "row",
                          justifyContent: "center",
                          alignItems: "center",
                          // width: "100%",
                          justifyContent: "space-between",
                        }}
                      >
                        <Text
                          style={{
                            textAlign: "center",
                            alignSelf: "center",
                            color: "#fff",
                            fontWeight: "bold",
                            fontSize: 18,
                            fontFamily: ConstantFontFamily.MontserratBoldFont,
                          }}
                        >
                          {"@" + this.props.navigation.state.params.username}
                        </Text>

                        {/* {this.props.loginStatus == 1 && (
                        <UserStar
                          UserName={this.props.userDetails.getIn([
                            "user",
                            "username"
                          ])}
                          UserId={this.props.userDetails.getIn(["user", "id"])}
                          ContainerStyle={{}}
                          ImageStyle={{
                            height: 20,
                            width: 20,
                            alignSelf: "center",
                            marginLeft: 5,
                            marginBottom: 5
                          }}
                        />
                      )} */}
                      </View>
                    </TouchableOpacity> : null
                  }
                  <View style={ButtonStyle.headerRightStyle}>
                    <HeaderRight navigation={this.props.navigation} />
                  </View>
                </View>
              </View>
            </Animated.View>
          ) : null}
        {this.state.userselectItem == "FEED" && (
          <ScrollView
            contentContainerStyle={{ height: "100%" }}
            ref={(ref) => {
              this.UserProfilescrollview = ref;
            }}
            showsVerticalScrollIndicator={false}
            onScroll={(event) => {
              this.setState({
                scrollY: event.nativeEvent.contentOffset.y,
              });
            }}
            scrollEventThrottle={16}
          >
            <Animated.View
              onLayout={(event) => {
                let { x, y, width, height } = event.nativeEvent.layout;
                if (width > 0) {
                  this.setState({ ProfileHeight: height });
                }
              }}
            >
              <Suspense fallback={<ShadowSkeleton />}>
                <UserProfileCard
                  item={this.props.userDetails}
                  navigation={this.props.navigation}
                  icon={this.showIcon}
                  feedY={this.state.feedY}
                  ProfileHeight={this.state.ProfileHeight}
                />
              </Suspense>
            </Animated.View>
            <View
              style={
                Dimensions.get("window").width >= 750 &&
                this.state.scrollY >= this.state.ProfileHeight
                  ? [
                    styles.header,
                    {
                      flexDirection: "row",
                      height: "100%",
                      top:
                        this.state.scrollY >= this.state.ProfileHeight
                          ? this.state.scrollY
                          : 0,
                      marginHorizontal: Dimensions.get("window").width >= 750 ? 5 : 0,
                      width: Dimensions.get("window").width >= 750 ? "99%" : '100%',
                    },
                  ]
                  :
                   {
                    flexDirection: "row",
                    height: "100%",
                    marginHorizontal: Dimensions.get("window").width >= 750 ? 5 : 0,
                    width: Dimensions.get("window").width >= 750 ? "99%" : '100%',
                  }
              }
            >
              {Dimensions.get("window").width <= 750
                // &&
                //   this.props.getTabView 
                ? (
                  <View
                    style={{
                      flex: 1,
                      width: "100%",
                      height: '100%'
                      // padding: 10
                    }}
                  // contentContainerStyle={{flex:1}}
                  >
                    {this.renderTabViewForMobile()}
                  </View>
                ) : (
                  <View
                    style={{
                      width:
                        Platform.OS == "web" &&
                          Dimensions.get("window").width >= 750
                          ? 450
                          : "100%",
                      flex: 1,
                    }}
                  >
                    <TabView
                      swipeEnabled={false}
                      lazy
                      navigationState={this.state}
                      renderScene={this._renderScene}
                      renderLazyPlaceholder={this._renderLazyPlaceholder}
                      renderTabBar={this._renderTabBar}
                      onIndexChange={this._handleIndexChange}
                      style={{
                        marginRight: 1,
                      }}
                    />
                  </View>
                )}
              <View
                style={{
                  width: 450,
                  flex: 1,
                  marginLeft: 20,
                  marginBottom: 5,
                  display:
                    Dimensions.get("window").width >= 750 &&
                      Platform.OS == "web"
                      ? "flex"
                      : "none",
                }}
              >
                <CommentDetailScreen
                  navigation={this.props.navigation}
                  postId={
                    this.props.PostDetails && this.props.PostDetails.post.id
                  }
                  listScroll={this.listScroll}
                  ProfileHeight={this.state.ProfileHeight}
                  scrollY={this.state.scrollY}
                  ActiveTab={this.state.routes[this.state.index].title}
                />
              </View>
            </View>
          </ScrollView>
        )}
        {Dimensions.get("window").width <= 750 ?
          <BottomScreen navigation={NavigationService} />
          : null
        }
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  listUserFeed: !state.UserFeedReducer.getIn(["UserFeedList"])
    ? List()
    : state.UserFeedReducer.getIn(["UserFeedList"]),
  userDetails: state.UserProfileDetailsReducer.get(
    "getCurrentUserProfileDetails"
  ),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  loginStatus: state.UserReducer.get("loginStatus"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getUserFollowUserList: state.LoginUserDetailsReducer.get("userFollowUserList")
    ? state.LoginUserDetailsReducer.get("userFollowUserList")
    : List(),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getTabView: state.AdminReducer.get("tabType"),
  getsearchBarStatus: state.AdminReducer.get("searchBarOpenStatus"),
});

const mapDispatchToProps = (dispatch) => ({
  setHASSCROLLEDACTION: (payload) => dispatch(setHASSCROLLEDACTION(payload)),
  userId: (payload) => dispatch(getCurrentUserProfileDetails(payload)),
  saveLoginUser: (payload) => dispatch(saveUserLoginDaitails(payload)),
  setSignUpModalStatus: (payload) => dispatch(setSIGNUPMODALACTION(payload)),
  setLoginModalStatus: (payload) => dispatch(setLOGINMODALACTION(payload)),
  setPostCommentReset: (payload) =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload }),
  searchOpenBarStatus: payload => dispatch({ type: "SEARCHBAR_STATUS", payload })
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  ProfileScreen
);

const styles = StyleSheet.create({
  header: {
    position: Platform.OS == "web" ? "fixed" : null,
    left: 0,
    right: 0,
    zIndex: 10,
  },
  container: {
    flex: 1,
    backgroundColor: ConstantColors.customeBackgroundColor,
    paddingTop: Dimensions.get("window").width <= 1100 ? 0 : 10,
    paddingHorizontal: Dimensions.get("window").width <= 1100 ? 0 : 10,
    // Platform.OS == "web" ? 10 : 5,
    height: "100%",
  },
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 6,
    backgroundColor: "white",
    color: "black",
    fontSize: 16,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont,
  },
  inputAndroid: {
    paddingHorizontal: Dimensions.get("window").width > 750 ? 10 : 3,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#fff",
    borderRadius: 6,
    color: "#000",
    backgroundColor: "white",
    paddingRight: Dimensions.get("window").width > 750 ? 30 : 0,
    fontSize: 16,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont,
  },
  actionButtonIcon: {
    fontSize: 20,
    // height: 20,
    color: "white",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
  },
});
