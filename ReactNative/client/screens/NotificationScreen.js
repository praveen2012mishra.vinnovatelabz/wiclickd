import { List } from "immutable";
import moment from "moment";
import React, { Component } from "react";
import {
  Animated,
  Dimensions,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image
} from "react-native";
import { Button, Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import applloClient from "../client";
import NewHeader from "../components/NewHeader";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import {
  ClikJoinMutation,
  InviteToClikMutation,
  RejectClikMemberMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import {
  DeleteAccountNotificationMutation,
  GetAccountNotificationsMutation
} from "../graphqlSchema/graphqlMutation/Notification";
import {
  ClikJoinVariables,
  InviteToClikVariables,
  RejectClikMemberVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import ButtonStyle from "../constants/ButtonStyle";
import { UserFollowMutation } from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { UserFollowVariables } from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import ShadowSkeletonComment from "../components/ShadowSkeletonComment";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import { graphql } from "react-apollo";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import HeaderRight from "../components/HeaderRight";

class NotificationScreen extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.Pagescrollview = null;
    this.state = {
      value: 50,
      showSlider: false,
      textStyle: "WHITE",
      uploading: false,
      profilePic: "",
      selectedItems: [],
      title: "",
      text: "",
      items: [],
      selectText: "Cliks Name",
      uploadMutipleImagePost: [],
      changeBackPicEnable: null,
      gradientColor: "rgba(0,0,0,0.9)",
      brightnessvalue: 0.5,
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      conditionIcon: false,
      cliksText: "",
      summary: "",
      RoleItems: [
        {
          label: "Admin",
          value: "Admin",
          key: 0
        },
        {
          label: "Super Admin",
          value: "Super Admin",
          key: 1
        },
        {
          label: "Member",
          value: "Member",
          key: 2
        }
      ],
      SelectRoleItems: "Admin",
      clikName: "",
      description: "",
      showDMS: true,
      modalVisible: false,
      CommentNotificationList: [],
      CommentLoading: false,
      showEmptyIcon: false
    };
    this.changeBannerImage = "";
    moment.updateLocale("en", {
      relativeTime: {
        future: "in %s",
        past: "%s ago",
        s: "a few seconds",
        ss: "%ds",
        m: "1m",
        mm: "%dm",
        h: "1h",
        hh: "%dh",
        d: "1d",
        dd: "%dd",
        w: "1w",
        ww: "%dw",
        M: "1M",
        MM: "%dM",
        y: "1y",
        yy: "%dy"
      }
    });
  }

  componentDidMount = () => {
    this.getAccountNotifications();
  };

  getAccountNotifications = () => {
    if (this.state.CommentNotificationList.length == 0) {
      this.setState({
        CommentLoading: true
      });
    }
    applloClient
      .query({
        query: GetAccountNotificationsMutation,
        variables: {
          first: 100,
          after: null
        },
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        this.setState({
          CommentLoading: false,
          CommentNotificationList: res.data.account.notifications.edges,
          showEmptyIcon:
            res.data.account.notifications.edges.length == 0 ? true : false
        });
        let timer = setTimeout(() => {
          this.getAccountNotifications();
        }, 60000);
      });
  };

  deleteAccountNotifications = id => {
    applloClient
      .query({
        query: DeleteAccountNotificationMutation,
        variables: {
          notification_id: id
        },
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        this.getAccountNotifications();
      });
  };

  goToPostDetailsScreen = async (id, title) => {
    let header = "Home";
    await this.props.setPostDetails({
      title: header,
      id: "Post:" + id,
      navigate: true
    });
    await this.props.setPostCommentDetails({
      id: id,
      title: title,
      loading: true
    });
    await this.props.setComment(false);
  };

  joinClik = async (clik_id, notification_id) => {
    ClikJoinVariables.variables.clik_id = clik_id;
    ClikJoinVariables.variables.qualification = "";
    ClikJoinVariables.variables.known_members = [];
    try {
      await applloClient
        .query({
          query: ClikJoinMutation,
          ...ClikJoinVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.deleteAccountNotifications(notification_id);
        });
    } catch (e) {
      console.log(e);
    }
  };

  inviteClik = async (clik_id, user_id, notification_id) => {
    InviteToClikVariables.variables.clik_id = clik_id;
    InviteToClikVariables.variables.invited_users = [
      {
        userid_or_email: user_id,
        member_type: "MEMBER"
      }
    ];
    try {
      await applloClient
        .query({
          query: InviteToClikMutation,
          ...InviteToClikVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.deleteAccountNotifications(notification_id);
        });
    } catch (e) {
      console.log(e);
    }
  };

  rejectClik = async (clik_id, user_id, notification_id) => {
    RejectClikMemberVariables.variables.clik_id = clik_id;
    RejectClikMemberVariables.variables.user_id = user_id;
    try {
      await applloClient
        .query({
          query: RejectClikMemberMutation,
          ...RejectClikMemberVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.deleteAccountNotifications(notification_id);
        });
    } catch (e) {
      console.log(e);
    }
  };

  followUser = async (username, notification_id) => {
    UserFollowVariables.variables.user_id = username;
    UserFollowVariables.variables.follow_type = "FOLLOW";
    try {
      await applloClient
        .query({
          query: UserFollowMutation,
          ...UserFollowVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.deleteAccountNotifications(notification_id);
          let resDataLogin = await this.props.Login();
          await this.props.saveLoginUser(resDataLogin.data.login);
        });
    } catch (e) {
      console.log(e);
    }
  };

  getUserStar = UserName => {
    let index = 0;
    index = this.props.getUserFollowUserList.findIndex(
      i =>
        i.getIn(["user", "username"]).replace("user:", "") == UserName &&
        UserName.replace("user:", "")
    );
    return index;
  };

  goToClikProfile = id => {
    this.props.ClikId({
      id: id,
      type: "feed"
    });
  };

  render() {
    const regex = /(<([^>]+)>)/ig;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: ConstantColors.customeBackgroundColor,
          width: "100%",
          paddingTop: Dimensions.get("window").width <= 750 ? 0 : 10,
          // paddingHorizontal: Dimensions.get("window").width <= 750 ? 5 : 0
        }}
      >
        {// Platform.OS !== "web" ?
          Dimensions.get("window").width <= 750 ? (
            <Animated.View
              style={{
                left: 0,
                right: 0,
                zIndex: 10,
                overflow: "hidden",
                borderRadius: Dimensions.get("window").width <= 750 ? 0 : 6
              }}
            >
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <View
                  style={{
                    width: "100%",
                    height: 50,
                    flexDirection: "row",
                    alignItems: "center",
                    marginBottom: Dimensions.get("window").width <= 750 ? 0 : 10,
                    backgroundColor: "#000",
                    borderRadius: Dimensions.get("window").width <= 750 ? 0 : 6,
                  }}
                >
                  <TouchableOpacity
                    style={ButtonStyle.headerBackStyle}
                    onPress={() => {
                      let nav = this.props.navigation.dangerouslyGetParent()
                        .state;
                      if (nav.routes.length > 1) {
                        this.props.navigation.goBack();
                        return;
                      } else {
                        this.props.navigation.navigate("home");
                      }
                    }}
                  >
                    <Icon
                      color={"#fff"}
                      name="angle-left"
                      type="font-awesome"
                      size={40}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[ButtonStyle.headerTitleStyle, { backgroundColor: '#000' }]}
                  >
                    <Text
                      style={{
                        textAlign: 'center',
                        color: "#fff",
                        fontWeight: "bold",
                        fontSize: 18,
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      Notifications
                    </Text>
                  </TouchableOpacity>
                  <View style={ButtonStyle.headerRightStyle}>
                    <HeaderRight navigation={this.props.navigation} />
                  </View>
                </View>
              </View>
            </Animated.View>

          ) : null
          // <TouchableOpacity
          //   style={{
          //     width: "100%",
          //     justifyContent: "center",
          //     borderRadius: 6,
          //     height: 40,
          //   }}
          // >
          //   <Text
          //     style={{
          //       color: "black",
          //       textAlign: "center",
          //       fontWeight: "bold",
          //       fontSize: 18,
          //       fontFamily: ConstantFontFamily.MontserratBoldFont
          //     }}
          //   >
          //     Notifications
          //   </Text>
          // </TouchableOpacity>
        }

        {this.state.CommentNotificationList.length == 0 &&
          this.state.showEmptyIcon == true && (
            <View
              style={{
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Icon
                color={"#000"}
                iconStyle={{
                  color: "#fff",
                  justifyContent: "center",
                  alignItems: "center",
                  alignSelf: "center"
                }}
                reverse
                name="bell"
                type="font-awesome"
                size={20}
                containerStyle={{
                  alignSelf: "center"
                }}
              />
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  color: "#000",
                  alignSelf: "center"
                }}
              >
                No Notifications
              </Text>
            </View>
          )}

        <ScrollView
          ref={scrollview => {
            this.Pagescrollview = scrollview;
          }}
          showsVerticalScrollIndicator={false}
          onLayout={event => {
            let { x, y, width, height } = event.nativeEvent.layout;
            if (width < 1024) {
              this.setState({
                showsVerticalScrollIndicatorView: true,
                currentScreentWidth: width
              });
            } else {
              this.setState({
                showsVerticalScrollIndicatorView: false,
                currentScreentWidth: width
              });
            }
          }}
          style={{
            height:
              Platform.OS !== "web"
                ? null
                : Dimensions.get("window").height - 80
          }}
        >
          {this.state.CommentLoading == true ? (
            <ShadowSkeletonComment />
          ) : (
            <View
              style={{
                marginHorizontal:
                  Dimensions.get("window").width <= 750 ? 5 : 10,
                marginTop: Dimensions.get("window").width <= 750 ? 10 : 0
              }}
            >
              {this.state.CommentNotificationList.map((item, i) => {
                if (item.node.contents.__typename == "CommentNotification") {
                  return (
                    <View
                      style={[
                        ButtonStyle.borderStyle, ButtonStyle.shadowStyle,
                        {
                          backgroundColor: "#fff",
                          marginBottom: 10,
                          padding: 5,
                          width: '100%'
                        }
                      ]}
                    >
                      <TouchableOpacity
                        onPress={() => {
                          this.goToPostDetailsScreen(
                            item.node.contents.post && item.node.contents.post.id && item.node.contents.post.id.replace("Post:", ""),
                            item.node.contents.post && item.node.contents.post.title && item.node.contents.post.title
                          )
                          item.node.contents.my_content ? this.props.setNotificationId(item.node.contents.my_content && item.node.contents.my_content.id) : ''
                        }
                        }
                        style={{
                          flexDirection: "row",
                          width: "100%"
                        }}
                      >
                        <View
                          style={{
                            width: "10%",
                            justifyContent: "center"
                          }}
                        >
                          <Icon
                            color={"#4169e1"}
                            name={"comment"}
                            type="font-awesome"
                            size={30}
                            iconStyle={{ alignSelf: "center" }}
                            containerStyle={{ alignSelf: "center" }}
                          />
                          <Text
                            style={{
                              marginTop: 10,
                              textAlign: "center",
                              color: "#000",
                              fontSize: 12,
                              fontWeight: "bold",
                              fontFamily: ConstantFontFamily.defaultFont
                            }}
                          >
                            {" "}
                            {moment
                              .utc(item.node.created)
                              .local()
                              .fromNow(true)}{" "}
                          </Text>
                        </View>

                        <View
                          style={{
                            width: "90%"
                          }}
                        >
                          <View
                            style={{
                              width: "100%",
                              padding: 20,
                            }}
                          >
                            <Text
                              style={{
                                color: "#000",
                                fontSize: 15,
                                fontWeight: "bold",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              {item.node.contents.my_content.__typename == "Comment" ?
                                item.node.contents.my_content && item.node.contents.my_content.author &&
                                "@" + item.node.contents.my_content.author
                                  .username : "Post:"}
                              {" "}
                            
                            <Text
                              style={{
                                color: "#000",
                                fontFamily: ConstantFontFamily.VerdanaFont,
                                fontSize: 13,
                                // lineHeight: 1.5,
                                // userSelect: "text",
                                fontWeight: 'bold',                               
                              }}
                            >{" "}


                              {
                                item.node.contents.my_content.__typename == "Comment" ?
                                  item.node.contents.my_content && item.node.contents.my_content.text &&
                                  item.node.contents.my_content.text.replace(regex, '').substring(0, 150)
                                  :
                                  item.node.contents.my_content && item.node.contents.my_content.title &&
                                  item.node.contents.my_content.title
                              }


                            </Text>
                            </Text>
                          </View>

                          <View
                            style={{
                              // flex: 1,
                              // justifyContent: "flex-start",
                              // flexDirection: "row",
                              marginBottom: 10,
                              padding: 20
                            }}
                          >
                            <Text
                              style={{
                                color: "#000",
                                fontSize: 15,
                                fontWeight: "bold",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              @
                              {item.node.contents.their_comment && item.node.contents.their_comment.author &&
                                item.node.contents.their_comment.author
                                  .username}{" "}
                            
                            <Text
                              style={{
                                color: "#000",
                                fontFamily: ConstantFontFamily.VerdanaFont,
                                fontSize: 13,
                                // lineHeight: 1.5,
                                // userSelect: "text",
                                fontWeight: 'bold',
                              }}
                            >
                              {" "}
                              {item.node.contents.their_comment && item.node.contents.their_comment.text &&
                                item.node.contents.their_comment.text.replace(regex, '').substring(0, 150)}
                              {"..."}
                            </Text>
                            </Text>
                          </View>
                        </View>

                        <View
                          style={{ height: 2, backgroundColor: "#e1e1e1" }}
                        ></View>
                      </TouchableOpacity>
                    </View>
                  );
                }

                else if (
                  item.node.contents.__typename ==
                  "ClikInviteRequestNotification"
                ) {
                  return (
                    <TouchableOpacity
                      style={[
                        ButtonStyle.borderStyle, ButtonStyle.shadowStyle,
                        {
                          flexDirection: "row",
                          backgroundColor: "#fff",
                          marginBottom: 10,
                          padding: 5,
                          width: '100%'
                        }
                      ]}
                    >
                      <View
                        style={{
                          width: "10%",
                          justifyContent: "center"
                        }}
                      >
                        <Icon
                          color={"#4169e1"}
                          name={"hashtag"}
                          type="font-awesome"
                          size={30}
                          iconStyle={{ alignSelf: "center" }}
                          containerStyle={{ alignSelf: "center" }}
                        />
                        <Text
                          style={{
                            marginTop: 10,
                            textAlign: "center",
                            color: "#000",
                            fontSize: 12,
                            fontWeight: "bold",
                            fontFamily: ConstantFontFamily.defaultFont
                          }}
                        >
                          {moment
                            .utc(item.node.created)
                            .local()
                            .fromNow(true)}{" "}
                        </Text>
                      </View>

                      <View
                        style={{
                          width: "90%"
                        }}
                      >
                        <View
                          style={{
                            flexDirection: "column",
                            alignItems: "center",
                            justifyContent: 'center',
                            width: "100%"
                          }}
                        >
                          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text
                              style={{
                                color: "#000",
                                fontWeight: "bold",
                                fontSize: 15,
                                fontFamily: ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              {" "}
                            @{item.node.contents.inviter.username}
                            </Text>

                            <Text
                              style={{
                                color: "#000",
                                fontSize: 13,
                                fontWeight: "bold",
                                fontFamily: ConstantFontFamily.defaultFont
                              }}
                            >
                              {" "}
                            invited you to{" "}
                            </Text>
                          </View>

                          <TouchableOpacity
                            style={{
                              justifyContent: 'center',
                              padding: 5,
                              marginTop: 10,
                              backgroundColor: "#E8F5FA",
                              borderRadius: 10
                            }}
                            onPress={() =>
                              this.goToClikProfile(item.node.contents.clik.name)
                            }
                          >
                            <Hoverable>
                              {isHovered => (
                                <Text
                                  style={{
                                    width: "100%",
                                    color: "#4169e1",
                                    fontSize: 15,
                                    fontWeight: "bold",
                                    fontFamily:
                                      ConstantFontFamily.MontserratBoldFont,
                                    textDecorationLine:
                                      isHovered == true ? "underline" : "none"
                                  }}
                                >
                                  {" "}
                                  #{item.node.contents.clik.name}{" "}
                                </Text>
                              )}
                            </Hoverable>
                          </TouchableOpacity>
                        </View>

                        <View
                          style={{
                            justifyContent: 'space-around',
                            flexDirection: "row",
                            // marginBottom: 10,
                            marginTop: 10,
                            paddingHorizontal: "10%"
                          }}
                        >
                          <Button
                            title={"Join"}
                            titleStyle={ButtonStyle.wtitleStyle}
                            buttonStyle={[ButtonStyle.gbackgroundStyle, { paddingVertical: 4, paddingHorizontal: 10, borderWidth: 0 }]}
                            containerStyle={[ButtonStyle.containerStyle, { marginBottom: 0 }]}
                            onPress={() =>
                              this.joinClik(
                                item.node.contents.clik.id,
                                item.node.id
                              )
                            }
                          />

                          <Button
                            title={"Reject"}
                            titleStyle={ButtonStyle.wtitleStyle}
                            buttonStyle={[ButtonStyle.rbackgroundStyle, { paddingVertical: 4, paddingHorizontal: 10, borderWidth: 0 }]}
                            containerStyle={[ButtonStyle.containerStyle, { marginBottom: 0 }]}
                            onPress={() =>
                              this.deleteAccountNotifications(item.node.id)
                            }
                          />
                        </View>
                      </View>
                    </TouchableOpacity>
                  );
                }

                else if (
                  item.node.contents.__typename ==
                  "ClikInviteAcceptedNotification"
                ) {
                  return (
                    // <View
                    //   style={[
                    //     ButtonStyle.borderStyle, ButtonStyle.shadowStyle,
                    //     {
                    //       backgroundColor: "#fff",
                    //       marginBottom: 10,
                    //       padding: 5,
                    //       width: '100%'
                    //     }
                    //   ]}
                    // >
                    //   <TouchableOpacity
                    //     style={{ flexDirection: "row" }}
                    //   >
                    //     <View
                    //       style={{
                    //         width: "10%",
                    //         justifyContent: "center"
                    //       }}
                    //     >
                    //       <Icon
                    //         color={"#4169e1"}
                    //         name={"hashtag"}
                    //         type="font-awesome"
                    //         size={30}
                    //         iconStyle={{ alignSelf: "center" }}
                    //         containerStyle={{ alignSelf: "center" }}
                    //       />
                    //       <Text
                    //         style={{
                    //           marginTop: 10,
                    //           textAlign: "center",
                    //           color: "#000",
                    //           fontSize: 12,
                    //           fontWeight: "bold",
                    //           fontFamily: ConstantFontFamily.defaultFont
                    //         }}
                    //       >
                    //         {moment
                    //           .utc(item.node.created)
                    //           .local()
                    //           .fromNow(true)}{" "}
                    //       </Text>
                    //     </View>
                    //     <View
                    //       style={{
                    //         width: "85%"
                    //       }}
                    //     >
                    //       <View
                    //         style={{
                    //           // width: "100%",
                    //           padding: 5,
                    //           flexDirection: "column",
                    //           alignSelf: "center",
                    //           alignItems: "center",
                    //           justifyContent: 'center'
                    //         }}
                    //       >
                    //         <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    //           <Text
                    //             style={{
                    //               color: "#000",
                    //               fontWeight: "bold",
                    //               fontSize: 15,
                    //               fontFamily:
                    //                 ConstantFontFamily.MontserratBoldFont
                    //             }}
                    //           >
                    //             @{item.node.contents.invitee.username}
                    //           </Text>

                    //           <Text
                    //             style={{
                    //               color: "#000",
                    //               fontSize: 13,
                    //               fontWeight: "bold",
                    //               fontFamily: ConstantFontFamily.defaultFont
                    //             }}
                    //           >
                    //             {" "}
                    //             {/* accepted your invitation to */}
                    //         Joined
                    //         {" "}
                    //           </Text>
                    //         </View>

                    //         <TouchableOpacity
                    //           style={{
                    //             justifyContent: 'center',
                    //             padding: 5,
                    //             marginTop: 10,
                    //             backgroundColor: "#E8F5FA",
                    //             borderRadius: 10
                    //           }}
                    //           onPress={() =>
                    //             this.goToClikProfile(
                    //               item.node.contents.clik.name
                    //             )
                    //           }
                    //         >
                    //           <Hoverable>
                    //             {isHovered => (
                    //               <Text
                    //                 style={{
                    //                   width: "100%",
                    //                   color: "#4169e1",
                    //                   fontSize: 15,
                    //                   fontWeight: "bold",
                    //                   fontFamily:
                    //                     ConstantFontFamily.MontserratBoldFont,
                    //                   textDecorationLine:
                    //                     isHovered == true ? "underline" : "none"
                    //                 }}
                    //               >
                    //                 {" "}
                    //               #{item.node.contents.clik.name}{" "}
                    //               </Text>
                    //             )}
                    //           </Hoverable>
                    //         </TouchableOpacity>
                    //       </View>
                    //     </View>
                    //   </TouchableOpacity>
                    //   {this.getUserStar(item.node.contents.invitee.username) ==
                    //     -1 && (
                    //       <View style={{ alignSelf: "center", marginBottom: 10 }}>
                    //         <Button
                    //           title="Follow User"
                    //           titleStyle={ButtonStyle.wtitleStyle}
                    //           buttonStyle={[ButtonStyle.gbackgroundStyle, { paddingVertical: 4, paddingHorizontal: 10 }]}
                    //           containerStyle={[ButtonStyle.containerStyle, { marginBottom: 0 }]}
                    //           onPress={() =>
                    //             this.followUser(
                    //               item.node.contents.invitee.id,
                    //               item.node.id
                    //             )
                    //           }
                    //         />
                    //       </View>
                    //     )}
                    // </View>
                    <View
                      style={[
                        ButtonStyle.borderStyle, ButtonStyle.shadowStyle,
                        {
                          backgroundColor: "#fff",
                          marginBottom: 10,
                          padding: 5,
                          width: '100%'
                        }
                      ]}
                    >
                      <TouchableOpacity
                        style={{ flexDirection: "row" }}
                      >
                        <View
                          style={{
                            width: "10%",
                            justifyContent: "center"
                          }}
                        >
                          <Icon
                            color={"#4169e1"}
                            name={"hashtag"}
                            type="font-awesome"
                            size={30}
                            iconStyle={{ alignSelf: "center" }}
                            containerStyle={{ alignSelf: "center" }}
                          />
                          <Text
                            style={{
                              marginTop: 10,
                              textAlign: "center",
                              color: "#000",
                              fontSize: 12,
                              fontWeight: "bold",
                              fontFamily: ConstantFontFamily.defaultFont
                            }}
                          >
                            {moment
                              .utc(item.node.created)
                              .local()
                              .fromNow(true)}{" "}
                          </Text>
                        </View>
                        <View
                          style={{
                            width: "85%"
                          }}
                        >
                          <View
                            style={{
                              // width: "100%",
                              padding: 5,
                              flexDirection: "column",
                              alignSelf: "center",
                              alignItems: "center",
                              justifyContent: 'center'
                            }}
                          >
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                              <Text
                                style={{
                                  color: "#000",
                                  fontWeight: "bold",
                                  fontSize: 15,
                                  fontFamily:
                                    ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                @{item.node.contents.invitee.username}
                              </Text>

                              <Text
                                style={{
                                  color: "#000",
                                  fontSize: 13,
                                  fontWeight: "bold",
                                  fontFamily: ConstantFontFamily.defaultFont
                                }}
                              >
                                {" "}
                                {/* accepted your invitation to */}
                              joined
                              {" "}
                              </Text>
                            </View>

                            <TouchableOpacity
                              style={{
                                justifyContent: 'center',
                                padding: 5,
                                marginTop: 10,
                                backgroundColor: "#E8F5FA",
                                borderRadius: 10
                              }}
                              onPress={() =>
                                this.goToClikProfile(
                                  item.node.contents.clik.name
                                )
                              }
                            >
                              <Hoverable>
                                {isHovered => (
                                  <Text
                                    style={{
                                      width: "100%",
                                      color: "#4169e1",
                                      fontSize: 15,
                                      fontWeight: "bold",
                                      fontFamily:
                                        ConstantFontFamily.MontserratBoldFont,
                                      textDecorationLine:
                                        isHovered == true ? "underline" : "none"
                                    }}
                                  >
                                    {" "}
                                    #{item.node.contents.clik.name}{" "}
                                  </Text>
                                )}
                              </Hoverable>
                            </TouchableOpacity>
                            {this.getUserStar(item.node.contents.invitee.username) ==
                              -1 && (
                                <Button
                                  title="Follow User"
                                  titleStyle={ButtonStyle.wtitleStyle}
                                  buttonStyle={[ButtonStyle.gbackgroundStyle, { paddingVertical: 4, paddingHorizontal: 10, borderWidth: 0 }]}
                                  containerStyle={[ButtonStyle.containerStyle, { marginBottom: 0, height: '', }]}
                                  onPress={() =>
                                    this.followUser(
                                      item.node.contents.invitee.id,
                                      item.node.id
                                    )
                                  }
                                />
                              )}
                          </View>
                        </View>
                      </TouchableOpacity>
                    </View>
                  );
                }

                else if (
                  item.node.contents.__typename == "ClikJoinRequestNotification"
                ) {
                  return (
                    <View
                      style={[
                        ButtonStyle.borderStyle, ButtonStyle.shadowStyle,
                        {
                          backgroundColor: "#fff",
                          marginBottom: 10,
                          padding: 5,
                          width: '100%'
                        }
                      ]}
                    >
                      <TouchableOpacity
                        style={{ flexDirection: "row" }}
                      >
                        <View
                          style={{
                            width: "10%",
                            justifyContent: "center"
                          }}
                        >
                          <Icon
                            color={"#4169e1"}
                            name={"hashtag"}
                            type="font-awesome"
                            size={30}
                            iconStyle={{ alignSelf: "center" }}
                            containerStyle={{ alignSelf: "center" }}
                          />
                          <Text
                            style={{
                              marginTop: 10,
                              textAlign: "center",
                              color: "#000",
                              fontSize: 12,
                              fontWeight: "bold",
                              fontFamily: ConstantFontFamily.defaultFont
                            }}
                          >
                            {moment
                              .utc(item.node.created)
                              .local()
                              .fromNow(true)}{" "}
                          </Text>
                        </View>

                        <View
                          style={{
                            width: "85%"
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              alignItems: "center",
                              // width: "100%",
                              justifyContent: 'center'
                            }}
                          >
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                              <Text
                                style={{
                                  color: "#000",
                                  fontWeight: "bold",
                                  fontSize: 15,
                                  fontFamily:
                                    ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                @{item.node.contents.invitee.username}
                              </Text>

                              <Text
                                style={{
                                  color: "#000",
                                  fontSize: 13,
                                  fontWeight: "bold",
                                  fontFamily: ConstantFontFamily.defaultFont
                                }}
                              >
                                {" "}
                                  wants to join{" "}
                              </Text>
                            </View>

                            <TouchableOpacity
                              style={{
                                justifyContent: "center",
                                padding: 5,
                                marginTop: 10,
                                backgroundColor: "#E8F5FA",
                                borderRadius: 10
                              }}
                              onPress={() =>
                                this.goToClikProfile(
                                  item.node.contents.clik.name
                                )
                              }
                            >
                              <Hoverable>
                                {isHovered => (
                                  <Text
                                    style={{
                                      width: "100%",
                                      color: "#4169e1",
                                      fontSize: 15,
                                      fontWeight: "bold",
                                      fontFamily:
                                        ConstantFontFamily.MontserratBoldFont,
                                      textDecorationLine:
                                        isHovered == true ? "underline" : "none"
                                    }}
                                  >
                                    {" "}
                                  #{item.node.contents.clik.name}{" "}
                                  </Text>
                                )}
                              </Hoverable>
                            </TouchableOpacity>
                          </View>
                          <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                            <Text
                              style={{
                                fontFamily: ConstantFontFamily.defaultFont,
                                color: "#000",
                                fontWeight: "bold"
                              }}
                            >
                              Qualification :{" "}

                            </Text>
                            <Text style={{ fontFamily: ConstantFontFamily.defaultFont, }}>
                              {item.node.contents.clik.qualifications.length > 0
                                ? item.node.contents.clik.qualifications[0]
                                : ""}
                            </Text>
                          </View>

                          <View
                            style={{
                              justifyContent: 'space-around',
                              flexDirection: "row",
                              // marginBottom: 10,
                              marginTop: 10,
                              paddingHorizontal: "10%"
                            }}
                          >
                            <Button
                              title={"Accept"}
                              titleStyle={ButtonStyle.wtitleStyle}
                              buttonStyle={[ButtonStyle.gbackgroundStyle, { paddingVertical: 4, paddingHorizontal: 10, borderWidth: 0 }]}
                              containerStyle={[ButtonStyle.containerStyle, { marginBottom: 0 }]}
                              onPress={() =>
                                this.inviteClik(
                                  item.node.contents.clik.id,
                                  item.node.contents.invitee.id,
                                  item.node.id
                                )
                              }
                            />

                            <Button
                              title={"Reject"}
                              titleStyle={ButtonStyle.wtitleStyle}
                              buttonStyle={[ButtonStyle.rbackgroundStyle, { paddingVertical: 4, paddingHorizontal: 10, borderWidth: 0 }]}
                              containerStyle={[ButtonStyle.containerStyle, { marginBottom: 0 }]}
                              onPress={() =>
                                this.rejectClik(
                                  item.node.contents.clik.id,
                                  item.node.contents.invitee.id,
                                  item.node.id
                                )
                              }
                            />
                          </View>
                        </View>
                      </TouchableOpacity>
                    </View>
                  );
                }

                else if (
                  item.node.contents.__typename ==
                  "ClikJoinAcceptedNotification"
                ) {
                  return (
                    <View
                      style={[
                        ButtonStyle.borderStyle, ButtonStyle.shadowStyle,
                        {
                          backgroundColor: "#fff",
                          marginBottom: 10,
                          padding: 5,
                          width: '100%'
                        }
                      ]}
                    >
                      <View
                        style={{ flexDirection: "row" }}
                      >
                        <View
                          style={{
                            width: "10%",
                            justifyContent: "center"
                          }}
                        >
                          <Icon
                            color={"#4169e1"}
                            name={"hashtag"}
                            type="font-awesome"
                            size={30}
                            iconStyle={{ alignSelf: "center" }}
                            containerStyle={{ alignSelf: "center" }}
                          />
                          <Text
                            style={{
                              marginTop: 10,
                              textAlign: "center",
                              color: "#000",
                              fontSize: 12,
                              fontWeight: "bold",
                              fontFamily: ConstantFontFamily.defaultFont
                            }}
                          >
                            {" "}
                            {moment
                              .utc(item.node.created)
                              .local()
                              .fromNow(true)}{" "}
                          </Text>
                        </View>
                        <View
                          style={{
                            width: "85%"
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "column",
                              alignItems: "center",
                              justifyContent: 'center'
                            }}
                          >
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                              <Text
                                style={{
                                  fontWeight: "bold",
                                  color: "#000",
                                  fontSize: 16,
                                  fontFamily:
                                    ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                {" "}
                              @{item.node.contents.invitee.username}
                              </Text>
                              <Text
                                style={{
                                  fontFamily: ConstantFontFamily.defaultFont,
                                  color: "#000",
                                  fontWeight: "bold"
                                }}
                              >
                                {" "}accepted your request to join{" "}
                              </Text>
                            </View>
                            <TouchableOpacity
                              style={{
                                justifyContent: "center",
                                padding: 5,
                                marginTop: 10,
                                backgroundColor: "#E8F5FA",
                                borderRadius: 10
                              }}
                              onPress={() =>
                                this.goToClikProfile(
                                  item.node.contents.clik.name
                                )
                              }
                            >
                              <Hoverable>
                                {isHovered => (
                                  <Text
                                    style={{
                                      width: "100%",
                                      color: "#4169e1",
                                      fontSize: 15,
                                      fontWeight: "bold",
                                      fontFamily:
                                        ConstantFontFamily.MontserratBoldFont,
                                      textDecorationLine:
                                        isHovered == true ? "underline" : "none"
                                    }}
                                  >
                                    {" "}
                                    #{item.node.contents.clik.name}{" "}
                                  </Text>
                                )}
                              </Hoverable>
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    </View>
                  );
                }

                else if (
                  item.node.contents.__typename ==
                  "UserInviteAcceptedNotification"
                ) {
                  return (
                    <View
                      style={[
                        ButtonStyle.borderStyle, ButtonStyle.shadowStyle,
                        {
                          backgroundColor: "#fff",
                          marginBottom: 10,
                          padding: 5,
                          width: "100%"
                        }
                      ]}
                    >
                      <View
                        style={{ flexDirection: "row" }}
                      >
                        <View
                          style={{
                            width: "10%",
                            justifyContent: "center"
                          }}
                        >
                          <Image
                            source={require("../assets/image/default-image.png")}
                            style={{
                              width: 40,
                              height: 40,
                              borderRadius: 30,
                              borderWidth: 1,
                              borderColor: "#e1e1e1",
                              alignSelf: "center",
                              marginBottom: 10
                            }}
                          />
                          <Text
                            style={{
                              marginTop: 10,
                              textAlign: "center",
                              color: "#000",
                              fontSize: 12,
                              fontWeight: "bold",
                              fontFamily: ConstantFontFamily.defaultFont
                            }}
                          >
                            {moment
                              .utc(item.node.created)
                              .local()
                              .fromNow(true)}{" "}
                          </Text>
                        </View>

                        <View
                          style={{
                            width: "88%"
                          }}
                        >
                          <View
                            style={{
                              width: "100%",
                              padding: 10,
                              alignItems: "center"
                            }}
                          >
                            <Text
                              style={{
                                color: "#000",
                                fontSize: 15,
                                fontWeight: "bold",
                                paddingBottom: 10,
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              @{item.node.contents.invitee.username}
                              <Text
                                style={{
                                  color: "#000",
                                  fontSize: 13,
                                  fontWeight: "bold",
                                  fontFamily: ConstantFontFamily.defaultFont
                                }}
                              >
                                {" "}
                              accepted your invitation to weclikd!
                            </Text>
                            </Text>
                          </View>

                          {this.getUserStar(
                            item.node.contents.invitee.username
                          ) == -1 && (
                              <View
                                style={{ alignSelf: "center", marginBottom: 10 }}
                              >
                                <Button
                                  title="Follow User"
                                  titleStyle={ButtonStyle.wtitleStyle}
                                  buttonStyle={[ButtonStyle.gbackgroundStyle, { paddingVertical: 4, paddingHorizontal: 10, borderWidth: 0 }]}
                                  containerStyle={[ButtonStyle.containerStyle, { marginBottom: 0 }]}
                                  onPress={() =>
                                    this.followUser(
                                      item.node.contents.invitee.id,
                                      item.node.id
                                    )
                                  }
                                />
                              </View>
                            )}
                        </View>
                        <View
                          style={{ height: 2, backgroundColor: "#e1e1e1" }}
                        ></View>
                      </View>
                    </View>
                  );
                }
              })}
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  listTrending_cliks: !state.TrendingCliksReducer.getIn(["Trending_cliks_List"])
    ? List()
    : state.TrendingCliksReducer.getIn(["Trending_cliks_List"]),
  link: state.LinkPostReducer.get("link"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  getUserFollowUserList: state.LoginUserDetailsReducer.get("userFollowUserList")
    ? state.LoginUserDetailsReducer.get("userFollowUserList")
    : List()
});

const mapDispatchToProps = dispatch => ({
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload)),
  setComment: payload => dispatch({ type: "SET_COMMENT", payload }),
  setNotificationId: payload => dispatch({ type: "GET_NOTIFICATION_ID", payload }),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  ClikId: payload => dispatch(getTrendingCliksProfileDetails(payload))
});

const NotificationScreenWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(NotificationScreen);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  NotificationScreenWrapper
);
