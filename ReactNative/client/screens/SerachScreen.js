import { List } from "immutable";
import moment from "moment";
import React, { createRef, Component } from "react";
import { graphql } from "react-apollo";
import {
  Animated,
  Dimensions,
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View,
  FlatList
} from "react-native";
import { Icon } from "react-native-elements";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { TabBar, TabView } from "react-native-tab-view";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { listClikMembers } from "../actionCreator/ClikMembersAction";
import { listClikUserRequest } from "../actionCreator/ClikUserRequestAction";
import { getFeedProfileDetails } from "../actionCreator/FeedProfileAction";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { listTopicFeed } from "../actionCreator/TopicsFeedAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { getTrendingTopicsProfileDetails } from "../actionCreator/TrendingTopicsProfileAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import applloClient from "../client";
import SearchClik from "../components/SearchClik";
import SearchFeed from "../components/SearchFeed";
import SearchTopic from "../components/SearchTopic";
import SearchUser from "../components/SearchUser";
import AppHelper from "../constants/AppHelper";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import {
  ClikFollowMutation,
  ClikUnfollowMutation,
  FeedFollowMutation,
  FeedUnFollowMutation,
  TopicFollowMutation,
  TopicUnFollowMutation,
  UserFollowMutation,
  UserUnfollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import {
  SearchClikMutation,
  SearchEveryThingMutation,
  SearchFeedMutation,
  SearchTopicMutation,
  SearchUserMutation
} from "../graphqlSchema/graphqlMutation/SearchMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  ClikFollowVariables,
  ClikUnfollowVariables,
  FeedFollowVariables,
  FeedUnFollowVariables,
  TopicFollowVariables,
  TopicUnFollowVariables,
  UserFollowVariables,
  UserUnfollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import {
  SearchClikVariables,
  SearchEveryVariables,
  SearchFeedVariables,
  SearchTopicVariables,
  SearchUserVariables
} from "../graphqlSchema/graphqlVariables/SearchVariables";
import { capitalizeFirstLetter } from "../library/Helper";
import NavigationService from "../library/NavigationService";
import ShadowSkeleton from "../components/ShadowSkeleton";
import { FlatGrid } from "react-native-super-grid";
import FeedList from "../components/FeedList";
import CommentDetailScreen from "./CommentDetailScreen";
import consolaGlobalInstance from "consola";
import SearchInput from "../components/SearchInput";
import ButtonStyle from "../constants/ButtonStyle";
import BottomScreen from "../components/BottomScreen";

let feedID = [];

class SearchScreen extends React.PureComponent {
  _isMounted = false;
  flatListRef = createRef();
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.Pagescrollview = null;
    this.state = {
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      //term: "",
      PostList: [],
      ClikList: [],
      TopicList: [],
      UserList: [],
      FeedList: [],
      SuggestList: [],
      TopicSuggestList: [],
      ClikSuggestList: [],
      UserSuggestList: [],
      FeedSuggestList: [],
      FilterItems: [
        {
          label: "POSTS",
          value: "POSTS",
          key: 1
        },
        {
          label: "TOPICS",
          value: "TOPICS",
          key: 2
        },
        {
          label: "CLIKS",
          value: "CLIKS",
          key: 3
        },
        {
          label: "USERS",
          value: "USERS",
          key: 4
        },
        {
          label: "FEEDS",
          value: "FEEDS",
          key: 5
        }
      ],
      SelectFilterItem: this.props.navigation
        .getParam("type", "NO-ID")
        .toUpperCase(),
      selectedItems: [],
      TermList: [],
      selection: {
        start: 1,
        end: 0
      },
      index: this.getIndex(),
      routes: [
        { key: "zero", title: "Posts", icon: "file", type: "font-awesome" },
        { key: "first", title: "Cliks", icon: "users", type: "font-awesome" },
        { key: "second", title: "Topics", icon: "tag", type: "font-awesome" },
        { key: "third", title: "Users", icon: "user", type: "font-awesome" },
        { key: "fourth", title: "Feeds", icon: "rss", type: "font-awesome" }
      ],
      showTips: true,
      activeFeed: -1,
      activeId: "",
      activeTitle: "",
      feedY: 0
    };
  }

  getSymbol = () => {
    switch (this.props.navigation.getParam("type", "NO-ID").toUpperCase()) {
      case "TOPICS":
        return "/";
      case "CLIKS":
        return "#";
      case "USERS":
        return "@";
      case "FEEDS":
        return "%";
      default:
        return "";
    }
  };

  getIndex = () => {
    switch (this.props.navigation.getParam("type", "NO-ID").toUpperCase()) {
      case "TOPICS":
        return 1;
      case "CLIKS":
        return 2;
      case "USERS":
        return 3;
      case "FEEDS":
        return 4;
      default:
        return 0;
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (
      prevProps.navigation.getParam("type", "NO-ID") !==
      this.props.navigation.getParam("type", "NO-ID")
    ) {
      this.setState({
        SelectFilterItem: this.props.navigation
          .getParam("type", "NO-ID")
          .toUpperCase(),
        index: this.getIndex()
      });
      // this.input.focus();
    }
    if (prevProps.term != this.props.term) {
      this.getRespectiveSuggestions(this.props.term);
    }
    if (prevProps.keyEvent != this.props.keyEvent) {
      if (this.props.keyEvent === 13) {
        this.setState({
          TopicSuggestList: [],
          ClikSuggestList: [],
          UserSuggestList: [],
          FeedSuggestList: []
        });
        this.getSuggestion(this.props.term);
      }
    }

    if (prevProps.termWeb != this.props.termWeb) {
      this.getRespectiveSuggestions(this.props.termWeb);
    }
    if (prevProps.keyEventWeb != this.props.keyEventWeb) {
      if (this.props.keyEventWeb === 13) {
        this.setState({
          TopicSuggestList: [],
          ClikSuggestList: [],
          UserSuggestList: [],
          FeedSuggestList: []
        });
        this.getSuggestion(this.props.termWeb);
      }
    }
    // setTimeout(() => {
    //   if (prevProps.trendingHomeFeedId == this.props.trendingHomeFeedId) {
    //     this.props.setPostCommentDetails({
    //       id:
    //         this.state.activeId != ""
    //           ? this.state.activeId
    //           : this.state.PostList.length > 0
    //           ? "Search" + this.state.PostList[0].id
    //           : "",
    //       title:
    //         this.state.activeTitle != ""
    //           ? this.state.activeTitle
    //           : this.state.PostList.length > 0
    //           ? this.state.PostList[0].title
    //           : ""
    //     });
    //     this.makeHighlight(
    //       this.state.activeId != ""
    //         ? this.state.activeId
    //         : this.state.PostList.length > 0
    //         ? "Search" + this.state.PostList[0].id
    //         : "",
    //       this.state.activeTitle != ""
    //         ? this.state.activeTitle
    //         : this.state.PostList.length > 0
    //         ? this.state.PostList[0].title
    //         : ""
    //     );
    //   }
    // }, 2000);
    if (this.state.PostList.length == 0) {
      this.props.setPostCommentReset({
        payload: [],
        postId: "",
        title: "",
        loading: true
      });
    }
  };

  // handleInput = e => {

  //   var code = e.keyCode || e.which;
  //   if (code === 13) {
  //     this.setState({
  //       TopicSuggestList: [],
  //       ClikSuggestList: [],
  //       UserSuggestList: [],
  //       FeedSuggestList: []
  //     });
  //     this.getSuggestion(this.props.term);
  //   }
  // };

  getSuggestion = value => {
    let topics = [];
    let cliks = [];
    let users = [];
    let feeds = [];
    let terms = [];
    var TotalTerm = value.split(" ");
    this.setState({
      TermList: TotalTerm
    });
    for (var i = 0; i < TotalTerm.length; i++) {
      if (TotalTerm[i].startsWith("#") == true) {
        cliks.push(TotalTerm[i].replace("#", ""));
      } else if (TotalTerm[i].startsWith("/") == true) {
        topics.push(TotalTerm[i].replace("/", ""));
      } else if (TotalTerm[i].startsWith("@") == true) {
        users.push(TotalTerm[i].replace("@", ""));
      } else if (TotalTerm[i].startsWith("%") == true) {
        feeds.push(TotalTerm[i].replace("%", "").replace("-", " "));
      } else {
        terms.push(TotalTerm[i]);
      }
    }
    SearchEveryVariables.variables.topics = topics;
    SearchEveryVariables.variables.cliks = cliks;
    SearchEveryVariables.variables.users = users;
    SearchEveryVariables.variables.feeds = feeds;
    SearchEveryVariables.variables.terms = terms;
    applloClient
      .query({
        query: SearchEveryThingMutation,
        ...SearchEveryVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        this.setState(
          {
            showTips: false,
            ClikList: res.data.search.everything.cliks,
            PostList: res.data.search.everything.posts,
            TopicList: res.data.search.everything.topics,
            UserList: res.data.search.everything.users,
            FeedList: res.data.search.everything.feeds
          },
          () => {
            this.flatListRef.current &&
              this.flatListRef.current.scrollToOffset({
                x: 0,
                y: 0,
                animated: true
              });
            setTimeout(() => {
              this.props.setPostCommentDetails({
                id:
                  this.state.PostList.length > 0
                    ? "Search" + this.state.PostList[0].id
                    : "",
                title:
                  this.state.PostList.length > 0
                    ? this.state.PostList[0].title
                    : ""
              });
              this.makeHighlight(
                this.state.PostList.length > 0
                  ? "Search" + this.state.PostList[0].id
                  : "",
                this.state.PostList.length > 0
                  ? this.state.PostList[0].title
                  : ""
              );
            }, 2000);
          }
        );
      });
  };

  getRespectiveSuggestions = async value => {
    var TotalTerm = value.split(" ");
    for (var i = 0; i < TotalTerm.length; i++) {
      if (TotalTerm[i].startsWith("#") == true || this.state.index == 1) {
        await this.setState({
          TopicSuggestList: [],
          UserSuggestList: [],
          FeedSuggestList: []
        });
        await this.getClikSuggestion(TotalTerm[i].replace("#", ""));
      } else if (
        TotalTerm[i].startsWith("/") == true ||
        this.state.index == 2
      ) {
        await this.setState({
          ClikSuggestList: [],
          UserSuggestList: [],
          FeedSuggestList: []
        });
        await this.getTopicSuggestion(TotalTerm[i].replace("/", ""));
      } else if (
        TotalTerm[i].startsWith("@") == true ||
        this.state.index == 3
      ) {
        await this.setState({
          TopicSuggestList: [],
          ClikSuggestList: [],
          FeedSuggestList: []
        });
        await this.getUserSuggestion(TotalTerm[i].replace("@", ""));
      } else if (
        TotalTerm[i].startsWith("%") == true ||
        this.state.index == 4
      ) {
        await this.setState({
          TopicSuggestList: [],
          ClikSuggestList: [],
          UserSuggestList: []
        });
        await this.getFeedSuggestion(TotalTerm[i].replace("%", ""));
      } else {
        await this.setState({
          TopicSuggestList: [],
          ClikSuggestList: [],
          UserSuggestList: [],
          FeedSuggestList: []
        });
      }
    }
  };

  getTopicSuggestion = value => {
    SearchTopicVariables.variables.prefix = value;
    applloClient
      .query({
        query: SearchTopicMutation,
        ...SearchTopicVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        let data = res.data.search.topics;
        this.setState({
          TopicSuggestList: data,
          ClikSuggestList: [],
          UserSuggestList: [],
          FeedSuggestList: []
        });
      });
  };

  getClikSuggestion = value => {
    SearchClikVariables.variables.prefix = value;
    applloClient
      .query({
        query: SearchClikMutation,
        ...SearchClikVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        let data = res.data.search.cliks;
        this.setState({
          ClikSuggestList: data,
          TopicSuggestList: [],
          UserSuggestList: [],
          FeedSuggestList: []
        });
      });
  };

  getUserSuggestion = value => {
    SearchUserVariables.variables.prefix = value;
    applloClient
      .query({
        query: SearchUserMutation,
        ...SearchUserVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        let data = res.data.search.users;
        this.setState({
          UserSuggestList: data,
          TopicSuggestList: [],
          ClikSuggestList: [],
          FeedSuggestList: []
        });
      });
  };

  getFeedSuggestion = value => {
    SearchFeedVariables.variables.prefix = value.replace("-", " ");
    applloClient
      .query({
        query: SearchFeedMutation,
        ...SearchFeedVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        let data = res.data.search.feeds;
        this.setState({
          FeedSuggestList: data,
          TopicSuggestList: [],
          ClikSuggestList: [],
          UserSuggestList: []
        });
      });
  };

  getTextColor = value => {
    let data = "#000";
    if (value.startsWith("#") == true) {
      data = "#4C82B6";
    } else if (value.startsWith("/") == true) {
      data = "#009B1A";
    } else if (value.startsWith("@") == true) {
      data = "#000";
    }
    return data;
  };

  getBackgroundColor = value => {
    let data = "#e1e1e1";
    if (value.startsWith("#") == true) {
      data = "#E8F5FA";
    } else if (value.startsWith("/") == true) {
      data = "#e3f9d5";
    } else if (value.startsWith("@") == true) {
      data = "#e1e1e1";
    }
    return data;
  };

  goToPostDetailsScreen = async id => {
    await this.props.setPostDetails({
      title: "Home",
      id: id
    });
    await this.props.setPostCommentDetails({
      id: id
    });
  };

  goToTopicProfile = async id => {
    this.props.topicId({
      id: id,
      type: "feed"
    });
  };

  goToClikProfile = id => {
    this.props.ClikId({
      id: id,
      type: "feed"
    });
    this.props.setClikUserRequest({
      id: id.replace("%3A", ":"),
      currentPage: AppHelper.PAGE_LIMIT
    });
    this.props.setClikMembers({
      id: id.replace("%3A", ":")
    });
  };

  putValue = async data => {
    let value = [];
    for (let i = 0; i < this.state.selectedItems.length; i++) {
      value.push(" " + this.state.selectedItems[i].name + " ");
    }
    await this.setState({
      term: value.toString().replace(",", "")
    });
    // this.input.focus();
  };

  followTopics = topicId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    TopicFollowVariables.variables.topic_id = topicId;
    TopicFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowTopics = async topicId => {
    TopicUnFollowVariables.variables.topic_id = topicId;
    applloClient
      .query({
        query: TopicUnFollowMutation,
        ...TopicUnFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  favroiteTopics = async topicId => {
    TopicFollowVariables.variables.topic_id = topicId;
    TopicFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  followCliks = cliksId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    ClikFollowVariables.variables.clik_id = cliksId;
    ClikFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: ClikFollowMutation,
        ...ClikFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowCliks = async cliksId => {
    ClikUnfollowVariables.variables.clik_id = cliksId;
    applloClient
      .query({
        query: ClikUnfollowMutation,
        ...ClikUnfollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  favroiteCliks = async cliksId => {
    ClikFollowVariables.variables.clik_id = cliksId;
    ClikFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: ClikFollowMutation,
        ...ClikFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  followUser = userId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    UserFollowVariables.variables.user_id = userId;
    UserFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: UserFollowMutation,
        ...UserFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowUser = async userId => {
    UserUnfollowVariables.variables.user_id = userId;
    applloClient
      .query({
        query: UserUnfollowMutation,
        ...UserUnfollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  favroiteUser = async userId => {
    UserFollowVariables.variables.user_id = userId;
    UserFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: UserFollowMutation,
        ...UserFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  followFeed = Id => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    FeedFollowVariables.variables.feed_id = Id;
    FeedFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: FeedFollowMutation,
        ...FeedFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowFeed = async Id => {
    FeedUnFollowVariables.variables.feed_id = Id;
    applloClient
      .query({
        query: FeedUnFollowMutation,
        ...FeedUnFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  favroiteFeed = async Id => {
    FeedFollowVariables.variables.feed_id = Id;
    FeedFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: FeedFollowMutation,
        ...FeedFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  getTopicStar = TopicName => {
    let index = 0;
    index = this.props.getUserFollowTopicList.findIndex(
      i =>
        i
          .getIn(["topic", "name"])
          .toLowerCase()
          .replace("topic:", "") ==
        TopicName.toLowerCase().replace("topic:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowTopicList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  getClikStar = ClikName => {
    let index = 0;
    index = this.props.getUserFollowCliksList.findIndex(
      i =>
        i
          .getIn(["clik", "name"])
          .toLowerCase()
          .replace("clik:", "") == ClikName.toLowerCase().replace("clik:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowCliksList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  getUserStar = UserName => {
    let index = 0;
    index = this.props.getUserFollowUserList.findIndex(
      i =>
        i
          .getIn(["user", "username"])
          .toLowerCase()
          .replace("user:", "") == UserName.toLowerCase().replace("user:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowUserList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  getFeedStar = Name => {
    let index = 0;
    index = this.props.getUserFollowFeedList.findIndex(
      i =>
        i
          .getIn(["feed", "name"])
          .toLowerCase()
          .replace("ExternalFeed:", "") ==
        Name.toLowerCase().replace("ExternalFeed:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowFeedList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  handleSelectionChange = ({ nativeEvent: { selection } }) =>
    this.setState({ selection });

  _renderTabBar = props => (
    Dimensions.get("window").width >= 750 &&
    <View
      style={{
        flexDirection: "row",
        width: "100%",
        borderWidth: 1,
        borderColor: "#C5C5C5",
        // borderRadius: 10,
        backgroundColor: "#fff",
        alignItems: "center",
        paddingHorizontal: 10,
        height: 30,
        // paddingBottom: 10
      }}
    >
      <TabBar
        {...props}
        indicatorStyle={{
          backgroundColor: "#009B1A",
          height: 3,
          borderRadius: 6
          //width: "14%",
          //marginHorizontal: "3%"
        }}
        style={{
          backgroundColor: "transparent",
          shadowColor: "transparent",
          width: "100%",
          height: 30,
          justifyContent:'center'
        }}
        labelStyle={{
          color: "#000",
          fontFamily: ConstantFontFamily.MontserratBoldFont,
          fontSize: 13,
          fontWeight: "bold"
        }}
        // renderIcon={({ route, focused, color }) => (
        //   <Icon
        //     name={route.icon}
        //     type={route.type}
        //     color={focused ? "#009B1A" : "#D3D3D3"}
        //   />
        // )}
        renderLabel={({ route, focused, color }) => (
          <Text
            style={{
              color: focused ? "#009B1A" : "#D3D3D3",
              fontFamily: ConstantFontFamily.MontserratBoldFont
            }}
          >
            {route.title}
          </Text>
        )}
        renderIndicator={({ route, focused, color }) => null}
      />
    </View>
  );

  getType = index => {
    switch (index) {
      case 1:
        return "TOPICS";
      //return "CLIKS";
      case 2:
        return "CLIKS";
      //return "TOPICS";
      case 3:
        return "USERS";
      case 4:
        return "FEEDS";
      default:
        return "POSTS";
    }
  };

  _handleIndexChange = index => {
    this.setState({ index });
    NavigationService.navigate("search", {
      type: this.getType(index)
    });
    this.props.setPostCommentReset({
      payload: [],
      postId: "",
      title: "",
      loading: true
    });
  };

  _renderLazyPlaceholder = ({ route }) => <ShadowSkeleton />;

  _renderItem = item => {
    var row = item.item;
    this.getfeedID("Search" + row.id);
    return (
      <FeedList
        loginModalStatusEventParent={null}
        item={{ item: { node: row } }}
        navigation={this.props.navigation}
        ViewMode={"Default"}
        highlight={this.makeHighlight}
        ActiveTab="Search"
      />
    );
  };

  getUnique = array => {
    var uniqueArray = [];
    for (let i = 0; i < array.length; i++) {
      if (uniqueArray.indexOf(array[i]) === -1) {
        uniqueArray.push(array[i]);
      }
    }
    return uniqueArray;
  };

  getfeedID = async id => {
    let data = feedID;
    await data.push(id);
    feedID = data;
  };

  makeHighlight = async (id, title) => {
    let newId = id.search("Search") != -1 ? id : "Search" + id;
    await this.setState({
      activeFeed: this.getUnique(feedID).indexOf(newId),
      activeId: id,
      activeTitle: title
    });
    this.setBorderColor();
  };

  setBorderColor = async () => {
    for (let i = 0; i < this.getUnique(feedID).length; i++) {
      let data = document.getElementById(this.getUnique(feedID)[i]);
      if (data != null && Dimensions.get("window").width >= 1000) {
        document.getElementById(this.getUnique(feedID)[i]).style.borderColor =
          i == this.state.activeFeed ? "green" : "#c5c5c5";
        if (i == this.state.activeFeed) {
          document.getElementById(this.getUnique(feedID)[i]).click();
        }
      }
    }
  };

  listScroll = value => {
    this.setState({
      feedY: value
    });
  };

  _renderScene = ({ route }) => {
    switch (route.key) {
      case "zero":
        return (
          <ScrollView showsVerticalScrollIndicator={false}>
            <View
              style={{
                //flex: 1,
                //marginTop: 10,
                //paddingRight: 5,
                flexDirection: "row",
                width: "100%",
                //justifyContent: "center",
                marginHorizontal: "auto"
              }}
            >
              {this.state.showTips == true ? (
                <View
                  style={{
                    marginTop: 10,
                    padding: 10,
                    // borderWidth: 1,
                    // borderColor: "#e1e1e1",
                    // borderRadius: 10,
                    width: "100%"
                  }}
                >
                  <View
                    style={{
                      borderColor: "#C5C5C5",
                      borderWidth: 1,
                      borderRadius: 10,
                      backgroundColor: "#fff",
                      padding: 10
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 18,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center",
                        marginBottom: 20
                      }}
                    >
                      Search Tips
                    </Text>

                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center"
                      }}
                    >
                      To filter results by topics, start the search term with /.
                    </Text>

                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center"
                      }}
                    >
                      To filter results by users, start the search term with @.
                    </Text>

                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center"
                      }}
                    >
                      To filter results by cliks, start the search term with #
                    </Text>

                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center"
                      }}
                    >
                      To filter results by feed, start the search term with %
                    </Text>
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center"
                      }}
                    >
                      use any number of these terms in a search
                    </Text>
                    <Text
                      style={{
                        fontSize: 18,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center",
                        marginTop: 20
                      }}
                    >
                      Example
                    </Text>
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center"
                      }}
                    >
                      /mathematics @john_nash nobel peace prize
                    </Text>
                  </View>
                </View>
              ) : null}
              <View
                style={{
                  marginTop: 5,
                  height: hp("100%") - 100,
                  width:
                    Platform.OS == "web" &&
                    Dimensions.get("window").width >= 750
                      ? 450
                      : "100%"
                }}
              >
                <FlatList
                  data={this.state.PostList}
                  renderItem={this._renderItem}
                  showsVerticalScrollIndicator={false}
                  scrollEnabled={true}
                  scrollEventThrottle={16}
                  onEndReached={({ distanceFromEnd }) => {
                    // this._handleLoadMore(distanceFromEnd);
                  }}
                  onEndReachedThreshold={0.2}
                  initialNumToRender={10}
                  removeClippedSubviews={true}
                />
              </View>
              {/* {Platform.OS == "web" && Dimensions.get("window").width >= 750 && ( */}
              <View
                style={{
                  // width: "48%",
                  width: 450,
                  marginLeft: 10,
                  marginBottom: 10,
                  marginTop: 5,
                  display:
                    Dimensions.get("window").width >= 750 &&
                    Platform.OS == "web"
                      ? "flex"
                      : "none"
                }}
              >
                <CommentDetailScreen
                  navigation={this.props.navigation}
                  postId={
                    this.props.PostDetails && this.props.PostDetails.post.id
                  }
                  listScroll={this.listScroll}
                />
              </View>
              {/* )} */}
              {(this.state.PostList.length == 0 && this.props.term != "") ||
                (this.props.termWeb != "" && this.state.showTips == false && (
                  <View>
                    <Icon
                      color={"#000"}
                      iconStyle={{
                        color: "#fff",
                        justifyContent: "center",
                        alignItems: "center",
                        alignSelf: "center"
                      }}
                      reverse
                      name="file"
                      type="font-awesome"
                      size={20}
                      containerStyle={{
                        alignSelf: "center"
                      }}
                    />
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        color: "#000",
                        alignSelf: "center"
                      }}
                    >
                      No Posts Results
                    </Text>
                  </View>
                ))}
            </View>
          </ScrollView>
        );
      case "first":
        return (
          <SearchClik
            navigation={this.props.navigation}
            ClikList={this.state.ClikList}
            term={this.props.term != "" ? this.props.term : this.props.termWeb}
          />
        );
      case "second":
        return (
          <SearchTopic
            navigation={this.props.navigation}
            TopicList={this.state.TopicList}
            term={this.props.term != "" ? this.props.term : this.props.termWeb}
          />
        );
      case "third":
        return (
          <SearchUser
            navigation={this.props.navigation}
            UserList={this.state.UserList}
            term={this.props.term != "" ? this.props.term : this.props.termWeb}
          />
        );
      default:
        return (
          <SearchFeed
            navigation={this.props.navigation}
            FeedList={this.state.FeedList}
            term={this.props.term != "" ? this.props.term : this.props.termWeb}
          />
        );
    }
  };

  componentDidMount() {
    if (this.props.termWeb.length > 0) {
      this.getRespectiveSuggestions(this.props.termWeb);
      this.getSuggestion(this.props.termWeb);
    }
    if (this.props.term.length > 0) {
      this.getRespectiveSuggestions(this.props.term);
      this.getSuggestion(this.props.term);
    }
  }

  componentWillUnmount() {
    this.props.setTerm(" ");
    this.props.setTermWeb(" ");
  }

  // componentDidUpdate(prevProps){
  //   if(prevProps.term!=this.props.term){
  //     this.getRespectiveSuggestions(this.props.term);
  //   }
  //   if(prevProps.keyEvent!=this.props.keyEvent){
  //     if (code === 13) {
  //     this.setState({
  //       TopicSuggestList: [],
  //       ClikSuggestList: [],
  //       UserSuggestList: [],
  //       FeedSuggestList: []
  //     });
  //     this.getSuggestion(this.props.term);
  //   }
  //   }
  // }

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: ConstantColors.customeBackgroundColor,
          width: "100%",
          paddingTop: Dimensions.get("window").width <= 750 ? 0 : 10,
          paddingHorizontal: Dimensions.get("window").width <= 750 ? 0 : 10
        }}
      >
        {(this.props.termWeb || this.props.term) != "" && (
          <View
            style={{
              zIndex: 5,
              alignSelf: "flex-start",
              width: Dimensions.get("window").width >= 1100 ? "48%" : "89%",
              marginLeft:
                Platform.OS == "web"
                  ? Dimensions.get("window").width >= 750
                    ? 0
                    : 10
                  : "5%",
              position: "absolute",
              backgroundColor: "#FEFEFA",
              top: this.props.getDisplayType === "web" ? 0 : 75,
              padding:
                this.state.TopicSuggestList.length > 0 ||
                this.state.ClikSuggestList.length > 0 ||
                this.state.UserSuggestList.length > 0 ||
                this.state.FeedSuggestList.length > 0
                  ? 10
                  : 0,
              borderWidth:
                this.state.TopicSuggestList.length > 0 ||
                this.state.ClikSuggestList.length > 0 ||
                this.state.UserSuggestList.length > 0 ||
                this.state.FeedSuggestList.length > 0
                  ? 1
                  : 0,
              borderColor: "#000",
              borderBottomLeftRadius: 6,
              borderBottomRightRadius: 6
            }}
          >
            {this.state.TopicSuggestList.map((item, index) => {
              return (
                <View
                  key={item.name}
                  style={{
                    backgroundColor: "#FEFEFA",
                    width: "95%",
                    padding: 5,
                    justifyContent: "space-between",
                    alignSelf: "flex-start",
                    flexDirection: "row",
                    height: 30
                  }}
                >
                  <View
                    style={{
                      padding: 5,
                      backgroundColor: item.parents ? "#e3f9d5" : "#e3f9d5",
                      borderRadius: 6,
                      alignSelf: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        color: item.parents ? "#009B1A" : "#009B1A",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        fontWeight: "bold"
                      }}
                      onPress={async () => {
                        await this.setState({
                          selectedItems: this.state.selectedItems.concat([
                            { name: "/" + item.name }
                          ]),
                          TopicSuggestList: [],
                          ClikSuggestList: [],
                          UserSuggestList: [],
                          FeedSuggestList: []
                        }),
                          await this.putValue("/" + item.name);
                        this.props.term != ""
                          ? this.props.setTerm("/" + item.name)
                          : this.props.setTermWeb("/" + item.name);
                        this.props.setTermWeb("/" + item.name);
                        this.getSuggestion(
                          this.props.term != ""
                            ? this.props.term
                            : this.props.termWeb
                        );
                      }}
                    >
                      /{item.name.toLowerCase()}
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      this.getTopicStar(item.name) == "FOLLOW"
                        ? this.favroiteTopics(item.id)
                        : this.getTopicStar(item.name) == "FAVORITE"
                        ? this.unfollowTopics(item.id)
                        : this.followTopics(item.id)
                    }
                  >
                    <Image
                      source={
                        this.getTopicStar(item.name) == "FOLLOW"
                          ? require("../assets/image/gstar.png")
                          : this.getTopicStar(item.name) == "FAVORITE"
                          ? require("../assets/image/ystar.png")
                          : require("../assets/image/wstar.png")
                      }
                      style={{
                        height: 20,
                        width: 20,
                        alignSelf: "center"
                      }}
                    />
                  </TouchableOpacity>
                </View>
              );
            })}

            {this.state.ClikSuggestList.map((item, index) => {
              return (
                <View
                  key={item.name}
                  style={{
                    backgroundColor: "#FEFEFA",
                    width: "95%",
                    padding: 5,
                    justifyContent: "space-between",
                    alignSelf: "flex-start",
                    flexDirection: "row",
                    height: 30
                  }}
                >
                  <View
                    style={{
                      padding: 5,
                      backgroundColor: "#E8F5FA",
                      borderRadius: 6,
                      alignSelf: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        color: "#4169e1",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        fontWeight: "bold"
                      }}
                      onPress={async () => {
                        await this.setState({
                          selectedItems: this.state.selectedItems.concat([
                            { name: "#" + item.name }
                          ]),
                          TopicSuggestList: [],
                          ClikSuggestList: [],
                          UserSuggestList: [],
                          FeedSuggestList: []
                        }),
                          await this.putValue("#" + item.name);
                        this.props.term != ""
                          ? this.props.setTerm("#" + item.name)
                          : this.props.setTermWeb("#" + item.name);
                        this.props.setTermWeb("#" + item.name);
                        //this.props.setTerm({term:"#" + item.name});
                        this.getSuggestion(
                          this.props.term != ""
                            ? this.props.term
                            : this.props.termWeb
                        );
                      }}
                    >
                      #{item.name}
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      this.getClikStar(item.name) == "FOLLOW"
                        ? this.favroiteCliks(item.id)
                        : this.getClikStar(item.name) == "FAVORITE"
                        ? this.unfollowCliks(item.id)
                        : this.followCliks(item.id)
                    }
                  >
                    <Image
                      source={
                        this.getClikStar(item.name) == "FOLLOW"
                          ? require("../assets/image/gstar.png")
                          : this.getClikStar(item.name) == "FAVORITE"
                          ? require("../assets/image/ystar.png")
                          : require("../assets/image/wstar.png")
                      }
                      style={{
                        height: 20,
                        width: 20,
                        alignSelf: "center"
                      }}
                    />
                  </TouchableOpacity>
                </View>
              );
            })}

            {this.state.UserSuggestList.map((item, index) => {
              return (
                <View
                  key={item.username}
                  style={{
                    backgroundColor: "#FEFEFA",
                    width: "95%",
                    padding: 5,
                    justifyContent: "space-between",
                    alignSelf: "flex-start",
                    flexDirection: "row",
                    height: 30
                  }}
                >
                  <Text
                    style={{
                      color: "#000",
                      fontFamily: ConstantFontFamily.defaultFont,
                      fontWeight: "bold"
                    }}
                    onPress={async () => {
                      await this.setState({
                        selectedItems: this.state.selectedItems.concat([
                          { name: "@" + item.username }
                        ]),
                        TopicSuggestList: [],
                        ClikSuggestList: [],
                        UserSuggestList: [],
                        FeedSuggestList: []
                      }),
                        await this.putValue("@" + item.name);
                      this.props.term != ""
                        ? this.props.setTerm("@" + item.username)
                        : this.props.setTermWeb("@" + item.username);
                      this.props.setTermWeb("@" + item.username);
                      //this.props.setTerm({term:"@" + item.username});
                      this.getSuggestion(
                        this.props.term != ""
                          ? this.props.term
                          : this.props.termWeb
                      );
                    }}
                  >
                    @{item.username}
                  </Text>
                  <TouchableOpacity
                    onPress={() =>
                      this.getUserStar(item.username) == "FOLLOW"
                        ? this.favroiteUser(item.id)
                        : this.getUserStar(item.username) == "FAVORITE"
                        ? this.unfollowUser(item.id)
                        : this.followUser(item.id)
                    }
                  >
                    <Image
                      source={
                        this.getUserStar(item.username) == "FOLLOW"
                          ? require("../assets/image/gstar.png")
                          : this.getUserStar(item.username) == "FAVORITE"
                          ? require("../assets/image/ystar.png")
                          : require("../assets/image/wstar.png")
                      }
                      style={{
                        height: 20,
                        width: 20,
                        alignSelf: "center"
                      }}
                    />
                  </TouchableOpacity>
                </View>
              );
            })}

            {this.state.FeedSuggestList.map((item, index) => {
              return (
                <View
                  key={item.id}
                  style={{
                    backgroundColor: "#FEFEFA",
                    width: "95%",
                    padding: 5,
                    justifyContent: "space-between",
                    alignSelf: "flex-start",
                    flexDirection: "row",
                    height: 30
                  }}
                >
                  <View style={{ flexDirection: "row", alignSelf: "center" }}>
                    <Image
                      source={{
                        uri: item.icon_url
                          ? item.icon_url
                          : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAhFBMVEX/pQD/////ngD/oQD/owD/oAD/+fT/nQD///3/7NP/pgD/4r7//vr/+vH/t1D/vF3/+O7/79r/zo//5MP/xnr/16P/2Kj/9OX/rSv/xXf/uVb/zIn/6c3/267/qRP/05v/s0L/wGn/sDb/wW3/tkv/z5T/yYH/qyD/sDf/37b/x4T/ryzcdsqXAAAM+ElEQVR4nN1daXfqOgx0bKUQCIQd2rKWbtz+//93E6ALEI/kLJAw57xP9yXNYFmSpbGtPA6dYDHYdr+WLVUltJZf3e1gEXTY71fwX8PhaKa1MUR0a0oXiL/JGK0/RsMwM8P2RvumetROQcbXUTsLw85A6aqz+wZpGljN1cKwN9KVH72/IKMfew4Mm8/a3PqbnWH0c1PKsE3145fAUNp8vGQYRrWZf+cgHV361QuGgarnAB5gVMAxHDfqOoAHUGOMGY70rT8xN/QIMYz8W39fAfAjO8OnOk/BX5gnG8PoPgjGFKN0hqN7MNED/FEaw3H9ncwv9PiSYdC49VcVikZwzjBU9Y6D5yAVnjG8Gy/zjR9vc2TYvqdJeIBu/2XYrGCVIi+Imn8YPt+bjSYwz78Me/dnowl074fh4z0OYTyIo2+GnfscwngQO0eGg/scwngQB0eGd+hIDyB1YNi2Ztz6CN83ppqFbwZJTIwZRrYP19NObzrtB/N5e7J6e4zWrYRtQvWqn5kDtEkYhlY/ox+8c3T6w8lzNDN7otf81ozQYcxwaDfSS4ZHNKfD1fbQs7nm97rDH8YMR1ZPamd45NlfvL/4lS7/xyFReTPrB3IM9wiD1ZOpLEuaeQqEexHDPfrjqrLUDyoogmFisvPBrIIkdaAW9oTGiWGC6aSr/WqRNAsFUjZnhjHCXeRXiaQZqK39c7IwTEguXhuVMVfaqm7hDGN0xrOK9Fipq77KYBijP6qEtdJMLe3/mothjMVLBXqtSwWEQHkZxgO5vXkAgUKn/AwT0QpVwVgtKIJhjMmyAsaajoIYet5uVlGOhTH0vHY1ORbI0POGVeRYKMPYVpeV8zkFM4x9jqlGovODwhlWTjBXPEPP61VKU1YGQ8+bz6qjiCiHoeeN/aqYalkMvU5VTLU0hnF0bFViGHWq5rYYNB+rIIKkaLPZbB/f38aL9rzfKZjvfFmBYSTab2qI4Wutzcd6O9gFFsF4BmyrMIwnSOjGVP314yQoZIoOqyooP+xV2U76uSmG3Qo32BOafnd1Iat2xLgiccMGMpqiSa6Z2a+Cw8GIWc4GOYayGVXYUr8Rk2z9y06y6pZ6QExyOcjqeoKaKANiki8LvBfQhs5L5SfjEeT722zWuqnBZDwi9juLLBTf6kMxHkgaZEh52rXwN0fEM3I0dabYr4m/OcLojTPH3rJWFJVpOHMMa+NSjzB665rQvdaMYrI31zFA1m/3nDHnOx4ZbGtHkfyPuRPFxxoFxiNIR07T8b1+FJXxnUy1jhSVP3NZd9SSIumBA8XH2rmbBP6HwzDWz6MmIP0mp1jT3YL+Wu5Ur5PdmMNGi2SnRTE5MRl0Vs4pXq6Qhptde7cYr97+bbqfh60W+YnqRynD8AorjdPu2jRYDDb7nRa5/rJ5kVpqr/z1Ylr/MOzvntc6jx6PjDSL65ceFu0d0v44oswsqbESUix9dzLuAfdX60ZGCZDeCCmWXZ5iu9ydXWQykTSfwmXjptyYIenjh+0oi7lSS1jhKDdmCJUKncmnu9KJjKxy3CnVoTrsCho572IjvRO9GuzpyQ8XtUnorgRuTERvXpVI0VFPM1w7chRm4iUm4c6KoaDrpq7Qz5K3NstL3zJoooKu0zjqd8lLy8ttMqm+gk8XjjKKpR1/lFHX1l46LNBlFLslTcXMyj0XeaVoLoYlRcUce9c2clMVedRhOadY5VFfOujytCQubkux03z60mfxMDYEpY1yQkZOBW3/Q/q7a0GOOi/DTnNrhN+FCQD5gpVGGccE5VdBz4XySmrx68Vmq3g7LUDnLZVX0if/rmHxcb8QJfubzOGYDf+q4lPwYrT6c9nKUfPlqU7hzYyCdiP0ZqLfvsEXGcdFUyxsv8WT5MvI8KVi+zksGRlmExqmQNTypBf2PfOCnY1ZTSaL3bA/zc9UtP4xI/Y9RTub/U6LZKsFzaLnxZy/LcMOkS5Ps+lbeecfHm7LaG3GQdadM3MJRX4qlnyGZcxTm+44m9i5L4gatObe0rxC4zSRrb+7iYAOmAoo+uxicXKVDv+epPtISihq9rXXkmjGJD/Grj5WQJE+uJfsrie2IeM/OipI+7y78Vndzcc1dbam8eQmzReEbNZOr3yqM+lXJ47859GMe0fRuRv7Rbrr4nT47IaV+V3/aG7SW4d055119z4X9689iCqRFknFB57gFg0TMW+4ojv9hf8hn47sEGguobjJtgU6v9/Fjh6nbmCD4nUSmwuYlnQY2ZjBOpsbba+Rq2QHzBgQMenSzQ7o9tdCp8ppKw3TkbrdOflEMktl22WaiRibm22RIi2T5XNtCMOoNEsVoTAQKkjfGTvVTFJ/DYGtDX5XRJFZInBF8MUtte5mJlk4cgqLBh7E5k3V/PQhcalMUYkbxNteykFKInVmci9mJt7S16ikISigyKQ2XIH4qmv9S5ASGCqjj9X4FeMSzfRw8hDehEFL3t0w7TKDS4u9ku5RI6P9WXf7/m9/3wXYmsBXIzxvBSkS4adfSzBT8k3092ihzhAcNW8EcRE7Gx+LbIsPiaSf2pdti+lAWdZ7Pp/d4JILYwYPBTMk+xkCC4tWT5CjfsJBZIQ2T4WaqV6jitoqvdTLS4FwxDBb+HCRZkqc+Kz3mvbXiNiYAU69j+FDh/xQXNA3S758P0hz3ny7DN+d6eOTYArzpuaV5eclip6Uv8e3IeBXMr39SUFBn61fHhGkUWSnIs4vcXI6LcZMTy8Qdv1YanFPrdEgGmwDheSmEsnZN9opk4pVWEC9Gi3hs0W09YlcdENpZULWTmFig58uQmDDVthPkeI3+Ao2GghcVyxgpS9Q8ZwgrZDJtW1CxJAxUxxOBSDlqp9JWy74TNy3X4ymuJ5w7kUiE3HTkDKrmOQLF6WwN827Y4gxkVSkJYtcbx4VP5kFRs4xNLItgqdIueGbmKUi9DW4wJ9zIposqsS0vhATMeBttvhnXuUaxOSSS3ekpVLEZLZopUcwacwXEX35OR1/kdbFZgZxh+IaLNfYLxmVoJFNOptmpoST2xAxxL9OnhaNS0b6F6k3mzJtCOuFtirRKqMnYTBlYES7Hy+R6jeYhiAyUzz+4AJHnmGWWJEgdf8O9ssdtNT30ZN5Yr6fRSebIHXFx/xeqOgGE4ZmDoa8mtWC1EnF5CZvwNgMzB1zaMA4tYAV6a09/IOhYgbOa8EtnCzDrHtm/qUyNP/gQyjmwxVmjqwm8ximqxCYig2KF7Bsar92m2foftrxARa7weUCtNKDMT9HwU2yPTcVlnwfx1c4EZEfzrEFI2NaavVujJkihrCWkt2Z4mwJwDYxsDcFdVPcHMjegsLLFjusWQY+UxjVPg16MN11yyhmY2jNFPFSH6Wm0K3n6F5kTGrsft+gyh1KMKHTa2cPF9kmIvBt8ENR2RR29HPk3hI5xSXAT4onImizwJJiHjltJjMF1S9c+AGqWIKZaQ6GWUpRKMXAERGIt3G4yHOeRIbEDeqToZALrNZx7o0FHRjuIRFPe+hqYN6GvDBK2pm7x52ba0xHF69lkX3DlRySmrbUEjN07VxMGEUl6gaiegT0eWj3xVJ9MTbM9Y1Owa1ksNWndDx+GCLzBmtg+lJsZ0O7NNi4LVdYPQKKu3CdA9I26iq+yuEwFfljxmGqC35tOIFBjkFbJdhBJF4JC06tgKeJgEADs6E5yKIGSlAyJuEoslsnFRNgwTII6oVB/8kslEjyLjk07uFTsoiBPhHUTGEFBHDQgYLNyd//kfWoc9mWQFhHBz4Rtj1AlqEflLDKYZbQUpsj4Ql7cBkEym0waNljVLw6UtLuFOnIPoUW4mtvoU9EiekmE0MzihmKC6oxx1SnGk5a8mPLYV0Q1DEgQ/sC0R/GDB3axKRnqzNPEbY3TgddQ4YoriGGdl+imzFDJjM/+0PJ9birdjDtPfT688n7p+vR+mYVPlixA1YaNe3PWa00+V2U+yEM+6ur9f5GjwznzRttB9S3gefsQ9jeM7zV1u/yQQm75L+bbf0uG/v6lYITtebY10wShjfejlkaDv0cheNJrXFoBewZln3O221wrJgcGDbv0J0SNf8wvMHBRKXj+xDGI8O63uRmx89Gn2+GIahz1RGkwjOGzB6y2qHxswz6YVjeJRK3wJ/dq78MvVEt741Mhf9Hu/GH4f14m5PthH8ZisqBNcDpdsIThjW8FTsF/mlv5JRhPW/iPcX5uXFnDL2x241NlQM1zsv/5wy9QNV5Mhp1UQ68YOiFkesFapUB6eiy8XPJME7DxfXdasFQWpMxjaHXfHa/0PDmMPo5Vc2QytDzOqM818VeH2T0yCLuszCMOQ5UbeYjaRpYhTpWhjHam7zXG18BSX06Qk1+xDD2q8P3ma+5I6FuAzrcZvAxGuLN2Jhhgodg8bbtzpaMtOjKaC2/utvBIuBPFPsPHJSoM14DLo8AAAAASUVORK5CYII="
                      }}
                      style={{
                        width: 24,
                        height: 24,
                        borderRadius: 12
                      }}
                    />
                    <Text
                      style={{
                        alignSelf: "center",
                        textAlign: "center",
                        color: "#000",
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontWeight: "bold",
                        marginLeft: 10
                      }}
                      onPress={async () => {
                        await this.setState({
                          selectedItems: this.state.selectedItems.concat([
                            { name: "%" + item.name }
                          ]),
                          TopicSuggestList: [],
                          ClikSuggestList: [],
                          UserSuggestList: [],
                          FeedSuggestList: []
                        }),
                          await this.putValue("%" + item.name);
                        //this.props.setTerm({term:"%" + item.name});
                        this.props.term != ""
                          ? this.props.setTerm("%" + item.name)
                          : this.props.setTermWeb("%" + item.name);
                        this.props.setTermWeb("%" + item.name);
                        this.getSuggestion(
                          this.props.term != ""
                            ? this.props.term
                            : this.props.termWeb
                        );
                      }}
                    >
                      {item.name}
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      this.getFeedStar(item.name) == "FOLLOW"
                        ? this.favroiteFeed(item.id)
                        : this.getFeedStar(item.name) == "FAVORITE"
                        ? this.unfollowFeed(item.id)
                        : this.followFeed(item.id)
                    }
                  >
                    <Image
                      source={
                        this.getFeedStar(item.name) == "FOLLOW"
                          ? require("../assets/image/gstar.png")
                          : this.getFeedStar(item.name) == "FAVORITE"
                          ? require("../assets/image/ystar.png")
                          : require("../assets/image/wstar.png")
                      }
                      style={{
                        height: 20,
                        width: 20,
                        alignSelf: "center"
                      }}
                    />
                  </TouchableOpacity>
                </View>
              );
            })}
          </View>
        )}

        {Platform.OS != "web" ||
          (Dimensions.get("window").width <= 750 && (
            <View
              style={{
                width: "100%",
                flexDirection: "row",
                backgroundColor: "#000",
                //borderRadius: 6,
                height: 42
              }}
            >
              <TouchableOpacity
               style={ButtonStyle.headerBackStyle}
                onPress={() => {
                  let nav = this.props.navigation.dangerouslyGetParent().state;
                  if (nav.routes.length > 1) {
                    this.props.navigation.goBack();
                    return;
                  } else {
                    this.props.navigation.navigate("home");
                  }
                }}
              >
                <Icon
                  color={"#fff"}
                  name="angle-left"
                  type="font-awesome"
                  size={40}
                />
              </TouchableOpacity>
              <View
                style={{
                  width: "100%",
                  flex: 1,
                  backgroundColor: "#000",
                  justifyContent: "center",
                  alignItems: "center",
                  flexDirection: "row"
                }}
              >
                <SearchInput
                  navigation={this.props.navigation}
                  refs={ref => {
                    this.input = ref;
                    //this.props.setTypeUrl(this.props.term)
                  }}
                  displayType={"android"}
                  selection={this.state.selection}
                  onselectionchange1={this.handleSelectionChange}
                />
              </View>
            </View>
          ))}
        {/* <TabView
          swipeEnabled={false}
          lazy
          navigationState={this.state}
          renderScene={this._renderScene}
          renderLazyPlaceholder={this._renderLazyPlaceholder}
          renderTabBar={this._renderTabBar}
          onIndexChange={this._handleIndexChange}
          initialLayout={{
            width: Dimensions.get("window").width,
            height: Dimensions.get("window").height
          }}
          style={{
            marginRight: 1
          }}
        /> */}

        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              flexDirection: "row",
              width: "100%",
              marginHorizontal: "auto"
            }}
          >
            {this.state.showTips == true ? (
              <View
                style={{
                  marginTop: 10,
                  padding: 10,
                  // borderWidth: 1,
                  // borderColor: "#e1e1e1",
                  // borderRadius: 10,
                  width: "100%"
                }}
              >
                <View
                  style={{
                    borderColor: "#C5C5C5",
                    borderWidth: 1,
                    borderRadius: 10,
                    backgroundColor: "#fff",
                    padding: 10
                  }}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center",
                      marginBottom: 20
                    }}
                  >
                    Search Tips
                  </Text>

                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center"
                    }}
                  >
                    To filter results by topics, start the search term with /.
                  </Text>

                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center"
                    }}
                  >
                    To filter results by users, start the search term with @.
                  </Text>

                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center"
                    }}
                  >
                    To filter results by cliks, start the search term with #
                  </Text>

                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center"
                    }}
                  >
                    To filter results by feed, start the search term with %
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center"
                    }}
                  >
                    use any number of these terms in a search
                  </Text>
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center",
                      marginTop: 20
                    }}
                  >
                    Example
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center"
                    }}
                  >
                    /mathematics @john_nash nobel peace prize
                  </Text>
                </View>
              </View>
            ) : null}
            <View
              style={{
                marginTop: 5,
                height: hp("100%") ,
                width:
                  Platform.OS == "web" && Dimensions.get("window").width >= 750
                    ? 450
                    : "100%"
              }}
            >
              {this.state.PostList.length == 0 && (
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: 20
                  }}
                >
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center"
                    }}
                    reverse
                    name="file"
                    type="font-awesome"
                    size={20}
                    containerStyle={{
                      alignSelf: "center"
                    }}
                  />
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                      alignSelf: "center"
                    }}
                  >
                    No Search Results
                  </Text>
                </View>
              )}
              <FlatList
                ref={this.flatListRef}
                data={this.state.PostList}
                renderItem={this._renderItem}
                showsVerticalScrollIndicator={false}
                scrollEnabled={true}
                scrollEventThrottle={16}
                onEndReached={({ distanceFromEnd }) => {
                  // this._handleLoadMore(distanceFromEnd);
                }}
                onEndReachedThreshold={0.2}
                initialNumToRender={10}
                removeClippedSubviews={true}
              />
            </View>

            {/* {Platform.OS == "web" && Dimensions.get("window").width >= 750 && ( */}
            <View
              style={{
                // width: "48%",
                width: 450,
                marginLeft: 10,
                marginBottom: 10,
                marginTop: 5,
                display:
                  Dimensions.get("window").width >= 750 && Platform.OS == "web"
                    ? "flex"
                    : "none"
              }}
            >
              <CommentDetailScreen
                navigation={this.props.navigation}
                postId={
                  this.props.PostDetails && this.props.PostDetails.post.id
                }
                listScroll={this.listScroll}
              />
            </View>
            {/* )} */}
          </View>
        </ScrollView>
        {Dimensions.get("window").width <= 750 &&
          <BottomScreen navigation={NavigationService} />
        }
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
    listTrending_cliks: !state.TrendingCliksReducer.getIn([
      "Trending_cliks_List"
    ])
      ? List()
      : state.TrendingCliksReducer.getIn(["Trending_cliks_List"]),
    link: state.LinkPostReducer.get("link"),
    getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
    loginStatus: state.UserReducer.get("loginStatus"),
    getUserFollowTopicList: state.LoginUserDetailsReducer.get(
      "userFollowTopicsList"
    )
      ? state.LoginUserDetailsReducer.get("userFollowTopicsList")
      : List(),
    getUserFollowCliksList: state.LoginUserDetailsReducer.get(
      "userFollowCliksList"
    )
      ? state.LoginUserDetailsReducer.get("userFollowCliksList")
      : List(),
    getUserFollowUserList: state.LoginUserDetailsReducer.get(
      "userFollowUserList"
    )
      ? state.LoginUserDetailsReducer.get("userFollowUserList")
      : List(),
    getUserFollowFeedList: state.LoginUserDetailsReducer.get(
      "userFollowFeedList"
    )
      ? state.LoginUserDetailsReducer.get("userFollowFeedList")
      : List(),
    getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get(
      "dimension"
    ),
    PostDetails: state.PostDetailsReducer.get("PostDetails"),
    term: state.ScreenLoadingReducer.get("setTerm"),
    keyEvent: state.AdminReducer.get("setKeyEvent"),
    getDisplayType: state.AdminReducer.get("setDisplayType"),
    termWeb: state.ScreenLoadingReducer.get("setTermWeb"),
    keyEventWeb: state.ScreenLoadingReducer.get("setKeyEventWeb"),
    trendingHomeFeedId: state.PostCommentDetailsReducer.get("PostId")
    //getUrl: state.ScreenLoadingReducer.get("setUrlType")["url"]
  };
};

const mapDispatchToProps = dispatch => ({
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  topicId: payload => dispatch(getTrendingTopicsProfileDetails(payload)),
  listTopicFeed: payload => dispatch(listTopicFeed(payload)),
  ClikId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  setClikUserRequest: payload => dispatch(listClikUserRequest(payload)),
  setClikMembers: payload => dispatch(listClikMembers(payload)),
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload)),
  setFeedDetails: payload => dispatch(getFeedProfileDetails(payload)),
  setTerm: payload => dispatch({ type: "SET_TERM", payload }),
  setTermWeb: payload => dispatch({ type: "SET_TERM_WEB", payload }),
  //setTypeUrl: payload => dispatch({ type: "SET_URL_TYPE", payload }),
  setPostCommentReset: payload =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload })
});

const SearchScreenWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(SearchScreen);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  SearchScreenWrapper
);
