// const defaultFont = "Montserrat";
const defaultFont = "Verdana";
const charterFont = "Charter";
const HeaderBoldFont = "Montserratbold";
const MontserratBoldFont = "VerdanaBold";
const VerdanaFont = "Verdana";

export default {
  defaultFont,
  charterFont,
  MontserratBoldFont,
  VerdanaFont,
  HeaderBoldFont
};
