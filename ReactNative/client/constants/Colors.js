
import {Dimensions} from 'react-native';

const tintColor = '#2f95dc';
const defultBackgroundColor = Dimensions.get('window').width<=750 ? '#f8f8f8' : "#fff";
// '#f4f4f4';

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  customeBackgroundColor: defultBackgroundColor,
};
