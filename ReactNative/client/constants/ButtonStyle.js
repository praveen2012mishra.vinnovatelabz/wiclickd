import ConstantFontFamily from "./FontFamily";
import { Dimensions, Platform } from "react-native";

const containerStyle = {
  marginVertical: 10,
  marginLeft: 5,
  height: 40,
  alignSelf: "center"
};

const borderStyle = {
  borderColor: "#D7D7D7",
  borderWidth: 1,
  borderRadius: 20
};

const cardBorderStyle = {
  borderColor: "#D7D7D7",
  borderWidth: 1,
  borderRadius: 0
}

const backgroundStyle = {
  backgroundColor: "#fff",
  borderRadius: 20,
  borderColor: "#C5C5C5",
  borderWidth: 1,
  paddingHorizontal: 20
};

const gbackgroundStyle = {
  backgroundColor: "#009B1A",
  borderRadius: 20,
  borderColor: "#C5C5C5",
  borderWidth: 1,
  paddingHorizontal: 20
};

const dbackgroundStyle = {
  backgroundColor: "#E3E6E8",
  borderRadius: 20,
  borderColor: "#C5C5C5",
  borderWidth: 1,
  paddingHorizontal: 20
};

const rbackgroundStyle = {
  backgroundColor: "#de5246",
  borderRadius: 20,
  borderColor: "#C5C5C5",
  borderWidth: 1,
  paddingHorizontal: 20
};

const titleStyle = {
  fontSize: 14,
  fontWeight: "bold",
  color: "#000",
  fontFamily: ConstantFontFamily.MontserratBoldFont
};

const wtitleStyle = {
  fontSize: 14,
  fontWeight: "bold",
  color: "#fff",
  fontFamily: ConstantFontFamily.MontserratBoldFont
};

const ctitleStyle = {
  fontSize: 12,
  fontWeight: "bold",
  color: "#000",
  fontFamily: ConstantFontFamily.MontserratBoldFont
};

const cwtitleStyle = {
  fontSize: 12,
  fontWeight: "bold",
  color: "#fff",
  fontFamily: ConstantFontFamily.MontserratBoldFont
};

const cdtitleStyle = {
  fontSize: 12,
  fontWeight: "bold",
  color: "#99A1A8",
  fontFamily: ConstantFontFamily.MontserratBoldFont
};

const cgbackgroundStyle = {
  backgroundColor: "#009B1A",
  borderRadius: 20,
  borderColor: "#C5C5C5",
  borderWidth: 1,
  paddingHorizontal: 10
};

const cdbackgroundStyle = {
  backgroundColor: "#E3E6E8",
  borderRadius: 20,
  borderColor: "#C5C5C5",
  borderWidth: 1,
  paddingHorizontal: 10
};

const crbackgroundStyle = {
  backgroundColor: "#de5246",
  borderRadius: 20,
  borderColor: "#C5C5C5",
  borderWidth: 1,
  paddingHorizontal: 10
};

const dtitleStyle = {
  fontSize: 14,
  fontWeight: "bold",
  color: "#99A1A8",
  fontFamily: ConstantFontFamily.MontserratBoldFont
};

const headerTitleStyle = {
  flex: 1,
  backgroundColor: "#f4f4f4",
  alignItems: 'flex-start',
  alignSelf: 'center',
  borderRadius: 6,
  marginRight: 10
}
const headerBackStyle = {
  flexDirection: "row",
  justifyContent: "flex-start",
  alignItems: 'center',
  marginHorizontal: 10,
  alignSelf: 'center',
}

const shadowStyle = {
  // shadowColor: "#000",
  // shadowOffset: {
  //   width: 0,
  //   height: 2,
  // },
  // shadowOpacity:0.4,
  // shadowRadius: 6.84,

  // elevation: 5,

  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 0,
  },
  shadowOpacity:0.5,
  shadowRadius: 5,
  width: "98%",
  marginTop: 4,
  borderWidth:0
  
}

const profileShadowStyle = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 0,
  },
  shadowOpacity:0.5,
  shadowRadius: 6.84,

  elevation: 5,
  width: Dimensions.get("window").width <= 750 ? '100%' : "98%",
  marginTop: 2,
  borderWidth:0
  
}

//for profile tab views
const cardShadowStyle = {
  // shadowColor: "#000",
  // shadowOffset: {
  //   width: 0,
  //   height: 0,
  // },
  // shadowOpacity: 0.3,
  // shadowRadius: 3,
  elevation: 5,
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 0,
  },
  shadowOpacity:0.5,
  shadowRadius: 6.84,
  width: Dimensions.get("window").width <= 750 ? '97' :  "98%",
  borderWidth:0,
  borderRadius: 10,
  marginHorizontal:10
}

const textAreaShadowStyle = {
  // shadowColor: "#000",
  // shadowOffset: {
  //   width: 0,
  //   height: 0,
  // },
  // shadowOpacity: 0.3,
  // shadowRadius: 3,
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 0,
  },
  shadowOpacity:0.5,
  shadowRadius: 6.84,
  elevation: 5,
  width: "100%",
  marginVertical: 5,
  borderRadius: 5,
  outline: 'none',
  borderWidth:0,
  // borderColor:'#d7d7d7'
}


const selectShadowStyle = {
  shadowColor: "#009B1A",
  shadowOffset: {
    width: 0,
    height: 0,
  },
  shadowOpacity:1.5,
  shadowRadius: 7,
  elevation: 9,
  marginTop: 4
}

const selecttextAreaShadowStyle = {
  shadowColor: '#009B1A',
  shadowOffset: {
    width: 0,
    height: 0,
  },
  shadowOpacity:Platform.OS == 'web' ? 1.5 : 0.5,
  shadowRadius: 7,
  elevation:Platform.OS == 'web' ? 9 : 5,
 
  width: "100%",
  marginVertical: 5,
  height: 45,
  borderRadius: 5,
  outline: 'none',
}
const headerRightStyle = {
  alignItems: 'center',
  justifyContent: 'center'
}
export default {
  backgroundStyle,
  gbackgroundStyle,
  dbackgroundStyle,
  rbackgroundStyle,
  containerStyle,
  titleStyle,
  wtitleStyle,
  dtitleStyle,
  borderStyle,
  ctitleStyle,
  cwtitleStyle,
  cdtitleStyle,
  cgbackgroundStyle,
  cdbackgroundStyle,
  crbackgroundStyle,
  headerTitleStyle,
  headerBackStyle,
  shadowStyle,
  selectShadowStyle,
  headerRightStyle,
  cardBorderStyle, //for card section
  cardShadowStyle, //for card section
  textAreaShadowStyle,
  selecttextAreaShadowStyle,
  profileShadowStyle
};
