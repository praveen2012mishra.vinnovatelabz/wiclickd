export const phrases = [
    "A Space for the Intellectually Curious.",
    "Privacy and security are utmost important to us; we do not sell ads or personal info.",
    "Weclikd facilitates discourse. Violation of principles results in forfeiture of content, earnings, and accounts.",
    "We cogently discuss ideas, and do not attack people, cultures, or events.",
    "Comments must be thoughtful and original.",
    "Our core values are quality, authenticity, and integrity.",
    "We hold the right to not support discourse of certain topics.",
    "We C. We Likd. #Weclikd."
];
//Format to fit space.
