const puppeteer = require("puppeteer");

(async () => {
  const browser = await puppeteer.launch({ headless: false, slowMo: 500 });
  const page = await browser.newPage();

  await page.goto("https://www.google.com");
  const searchBox = await page.$("input[type=text]");
  await searchBox.type("Weclikd");
  const inputElement = await page.$(
    'input[type=submit][value="Google Search"]'
  );
  await inputElement.click();

  // await page.goto("http://electric-block-241402.appspot.com/");
  // const inputElement = await page.$('div [value="Login"]');
  // await inputElement.click();

  //await browser.close();
})();
