const createExpoWebpackConfigAsync = require("@expo/webpack-config");

module.exports = async function(env, argv) {
  const config = await createExpoWebpackConfigAsync(env, argv);
  // Customize the config before returning it.
  let Uconfig = {
    ...config,
    optimization: {
      moduleIds: "hashed",
      splitChunks: {
        cacheGroups: {
          default: false,
          vendors: false,
          // vendor chunk
          vendor: {
            name: "vendor",
            // async + async chunks
            chunks: "all",
            // import file path containing node_modules
            test: /node_modules/,
            priority: 20
          }
        }
      }
    }
  };
  //console.log(Uconfig);
  return Uconfig;
};
