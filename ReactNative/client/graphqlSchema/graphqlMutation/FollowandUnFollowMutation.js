import gql from "graphql-tag";
import { clikFields, userFields } from "../GraphQLFragment";

export const UserFollowMutation = gql`
  mutation FollowUser($user_id: ID!, $follow_type: FollowType) {
    user_follow(input: { user_id: $user_id, follow_type: $follow_type }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const ClikFollowMutation = gql`
  mutation FollowClik($clik_id: ID!, $follow_type: FollowType) {
    clik_follow(input: { clik_id: $clik_id, follow_type: $follow_type }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const UserUnfollowMutation = gql`
  mutation UnfollowUser($user_id: ID!) {
    user_unfollow(input: { user_id: $user_id }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const ClikUnfollowMutation = gql`
  mutation UnfollowClik($clik_id: ID!) {
    clik_unfollow(input: { clik_id: $clik_id }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const TopicFollowMutation = gql`
  mutation FollowTopic($topic_id: ID!, $follow_type: FollowType) {
    topic_follow(input: { topic_id: $topic_id, follow_type: $follow_type }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const TopicUnFollowMutation = gql`
  mutation UnfollowTopic($topic_id: ID!) {
    topic_unfollow(input: { topic_id: $topic_id }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const FeedFollowMutation = gql`
  mutation FollowExternalFeed($feed_id: ID!, $follow_type: FollowType) {
    external_feed_follow(
      input: { feed_id: $feed_id, follow_type: $follow_type }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const FeedUnFollowMutation = gql`
  mutation UnfollowExternalFeed($feed_id: ID!) {
    external_feed_unfollow(input: { feed_id: $feed_id }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const ClikJoinMutation = gql`
  mutation JoinClik(
    $clik_id: ID!
    $qualification: String
    $known_members: [ID]
    $invite_key: String
  ) {
    clik_join(
      input: {
        clik_id: $clik_id
        qualification: $qualification
        known_members: $known_members
        invite_key: $invite_key
      }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
      member_type
    }
  }
`;

export const InviteToClikMutation = gql`
  mutation InviteToClik($clik_id: ID!, $invited_users: [InvitedUserInput]) {
    clik_invite(input: { clik_id: $clik_id, invited_users: $invited_users }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const RejectClikMemberMutation = gql`
  mutation RejectClikMember($clik_id: ID!, $user_id: ID!) {
    clik_member_reject(input: { clik_id: $clik_id, user_id: $user_id }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const PromoteClikMemberMutation = gql`
  mutation PromoteClikMember(
    $clik_id: ID!
    $user_id: ID!
    $member_type: ClikMemberType!
  ) {
    clik_member_promote(
      input: { clik_id: $clik_id, user_id: $user_id, member_type: $member_type }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const KickClikMemberMutation = gql`
  mutation KickClikMember($clik_id: ID!, $user_id: ID!) {
    clik_member_kick(input: { clik_id: $clik_id, user_id: $user_id }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const ClikGenerateKeyMutation = gql`
  mutation CreateClikInviteKey($clik_id: ID!, $member_type: ClikMemberType) {
    clik_create_invite_key(
      input: { clik_id: $clik_id, member_type: $member_type }
    ) {
      invite_key
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const InviteKeyClikProfileMutation = gql`
  query GetClikInviteKeyInfo($invite_key: String!) {
    clik_invite_key(invite_key: $invite_key) {
      clik {
        ...clikFields
      }
      inviter {
        ...userFields
      }
      member_type
      expired
      created
    }
  }
  ${clikFields}
  ${userFields}
`;
