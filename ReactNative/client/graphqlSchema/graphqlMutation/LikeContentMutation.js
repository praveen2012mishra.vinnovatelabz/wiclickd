import gql from "graphql-tag";
import {
  __PageInfoField,
  commentFields,
  statusFields
} from "../GraphQLFragment";

export const LikeContentMutation = gql`
  mutation LikeContent($content_id: ID!, $like_type: LikeType!) {
    content_like(input: { content_id: $content_id, type: $like_type }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const CreateCommentMutation = gql`
  mutation CreateComment(
    $text: String
    $parent_content_id: ID!
    $clik: String
  ) {
    comment_create(
      input: { text: $text, parent_content_id: $parent_content_id, clik: $clik }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
      comment {
        ...commentFields
      }
    }
  }
  ${commentFields}
`;

export const EditCommentMutation = gql`
  mutation EditComment($content_id: ID!, $text: String) {
    comment_edit(input: { content_id: $content_id, text: $text }) {
      status {
        ...statusFields
      }
    }
  }
  ${statusFields}
`;
