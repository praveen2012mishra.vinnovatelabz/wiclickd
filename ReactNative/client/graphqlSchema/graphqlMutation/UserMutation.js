import gql from "graphql-tag";
import {
  userFields,
  topicFields,
  clikFields,
  externalFeedFields
} from "../GraphQLFragment";

export const AccountCreateMutation = gql`
  mutation CreateAccount(
    $first_name: String
    $last_name: String
    $email: String
    $username: String
    $clik_invite_key: String
    $inviter_username: String
  ) {
    account_create(
      input: {
        first_name: $first_name
        last_name: $last_name
        email: $email
        username: $username
        clik_invite_key: $clik_invite_key
        inviter_username: $inviter_username
      }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
      account {
        id
        first_name
        last_name
        email
        settings {
          subscription
          privacy {
            dm_settings
          }
          email_notifications {
            notification_email
            monthly_earnings
            clik_notifications
            weclikd_updates
          }
        }
        created
        my_users {
          id
          user {
            ...userFields
          }
        }
      }
    }
  }
  ${userFields}
`;

export const UserQueryMutation = gql`
  query GetUserProfile($id: ID!) {
    user(id: $id) {
      ...userFields
    }
  }
  ${userFields}
`;

export const UserLoginMutation = gql`
  mutation Login {
    login {
      status {
        success
        status
        custom_status
        user_msg
      }
      account {
        id
        first_name
        last_name
        email
        settings {
          subscription
          privacy {
            dm_settings
          }
          email_notifications {
            notification_email
            monthly_earnings
            clik_notifications
            weclikd_updates
          }
        }
        created
        my_users {
          id
          user {
            ...userFields
          }
          users_followed {
            settings {
              follow_type
            }
            user {
              ...userFields
            }
            created
          }
          topics_followed {
            settings {
              follow_type
            }
            topic {
              ...topicFields
            }
            created
          }
          cliks_followed {
            settings {
              follow_type
            }
            member_type
            clik {
              ...clikFields
            }
            created
          }
          feeds_followed {
            settings {
              follow_type
            }
            feed {
              ...externalFeedFields
            }
            created
          }
        }
      }
    }
  }
  ${userFields}
  ${topicFields}
  ${clikFields}
  ${externalFeedFields}
`;

export const UserEditMutation = gql`
  mutation EditUser(
    $username: String
    $profile_pic: String
    $banner_pic: String
    $description: String
    $full_name: String
  ) {
    user_edit(
      input: {
        username: $username
        profile_pic: $profile_pic
        banner_pic: $banner_pic
        description: $description
        full_name: $full_name
      }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
      user {
        ...userFields
      }
    }
  }
  ${userFields}
`;

export const ChangeSubscriptionMutation = gql`
  mutation ChangeSubscription(
    $type: SubscriptionType!
    $payment_id: String
    $email: String
  ) {
    account_change_subscription(
      input: { type: $type, payment_id: $payment_id, email: $email }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const ChangeAccountSettingsMutation = gql`
  mutation ChangeAccountSettings(
    $privacy: PrivacySettingsInput
    $email_notification: EmailNotificationSettingsInput
  ) {
    account_change_settings(
      input: { privacy: $privacy, email_notifications: $email_notification }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const ChangePaymentInfoMutation = gql`
  mutation ChangePaymentInfo($payment_id: String, $email: String) {
    account_change_payment_info(
      input: { payment_id: $payment_id, email: $email }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const CheckUsernameMutation = gql`
  mutation CheckUsername($username: String!) {
    username_check(input: { username: $username }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;
