import gql from "graphql-tag";
import {
  clikFields,
  userFields,
  postFields,
  topicFields,
  externalFeedFields
} from "../GraphQLFragment";

export const SearchTopicMutation = gql`
  query TopicCompletion($prefix: String!) {
    search {
      topics(prefix: $prefix) {
        ...topicFields
      }
    }
  }
  ${topicFields}
`;

export const SearchUserMutation = gql`
  query UserCompletion($prefix: String!) {
    search {
      users(prefix: $prefix) {
        ...userFields
      }
    }
  }
  ${userFields}
`;

export const SearchClikMutation = gql`
  query ClikCompletion($prefix: String!) {
    search {
      cliks(prefix: $prefix) {
        ...clikFields
      }
    }
  }
  ${clikFields}
`;

export const SearchFeedMutation = gql`
  query FeedCompletion($prefix: String!) {
    search {
      feeds(prefix: $prefix) {
        ...externalFeedFields
      }
    }
  }
  ${externalFeedFields}
`;

export const SearchEveryThingMutation = gql`
  query Search(
    $topics: [String]
    $cliks: [String]
    $users: [String]
    $feeds: [String]
    $terms: [String]
  ) {
    search {
      everything(
        input: {
          topics: $topics
          cliks: $cliks
          users: $users
          feeds: $feeds
          terms: $terms
        }
      ) {
        topics {
          ...topicFields
        }
        posts {
          ...postFields
        }
        cliks {
          ...clikFields
        }
        users {
          ...userFields
        }
        feeds {
          ...externalFeedFields
        }
      }
    }
  }
  ${userFields}
  ${clikFields}
  ${postFields}
  ${topicFields}
  ${externalFeedFields}
`;

export const DefaultTopicListMutation = gql`
  query GetTopicList($first: Int, $after: String) {
    topic_list(first: $first, after: $after) {
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        node {
          ...topicFields
        }
      }
    }
  }
  ${topicFields}
`;

export const DefaultClikListMutation = gql`
  query GetClikList($first: Int, $after: String) {
    clik_list(first: $first, after: $after) {
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        node {
          ...clikFields
        }
      }
    }
  }
  ${clikFields}
`;

export const DefaultUserListMutation = gql`
  query GetUserList($first: Int, $after: String) {
    user_list(first: $first, after: $after) {
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        node {
          ...userFields
        }
      }
    }
  }
  ${userFields}
`;

export const DefaultExternalFeedListMutation = gql`
  query GetExternalFeedList($first: Int, $after: String) {
    external_feed_list(first: $first, after: $after) {
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        node {
          ...externalFeedFields
        }
      }
    }
  }
  ${externalFeedFields}
`;
