import gql from "graphql-tag";
import {
  clikFields,
  userFields,
  postFields,
  topicFields,
  externalFeedFields,
  adminpostFields,
  statusFields
} from "../GraphQLFragment";

// export const Trending_Cliks_Mutation = gql`
//   query GetTrendingCliks($currentPage: Int!) {
//     trending_cliks(first: $currentPage) {
//       pageInfo {
//         hasNextPage
//         hasPreviousPage
//         startCursor
//         endCursor
//       }
//       edges {
//         cursor
//         node {
//           ...clikFields
//         }
//       }
//     }
//   }
//   ${clikFields}
// `;

export const Trending_Cliks_Mutation = gql`
  query GetClikList($first: Int, $after: String, $sort: SortEnum) {
    clik_list(first: $first, after: $after, sort: $sort) {
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        node {
          ...clikFields
        }
      }
    }
  }
  ${clikFields}
`;

// export const Trending_Users_Mutation = gql`
//   query GetTrendingUsers($currentPage: Int!) {
//     trending_users(first: $currentPage) {
//       pageInfo {
//         hasNextPage
//         hasPreviousPage
//         startCursor
//         endCursor
//       }
//       edges {
//         cursor
//         node {
//           ...userFields
//         }
//       }
//     }
//   }
//   ${userFields}
// `;

export const Trending_Users_Mutation = gql`
  query GetUserList($first: Int, $after: String, $sort: SortEnum) {
    user_list(first: $first, after: $after, sort: $sort) {
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        node {
          ...userFields
        }
      }
    }
  }
  ${userFields}
`;

export const ClikQuery = gql`
  query GetClikProfile($id: ID!) {
    clik(id: $id) {
      ...clikFields
    }
  }
  ${clikFields}
`;

export const ClikFeed = gql`
  query GetClikFeed($id: ID!, $first: Int, $after: String, $sort: SortEnum) {
    clik(id: $id) {
      posts(first: $first, after: $after, sort: $sort) {
        pageInfo {
          hasNextPage
          endCursor
        }
        edges {
          node {
            ...postFields
          }
        }
      }
    }
  }
  ${postFields}
`;

export const AdminClikFeed = gql`
  query GetClikFeed($id: ID!, $first: Int, $after: String, $sort: SortEnum) {
    clik(id: $id) {
      posts(first: $first, after: $after, sort: $sort) {
        pageInfo {
          hasNextPage
          endCursor
        }
        edges {
          node {
            ...adminpostFields
          }
        }
      }
    }
  }
  ${adminpostFields}
`;

// export const TRENDING_TOPICS = gql`
//   query GetTrendingTopics($currentPage: Int!) {
//     trending_topics(first: $currentPage) {
//       pageInfo {
//         hasNextPage
//         hasPreviousPage
//         startCursor
//         endCursor
//       }
//       edges {
//         cursor
//         node {
//           ...topicFields
//         }
//       }
//     }
//   }
//   ${topicFields}
// `;

export const TRENDING_TOPICS = gql`
  query GetTopicList($first: Int, $after: String, $sort: SortEnum) {
    topic_list(first: $first, after: $after, sort: $sort) {
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        node {
          ...topicFields
        }
      }
    }
  }
  ${topicFields}
`;

export const TopicQuery = gql`
  query GetTopicProfile($id: ID!) {
    topic(id: $id) {
      ...topicFields
    }
  }
  ${topicFields}
`;

export const TopicFeed = gql`
  query GetTopicFeed($id: ID!, $first: Int, $after: String, $sort: SortEnum) {
    topic(id: $id) {
      posts(first: $first, after: $after, sort: $sort) {
        pageInfo {
          hasNextPage
          endCursor
        }
        edges {
          node {
            ...postFields
          }
        }
      }
    }
  }
  ${postFields}
`;

export const AdminTopicFeed = gql`
  query GetTopicFeed($id: ID!, $first: Int, $after: String, $sort: SortEnum) {
    topic(id: $id) {
      posts(first: $first, after: $after, sort: $sort) {
        pageInfo {
          hasNextPage
          endCursor
        }
        edges {
          node {
            ...adminpostFields
          }
        }
      }
    }
  }
  ${adminpostFields}
`;

export const ClikUserApplications = gql`
  query GetClikUserApplications($clik_id: ID!, $first: Int, $after: String) {
    clik(id: $clik_id) {
      applications(first: $first, after: $after) {
        pageInfo {
          hasNextPage
          endCursor
        }
        edges {
          node {
            user {
              ...userFields
            }
            known_members {
              ...userFields
            }
            qualification
            created
          }
        }
      }
    }
  }
  ${userFields}
`;

export const EditClikMutation = gql`
  mutation EditClik(
    $clik_id: ID!
    $description: String
    $banner_pic: String
    $icon_pic: String
    $website: String
    $qualifications: [String]
    $max_members: Int
    $invite_only: Boolean
  ) {
    clik_edit(
      input: {
        clik_id: $clik_id
        description: $description
        banner_pic: $banner_pic
        website: $website
        qualifications: $qualifications
        max_members: $max_members
        invite_only: $invite_only
        icon_pic: $icon_pic
      }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
      clik {
        ...clikFields
      }
    }
  }
  ${clikFields}
`;

export const DeleteClikMutation = gql`
  mutation DeleteClik($clik_id: ID!) {
    clik_delete(input: { clik_id: $clik_id }) {
      status {
        ...statusFields
      }
      clik {
        ...clikFields
      }
    }
  }
  ${clikFields}
  ${statusFields}
`;

export const GetChildTopicsMutation = gql`
  query GetChildTopics($id: ID!) {
    topic(id: $id) {
      children {
        ...topicFields
      }
    }
  }
  ${topicFields}
`;

// export const Trending_ExternalFeeds_Mutation = gql`
//   query GetTrendingExternalFeeds($currentPage: Int!) {
//     trending_external_feeds(first: $currentPage) {
//       pageInfo {
//         hasNextPage
//         hasPreviousPage
//         startCursor
//         endCursor
//       }
//       edges {
//         cursor
//         node {
//           ...externalFeedFields
//         }
//       }
//     }
//   }
//   ${externalFeedFields}
// `;

export const Trending_ExternalFeeds_Mutation = gql`
  query GetExternalFeedList($first: Int, $after: String, $sort: SortEnum) {
    external_feed_list(first: $first, after: $after, sort: $sort) {
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        node {
          ...externalFeedFields
        }
      }
    }
  }
  ${externalFeedFields}
`;