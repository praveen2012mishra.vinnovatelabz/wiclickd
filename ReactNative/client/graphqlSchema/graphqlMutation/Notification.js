import gql from "graphql-tag";
import { __PageInfoField, notificationFields } from "../GraphQLFragment";

export const GetAccountNotificationsMutation = gql`
  query GetAccountNotifications($first: Int, $after: String) {
    account {
      notifications(first: $first, after: $after) {
        pageInfo {
          hasNextPage
          endCursor
        }
        edges {
          node {
            ...notificationFields
          }
        }
      }
    }
  }
  ${notificationFields}
`;

export const GetNumUnreadNotificationsMutation = gql`
  query GetNumUnreadNotifications {
    account {
      num_unread_notifications
    }
  }
`;

export const MarkNotificationsAsReadMutation = gql`
  mutation MarkNotificationsAsRead {
    account_mark_notifications_as_read {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const DeleteAccountNotificationMutation = gql`
  mutation DeleteAccountNotification($notification_id: ID) {
    account_notification_delete(input: { notification_id: $notification_id }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;
