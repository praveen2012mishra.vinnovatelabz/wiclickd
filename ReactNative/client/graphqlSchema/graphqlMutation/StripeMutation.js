import gql from "graphql-tag";

export const GetPaymentInfoOuery = gql`
  query GetPaymentInfo {
    account {
      payment_info {
        __typename
        ... on CreditCard {
          last4
          brand
          exp_month
          exp_year
          email
        }
        ... on NoPaymentInfo {
          status {
            success
            status
            custom_status
            user_msg
          }
        }
      }
    }
  }
`;


export const IsAccountHaveOuery = gql`
query GetPaymentInfo {
  account {
    payout_info {
      has_stripe_account
    }
  }
}
`;
