import gql from "graphql-tag";
import {
  __PageInfoField,
  postFields,
  adminpostFields,
  commentFields,
  clikFields,
  userFields,
  topicFields,
  statusFields
} from "../GraphQLFragment";

export const PostCreateMutation = gql`
  mutation CreatePost(
    $title: String!
    $summary: String!
    $text: String
    $thumbnail_pic: String
    $thumbnail_pic_url: String
    $pictures: [String]
    $topics: [String]
    $cliks: [String]
    $link: String
  ) {
    post_create(
      input: {
        title: $title
        summary: $summary
        text: $text
        thumbnail_pic: $thumbnail_pic
        thumbnail_pic_url: $thumbnail_pic_url
        pictures: $pictures
        topics: $topics
        cliks: $cliks
        link: $link
      }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
      post {
        ...postFields
      }
    }
  }
  ${postFields}
`;

export const PostShareMutation = gql`
  mutation SharePost($content_id: ID!, $topics: [String], $cliks: [String]) {
    post_share(input: {content_id: $content_id, topics: $topics, cliks: $cliks}) {
      status {
        success
        status
        custom_status
        user_msg
      }
      post {
        ...postFields
      }
    }
  }
  ${postFields}
`;



export const UserFeedMutation = gql`
  query UserFeed($id: ID!, $first: Int, $after: String, $sort: SortEnum) {
    user(id: $id) {
      posts(first: $first, after: $after, sort: $sort) {
        pageInfo {
          hasNextPage
          endCursor
        }
        edges {
          node {
            ...postFields
          }
        }
      }
    }
  }
  ${postFields}
`;

export const AdminUserFeedMutation = gql`
  query UserFeed($id: ID!, $first: Int, $after: String, $sort: SortEnum) {
    user(id: $id) {
      posts(first: $first, after: $after, sort: $sort) {
        pageInfo {
          hasNextPage
          endCursor
        }
        edges {
          node {
            ...adminpostFields
          }
        }
      }
    }
  }
  ${adminpostFields}
`;
export const PostQuery = gql`
  query GetPost($id: ID!, $first: Int, $after: String, $clik_id: [ID]) {
    post(id: $id) {
      ...postFields
      comments(first: $first, after: $after, clik_ids: $clik_id) {
        pageInfo {
          endCursor
          hasNextPage
        }
        edges {
          node {
            ...commentFields
            comments(first: $first, clik_ids: $clik_id) {
              pageInfo {
                endCursor
                hasNextPage
              }
              edges {
                node {
                  ...commentFields
                  comments(first: $first, clik_ids: $clik_id) {
                    pageInfo {
                      endCursor
                      hasNextPage
                    }
                    edges {
                      node {
                        ...commentFields
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  ${commentFields}
  ${postFields}
`;

export const AdminPostQuery = gql`
  query GetPost($id: ID!, $first: Int, $after: String, $clik_id: [ID]) {
    post(id: $id) {
      ...adminpostFields
      comments(first: $first, after: $after, clik_ids: $clik_id) {
        pageInfo {
          endCursor
          hasNextPage
        }
        edges {
          node {
            ...commentFields
            comments(first: $first, clik_ids: $clik_id) {
              pageInfo {
                endCursor
                hasNextPage
              }
              edges {
                node {
                  ...commentFields
                  comments(first: $first, clik_ids: $clik_id) {
                    pageInfo {
                      endCursor
                      hasNextPage
                    }
                    edges {
                      node {
                        ...commentFields
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  ${commentFields}
  ${adminpostFields}
`;

export const CommentQuery = gql`
  query GetComments($id: ID!, $first: Int, $after: String, $clik_id: ID) {
    comments(id: $id, first: $first, after: $after, clik_id: $clik_id) {
      pageInfo {
        endCursor
        hasNextPage
      }
      edges {
        node {
          ...commentFields
          comments(first: $first, clik_id: $clik_id) {
            pageInfo {
              endCursor
              hasNextPage
            }
            edges {
              node {
                ...commentFields
                comments(first: $first, clik_id: $clik_id) {
                  pageInfo {
                    endCursor
                    hasNextPage
                  }
                  edges {
                    node {
                      ...commentFields
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  ${commentFields}
`;

export const SwiperCommentQuery = gql`
  query GetComment($id: ID!, $first: Int, $after: String, $clik_id: ID) {
    comments(id: $id, first: $first, after: $after, clik_id: $clik_id) {
      pageInfo {
        endCursor
        hasNextPage
      }
      edges {
        node {
          ...commentFields
          comments(first: $first, clik_id: $clik_id) {
            pageInfo {
              endCursor
              hasNextPage
            }
            edges {
              node {
                ...commentFields
                comments(first: $first, clik_id: $clik_id) {
                  pageInfo {
                    endCursor
                    hasNextPage
                  }
                  edges {
                    node {
                      ...commentFields
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  ${commentFields}
`;

export const CreateClikMutation = gql`
  mutation CreateClik(
    $name: String!
    $description: String!
    $banner_pic: String
    $website: String
    $qualifications: [String]
    $max_members: Int
    $invited_users: [InvitedUserInput]
    $invite_only: Boolean
    $icon_pic: String
  ) {
    clik_create(
      input: {
        name: $name
        description: $description
        banner_pic: $banner_pic
        website: $website
        qualifications: $qualifications
        max_members: $max_members
        invited_users: $invited_users
        invite_only: $invite_only
        icon_pic: $icon_pic
      }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
      member_type
      clik {
        ...clikFields
      }
    }
  }
  ${clikFields}
`;

export const ClikMembersMutation = gql`
  query GetClikMembers($clik_id: ID!, $first: Int, $after: String) {
    clik(id: $clik_id) {
      members(first: $first, after: $after) {
        pageInfo {
          hasNextPage
          endCursor
        }
        edges {
          node {
            user {
              ...userFields
            }
            type
            joined
          }
        }
      }
    }
  }
  ${userFields}
`;

export const UrlInfoMutation = gql`
  query GetUrlInfo($url: String!) {
    url_info(url: $url) {
      status
      post {
        ...postFields
      }
      unfurl {
        title
        summary
        thumbnail_url
        normalized_url
      }
    }
  }
  ${postFields}
`;

export const CreateTopicMutation = gql`
  mutation CreateTopic(
    $name: String!
    $description: String
    $banner_pic: String
    $parent: String
  ) {
    topic_create(
      input: {
        name: $name
        description: $description
        banner_pic: $banner_pic
        parent: $parent
      }
    ) {
      status {
        ...statusFields
      }
      topic {
        ...topicFields
      }
    }
  }
  ${topicFields}
  ${statusFields}
`;

export const EditTopicMutation = gql`
  mutation EditTopic(
    $topic_id: ID!
    $name: String
    $description: String
    $banner_pic: String
    $parent: String
  ) {
    topic_edit(
      input: {
        topic_id: $topic_id
        name: $name
        description: $description
        banner_pic: $banner_pic
        parent: $parent
      }
    ) {
      status {
        ...statusFields
      }
      topic {
        ...topicFields
      }
    }
  }
  ${topicFields}
  ${statusFields}
`;

export const PostEditMutation = gql`
  mutation EditPost(
    $content_id: ID!
    $title: String
    $summary: String
    $text: String
    $thumbnail_pic: String
    $pictures: [String]
    $topics: [String]
    $link: String
  ) {
    post_edit(
      input: {
        content_id: $content_id
        title: $title
        summary: $summary
        text: $text
        thumbnail_pic: $thumbnail_pic
        pictures: $pictures
        topics: $topics
        link: $link
      }
    ) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;