import gql from "graphql-tag";
import {
  postFields,
  externalFeedFields,
  statusFields,
  adminpostFields
} from "../GraphQLFragment";

export const HomeFeedMutation = gql`
  query HomeFeed($first: Int, $after: String, $sort: SortEnum) {
    home_feed {
      posts(first: $first, after: $after, sort: $sort) {
        pageInfo {
          hasNextPage
          endCursor
        }
        edges {
          node {
            ...postFields
          }
        }
      }
    }
  }
  ${postFields}
`;

export const AdminHomeFeedMutation = gql`
  query HomeFeedAdmin($first: Int, $after: String, $sort: SortEnum) {
    home_feed {
      posts(first: $first, after: $after, sort: $sort) {
        pageInfo {
          hasNextPage
          endCursor
        }
        edges {
          node {
            ...adminpostFields
          }
        }
      }
    }
  }
  ${adminpostFields}
`;

export const ContentReportMutation = gql`
  mutation ReportContent($content_id: ID!, $reports: [SingleReport]!) {
    content_report(input: { content_id: $content_id, reports: $reports }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;

export const ExternalFeedProfileQuery = gql`
  query GetExternalFeeds($id: ID!) {
    external_feed(id: $id) {
      ...externalFeedFields
    }
  }
  ${externalFeedFields}
`;

export const ExternalFeedMutation = gql`
  query GetExternalFeed(
    $id: ID!
    $first: Int
    $after: String
    $sort: SortEnum
  ) {
    external_feed(id: $id) {
      posts(first: $first, after: $after, sort: $sort) {
        pageInfo {
          hasNextPage
          endCursor
        }
        edges {
          node {
            ...postFields
          }
        }
      }
    }
  }
  ${postFields}
`;

export const AdminExternalFeedMutation = gql`
  query GetExternalFeed(
    $id: ID!
    $first: Int
    $after: String
    $sort: SortEnum
  ) {
    external_feed(id: $id) {
      posts(first: $first, after: $after, sort: $sort) {
        pageInfo {
          hasNextPage
          endCursor
        }
        edges {
          node {
            ...adminpostFields
          }
        }
      }
    }
  }
  ${adminpostFields}
`;

export const ExternalFeedTestMutation = gql`
  mutation TextExternalFeed($url: String) {
    external_feed_test(input: { url: $url }) {
      status {
        ...statusFields
      }
      external_feed {
        ...externalFeedFields
      }
    }
  }
  ${statusFields}
  ${externalFeedFields}
`;

export const AddExternalFeedMutation = gql`
  mutation CreateExternalFeed(
    $name: String!
    $url: String
    $base_topic: String
    $website: String
    $summary_source: SummarySource
    $icon_url: String
  ) {
    external_feed_create(
      input: {
        name: $name
        url: $url
        website: $website
        base_topic: $base_topic
        summary_source: $summary_source
        icon_url: $icon_url
      }
    ) {
      status {
        ...statusFields
      }
      external_feed {
        ...externalFeedFields
      }
    }
  }
  ${statusFields}
  ${externalFeedFields}
`;

export const EditExternalFeedMutation = gql`
  mutation EditExternalFeed(
    $feed_id: ID!
    $name: String
    $base_topic: String
    $website: String
    $summary_source: SummarySource
    $icon_url: String
  ) {
    external_feed_edit(
      input: {
        feed_id: $feed_id
        name: $name
        website: $website
        base_topic: $base_topic
        summary_source: $summary_source
        icon_url: $icon_url
      }
    ) {
      status {
        ...statusFields
      }
      external_feed {
        ...externalFeedFields
      }
    }
  }
  ${statusFields}
  ${externalFeedFields}
`;

export const DeleteExternalFeedMutation = gql`
  mutation DeleteExternalFeed($feed_id: ID!) {
    external_feed_delete(input: { feed_id: $feed_id }) {
      status {
        ...statusFields
      }
      external_feed {
        ...externalFeedFields
      }
    }
  }
  ${statusFields}
  ${externalFeedFields}
`;

export const DeleteTopicMutation = gql`
  mutation DeleteTopic($topic_id: ID!) {
    topic_delete(input: { topic_id: $topic_id }) {
      status {
        ...statusFields
      }
    }
  }
  ${statusFields}
`;

export const DeleteContentMutation = gql`
  mutation DeleteContent($content_id: ID!) {
    content_delete(input: { content_id: $content_id }) {
      status {
        success
        status
        custom_status
        user_msg
      }
    }
  }
`;