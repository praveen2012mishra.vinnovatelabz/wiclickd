
export const AccountCreateVariables = {
    "variables": {
        "first_name": 'default first name',
        "last_name": 'default last name',
        "email": '',
        "username": '',
        'clik_invite_key':'',
        'inviter_username':'',
    }
}


export const UserCreateVariables = {
    "variables": {
        "username": '',
        "description": '',
        "profile_pic": '',
        "banner_background": {
            "type": "PICTURE",
            "picture": {
                "background_pic": '',
                "font": '',
                "brightness": '',
                "crop_left_x": 0,
                "crop_top_y": 0,
                "crop_right_x": 1,
                "crop_bottom_y": 1
            }
        }
    }
}


export const UserDetailsVariables = {
    "variables": {
        "id": ""
    }
}


export const ChangeSubscriptionVariables = {
    "variables": {
        "type": ""
    }
}

export const ChangeAccountSettingsVariables = {
    "variables": {
        "privacy": {
            "dm_settings": ""
        },
        "email_notification": {
            "notification_email": "",
            "monthly_earnings": false,
            "clik_notifications": false,
            "weclikd_updates": false
        }
    }
}



export const UserEditVariables = {
    "variables": {
        "username": "",
        "profile_pic": null,
        "banner_pic": null,
        "description": "",
        "full_name": ""
    }
}