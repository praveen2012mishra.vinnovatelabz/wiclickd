export const PostCreateVariables = {
  variables: {
    title: "",
    summary: "",
    text: "",
    thumbnail_pic: "",
    thumbnail_pic_url: "",
    pictures: [],
    topics: [],
    link: "",
    cliks: []
  }
};

export const PostQueryVariables = {
  variables: {
    id: "",
    first: 0,
    after: null,
    clik_id: null
  }
};

export const CommentQueryVariables = {
  variables: {
    id: "",
    clik_id: null,
    first: 0,
    after: null
  }
};

export const CreateClikVariables = {
  variables: {
    name: "",
    description: "",
    banner_pic: "",
    icon_pic: "",
    website: "",
    qualifications: [],
    invited_users: [],
    max_members: null,
    invite_only: true
  }
};

export const EditClikVariables = {
  variables: {
    clik_id: "",
    description: null,
    banner_pic: null,
    icon_pic:null,
    website: null,
    qualifications: null,
    max_members: null,
    invite_only: true
  }
};
export const UrlInfoVariables = {
  variables: {
    url: ""
  }
};

export const ExternalFeedVariables = {
  variables: {
    url: ""
  }
};

export const CreateTopicVariables = {
  variables: {
    name: "",
    description: "",
    banner_pic: "",
    parent: ""
  }
};

export const EditTopicVariables = {
  variables: {
    topic_id: "",
    name: "",
    description: "",
    banner_pic: "",
    parent: ""
  }
};

export const AddExternalFeedVariables = {
  variables: {
    name: "",
    url: "",
    website: "",
    base_topic: "",
    summary_source: "",
    icon_url: ""
  }
};

export const EditExternalFeedVariables = {
  variables: {
    name: "",
    feed_id: "",
    website: "",
    base_topic: "",
    summary_source: "",
    icon_url: ""
  }
};

export const PostEditVariables = {
  variables: {
    content_id: "",
    title: "",
    summary: "",
    text: "",
    thumbnail_pic: "",
    pictures: [],
    topics: [],
    link: ""
  }
};