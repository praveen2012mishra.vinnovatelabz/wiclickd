import gql from "graphql-tag";

export const __PageInfoField = gql`
  fragment __PageInfoField on CommentConnection {
    pageInfo {
      hasNextPage
      hasPreviousPage
      startCursor
      endCursor
    }
  }
`;

export const userFields = gql`
  fragment userFields on User {
    id
    username
    profile_pic
    banner_pic
    full_name
    description
  }
`;

export const clikFields = gql`
  fragment clikFields on Clik {
    id
    name
    icon_pic
    banner_pic
    description
    num_followers
    num_members
    max_members
    website
    qualifications
    invite_only
  }
`;

export const postFields = gql`
  fragment postFields on Post {
    id
    author {
      ...userFields
    }
    reasons_shared {
      users {
        id
        username
        profile_pic
      }
      cliks {
        id
        name
        icon_pic
      }
    }
    text
    likes_score
    comments_score
    reports_score
    title
    summary
    created
    thumbnail_pic
    pictures
    topics
    cliks
    link
    user_like_type
    external_feed {
      icon_url
      id
    }
  }
  ${userFields}
`;

export const adminpostFields = gql`
  fragment adminpostFields on Post {
    id
    author {
      ...userFields
    }
    reasons_shared {
      users {
        id
        username
        profile_pic
      }
      cliks {
        id
        name
        icon_pic
      }
    }
    admin_stats {
      sharer {
        ...userFields
      }
      likes {
        red
        silver
        gold
        diamond
      }
      num_likes
      num_reports
      num_comments
    }
    text
    likes_score
    comments_score
    reports_score
    title
    summary
    created
    thumbnail_pic
    pictures
    topics
    cliks
    link
    user_like_type
    external_feed {
      icon_url
      id
    }
  }
  ${userFields}
`;

export const commentFields = gql`
  fragment commentFields on Comment {
    id
    author {
      ...userFields
    }
    text
    created
    likes_score
    comments_score
    reports_score
    clik
  }
  ${userFields}
`;

export const topicFields = gql`
  fragment topicFields on Topic {
    id
    name
    banner_pic
    description
    parents
    num_followers
  }
`;

export const statusFields = gql`
  fragment statusFields on WeclikdStatus {
    success
    status
    custom_status
    user_msg
    debug_error_msg
  }
`;

export const externalFeedFields = gql`
  fragment externalFeedFields on ExternalFeed {
    id
    name
    url
    website
    base_topic
    topic {
      ...topicFields
    }
    created
    icon_url
    followers
  }
  ${topicFields}
`;

export const commentNotificationFields = gql`
  fragment commentNotificationFields on CommentNotification {
    my_content {
      __typename
      ... on Comment {
        ...commentFields
      }
      ... on Post {
        ...postFields
      }
    }
    their_comment {
      ...commentFields
    }
    post {
      ...postFields
    }
    discussion {
      ...commentFields
    }
  }
  ${commentFields}
  ${postFields}
`;

export const clikInviteRequestNotificationFields = gql`
  fragment clikInviteRequestNotificationFields on ClikInviteRequestNotification {
    clik {
      ...clikFields
    }
    inviter {
      ...userFields
    }
  }
  ${clikFields}
  ${userFields}
`;

export const clikInviteAcceptedNotificationFields = gql`
  fragment clikInviteAcceptedNotificationFields on ClikInviteAcceptedNotification {
    clik {
      ...clikFields
    }
    invitee {
      ...userFields
    }
  }
  ${clikFields}
  ${userFields}
`;

export const clikJoinRequestNotificationFields = gql`
  fragment clikJoinRequestNotificationFields on ClikJoinRequestNotification {
    clik {
      ...clikFields
    }
    invitee {
      ...userFields
    }
  }
  ${clikFields}
  ${userFields}
`;

export const clikJoinAcceptedNotificationFields = gql`
  fragment clikJoinAcceptedNotificationFields on ClikJoinAcceptedNotification {
    clik {
      ...clikFields
    }
    inviter {
      ...userFields
    }
  }
  ${clikFields}
  ${userFields}
`;

export const userInviteAcceptedNotificationFields = gql`
  fragment userInviteAcceptedNotificationFields on UserInviteAcceptedNotification {
    invitee {
      ...userFields
    }
  }
  ${userFields}
`;

export const notificationFields = gql`
  fragment notificationFields on Notification {
    id
    contents {
      __typename
      ... on CommentNotification {
        ...commentNotificationFields
      }
      ... on ClikInviteRequestNotification {
        ...clikInviteRequestNotificationFields
      }
      ... on ClikInviteAcceptedNotification {
        ...clikInviteAcceptedNotificationFields
      }
      ... on ClikJoinRequestNotification {
        ...clikJoinRequestNotificationFields
      }
      ... on ClikJoinAcceptedNotification {
        ...clikJoinAcceptedNotificationFields
      }
      ... on UserInviteAcceptedNotification {
        ...userInviteAcceptedNotificationFields
      }
    }
    created
  }
  ${commentNotificationFields}
  ${clikInviteRequestNotificationFields}
  ${clikInviteAcceptedNotificationFields}
  ${clikJoinRequestNotificationFields}
  ${clikJoinAcceptedNotificationFields}
  ${userInviteAcceptedNotificationFields}
`;