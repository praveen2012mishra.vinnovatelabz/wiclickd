import React from "react";
import { Text, View, TouchableHighlight } from "react-native";
import ConstantFontFamily from "../constants/FontFamily";


class ReportCliksTopics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listTopic: [],
      selectTopic: [],
      listCliks: [],
      selectCliks: []
    };
  }

  changeDataTopicList = async item => {
    let data = [];
    if (item) {
      item.map(
        async (v, i) =>
          await data.push({
            color: "#009B1A",
            name: v
          })
      );
    }
    return await data;
  };

  changeDataCliksList = async item => {
    let data = [];
    if (item) {
      item.map(
        async (v, i) =>
          await data.push({
            color: "#4C82B6",
            name: v
          })
      );
    }
    return await data;
  };

  async componentDidMount() {
    this.setState({
      listTopic: await this.changeDataTopicList(this.props.topicsList),
      listCliks: await this.changeDataCliksList(this.props.cliksList)
    });
  }

  __handleTopic = async (t, i) => {
    let data = this.state.listTopic;
    let index = this.state.selectTopic.findIndex(i => i == t);
    if (index == -1) {
      await this.setState({
        selectTopic: this.state.selectTopic.concat([t])
      });
      data[i].color = data[i].color == "red" ? "#009B1A" : "red";
      await this.setState({
        listTopic: data
      });
    } else {
      let updatedData = this.state.selectTopic;
      updatedData.splice(index, 1);
      await this.setState({
        selectTopic: updatedData
      });
      data[i].color = data[i].color == "red" ? "#009B1A" : "red";
      await this.setState({
        listTopic: data
      });
    }
    await this.props.submitTopic(this.state.selectTopic);
  };

  __handleCliks = async (t, i) => {
    let data = this.state.listCliks;
    let index = this.state.selectCliks.findIndex(i => i == t);
    if (index == -1) {
      await this.setState({
        selectCliks: this.state.selectCliks.concat([t])
      });
      data[i].color = data[i].color == "red" ? "#4C82B6" : "red";
      await this.setState({
        listCliks: data
      });
    } else {
      let updatedData = this.state.selectCliks;
      updatedData.splice(index, 1);
      await this.setState({
        selectCliks: updatedData
      });
      data[i].color = data[i].color == "red" ? "#4C82B6" : "red";
      await this.setState({
        listCliks: data
      });
    }
    await this.props.submitCliks(this.state.selectCliks);
  };

  render() {
    const { listTopic, listCliks } = this.state;
    return (
      <View
        style={{
          flexDirection: "row",
          width: "100%"
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-start",
            flexWrap: "wrap",
            width: "100%",
            padding: 10
          }}
        >
          {listCliks.map((t, i) => {
            return (
              <TouchableHighlight
                onPress={() => {
                  this.__handleCliks(t.name, i);
                }}
                key={i}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    height: 30,
                    marginLeft: 5,
                    marginTop: 5,
                    padding: 3
                  }}
                >
                  <Text
                    style={{
                      color: t.color,
                      textDecorationLine: t.color == 'red' ? 'line-through' : null,
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                    }}
                  >
                    #{t.name}
                  </Text>
                </View>
              </TouchableHighlight>
            );
          })}
          {listTopic.map((t, i) => {
          return (
            <TouchableHighlight
              onPress={() => {
                this.__handleTopic(t.name, i);
              }}
              key={i}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  height: 30,
                  marginLeft: 5,
                  marginTop: 5,
                  padding: 3
                }}
              >
                <Text
                  style={{
                    color: t.color,
                    textDecorationLine: t.color == 'red' ? 'line-through' : null,
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                  }}
                >
                  /{t.name}
                </Text>
              </View>
            </TouchableHighlight>
          );
        })}
        </View>
      </View>
    );
  }
}

export default ReportCliksTopics;
