import "@expo/browser-polyfill";
import { openBrowserAsync } from "expo-web-browser";
import moment from "moment";
import React, { useEffect, useRef, useState } from "react";
import {
  TextInput,
  Image,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  Platform,
  Dimensions,
  Clipboard,
  TouchableWithoutFeedback
} from "react-native";
import { Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { getFeedProfileDetails } from "../actionCreator/FeedProfileAction";
import { setFEEDREPORTMODALACTION } from "../actionCreator/FeedReportModalAction";
import { postEditDetails } from "../actionCreator/LinkPostAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { listTopicFeed } from "../actionCreator/TopicsFeedAction";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { getTrendingTopicsProfileDetails } from "../actionCreator/TrendingTopicsProfileAction";
import ChangeIcon from "../components/ChangeIcon";
import ConstantFontFamily from "../constants/FontFamily";
import { rootTopics } from "../constants/RootTopics";
import { capitalizeFirstLetter } from "../library/Helper";
import DiscussByUsersList from "./DiscussByUsersList";
import ImageComponent from "./ImageComponent";
import Overlay from "react-native-modal-overlay";
import FeedShareModal from "../components/FeedShareModal";
import Modal from "modal-enhanced-react-native-web";
import FeedReportModal from "../components/FeedReportModal";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger
} from "react-native-popup-menu";
import { DeleteContentMutation } from "../graphqlSchema/graphqlMutation/FeedMutation";
import applloClient from "../client";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";

let activeID = "";
let lastTap = null;
const FeedList = props => {
  const swiperRef = useRef(null);

  let [summaryHeight, setsummaryHeight] = useState(10);
  let [titleHeight, settitleHeight] = useState(10);

  let bgImage = {
    uri: props.item.item.node.thumbnail_pic
  };

  moment.updateLocale("en", {
    relativeTime: {
      future: "in %s",
      past: "%s ago",
      s: "a few secs",
      ss: "%d secs",
      m: "1 minute",
      mm: "%d minutes",
      h: "1 hour",
      hh: "%d hours",
      d: "1 day",
      dd: "%d days",
      w: "1 week",
      ww: "%d weeks",
      M: "1 month",
      MM: "%d months",
      y: "1 year",
      yy: "%d years"
    }
  });

  useEffect(() => {
    if (props.isFocused == false && swiperRef != null) {
      swiperRef.current.goTo(0);
    }
    let data = document.getElementById(props.item.item.node.id);
    if (data != null) {
      data.addEventListener("keydown", event => {
        if (event.keyCode == 82) {
          props.loginStatus == 1
            ? handleBugClick()
            : props.setLoginModalStatus(true);
        }
        if (event.keyCode == 67) {
          props.setPostDetails({
            title: "Home",
            id: "Post:" + activeID.replace("Post:", ""),
            comment: true
          });
          props.setPostCommentDetails({
            id: activeID.replace("Post:", "")
          });
        }
        if (event.keyCode == 13) {
          event.stopImmediatePropagation();
          goToPostDetailsScreen(activeID.replace("Post:", ""));
        }
      });

      data.addEventListener("click", event => {
        if (data.style.borderColor == "red") {
          activeID = data.id;
          data.scrollIntoView();
        }
      });
    }
    //l=76, r=82, c=67,Enter =13
  });
  let [showBug, setshowBug] = useState(false);
  let [pressBug, setpressBug] = useState(false);
  let [pressShare, setpressShare] = useState(false);
  let [TopicList, setTopicList] = useState(
    props.item.item.node.topics && props.item.item.node.topics
  );
  let [ClikList, setClikList] = useState(
    props.item.item.node.cliks && props.item.item.node.cliks
  );
  let [pressComment, setpressComment] = useState(false);
  let [loaded, setloaded] = useState(false);
  let [showMenu, setshowMenu] = useState(Platform.OS == "web" ? false : true);
  let [MenuHover, setMenuHover] = useState(false);

  const openWindow = async link => {
    await openBrowserAsync(link);
  };
  const loginModalStatusEvent = async status => {
    props.loginModalStatusEventParent(status);
  };
  const goToPostDetailsScreen = async (id, navigate) => {
    props.setPostCommentReset({
      payload: [],
      postId: props.item.item.node.id
        .replace("Trending", "")
        .replace("New", "")
        .replace("Discussion", "")
        .replace("Search", ""),
      title: props.item.item.node.title,
      loading: props.item.item.node.comments_score > 0 ? true : false
    });
    props.highlight(
      props.ActiveTab + props.item.item.node.id,
      props.item.item.node.title
    );
    let title = "Home";
    props.setPostCommentDetails({
      id: id,
      title: props.item.item.node.title,
      loading: props.item.item.node.comments_score > 0 ? true : false
    });
    if (navigate == true) {
      props.setPostDetails({
        title: title,
        id: "Post:" + id,
        navigate: navigate
      });
    }
    props.setComment(false);
  };

  const goToClikProfile = id => {
    if (id != null) {
      props.cliksUserId({
        id: id,
        type: "feed"
      });
    }
  };

  async function goToProfile(id) {
    props.topicId({
      id: id,
      type: "feed"
    });
  }

  const handleClick = e => {
    setshowBug(e);
  };

  const handleBugClick = e => {
    props.setFeedReportModalAction(true);
    setpressBug(true);
  };

  const handleShareClick = e => {
    props.setFeedShareModal(true);
    setpressShare(true);
  };

  const onClose = () => {
    props.setFeedShareModal(false);
    setpressShare(false);
    props.setFeedReportModalAction(false);
    setpressBug(false);
  };

  const getHeart = liketype => {
    switch (liketype) {
      case "RED":
        return 1;
      case "SILVER":
        return 2;
      case "GOLD":
        return 3;
      case "DIAMOND":
        return 4;
      default:
        return 0;
    }
  };

  const getColour = score => {
    return "#969faa";
    // if (score == 0) {
    //   return "#969faa";
    // } else if (0 < score && score <= 25) {
    //   return "#de5246";
    // } else if (25 < score && score <= 50) {
    //   return "#b0b0b0";
    // } else if (50 < score && score <= 75) {
    //   return "#ebca44";
    // } else {
    //   return "#8bbaf9";
    // }
  };

  const goToFeedProfile = async id => {
    await props.setFeedDetails({
      id: id,
      type: "feed"
    });
  };

  const getTopic = List => {
    if (List.length > 0) {
      let prevList = TopicList;
      let newList = [...prevList, ...List];
      setTopicList(newList);
    }
  };
  const getClik = List => {
    if (List.length > 0) {
      let prevList = ClikList;
      let newList = [...prevList, ...List];
      setClikList(newList);
    }
  };

  const writeToClipboard = async () => {
    let Domain = window.location.href
      .replace("http://", "")
      .replace("https://", "")
      .replace("www.", "")
      .split(/[/?#]/)[0];
    let PostId = "/post/" + props.item.item.node.id.replace("Post:", "");
    await Clipboard.setString(
      Domain.startsWith("localhost") == true
        ? "http://" + Domain + PostId
        : "https://" + Domain + PostId
    );
  };

  const handleDoubleTap = () => {
    if (Dimensions.get("window").width >= 1100) {
      let tempData =
        props.trendingHomeFeedId && props.trendingHomeFeedId.includes("Post:")
          ? props.trendingHomeFeedId.replace("Post:", "")
          : props.trendingHomeFeedId;
      let data1 = props.ActiveTab + props.item.item.node.id;
      let data2 = props.ActiveTab + "Post:" + tempData;
      let data1content = document.getElementById(data1);
      let data2content = document.getElementById(data2);
      if (data1content != null) {
        data1content.style.borderColor = "green";
      }
      if (data2content != null) {
        data2content.style.borderColor = "#c5c5c5";
      }
      if (data1 == data2) {
        goToPostDetailsScreen(
          props.item.item.node.id.replace("Post:", ""),
          true
        );
      } else {
        goToPostDetailsScreen(
          props.item.item.node.id.replace("Post:", ""),
          false
        );
      }
    } else {
      goToPostDetailsScreen(props.item.item.node.id.replace("Post:", ""), true);
    }
  };

  const handleClickComment = () => {
    if (Dimensions.get("window").width >= 1100) {
      let tempData =
        props.trendingHomeFeedId && props.trendingHomeFeedId.includes("Post:")
          ? props.trendingHomeFeedId.replace("Post:", "")
          : props.trendingHomeFeedId;
      let data1 = props.ActiveTab + props.item.item.node.id;
      let data2 = props.ActiveTab + "Post:" + tempData;
      let data1content = document.getElementById(data1);
      let data2content = document.getElementById(data2);
      if (data1content != null) {
        data1content.style.borderColor = "green";
      }
      if (data2content != null) {
        data2content.style.borderColor = "#c5c5c5";
      }
      if (data1 == data2) {
        goToPostDetailsScreen(
          props.item.item.node.id.replace("Post:", ""),
          false
        );
      } else {
        goToPostDetailsScreen(
          props.item.item.node.id.replace("Post:", ""),
          false
        );
      }
    } else {
      goToPostDetailsScreen(props.item.item.node.id.replace("Post:", ""), true);
    }
  };

  const handleDeleteFeed = id => {
    applloClient
      .query({
        query: DeleteContentMutation,
        variables: {
          content_id: id
        },
        fetchPolicy: "no-cache"
      })
      .then(response => {
        if (response.data.content_delete.status.success == true) {
          props.deleteItem(id);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  const goToUserProfile = async username => {
    await props.userId({
      username: username,
      type: "feed"
    });
    await props.navigation.navigate("profile", {
      username: username,
      type: "feed"
    });
  };

  return (
    <View
      style={{ marginHorizontal: Dimensions.get("window").width <= 750 ? 5 : 0 }}
      onMouseEnter={() => setshowMenu(true)}
      onMouseLeave={() =>
        MenuHover == true ? setshowMenu(true) : setshowMenu(false)
      }
    >
      <View
        nativeID={props.ActiveTab + props.item.item.node.id}
        style={{
          borderColor: "#D7D7D7",
          width: "100%",
          borderWidth: Dimensions.get("window").width <= 750 ? 1 : 3,
          borderRadius: 20,
          backgroundColor: "#fff"
        }}
      >
        {pressBug == true ? (
          Platform.OS !== "web" ? (
            <Overlay
              animationType="zoomIn"
              visible={pressBug}
              onClose={onClose}
              closeOnTouchOutside
              children={
                <FeedReportModal
                  onClose={onClose}
                  PostId={props.item.item.node.id}
                  TopicList={TopicList}
                  ClikList={ClikList}
                />
              }
              childrenWrapperStyle={{
                padding: 0,
                margin: 0,
                borderRadius: 6
              }}
            />
          ) : (
            <Modal
              isVisible={pressBug}
              onBackdropPress={onClose}
              style={{
                marginHorizontal:
                  Dimensions.get("window").width > 750 ? "30%" : 10,
                padding: 0
              }}
            >
              <FeedReportModal
                onClose={onClose}
                PostId={props.item.item.node.id}
                TopicList={TopicList}
                ClikList={ClikList}
              />
            </Modal>
          )
        ) : null}

        {pressShare == true ? (
          Platform.OS !== "web" ? (
            <Overlay
              animationType="zoomIn"
              visible={pressShare}
              onClose={onClose}
              closeOnTouchOutside
              children={
                <FeedShareModal
                  onClose={onClose}
                  PostId={props.item.item.node.id}
                  getTopic={getTopic}
                  getClik={getClik}
                />
              }
              childrenWrapperStyle={{
                padding: 0,
                margin: 0,
                borderRadius: 6
              }}
            />
          ) : (
            <Modal
              isVisible={pressShare}
              onBackdropPress={onClose}
              style={{
                marginHorizontal:
                  Dimensions.get("window").width > 750 ? "30%" : 10,
                padding: 0
              }}
            >
              <FeedShareModal
                onClose={onClose}
                PostId={props.item.item.node.id}
                getTopic={getTopic}
                getClik={getClik}
              />
            </Modal>
          )
        ) : null}
        {props.item.item.node.thumbnail_pic != null &&
          props.ViewMode != "Text" && (
            <View>
              <TouchableOpacity onPress={() => handleDoubleTap()}>
                <View>
                  <ImageComponent bgImage={bgImage} placeholderColor="#000" />
                </View>
              </TouchableOpacity>
            </View>
          )}

        {props.ViewMode != "Text" && (
          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-start",
              width: "100%",
              marginTop: 5
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "flex-start",
                flexWrap: "wrap",
                width: "95%"
              }}
            >
              {ClikList &&
                ClikList.map((t, i) => {
                  return (
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                        marginLeft: 5,
                        marginTop: 5,
                        padding: 3,
                        backgroundColor: "#E8F5FA",
                        borderRadius: 6
                      }}
                      key={i}
                    >
                      <Hoverable>
                        {isHovered => (
                          <TouchableHighlight
                            onPress={() => goToClikProfile(t)}
                          >
                            <Text
                              style={{
                                color: "#4169e1",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                fontWeight: "bold",
                                fontSize: 14,
                                textDecorationLine:
                                  isHovered == true ? "underline" : "none"
                              }}
                            >
                              #{t}
                            </Text>
                          </TouchableHighlight>
                        )}
                      </Hoverable>
                    </View>
                  );
                })}

              {TopicList &&
                TopicList.map((t, i) => {
                  return (
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                        marginLeft: 5,
                        marginTop: 5,
                        padding: 3,
                        backgroundColor: "#e3f9d5",
                        borderRadius: 6
                      }}
                      key={i}
                    >
                      <Hoverable>
                        {isHovered => (
                          <TouchableHighlight
                            onPress={() =>
                              goToProfile(
                                capitalizeFirstLetter(t.toLowerCase())
                              )
                            }
                          >
                            <Text
                              style={{
                                color: "#009B1A",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                fontWeight: "bold",
                                fontSize: 14,
                                textDecorationLine:
                                  isHovered == true ? "underline" : "none"
                              }}
                            >
                              /{t.toLowerCase()}
                            </Text>
                          </TouchableHighlight>
                        )}
                      </Hoverable>
                    </View>
                  );
                })}

              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  marginLeft: 5,
                  marginTop: 5,
                  padding: 10,
                  backgroundColor: "#f0f0f0",
                  borderRadius: 6,
                  height: 30
                }}
              >
                <Hoverable>
                  {isHovered => (
                    <TouchableHighlight
                      onPress={() =>
                        props.loginStatus == 1
                          ? handleShareClick()
                          : props.setLoginModalStatus(true)
                      }
                    >
                      <Text
                        style={{
                          color: "#969faa",
                          fontFamily: ConstantFontFamily.MontserratBoldFont,
                          fontWeight: "bold",
                          fontSize: 25,
                          textDecorationLine:
                            isHovered == true ? "underline" : "none"
                        }}
                      >
                        +
                      </Text>
                    </TouchableHighlight>
                  )}
                </Hoverable>
              </View>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "flex-end",
                marginRight: 5
              }}
            >
              {/* {showMenu == true && ( */}
              <TouchableOpacity
                onMouseEnter={() => setMenuHover(true)}
                onMouseLeave={() => setMenuHover(false)}
              >
                <Menu>
                  <MenuTrigger>
                    <Image
                      source={require("../assets/image/menu.png")}
                      style={{
                        height: 16,
                        width: 16,
                        alignSelf: "flex-end",
                        marginTop: 5,
                        transform: [{ rotate: "90deg" }]
                      }}
                    />
                  </MenuTrigger>
                  <MenuOptions
                    optionsContainerStyle={{
                      borderRadius: 6,
                      borderWidth: 1,
                      borderColor: "#e1e1e1",
                      shadowColor: "transparent"
                    }}
                    customStyles={{
                      optionsContainer: {
                        minHeight: 50,
                        width: 150,
                        marginTop: 15,
                        marginLeft: -140
                      }
                    }}
                  >
                    <MenuOption
                      onSelect={() => {
                        props.loginStatus == 1
                          ? handleDeleteFeed(props.item.item.node.id)
                          : props.setLoginModalStatus(true);
                      }}
                    >
                      <Hoverable>
                        {isHovered => (
                          <Text
                            style={{
                              textAlign: "center",
                              color: isHovered == true ? "#009B1A" : "#000",
                              fontFamily: ConstantFontFamily.MontserratBoldFont
                            }}
                          >
                            Delete
                          </Text>
                        )}
                      </Hoverable>
                    </MenuOption>

                    <MenuOption
                      onSelect={() => {
                        props.loginStatus == 1
                          ? handleBugClick()
                          : props.setLoginModalStatus(true);
                      }}
                    >
                      <Hoverable>
                        {isHovered => (
                          <Text
                            style={{
                              textAlign: "center",
                              color: isHovered == true ? "#009B1A" : "#000",
                              fontFamily: ConstantFontFamily.MontserratBoldFont
                            }}
                          >
                            Report
                          </Text>
                        )}
                      </Hoverable>
                    </MenuOption>

                    <MenuOption onSelect={() => writeToClipboard()}>
                      <Hoverable>
                        {isHovered => (
                          <Text
                            style={{
                              textAlign: "center",
                              color: isHovered == true ? "#009B1A" : "#000",
                              fontFamily: ConstantFontFamily.MontserratBoldFont
                            }}
                          >
                            Copy Link
                          </Text>
                        )}
                      </Hoverable>
                    </MenuOption>
                  </MenuOptions>
                </Menu>
              </TouchableOpacity>
              {/* )} */}
            </View>
          </View>
        )}

        <View
          style={{
            width: "100%",
            marginTop: 10
          }}
        >
          <TextInput
            value={props.item.item.node.title}
            editable={false}
            style={{
              textAlign: "center",
              color: "#000",
              paddingHorizontal: 10,
              fontWeight: "bold",
              fontFamily: ConstantFontFamily.MontserratBoldFont,
              fontSize: 14,
              height: titleHeight
            }}
            scrollEnabled={false}
            onContentSizeChange={e => {
              settitleHeight(e.nativeEvent.contentSize.height);
            }}
            multiline={true}
          />
        </View>

        {props.ViewMode == "Text" && (
          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-start",
              flexWrap: "wrap",
              width: "100%",
              marginTop: 5
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "flex-start",
                flexWrap: "wrap",
                width: "100%"
              }}
            >
              {props.item.item.node.topics &&
                props.item.item.node.topics.map((t, i) => {
                  return (
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                        marginLeft: 5,
                        marginTop: 5,
                        padding: 3,
                        backgroundColor: "#e3f9d5",
                        borderRadius: 6
                      }}
                      key={i}
                    >
                      <Hoverable>
                        {isHovered => (
                          <TouchableHighlight
                            onPress={() =>
                              goToProfile(
                                capitalizeFirstLetter(t.toLowerCase())
                              )
                            }
                          >
                            <Text
                              style={{
                                color: "#009B1A",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                fontWeight: "bold",
                                fontSize: 14,
                                textDecorationLine:
                                  isHovered == true ? "underline" : "none"
                              }}
                            >
                              /{t.toLowerCase()}
                            </Text>
                          </TouchableHighlight>
                        )}
                      </Hoverable>
                    </View>
                  );
                })}
            </View>
          </View>
        )}

        <TextInput
          value={
            props.item.item.node.summary != null
              ? props.item.item.node.summary.length > 300
                ? props.item.item.node.summary.substring(0, 297) + "..."
                : props.item.item.node.summary
              : null
          }
          editable={false}
          style={{
            color: "#000",
            padding: 5,
            fontFamily: ConstantFontFamily.VerdanaFont,
            fontSize: 13,
            lineHeight: 20,
            height: summaryHeight
          }}
          scrollEnabled={false}
          onContentSizeChange={e => {
            setsummaryHeight(e.nativeEvent.contentSize.height);
          }}
          multiline={true}
        />

        {props.item.item.node.reasons_shared && (
          <DiscussByUsersList
            title={`Discussed By`}
            items={props.item.item.node.reasons_shared.users}
            keyName={"username"}
            displayCount={
              props.ViewMode != "Card" || props.ViewMode == "Text" ? 2 : 1
            }
          />
        )}

        <View
          style={{
            flex: 1,
            flexDirection: "row",
            padding: 3,
            justifyContent: "space-between",
            marginBottom: props.ViewMode == "Card" ? 8 : 0
          }}
        >
          <TouchableOpacity>
            <Icon
              name={"pencil-square-o"}
              color={"#969faa"}
              type="font-awesome"
              size={21}
              iconStyle={{ alignSelf: "center" }}
              containerStyle={{ alignSelf: "center" }}
              onPress={() => props.setPostEditDetails(props.item.item.node)}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              flexDirection: "row",
              marginTop: -5
            }}
            onPress={() =>
              props.loginStatus == 1
                ? handleClickComment()
                : props.setLoginModalStatus(true)
            }
          >
            <Icon
              color={getColour(props.item.item.node.comments_score)}
              name={"comment-o"}
              type="font-awesome"
              size={21}
              iconStyle={{ alignSelf: "center" }}
              containerStyle={{ alignSelf: "center" }}
            />
            <Text
              style={{
                textAlign: "center",
                fontWeight: "bold",
                fontSize: 16,
                paddingHorizontal: 3,
                paddingTop: 5
              }}
            >
              {props.item.item.node.admin_stats &&
                props.item.item.node.admin_stats.num_comments}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity style={{ flexDirection: "row" }}>
            <ChangeIcon
              PostId={props.item.item.node.id}
              selectedIconList={props.item.item.node.user_like_type}
              score={props.item.item.node.likes_score}
              loginModalStatus={loginModalStatusEvent}
              onOpen={status => handleClick(status)}
              showStatus={showBug}
              pressBug={pressBug}
            />
            <Text
              style={{
                textAlign: "center",
                fontWeight: "bold",
                fontSize: 16,
                paddingHorizontal: 3
              }}
            >
              {props.item.item.node.admin_stats &&
                props.item.item.node.admin_stats.num_likes}
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            width: "100%",
            flexDirection: "column-reverse",
            paddingHorizontal: 5,
            paddingTop: 5,
            paddingBottom: 7
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "row"
            }}
          >
            {props.item.item.node.link == "" ||
            props.item.item.node.link == null ? (
              <Hoverable>
                {isHovered => (
                  <Text
                    style={{
                      justifyContent: "flex-start",
                      alignContent: "flex-start",
                      color: "#6D757F",
                      paddingRight: 10,
                      fontFamily: ConstantFontFamily.VerdanaFont,
                      fontSize: 13,
                      alignSelf: "center",
                      textDecorationLine:
                        isHovered == true ? "underline" : "none"
                    }}
                    onPress={() =>
                      goToUserProfile(
                        props.item.item.node.author &&
                          props.item.item.node.author.username
                      )
                    }
                  >
                    @
                    {props.item.item.node.author &&
                      props.item.item.node.author.username}
                  </Text>
                )}
              </Hoverable>
            ) : (
              <Hoverable>
                {isHovered => (
                  <View
                    style={{
                      flexDirection: "row"
                    }}
                  >
                    {props.item.item.node.link != null &&
                    props.item.item.node.link != "" ? (
                      props.item.item.node.external_feed != null &&
                      props.item.item.node.external_feed.icon_url != null ? (
                        <TouchableOpacity
                          style={{
                            alignSelf: "center"
                          }}
                          onPress={() =>
                            goToFeedProfile(
                              props.item.item.node.external_feed.id.replace(
                                "ExternalFeed:",
                                ""
                              )
                            )
                          }
                        >
                          <Image
                            source={{
                              uri: props.item.item.node.external_feed.icon_url
                            }}
                            style={{
                              width: 20,
                              height: 20,
                              borderRadius: 10
                            }}
                          />
                        </TouchableOpacity>
                      ) : (
                        <View style={{ alignSelf: "center" }}>
                          <Icon
                            name="link"
                            type="font-awesome"
                            color="#6D757F"
                            size={15}
                          />
                        </View>
                      )
                    ) : null}
                    <TouchableOpacity
                      style={{
                        alignSelf: "center",
                        marginLeft: 5
                      }}
                      onPress={() => openWindow(props.item.item.node.link)}
                    >
                      <Text
                        style={{
                          textAlign: "center",
                          alignSelf: "center",
                          color: "#6D757F",
                          fontSize: props.ViewMode == "Card" ? 11 : 13,
                          fontFamily: ConstantFontFamily.VerdanaFont,
                          textDecorationLine:
                            isHovered == true ? "underline" : "none"
                        }}
                      >
                        {
                          props.item.item.node.link
                            .replace("http://", "")
                            .replace("https://", "")
                            .replace("www.", "")
                            .split(/[/?#]/)[0]
                        }
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              </Hoverable>
            )}
            <View style={{ alignSelf: "center", marginLeft: 15 }}>
              <Text
                style={{
                  textAlign: "center",
                  color: "#6D757F",
                  fontSize: props.ViewMode == "Card" ? 10 : 12,
                  fontFamily: ConstantFontFamily.VerdanaFont,
                  alignSelf: "center"
                }}
              >
                {props.ViewMode != "Card" && props.ViewMode != "Default"
                  ? moment
                      .utc(props.item.item.node.created)
                      .local()
                      .fromNow()
                  : moment
                      .utc(props.item.item.node.created)
                      .local()
                      .fromNow(true)}
              </Text>
            </View>
          </View>
        </View>
      </View>

      {props.ViewMode != "Card" && (
        <View
          style={{
            marginBottom: 10
          }}
        ></View>
      )}
    </View>
  );
};

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  trendingHomeFeedId: state.PostCommentDetailsReducer.get("PostId")
});

const mapDispatchToProps = dispatch => ({
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  cliksUserId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  topicId: payload => dispatch(getTrendingTopicsProfileDetails(payload)),
  listTopicFeed: payload => dispatch(listTopicFeed(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setFeedReportModalAction: payload =>
    dispatch(setFEEDREPORTMODALACTION(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload)),
  setFeedDetails: payload => dispatch(getFeedProfileDetails(payload)),
  setPostEditDetails: payload => dispatch(postEditDetails(payload)),
  setComment: payload => dispatch({ type: "SET_COMMENT", payload }),
  setFeedShareModal: payload => dispatch({ type: "SHAREFEEDSTATUS", payload }),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setPostCommentReset: payload =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload }),
  userId: payload => dispatch(getCurrentUserProfileDetails(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  React.memo(FeedList)
);
