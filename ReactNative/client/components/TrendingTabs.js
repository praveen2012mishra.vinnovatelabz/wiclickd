import React, { Component, lazy, Suspense } from "react";
import {
  Dimensions,
  ScrollView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet
} from "react-native";
import { TabBar, TabView } from "react-native-tab-view";
import ShadowSkeletonStar from "../components/ShadowSkeletonStar";
import ConstantFontFamily from "../constants/FontFamily";
import { retry } from "../library/Helper";
import { Button, Icon } from "react-native-elements";
import NavigationService from "../library/NavigationService";
import { connect } from "react-redux";
import { compose } from "recompose";
import ButtonStyle from "../constants/ButtonStyle";

const TrendingCliks = lazy(() =>
  retry(() => import("../components/TrendingClicks"))
);
const TrendingExternalFeed = lazy(() =>
  retry(() => import("../components/TrendingExternalFeed"))
);
const TrendingTopics = lazy(() =>
  retry(() => import("../components/TrendingTopics"))
);
const TrendingUsers = lazy(() =>
  retry(() => import("../components/TrendingUsers"))
);

class TrendingTabs extends Component {
  state = {
    index: 0,
    routes: [
      { key: "first", title: "Cliks", icon: "users", type: "font-awesome" },
      { key: "second", title: "Topics", icon: "book", type: "font-awesome" },
      { key: "third", title: "Users", icon: "user", type: "font-awesome" },
      { key: "fourth", title: "Feeds", icon: "rss", type: "font-awesome" }
    ],
    searchedFollowText: this.props.searchedFollowText
  };

  constructor(props) {
    super(props);
  }

  componentDidUpdate = async prevProps => {
    if (prevProps.searchedFollowText != this.props.searchedFollowText) {
      await this.setState({
        searchedFollowText: this.props.searchedFollowText
      });
    }
  };

  _renderTabBar = props => (
    // Dimensions.get("window").width >= 750 &&
    <View style={{height: 70}}>
      <TabBar
        {...props}
        indicatorStyle={{
          backgroundColor: "transparent",
          height: 1,
          borderRadius: 6
          // width: "18.75%",
          // marginHorizontal: "3.125%"
        }}
        style={{ backgroundColor: "transparent", shadowColor: "transparent" , height:60}}
        labelStyle={{
          color: "#000",
          fontFamily: ConstantFontFamily.MontserratBoldFont,
          fontSize: 13,
          fontWeight: "bold",
          height:60,
          justifyContent:'center'
        }}
        renderIcon={({ route, focused, color }) => (
          <Icon
            name={route.icon}
            type={route.type}
            color={focused ? "#009B1A" : "#D3D3D3"}
          />
        )}
        renderLabel={({ route, focused, color }) => (
          <Text
            style={{
              color: focused ? "#009B1A" : "#D3D3D3",
              fontFamily: ConstantFontFamily.MontserratBoldFont
            }}
          >
            {route.title}
          </Text>
        )}
        // renderIndicator={({ route, focused, color }) => null}
      />
      {
        <View
          style={{
            width: "100%",
            height: 40,
            justifyContent: "flex-start",
            flexDirection: "row",
            alignItems: "center",
            //borderColor: "#000",
            //borderWidth: 1,
            borderRadius: 5,
            marginTop:  30,
            marginBottom:5
            //paddingHorizontal: "3%",
            //visibility: 'hidden'
          }}
        >
          <View style={{ width: "10%", marginRight: "auto" }}>
            <Icon name="search" size={18} type="font-awesome" />
          </View>

          <TextInput
            autoFocus={true}
            // placeholder="Search Weclikd"
            placeholder={
              this.state.index == 0
                ? "Search Cliks"
                : this.state.index == 1
                ? "Search Topics"
                : this.state.index == 2
                ? "Search Users"
                : this.state.index == 3 && "Search Feeds"
            }
            onChangeText={query => {
              // console.log(this.props.searchedFollowText,'------------------------------->');
              this.props.onchangetext(query);
            }}
            value={this.props.searchedFollowText}
            style={{
              height: 40,
              width: "74%",
              paddingHorizontal: 10,
              //border: "none",
              outline: "none",
              position: "absolute",
              left: "13%",
              fontFamily: ConstantFontFamily.defaultFont
            }}
          />

          {this.props.searchedFollowText ? (
            <TouchableOpacity
              onPress={() => this.props.onpress("")}
              style={{ marginLeft: "auto", width: "10%" }}
            >
              <Icon name="close" size={18} type="font-awesome" />
            </TouchableOpacity>
          ) : null}
        </View>
      }
    </View>
  );

  _handleIndexChange = index =>
    this.setState({ index, searchedFollowText: "" }, () => {
      this.props.activeIndex(index);
    });

  _renderLazyPlaceholder = ({ route }) => <ShadowSkeletonStar />;

  _renderScene = ({ route }) => {
    switch (route.key) {
      case "first":
        return (
          <View style={{ flex: 1 }}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Suspense fallback={<ShadowSkeletonStar />}>
                <TrendingCliks
                  navigation={this.props.navigation}
                  searchedFollowText={this.state.searchedFollowText}
                />
              </Suspense>
            </ScrollView>
            {/* {this.props.getUsernameModalStatus == false && (
              <Button
                onPress={() =>
                  NavigationService.navigate("search", {
                    type: "CLIKS"
                  })
                }
                color="#fff"
                title="View All Cliks"
                titleStyle={{
                  fontSize: 14,
                  fontWeight: "bold",
                  color: "#000",
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
                buttonStyle={{
                  backgroundColor: "#fff",
                  borderRadius: 20,
                  borderColor: "#c5c5c5",
                  borderWidth: 1
                }}
                containerStyle={{
                  marginVertical: 10,
                  marginLeft: 5,
                  width: 150,
                  height: 40,
                  alignSelf: "center"
                }}
              />
            )} */}
          </View>
        );
      case "second":
        return (
          <View style={{ flex: 1 }}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Suspense fallback={<ShadowSkeletonStar />}>
                <TrendingTopics
                  navigation={this.props.navigation}
                  randomItemEvent={this.props.randomItemEvent}
                  searchedFollowText={this.state.searchedFollowText}
                />
              </Suspense>
            </ScrollView>
            {this.props.getUsernameModalStatus == false && (
              <Button
                onPress={() => NavigationService.navigate("topichierarchy")}
                color="#fff"
                title="Topic Hierarchy"
                titleStyle={ButtonStyle.titleStyle}
                buttonStyle={ButtonStyle.backgroundStyle}
                containerStyle={ButtonStyle.containerStyle}
              />
            )}
          </View>
        );
      case "third":
        return (
          <View style={{ flex: 1 }}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Suspense fallback={<ShadowSkeletonStar />}>
                <TrendingUsers
                  navigation={this.props.navigation}
                  searchedFollowText={this.state.searchedFollowText}
                />
              </Suspense>
            </ScrollView>
            {/* {this.props.getUsernameModalStatus == false && (
              <Button
                onPress={() =>
                  NavigationService.navigate("search", {
                    type: "USERS"
                  })
                }
                color="#fff"
                title="View All Users"
                titleStyle={{
                  fontSize: 14,
                  fontWeight: "bold",
                  color: "#000",
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
                buttonStyle={{
                  backgroundColor: "#fff",
                  borderRadius: 20,
                  borderColor: "#c5c5c5",
                  borderWidth: 1
                }}
                containerStyle={{
                  marginVertical: 10,
                  marginLeft: 5,
                  width: 150,
                  height: 40,
                  alignSelf: "center"
                }}
              />
            )} */}
          </View>
        );
      default:
        return (
          <View style={{ flex: 1 }}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <Suspense fallback={<ShadowSkeletonStar />}>
                <TrendingExternalFeed
                  navigation={this.props.navigation}
                  searchedFollowText={this.state.searchedFollowText}
                />
              </Suspense>
            </ScrollView>
            {/* {this.props.getUsernameModalStatus == false && (
              <Button
                onPress={() =>
                  NavigationService.navigate("search", {
                    type: "FEEDS"
                  })
                }
                color="#fff"
                title="View All Feeds"
                titleStyle={{
                  fontSize: 14,
                  fontWeight: "bold",
                  color: "#000",
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
                buttonStyle={{
                  backgroundColor: "#fff",
                  borderRadius: 20,
                  borderColor: "#c5c5c5",
                  borderWidth: 1
                }}
                containerStyle={{
                  marginVertical: 10,
                  marginLeft: 5,
                  width: 150,
                  height: 40,
                  alignSelf: "center"
                }}
              />
            )} */}
          </View>
        );
    }
  };

  render() {
    //console.log(this.props.getUsernameModalStatus,'==================>');
    return (
      <TabView
        lazy
        navigationState={this.state}
        renderScene={this._renderScene}
        renderLazyPlaceholder={this._renderLazyPlaceholder}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
        // initialLayout={{
        //   width: Dimensions.get("window").width,
        //   height: Dimensions.get("window").height
        // }}
        style={{
          marginRight: 1
        }}
      />
    );
  }
}

const mapStateToProps = state => ({
  getUsernameModalStatus: state.UsernameModalReducer.get("modalStatus")
});

const mapDispatchToProps = dispatch => ({
  setSearchBarStatus: payload =>
    dispatch({ type: "SEARCH_BAR_STATUS", payload })
});

//export default TrendingTabs;
export default compose(connect(mapStateToProps, mapDispatchToProps))(
  TrendingTabs
);

const styles = StyleSheet.create({
  container: {
    fontFamily: ConstantFontFamily.MontserratBoldFont,
    fontSize: 44,
    height: 40,
    backgroundColor: "#000"
  }
});
