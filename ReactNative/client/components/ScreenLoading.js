import Modal from "modal-enhanced-react-native-web";
import React from "react";
import { Dimensions, Platform, View } from "react-native";
import Overlay from "react-native-modal-overlay";
import { ActivityIndicator } from "react-native-web";
import { connect } from "react-redux";
import { compose } from "recompose";

const ScreenLoading = (props) => {
  return (
    <View
      onLayout={(event) => {
        let { width, height } = event.nativeEvent.layout;
      }}
    >
      {Platform.OS == "web" ? (
        <Modal
          isVisible={props.isVisible}
          navigation={props.navigation}
          style={{
            marginHorizontal: Dimensions.get("window").width > 750 ? "30%" : 0,
            padding: 0,
          }}
        >
          <View
            style={{
              height: Dimensions.get("window").height,
              paddingVertical: 10,
            }}
          >
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <ActivityIndicator size="large" color="#FFF" />
            </View>
          </View>
        </Modal>
      ) : (
        <Overlay
          animationType="zoomIn"
          isVisible={props.isVisible}
          children={
            <View
              style={{
                height: Dimensions.get("window").height,
                paddingVertical: 10,
              }}
            >
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <ActivityIndicator size="large" color="#FFF" />
              </View>
            </View>
          }
          closeOnTouchOutside
          childrenWrapperStyle={{
            padding: 0,
          }}
        ></Overlay>
      )}
    </View>
  );
};

export default compose(connect(null, null))(ScreenLoading);
