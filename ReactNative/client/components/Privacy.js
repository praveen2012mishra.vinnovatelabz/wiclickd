import Modal from "modal-enhanced-react-native-web";
import React from "react";
import { Dimensions, Platform, StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-elements";
import Overlay from "react-native-modal-overlay";
import RNPickerSelect from "react-native-picker-select";
import PrivacyPolicyModal from "../components/PrivacyPolicyModal";
import ConstantFontFamily from "../constants/FontFamily";
import ButtonStyle from "../constants/ButtonStyle";

export default class Privacy extends React.Component {
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.state = {
      showPrivacyPolicyModal: false,
      RoleItems: [
        {
          label: "Member",
          value: "Member",
          key: 0
        },
        {
          label: "Admin",
          value: "Admin",
          key: 1
        },
        {
          label: "Super Admin",
          value: "Super Admin",
          key: 2
        }
      ],
      SelectRoleItems: "Member"
    };
  }
  onClose = () => {
    this.setState({
      showPrivacyPolicyModal: false
    });
  };
  onOpen = () => {
    this.setState({
      showPrivacyPolicyModal: true
    });
  };
  render() {
    const { showPrivacyPolicyModal } = this.state;
    return (
      <View
        style={{
          marginHorizontal: 20
        }}
      >
        {showPrivacyPolicyModal == true ? (
          Platform.OS !== "web" ? (
            <Overlay
              animationType="zoomIn"
              visible={showPrivacyPolicyModal}
              onClose={this.onClose}
              closeOnTouchOutside
              children={
                <PrivacyPolicyModal onClose={this.onClose} {...this.props} />
              }
              childrenWrapperStyle={{
                padding: 0,
                margin: 0
              }}
            />
          ) : (
            <Modal
              isVisible={showPrivacyPolicyModal}
              onBackdropPress={this.onClose}
              style={{
                marginHorizontal:
                  Dimensions.get("window").width > 750 ? "30%" : 10,
                padding: 0
              }}
            >
              <PrivacyPolicyModal onClose={this.onClose} {...this.props} />
            </Modal>
          )
        ) : null}
        <View
          style={{ flexDirection: "row", width: "100%", marginVertical: 10 }}
        >
          <View style={{ justifyContent: "flex-start", width: "50%" }}>
            <Text
              style={{
                alignSelf: "center",
                textAlign: "center",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold"
              }}
            >
              Who can message me:
            </Text>
          </View>
          <View
            style={{
              justifyContent: "flex-end",
              alignSelf: "center",
              width: "50%",
              marginLeft: 10,
              borderColor: "#e1e1e1",
              borderWidth: 1,
              borderRadius: 6,
              alignSelf: "center"
            }}
          >
            <RNPickerSelect
              placeholder={{}}
              items={this.state.RoleItems}
              onValueChange={(itemValue, itemIndex) => {
                this.setState({
                  SelectRoleItems: itemValue
                });
              }}
              onUpArrow={() => {
                this.inputRefs.name.focus();
              }}
              onDownArrow={() => {
                this.inputRefs.picker2.togglePicker();
              }}
              style={{ ...styles }}
              value={this.state.SelectRoleItems}
              ref={el => {
                this.inputRefs.picker = el;
              }}
            />
          </View>
        </View>

        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            marginVertical: 10
          }}
        >
          <Button
            color="#fff"
            title="Save"
            titleStyle={ButtonStyle.titleStyle}
            buttonStyle={ButtonStyle.backgroundStyle}
            containerStyle={ButtonStyle.containerStyle}
          />
        </View>

        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            marginVertical: 10
          }}
        >
          <Button
            color="#fff"
            title="View Privacy Policy"
            titleStyle={ButtonStyle.titleStyle}
            buttonStyle={ButtonStyle.backgroundStyle}
            containerStyle={ButtonStyle.containerStyle}
            onPress={this.onOpen}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  inputAndroid: {
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#fff",
    borderRadius: 8,
    color: "#000",
    backgroundColor: "white",
    paddingRight: 30,
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  }
});
