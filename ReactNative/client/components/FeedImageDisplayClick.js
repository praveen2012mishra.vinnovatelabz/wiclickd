import { List } from "immutable";
import moment from "moment";
import React, { Component } from "react";
import {
    Image,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import { Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";

class FeedImageDisplayClick extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <View style={{
                paddingLeft: this.props.item.length == 1 ? 20 : 5,
                paddingTop: 5,
                top: this.props.item.length == 1 ? 25 : 15,
                justifyContent: this.props.item.length == 1 ? 'center' : 'flex-start',
            }}>
                {this.props.item.length > 0 && (<Image
                    //source={require("../assets/image/default-image.png")}
                    source={this.props.item[0].profile_pic ? this.props.item[0].profile_pic : require("../assets/image/default-image.png")}
                    style={{
                        width: 30,
                        height: 30,
                        borderRadius: 10,
                        // borderWidth: 1,
                        // borderColor: "#e1e1e1",
                        marginRight: 5,
                    }}
                />)}
                {this.props.item.length > 1 && (<Image
                    //source={require("../assets/image/default-image.png")}
                    source={this.props.item[1].profile_pic ? this.props.item[1].profile_pic : require("../assets/image/default-image.png")}
                    style={{
                        width: 30,
                        height: 30,
                        borderRadius: 10,
                        // borderWidth: 1,
                        // borderColor: "#e1e1e1",
                        marginRight: 5,
                        position: 'absolute', left: 25, top: 20
                    }}
                />)}
                {this.props.item.length > 2 && (<Image
                    //source={require("../assets/image/default-image.png")}
                    source={this.props.item[2].profile_pic ? this.props.item[2].profile_pic : require("../assets/image/default-image.png")}
                    style={{
                        width: 30,
                        height: 30,
                        borderRadius: 10,
                        // borderWidth: 1,
                        // borderColor: "#e1e1e1",
                        marginRight: 5,
                    }}
                />)}
            </View>

        );
    }

}

const mapStateToProps = state => ({
    loginStatus: state.UserReducer.get("loginStatus"),
    isAdmin: state.AdminReducer.get("isAdmin"),
});

const mapDispatchToProps = dispatch => ({
    setActiveId: payload => dispatch({ type: "SET_ACTIVE_ID", payload })
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
    FeedImageDisplayClick
);
