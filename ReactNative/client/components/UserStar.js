import "@expo/browser-polyfill";
import _ from "lodash";
import { List } from "immutable";
import React, { Component } from "react";
import { graphql } from "react-apollo";
import { Image, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setLikeContent } from "../actionCreator/LikeContentAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingExternalFeeds } from "../actionCreator/TrendingExternalFeedsAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import { getTrendingUsers } from "../actionCreator/TrendingUsersAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import AppHelper from "../constants/AppHelper";
import {
  UserFollowMutation,
  UserUnfollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  UserFollowVariables,
  UserUnfollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";

class UserStar extends Component {
  constructor(props) {
    super(props);
    const gstar = require("../assets/image/gstar.png");
    const ystar = require("../assets/image/ystar.png");
    const wstar = require("../assets/image/wstar.png");

    this.state = {
      index: 0,
      starList: [wstar, gstar, ystar],
      followList: ["TRENDING", "FOLLOW", "FAVORITE"]
    };
  }

  async componentDidMount() {
    await this.getUserStar(this.props.UserName);
  }

  componentDidUpdate = async prevProps => {
    if (prevProps.UserName !== this.props.UserName) {
      await this.getUserStar(this.props.UserName);
    }
    if (prevProps.getUserFollowUserList !== this.props.getUserFollowUserList) {
      await this.getUserStar(this.props.UserName);
    }
  };

  getUserStar = async UserName => {
    if (this.props.loginStatus == 0) {
      await this.setState({
        index: 0
      });
      return false;
    }
    let index = 0;
    index = this.props.getUserFollowUserList.findIndex(
      i =>
        i.getIn(["user", "username"]).replace("user:", "") == UserName &&
        UserName.replace("user:", "")
    );
    if (index == -1) {
      await this.setState({
        index: 0
      });
    } else if (
      this.props.getUserFollowUserList.getIn([
        index,
        "settings",
        "follow_type"
      ]) == "FOLLOW"
    ) {
      await this.setState({
        index: 1
      });
    } else {
      await this.setState({
        index: 2
      });
    }
  };

  changeStar = async () => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    if (this.state.index + 1 === this.state.starList.length) {
      await this.setState({
        index: 0
      });
      await this.changeStarApi(this.state.followList[this.state.index]);
    } else {
      await this.setState({
        index: this.state.index + 1
      });
      await this.changeStarApi(this.state.followList[this.state.index]);
    }
  };

  changeStarApi = _.debounce(typeIndex => {
    typeIndex == "FAVORITE"
      ? this.favroiteUser(this.props.UserId)
      : typeIndex == "TRENDING"
      ? this.unfollowUser(this.props.UserId)
      : this.followUser(this.props.UserId);
  }, 1000);

  followUser = userId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    UserFollowVariables.variables.user_id = userId;
    UserFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: UserFollowMutation,
        ...UserFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
        // await this.props.getTrendingUsers({
        //   currentPage: AppHelper.PAGE_LIMIT
        // });
      });
  };

  favroiteUser = userId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    UserFollowVariables.variables.user_id = userId;
    UserFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: UserFollowMutation,
        ...UserFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
        // await this.props.getTrendingUsers({
        //   currentPage: AppHelper.PAGE_LIMIT
        // });
      });
  };

  unfollowUser = async userId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    UserUnfollowVariables.variables.user_id = userId;
    applloClient
      .query({
        query: UserUnfollowMutation,
        ...UserUnfollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
        // await this.props.getTrendingUsers({
        //   currentPage: AppHelper.PAGE_LIMIT
        // });
      });
  };

  render() {
    return (
      <TouchableOpacity
        style={this.props.ContainerStyle}
        onPress={() => this.changeStar()}
      >
        <Image
          source={this.state.starList[this.state.index]}
          style={this.props.ImageStyle}
        />
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  DiscussionHomeFeed: state.HomeFeedReducer.get("DiscussionHomeFeedList"),
  getUserFollowUserList: state.LoginUserDetailsReducer.get("userFollowUserList")
    ? state.LoginUserDetailsReducer.get("userFollowUserList")
    : List()
});

const mapDispatchToProps = dispatch => ({
  LikeContent: payload => dispatch(setLikeContent(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setDiscussionHomeFeed: payload =>
    dispatch({ type: "SET_DISCUSSION_HOME_FEED", payload }),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  getTrendingUsers: payload => dispatch(getTrendingUsers(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  getTrendingExternalFeeds: payload =>
    dispatch(getTrendingExternalFeeds(payload))
});

const UserStarWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(UserStar);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  UserStarWrapper
);
