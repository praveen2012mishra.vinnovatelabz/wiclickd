import React, { useEffect, useState } from "react";
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Icon, Button } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { postLink } from "../actionCreator/LinkPostAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { setSHARELINKMODALACTION } from "../actionCreator/ShareLinkModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import applloClient from "../client";
import ConstantFontFamily from "../constants/FontFamily";
import { UrlInfoMutation } from "../graphqlSchema/graphqlMutation/PostMutation";
import { UrlInfoVariables } from "../graphqlSchema/graphqlVariables/PostVariables";
import NavigationService from "../library/NavigationService";
import ProgressBar from "./ProgressBar";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import ButtonStyle from "../constants/ButtonStyle";

const LinkAddAlertCard = props => {
  let textInput = null;
  useEffect(() => {
    return handleClick();
  }, []);
  const [link, setLink] = useState("");
  const [Error, setError] = useState("");
  const [loading, setloading] = useState(false);
  let [value, setvalue] = useState(0);

  async function getProgress() {
    if (value < 100) {
      setvalue(value + 100);
    }
  }

  function isUrl(s) {
    var regexp = /(https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return regexp.test(s);
  }

  async function checkLink() {
    if (link.startsWith("https") == false) {
      setError("All links must start with https.");
      return false;
    }
    if (isUrl(link) == false) {
      setError("Not a valid URL");
      return false;
    }
  }

  async function getContent() {
    if (link.startsWith("https") == false) {
      setError("All links must start with https.");
      return false;
    }
    if (isUrl(link) == false) {
      setError("Not a valid URL");
      return false;
    }
    UrlInfoVariables.variables.url = link;
    await getProgress();
    setloading(true);
    var sendDate = new Date().getTime();
    try {
      applloClient
        .query({
          query: UrlInfoMutation,
          ...UrlInfoVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          setError("");
          if (res.data.url_info.status == "UNFURL_SUCCESS") {
            await props.postlink({
              description: res.data.url_info.unfurl.summary,
              image: res.data.url_info.unfurl.thumbnail_url,
              title: res.data.url_info.unfurl.title,
              url: res.data.url_info.unfurl.normalized_url,
              withoutUrl: false
            });
            await NavigationService.navigate("createpost");
            setLink("");
            props.setShareLinkModalStatus(false);
            var receiveDate = await new Date().getTime();
            var responseTimeMs = receiveDate - sendDate;
            let timer = setTimeout(() => {
              setloading(false);
            }, responseTimeMs);
          } else if (res.data.url_info.status == "ALREADY_SHARED") {
            props.setPostDetails({
              id: res.data.url_info.post.id,
              title: "Home",
              navigate: true
            });
            props.setPostCommentDetails({
              id: res.data.url_info.post.id,
              title: res.data.url_info.post.title
            });
            setloading(false);
            setLink("");
            props.setShareLinkModalStatus(false);
          } else {
            props.postlink({
              description: "",
              image: "",
              title: "",
              url: link,
              withoutUrl: false
            });
            setloading(false);
            setLink("");
            props.setShareLinkModalStatus(false);
            NavigationService.navigate("createpost");
          }
        })
        .catch(e => {
          props.postlink({
            description: "",
            image: "",
            title: "",
            url: link,
            withoutUrl: false
          });
          setloading(false);
          setLink("");
          props.setShareLinkModalStatus(false);
          NavigationService.navigate("createpost");
        });
    } catch (e) {
      props.postlink({
        description: "",
        image: "",
        title: "",
        url: link,
        withoutUrl: false
      });
      setloading(false);
      setLink("");
      props.setShareLinkModalStatus(false);
      NavigationService.navigate("createpost");
    }
  }

  function checkEnter(e) {
    var code = e.keyCode || e.which;
    if (code === 13) {
      getContent();
    }
  }

  function handleClick() {
    textInput.focus();
  }

  function navigatePost() {
    props.postlink({
      description: "",
      image: "",
      title: "",
      url: "",
      withoutUrl: true
    });
    setloading(false);
    setLink("");
    props.setShareLinkModalStatus(false);
    NavigationService.navigate("createpost");
  }
  return (
    <View
      style={{
        width: "100%",
      }}
    >
      <Hoverable>
        {isHovered => (
          <TouchableOpacity
            style={{
              flexDirection: "row",
              justifyContent: "flex-start",
              flex: 1,
              position: "absolute",
              zIndex: 999999,
              left: 0,
              top: 0
            }}
            onPress={props.onClose}
          >
            <Icon
              color={isHovered == true ? "rgba(256,256,256,0.4)" : "#000"}
              iconStyle={{
                color: "#fff",
                justifyContent: "center",
                alignItems: "center"
              }}
              reverse
              name="close"
              type="antdesign"
              size={16}
            />
          </TouchableOpacity>
        )}
      </Hoverable>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          backgroundColor: "#000",
          alignItems: "center",
          height: 50,
          borderTopLeftRadius: 6,
          borderTopRightRadius: 6
        }}
      >
        <Image
          source={
            Platform.OS == "web" && props.getCurrentDeviceWidthAction > 750
              ? require("../assets/image/weclickd-logo.png")
              : Platform.OS == "web"
                ? require("../assets/image/weclickd-logo.png")
                : require("../assets/image/weclickd-logo-only-icon.png")
          }
          style={
            Platform.OS == "web" && props.getCurrentDeviceWidthAction > 750
              ? {
                height: 30,
                width: Platform.OS == "web" ? 90 : 30,
                padding: 0,
                margin: 0,
                marginVertical: 10
              }
              : {
                height: 30,
                width: Platform.OS == "web" ? 90 : 30,
                padding: 0,
                margin: 0,
                marginVertical: 10
              }
          }
        />
      </View>
      <View
        style={{
          borderRadius: 4,
          overflow: "visible",
          width: "100%",
          backgroundColor: "#fff"
        }}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            backgroundColor: "#fff",
            borderBottomLeftRadius: 6,
            borderBottomRightRadius: 6
          }}
        >
          <ProgressBar progress={loading} value={value} />
          <View style={{ flex: 1, margin: 20, marginVertical: 40 }}>
            {/* <Text
              style={{
                color: "#000",
                justifyContent: "center",
                alignSelf: "center",
                marginBottom: 15,
                fontWeight: "bold",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 14
              }}
            >
              Share an article worthy a discussion.
            </Text> */}
            <TextInput
              onChangeText={value => {
                setLink(value);
                //checkLink(value);
              }}
              onKeyPress={checkEnter}
              value={link}
              autoFocus={true}
              placeholder="Share an article worthy a discussion."
              placeholderTextColor="#53555A"              
              underlineColorAndroid="transparent"
              style={{
                width: "100%",
                alignSelf: "center",
                height: 40,
                paddingLeft: 10,
                borderWidth: 1,
                borderRadius: 6,
                fontFamily: ConstantFontFamily.defaultFont,
                textAlign: link ? "left" : "center",
                outline:'none'
                }}
              ref={e => {
                textInput = e;
              }}
            />
          </View>
          <View
            style={{
              width: "100%",
              flexDirection: "row",
              justifyContent: "center"
              // marginBottom: 40
            }}
          >
            {loading == false ? (
              <Button
                title="Share"
                titleStyle={ButtonStyle.wtitleStyle}
                buttonStyle={[ButtonStyle.gbackgroundStyle, {borderRadius:10}]}
                containerStyle={[ButtonStyle.containerStyle, {marginVertical:0}]}
                disabled={link == "" ? true : false}
                onPress={() => getContent()}
              />
            ) : (
                <Text
                  style={{
                    color: "#000",
                    justifyContent: "center",
                    alignSelf: "center",
                    marginBottom: 20,
                    fontWeight: "bold",
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    fontSize: 14
                  }}
                >
                  Analyzing Link...
                </Text>
              )}
          </View>
          <Text
            style={{
              color: "red",
              justifyContent: "center",
              alignSelf: "center",
              marginBottom: 15,
              fontWeight: "bold",
              fontFamily: ConstantFontFamily.MontserratBoldFont,
              fontSize: 14
            }}
          >
            {Error}
          </Text>

          <View
            style={{
              flexDirection: "row",
              width: "100%",
              alignItems: "center",
              justifyContent: "center",
              height: 20,
              marginVertical: 10
            }}
          >
            <View
              style={{
                borderWidth: 1,
                borderColor: "#e1e1e1",
                width: "43%",
                justifyContent: "flex-start",
                height: 1,
                borderRadius: 6,
                backgroundColor: "#e1e1e1"
              }}
            ></View>
            <Text
              style={{
                fontSize: 16,
                color: "#49525D",
                textAlign: "center",
                marginHorizontal: "4%",
                marginVertical: "10%",
                fontFamily: ConstantFontFamily.MontserratBoldFont
              }}
            >
              or
            </Text>
            <View
              style={{
                borderWidth: 1,
                borderColor: "#e1e1e1",
                width: "43%",
                justifyContent: "flex-end",
                height: 1,
                borderRadius: 6,
                backgroundColor: "#e1e1e1"
              }}
            ></View>
          </View>
          <Text
            style={{
              color: "#000",
              justifyContent: "center",
              alignSelf: "center",              
              marginTop: 40,
              fontWeight: "bold",
              fontFamily: ConstantFontFamily.MontserratBoldFont,
              fontSize: 14
            }}
          >
            Create your own discussion prompt.
          </Text>
          <Button
            title="Create"
            titleStyle={ButtonStyle.titleStyle}
            buttonStyle={[ButtonStyle.backgroundStyle, {borderRadius:10}]}
            containerStyle={[ButtonStyle.containerStyle, {marginTop:40, marginBottom:50}]}
            onPress={() => navigatePost()}
          />
        </ScrollView>
      </View>
    </View>
  );
};

const mapStateToProps = state => ({
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension")
});

const mapDispatchToProps = dispatch => ({
  postlink: payload => dispatch(postLink(payload)),
  setShareLinkModalStatus: payload =>
    dispatch(setSHARELINKMODALACTION(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  LinkAddAlertCard
);