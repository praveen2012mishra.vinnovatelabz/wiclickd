import { List } from "immutable";
import React from "react";
import { graphql } from "react-apollo";
import {
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  ActivityIndicator,
  Dimensions
} from "react-native";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import applloClient from "../client";
import ConstantFontFamily from "../constants/FontFamily";
import { UserFollowMutation } from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { UserFollowVariables } from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import NavigationService from "../library/NavigationService";
import {
  FeedFollowMutation,
  FeedUnFollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import {
  FeedFollowVariables,
  FeedUnFollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { getFeedProfileDetails } from "../actionCreator/FeedProfileAction";
import { Button, Icon } from "react-native-elements";
import ExternalFeedStar from "./ExternalFeedStar";
import { render } from "react-dom";
import { SearchFeedMutation } from "../graphqlSchema/graphqlMutation/SearchMutation";
import { SearchFeedVariables } from "../graphqlSchema/graphqlVariables/SearchVariables";
import { Trending_ExternalFeeds_Mutation } from "../graphqlSchema/graphqlMutation/TrendingMutation";

class TrendingExternalFeed extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onEndReachedCalledDuringMomentum = true;
    this.viewabilityConfig = {
      viewAreaCoveragePercentThreshold: 50
    };
    this.state = {
      searchedFollowText: this.props.searchedFollowText,
      listTrending_feeds: [],
      page: 1,
      loading: true,
      loadingMore: false,
      refreshing: false,
      pageInfo: null,
      error: null,
      apiCall: true
    };
  }

  goToProfile = id => {
    this.props.setFeedDetails({
      id: id,
      type: "feed"
    });
    this.props.setPostCommentReset({
      payload: [],
      postId: "",
      title: "",
      loading: true
    });
    this.props.leftPanelModalFunc(false)
  };

  componentDidUpdate = async prevProps => {
    if (prevProps.searchedFollowText != this.props.searchedFollowText) {
      await this.feedSearch(this.props.searchedFollowText);
    } else if (prevProps.searchedFollowText == "") {
      //this.setState({ listTrending_feeds: this.props.listTrending_feeds });
    }
  };

  feedSearch = search => {
    this.setState({ search });
    let tempData = [];
    let tempArray = [];

    if (search.length > 0) {
      SearchFeedVariables.variables.prefix = search;
      applloClient
        .query({
          query: SearchFeedMutation,
          ...SearchFeedVariables,
          fetchPolicy: "no-cache"
        })
        .then(res => {
          tempArray = res.data.search.feeds;
          for (let i = 0; i < tempArray.length; i++) {
            tempData.push({ node: tempArray[i] });
          }
          this.setState({
            listTrending_feeds: tempData
          });
        });
    } else {
      this.setState({pageInfo : null}, ()=>{
        this.getTrendingFeedList();
      })
      // this.setState({ listTrending_feeds: this.props.listTrending_feeds });
    }
  };

  componentDidMount = () => {
    this.getTrendingFeedList();
  };

  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this.getTrendingFeedList();
      }
    );
  };

  _handleLoadMore = distanceFromEnd => {
    this.setState(
      (prevState, nextProps) => ({
        //  loadingMore: true
      }),
      () => {
        if (
          0 <= distanceFromEnd &&
          distanceFromEnd <= 5 &&
          this.state.apiCall == true
        ) {
          this.setState({
            apiCall: false
          });
          this.getTrendingFeedList();
        }
      }
    );
  };

  _renderFooter = () => {
    if (!this.state.loadingMore) return null;
    return (
      <View>
        <ActivityIndicator animating size="large" color="#000" />
      </View>
    );
  };

  getTrendingFeedList = () => {
    let __self = this;
    const { page, pageInfo } = this.state;

    if (pageInfo == null) {
      this.setState({
        loadingMore: true
      });
      applloClient
        .query({
          query: Trending_ExternalFeeds_Mutation,
          variables: {
            first: 20,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "TRENDING"
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          __self.setState({
            loading: false,
            listTrending_feeds: this.props.getUserFollowFeedList
              .toJS()
              .concat(this.getFeedList(res.data.external_feed_list.edges)),
            pageInfo: res.data.external_feed_list.pageInfo,
            page: page + 1,
            apiCall: true,
            loadingMore: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else if (pageInfo != null && pageInfo.hasNextPage == true) {
      this.setState({
        loadingMore: true
      });
      applloClient
        .query({
          query: Trending_ExternalFeeds_Mutation,
          variables: {
            first: 20,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "TRENDING"
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          __self.setState({
            loading: false,
            listTrending_feeds: __self.state.listTrending_feeds.concat(
              this.getFeedList(res.data.external_feed_list.edges)
            ),
            pageInfo: res.data.external_feed_list.pageInfo,
            apiCall: true,
            loadingMore: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    }
  };

  onViewableItemsChanged = ({ viewableItems, changed }) => {
    let perLoadDataCount = 20;
    let halfOfLoadDataCount = perLoadDataCount / 2;
    let lastAddArr =
      this.state.listTrending_feeds.length > 0 &&
      this.state.listTrending_feeds.slice(-perLoadDataCount);
    try {
      if (lastAddArr[halfOfLoadDataCount] && viewableItems.length > 0) {
        viewableItems.find(item => {
          let feedId = item.item.node ? item.item.node.id : item.item.feed.id;
          let LastfeedId = lastAddArr[halfOfLoadDataCount].node
            ? lastAddArr[halfOfLoadDataCount].node.id
            : lastAddArr[halfOfLoadDataCount].feed.id;
          if (feedId === LastfeedId) {
            this._handleLoadMore(0);
          }
        });
      }
    } catch (e) {
      console.log(e, "lastAddArr", lastAddArr[halfOfLoadDataCount]);
    }
  };

  getFeedList = item => {
    let newArray = [];
    item.forEach(obj => {
      let index = this.props.getUserFollowFeedList.findIndex(
        i => i.getIn(["feed", "name"]) == obj.node.name
      );
      if (index == -1) {
        newArray.push({ ...obj });
      }
    });
    return newArray;
  };

  _renderItem = item => {
    var row = item.item.node ? item.item.node : item.item.feed;
    // let index = this.props.getUserFollowFeedList.findIndex(
    //   i => i.getIn(["feed", "name"]) == row.name
    // );
    // if (index == -1){
    return (
      <View
        key={item.index}
        style={{
          overflow: "hidden",
          padding: 2,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          width: "100%"
        }}
      >
        <Hoverable>
          {isHovered => (
            <TouchableOpacity
              onPress={() =>
                this.goToProfile(row.id.replace("ExternalFeed:", ""))
              }
              style={{
                flexDirection: "row",
                borderRadius: 4,
                overflow: "hidden",
                width: "85%"
              }}
            >
              <View
                style={{
                  //width: "20%",
                  margin: 0,
                  flexDirection: "row",
                  backgroundColor: "rgba(255,255,255,0.5)"
                }}
              >
                {row.icon_url ? (
                  <Image
                    source={{
                      uri: row.icon_url
                    }}
                    style={{
                      width: 30,
                      height: 30,
                      borderRadius: 18
                    }}
                  />
                ) : (
                  <Image
                    source={{
                      uri:
                        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAhFBMVEX/pQD/////ngD/oQD/owD/oAD/+fT/nQD///3/7NP/pgD/4r7//vr/+vH/t1D/vF3/+O7/79r/zo//5MP/xnr/16P/2Kj/9OX/rSv/xXf/uVb/zIn/6c3/267/qRP/05v/s0L/wGn/sDb/wW3/tkv/z5T/yYH/qyD/sDf/37b/x4T/ryzcdsqXAAAM+ElEQVR4nN1daXfqOgx0bKUQCIQd2rKWbtz+//93E6ALEI/kLJAw57xP9yXNYFmSpbGtPA6dYDHYdr+WLVUltJZf3e1gEXTY71fwX8PhaKa1MUR0a0oXiL/JGK0/RsMwM8P2RvumetROQcbXUTsLw85A6aqz+wZpGljN1cKwN9KVH72/IKMfew4Mm8/a3PqbnWH0c1PKsE3145fAUNp8vGQYRrWZf+cgHV361QuGgarnAB5gVMAxHDfqOoAHUGOMGY70rT8xN/QIMYz8W39fAfAjO8OnOk/BX5gnG8PoPgjGFKN0hqN7MNED/FEaw3H9ncwv9PiSYdC49VcVikZwzjBU9Y6D5yAVnjG8Gy/zjR9vc2TYvqdJeIBu/2XYrGCVIi+Imn8YPt+bjSYwz78Me/dnowl074fh4z0OYTyIo2+GnfscwngQO0eGg/scwngQB0eGd+hIDyB1YNi2Ztz6CN83ppqFbwZJTIwZRrYP19NObzrtB/N5e7J6e4zWrYRtQvWqn5kDtEkYhlY/ox+8c3T6w8lzNDN7otf81ozQYcxwaDfSS4ZHNKfD1fbQs7nm97rDH8YMR1ZPamd45NlfvL/4lS7/xyFReTPrB3IM9wiD1ZOpLEuaeQqEexHDPfrjqrLUDyoogmFisvPBrIIkdaAW9oTGiWGC6aSr/WqRNAsFUjZnhjHCXeRXiaQZqK39c7IwTEguXhuVMVfaqm7hDGN0xrOK9Fipq77KYBijP6qEtdJMLe3/mothjMVLBXqtSwWEQHkZxgO5vXkAgUKn/AwT0QpVwVgtKIJhjMmyAsaajoIYet5uVlGOhTH0vHY1ORbI0POGVeRYKMPYVpeV8zkFM4x9jqlGovODwhlWTjBXPEPP61VKU1YGQ8+bz6qjiCiHoeeN/aqYalkMvU5VTLU0hnF0bFViGHWq5rYYNB+rIIKkaLPZbB/f38aL9rzfKZjvfFmBYSTab2qI4Wutzcd6O9gFFsF4BmyrMIwnSOjGVP314yQoZIoOqyooP+xV2U76uSmG3Qo32BOafnd1Iat2xLgiccMGMpqiSa6Z2a+Cw8GIWc4GOYayGVXYUr8Rk2z9y06y6pZ6QExyOcjqeoKaKANiki8LvBfQhs5L5SfjEeT722zWuqnBZDwi9juLLBTf6kMxHkgaZEh52rXwN0fEM3I0dabYr4m/OcLojTPH3rJWFJVpOHMMa+NSjzB665rQvdaMYrI31zFA1m/3nDHnOx4ZbGtHkfyPuRPFxxoFxiNIR07T8b1+FJXxnUy1jhSVP3NZd9SSIumBA8XH2rmbBP6HwzDWz6MmIP0mp1jT3YL+Wu5Ur5PdmMNGi2SnRTE5MRl0Vs4pXq6Qhptde7cYr97+bbqfh60W+YnqRynD8AorjdPu2jRYDDb7nRa5/rJ5kVpqr/z1Ylr/MOzvntc6jx6PjDSL65ceFu0d0v44oswsqbESUix9dzLuAfdX60ZGCZDeCCmWXZ5iu9ydXWQykTSfwmXjptyYIenjh+0oi7lSS1jhKDdmCJUKncmnu9KJjKxy3CnVoTrsCho572IjvRO9GuzpyQ8XtUnorgRuTERvXpVI0VFPM1w7chRm4iUm4c6KoaDrpq7Qz5K3NstL3zJoooKu0zjqd8lLy8ttMqm+gk8XjjKKpR1/lFHX1l46LNBlFLslTcXMyj0XeaVoLoYlRcUce9c2clMVedRhOadY5VFfOujytCQubkux03z60mfxMDYEpY1yQkZOBW3/Q/q7a0GOOi/DTnNrhN+FCQD5gpVGGccE5VdBz4XySmrx68Vmq3g7LUDnLZVX0if/rmHxcb8QJfubzOGYDf+q4lPwYrT6c9nKUfPlqU7hzYyCdiP0ZqLfvsEXGcdFUyxsv8WT5MvI8KVi+zksGRlmExqmQNTypBf2PfOCnY1ZTSaL3bA/zc9UtP4xI/Y9RTub/U6LZKsFzaLnxZy/LcMOkS5Ps+lbeecfHm7LaG3GQdadM3MJRX4qlnyGZcxTm+44m9i5L4gatObe0rxC4zSRrb+7iYAOmAoo+uxicXKVDv+epPtISihq9rXXkmjGJD/Grj5WQJE+uJfsrie2IeM/OipI+7y78Vndzcc1dbam8eQmzReEbNZOr3yqM+lXJ47859GMe0fRuRv7Rbrr4nT47IaV+V3/aG7SW4d055119z4X9689iCqRFknFB57gFg0TMW+4ojv9hf8hn47sEGguobjJtgU6v9/Fjh6nbmCD4nUSmwuYlnQY2ZjBOpsbba+Rq2QHzBgQMenSzQ7o9tdCp8ppKw3TkbrdOflEMktl22WaiRibm22RIi2T5XNtCMOoNEsVoTAQKkjfGTvVTFJ/DYGtDX5XRJFZInBF8MUtte5mJlk4cgqLBh7E5k3V/PQhcalMUYkbxNteykFKInVmci9mJt7S16ikISigyKQ2XIH4qmv9S5ASGCqjj9X4FeMSzfRw8hDehEFL3t0w7TKDS4u9ku5RI6P9WXf7/m9/3wXYmsBXIzxvBSkS4adfSzBT8k3092ihzhAcNW8EcRE7Gx+LbIsPiaSf2pdti+lAWdZ7Pp/d4JILYwYPBTMk+xkCC4tWT5CjfsJBZIQ2T4WaqV6jitoqvdTLS4FwxDBb+HCRZkqc+Kz3mvbXiNiYAU69j+FDh/xQXNA3S758P0hz3ny7DN+d6eOTYArzpuaV5eclip6Uv8e3IeBXMr39SUFBn61fHhGkUWSnIs4vcXI6LcZMTy8Qdv1YanFPrdEgGmwDheSmEsnZN9opk4pVWEC9Gi3hs0W09YlcdENpZULWTmFig58uQmDDVthPkeI3+Ao2GghcVyxgpS9Q8ZwgrZDJtW1CxJAxUxxOBSDlqp9JWy74TNy3X4ymuJ5w7kUiE3HTkDKrmOQLF6WwN827Y4gxkVSkJYtcbx4VP5kFRs4xNLItgqdIueGbmKUi9DW4wJ9zIposqsS0vhATMeBttvhnXuUaxOSSS3ekpVLEZLZopUcwacwXEX35OR1/kdbFZgZxh+IaLNfYLxmVoJFNOptmpoST2xAxxL9OnhaNS0b6F6k3mzJtCOuFtirRKqMnYTBlYES7Hy+R6jeYhiAyUzz+4AJHnmGWWJEgdf8O9ssdtNT30ZN5Yr6fRSebIHXFx/xeqOgGE4ZmDoa8mtWC1EnF5CZvwNgMzB1zaMA4tYAV6a09/IOhYgbOa8EtnCzDrHtm/qUyNP/gQyjmwxVmjqwm8ximqxCYig2KF7Bsar92m2foftrxARa7weUCtNKDMT9HwU2yPTcVlnwfx1c4EZEfzrEFI2NaavVujJkihrCWkt2Z4mwJwDYxsDcFdVPcHMjegsLLFjusWQY+UxjVPg16MN11yyhmY2jNFPFSH6Wm0K3n6F5kTGrsft+gyh1KMKHTa2cPF9kmIvBt8ENR2RR29HPk3hI5xSXAT4onImizwJJiHjltJjMF1S9c+AGqWIKZaQ6GWUpRKMXAERGIt3G4yHOeRIbEDeqToZALrNZx7o0FHRjuIRFPe+hqYN6GvDBK2pm7x52ba0xHF69lkX3DlRySmrbUEjN07VxMGEUl6gaiegT0eWj3xVJ9MTbM9Y1Owa1ksNWndDx+GCLzBmtg+lJsZ0O7NNi4LVdYPQKKu3CdA9I26iq+yuEwFfljxmGqC35tOIFBjkFbJdhBJF4JC06tgKeJgEADs6E5yKIGSlAyJuEoslsnFRNgwTII6oVB/8kslEjyLjk07uFTsoiBPhHUTGEFBHDQgYLNyd//kfWoc9mWQFhHBz4Rtj1AlqEflLDKYZbQUpsj4Ql7cBkEym0waNljVLw6UtLuFOnIPoUW4mtvoU9EiekmE0MzihmKC6oxx1SnGk5a8mPLYV0Q1DEgQ/sC0R/GDB3axKRnqzNPEbY3TgddQ4YoriGGdl+imzFDJjM/+0PJ9birdjDtPfT688n7p+vR+mYVPlixA1YaNe3PWa00+V2U+yEM+6ur9f5GjwznzRttB9S3gefsQ9jeM7zV1u/yQQm75L+bbf0uG/v6lYITtebY10wShjfejlkaDv0cheNJrXFoBewZln3O221wrJgcGDbv0J0SNf8wvMHBRKXj+xDGI8O63uRmx89Gn2+GIahz1RGkwjOGzB6y2qHxswz6YVjeJRK3wJ/dq78MvVEt741Mhf9Hu/GH4f14m5PthH8ZisqBNcDpdsIThjW8FTsF/mlv5JRhPW/iPcX5uXFnDL2x241NlQM1zsv/5wy9QNV5Mhp1UQ68YOiFkesFapUB6eiy8XPJME7DxfXdasFQWpMxjaHXfHa/0PDmMPo5Vc2QytDzOqM818VeH2T0yCLuszCMOQ5UbeYjaRpYhTpWhjHam7zXG18BSX06Qk1+xDD2q8P3ma+5I6FuAzrcZvAxGuLN2Jhhgodg8bbtzpaMtOjKaC2/utvBIuBPFPsPHJSoM14DLo8AAAAASUVORK5CYII="
                    }}
                    style={{
                      width: 30,
                      height: 30,
                      borderRadius: 18
                    }}
                  />
                )}
              </View>
              <View
                style={{
                  //width: "80%",
                  alignSelf: "center",
                  //marginLeft: 7
                  marginLeft: '5%'
                }}
              >
                <Text
                  style={{
                    textAlign: "left",
                    fontSize: 14,
                    fontWeight: "bold",
                    fontFamily: ConstantFontFamily.defaultFont,
                    textDecorationLine: isHovered == true ? "underline" : "none"
                  }}
                >
                  
                  {row.name}{" "}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        </Hoverable>
        <ExternalFeedStar
          FeedName={row.name}
          FeedId={row.id}
          ContainerStyle={{
            flex: 1,
            justifyContent: "flex-end",
            width: "20%",
            paddingLeft: 15
          }}
          ImageStyle={{
            height: 20,
            width: 20,
            alignSelf: "flex-end"
          }}
        />
      </View>
    );
    // }
  };

  render() {
    return (
      <View>
        {/* <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            backgroundColor: "#fff",
            paddingTop: 0
          }}
        >
          {this.state.listTrending_feeds.map((item, indexx) => {
            if (indexx < 10) {
              return (
                <View
                  key={indexx}
                  style={{
                    overflow: "hidden",
                    padding: 2,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    width: "100%"
                  }}
                >
                  <Hoverable>
                    {isHovered => (
                      <TouchableOpacity
                        onPress={() =>
                          this.goToProfile(
                            item.node.id.replace("ExternalFeed:", "")
                          )
                        }
                        style={{
                          flexDirection: "row",
                          borderRadius: 4,
                          overflow: "hidden",
                          width: "80%"
                        }}
                      >
                        <View
                          style={{
                            width: "20%",
                            margin: 0,
                            flexDirection: "row",
                            backgroundColor: "rgba(255,255,255,0.5)"
                          }}
                        >
                          {item.node.icon_url ? (
                            <Image
                              source={{
                                uri: item.node.icon_url
                              }}
                              style={{
                                width: 36,
                                height: 36,
                                borderRadius: 18
                              }}
                            />
                          ) : (
                            <Image
                              source={{
                                uri:
                                  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAhFBMVEX/pQD/////ngD/oQD/owD/oAD/+fT/nQD///3/7NP/pgD/4r7//vr/+vH/t1D/vF3/+O7/79r/zo//5MP/xnr/16P/2Kj/9OX/rSv/xXf/uVb/zIn/6c3/267/qRP/05v/s0L/wGn/sDb/wW3/tkv/z5T/yYH/qyD/sDf/37b/x4T/ryzcdsqXAAAM+ElEQVR4nN1daXfqOgx0bKUQCIQd2rKWbtz+//93E6ALEI/kLJAw57xP9yXNYFmSpbGtPA6dYDHYdr+WLVUltJZf3e1gEXTY71fwX8PhaKa1MUR0a0oXiL/JGK0/RsMwM8P2RvumetROQcbXUTsLw85A6aqz+wZpGljN1cKwN9KVH72/IKMfew4Mm8/a3PqbnWH0c1PKsE3145fAUNp8vGQYRrWZf+cgHV361QuGgarnAB5gVMAxHDfqOoAHUGOMGY70rT8xN/QIMYz8W39fAfAjO8OnOk/BX5gnG8PoPgjGFKN0hqN7MNED/FEaw3H9ncwv9PiSYdC49VcVikZwzjBU9Y6D5yAVnjG8Gy/zjR9vc2TYvqdJeIBu/2XYrGCVIi+Imn8YPt+bjSYwz78Me/dnowl074fh4z0OYTyIo2+GnfscwngQO0eGg/scwngQB0eGd+hIDyB1YNi2Ztz6CN83ppqFbwZJTIwZRrYP19NObzrtB/N5e7J6e4zWrYRtQvWqn5kDtEkYhlY/ox+8c3T6w8lzNDN7otf81ozQYcxwaDfSS4ZHNKfD1fbQs7nm97rDH8YMR1ZPamd45NlfvL/4lS7/xyFReTPrB3IM9wiD1ZOpLEuaeQqEexHDPfrjqrLUDyoogmFisvPBrIIkdaAW9oTGiWGC6aSr/WqRNAsFUjZnhjHCXeRXiaQZqK39c7IwTEguXhuVMVfaqm7hDGN0xrOK9Fipq77KYBijP6qEtdJMLe3/mothjMVLBXqtSwWEQHkZxgO5vXkAgUKn/AwT0QpVwVgtKIJhjMmyAsaajoIYet5uVlGOhTH0vHY1ORbI0POGVeRYKMPYVpeV8zkFM4x9jqlGovODwhlWTjBXPEPP61VKU1YGQ8+bz6qjiCiHoeeN/aqYalkMvU5VTLU0hnF0bFViGHWq5rYYNB+rIIKkaLPZbB/f38aL9rzfKZjvfFmBYSTab2qI4Wutzcd6O9gFFsF4BmyrMIwnSOjGVP314yQoZIoOqyooP+xV2U76uSmG3Qo32BOafnd1Iat2xLgiccMGMpqiSa6Z2a+Cw8GIWc4GOYayGVXYUr8Rk2z9y06y6pZ6QExyOcjqeoKaKANiki8LvBfQhs5L5SfjEeT722zWuqnBZDwi9juLLBTf6kMxHkgaZEh52rXwN0fEM3I0dabYr4m/OcLojTPH3rJWFJVpOHMMa+NSjzB665rQvdaMYrI31zFA1m/3nDHnOx4ZbGtHkfyPuRPFxxoFxiNIR07T8b1+FJXxnUy1jhSVP3NZd9SSIumBA8XH2rmbBP6HwzDWz6MmIP0mp1jT3YL+Wu5Ur5PdmMNGi2SnRTE5MRl0Vs4pXq6Qhptde7cYr97+bbqfh60W+YnqRynD8AorjdPu2jRYDDb7nRa5/rJ5kVpqr/z1Ylr/MOzvntc6jx6PjDSL65ceFu0d0v44oswsqbESUix9dzLuAfdX60ZGCZDeCCmWXZ5iu9ydXWQykTSfwmXjptyYIenjh+0oi7lSS1jhKDdmCJUKncmnu9KJjKxy3CnVoTrsCho572IjvRO9GuzpyQ8XtUnorgRuTERvXpVI0VFPM1w7chRm4iUm4c6KoaDrpq7Qz5K3NstL3zJoooKu0zjqd8lLy8ttMqm+gk8XjjKKpR1/lFHX1l46LNBlFLslTcXMyj0XeaVoLoYlRcUce9c2clMVedRhOadY5VFfOujytCQubkux03z60mfxMDYEpY1yQkZOBW3/Q/q7a0GOOi/DTnNrhN+FCQD5gpVGGccE5VdBz4XySmrx68Vmq3g7LUDnLZVX0if/rmHxcb8QJfubzOGYDf+q4lPwYrT6c9nKUfPlqU7hzYyCdiP0ZqLfvsEXGcdFUyxsv8WT5MvI8KVi+zksGRlmExqmQNTypBf2PfOCnY1ZTSaL3bA/zc9UtP4xI/Y9RTub/U6LZKsFzaLnxZy/LcMOkS5Ps+lbeecfHm7LaG3GQdadM3MJRX4qlnyGZcxTm+44m9i5L4gatObe0rxC4zSRrb+7iYAOmAoo+uxicXKVDv+epPtISihq9rXXkmjGJD/Grj5WQJE+uJfsrie2IeM/OipI+7y78Vndzcc1dbam8eQmzReEbNZOr3yqM+lXJ47859GMe0fRuRv7Rbrr4nT47IaV+V3/aG7SW4d055119z4X9689iCqRFknFB57gFg0TMW+4ojv9hf8hn47sEGguobjJtgU6v9/Fjh6nbmCD4nUSmwuYlnQY2ZjBOpsbba+Rq2QHzBgQMenSzQ7o9tdCp8ppKw3TkbrdOflEMktl22WaiRibm22RIi2T5XNtCMOoNEsVoTAQKkjfGTvVTFJ/DYGtDX5XRJFZInBF8MUtte5mJlk4cgqLBh7E5k3V/PQhcalMUYkbxNteykFKInVmci9mJt7S16ikISigyKQ2XIH4qmv9S5ASGCqjj9X4FeMSzfRw8hDehEFL3t0w7TKDS4u9ku5RI6P9WXf7/m9/3wXYmsBXIzxvBSkS4adfSzBT8k3092ihzhAcNW8EcRE7Gx+LbIsPiaSf2pdti+lAWdZ7Pp/d4JILYwYPBTMk+xkCC4tWT5CjfsJBZIQ2T4WaqV6jitoqvdTLS4FwxDBb+HCRZkqc+Kz3mvbXiNiYAU69j+FDh/xQXNA3S758P0hz3ny7DN+d6eOTYArzpuaV5eclip6Uv8e3IeBXMr39SUFBn61fHhGkUWSnIs4vcXI6LcZMTy8Qdv1YanFPrdEgGmwDheSmEsnZN9opk4pVWEC9Gi3hs0W09YlcdENpZULWTmFig58uQmDDVthPkeI3+Ao2GghcVyxgpS9Q8ZwgrZDJtW1CxJAxUxxOBSDlqp9JWy74TNy3X4ymuJ5w7kUiE3HTkDKrmOQLF6WwN827Y4gxkVSkJYtcbx4VP5kFRs4xNLItgqdIueGbmKUi9DW4wJ9zIposqsS0vhATMeBttvhnXuUaxOSSS3ekpVLEZLZopUcwacwXEX35OR1/kdbFZgZxh+IaLNfYLxmVoJFNOptmpoST2xAxxL9OnhaNS0b6F6k3mzJtCOuFtirRKqMnYTBlYES7Hy+R6jeYhiAyUzz+4AJHnmGWWJEgdf8O9ssdtNT30ZN5Yr6fRSebIHXFx/xeqOgGE4ZmDoa8mtWC1EnF5CZvwNgMzB1zaMA4tYAV6a09/IOhYgbOa8EtnCzDrHtm/qUyNP/gQyjmwxVmjqwm8ximqxCYig2KF7Bsar92m2foftrxARa7weUCtNKDMT9HwU2yPTcVlnwfx1c4EZEfzrEFI2NaavVujJkihrCWkt2Z4mwJwDYxsDcFdVPcHMjegsLLFjusWQY+UxjVPg16MN11yyhmY2jNFPFSH6Wm0K3n6F5kTGrsft+gyh1KMKHTa2cPF9kmIvBt8ENR2RR29HPk3hI5xSXAT4onImizwJJiHjltJjMF1S9c+AGqWIKZaQ6GWUpRKMXAERGIt3G4yHOeRIbEDeqToZALrNZx7o0FHRjuIRFPe+hqYN6GvDBK2pm7x52ba0xHF69lkX3DlRySmrbUEjN07VxMGEUl6gaiegT0eWj3xVJ9MTbM9Y1Owa1ksNWndDx+GCLzBmtg+lJsZ0O7NNi4LVdYPQKKu3CdA9I26iq+yuEwFfljxmGqC35tOIFBjkFbJdhBJF4JC06tgKeJgEADs6E5yKIGSlAyJuEoslsnFRNgwTII6oVB/8kslEjyLjk07uFTsoiBPhHUTGEFBHDQgYLNyd//kfWoc9mWQFhHBz4Rtj1AlqEflLDKYZbQUpsj4Ql7cBkEym0waNljVLw6UtLuFOnIPoUW4mtvoU9EiekmE0MzihmKC6oxx1SnGk5a8mPLYV0Q1DEgQ/sC0R/GDB3axKRnqzNPEbY3TgddQ4YoriGGdl+imzFDJjM/+0PJ9birdjDtPfT688n7p+vR+mYVPlixA1YaNe3PWa00+V2U+yEM+6ur9f5GjwznzRttB9S3gefsQ9jeM7zV1u/yQQm75L+bbf0uG/v6lYITtebY10wShjfejlkaDv0cheNJrXFoBewZln3O221wrJgcGDbv0J0SNf8wvMHBRKXj+xDGI8O63uRmx89Gn2+GIahz1RGkwjOGzB6y2qHxswz6YVjeJRK3wJ/dq78MvVEt741Mhf9Hu/GH4f14m5PthH8ZisqBNcDpdsIThjW8FTsF/mlv5JRhPW/iPcX5uXFnDL2x241NlQM1zsv/5wy9QNV5Mhp1UQ68YOiFkesFapUB6eiy8XPJME7DxfXdasFQWpMxjaHXfHa/0PDmMPo5Vc2QytDzOqM818VeH2T0yCLuszCMOQ5UbeYjaRpYhTpWhjHam7zXG18BSX06Qk1+xDD2q8P3ma+5I6FuAzrcZvAxGuLN2Jhhgodg8bbtzpaMtOjKaC2/utvBIuBPFPsPHJSoM14DLo8AAAAASUVORK5CYII="
                              }}
                              style={{
                                width: 36,
                                height: 36,
                                borderRadius: 18
                              }}
                            />
                          )}
                        </View>
                        <View
                          style={{
                            width: "80%",
                            alignSelf: "center",
                            marginLeft: 7
                          }}
                        >
                          <Text
                            style={{
                              textAlign: "left",
                              fontSize: 14,
                              fontWeight: "bold",
                              fontFamily: ConstantFontFamily.defaultFont,
                              textDecorationLine:
                                isHovered == true ? "underline" : "none"
                            }}
                          >
                            {" "}
                            {item.node.name}{" "}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    )}
                  </Hoverable>
                  <ExternalFeedStar
                    FeedName={item.node.name}
                    FeedId={item.node.id}
                    ContainerStyle={{
                      flex: 1,
                      justifyContent: "flex-end",
                      width: "20%",
                      paddingLeft: 15
                    }}
                    ImageStyle={{
                      height: 20,
                      width: 20,
                      alignSelf: "flex-end"
                    }}
                  />
                </View>
              );
            }
          })}
        </ScrollView> */}
        <FlatList
          extraData={this.state}
          contentContainerStyle={{
            flexDirection: "column",
            height: Dimensions.get("window").height,
            width: "100%"
          }}
          data={this.state.listTrending_feeds}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItem}
          showsVerticalScrollIndicator={false}
          onRefresh={this._handleRefresh}
          refreshing={this.state.refreshing}
          // onEndReached={({ distanceFromEnd }) => {
          //   this._handleLoadMore(distanceFromEnd);
          // }}
          onEndReachedThreshold={0.2}
          initialNumToRender={10}
          ListFooterComponent={this._renderFooter}
          onViewableItemsChanged={this.onViewableItemsChanged}
          viewabilityConfig={this.viewabilityConfig}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  listTrending_feeds: !state.TrendingExternalFeedsReducer.getIn([
    "Trending_ExternalFeeds"
  ])
    ? List()
    : state.TrendingExternalFeedsReducer.getIn(["Trending_ExternalFeeds"]),
  getUserFollowUserList: state.LoginUserDetailsReducer.get("userFollowUserList")
    ? state.LoginUserDetailsReducer.get("userFollowUserList")
    : List(),
  getUserFollowFeedList: state.LoginUserDetailsReducer.get("userFollowFeedList")
    ? state.LoginUserDetailsReducer.get("userFollowFeedList")
    : List(),
  loginStatus: state.UserReducer.get("loginStatus")
});

const mapDispatchToProps = dispatch => ({
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setFeedDetails: payload => dispatch(getFeedProfileDetails(payload)),
  setPostCommentReset: payload =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload }),
  leftPanelModalFunc : payload => dispatch ({type : 'LEFT_PANEL_OPEN' , payload})
});

const TrendingExternalFeedWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(TrendingExternalFeed);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  TrendingExternalFeedWrapper
);
