import { List } from "immutable";
import moment from "moment";
import React, { Component } from "react";
import {
  Image,
  Text,
  TouchableOpacity,
  View,
  Platform,
  Dimensions,
  TextInput,
  TouchableHighlight
} from "react-native";
import { Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setFEEDREPORTMODALACTION } from "../actionCreator/FeedReportModalAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import ChangeIcon from "../components/ChangeIcon";
import ConstantFontFamily from "../constants/FontFamily";
import EditCommentCard from "./EditCommentCard";
import { getLocalStorage } from "../library/Helper";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger
} from "react-native-popup-menu";
import Overlay from "react-native-modal-overlay";
import Modal from "modal-enhanced-react-native-web";
import FeedReportModal from "../components/FeedReportModal";
import { DeleteContentMutation } from "../graphqlSchema/graphqlMutation/FeedMutation";
import applloClient from "../client";
import getEnvVars from "../environment";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";

let commentID = [];
let commentsWithArrowKeys = {};
let activeCommentId = null;
let nextSelectId = null;
const apiUrl = getEnvVars();

class PostDetailsCommentSwaper extends Component {
  state = {
    modalVisible: false,
    outSideClick: false,
    showBug: false,
    parent_content_id: "",
    height: 0,
    pressBug: false,
    nameWidth: 0,
    showEditButton: true,
    editModalVisible: false,
    blockUi: false,
    commentID: [],
    activeComment: -1,
    activeIndex: 0,
    pressComment: false,
    showMenu: Platform.OS == "web" ? false : true,
    MenuHover: false,
    showMore: false,
    commentHeight: 10,
    showBug: false
  };
  constructor(props) {
    super(props);
    this.UserId = "";
    this.singleItem = this.props.item.node;
    this.username = this.singleItem.author.username;
    this.title = this.singleItem.text;
    this.setclik = this.props.clickList ? this.props.clickList : null;
    this.commentID = [];

    moment.updateLocale("en", {
      relativeTime: {
        future: "in %s",
        past: "%s ago",
        s: "a few secs",
        ss: "%d secs",
        m: "1 minute",
        mm: "%d minutes",
        h: "1 hour",
        hh: "%d hours",
        d: "1 day",
        dd: "%d days",
        w: "1 week",
        ww: "%d weeks",
        M: "1 month",
        MM: "%d months",
        y: "1 year",
        yy: "%d years"
      }
    });
  }
  onClose = () => {
    this.setState({
      modalVisible: false,
      outSideClick: true,
      editModalVisible: false,
      blockUi: !this.state.blockUi
    });
    this.props.setFeedReportModalAction(false);
    this.props.disableSwaper(false);
    this.setState({
      pressBug: false
    });
  };

  onSubmit = (position, data) => {
    this.setState({
      modalVisible: false
    });
    this.props.closeModalBySubmit(position, data);
  };

  onOpen = async id => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    let changeData = this.state.modalVisible ? false : true;
    await this.setState({
      modalVisible: changeData,
      parent_content_id: id
    });
    await this.props.modalVisible(changeData, id);
    await this.setState({
      pressComment: changeData
    });
  };

  getStatus(clik) {
    let index = 0;
    if (clik != null) {
      index = this.props.getUserFollowCliksList.findIndex(
        i =>
          i
            .getIn(["clik", "name"])
            .toLowerCase()
            .replace("clik:", "") == clik.toLowerCase().replace("clik:", "")
      );
    }
    return index;
  }

  loginModalStatusEvent = async status => {
    this.setState({
      modalVisible: status
    });
  };

  goToUserProfile = async username => {
    this.props.userId({
      username: username,
      type: "feed"
    });
    this.props.navigation.navigate("profile", {
      username: username,
      type: "feed"
    });
  };

  onLayout = async event => {
    const { width, height } = event.nativeEvent.layout;
    if (width > 0) {
      this.props.calHeight(height, this.props.index);
      await this.setState({
        height: height
      });
      await this.getcommentID(this.props.nativeID, this.props.item);
    }
  };

  getcommentID = async (id, item) => {
    let data = commentID;
    let a = { ...commentsWithArrowKeys, [id]: { ...item } };
    await data.push(id);
    commentsWithArrowKeys = a;
    commentID = data;
  };

  getUnique = array => {
    var uniqueArray = [];
    for (let i = 0; i < array.length; i++) {
      if (uniqueArray.indexOf(array[i]) === -1) {
        uniqueArray.push(array[i]);
      }
    }
    return uniqueArray;
  };

  componentDidMount = async () => {
    getLocalStorage("UserName").then(res => {
      this.UserId = res;
    });
    this.props.onRef(this);
    if (commentID.indexOf(this.props.nativeID) == -1) {
      let elementOfView = document.getElementById(this.props.nativeID);
      if (elementOfView != null) {
        elementOfView.addEventListener("click", () => {
          let navTo = elementOfView.getAttribute("navTo");
          if (navTo == "right") {
            this.props.swiperRef.current.goToNext();
          } else if (navTo == "left") {
            this.props.swiperRef.current.goToPrev();
          }
        });

        await elementOfView.addEventListener("keydown", event => {
          if (
            event.keyCode == 82 &&
            moment
              .utc(this.singleItem.created)
              .add(5, "minutes")
              .format() <
            moment()
              .utc()
              .format()
          ) {
            this.props.loginStatus == 1
              ? this.handleBugClick()
              : this.props.setSignUpModalStatus(true);
          }
          if (event.keyCode == 67) {
            this.onOpen(this.singleItem.id);
          }
        });
      }
    }

    let textOfView = document.getElementById("Text:" + this.props.nativeID);
    if (textOfView != null) {
      await textOfView.addEventListener("mousedown", event => {
        let selection = "";
        this.props.setGesture(false);
        if (window.getSelection) {
          selection = window.getSelection();
          if (selection.toString() != "") {
            this.props.setGesture(false);
          }
        } else if (document.selection) {
          selection = document.selection.createRange();
          if (selection.toString() != "") {
            this.props.setGesture(false);
          }
        }
      });
      await textOfView.addEventListener("mouseup", event => {
        this.props.setGesture(true);
      });
    }
  };

  setNewBorderColor = async selectComment => {
    const prevId = this.props.selectComment.prevId;
    setTimeout(() => {
      let prevElementOfView = document.getElementById(prevId);
      let slectElementOfView = document.getElementById(selectComment);
      let elementOfTouchableOpacity = document.getElementById(
        "Comment:" + selectComment
      );
      if (slectElementOfView) {
        if (prevElementOfView) prevElementOfView.style.borderColor = "white";
        slectElementOfView.style.borderColor = "red";
        if (elementOfTouchableOpacity) {
          elementOfTouchableOpacity.focus();
          elementOfTouchableOpacity.style.outline = "none";
        }
      }
    }, 10);
  };

  onLayoutUsername = async event => {
    const { width, height } = event.nativeEvent.layout;
    if (width > 0) {
      activeComment;
      await this.setState({
        nameWidth: width
      });
    }
  };

  handleClick = e => {
    this.setState({
      showBug: e
    });
  };

  handleBugClick = e => {
    this.props.setFeedReportModalAction(true);
    this.setState({
      pressBug: true
    });
  };

  editCommentField = async () => {
    await this.setState(
      {
        editModalVisible: !this.state.editModalVisible,
        blockUi: !this.state.blockUi
      },
      async () => {
        await this.props.editmodalVisible(this.state.editModalVisible);
        await this.props.setGesture(false);
      }
    );
    this.props.disableSwaper(true);
  };

  getHeart = liketype => {
    switch (liketype) {
      case "RED":
        return [1, 0, 0, 0];
      case "SILVER":
        return [0, 1, 0, 0];
      case "GOLD":
        return [0, 0, 1, 0];
      case "DIAMOND":
        return [0, 0, 0, 1];
      default:
        return [0, 0, 0, 0];
    }
  };

  getColour = score => {
    return "#969faa";
    // if (score == 0) {
    //   return "#969faa";
    // } else if (0 < score && score <= 25) {
    //   return "#de5246";
    // } else if (25 < score && score <= 50) {
    //   return "#b0b0b0";
    // } else if (50 < score && score <= 75) {
    //   return "#ebca44";
    // } else {
    //   return "#8bbaf9";
    // }
  };

  getUpdateCode = item => {
    // if (item.search('pre') != -1) {
    //   let pre = document.getElementsByTagName('pre'), pl = pre.length;
    //   for (let i = 0; i < pl; i++) {
    //     console.log(item,pre[i],item.search(pre[i].innerHTML));
    //     let pro = pre[i].innerHTML.split(/<br>/), pz = pro.length;
    //     //pre[i].innerHTML = '';
    //     if (item.search(pre[i].innerHTML) != -1) {
    //       for (let a = 0; a < pz; a++) {
    //         pre[i].style.backgroundColor = '#F1F1F1'
    //         pre[i].style.whiteSpace = 'pre-wrap'
    //       }
    //     }
    //   }
    // }

    // if (item.search('blockquote') != -1) {
    //   let block = document.getElementsByTagName('blockquote'), blockpl = block.length;
    //   for (let i = 0; i < blockpl; i++) {
    //     console.log(item,block[i],item.search(block[i].innerHTML));
    //     let pro = block[i].innerHTML.split(/<br>/), pz = pro.length;
    //     //block[i].innerHTML = '';
    //     if (item.search(block[i].innerHTML) != -1) {
    //       for (let a = 0; a < pz; a++) {
    //         block[i].style.backgroundColor = 'white'
    //         block[i].style.borderLeft = '5px solid #f1f1f1'
    //         block[i].style.borderTopColor = 'white';
    //         block[i].style.borderRightColor = 'white';
    //         block[i].style.borderBottomColor = 'white';
    //         block[i].style.whiteSpace = 'pre-wrap';
    //         block[i].style.paddingLeft = '5px';
    //       }
    //     }
    //   }
    // }

    return item;
  };

  handleClick = e => {
    this.setState({ showBug: e });
  };

  handleDeleteMenu = () => {
    let value = false;
    if (this.username == this.UserId || this.props.isAdmin == true) {
      value = true
    } else {
      value = false;
    }
    return value
  }

  deleteComment = (commentId) => {
    applloClient
      .query({
        query: DeleteContentMutation,
        variables: {
          content_id: commentId.id
        },
        fetchPolicy: "no-cache"
      })
      .then(response => {
        if (response.data.content_delete.status.success == true) {
          this.getCommentList (this.props.PostId)         
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  getCommentList = async id => {
    fetch(apiUrl.API_URL + "v1/comments/" + id.replace("Post:", ""))
      .then(response => response.json())
      .then(async res => {
        let l1 = res.data.comments.edges;
        this.props.setPostCommentDetails(l1);
      });
  };

  render() {
    return (
      <View
        ref={myref => {
          this.myref = myref;
        }}
        style={{
          borderWidth: 1,
          borderColor: "transparent"
        }}
        onLayout={this.onLayout}
        nativeID={this.props.nativeID}
        onMouseEnter={() => this.setState({ showMenu: true })}
        onMouseLeave={() =>
          this.state.MenuHover == true
            ? this.setState({ showMenu: true })
            : this.setState({ showMenu: false })
        }
      >
        {this.state.pressBug == true ? (
          Platform.OS !== "web" ? (
            <Overlay
              animationType="zoomIn"
              visible={this.state.pressBug}
              onClose={this.onClose}
              closeOnTouchOutside
              children={
                <FeedReportModal
                  onClose={this.onClose}
                  PostId={"Comment:" + this.props.nativeID}
                  TopicList={[]}
                  ClikList={[]}
                />
              }
              childrenWrapperStyle={{
                padding: 0,
                margin: 0,
                borderRadius: 6
              }}
            />
          ) : (
            <Modal
              isVisible={this.state.pressBug}
              onBackdropPress={this.onClose}
              style={{
                marginHorizontal:
                  Dimensions.get("window").width > 750 ? "30%" : 10,
                padding: 0
              }}
            >
              <FeedReportModal
                onClose={this.onClose}
                PostId={"Comment:" + this.props.nativeID}
                TopicList={[]}
                ClikList={[]}
              />
            </Modal>
          )
        ) : null}

        {this.state.editModalVisible == false && (
          <View
            onMouseEnter={() => this.props.setGesture(true)}
            onMouseLeave={() => this.props.setGesture(false)}
            // onPress={async () => {
            //   this.props.setActiveId(this.props.nativeID);
            //   await this.setState({
            //     activeComment: this.getUnique(commentID).indexOf(
            //       this.props.nativeID
            //     ),
            //     activeIndex: this.props.index
            //   });
            //   this.setNewBorderColor(this.props.nativeID);
            // }}
            nativeID={"Comment:" + this.props.nativeID}
            style={{
              //marginBottom: 2 
            }}
          >
            <View
              style={{
                marginLeft: 10,
                flexDirection: "row",
                width: "100%"
              }}
            >
              {this.title && (
                <View
                  style={{
                    width: "95%",
                    paddingLeft: this.props.margin,
                    justifyContent: "flex-start"
                  }}
                >
                  {Platform.OS != "web" ? (
                    <Text
                      nativeID={"Text:" + this.props.nativeID}
                      onPress={() => {
                        this.props.setGesture(false);
                      }}
                      style={{
                        // color: "#000",
                        // paddingTop: 0,
                        // fontFamily: ConstantFontFamily.VerdanaFont,
                        // fontSize: 13,
                        // lineHeight: 20
                        textAlign: "center",
                        color: "#000",
                        paddingHorizontal: 10,
                        fontFamily: ConstantFontFamily.VerdanaFont,
                        fontSize: 16,
                        // height: titleHeight,
                        paddingVertical: 20,
                        overflow: 'hidden'
                      }}
                    >
                      {/* {this.title.length > 1000 && this.state.showMore == false
                        ? this.title.substring(0, 1000) + "..." + " "
                        : this.title} */}
                      {this.title.length > 1000 &&
                        this.state.showMore == false && (
                          <Text
                            onPress={() =>
                              this.setState({
                                showMore: true
                              })
                            }
                            style={{
                              color: "#000",
                              fontFamily: ConstantFontFamily.MontserratBoldFont,
                              fontSize: 13
                            }}
                          >
                            Read More
                          </Text>
                        )}
                    </Text>
                  ) : (
                    <table>
                      <tbody>
                        <tr>
                          <td
                            dangerouslySetInnerHTML={{ __html: this.title }}
                            style={{
                              color: "#000",
                              fontFamily: ConstantFontFamily.VerdanaFont,
                              fontSize: 13,
                              // lineHeight: 1.5,
                              userSelect: "text"
                            }}
                            onClick={() => {
                              this.props.setGesture(false);
                            }}
                          />
                        </tr>
                      </tbody>
                    </table>
                  )}
                </View>
              )}
            </View>

            <View
              style={{
                flexDirection: "row",
                width: "100%",
                marginTop: 3,
                paddingLeft: this.props.margin == 30 ? 40 : 10
              }}
            >
              <View
                style={{
                  width: "30%",
                  flexDirection: "row",
                  justifyContent: "flex-end",
                  paddingRight: 10,
                  alignItems: "center"
                }}
              ></View>
            </View>
            <View
              style={{
                width: "100%",
                flexDirection: "row",
                paddingHorizontal: 10,
                paddingTop: 5,
                //paddingBottom: 7
              }}
            >
              <View
                style={{
                  width: "60%",
                  flexDirection: "row"
                }}
              >
                <View style={{ alignSelf: "center" }}>
                  {this.props.item.node.author.profile_pic ? (
                    <Image
                      source={{
                        uri: this.props.item.node.author.profile_pic
                      }}
                      style={{
                        width: 30,
                        height: 30,
                        borderRadius: 18,
                        borderWidth: 1,
                        borderColor: "#e1e1e1",
                        marginRight: 5
                      }}
                    />
                  ) : (
                    <Image
                      source={require("../assets/image/default-image.png")}
                      style={{
                        width: 30,
                        height: 30,
                        borderRadius: 18,
                        borderWidth: 1,
                        borderColor: "#e1e1e1",
                        marginRight: 5
                      }}
                    />
                  )}
                </View>
                <Hoverable>
                  {isHovered => (
                    <TouchableHighlight
                      onPress={() =>
                        this.goToUserProfile(this.singleItem.author.username)
                      }
                      style={{ alignSelf: "center" }}
                    >
                      <Text
                        style={{
                          justifyContent: "flex-start",
                          alignContent: "flex-start",
                          color: "#6D757F",
                          paddingRight: 10,
                          fontFamily: ConstantFontFamily.VerdanaFont,
                          fontSize: 13,
                          alignSelf: "center",
                          textAlign: "center",
                          textDecorationLine:
                            isHovered == true ? "underline" : "none"
                        }}
                      >
                        @{this.props.item.node.author.username}
                      </Text>
                    </TouchableHighlight>
                  )}
                </Hoverable>
              </View>
              <View
                style={{
                  width: "40%",
                  flexDirection: "row",
                  flex: 1,
                  justifyContent: "space-between",
                  alignItems: "center"
                }}
              >
                <TouchableOpacity
                  onMouseEnter={() => this.setState({ MenuHover: true })}
                  onMouseLeave={() => this.setState({ MenuHover: false })}
                >
                  <Menu>
                    <MenuTrigger>
                      <Image
                        source={require("../assets/image/menu.png")}
                        style={{
                          height: 16,
                          width: 16,
                          alignSelf: "flex-end",
                          marginRight: 15
                          //transform: [{ rotate: "90deg" }]
                        }}
                      />
                    </MenuTrigger>
                    <MenuOptions
                      optionsContainerStyle={{
                        borderRadius: 6,
                        borderWidth: 1,
                        borderColor: "#e1e1e1",
                        shadowColor: "transparent"
                      }}
                      customStyles={{
                        optionsContainer: {
                          minHeight: 50,
                          width: 150,
                          marginTop: 15,
                          marginLeft: -140
                        }
                      }}
                    >
                      <MenuOption
                        onSelect={() => {
                          this.props.loginStatus == 1
                            ? this.handleBugClick()
                            : this.props.setSignUpModalStatus(true);
                        }}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Report
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>
                      {moment
                        .utc(this.singleItem.created)
                        .add(10, "minutes")
                        .format() >
                        moment()
                          .utc()
                          .format() && this.username == this.UserId &&
                        <MenuOption
                          onSelect={() => {
                            this.editCommentField()
                          }}
                        >
                          <Hoverable>
                            {isHovered => (
                              <Text
                                style={{
                                  textAlign: "center",
                                  color: isHovered == true ? "#009B1A" : "#000",
                                  fontFamily:
                                    ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                Edit
                              </Text>
                            )}
                          </Hoverable>
                        </MenuOption>
                      }
                      {this.handleDeleteMenu() == true &&
                        <MenuOption
                          onSelect={() => {
                            this.deleteComment(this.props.item.node)
                          }}
                        >
                          <Hoverable>
                            {isHovered => (
                              <Text
                                style={{
                                  textAlign: "center",
                                  color: isHovered == true ? "#009B1A" : "#000",
                                  fontFamily:
                                    ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                Delete
                              </Text>
                            )}
                          </Hoverable>
                        </MenuOption>
                      }
                    </MenuOptions>
                  </Menu>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                  disabled={
                    this.getStatus(this.singleItem.clik) != -1 ||
                      this.props.loginStatus == 0
                      ? false
                      : true
                  }
                  onPress={() => {
                    this.onOpen(this.singleItem.id);
                  }}
                >
                  <Icon
                    color={this.getColour(this.singleItem.comments_score)}
                    name={"comment-o"}
                    type="font-awesome"
                    size={20}
                    iconStyle={{ alignSelf: "center", marginBottom: 2, }}
                    containerStyle={{ alignSelf: "center" }}
                  />
                  <Text
                    style={{
                      // marginBottom: 2,
                      // textAlign: "center",
                      // fontSize: 18,
                      // paddingHorizontal: 3,
                      // fontFamily: ConstantFontFamily.VerdanaFont,
                      // color: "#6D757F",

                      marginBottom: 2,
                      textAlign: "center",
                      fontSize: 13,
                      paddingHorizontal: 3,
                      fontFamily: ConstantFontFamily.VerdanaFont,
                      color: "#6D757F"
                    }}
                  >
                    {this.props.item.node.comments_score}
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={{ flexDirection: "row", alignItems: "center" }}
                >
                  <ChangeIcon
                    PostId={this.singleItem.id}
                    selectedIconList={this.singleItem.user_like_type}
                    score={this.singleItem.likes_score}
                    loginModalStatus={this.loginModalStatusEvent}
                    onOpen={status => this.handleClick(status)}
                    dataFrom={"comment"}
                    showStatus={this.state.showBug}
                    pressBug={this.state.pressBug}
                  />
                  <Text
                    style={{
                      marginBottom: 2,
                      textAlign: "center",
                      paddingHorizontal: 3,
                      fontSize: 13,
                      fontFamily: ConstantFontFamily.VerdanaFont,
                      color: "#6D757F"
                    }}
                  >
                    {this.props.item.node.likes_score}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        )}

        {this.title &&
          this.state.editModalVisible == true &&
          this.props.loginStatus == 1 && (
            <View style={{ marginLeft: this.props.margin == 30 ? 30 : 0 }}>
              {
                <EditCommentCard
                  onClose={this.onClose}
                  parent_content_id={this.state.parent_content_id}
                  closeModalBySubmit={this.onSubmit}
                  clickList={this.setclik ? [...this.setclik] : null}
                  initial="child"
                  topComment={this.singleItem}
                  navigation={this.props.navigation}
                  outSideClick={this.outSideClick}
                />
              }
            </View>
          )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List(),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  selectComment: state.PostCommentDetailsReducer.get("selectComment"),
  PostId: state.PostCommentDetailsReducer.get("PostId"),
});

const mapDispatchToProps = dispatch => ({
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setFeedReportModalAction: payload =>
    dispatch(setFEEDREPORTMODALACTION(payload)),
  setActiveId: payload => dispatch({ type: "SET_ACTIVE_ID", payload }),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  PostDetailsCommentSwaper
);
