import React, { useState } from "react";
import {
  Image,
  Platform,
  Text,
  TouchableOpacity,
  View,
  TouchableHighlight
} from "react-native";
import { Button, Icon, Tooltip } from "react-native-elements";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import ConstantFontFamily from "../constants/FontFamily";
import ReportCliksTopics from "../components/ReportCliksTopics";
import applloClient from "../client";
import { ContentReportMutation } from "../graphqlSchema/graphqlMutation/FeedMutation";
import { ContentReportVariables } from "../graphqlSchema/graphqlVariables/LikeContentVariables";
import ButtonStyle from "../constants/ButtonStyle";

const FeedListRightContent = props => {
  const [offTopicActive, setoffTopicActive] = useState(false);
  const [lowQualityActive, setlowQualityActive] = useState(false);
  const [hateActive, sethateActive] = useState(false);
  const [submit, setsubmit] = useState(false);
  const [showoffTopictooltip, setshowoffTopictooltip] = useState(false);
  const [showlowQualitytooltip, setshowlowQualitytooltip] = useState(false);
  const [showhatetooltip, setshowhatetooltip] = useState(false);

  const [showCliksSubmit, setshowCliksSubmit] = useState([]);
  const [showTopicsSubmit, setshowTopicsSubmit] = useState([]);

  const offTopicBtn = () => {
    offTopicActive == false
      ? setoffTopicActive(true)
      : setoffTopicActive(false);
  };
  const lowQualityBtn = () => {
    lowQualityActive == false
      ? setlowQualityActive(true)
      : setlowQualityActive(false);
  };
  const hateBtn = () => {
    hateActive == false ? sethateActive(true) : sethateActive(false);
  };
  const submitReport = async id => {
    setsubmit(true);
    setoffTopicActive(false);
    setlowQualityActive(false);
    sethateActive(false);
    ContentReportVariables.variables.content_id = id;
    ContentReportVariables.variables.reports[0].type = "OFF_TOPIC";
    ContentReportVariables.variables.reports[0].off_topic_topics = [
      ...showTopicsSubmit
    ];
    ContentReportVariables.variables.reports[0].off_topic_cliks = [
      ...showCliksSubmit
    ];
    await applloClient.query({
      query: ContentReportMutation,
      ...ContentReportVariables,
      fetchPolicy: "no-cache"
    });
  };
  const offTooltip = () => {
    setshowoffTopictooltip(false);
    setshowlowQualitytooltip(false);
    setshowhatetooltip(false);
  };
  const offTopictooltip = () => {
    showoffTopictooltip == false
      ? setshowoffTopictooltip(true)
      : setshowoffTopictooltip(false);
  };
  const lowQualitytooltip = () => {
    showlowQualitytooltip == false
      ? setshowlowQualitytooltip(true)
      : setshowlowQualitytooltip(false);
  };
  const hatetooltip = () => {
    showhatetooltip == false
      ? setshowhatetooltip(true)
      : setshowhatetooltip(false);
  };

  const submitTopic = item => {
    return setshowTopicsSubmit(item);
  };
  const submitCliks = item => {
    return setshowCliksSubmit(item);
  };

  return (
    <View
      style={{
        backgroundColor: "#fff",
        height:
          props.item.item.node.thumbnail_pic != null ? hp("50%") : hp("40%")
      }}
    >
      <Text
        style={{
          alignSelf: "center",
          color: "#000",
          fontFamily: ConstantFontFamily.MontserratBoldFont,
          fontSize: 18,
          fontWeight: "bold",
          marginLeft: 10,
          marginTop: 10
        }}
      >
        Report Content
      </Text>

      <TouchableOpacity
        onPress={() => offTooltip()}
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <View
          style={{
            flexDirection: "row",
            width: "100%"
          }}
        >
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              width: "33%"
            }}
          >
            <View
              style={{
                flexDirection: "column",
                width: "100%",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                disabled={submit == true ? true : false}
                onPress={() =>
                  props.loginStatus == 1
                    ? offTopicBtn()
                    : props.setSignUpModalStatus(true)
                }
                style={{ flexDirection: "row" }}
              >
                <Image
                  source={require("../assets/image/off-topic.png")}
                  style={[
                    { height: 50, width: 50 },
                    offTopicActive && styles.ActiveImageStyle
                  ]}
                />
              </TouchableOpacity>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    color: "#000",
                    marginVertical: 10,
                    fontFamily: ConstantFontFamily.defaultFont,
                    fontSize: 14,
                    fontWeight: "bold"
                  }}
                >
                  Off-Topic
                </Text>
              </View>
              {Platform.OS == "web" ? (
                <View>
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    name="info-circle"
                    type="font-awesome"
                    size={18}
                    onPress={() => offTopictooltip()}
                  />
                  {showoffTopictooltip == true && (
                    <Tooltip
                      withPointer={false}
                      withOverlay={false}
                      toggleOnPress={true}
                      containerStyle={{
                        left: -50,
                        top: -60,
                        width: 100
                      }}
                      popover={<Text>Info here</Text>}
                    />
                  )}
                </View>
              ) : (
                <Tooltip withOverlay={false} popover={<Text>Info here</Text>}>
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    name="info-circle"
                    type="font-awesome"
                    size={18}
                  />
                </Tooltip>
              )}
            </View>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              width: "33%"
            }}
          >
            <View
              style={{
                flexDirection: "column",
                width: "100%",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                disabled={submit == true ? true : false}
                onPress={() =>
                  props.loginStatus == 1
                    ? lowQualityBtn()
                    : props.setSignUpModalStatus(true)
                }
                style={{ flexDirection: "row" }}
              >
                <Image
                  source={require("../assets/image/low-quality.png")}
                  style={[
                    { height: 50, width: 50 },
                    lowQualityActive && styles.ActiveImageStyle
                  ]}
                />
              </TouchableOpacity>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    color: "#000",
                    marginVertical: 10,
                    fontFamily: ConstantFontFamily.defaultFont,
                    fontSize: 14,
                    fontWeight: "bold"
                  }}
                >
                  Low Quality
                </Text>
              </View>
              {Platform.OS == "web" ? (
                <View>
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    name="info-circle"
                    type="font-awesome"
                    size={18}
                    onPress={() => lowQualitytooltip()}
                  />
                  {showlowQualitytooltip == true && (
                    <Tooltip
                      withPointer={false}
                      withOverlay={false}
                      toggleOnPress={true}
                      containerStyle={{
                        left: -50,
                        top: -60,
                        width: 100
                      }}
                      popover={<Text>Info here</Text>}
                    />
                  )}
                </View>
              ) : (
                <Tooltip withOverlay={false} popover={<Text>Info here</Text>}>
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    name="info-circle"
                    type="font-awesome"
                    size={18}
                  />
                </Tooltip>
              )}
            </View>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              width: "33%"
            }}
          >
            <View
              style={{
                flexDirection: "column",
                width: "100%",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                disabled={submit == true ? true : false}
                onPress={() =>
                  props.loginStatus == 1
                    ? hateBtn()
                    : props.setSignUpModalStatus(true)
                }
                style={{ flexDirection: "row" }}
              >
                <Image
                  source={require("../assets/image/hate.png")}
                  style={[
                    { height: 50, width: 50 },
                    hateActive && styles.ActiveImageStyle
                  ]}
                />
              </TouchableOpacity>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    color: "#000",
                    marginVertical: 10,
                    fontFamily: ConstantFontFamily.defaultFont,
                    fontSize: 14,
                    fontWeight: "bold"
                  }}
                >
                  Hate
                </Text>
              </View>
              {Platform.OS == "web" ? (
                <View>
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    name="info-circle"
                    type="font-awesome"
                    size={18}
                    onPress={() => hatetooltip()}
                  />
                  {showhatetooltip == true && (
                    <Tooltip
                      withPointer={false}
                      withOverlay={false}
                      toggleOnPress={true}
                      containerStyle={{
                        left: -50,
                        top: -60,
                        width: 100
                      }}
                      popover={<Text>Info here</Text>}
                    />
                  )}
                </View>
              ) : (
                <Tooltip withOverlay={false} popover={<Text>Info here</Text>}>
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    name="info-circle"
                    type="font-awesome"
                    size={18}
                  />
                </Tooltip>
              )}
            </View>
          </View>
        </View>

        {offTopicActive == true && (
          <View
            style={{
              flexDirection: "row",
              width: "100%"
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "flex-start",
                flexWrap: "wrap",
                width: "100%",
                padding: 10
              }}
            >
              <ReportCliksTopics
                topicsList={props.item.item.node.topics}
                cliksList={props.item.item.node.cliks}
                submitTopic={submitTopic}
                submitCliks={submitCliks}
              />
            </View>
          </View>
        )}

        <View
          style={{
            marginTop: 20
          }}
        >
          {submit == false ? (
            <Button
              title="Submit Report"
              buttonStyle={{
                backgroundColor: "#000",
                borderColor: "#fff",
                borderRadius: 6,
                borderWidth: 1
              }}
              titleStyle={ButtonStyle.wtitleStyle}
              buttonStyle={ButtonStyle.gbackgroundStyle}
              containerStyle={ButtonStyle.containerStyle}
              onPress={() => submitReport(props.item.item.node.id)}
              disabled={props.loginStatus == 1 ? false : true}
            />
          ) : (
            <Text
              style={{
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold"
              }}
            >
              Report Submitted !
            </Text>
          )}
        </View>
      </TouchableOpacity>
    </View>
  );
};

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus")
});

const mapDispatchToProps = dispatch => ({
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  FeedListRightContent
);

export const styles = {
  ActiveImageStyle: {
    borderColor: "green",
    borderWidth: 3,
    borderRadius: 25
  }
};
