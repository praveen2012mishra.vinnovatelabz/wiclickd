import firebase from "firebase/app";
import "firebase/auth";
import getEnvVars from '../environment';

const env = getEnvVars();

// Initialize Firebase
const firebaseConfig = {
    apiKey: env.APIKEY,
    authDomain: env.AUTHDOMAIN,
    databaseURL: env.DATABASEURL,
    projectId: env.PROJECTID,
    storageBucket: env.STORAGEBUCKET,
    messagingSenderId: env.MESSAGINGSENDERID,
    appId: env.APPID,
    measurementId: env.MEASUREMENTID,
};

firebase.initializeApp(firebaseConfig);

export default firebase;