import { List } from "immutable";
import React, { useState, useEffect } from "react";
import { graphql } from "react-apollo";
import {
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Image,
  TouchableHighlight,
  FlatList,
  ActivityIndicator,
  Dimensions
} from "react-native";
import { Button, Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { listTopicFeed } from "../actionCreator/TopicsFeedAction";
import { getTrendingTopicsProfileDetails } from "../actionCreator/TrendingTopicsProfileAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import AppHelper from "../constants/AppHelper";
import ConstantFontFamily from "../constants/FontFamily";
import {
  TopicFollowMutation,
  TopicUnFollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  TopicFollowVariables,
  TopicUnFollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import NavigationService from "../library/NavigationService";
import TopicStar from "./TopicStar";
import { SearchTopicMutation } from "../graphqlSchema/graphqlMutation/SearchMutation";
import {
  SearchTopicVariables,
  SearchUserVariables
} from "../graphqlSchema/graphqlVariables/SearchVariables";
import { TRENDING_TOPICS } from "../graphqlSchema/graphqlMutation/TrendingMutation";
import ConstantColors from '../constants/Colors';

class TrendingTopics extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onEndReachedCalledDuringMomentum = true;
    this.viewabilityConfig = {
      viewAreaCoveragePercentThreshold: 50
    };
    this.state = {
      searchedFollowText: this.props.searchedFollowText,
      // getTrending_Topics_List: this.props.getTrending_Topics_List,
      getTrending_Topics_List: [],
      page: 1,
      loading: true,
      loadingMore: false,
      refreshing: false,
      pageInfo: null,
      error: null,
      apiCall: true
    };
  }

  goToProfile = async id => {
    this.props.topicId({
      id: id,
      type: "feed"
    });
    this.props.setPostCommentReset({
      payload: [],
      postId: "",
      title: "",
      loading: true
    });
    this.props.leftPanelModalFunc(false)
  };

  componentDidMount = () => {
    this.getTrendingTopicList();
  };

  componentDidUpdate = async prevProps => {
    if (prevProps.searchedFollowText != this.props.searchedFollowText) {
      await this.topicSearch(this.props.searchedFollowText);
    } else if (prevProps.searchedFollowText == "") {
      // this.setState({
      //   getTrending_Topics_List: this.props.getTrending_Topics_List
      // });
    }
  };

  topicSearch = search => {
    this.setState({ search });
    let tempData = [];
    let tempArray = [];

    if (search.length > 0) {
      SearchTopicVariables.variables.prefix = search;
      applloClient
        .query({
          query: SearchTopicMutation,
          ...SearchTopicVariables,
          fetchPolicy: "no-cache"
        })
        .then(res => {
          tempArray = res.data.search.topics;
          for (let i = 0; i < tempArray.length; i++) {
            tempData.push({ node: tempArray[i] });
          }
          this.setState({
            getTrending_Topics_List: tempData
          });
        });
    } else {
      this.setState({pageInfo : null}, ()=>{
        this.getTrendingTopicList();
      })
      // this.setState({
      //   getTrending_Topics_List: this.props.getTrending_Topics_List
      // });
    }
  };

  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this.getTrendingTopicList();
      }
    );
  };

  _handleLoadMore = distanceFromEnd => {
    this.setState(
      (prevState, nextProps) => ({
        //  loadingMore: true
      }),
      () => {
        if (
          0 <= distanceFromEnd &&
          distanceFromEnd <= 5 &&
          this.state.apiCall == true
        ) {
          this.setState({
            apiCall: false
          });
          this.getTrendingTopicList();
        }
      }
    );
  };

  _renderFooter = () => {
    if (!this.state.loadingMore) return null;
    return (
      <View>
        <ActivityIndicator animating size="large" color="#000" />
      </View>
    );
  };

  getTrendingTopicList = () => {
    let __self = this;
    const { page, pageInfo } = this.state;

    if (pageInfo == null) {
      this.setState({
        loadingMore: true
      });
      applloClient
        .query({
          query: TRENDING_TOPICS,
          variables: {
            first: 20,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "TRENDING"
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          __self.setState({
            loading: false,
            getTrending_Topics_List: this.props.getUserFollowTopicList
              .toJS()
              .concat(this.getTopicList(res.data.topic_list.edges)),
            pageInfo: res.data.topic_list.pageInfo,
            page: page + 1,
            apiCall: true,
            loadingMore: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else if (pageInfo != null && pageInfo.hasNextPage == true) {
      this.setState({
        loadingMore: true
      });
      applloClient
        .query({
          query: TRENDING_TOPICS,
          variables: {
            first: 20,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "TRENDING"
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          __self.setState({
            loading: false,
            getTrending_Topics_List: __self.state.getTrending_Topics_List.concat(
              this.getTopicList(res.data.topic_list.edges)
            ),
            pageInfo: res.data.topic_list.pageInfo,
            apiCall: true,
            loadingMore: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    }
  };

  onViewableItemsChanged = ({ viewableItems, changed }) => {
    let perLoadDataCount = 20;
    let halfOfLoadDataCount = perLoadDataCount / 2;
    let lastAddArr =
      this.state.getTrending_Topics_List.length > 0 &&
      this.state.getTrending_Topics_List.slice(-perLoadDataCount);
    try {
      if (lastAddArr[halfOfLoadDataCount] && viewableItems.length > 0) {
        viewableItems.find(item => {
          let topicId = item.item.node ? item.item.node.id : item.item.topic.id;
          let LasttopicId = lastAddArr[halfOfLoadDataCount].node
            ? lastAddArr[halfOfLoadDataCount].node.id
            : lastAddArr[halfOfLoadDataCount].topic.id;
          if (topicId === LasttopicId) {
            this._handleLoadMore(0);
          }
        });
      }
    } catch (e) {
      console.log(e, "lastAddArr", lastAddArr[halfOfLoadDataCount]);
    }
  };

  getTopicList = item => {
    let newArray = [];
    item.forEach(obj => {
      let index = this.props.getUserFollowTopicList.findIndex(
        i => i.getIn(["topic", "name"]) == obj.node.name
      );
      if (index == -1) {
        newArray.push({ ...obj });
      }
    });
    return newArray;
  };

  _renderItem = item => {
    var row = item.item.node ? item.item.node : item.item.topic;
    return (
      <View
        key={item.index}
        style={{
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          width: "100%"
        }}
      >
        <View
          style={{
            width: "80%",
            backgroundColor: ConstantColors.customeBackgroundColor,
          }}
        >
          <TouchableOpacity
            onPress={() => this.goToProfile(row.name)}
            style={{
              marginTop: 10,
              height: 30,
              alignSelf: "flex-start",
              padding: 5,
              backgroundColor: row.parents ? "#e3f9d5" : "#e3f9d5",
              borderRadius: 6
            }}
          >
            <Hoverable>
              {isHovered => (
                <Text
                  style={{
                    width: "100%",
                    color: row.parents ? "#009B1A" : "#009B1A",
                    fontSize: 15,
                    fontWeight: "bold",
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    textDecorationLine: isHovered == true ? "underline" : "none"
                  }}
                >
                  /{row.name.toLowerCase()}
                </Text>
              )}
            </Hoverable>
          </TouchableOpacity>
        </View>
        <TopicStar
          TopicName={row.name}
          ContainerStyle={{
            height: 30,
            justifyContent: "center",
            width: "20%",
            alignItems: "center",
            marginTop: 10,
            paddingLeft: 10
          }}
          ImageStyle={{
            height: 20,
            width: 20,
            alignSelf: "flex-end",
            marginRight: 2
          }}
        />
      </View>
    );
  };

  render() {
    return (
      <View style={{backgroundColor: ConstantColors.customeBackgroundColor,}}>
        {/* <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            backgroundColor: "#fff",
            paddingTop: 0
          }}
        >
          {this.state.getTrending_Topics_List.map((item, i) => {
            if (i < 10) {
              return (
                <View
                  key={i}
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    width: "100%"
                  }}
                >
                  <View
                    style={{
                      width: "80%",
                      backgroundColor: "#fff"
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => this.goToProfile(item.node.name)}
                      style={{
                        marginTop: 10,
                        height: 30,
                        alignSelf: "flex-start",
                        padding: 5,
                        backgroundColor: item.node.parents
                          ? "#e3f9d5"
                          : "#FEF6D1",
                        borderRadius: 6
                      }}
                    >
                      <Hoverable>
                        {isHovered => (
                          <Text
                            style={{
                              width: "100%",
                              color: item.node.parents ? "#009B1A" : "#FEC236",
                              fontSize: 15,
                              fontWeight: "bold",
                              fontFamily: ConstantFontFamily.MontserratBoldFont,
                              textDecorationLine:
                                isHovered == true ? "underline" : "none"
                            }}
                          >
                            /{item.node.name.toLowerCase()}
                          </Text>
                        )}
                      </Hoverable>
                    </TouchableOpacity>
                  </View>
                  <TopicStar
                    TopicName={item.node.name}
                    ContainerStyle={{
                      height: 30,
                      justifyContent: "center",
                      width: "20%",
                      alignItems: "center",
                      marginTop: 10,
                      paddingLeft: 10
                    }}
                    ImageStyle={{
                      height: 20,
                      width: 20,
                      alignSelf: "flex-end",
                      marginRight: 2
                    }}
                  />
                </View>
              );
            }
          })}
        </ScrollView> */}
        <FlatList
          extraData={this.state}
          contentContainerStyle={{
            flexDirection: "column",
            height: Dimensions.get("window").height,
            width: "100%"
          }}
          data={this.state.getTrending_Topics_List}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItem}
          showsVerticalScrollIndicator={false}
          onRefresh={this._handleRefresh}
          refreshing={this.state.refreshing}
          // onEndReached={({ distanceFromEnd }) => {
          //   this._handleLoadMore(distanceFromEnd);
          // }}
          onEndReachedThreshold={0.2}
          initialNumToRender={10}
          ListFooterComponent={this._renderFooter}
          onViewableItemsChanged={this.onViewableItemsChanged}
          viewabilityConfig={this.viewabilityConfig}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  getTrending_Topics_List: state.TrendingTopicsReducer.get(
    "Trending_Topics_List"
  )
    ? state.TrendingTopicsReducer.get("Trending_Topics_List")
    : List(),
  getUserFollowTopicList: state.LoginUserDetailsReducer.get(
    "userFollowTopicsList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowTopicsList")
    : List(),
  loginStatus: state.UserReducer.get("loginStatus")
});

const mapDispatchToProps = dispatch => ({
  topicId: payload => dispatch(getTrendingTopicsProfileDetails(payload)),
  listTopicFeed: payload => dispatch(listTopicFeed(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setTopicFollowList: payload =>
    dispatch({ type: "SET_TOPIC_FOLLOW_LIST", payload }),
  setPostCommentReset: payload =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload }),
  leftPanelModalFunc : payload => dispatch ({type : 'LEFT_PANEL_OPEN' , payload})
});

const TrendingTopicsWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(TrendingTopics);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  TrendingTopicsWrapper
);
