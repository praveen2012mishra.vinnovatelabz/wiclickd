import React, { Component, lazy, Suspense } from "react";
import {
  Text,
  View,
  TextInput,
  Platform,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import ButtonStyle from "../constants/ButtonStyle";
import { Button } from "react-native-elements";
import ConstantFontFamily from "../constants/FontFamily";
import applloClient from "../client";
import { ClikJoinMutation } from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { ClikJoinVariables } from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { graphql } from "react-apollo";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import { Hoverable } from "react-native-web-hooks";
import { List } from "immutable";

class JoinWithInviteKey extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qualification: "",
      username: "",
      showError: false,
      MutipleUserList: [],
      UserList: [],
      status: "Default",
      member_type: "",
      user_msg: "",
      invite_key: ""
    };
  }
  requestInvite = async () => {
    ClikJoinVariables.variables.clik_id = this.props.cliksDetails
      .getIn(["data", "clik"])
      .get("id");
    ClikJoinVariables.variables.qualification = this.state.qualification;
    ClikJoinVariables.variables.known_members = [];
    ClikJoinVariables.variables.invite_key = this.state.invite_key;
    try {
      await applloClient
        .query({
          query: ClikJoinMutation,
          ...ClikJoinVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          if (res.data.clik_join.status.custom_status == "JOINED") {
            let member_type = res.data.clik_join.member_type;
            this.setState({
              status: "Success",
              member_type: member_type
            });
            let resDataLogin = await this.props.Login();
            await this.props.saveLoginUser(resDataLogin.data.login);
          } else if (res.data.clik_join.status.custom_status == "PENDING") {
            this.setState({
              status: "Pending"
            });
          } else {
            let user_msg = res.data.clik_join.status.user_msg;
            this.setState({
              status: "Failure",
              user_msg: user_msg
            });
          }
        });
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    return (
      <View
        style={{
          borderWidth: 1,
          borderColor: "#C5C5C5",
          borderRadius: 10,
          backgroundColor: "#fff",
          marginBottom: 10
        }}
      >
        <Text
          style={{
            textAlign: "left",
            color: "#000",
            fontSize: 16,
            fontWeight: "bold",
            fontFamily: ConstantFontFamily.MontserratBoldFont,
            marginVertical: 10,
            marginHorizontal: 10
          }}
        >
          Join with invite key
        </Text>
        <TextInput
          value={this.state.invite_key}
          placeholder={"Enter Key"}
          style={{
            padding: 5,
            color: "#000",
            fontFamily: ConstantFontFamily.defaultFont,
            fontSize: 14,
            width: "100%",
            borderRadius: 6,
            borderColor: "#C5C5C5",
            borderWidth: 1,
            marginLeft: 19,
            width: "95%",
            minHeight: Platform.OS === "ios" && 4 ? 10 * 4 : null
          }}
          onChangeText={text =>
            this.setState({
              invite_key: text
            })
          }
        />
        {this.state.status == "Default" && (
          <Button
            onPress={() => this.requestInvite()}
            color="#000"
            title="Join"
            titleStyle={ButtonStyle.titleStyle}
            buttonStyle={ButtonStyle.backgroundStyle}
            containerStyle={ButtonStyle.containerStyle}
          />
        )}
        {this.state.status == "Success" && (
          <View
            style={{
              width: "100%",
              backgroundColor: "#fff",
              paddingVertical: 20
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              Congratulations!
            </Text>

            <View
              style={{
                justifyContent: "center",
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  color: "#000",
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  fontSize: 16,
                  fontWeight: "bold",
                  margin: 10
                }}
              >
                You are now a {this.state.member_type.toLowerCase()} of{" "}
              </Text>

              <TouchableOpacity
                style={{
                  marginTop: 5,
                  height: 30,
                  alignSelf: "flex-start",
                  padding: 5,
                  backgroundColor: "#E8F5FA",
                  borderRadius: 6
                }}
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        width: "100%",
                        color: "#4169e1",
                        fontSize: 15,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        textDecorationLine:
                          isHovered == true ? "underline" : "none"
                      }}
                    >
                      {" "}
                      #
                      {this.props.cliksDetails
                        .getIn(["data", "clik"])
                        .get("name")}{" "}
                    </Text>
                  )}
                </Hoverable>
              </TouchableOpacity>
            </View>
          </View>
        )}

        {this.state.status == "Failure" && (
          <View
            style={{
              width: "100%",
              backgroundColor: "#fff",
              paddingVertical: 20
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              Sorry, you could not join the clik at this time.
            </Text>

            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              {this.state.user_msg}
            </Text>
          </View>
        )}

        {this.state.status == "Pending" && (
          <View
            style={{
              width: "100%",
              backgroundColor: "#fff",
              paddingVertical: 20
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              An admin of the clik will review your application.
            </Text>
          </View>
        )}
      </View>
    );
  }
}
const mapStateToProps = state => ({
  cliksDetails: state.TrendingCliksProfileReducer.get(
    "getTrendingCliksProfileDetails"
  ),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  listClikUserRequest: !state.ClikUserRequestReducer.get("ClikUserRequestList")
    ? List()
    : state.ClikUserRequestReducer.get("ClikUserRequestList"),
  listClikMembers: !state.ClikMembersReducer.get("clikMemberList")
    ? List()
    : state.ClikMembersReducer.get("clikMemberList"),
  loginStatus: state.UserReducer.get("loginStatus"),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List()
});

const mapDispatchToProps = dispatch => ({
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload))
});

const JoinWithInviteKeyWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(JoinWithInviteKey);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  JoinWithInviteKeyWrapper
);
