import React, { Component } from "react";
import { View, Text, Dimensions, StyleSheet } from "react-native";
import IconMenu from "./IconMenu";
import NavigationService from "../library/NavigationService";

class BottomScreenForIconMenu extends Component {  

  render() {
    return (
      <View
        style={{
          width: "100%",
          backgroundColor:
            Dimensions.get("window").width <= 750 ? "#fff" : "#000",
          height: 50,
          flexDirection: "row",
          justifyContent: "space-around",
          alignItems: "center",
          borderTopWidth: 1,
          borderTopColor: "#c5c5c5",
        }}
      >
        <IconMenu navigation={NavigationService} />
      </View>
    );
  }
}
export default BottomScreenForIconMenu

