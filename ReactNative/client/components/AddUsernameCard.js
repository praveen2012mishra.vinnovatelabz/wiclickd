import React from "react";
import { graphql } from "react-apollo";
import {
  AsyncStorage,
  Dimensions,
  Image,
  Text,
  TextInput,
  View,
  TouchableOpacity
} from "react-native";
import { Button, Icon } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import {
  saveUserLoginDaitails,
  setLoginStatus
} from "../actionCreator/UserAction";
import { setUSERNAMEMODALACTION } from "../actionCreator/UsernameModalAction";
import applloClient from "../client";
import ConstantFontFamily from "../constants/FontFamily";
import {
  AccountCreateMutation,
  CheckUsernameMutation,
  UserLoginMutation
} from "../graphqlSchema/graphqlMutation/UserMutation";
import firebase from "firebase/app";
import "firebase/auth";
import { getLocalStorage, setLocalStorage } from "../library/Helper";
import jwt_decode from "jwt-decode";
import { setAdminStatus } from "../actionCreator/AdminAction";
import AppHelper from "../constants/AppHelper";
import ButtonStyle from "../constants/ButtonStyle";
import SidePanel from "./SidePanel";
import NavigationService from "../library/NavigationService";
import { setCreateAccount } from "../actionCreator/CreateAccountAction";
import { AccountCreateVariables } from "../graphqlSchema/graphqlVariables/UserVariables";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import { getTrendingUsers } from "../actionCreator/TrendingUsersAction";
import { getTrendingExternalFeeds } from "../actionCreator/TrendingExternalFeedsAction";
import getEnvVars from "../environment";
import { InviteKeyClikProfileMutation } from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { Hoverable } from "react-native-web-hooks";
import { UserQueryMutation } from "../graphqlSchema/graphqlMutation/UserMutation";

const apiUrl = getEnvVars();

const expr = /^[a-zA-Z0-9_]{3,16}$/;

class AddUsernameCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      UserNameData: "",
      errorMessage: "",
      typingUsername: false,
      errorForm: true,
      exists_msg: "",
      clikName: "",
      inviteKey: "",
      userName: "",
      InviteClik: {},
      inviteUsername: "",
      inviterProfilePic: "",
      tempSize: 16,
    };
  }

  checkUssername = async username => {
    this.setState({
      typingUsername: true
    });
    return await this.ValidateUsername(username);
  };

  ValidateUsername = username => {
    if (!expr.test(username)) {
      let msg =
        "Only Alphabets, Numbers and Underscore and between 3 to 16 characters.";
      this.setState({
        errorMessage: msg
      });
    } else {
      this.setState({
        UserNameData: username,
        errorMessage: "",
        errorForm: false
      });
    }
  };

  formSubmit = async () => {
    let __self = this;
    await applloClient
      .query({
        query: CheckUsernameMutation,
        variables: {
          username: this.state.UserNameData
        },
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        if (res) {
          if (!res.data.username_check.status.success) {
            __self.setState({
              errorMessage: res.data.username_check.status.user_msg
            });
          } else {
            let UserName = __self.state.UserNameData;
            let newUser = {
              UserPassword: "",
              UserName: UserName,
              UserFirstName: "",
              UserLastName: "",
              UserEmail: ""
            };
            setLocalStorage("newUserFirebase", JSON.stringify(newUser)).then(
              async () => {
                (await AsyncStorage.getItem("skipCredentials")) == "true"
                  ? this.props.navigation.navigate("verification")
                  : // this.props.navigation.navigate("credentials", { "clikName": this.state.clikName, "inviteKey": this.state.inviteKey });
                  this.props.setUsernameModalStatus(false);
                this.props.setSignUpModalStatus(true);
              }
            );
          }
        }
      })
      .catch(e => console.log(e));
  };

  componentDidMount = () => {
    this.setState(
      {
        clikName:
          this.props.inviteUserDetails &&
          this.props.inviteUserDetails.get("clikName"),
        inviteKey:
          this.props.inviteUserDetails &&
          this.props.inviteUserDetails.get("inviteKey"),
        userName:
          this.props.inviteUserDetails &&
          this.props.inviteUserDetails.get("userName")
      },
      () => {
        // if (this.state.inviteKey != "") {
        //   this.getKeyProfile(this.state.inviteKey);
        // }
      }
    );
    if (
      this.props.inviteUserDetails &&
      this.props.inviteUserDetails.get("inviteKey") != ""
    ) {
      this.getKeyProfile(
        this.props.inviteUserDetails &&
        this.props.inviteUserDetails.get("inviteKey")
      );
    }
    if (
      this.props.inviteUserDetails &&
      this.props.inviteUserDetails.get("userName") != ""
    ) {
      this.setState({
        inviteUsername:
          this.props.inviteUserDetails &&
          this.props.inviteUserDetails.get("userName")
      });
      this.getUserPic(
        this.props.inviteUserDetails &&
        this.props.inviteUserDetails.get("userName")
      );
    }
    this.props.setUsernameModalStatus(true);
    //   window.addEventListener("popstate", function(e) {
    //     e.preventDefault();
    //     let BackButton = document.getElementById("BackButton");
    //     BackButton.click();
    //   });
  };

  getKeyProfile = async key => {
    try {
      await applloClient
        .query({
          query: InviteKeyClikProfileMutation,
          variables: {
            invite_key: key
          },
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState({
            InviteClik: res.data.clik_invite_key,
            inviteUsername: res.data.clik_invite_key.inviter.username,
            inviterProfilePic: res.data.clik_invite_key.inviter.profile_pic
          });
        });
    } catch (e) {
      console.log(e);
    }
  };

  getUserPic = async key => {
    try {
      await applloClient
        .query({
          query: UserQueryMutation,
          variables: {
            id: "User:" + key
          },
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState({
            inviterProfilePic: res.data.user.profile_pic
          });
        });
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    return (
      <View
        style={{
          backgroundColor: "#f4f4f4",
          borderColor: "#c5c5c5",
          borderRadius: 6,
          // maxHeight: 450,
          width: Dimensions.get('window').width >= 750 ? 450 : '100%',
          height:500,
          // alignSelf:'center'
        }}
      >
        {/* <View style={{ width: Dimensions.get("window").width - 550 }}>
          <SidePanel
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
            navigation={NavigationService}
          />
        </View> */}
        {/* ======================== header section====================== */}
        <View>
          <Hoverable>
            {isHovered => (
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  flex: 1,
                  position: "absolute",
                  zIndex: 999999,
                  left: 0,
                  top: 0
                }}
                onPress={this.props.onClose}
              >
                <Icon
                  color={isHovered == true ? "rgba(256,256,256,0.4)" : "#000"}
                  iconStyle={{
                    color: "#fff",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  reverse
                  name="close"
                  type="antdesign"
                  size={16}
                />
              </TouchableOpacity>
            )}
          </Hoverable>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              backgroundColor: "#000",
              alignItems: "center",
              height: 50,
              borderTopLeftRadius: 6,
              borderTopRightRadius: 6,
              width: "100%"
            }}
          >
            {/* <Text style={[styles.TextHeaderStyle, { color: "#fff" }]}>
              Sign Up
            </Text> */}
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "row",
              }}
            >
              <Image
                source={require("../assets/image/logo.png")}
                style={{
                  height: 30,
                  width: 30,
                  marginRight: 5
                }}
                resizeMode={"contain"}
              />
              <Text
                style={{
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  fontWeight: "bold",
                  fontSize: 22,
                  textAlign: "center",
                  color: "white"
                }}
              >
                weclikd
                  </Text>
            </View>
          </View>
        </View>

        {/* ======================== Body====================== */}

        <View
          style={{
            // width: 550,
            alignItems: "center",
            flex: 1,
            padding: 20,
            backgroundColor: "#fff",
            borderBottomLeftRadius: 6,
            borderBottomRightRadius: 6,
            borderColor: "#c5c5c5"
          }}
        >
          {/* <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "row"
            }}
          >
            <Image
              source={require("../assets/image/logo.png")}
              style={{
                height: 60,
                width: 60
              }}
              resizeMode={"contain"}
            />
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 45,
                fontFamily: ConstantFontFamily.MontserratBoldFont
              }}
            >
              weclikd
            </Text>
          </View> */}

          <View
            style={{
              alignItems: "center",
              // justifyContent: "center",
              marginTop: 20,
              marginHorizontal: 20
            }}
          >
            {this.props.inviteUserDetails &&
              this.props.inviteUserDetails.get("clikName") === "" &&
              this.props.inviteUserDetails &&
              this.props.inviteUserDetails.get("inviteKey") === "" &&
              this.props.inviteUserDetails &&
              this.props.inviteUserDetails.get("userName") === "" && (
                <View>
                  <Text style={styles.StaticTextStyle}>
                    {" "}
                    engage in quality discussions
                  </Text>
                  <Text style={styles.StaticTextStyle}>
                    gain visibility for your content
                  </Text>
                  <Text style={styles.StaticTextStyle}>
                    {" "}
                    monetize your post
                  </Text>
                </View>
              )}
          </View>
          {/* <View> */}
          {this.props.inviteUserDetails &&
            this.props.inviteUserDetails.get("inviteKey") != "" ? (
              <View>
                {this.state.inviterProfilePic ? (
                  <Image
                    source={{
                      uri: this.state.inviterProfilePic
                    }}
                    style={{
                      width: 60,
                      height: 60,
                      borderRadius: 30,
                      borderWidth: 1,
                      borderColor: "#e1e1e1",
                      alignSelf: "center",
                      marginBottom: 10
                    }}
                  />
                ) : (
                    <Image
                      source={require("../assets/image/default-image.png")}
                      style={{
                        width: 60,
                        height: 60,
                        borderRadius: 30,
                        borderWidth: 1,
                        borderColor: "#e1e1e1",
                        alignSelf: "center",
                        marginBottom: 10
                      }}
                    />
                  )}
                <Text style={[styles.StaticTextStyle, { marginBottom: 10 }]}>
                  @{this.state.inviteUsername} has invited you to join
              </Text>
              </View>
            ) : // <Text style={[styles.StaticTextStyle, { marginBottom: 10 }]}>
            //   You were invited to join
            // </Text>
            null}

          {this.props.inviteUserDetails &&
            this.props.inviteUserDetails.get("userName") != "" ? (
              <View>
                {this.state.inviterProfilePic ? (
                  <Image
                    source={{
                      uri: this.state.inviterProfilePic
                    }}
                    style={{
                      width: 60,
                      height: 60,
                      borderRadius: 30,
                      borderWidth: 1,
                      borderColor: "#e1e1e1",
                      alignSelf: "center",
                      marginBottom: 10
                    }}
                  />
                ) : (
                    <Image
                      source={require("../assets/image/default-image.png")}
                      style={{
                        width: 60,
                        height: 60,
                        borderRadius: 30,
                        borderWidth: 1,
                        borderColor: "#e1e1e1",
                        alignSelf: "center",
                        marginBottom: 10
                      }}
                    />
                  )}
                <Text style={[styles.StaticTextStyle, { marginBottom: 10 }]}>
                  @
                {this.props.inviteUserDetails &&
                    this.props.inviteUserDetails.get("userName")}{" "}
                has invited you to join Weclikd
              </Text>
              </View>
            ) : // <Text style={[styles.StaticTextStyle, { marginBottom: 10 }]}>
            //   You were invited to join
            // </Text>
            null}

          {this.props.inviteUserDetails &&
            this.props.inviteUserDetails.get("clikName") != "" && (
              <View>
                {this.props.inviteUserDetails &&
                  this.props.inviteUserDetails.get("inviteKey") == "" && (
                    <Text
                      style={[styles.StaticTextStyle, { marginBottom: 10 }]}
                    >
                      You were invited to join
                    </Text>
                  )}
                <View
                  style={{
                    width: 150,
                    backgroundColor: "#E8F5FA",
                    alignItems: "center",
                    justifyContent: "center",
                    borderWidth: 1,
                    borderColor: "#c5c5c5",
                    borderRadius: 10,
                    height: 30,
                    marginBottom: 20
                  }}
                >
                  <Text
                    style={{
                      color: "#4169e1",
                      fontSize: 15,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }}
                  >
                    {/* #{this.state.clikName} */}#
                    {this.props.inviteUserDetails &&
                      this.props.inviteUserDetails.get("clikName")}
                  </Text>
                </View>
              </View>
            )}

          <View
            style={{
              width: "100%",
              paddingTop: 10,
              paddingBottom: 10
            }}
          >
            <View
              style={{
                alignItems: "center",
                justifyContent: "flex-start",
                flexDirection: "row",
                height: 30
              }}
            >
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 10,
                  fontFamily: ConstantFontFamily.defaultFont,
                  color: "red"
                }}
              >
                {this.state.errorMessage}
              </Text>
            </View>

            <Text
              style={{
                fontWeight: "bold",
                fontSize: 16,
                fontFamily: ConstantFontFamily.MontserratBoldFont
              }}
            >
              Choose a username
            </Text>
            <View
              style={{
                marginTop: 5,
                flexDirection: "row",
                height: 45,
                alignItems: "center",
                width: "100%"
              }}
            >
              <TextInput
                onChangeText={UserName => this.checkUssername(UserName)}
                onFocus={() => this.setState({ tempSize: 16 })}
                placeholder="What should we call you?"
                underlineColorAndroid="transparent"
                style={{
                  height: 45,
                  paddingLeft: 10,
                  borderWidth: 1,
                  borderRadius: 5,
                  color: "#000",
                  fontFamily: ConstantFontFamily.defaultFont,
                  width: "100%",
                  fontSize: this.state.tempSize
                }}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-end"
                }}
              >
                {this.state.typingUsername == true ? (
                  this.state.errorMessage == "" ? (
                    // <Image
                    //   style={{
                    //     width: 20,
                    //     height: 20,
                    //     marginRight: 10
                    //   }}
                    //   source={require("../assets/image/success.png")}
                    // />
                    <Icon
                      size={20}
                      name="check"
                      type="font-awesome"
                      iconStyle={{ marginRight: 10 }}
                      color="#009B1A"
                      underlayColor="#000"
                    />
                  ) : (
                      // <Image
                      //   style={{
                      //     width: 20,
                      //     height: 20,
                      //     marginRight: 10
                      //   }}
                      //   source={require("../assets/image/close-red.png")}
                      // />
                      <Icon
                        size={20}
                        name="close"
                        type="font-awesome"
                        iconStyle={{ marginRight: 10 }}
                        color="#de5246"
                        underlayColor="#000"
                      />
                    )
                ) : (
                    // <Image
                    //   style={{
                    //     width: 20,
                    //     height: 20,
                    //     marginRight: 10
                    //   }}
                    //   source={require("../assets/image/close-red.png")}
                    // />
                    <Icon
                      size={20}
                      name="close"
                      type="font-awesome"
                      iconStyle={{ marginRight: 10 }}
                      color="#de5246"
                      underlayColor="#000"
                    />
                  )}
              </View>
            </View>
            <View
              style={{
                marginVertical: 20,
                flexDirection: "row",
                justifyContent: 'flex-end'
              }}
            >
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 12,
                  color: "#49525D",
                  fontFamily: ConstantFontFamily.VerdanaFont
                }}
              >
                Already have an account? &nbsp;
                <Text
                  style={{
                    textDecorationLine: "underline",
                    color: "#49525D",
                    fontSize: 12,
                    fontFamily: ConstantFontFamily.VerdanaFont
                  }}
                  onPress={() => {
                    this.props.setLoginModalStatus(true);
                  }}
                >
                  Login
                </Text>
              </Text>
            </View>

            <View
              style={{
                alignItems: "center"
              }}
            >
              <Button
                title="Sign up"
                titleStyle={ButtonStyle.titleStyle}
                buttonStyle={ButtonStyle.backgroundStyle}
                containerStyle={ButtonStyle.containerStyle}
                disabled={this.state.errorForm ? true : false}
                onPress={this.formSubmit}
              />
            </View>
          </View>

          {/* bottom view */}

          <View
            style={{
              width: "100%",
              marginTop: 10,
              alignItems: "center"
            }}
          >
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 10,
                fontFamily: ConstantFontFamily.defaultFont,
                textAlign: "center"
              }}
            >
              By clicking Sign up, you agree to our{" "}
            </Text>

            <Text
              style={{
                fontWeight: "bold",
                fontSize: 10,
                fontFamily: ConstantFontFamily.defaultFont,
                textAlign: "center"
              }}
            >
              <Text
                style={{
                  textDecorationLine: "underline"
                }}
                onPress={() => {
                  this.props.onClose();
                  NavigationService.navigate("termsandconditions");
                }}
              >
                Terms
              </Text>
              <Text>
                {" "}
                and that you have read our{" "}
                <Text
                  style={{
                    textDecorationLine: "underline"
                  }}
                  onPress={() => {
                    this.props.onClose();
                    NavigationService.navigate("privacyPolicy");
                  }}
                >
                  Privacy Policy
                </Text>{" "}
                and{" "}
                <Text
                  style={{
                    textDecorationLine: "underline"
                  }}
                  onPress={() => {
                    this.props.onClose();
                    NavigationService.navigate("privacyPolicy");
                  }}
                >
                  Content Policy
                </Text>
                .
              </Text>
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  inviteUserDetails: state.AdminReducer.get("inviteUserDetails")
});

const mapDispatchToProps = dispatch => ({
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  changeLoginStatus: payload => dispatch(setLoginStatus(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setUsernameModalStatus: payload => dispatch(setUSERNAMEMODALACTION(payload)),
  changeAdminStatus: payload => dispatch(setAdminStatus(payload)),
  setCreateAccount: payload => dispatch(setCreateAccount(payload)),
  getTrendingUsers: payload => dispatch(getTrendingUsers(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  getTrendingExternalFeeds: payload =>
    dispatch(getTrendingExternalFeeds(payload))
});

const AddUsernameCardContainerWrapper = compose(
  graphql(UserLoginMutation, {
    name: "Login",
    options: { fetchPolicy: "no-cache" }
  }),
  graphql(AccountCreateMutation, {
    name: "UserCreate"
  })
)(AddUsernameCard);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddUsernameCardContainerWrapper);

export const styles = {
  TextHeaderStyle: {
    fontSize: 23,
    color: "#000",
    textAlign: "center",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  StaticTextStyle: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    fontFamily: ConstantFontFamily.defaultFont
  },


};
