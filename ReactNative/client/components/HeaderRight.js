import firebase from "firebase/app";
import "firebase/auth";
import React, { useEffect } from "react";
import {
  TouchableOpacity,
  AsyncStorage,
  Dimensions,
  Image,
  Platform,
  Text,
  View,
  StyleSheet
} from "react-native";
import { Button } from "react-native-elements";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger
} from "react-native-popup-menu";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setAdminStatus } from "../actionCreator/AdminAction";
import { setFEEDREPORTMODALACTION } from "../actionCreator/FeedReportModalAction";
import { setInviteSIGNUPMODALACTION } from "../actionCreator/InviteSignUpModalAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setRESETPASSWORDMODALACTION } from "../actionCreator/ResetPasswordModalAction";
import { setSHARELINKMODALACTION } from "../actionCreator/ShareLinkModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import {
  setLoginStatus,
  saveUserLoginDaitails
} from "../actionCreator/UserAction";
import { setUSERNAMEMODALACTION } from "../actionCreator/UsernameModalAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import { setVERIFYEMAILMODALACTION } from "../actionCreator/VerifyEmailModalAction";
import ConstantFontFamily from "../constants/FontFamily";
import { Badge, Icon } from "react-native-elements";
import NavigationService from "../library/NavigationService";
import { setUserApproachAction } from "../actionCreator/UserApproachAction";
import { setScreenLoadingModalAction } from "../actionCreator/ScreenLoadingModalAction";
import { setLocalStorage } from "../library/Helper";
import jwt_decode from "jwt-decode";
import { graphql } from "react-apollo";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import AppHelper from "../constants/AppHelper";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingExternalFeeds } from "../actionCreator/TrendingExternalFeedsAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import { getTrendingUsers } from "../actionCreator/TrendingUsersAction";
import applloClient from "../client";
import {
  GetNumUnreadNotificationsMutation,
  MarkNotificationsAsReadMutation
} from "../graphqlSchema/graphqlMutation/Notification";
import SearchInput from "../components/SearchInput";
import SearchInputWeb from "../components/SearchInputWeb";
import { GetAccountNotificationsMutation } from "../graphqlSchema/graphqlMutation/Notification";

class HeaderRight extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      UnreadNotifications: 0,
      term: "",
      showSearchIcon: true,
      browser: ""
    };
  }
  gotoprofile = () => {
    this.props.userId({
      username:
        this.props.profileData &&
        this.props.profileData
          .getIn(["my_users"])
          .getIn(["0", "user", "username"]),
      type: "feed"
    });
    this.props.navigation.navigate("profile", {
      username:
        this.props.profileData &&
        this.props.profileData
          .getIn(["my_users"])
          .getIn(["0", "user", "username"]),
      type: "feed"
    });
  };

  logout = async () => {
    // await AsyncStorage.multiRemove([
    //   "userLoginId",
    //   "MyUserUserId",
    //   "userIdTokenWeclikd",
    //   "UserId",
    //   "admin",
    //   "skipCredentials"
    // ])
    // .then(async r => {
    //   await firebase
    //     .auth()
    //     .signOut()
    //     .then(async res => {
    //       await this.props.changeLoginStatus(0);
    //       await this.props.changeAdminStatus(false);
    //       if (
    //         (await this.props.navigation.getCurrentRoute().routeName) ==
    //         "notification" ||
    //         (await this.props.navigation.getCurrentRoute().routeName) ==
    //         "settings"
    //       ) {
    //         await this.props.navigation.navigate("home");
    //       }
    //       await this.props.resetLoginUserDetails();
    //       await this.props.resetUserProfileDetails();

    //       if (Platform.OS == "web") {
    //         this.extensionLogout();
    //       }
    //     });
    // });
    await localStorage.clear();
    await firebase.auth().signOut();
    await this.props.changeLoginStatus(0);
    await this.props.changeAdminStatus(false);
    if (
      (await this.props.navigation.getCurrentRoute().routeName) ==
      "notification" ||
      (await this.props.navigation.getCurrentRoute().routeName) == "settings"
    ) {
      await this.props.navigation.navigate("home");
    }
    await this.props.resetLoginUserDetails();
    await this.props.resetUserProfileDetails();

    if (Platform.OS == "web") {
      this.extensionLogout();
    }
    var req = indexedDB.deleteDatabase("firebaseLocalStorageDb");
  };

  extensionLogout = () => {
    try {
      window.parent.postMessage({ type: "wecklid_logout" }, "*");
    } catch (err) {
      console.log("Extension Logout Error ", err);
    }
  };

  loginHandle = () => {
    this.props.leftPanelModalFunc(false)
    this.props.setLoginModalStatus(true);
  };

  inviteSignHandle = async () => {
    await this.props.leftPanelModalFunc(false)
    await this.props.setInviteUserDetail({
      clikName: "",
      inviteKey: "",
      userName: ""
    });
    await this.props.setUsernameModalStatus(true);

    // this.props.navigation.navigate("username");
  };

  componentDidMount = async () => {
    if(!this.state.UnreadNotifications > 0){
      AsyncStorage.setItem('notificationMessageNumber',101);
    }
    if (this.props.getUserApproach == "login") {
      // navigator.storage.estimate().then(data => {
      //   if (data.usageDetails.caches) {
      //     this.userLogin();
      //   } else {
      //     alert("Google Login is not supported in Incognito mode");
      //   }
      // });
      //this.props.setLoginModalStatus(false);
      await this.props.setUserApproachAction({ type: "" });
      this.userLogin();
    }
    if (this.props.getUserApproach == "signUp") {
      this.props.setSignUpModalStatus(true);
    }
    if (this.props.loginStatus == 1) {
      this.getUnreadNotifications();
    }
  };

  onModalClose = async () => {
    await this.props.setLoginButtonText("Logged In!");
    await this.props.setGoogleLogin(true);
    setTimeout(() => {
      this.props.setLoginModalStatus(false);
      this.props.setLoginButtonText("Login");
      this.props.setGoogleLogin(false);
    }, 2000);
  };

  getUnreadNotifications = () => {
    applloClient
      .query({
        query: GetNumUnreadNotificationsMutation,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        this.setState({
          UnreadNotifications: res.data.account.num_unread_notifications
        });
        let timer = setTimeout(() => {
          if (this.props.loginStatus == 1) {
            this.getUnreadNotifications();
          }
        }, 60000);
      });
  };

  setMarkAsRead = () => {
    applloClient
      .query({
        query: MarkNotificationsAsReadMutation,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        //console.log(res,'===================================>');
        this.setState({
          UnreadNotifications: 0
        });
        this.getUnreadNotifications();
      });
  };

  userLogin = async () => {
    let __self = this;
    await this.props.setLoginButtonText("Logging in...");
    await this.props.setGoogleLogin(true);
    await firebase.auth().onAuthStateChanged(async res => {
      if (res) {
        __self.props.setScreenLoadingModalAction(true);
        return await res
          .getIdToken(true)
          .then(async function (idToken) {
            await setLocalStorage("userIdTokenFirebase", idToken);
            await setLocalStorage(
              "admin",
              jwt_decode(idToken).admin ? "true" : "false"
            );
            await __self.props.changeAdminStatus(
              jwt_decode(idToken).admin ? jwt_decode(idToken).admin : false
            );

            if (Platform.OS == "web") {
              __self.setUserNameInExtension = __self.setLoginTokenInExtension(
                idToken
              );
            }
            return idToken;
          })
          .then(async res => {
            if (res) {
              let loginData = await __self.props.Login();
              if (loginData.data.login.status.status == "NOT_FOUND") {
                await __self.props.changeLoginStatus(0);
                __self.props.setScreenLoadingModalAction(false);
                __self.props.setLoginButtonText("Login");
                __self.props.setGoogleLogin(false);
                alert("Invalid email or password");
              } else {
                await __self.props.saveLoginUser(loginData.data.login);
                await __self.props.changeLoginStatus(1);
                await __self.props.getHomefeed({
                  currentPage: AppHelper.PAGE_LIMIT
                });
                await __self.props.getTrendingUsers({
                  currentPage: AppHelper.PAGE_LIMIT
                });
                await __self.props.getTrendingClicks({
                  currentPage: AppHelper.PAGE_LIMIT
                });
                await __self.props.getTrendingTopics({
                  currentPage: AppHelper.PAGE_LIMIT
                });
                await __self.props.getTrendingExternalFeeds({
                  currentPage: AppHelper.PAGE_LIMIT
                });
                await AsyncStorage.setItem(
                  "userLoginId",
                  loginData.data.login.account.id
                );
                await AsyncStorage.setItem(
                  "MyUserUserId",
                  loginData.data.login.account.my_users[0].id
                );
                await AsyncStorage.setItem(
                  "UserId",
                  loginData.data.login.account.my_users[0].user.id
                );
                await AsyncStorage.setItem(
                  "UserName",
                  loginData.data.login.account.my_users[0].user.username
                );
                await __self.onModalClose();
                // set user name to extension
                if (Platform.OS == "web") {
                  __self.setUserNameInExtension(
                    loginData.data.login.account.my_users[0].user.username
                  );
                }
                await __self.props.setScreenLoadingModalAction(false);
              }
            }
          })
          .catch(error => {
            console.log(error);
            __self.props.setScreenLoadingModalAction(false);
            alert("Invalid email or password");
            this.props.setLoginButtonText("Login");
            this.props.setGoogleLogin(false);
            return false;
          });
      }
      // else {
      //   alert("Google Login is not supported in Incognito mode");
      // }
    });
  };

  setLoginTokenInExtension = idToken => UserName => {
    try {
      window.parent.postMessage(
        { type: "wecklid_login", userIdTokenFirebase: idToken, UserName },
        "*"
      );
    } catch (e) {
      console.log("extension login Error ", e);
    }
  };

  componentWillUnmount = () => {
    this.setState({ showSearchIcon: true });
  };

  getAccountNotifications = () => {
    applloClient
      .query({
        query: GetAccountNotificationsMutation,
        variables: {
          first: 100,
          after: null
        },
        fetchPolicy: "no-cache"
      })
      .then(async res => { });
  };

  render() {
    return (
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        {!this.state.showSearchIcon && 
          Dimensions.get("window").width <= 1100 && (
            <View style={{ width: !this.state.showSearchIcon && '100%' }}>
              <SearchInputWeb
                navigation={this.props.navigation}
                refs={ref => {
                  this.input = ref;
                }}
                displayType={"web"}
                press={status => {  
                  this.setState({ showSearchIcon: status });
                }}
              />
            </View>
          )}
        {/* {Dimensions.get("window").width >= 1100 && (
            <SearchInputWeb
              navigation={this.props.navigation}
              refs={ref => {
                this.input = ref;
              }}
              key={Math.random()}
              displayType={"web"}
            />
          )} */}
        {this.props.loginStatus == 1 ? (
          <View
            style={{
              alignItems: "center",
              // justifyContent: "flex-end",
              height: 40,
              flexDirection: "row",
              // marginRight: Dimensions.get("window").width >= 750 && 15
            }}
          >
            {/* {this.props.loginStatus === 1 && ( */}
            {/* <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "flex-end",
                  alignItems: "flex-end",
                  alignSelf: "center"
                }}
              >
                {!this.state.showSearchIcon &&
                  Dimensions.get("window").width <= 1100 && (
                    <SearchInputWeb
                      navigation={this.props.navigation}
                      refs={ref => {
                        this.input = ref;
                      }}
                      displayType={"web"}
                      press={status => {
                        this.setState({ showSearchIcon: status });
                      }}
                    />
                  )}
                {Dimensions.get("window").width >= 1100 && (
                  <SearchInputWeb
                    navigation={this.props.navigation}
                    refs={ref => {
                      this.input = ref;
                    }}
                    key={Math.random()}
                    displayType={"web"}
                  />
                )}
              </View> */}
            {/* )} */}
            {Dimensions.get("window").width >= 1100 && (
              <View
                style={{
                  flexDirection: "row",
                  marginLeft: 10
                }}
              >
                {Dimensions.get("window").width >= 750 && (
                  <Icon
                    testID="AddLink"
                    size={22}
                    name="plus"
                    type="font-awesome"
                    iconStyle={styles.actionButtonIcon}
                    color="#fff"
                    underlayColor="#000"
                    onPress={() => this.props.setShareLinkModalStatus(true)}
                  />
                )}
                <Icon
                  size={20}
                  name="bar-chart"
                  type="font-awesome"
                  iconStyle={styles.actionButtonIcon}
                  color="#fff"
                  underlayColor="#000"
                  onPress={() => NavigationService.navigate("analytics")}
                />
                <View style={{ flexDirection: "row", marginRight: Dimensions.get('window').width <= 750 ? 19 : 30 }}>
                  <Icon
                    name="bell-o"
                    size={22}
                    type="font-awesome"
                    iconStyle={{
                      color: Dimensions.get('window').width <= 750 ? "#fff" : "#fff",
                      alignSelf: "center",
                      alignItems: "center",
                      justifyContent: "center",
                      // paddingTop: 2
                    }}
                    underlayColor={"#000"}
                    color="#fff"
                    onPress={() => {
                      NavigationService.navigate("notification"),
                        this.setMarkAsRead(),
                        this.getAccountNotifications();
                    }}
                  />                  
                  {this.state.UnreadNotifications > 0 && (
                    <Badge
                      value={this.state.UnreadNotifications}
                      containerStyle={{
                        position: "absolute",
                        top: -5,
                        right: this.state.UnreadNotifications >= 10 ? -15 : -10
                      }}
                      badgeStyle={{
                        backgroundColor: "#de5246",
                        borderRadius: 10,
                        borderColor: "#de5246",
                        justifyContent: "center"
                      }}
                      textStyle={{
                        textAlign: "center",
                        color: "#fff",
                        fontSize: 13,
                        fontFamily: ConstantFontFamily.defaultFont
                      }}
                    />
                  )}
                </View>
              </View>
            )}
            {
              this.state.showSearchIcon === true &&
              Dimensions.get("window").width <= 1100 && (
                <View style={{ flexDirection: "row", alignItems: 'center' }}>
                  <Icon
                    name="search"
                    size={25}
                    type="feather"
                    iconStyle={styles.actionButtonIcon}
                    iconStyle={{
                      color: Dimensions.get('window').width <= 750 ? "#fff" : "#fff",
                      marginRight: Dimensions.get('window').width <= 750 ? 19 : 30,
                    }}
                    color="#fff"
                    underlayColor="#000"
                    onPress={() => {
                      this.props.setSearchBarStatus(true);
                      this.props.searchOpenBarStatus(true);
                      this.setState({ showSearchIcon: false });
                    }}
                  />
                  {Dimensions.get("window").width >= 750 && (
                    <Icon
                      testID="AddLink"
                      size={24}
                      name="plus"
                      type="font-awesome"
                      iconStyle={styles.actionButtonIcon}
                      color="#fff"
                      underlayColor="#000"
                      onPress={() => this.props.setShareLinkModalStatus(true)}
                    />
                  )}
                  {Dimensions.get('window').width >= 1100 &&
                    <Icon
                      size={22}
                      name="bar-chart"
                      type="font-awesome"
                      iconStyle={styles.actionButtonIcon}
                      iconStyle={{
                        color: Dimensions.get('window').width <= 750 ? "#000" : "#fff",
                        marginRight: Dimensions.get('window').width <= 750 ? 19 : 30,
                      }}
                      color="#fff"
                      underlayColor="#000"
                      onPress={() => NavigationService.navigate("analytics")}
                    />
                  }

                  <View
                    style={{
                      flexDirection: "row",
                      marginRight: Dimensions.get('window').width <= 750 ? 19 : 30,
                      // paddingTop: 3
                    }}
                  >
                    <Icon
                      name="bell-o"
                      size={24}
                      type="font-awesome"
                      iconStyle={{
                        color: Dimensions.get('window').width <= 750 ? "#fff" : "#fff",
                        alignSelf: "center",
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                      underlayColor={Dimensions.get('window').width <= 750 ? "#000" : "#000"}
                      color="#fff"
                      onPress={() => {
                        NavigationService.navigate("notification"),
                          this.setMarkAsRead(),
                          this.getAccountNotifications();
                      }}
                    />
                    {this.state.UnreadNotifications > 0 && (
                      <Badge
                        value={this.state.UnreadNotifications}
                        containerStyle={{
                          position: "absolute",
                          top: -5,
                          right:
                            this.state.UnreadNotifications >= 10 ? -15 : -10
                        }}
                        badgeStyle={{
                          backgroundColor: "#de5246",
                          borderRadius: 10,
                          borderColor: "#de5246",
                          justifyContent: "center"
                        }}
                        textStyle={{
                          textAlign: "center",
                          color: "#fff",
                          fontSize: 13,
                          fontFamily: ConstantFontFamily.defaultFont
                        }}
                      />
                    )}
                  </View>
                  {/* </View> */}


                  {/* { this.state.showSearchIcon === true && Dimensions.get('window').width <= 750 && ( */}
                  {/* <Menu > */}
                  <TouchableOpacity testID="ProfileIcon" onPress={() => { this.props.leftPanelModalFunc(true) }}>
                    {this.props.profileData &&
                      this.props.profileData
                        .getIn(["my_users"])
                        .getIn(["0", "user", "profile_pic"]) != null ? (
                        <Image
                          source={{
                            uri: this.props.profileData
                              .getIn(["my_users"])
                              .getIn(["0", "user", "profile_pic"])
                          }}
                          style={{
                            height: 27,
                            width: 27,
                            borderRadius: 13,
                            borderWidth: 1,
                            borderColor: Dimensions.get('window').width <= 750 ? "#fff" : "#fff",
                            marginRight:
                              Dimensions.get("window").width <= 1100 ? 10 : null
                          }}
                          navigation={this.props.navigation}
                        />
                      ) : (
                        <Image
                          source={require("../assets/image/default-image.png")}
                          style={{
                            height: 27,
                            width: 27,
                            borderRadius: 13,
                            borderWidth: 1,
                            borderColor: Dimensions.get('window').width <= 750 ? "#fff" : "#fff",
                          }}
                          navigation={this.props.navigation}
                        />
                      )}
                  </TouchableOpacity>
                  {/* </Menu> */}
                </View>
              )}

            {this.state.showSearchIcon === true && Dimensions.get('window').width >= 1100 && (
                <Menu>
                  <MenuTrigger testID="ProfileIcon">
                    {this.props.profileData &&
                      this.props.profileData
                        .getIn(["my_users"])
                        .getIn(["0", "user", "profile_pic"]) != null ? (
                        <Image
                          source={{
                            uri: this.props.profileData
                              .getIn(["my_users"])
                              .getIn(["0", "user", "profile_pic"])
                          }}
                          style={{
                            height: 26,
                            width: 26,
                            borderRadius: 13,
                            borderWidth: 1,
                            borderColor: Dimensions.get('window').width <= 750 ? "#fff" : "#fff",
                            marginRight:
                              Dimensions.get("window").width <= 1100 ? 10 : null
                          }}
                          navigation={this.props.navigation}
                        />
                      ) : (
                        <Image
                          source={require("../assets/image/default-image.png")}
                          style={{
                            height: 26,
                            width: 26,
                            borderRadius: 13,
                            borderWidth: 1,
                            borderColor: Dimensions.get('window').width <= 750 ? "#fff" : "#fff",
                          }}
                          navigation={this.props.navigation}
                        />
                      )}
                  </MenuTrigger>               
                  <MenuOptions
                    optionsContainerStyle={{
                      borderRadius: 10,
                      borderWidth: 1,
                      borderColor: "#c5c5c5",
                      shadowColor: "transparent",
                      
                    }}
                    customStyles={{
                      optionsContainer: {
                        marginTop: 50,
                        marginRight:20,
                        marginLeft:6
                      },
                      optionWrapper: { padding: 5}
                    }}
                  >
                    <MenuOption onSelect={() => this.gotoprofile()}>
                      <Hoverable>
                        {isHovered => (
                          <Text
                            testID="ProfileDetails"
                            style={{
                              textAlign: "center",
                              color: isHovered == true ? "#009B1A" : "#000",
                              fontFamily: ConstantFontFamily.MontserratBoldFont
                            }}
                          >
                            Profile
                          </Text>
                        )}
                      </Hoverable>
                    </MenuOption>

                    <MenuOption
                      onSelect={() => {
                        return this.props.navigation.navigate("settings");
                      }}
                    >
                      <Hoverable>
                        {isHovered => (
                          <Text
                            style={{
                              textAlign: "center",
                              color: isHovered == true ? "#009B1A" : "#000",
                              fontFamily: ConstantFontFamily.MontserratBoldFont
                            }}
                          >
                            Settings
                          </Text>
                        )}
                      </Hoverable>
                    </MenuOption>
                    <MenuOption
                      onSelect={() => this.props.navigation.navigate("analytics")}
                    >
                      <Hoverable>
                        {isHovered => (
                          <Text
                            style={{
                              textAlign: "center",
                              color: isHovered == true ? "#009B1A" : "#000",
                              fontFamily: ConstantFontFamily.MontserratBoldFont
                            }}
                          >
                            Analytics
                          </Text>
                        )}
                      </Hoverable>
                    </MenuOption>
                    <MenuOption onSelect={() => this.logout()}>
                      <Hoverable>
                        {isHovered => (
                          <Text
                            testID="SignOut"
                            style={{
                              textAlign: "center",
                              color: isHovered == true ? "#009B1A" : "#000",
                              fontFamily: ConstantFontFamily.MontserratBoldFont
                            }}
                          >
                            Sign Out
                          </Text>
                        )}
                      </Hoverable>
                    </MenuOption>
                  </MenuOptions>
                </Menu>
                              )}

          </View>
        ) : (

            <View>
              {this.props.getScreenLoadingStatus ? null : (
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    marginLeft: 10,
                    display: Dimensions.get('window').width <= 750 ? 'none' : 'flex'
                  }}
                >
                  {//this.props.getCurrentDeviceWidthAction > 950 &&
                    this.props.loginStatus == 0 && (
                      <Button
                        title="Sign Up"
                        titleStyle={{
                          fontSize: 14,
                          fontFamily: ConstantFontFamily.MontserratBoldFont,
                          color: Dimensions.get('window').width <= 750 ? "#000" : "#fff",
                        }}
                        buttonStyle={{
                          backgroundColor: Dimensions.get('window').width <= 750 ? "#f4f4f4" : "#000",
                          borderColor: Dimensions.get('window').width <= 750 ? "#f4f4f4" : "#fff",
                          borderRadius: 6,
                          borderWidth: 1,
                          alignSelf: "center",
                          height: 30,
                          paddingHorizontal: 10
                        }}
                        containerStyle={{
                          alignSelf: "center",
                          marginRight: 10
                        }}
                        onPress={() => this.inviteSignHandle()}
                      />
                    )}

                  <Button
                    title="Login"
                    titleStyle={{
                      fontSize: 14,
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: Dimensions.get('window').width <= 750 ? "#000" : "#fff",
                    }}
                    buttonStyle={{
                      backgroundColor: Dimensions.get('window').width <= 750 ? "#f4f4f4" : "#000",
                      borderColor: Dimensions.get('window').width <= 750 ? "#f4f4f4" : "#fff",
                      borderRadius: 6,
                      borderWidth: 1,
                      alignSelf: "center",
                      width: 80,
                      height: 30,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                    containerStyle={{
                      alignSelf: "center"
                    }}
                    testID="welcome"
                    onPress={this.loginHandle}
                  />
                </View>
              )}
            </View>
          )}
        {/* {Dimensions.get('window').width <= 750 &&
              <Icon
              color={Dimensions.get('window').width <= 750 ? "#fff" : "#fff"}
              iconStyle={{ marginRight: 10 }}
              onPress={() => this.props.leftPanelModalFunc(true)}
              name="bars"
              type="font-awesome"
              size={22}
            />
            } */}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getLoginModalStatus: state.LoginModalReducer.get("modalStatus"),
  getSignUpModalStatus: state.SignUpModalReducer.get("modalStatus"),
  getUsernameModalStatus: state.UsernameModalReducer.get("modalStatus"),
  getResetPasswordModalStatus: state.ResetPasswordModalReducer.get(
    "modalStatus"
  ),
  getVerifyEmailModalStatus: state.VerifyEmailModalReducer.get("modalStatus"),
  getShareLinkModalStatus: state.ShareLinkModalReducer.get("modalStatus"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getFeedReportModalStatus: state.FeedReportModalReducer.get("modalStatus"),
  getScreenLoadingStatus: state.ScreenLoadingReducer.get("modalStatus"),
  getUserApproach: state.UserApproachReducer.get("setUserApproach"),
  searchBarStatus: state.AdminReducer.get("searchBarStatus"),
  getLoginButtonText: state.AdminReducer.get("loginButtonText"), 
  getsearchBarStatus: state.AdminReducer.get("searchBarOpenStatus"),  
});

const mapDispatchToProps = dispatch => ({
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setInviteSignUpModalStatus: payload =>
    dispatch(setInviteSIGNUPMODALACTION(payload)),
  setUsernameModalStatus: payload => dispatch(setUSERNAMEMODALACTION(payload)),
  setResetPasswordModalStatus: payload =>
    dispatch(setRESETPASSWORDMODALACTION(payload)),
  setVerifyEmailModalStatus: payload =>
    dispatch(setVERIFYEMAILMODALACTION(payload)),
  setShareLinkModalStatus: payload =>
    dispatch(setSHARELINKMODALACTION(payload)),
  setFeedReportModalStatus: payload =>
    dispatch(setFEEDREPORTMODALACTION(payload)),
  changeLoginStatus: payload => dispatch(setLoginStatus(payload)),
  changeAdminStatus: payload => dispatch(setAdminStatus(payload)),
  setUserApproachAction: payload => dispatch(setUserApproachAction(payload)),
  setScreenLoadingModalAction: payload =>
    dispatch(setScreenLoadingModalAction(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingUsers: payload => dispatch(getTrendingUsers(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  getTrendingExternalFeeds: payload =>
    dispatch(getTrendingExternalFeeds(payload)),
  setSearchBarStatus: payload =>
    dispatch({ type: "SEARCH_BAR_STATUS", payload }),
  resetLoginUserDetails: payload =>
    dispatch({ type: "LOGIN_USER_DETAILS_RESET", payload }),
  resetUserProfileDetails: payload =>
    dispatch({ type: "USER_PROFILE_DETAILS_RESET", payload }),
  setMessageModalStatus: payload =>
    dispatch({ type: "MESSAGEMODALSTATUS", payload }),
  setLoginButtonText: payload =>
    dispatch({ type: "SET_LOGIN_BUTTON_TEXT", payload }),
  setInviteUserDetail: payload =>
    dispatch({ type: "SET_INVITE_USER_DETAIL", payload }),
  setGoogleLogin: payload => dispatch({ type: "SET_GOOGLE_LOGIN", payload }),
  leftPanelModalFunc: payload => dispatch({ type: 'LEFT_PANEL_OPEN', payload }),
  searchOpenBarStatus : payload => dispatch({type : "SEARCHBAR_STATUS", payload})
});

const HeaderRightWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(HeaderRight);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  HeaderRightWrapper
);

const styles = StyleSheet.create({
  actionButtonIcon: {
    backgroundColor: "#000",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 30
  }
});