import React, { Component } from "react";
import ConstantFontFamily from "../constants/FontFamily";
import {
    Animated,
    Dimensions,
    Image,
    ImageBackground,
    Platform,
    ScrollView,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    View,
    FlatList, AsyncStorage
} from "react-native";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";
import {
    SearchClikVariables,
    SearchUserVariables
} from "../graphqlSchema/graphqlVariables/SearchVariables";
import applloClient from "../client";
import { fromJS, List } from "immutable";
import {
    SearchClikMutation,
    SearchUserMutation
} from "../graphqlSchema/graphqlMutation/SearchMutation";
import {
    UserFollowVariables,
    UserUnfollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import {
    UserFollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { Hoverable } from "react-native-web-hooks";

class SearchInputComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            topic: '',
            clik: "",
            TopicList: [],
            ClikList: [],
            selectedCliks: {},
            selectedTopics: [],
            SelectedItem: {},
            FollowUserList:[]
        };
    }

    getFollowUserList = items => {
        let FollowUserList = [];
        items.map(async (item) => {            
            let index = 0;
            index = this.props.getUserFollowUserList.findIndex(
                i =>{
                    //console.log(i.getIn(["user", "username"]).replace("user:", "") , item.getIn(["topic", "username"]));
                    return i.getIn(["user", "username"]).replace("user:", "") == item.getIn(["topic", "username"]) &&
                    item.getIn(["topic", "username"]).replace("user:", "")
           
                }
                    );            
            if(index != -1){
                FollowUserList.push(item)
            }
        })
        this.setState({FollowUserList:FollowUserList})
    }

    customRenderTopicSuggestion = async value => {
        //console.log(value);
        let topicsearchData = [];
        let cliksearchData = [];
        this.setState({ topic: value, clik: value });
        if (value.charAt(0) == "@") {
            SearchUserVariables.variables.prefix = value;
            applloClient
                .query({
                    query: SearchUserMutation,
                    ...SearchUserVariables,
                    fetchPolicy: "no-cache"
                })
                .then(res => {
                    res.data.search.users.forEach(async (element, index) => {
                        await topicsearchData.push({ topic: element });

                    });
                    //console.log(element);
                    this.getFollowUserList(fromJS(topicsearchData))
                    this.setState({ TopicList: fromJS(topicsearchData) });
                });
        } else if (value.charAt(0) == "#") {
            SearchClikVariables.variables.prefix = value;
            applloClient
                .query({
                    query: SearchClikMutation,
                    ...SearchClikVariables,
                    fetchPolicy: "no-cache"
                })
                .then(res1 => {
                    res1.data.search.cliks.forEach(async (element, index) => {
                        await cliksearchData.push({ clik: element });
                    });
                    this.setState({ ClikList: fromJS(cliksearchData) });
                });
        } else if (value == "") {
            this.setState({ TopicList: [], ClikList: [] });
        } else {
            SearchUserVariables.variables.prefix = value;
            applloClient
                .query({
                    query: SearchUserMutation,
                    ...SearchUserVariables,
                    fetchPolicy: "no-cache"
                })
                .then(res => {
                    res.data.search.users.forEach(async (element, index) => {
                        await topicsearchData.push({ topic: element });

                    });
                    //console.log('--------------------------->',fromJS(topicsearchData) );
                    this.setState({ TopicList: fromJS(topicsearchData) });
                    this.getFollowUserList(fromJS(topicsearchData))
                });
            SearchClikVariables.variables.prefix = value;
            applloClient
                .query({
                    query: SearchClikMutation,
                    ...SearchClikVariables,
                    fetchPolicy: "no-cache"
                })
                .then(res1 => {
                    res1.data.search.cliks.forEach(async (element, index) => {
                        await cliksearchData.push({ clik: element });
                    });
                    this.setState({ ClikList: fromJS(cliksearchData) });
                });
        }
    };

    handleClikSelectInput = clik => {
        //console.log(clik);
        if (Object.values(this.state.selectedCliks).length <3) {
            //let index = this.state.selectedCliks.findIndex(i => i.name == clik.name);
            if (this.state.selectedCliks.name == clik.name) {
                alert("clik name already selected");
            } else {
                this.setState({
                     selectedCliks: Object.assign({}, clik),
                    clik: "",
                    topic: "",
                    ClikList: [],
                    TopicList: []
                });
            }
        } else {
            alert("You can only choose Maximum 3 Cliks to Tag");
            this.setState({
                clik: ""
            });
        }
    };

    componentDidUpdate = async (prevProps, prevState) => {
        //console.log( (prevState.selectedCliks != this.state.selectedCliks && prevState.selectedCliks.name != this.state.selectedCliks.name) || prevState.selectedTopics.length != this.state.selectedTopics.length);
        if ( (prevState.selectedCliks != this.state.selectedCliks && prevState.selectedCliks.name != this.state.selectedCliks.name) || prevState.selectedTopics.length != this.state.selectedTopics.length) {
            let newObj = Object.assign([], this.state.selectedTopics);
            newObj.selectedCliks = Object.assign({}, this.state.selectedCliks)
            newObj.selectedUser = Object.assign([], newObj.selectedUser, this.state.selectedTopics)
            this.props.click(newObj)
        }
    }

    handleTopicSelectInput = topic => {
        if (this.state.selectedTopics.length < 3) {
            let index = this.state.selectedTopics.findIndex(i => i.name == topic.name);
            if (index != -1) {
                alert("topic name already selected");
            } else {
                this.setState({
                    selectedTopics: this.state.selectedTopics.concat([topic]),
                    topic: "",
                    TopicList: [],
                    ClikList: []
                });
            }
        } else {
            alert("You can only choose Maximum 3 Topics to Tag");
            this.setState({
                topic: ""
            });
        }
    };    

    render() {

        return (
            <View style={{ flexDirection: "column", }}>
                <View style={{
                    width: "100%",
                    flex: 1,
                    backgroundColor: "#fff",
                    justifyContent: "center",
                    flexDirection: "row",
                    alignItems: "center"
                }}>
                    <Icon
                        iconStyle={{
                            position: "absolute",
                            left: 20,
                            color: 'black'
                        }}
                        name="search"
                        size={20}
                        type="font-awesome"
                    />
                    <TextInput
                        value={this.state.topic}
                        autoFocus={false}
                        placeholder="Search clicks and users"
                        onChangeText={topic =>
                            this.customRenderTopicSuggestion(topic)
                        }
                        style={{
                            height: 34,
                            padding: 5,
                            marginVertical: 5,
                            width: "95%",
                            borderColor: "#e1e1e1",
                            borderWidth: 1,
                            borderRadius: 20,
                            backgroundColor: "#fff",
                            marginHorizontal: 10,
                            fontFamily: ConstantFontFamily.defaultFont,
                            fontWeight: "bold",
                            paddingLeft: 35,
                            outline: "none",
                            color: 'grey'
                        }}
                    />
                    <Icon
                        iconStyle={{
                            position: "relative",
                            left: -3,
                            color: 'grey',
                            cursor: 'pointer'
                        }}
                        name="times"
                        size={20}
                        type="font-awesome"
                        onPress={() => {
                            //console.log(this.props.click);
                            this.props.close(false)
                        }
                        }
                    />
                </View>
                <View style={{ width: 260, maxHeight: 250, overflowY: 'auto', minHeight: 150, marginVertical: 10 }}>
                    {this.state.topic.length > 0 && (
                        <View style={{ minHeight: 100 }}>
                            <ScrollView
                                nestedScrollEnabled
                                showsVerticalScrollIndicator={false}
                            >
                                {this.state.FollowUserList.map((item, index) => {
                                    return (
                                        <View
                                            key={item.name}
                                            style={{
                                                //backgroundColor: "#FEFEFA",
                                                width: "100%",
                                                //padding: 5
                                            }}
                                        >
                                            <Hoverable>
                                                {isHovered => (
                                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                        {item.getIn(["topic", "profile_pic"]) ? (<Image
                                                            source={item.getIn(["topic", "profile_pic"])}
                                                            style={{
                                                                width: 36,
                                                                height: 36,
                                                                borderRadius: 20,
                                                                // borderWidth: 1,
                                                                // borderColor: "#e1e1e1",
                                                                marginRight: 5,
                                                                //position: 'relative',
                                                                //top: '-16%',
                                                                //left: '25%'
                                                            }}
                                                        />) : (<Image
                                                            source={require("../assets/image/default-image.png")}
                                                            style={{
                                                                width: 36,
                                                                height: 36,
                                                                borderRadius: 20,
                                                                marginRight: 5,
                                                            }}
                                                        />)}
                                                        <Text
                                                            style={{
                                                                width: "100%",
                                                                color: 'grey',
                                                                fontSize: 15,
                                                                fontWeight: "bold",
                                                                fontFamily:
                                                                    ConstantFontFamily.MontserratBoldFont,
                                                                textDecorationLine:
                                                                    isHovered == true ? "underline" : "none"
                                                            }}
                                                            onPress={() =>
                                                                this.handleTopicSelectInput(
                                                                    { name: item.getIn(["topic", "username"]), image: item.getIn(["topic", "profile_pic"]) ? item.getIn(["topic", "profile_pic"]) : null }
                                                                )
                                                            }
                                                        >
                                                            @{item.getIn(["topic", "username"]).toLowerCase()}
                                                        </Text>
                                                    </View>

                                                )}
                                            </Hoverable>
                                        </View>
                                    );
                                })}

                                {this.state.ClikList.map((item, index) => {
                                    let indexx = this.props.getUserFollowCliksList.findIndex(
                                        i =>
                                            i.getIn(["clik", "name"]) ==
                                            item.getIn(["clik", "name"])
                                    );
                                    if (
                                        indexx != -1 &&
                                        (this.props.getUserFollowCliksList.getIn([
                                            index,
                                            "member_type"
                                        ]) == "SUPER_ADMIN" ||
                                            this.props.getUserFollowCliksList.getIn([
                                                index,
                                                "member_type"
                                            ]) == "ADMIN" ||
                                            this.props.getUserFollowCliksList.getIn([
                                                index,
                                                "member_type"
                                            ]) == "MEMBER")
                                    ) {
                                        return (
                                            <View
                                                key={item.name}
                                                style={{
                                                    backgroundColor: "#FEFEFA",
                                                    width: "100%",
                                                    padding: 5
                                                }}
                                                onPress={() =>
                                                    this.handleClikSelectInput(
                                                        { name: item.getIn(["clik", "name"]), image: item.getIn(["clik", "icon_pic"]) }
                                                    )
                                                }
                                            >
                                                <Hoverable>
                                                    {isHovered => (
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}
                                                        >

                                                            <Image
                                                                //source={require("../assets/image/default-image.png")}
                                                                source={item.getIn(["clik", "icon_pic"])}
                                                                style={{
                                                                    width: 36,
                                                                    height: 36,
                                                                    borderRadius: 5,
                                                                    // borderWidth: 1,
                                                                    // borderColor: "#e1e1e1",
                                                                    marginRight: 5,
                                                                    //position: 'relative',
                                                                    //top: 0,
                                                                    //left: 0
                                                                }}
                                                            />
                                                            <Text
                                                                style={{
                                                                    alignSelf: 'flex-start',
                                                                    padding: 5,
                                                                    backgroundColor: "#E8F5FA",
                                                                    borderRadius: 10,
                                                                    maxWidth: "100%",
                                                                    textAlign: "left",
                                                                    color: "#4169e1",
                                                                    fontSize: 15,
                                                                    fontWeight: "bold",
                                                                    fontFamily:
                                                                        ConstantFontFamily.MontserratBoldFont,
                                                                    textDecorationLine:
                                                                        isHovered == true ? "underline" : "none"
                                                                }}
                                                            >
                                                                #{item.getIn(["clik", "name"])}
                                                            </Text>
                                                        </View>
                                                    )}
                                                </Hoverable>
                                            </View>
                                        );
                                    }
                                })}


                            </ScrollView>
                        </View>
                    )}

                    <View style={{ alignSelf: 'flex-start' }}>
                        {this.props.getUserFollowCliksList
                            .slice(0, 3)
                            .map((item, i) => {
                                return (
                                    <TouchableOpacity
                                        key={i}
                                        onPress={() =>
                                            this.handleClikSelectInput(
                                                { name: item.getIn(["clik", "name"]), image: item.getIn(["clik", "icon_pic"]) }
                                            )
                                        }
                                        style={{
                                            marginLeft: 5,
                                            margin: 5,
                                            alignSelf: 'flex-start',
                                            padding: 5,
                                            //backgroundColor: "#E8F5FA",
                                            //borderRadius: 10,
                                            //maxWidth: "100%"
                                        }}
                                    >
                                        <Hoverable>
                                            {isHovered => (
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}
                                                >

                                                    <Image
                                                        //source={require("../assets/image/default-image.png")}
                                                        source={item.getIn(["clik", "icon_pic"])}
                                                        style={{
                                                            width: 36,
                                                            height: 36,
                                                            borderRadius: 5,
                                                            // borderWidth: 1,
                                                            // borderColor: "#e1e1e1",
                                                            marginRight: 5,
                                                            //position: 'relative',
                                                            //top: 0,
                                                            //left: 0
                                                        }}
                                                    />
                                                    <Text
                                                        style={{
                                                            alignSelf: 'flex-start',
                                                            padding: 5,
                                                            backgroundColor: "#E8F5FA",
                                                            borderRadius: 10,
                                                            maxWidth: "100%",
                                                            textAlign: "left",
                                                            color: "#4169e1",
                                                            fontSize: 15,
                                                            fontWeight: "bold",
                                                            fontFamily:
                                                                ConstantFontFamily.MontserratBoldFont,
                                                            textDecorationLine:
                                                                isHovered == true ? "underline" : "none"
                                                        }}
                                                    >
                                                        #{item.getIn(["clik", "name"])}
                                                    </Text>
                                                </View>
                                            )}
                                        </Hoverable>
                                    </TouchableOpacity>
                                );
                            })}
                    </View>

                    <View style={{ alignSelf: 'flex-start' }}>
                        {this.props.getUserFollowUserList
                            .slice(0, 3)
                            .map((item, i) => {
                                return (
                                    <TouchableOpacity
                                        onPress={() =>
                                            this.handleTopicSelectInput(
                                                { name: item.getIn(["user", "username"]), image: item.getIn(["user", "profile_pic"]) }
                                            )
                                        }
                                        style={{
                                            marginTop: 10,
                                            marginLeft: 5,
                                            alignSelf: 'flex-start',
                                            padding: 5,
                                            backgroundColor: '#fff',
                                            borderRadius: 10
                                        }}
                                        key={i}
                                    >
                                        <Hoverable>
                                            {isHovered => (
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <Image
                                                        source={item.getIn(["user", "profile_pic"])}
                                                        style={{
                                                            width: 36,
                                                            height: 36,
                                                            borderRadius: 20,
                                                            // borderWidth: 1,
                                                            // borderColor: "#e1e1e1",
                                                            marginRight: 5,
                                                            //position: 'relative',
                                                            //top: '-16%',
                                                            //left: '25%'
                                                        }}
                                                    />
                                                    <Text
                                                        style={{
                                                            width: "100%",
                                                            color: 'grey',
                                                            fontSize: 15,
                                                            fontWeight: "bold",
                                                            fontFamily:
                                                                ConstantFontFamily.MontserratBoldFont,
                                                            textDecorationLine:
                                                                isHovered == true ? "underline" : "none"
                                                        }}
                                                    >
                                                        @{item.getIn(["user", "username"]).toLowerCase()}
                                                    </Text>
                                                </View>

                                            )}
                                        </Hoverable>
                                    </TouchableOpacity>
                                );
                            })}
                    </View>
                </View>

            </View>
        );
    }
}


const mapStateToProps = state => {
    return ({
        term: state.ScreenLoadingReducer.get("setTerm"),
        getUserFollowCliksList: state.LoginUserDetailsReducer.get(
            "userFollowCliksList"
        ),
        getUserFollowUserList: state.LoginUserDetailsReducer.get("userFollowUserList")
            ? state.LoginUserDetailsReducer.get("userFollowUserList")
            : List(),
    })
};

const mapDispatchToProps = dispatch => ({
    setTerm: payload => dispatch({ type: "SET_TERM", payload })
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
    SearchInputComponent
);

