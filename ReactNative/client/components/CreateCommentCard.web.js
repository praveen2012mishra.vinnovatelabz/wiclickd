import React, { useEffect, useState, useRef } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Dimensions,
  Image, Alert, AsyncStorage
} from "react-native";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger
} from "react-native-popup-menu";
import RNPickerSelect from "react-native-picker-select";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import applloClient from "../client";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import { CreateCommentMutation } from "../graphqlSchema/graphqlMutation/LikeContentMutation";
import { CreateCommentVariables } from "../graphqlSchema/graphqlVariables/LikeContentVariables";
import { capitalizeFirstLetter } from "../library/Helper";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import { Icon, Button } from "react-native-elements";
import ButtonStyle from "../constants/ButtonStyle";
import {
  EditorState,
  RichUtils,
  convertToRaw,
  convertFromHTML,
  ContentState
} from "draft-js";
//import 'draft-js/dist/Draft.css';
import { stateToHTML } from "draft-js-export-html";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
//import '../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import "../assets/react-draft.css";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import SearchInputComponent from './SearchInputComment'
import NavigationService from "../library/NavigationService";

function myBlockStyleFn(contentBlock) {
  const type = contentBlock.getType();
  if (type === "blockquote") {
    return styles.blockQuote;
  }
}

const CreateCommentCard = props => {
  let textStyle = "#959FAA";
  const [title, settitle] = useState("");
  const [item, setitem] = useState([]);
  const [click, setclick] = useState(null);
  const [showtopictooltip, setshowtopictooltip] = useState(false);
  const [getBorderColor, setBorderColor] = useState("#e8e8e8");
  const [editorState, onChange] = useState(EditorState.createEmpty());
  const [placeholderState, setPlaceholderState] = useState(true);
  const [colorState, setColorState] = useState([
    { type: "UNDERLINE", state: false },
    { type: "BOLD", state: false },
    { type: "ITALIC", state: false },
    { type: "blockquote", state: false },
    { type: "code-block", state: false },
    { type: "unordered-list-item", state: false }
  ]);
  const [titleContent, setTitleContent] = useState("characters more");
  const [getSubmitData, setSubmitData] = useState("");
  const [getLockStatus, SetLockStatus] = useState(false);
  const [MenuHover, setMenuHover] = useState(false);
  const [getDisplayInputStatus, setDisplayInputStatus] = useState(false);
  const [getSelectedItem, setSelectedItem] = useState([]);
  const [selectedUser, setSelectedUser] = useState([]);
  const [selectedCliks, setSelectedCliks] = useState({});
  const [opened, setOpened] = useState(false);
  const [getFeedItem, setFeedItem] = useState('');

  useEffect(() => {
    if (props.parent_content_id != getFeedItem) {
      setFeedItem(props.parent_content_id);
      setSelectedCliks({});
      setSelectedUser([])
      //console.log(window.location.href);
      //window.location.href=window.location.href+'/'+props.parent_content_id
      //console.log(window.location.href);
    }
    //console.log(props.parent_content_id,getFeedItem);
    // if(props.parent_content_id && getFeedItem){
    //   let id=props.parent_content_id.replace('Post:','')
    //   console.log(window.location.href+id);
    //   //window.location.href=window.location.href+id
    //   //console.log(window.location.href);
    //   NavigationService.navigate("feedId",{id:id});
    //   props.setFeedId(id);
    // }
    // props.setFeedId('');
  }, [props.parent_content_id]);

  const onEditorStateChange = editorState => {
    if (props.loginStatus == 0) {
      props.setLoginModalStatus(true);
      settitle("");
    } else {
      if (editorState.getCurrentContent().getPlainText("\u0001").length == 0) {
        setPlaceholderState(false);
      }
      if (title.length < 1999) {
        onChange(editorState);
        settitle(editorState.getCurrentContent().getPlainText("\u0001"));
        title.length < 140
          ? setTitleContent("characters more")
          : setTitleContent("characters left");
        setSubmitData(draftToHtml(convertToRaw(editorState.getCurrentContent())));
      }

      else {
        //onChange();
        //settitle(editorState.getCurrentContent().getPlainText("\u0001"));
        title.length < 140
          ? setTitleContent("characters more")
          : setTitleContent("characters left");
        //setSubmitData(draftToHtml(convertToRaw(editorState.getCurrentContent())));
        // onChange(
        //   EditorState.push(
        //     editorState,
        //     ContentState.createFromText("")
        //   )
        // );
      }

    }
  };

  const submitComment = () => {
    //console.log(title);
    CreateCommentVariables.variables.text = getSubmitData;
    CreateCommentVariables.variables.parent_content_id =
      props.parent_content_id;

    if (props.initial != "main") {
      CreateCommentVariables.variables.clik = props.topComment.clik
        ? props.topComment.clik
        : null;
    } else {
      if (click == null) {
        CreateCommentVariables.variables.clik = null;
      } else if (click == "Everyone" || click == "all") {
        CreateCommentVariables.variables.clik = null;
      } else {
        CreateCommentVariables.variables.clik = click
          .toLowerCase()
          .replace("#", "");
      }
    }
    applloClient
      .mutate({
        mutation: CreateCommentMutation,
        ...CreateCommentVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        settitle("");
        setclick(null);
        let prevCommentList = props.PostCommentDetails;
        let parentId = props.parent_content_id;
        let createData = {
          node: { ...res.data.comment_create.comment },
          comments: {
            edges: []
          }
        };
        if (parentId.startsWith("Comment")) {
          const updateArray = updateNestedArray(
            prevCommentList,
            parentId,
            createData
          );
          props.setPostCommentDetails(updateArray);
        } else {
          props.setPostCommentDetails([createData, ...prevCommentList]);
        }

        //----------------------------------------------------------------------------------------------------------
      })
      .catch(e => {
        console.log(e.message.toLowerCase().replace("graphql error: ", ""));
      });
    setPlaceholderState(true);
    setSelectedCliks({});
    setSelectedUser([])
    //}
  };

  useEffect(() => {
    document.addEventListener("touchstart", function () { }, { passive: false });
    let data = [
      {
        label: "Everyone",
        value: "all",
        key: -1
      }
    ];
    if (
      props.profileData &&
      props.profileData.getIn(["my_users", "0", "cliks_followed"]).size > 0
    ) {
      props.profileData &&
        props.profileData
          .getIn(["my_users", "0", "cliks_followed"])
          .map((item, index) => {
            if (
              item.getIn(["member_type"]) == "SUPER_ADMIN" ||
              item.getIn(["member_type"]) == "ADMIN" ||
              item.getIn(["member_type"]) == "MEMBER"
            ) {
              data.push({
                label:
                  "#" + capitalizeFirstLetter(item.getIn(["clik", "name"])),
                value: item.getIn(["clik", "name"]),
                key: index,
                color: "#4169e1"
              });
            }
          });
    }
    setitem(data);
    //setclick(data.length > 0 ? data[0].label : null);  


  }, []);



  useEffect(() => {
    //console.log(getSelectedItem, selectedUser, selectedCliks);
    if (getSelectedItem.selectedUser) {
      let newArray = Object.assign([], selectedUser, getSelectedItem.selectedUser)
      //console.log(newArray);
      setSelectedUser(getSelectedItem.selectedUser)
    }
    if (getSelectedItem.selectedCliks) {
      let newArray = Object.assign({}, getSelectedItem.selectedCliks)

      if (Object.values(getSelectedItem.selectedCliks).length > 0) {
        setOpened(false);
      }
      setSelectedCliks(getSelectedItem.selectedCliks);
      //setclick(getSelectedItem.selectedCliks.name)                     
    }

  }, [getSelectedItem]);

  useEffect(() => {
    if (props.title) {
      //e.current.focus();
    } else {
      //e.current.focus();
    }
  }, [props.title]);

  const goToUserProfile = async username => {
    props.onClose();
    await props.userId({
      username: username,
      type: "feed"
    });
    await props.navigation.navigate("profile", {
      username: username,
      type: "feed"
    });
  };

  const customRenderUserTags = tags => {
    return (
      <View
        style={{
          flexDirection: "row",
          flexWrap: "wrap",
          alignItems: "flex-start",
          backgroundColor: "#fff",
          width: "100%",
          paddingVertical: 2,
          flexDirection: 'column'
        }}
      >
        {tags.map((t, i) => {
          return (
            <View
              key={t.name}
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                marginBottom: 2,
              }}
            >
              {t.image ? (<Image
                source={t.image}
                style={{
                  width: 25,
                  height: 25,
                  borderRadius: 20,
                  marginRight: 5,
                }}
              />) : (<Image
                source={require("../assets/image/default-image.png")}
                style={{
                  width: 25,
                  height: 25,
                  borderRadius: 20,
                  marginRight: 5,
                }}
              />)}
              <Text
                style={{
                  color: "grey",
                  // rootTopics.includes(t.name) == false
                  //   ? "#009B1A"
                  //   : "#FEC236",
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
              >
                @{t.name.toLowerCase()}
              </Text>
              <Icon
                color={'grey'}
                name="times"
                type="font-awesome"
                size={20}
                iconStyle={{ marginLeft: 5, cursor: 'pointer' }}
                onPress={() => {
                  let newArray = Object.assign([], selectedUser);
                  delete newArray[i]
                  setSelectedUser(Object.assign([], newArray))
                }}
              />
            </View>
          );
        })}
      </View>
    );
  };

  const customRenderClikTags = tags => {
    return (
      <TouchableOpacity
        style={{
          flexDirection: "row",
          flexWrap: "wrap",
          alignItems: "flex-start",
          backgroundColor: "#fff",
          width: "100%",
          //paddingVertical: 2,
          marginBottom: 2
        }}
      >
        <View style={{ flexDirection: 'row', alignItems: 'center' }}
        >
          <Image
            //source={require("../assets/image/default-image.png")}
            source={tags.image}
            style={{
              width: 25,
              height: 25,
              borderRadius: 5,
              marginRight: 5,
            }}
          />
          <View
            style={{ flexDirection: 'row', backgroundColor: "#E8F5FA", alignItems: 'center', cursor: 'pointer', borderRadius: 10, paddingHorizontal: 5 }}
          >
            <Text
              style={{
                alignSelf: 'flex-start',
                padding: 2,
                //maxWidth: "100%",
                //textAlign: "left",
                color: "#4169e1",
                //fontSize: 15,
                fontWeight: "bold",
                fontFamily:
                  ConstantFontFamily.MontserratBoldFont,
                // textDecorationLine:
                //     isHovered == true ? "underline" : "none"
              }}
            >
              #{tags.name}
            </Text>
            <Icon
              color={"#4C82B6"}
              name="times"
              type="font-awesome"
              size={20}
              iconStyle={{ marginLeft: 5, cursor: 'pointer' }}
              onPress={() => {
                setSelectedCliks({})
                setclick(null)
              }}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <>
      <View
        style={[ButtonStyle.shadowStyle, {
          width: Dimensions.get("window").width <= 750 ? "95%" : '98%',
          backgroundColor: "#fff",
          // borderWidth: props.initial == "main" ? 1 : 0,
          borderColor: "#dedede",
          borderRadius: 20,
          marginBottom: props.initial == "main" ? 10 : 0,
          marginHorizontal: Dimensions.get("window").width <= 750 ? 10 : 0,
          // marginLeft:  Dimensions.get('window').width>=750  ? 3 : 0,  
          // marginTop:  Dimensions.get('window').width>=750  && 2, 
        }]}
      >
        <View
          style={{
            flexDirection: "row",
            marginTop: props.initial == "main" ? 10 : 0,
            marginBottom: props.initial == "main" ? 10 : 0,
            flexDirection: "row",
            marginLeft: 10
          }}
        >
          {props.initial == "main" &&
            item != null &&
            item.length > 0 &&
            item[0] != "" && (
              <View
                style={{
                  flexDirection: "row",
                  width: "100%",
                  position: 'relative',
                  justifyContent: 'space-between'
                }}
              >
                <View style={{
                  flexDirection: "row",
                  width: "50%",
                  //alignItems:'center'
                }}>
                  <View><Text
                    style={{
                      alignSelf: "center",
                      marginRight: 10,
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      fontSize: 15,
                      marginTop: 3
                    }}
                  >
                    Discuss With:
                </Text></View>

                  <View style={{ display: !props.loginStatus && 'none' }} >
                    {props.loginStatus && (<TouchableOpacity
                    // onMouseEnter={() => setMenuHover(true)}
                    // onMouseLeave={() => setMenuHover(false)}                    
                    //
                    >
                      {selectedUser && (<View
                        style={{
                          //marginVertical: 5,
                          maxHeight: 195,
                          overflowY: 'auto',
                        }}
                      >
                        {selectedUser.length > 0 &&
                          customRenderUserTags(selectedUser)}
                        {Object.values(selectedCliks).length > 0 &&
                          customRenderClikTags(selectedCliks)}
                      </View>)}
                      <Menu opened={opened}>
                        <MenuTrigger onPress={() => {
                          setOpened(true)
                          // if (selectedUser.length > 0 || selectedCliks.length > 0) {
                          //   setDisplayInputStatus(false)
                          // }
                        }}>
                          {!selectedCliks.name && (<Icon
                            size={25}
                            name="ios-add-circle-outline"
                            type="ionicon"
                            iconStyle={styles.actionButtonIcon}
                            color="grey"
                            underlayColor="#fff"
                          />)}
                        </MenuTrigger>
                        <MenuOptions
                          optionsContainerStyle={{
                            borderRadius: 6,
                            borderWidth: 1,
                            borderColor: "#e1e1e1",
                            shadowColor: "transparent",
                          }}
                          customStyles={{
                            optionsContainer: {
                              minHeight: 50,
                              minWidth: 150,
                              marginTop: 30,
                              marginLeft: -5,
                              width: 260
                            }
                          }}
                          onSelect={(ele) => console.log(ele)} //{() =>!getDisplayInputStatus}
                        >
                          {/* {getDisplayInputStatus && ( */}
                          <SearchInputComponent
                            close={(ele) => setOpened(ele)}
                            click={async (ele) => {
                              //let getSelectedItem = await AsyncStorage.getItem('getSelectedItem');
                              //let parsed = JSON.parse(getSelectedItem);
                              setSelectedItem(ele)
                              setclick(ele.selectedCliks.name)
                              //console.log(ele);
                            }} />
                          {/* )} */}
                        </MenuOptions>
                      </Menu>
                    </TouchableOpacity>)}
                  </View>
                </View>
                <View style={{
                  flexDirection: "row",
                  //width: "50%",
                  justifyContent: 'flex-end',
                  // left: 165,
                  alignItems: 'baseline',
                  display: !props.loginStatus && 'none',
                  paddingTop: 4,
                  marginRight: 15
                  //alignSelf: 'center'
                }}
                >
                  {getLockStatus && (
                    <View><Icon
                      size={20}
                      name="lock"
                      type="font-awesome"
                      iconStyle={{ cursor: 'pointer' }}
                      color="grey"
                      underlayColor="#fff"
                      onPress={() => {
                        //console.log(getLockStatus, '----------------------->');
                        !getLockStatus ? alert('Discussion will be closed to users and cliks that were invited') : alert('Discussion will be open to everyone');
                        SetLockStatus(false)
                      }}
                    />
                    </View>
                  )}
                  {!getLockStatus && (
                    <View>
                      <Icon
                        size={20}
                        name="unlock-alt"
                        type="font-awesome"
                        iconStyle={{ cursor: 'pointer' }}
                        color="grey"
                        underlayColor="#fff"
                        onPress={() => {
                          !getLockStatus ? alert('Discussion will be closed to users and cliks that were invited') : alert('Discussion will be open to everyone');
                          SetLockStatus(true)
                        }}
                      />
                    </View>)}

                </View>
              </View>
            )}
        </View>
        {Dimensions.get('window').width <= 750 && title.length < 2000 && (<Text
          style={{
            color: title.length < 140 ? "#de5246" : "#009B1A",
            fontSize: 13,
            fontFamily: ConstantFontFamily.defaultFont,
            alignSelf: "flex-end",
            marginRight: 10
          }}
        >
          {title
            ? title.length < 140
              ? `${140 - title.length} ${titleContent}`
              : `${2000 - title.length} ${titleContent}`
            : ""}
        </Text>)}
        <View
          style={{
            height: Platform.OS != "web" ? hp("25%") : null
          }}
        >
          <View style={{ flex: 1 }}>
            <View
              onFocus={() => {
                return setBorderColor("black");
              }}
              onBlur={() => {
                return setBorderColor("#c5c5c5");
              }}
              style={{
                borderWidth: 1,
                borderColor: `${getBorderColor}`,
                borderRadius: 6,
                ...Platform.select({
                  web: {
                    marginHorizontal: 10,
                    marginTop: 10
                  },
                  android: {
                    marginTop: 10
                  },
                  ios: {
                    marginTop: 10
                  }
                })
              }}
            >
              <View>
                <View style={{ flexDirection: "column" }}>
                  <Editor
                    editorState={editorState}
                    wrapperClassName="demo-wrapper"
                    editorClassName="demo-editor"
                    onEditorStateChange={onEditorStateChange}
                    onBlur={() => {
                      if (
                        editorState.getCurrentContent().getPlainText("\u0001")
                          .length == 0
                      ) {
                        setPlaceholderState(true);
                      }
                    }}
                    onChange={() => {
                      document.querySelector(".demo-editor").scrollTop =
                        parseInt(
                          document.querySelector(".demo-editor").scrollHeight
                        ) +
                        parseInt(
                          document.querySelector(".demo-editor").scrollHeight
                        );
                      //console.log(editorState.getCurrentContent().getPlainText('\u0001').length);
                      if (
                        editorState.getCurrentContent().getPlainText("\u0001")
                          .length == 0
                      ) {
                        setPlaceholderState(true);
                      }
                    }}
                    editorStyle={{
                      height: "100px",
                      overflowY: title.length > 0 ? "auto" : "unset",
                      fontSize: "13px",
                      color: "#000",
                      fontFamily: ConstantFontFamily.VerdanaFont,
                      paddingLeft: "10px"
                    }}
                    toolbar={{
                      options: ["inline", "blockType", "list", "link"],
                      inline: {
                        options: ["bold", "italic", "underline"]
                      },
                      blockType: {
                        inDropdown: false,
                        options: ["Blockquote", "Code"],
                        className: undefined,
                        component: undefined,
                        dropdownClassName: undefined
                      },
                      list: {
                        options: ["unordered"]
                      },
                      link: {
                        options: ["link"]
                      }
                    }}
                    placeholder={
                      placeholderState
                        ? "Write an insightful comment (longer than 140 characters)."
                        : ""
                    }
                  />
                </View>
              </View>
            </View>
            {Dimensions.get('window').width >= 750 && title.length < 2000 && (
            <Text
                  style={{
                    color: title.length < 140 ? "#de5246" : "#009B1A",
                    fontSize: 13,
                    fontFamily: ConstantFontFamily.defaultFont,
                    marginRight: 10,
                    alignSelf:'flex-end'
                  }}
                >
                  {title
                    ? title.length < 140
                      ? `${140 - title.length} ${titleContent}`
                      : `${2000 - title.length} ${titleContent}`
                    : ""}
                </Text>)}
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                width: "100%",
                // alignItems: "flex-start",
                paddingHorizontal: 10,
                paddingTop: 5,
                alignItems: 'center'
                // zIndex: -1
              }}
            >
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Hoverable>
                  {isHovered => (
                    <TouchableOpacity
                      onPress={() =>
                        goToUserProfile(
                          props.profileData &&
                          props.profileData.getIn([
                            "my_users",
                            "0",
                            "user",
                            "username"
                          ])
                        )
                      }
                    >
                      <View style={{ flexDirection: "row", alignItems: 'center' }}>
                        {props.profileData &&
                          props.profileData.getIn([
                            "my_users",
                            "0",
                            "user",
                            "profile_pic"
                          ]) ? (
                          <Image
                            source={{
                              uri: props.profileData.getIn([
                                "my_users",
                                "0",
                                "user",
                                "profile_pic"
                              ])
                            }}
                            style={{
                              width: 30,
                              height: 30,
                              borderRadius: 18,
                              borderWidth: 1,
                              borderColor: "#e1e1e1",
                              marginRight: 5
                            }}
                          />
                        ) : (
                          <Image
                            source={require("../assets/image/default-image.png")}
                            style={{
                              width: 30,
                              height: 30,
                              borderRadius: 18,
                              borderWidth: 1,
                              borderColor: "#e1e1e1",
                              marginRight: 5
                            }}
                          />
                        )}

                        <Text
                          style={{
                            color: "#6D757F",
                            fontSize: 13,
                            fontFamily: ConstantFontFamily.VerdanaFont,
                            textDecorationLine:
                              isHovered == true ? "underline" : "none",
                            // position: "relative",
                            // top: 8,
                            marginRight: 5
                          }}
                        >
                          {props.profileData &&
                            "@" +
                            props.profileData.getIn([
                              "my_users",
                              "0",
                              "user",
                              "username"
                            ])}
                        </Text>


                      </View>
                    </TouchableOpacity>
                  )}
                </Hoverable>
                

              </View>
              

              <View
                style={{
                  // justifyContent: "flex-end",
                  // alignItems: "center",
                  // alignSelf: "center",
                  // paddingHorizontal: 10,
                  flexDirection: "row"
                }}
              >
                {props.initial != "main" && (
                  <Button
                    title="Cancel"
                    titleStyle={ButtonStyle.cwtitleStyle}
                    buttonStyle={[ButtonStyle.crbackgroundStyle, { paddingHorizontal: 10, paddingVertical: Dimensions.get("window").width <= 750 ? 5 : 6, borderRadius: 5, backgroundColor: "#de5246", borderColor: "#C5C5C5", }]}
                    containerStyle={[
                      ButtonStyle.containerStyle,
                      {
                        marginTop: 0,
                        marginBottom: 5,
                        height: 30
                      }
                    ]}
                    onPress={() => {
                      props.onClose();
                    }}
                  />
                )}

                <Button
                  title="Submit"
                  titleStyle={
                    title.length < 140
                      ? ButtonStyle.cdtitleStyle
                      : ButtonStyle.cwtitleStyle
                  }
                  buttonStyle={
                    title.length < 140
                      ? [ButtonStyle.cdbackgroundStyle, { paddingVertical: Dimensions.get("window").width <= 750 ? 4 : 5, borderRadius: 5 }]
                      : [ButtonStyle.cgbackgroundStyle, , { paddingVertical: Dimensions.get("window").width <= 750 ? 4 : 5, borderRadius: 5 }]
                  }
                  containerStyle={[
                    ButtonStyle.containerStyle,
                    {
                      marginTop: 0,
                      marginBottom: 5,
                      height: 30
                    }
                  ]}
                  disabled={title.length < 140 ? true : false}
                  onPress={() => {
                    submitComment();
                    onChange(
                      EditorState.push(
                        editorState,
                        ContentState.createFromText("")
                      )
                    );
                  }}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    </>
  );
};
const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  PostCommentDetails: state.PostCommentDetailsReducer.get("PostCommentDetails"),
  loginStatus: state.UserReducer.get("loginStatus"),
  getFeedId: state.FeedReportModalReducer.get('setFeedId')
});

const mapDispatchToProps = dispatch => ({
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  clikId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setFeedId: payload => dispatch({ type: "SET_FEED_ID", payload })
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  CreateCommentCard
);

const styleMap = {
  CODE: {
    backgroundColor: "red",
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2
  },
  HIGHLIGHT: {
    //'backgroundColor': 'red',
    //'padding': 5,
    fontFamily: ConstantFontFamily.VerdanaFont,
    // fontSize: 13,
    // lineHeight: 1.5,
    //'fontSize': 18,
    width: "100%",
    borderRadius: 6
  },
  superFancyBlockquote: {
    color: "#999",
    "font-family": "Hoefler Text",
    "font-style": "italic",
    "text-align": "center"
  }
};

const styles = StyleSheet.create({
  actionButtonIcon: {
    backgroundColor: "#fff",
    alignSelf: "flex-start",
    cursor: 'pointer',
    marginLeft: 5
  },
  blockQuote: {
    //borderLeft: '5px solid #eee',
    backgroundColor: "red",
    color: "#666",
    fontFamily: "Hoefler Text",
    //fontStyle: 'italic',
    marginVertical: 16,
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  container: {
    flex: 1,
    backgroundColor: ConstantColors.customeBackgroundColor,
    padding: Platform.OS == "web" ? 10 : 0
  },
  totalFollowCount: {
    marginTop: 15,
    color: "#ffffff",
    alignItems: "center"
  },
  containerFollow: {
    justifyContent: "flex-end",
    marginRight: 10
  },
  TotalcontainerFollow: {
    flexDirection: "row",
    justifyContent: "flex-end",
    marginRight: 12,
    marginBottom: 10
  },
  profileContainer: {
    marginTop: 10
  },
  totalProfileContainer: {
    alignItems: "center",
    justifyContent: "center"
  },
  image: {
    width: "100%",
    height: hp("40%")
  },
  followbtnStyle: {
    height: 30,
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#1A1819",
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 30,
    paddingRight: 30,
    borderColor: "#fff",
    borderWidth: 2,
    borderRadius: 5
  },
  bgContainerProfile: {
    margin: 0,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  usertext: {
    color: "#fff",
    fontSize: 12,
    fontFamily: ConstantFontFamily.defaultFont
  },
  userblacktext: {
    color: "#fff",
    fontSize: 12,
    fontFamily: ConstantFontFamily.defaultFont
  },
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    flexDirection: "row",
    width: 150,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    textAlign: "center",
    textAlignVertical: "center",
    borderRadius: 4,
    borderWidth: 0,
    backgroundColor: "#E8F5FA",
    color: "#4169e1",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  inputAndroid: {
    paddingVertical: 8,
    paddingRight: 20,
    paddingLeft: 10,
    flexDirection: "row",
    width: 150,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    textAlign: "center",
    textAlignVertical: "center",
    borderRadius: 8,
    borderWidth: 0,
    backgroundColor: "#E8F5FA",
    color: "#4169e1",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },

  TextStyle: {
    textAlign: "center"
  }
});
function updateNestedArray(input, parentId, newData) {
  let key, value, outputValue;
  if (input === null || typeof input !== "object") {
    return input;
  }
  outputValue = Array.isArray(input) ? [] : {};
  for (key in input) {
    value = input[key];
    if (
      value !== null &&
      !!value["node"] &&
      !!value["comments"] &&
      value.node.id === parentId
    ) {
      let clone = { ...value };
      value.comments.edges.unshift(newData);
      outputValue[key] = updateNestedArray(clone, parentId, newData);
    } else {
      outputValue[key] = updateNestedArray(value, parentId, newData);
    }
  }
  return outputValue;
}