import React, { Component } from "react";
import ConstantFontFamily from "../constants/FontFamily";
import {
  SearchClikMutation,
  SearchEveryThingMutation,
  SearchFeedMutation,
  SearchTopicMutation,
  SearchUserMutation
} from "../graphqlSchema/graphqlMutation/SearchMutation";
import {
  Animated,
  Dimensions,
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View,
  FlatList
} from "react-native";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";
import NavigationService from "../library/NavigationService";

class SearchInputWeb extends Component {
  constructor(props) {
    super(props);
    this.state = {
      term: "",
      keyEvent: "",
      boxColor: "#000"
    };
  }

  getTopicStar = TopicName => {
    let index = 0;
    index = this.props.getUserFollowTopicList.findIndex(
      i =>
        i
          .getIn(["topic", "name"])
          .toLowerCase()
          .replace("topic:", "") ==
        TopicName.toLowerCase().replace("topic:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowTopicList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  getClikStar = ClikName => {
    let index = 0;
    index = this.props.getUserFollowCliksList.findIndex(
      i =>
        i
          .getIn(["clik", "name"])
          .toLowerCase()
          .replace("clik:", "") == ClikName.toLowerCase().replace("clik:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowCliksList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  favroiteCliks = async cliksId => {
    ClikFollowVariables.variables.clik_id = cliksId;
    ClikFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: ClikFollowMutation,
        ...ClikFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowCliks = async cliksId => {
    ClikUnfollowVariables.variables.clik_id = cliksId;
    applloClient
      .query({
        query: ClikUnfollowMutation,
        ...ClikUnfollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  getUserStar = UserName => {
    let index = 0;
    index = this.props.getUserFollowUserList.findIndex(
      i =>
        i
          .getIn(["user", "username"])
          .toLowerCase()
          .replace("user:", "") == UserName.toLowerCase().replace("user:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowUserList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  getFeedStar = Name => {
    let index = 0;
    index = this.props.getUserFollowFeedList.findIndex(
      i =>
        i
          .getIn(["feed", "name"])
          .toLowerCase()
          .replace("ExternalFeed:", "") ==
        Name.toLowerCase().replace("ExternalFeed:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowFeedList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  favroiteFeed = async Id => {
    FeedFollowVariables.variables.feed_id = Id;
    FeedFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: FeedFollowMutation,
        ...FeedFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowFeed = async Id => {
    FeedUnFollowVariables.variables.feed_id = Id;
    applloClient
      .query({
        query: FeedUnFollowMutation,
        ...FeedUnFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  favroiteTopics = async topicId => {
    TopicFollowVariables.variables.topic_id = topicId;
    TopicFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowTopics = async topicId => {
    TopicUnFollowVariables.variables.topic_id = topicId;
    applloClient
      .query({
        query: TopicUnFollowMutation,
        ...TopicUnFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  followTopics = topicId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    TopicFollowVariables.variables.topic_id = topicId;
    TopicFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  componentDidMount() {
    this.props.setTerm(this.state.term);
    this.props.setKeyEvent(this.state.keyEvent);
    this.props.setDisplayType("");
  }

  handleEvent = e => {
    var code = e.keyCode || e.which;
    this.props.setKeyEvent(code);
    this.setState({ keyEvent: code });
    this.props.setDisplayType(this.props.displayType);
    //this.props.onkeypress1(e);
    if (e.nativeEvent.key == "Enter") {
      NavigationService.navigate("search");
    }
  };

  render() {
    return (
      // <View>
      //   <Animated.View
      //     style={{
      //       // zIndex: 10,
      //       overflow: "hidden",
      //       borderRadius: 15,
      //       borderColor: "#fff",
      //       borderWidth: 1,            
      //       //In web only work
      //       width: Dimensions.get("window").width >= 1100 ? 440 : "92%",          
      //       // position: "relative",
      //       right: Dimensions.get("window").width <= 750 ? 10 : 0,
      //       // right: Dimensions.get("window").width >= 1100 ? 285 : 0
      //     }}
      //   >
      //     <View
      //       style={{
      //         // width: Dimensions.get("window").width >= 1100 ? "100%" : "100%",
      //         flexDirection: "row",
      //         // backgroundColor: "#fff",
      //         // backgroundColor: this.state.boxColor,
      //         backgroundColor: Dimensions.get("window").width <= 1100 ? '#fff' : '#000',
      //         borderRadius: 10,
      //         height: 32
      //       }}
      //     >
      //       {Platform.OS != "web" && (
      //         <TouchableOpacity
      //           style={{
      //             flexDirection: "row",
      //             justifyContent: "flex-start",
      //             alignSelf: "center"
      //           }}
      //           onPress={() => {
      //             let nav = this.props.navigation.dangerouslyGetParent().state;
      //             if (nav.routes.length > 1) {
      //               this.props.navigation.goBack();
      //               return;
      //             } else {
      //               this.props.navigation.navigate("home");
      //             }
      //           }}
      //         >
      //           <Icon
      //             color={"#fff"}
      //             iconStyle={{ paddingLeft: 15 }}
      //             name="angle-left"
      //             type="font-awesome"
      //             size={40}
      //           />
      //         </TouchableOpacity>
      //       )}
      //       <View
      //         style={{
      //           width: "100%",
      //           flex: 1,
      //           // backgroundColor: "#fff",
      //           justifyContent: "center",
      //           flexDirection: "row",
      //           alignItems: "center"
      //         }}
      //       >
      //         <Icon
      //           iconStyle={{
      //             position: "absolute",
      //             left: 20,
      //             fontWeight:100
      //           }}
      //           name="search"
      //           color={this.state.boxColor == "#fff" ? "#000" : "#717071"}
      //           size={18}
      //           type="font-awesome"
      //         />

      //         {
      //           <TextInput
      //             value={this.props.term && this.props.term.replace(",", "")}
      //             ref={r => {
      //               this.props.refs(r);
      //             }}
      //             autoFocus={false}
      //             placeholder="Search Weclikd"
      //             onChangeText={term => {
      //               this.props.setTerm(term);
      //               this.setState({ term: term});
      //               //this.setState({boxColor:'#fff'});
      //             }}
      //             onKeyPress={this.handleEvent}
      //             onFocus={() =>
      //               this.setState({
      //                 boxColor: "#fff"
      //               })
      //             }
      //             onBlur={() =>{
      //               if(this.props.term){
      //                 this.setState({ boxColor:'#fff' });
      //               }
      //               else{
      //                 this.setState({
      //                   boxColor: "#000"
      //                 })
      //               }
      //               }
      //             }
      //             style={{
      //               height: 40,
      //               width: "95%",
      //               borderRadius: 20,
      //               backgroundColor: this.state.boxColor,
      //               marginHorizontal: 10,
      //               fontFamily: ConstantFontFamily.defaultFont,
      //               // fontWeight: "bold",
      //               paddingLeft: 35,
      //               minWidth: Dimensions.get("window").width <= 750 && 330,
      //               outline: "none",
      //               // backgroundColor: Dimensions.get("window").width <= 1100 ? '#fff' : '#000'
      //             }}
      //           />
      //         }
      //         {Dimensions.get("window").width <= 1100 && (
      //           <Icon
      //             iconStyle={{
      //               position: "relative",
      //               left: -40
      //             }}
      //             name="times"
      //             onPress={() => {
      //               this.props.press(true);
      //               this.props.searchOpenBarStatus(false);
      //             }}
      //             size={20}
      //             type="font-awesome"
      //           />
      //         )}
      //       </View>
      //     </View>
      //   </Animated.View>
      // </View>


      <View style={{
        backgroundColor: this.state.boxColor == "#fff" ? "#fff" : '#000',
        width: Dimensions.get("window").width <= 750 ? 
        Dimensions.get("window").width-60 :
        Dimensions.get("window").width >= 750 && Dimensions.get("window").width <= 1100 ? Dimensions.get("window").width-100 : 440,
        height: 34,
        // transform: Dimensions.get("window").width <= 1100 && [{ translateX: -11 }],
        borderRadius: 20,
        flexDirection: 'row',
        borderColor: '#fff',
        borderWidth: 1,
        justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: '2%',
        marginHorizontal: 
        Platform.OS =='web' && Dimensions.get("window").width <=750 ? 10 :
        Platform.OS !='web' ? 50 : 0,
      }}>
        <View style={{ flexDirection: 'row', width: '90%', alignItems: 'center' }}>
          <Icon
            iconStyle={{ marginRight: 'auto' }}
            name="search"
            color={this.state.boxColor == "#fff" ? "#000" : "#717071"}
            size={18}
            type="font-awesome"
          />
          <TextInput style={{
            width: '90%',
            paddingLeft: 15, height: 32, borderColor: '#0000', borderWidth: 0,
            backgroundColor: this.state.boxColor,
            outline:'none'
          }}
            placeholder="Search Weclikd"
            onChangeText={term => {
              this.props.setTerm(term);
              this.setState({ term: term });
            }}
            onKeyPress={this.handleEvent}
            onFocus={() =>
              this.setState({
                boxColor: "#fff"
              })
            }
            onBlur={() => {
              if (this.props.term) {
                this.setState({ boxColor: '#fff' });
              }
              else {
                this.setState({
                  boxColor: "#000"
                })
              }
            }
            } />
        </View>
        {Dimensions.get("window").width <= 1100 && (
        <Icon
          name="times"
          onPress={() => {
            this.props.press(true);
            this.props.searchOpenBarStatus(false);
          }}
          size={20}
          type="font-awesome"
        />
        )}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    term: state.ScreenLoadingReducer.get("setTermWeb"),
    getsearchBarStatus: state.AdminReducer.get("searchBarOpenStatus"), 
  };
};

const mapDispatchToProps = dispatch => ({
  setTerm: payload => dispatch({ type: "SET_TERM_WEB", payload }),
  setKeyEvent: payload => dispatch({ type: "SET_KEY_EVENT_WEB", payload }),
  setDisplayType: payload => dispatch({ type: "SET_DISPLAY_TYPE", payload }),
  searchOpenBarStatus: payload => dispatch({ type: "SEARCHBAR_STATUS", payload })
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  SearchInputWeb
);

//export default SearchInputWeb;
