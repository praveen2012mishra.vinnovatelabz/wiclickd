import { List } from "immutable";
import React from "react";
import { graphql } from "react-apollo";
import {
  ActivityIndicator,
  Dimensions,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  ImageBackground
} from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import { listClikMembers } from "../actionCreator/ClikMembersAction";
import { listClikUserRequest } from "../actionCreator/ClikUserRequestAction";
import { getFeedProfileDetails } from "../actionCreator/FeedProfileAction";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { listTopicFeed } from "../actionCreator/TopicsFeedAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { getTrendingTopicsProfileDetails } from "../actionCreator/TrendingTopicsProfileAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import applloClient from "../client";
import ConstantFontFamily from "../constants/FontFamily";
import { rootTopics } from "../constants/RootTopics";
import {
  TopicFollowMutation,
  TopicUnFollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { DefaultTopicListMutation } from "../graphqlSchema/graphqlMutation/SearchMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  TopicFollowVariables,
  TopicUnFollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import TopicStar from "./TopicStar";

class SearchTopic extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onEndReachedCalledDuringMomentum = true;
    this.viewabilityConfig = {
      viewAreaCoveragePercentThreshold: 50
    };
    this.state = {
      TopicList: this.props.TopicList,
      term: this.props.term,
      page: 1,
      loading: true,
      loadingMore: false,
      refreshing: false,
      pageInfo: null,
      error: null,
      apiCall: true
    };
  }

  componentDidMount = () => {
    this.getDefaultTopicList();
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.TopicList !== this.props.TopicList) {
      this.setState({
        TopicList: this.props.TopicList
      });
    }
    if (prevProps.term !== this.props.term) {
      this.setState({
        term: this.props.term
      });
    }
  };

  goToTopicProfile = async id => {
    this.props.topicId({
      id: id,
      type: "feed"
    });
  };

  followTopics = topicId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    TopicFollowVariables.variables.topic_id = topicId;
    TopicFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowTopics = async topicId => {
    TopicUnFollowVariables.variables.topic_id = topicId;
    applloClient
      .query({
        query: TopicUnFollowMutation,
        ...TopicUnFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  favroiteTopics = async topicId => {
    TopicFollowVariables.variables.topic_id = topicId;
    TopicFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  getTopicStar = TopicName => {
    let index = 0;
    index = this.props.getUserFollowTopicList.findIndex(
      i =>
        i
          .getIn(["topic", "name"])
          .toLowerCase()
          .replace("topic:", "") ==
        TopicName.toLowerCase().replace("topic:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowTopicList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this.getDefaultTopicList();
      }
    );
  };

  _handleLoadMore = distanceFromEnd => {
    this.setState(
      (prevState, nextProps) => ({
        //  loadingMore: true
      }),
      () => {
        if (
          0 <= distanceFromEnd &&
          distanceFromEnd <= 5 &&
          this.state.apiCall == true
        ) {
          this.setState({
            apiCall: false
          });
          this.getDefaultTopicList();
        }
      }
    );
  };

  _renderFooter = () => {
    if (!this.state.loadingMore) return null;
    return (
      <View>
        <ActivityIndicator animating size="large" color="#000" />
      </View>
    );
  };

  getDefaultTopicList = () => {
    let __self = this;
    const { page, pageInfo } = this.state;

    if (pageInfo == null) {
      this.setState({
        loadingMore: true
      });
      applloClient
        .query({
          query: DefaultTopicListMutation,
          variables: {
            first: 20,
            after: pageInfo ? pageInfo.endCursor : null
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          __self.setState({
            loading: false,
            TopicList: res.data.topic_list.edges,
            pageInfo: res.data.topic_list.pageInfo,
            page: page + 1,
            apiCall: true,
            loadingMore: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else if (pageInfo != null && pageInfo.hasNextPage == true) {
      this.setState({
        loadingMore: true
      });
      applloClient
        .query({
          query: DefaultTopicListMutation,
          variables: {
            first: 20,
            after: pageInfo ? pageInfo.endCursor : null
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          __self.setState({
            loading: false,
            TopicList: __self.state.TopicList.concat(res.data.topic_list.edges),
            pageInfo: res.data.topic_list.pageInfo,
            apiCall: true,
            loadingMore: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    }
  };

  _renderItem = item => {
    var row = item.item.node ? item.item.node : item.item;
    return (
      <View key={item.index}>
        {/* <View
          style={{
            overflow: "hidden",
            marginBottom: 5,
            width: "100%",
            flexDirection: "row"
          }}
        >
          <View
            style={{
              flexDirection: "row",
              width: "100%"
            }}
          >
            <View
              style={{
                width: "90%",
                justifyContent: "flex-start",
                alignSelf: "center",
                alignItems: "center",
                flexDirection: "row"
              }}
            >
              {row.parents &&
                row.parents.map((item, index) => {
                  return (
                    <TouchableOpacity
                      style={{
                        height: 30,
                        paddingVertical: 5,
                        backgroundColor:
                          rootTopics.includes(item) == false
                            ? "#e3f9d5"
                            : "#FEF6D1",
                        alignSelf: "flex-start",
                        borderTopLeftRadius: index == 0 ? 6 : 0,
                        borderBottomLeftRadius: index == 0 ? 6 : 0
                      }}
                      key={index}
                      onPress={() => this.goToTopicProfile(item)}
                    >
                      <Text
                        style={{
                          color:
                            rootTopics.includes(item) == false
                              ? "#009B1A"
                              : "#FEC236",
                          fontWeight: "bold",
                          fontSize: 14,
                          fontFamily: ConstantFontFamily.MontserratBoldFont,
                          paddingHorizontal: 3
                        }}
                      >
                        /{item.toLowerCase()}
                      </Text>
                    </TouchableOpacity>
                  );
                })}

              <TouchableOpacity
                style={{
                  height: 30,
                  paddingVertical: 5,
                  backgroundColor:
                    rootTopics.includes(row.name) == false
                      ? "#e3f9d5"
                      : "#FEF6D1",
                  alignSelf: "flex-start",
                  borderTopRightRadius: 6,
                  borderBottomRightRadius: 6,
                  borderTopLeftRadius: row.parents ? 0 : 6,
                  borderBottomLeftRadius: row.parents ? 0 : 6
                }}
                onPress={() => this.goToTopicProfile(row.name)}
              >
                <Text
                  style={{
                    color:
                      rootTopics.includes(row.name) == false
                        ? "#009B1A"
                        : "#FEC236",
                    fontWeight: "bold",
                    fontSize: 14,
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    paddingHorizontal: 3
                  }}
                >
                  /{row.name}
                </Text>
              </TouchableOpacity>
              <Text
                numberOfLines={1}
                style={{
                  textAlign: "left",
                  color: "#000",
                  fontSize: 13,
                  fontFamily: ConstantFontFamily.defaultFont,
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  paddingHorizontal: 10
                }}
              >
                {row.description}
              </Text>
            </View>

            <View
              style={{
                width: "10%",
                justifyContent: "center",
                flexDirection: "row",
                alignSelf: "center",
                alignItems: "center",
                height: 30
              }}
            >
              <View
                style={{
                  flexDirection: "row"
                }}
              >
                <TopicStar
                  TopicName={row.name}
                  ContainerStyle={{}}
                  ImageStyle={{
                    height: 20,
                    width: 20,
                    alignSelf: "center"
                  }}
                />

                <Text
                  style={{
                    marginHorizontal: 5,
                    color: "#000",
                    fontSize: 15,
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    fontWeight: "bold",
                    textAlign: "center"
                  }}
                >
                  {row.num_followers}
                </Text>
              </View>
            </View>
          </View>
        </View> */}

        <TouchableOpacity
          style={{
            borderRadius: 10,
            overflow: "hidden",
            marginBottom: 5,
            borderWidth: 1,
            borderColor: "#c5c5c5",
            backgroundColor: "#fff"
          }}
          onPress={() => this.goToTopicProfile(row.name)}
        >
          <ImageBackground
            style={{
              height: Dimensions.get("window").height / 4
            }}
            source={{
              uri: row.banner_pic
            }}
          ></ImageBackground>

          <View
            style={{
              flexDirection: "column",
              justifyContent: "flex-end",
              backgroundColor: "#fff",
              // padding: 10,
              paddingHorizontal: 20
            }}
          >
            <View
              style={{
                flexDirection: "row",
                width: "100%",
                marginVertical: 15,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <View
                style={{
                  width: "70%",
                  justifyContent: "flex-start",
                  alignSelf: "center",
                  alignItems: "center",
                  flexDirection: "row",
                  flexWrap: "wrap",
                }}
              >
                {/* {row.parents &&
                  row.parents.map((item, index) => {
                    return (
                      <TouchableOpacity
                        style={{
                          height: 30,
                          paddingVertical: 5,
                          backgroundColor:
                            rootTopics.includes(item) == false
                              ? "#e3f9d5"
                              : "#FEF6D1",
                          alignSelf: "flex-start",
                          borderTopLeftRadius: index == 0 ? 6 : 0,
                          borderBottomLeftRadius: index == 0 ? 6 : 0
                        }}
                        key={index}
                        onPress={() => this.goToTopicProfile(item)}
                      >
                        <Text
                          style={{
                            color:
                              rootTopics.includes(item) == false
                                ? "#009B1A"
                                : "#FEC236",
                            fontWeight: "bold",
                            fontSize: 14,
                            fontFamily: ConstantFontFamily.MontserratBoldFont,
                            paddingHorizontal: 3
                          }}
                        >
                          /{item.toLowerCase()}
                        </Text>
                      </TouchableOpacity>
                    );
                  })} */}

                <TouchableOpacity
                  style={{
                    height: 30,
                    paddingVertical: 5,
                    backgroundColor: '#e3f9d5',
                    // rootTopics.includes(row.name) == false
                    //   ? "#e3f9d5"
                    //   : "#FEF6D1",
                    alignSelf: "flex-start",
                    borderTopRightRadius: 6,
                    borderBottomRightRadius: 6,
                    borderTopLeftRadius: row.parents ? 0 : 6,
                    borderBottomLeftRadius: row.parents ? 0 : 6
                  }}
                  onPress={() => this.goToTopicProfile(row.name)}
                >
                  <Text
                    style={{
                      color: '#009B1A',
                      // rootTopics.includes(row.name) == false
                      //   ? "#009B1A"
                      //   : "#FEC236",
                      fontWeight: "bold",
                      fontSize: 14,
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      paddingHorizontal: 3
                    }}
                  >
                    /{row.name}
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: "30%",
                  justifyContent: "flex-end",
                  flexDirection: "row",
                  alignSelf: "center",
                  alignItems: "center",
                  height: 30
                }}
              >
                <View
                  style={{
                    flexDirection: "row"
                  }}
                >
                  <TopicStar
                    TopicName={row.name}
                    ContainerStyle={{}}
                    ImageStyle={{
                      height: 20,
                      width: 20,
                      alignSelf: "center"
                    }}
                  />

                  {/* <Text
                    style={{
                      marginHorizontal: 5,
                      color: "#000",
                      fontSize: 15,
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      fontWeight: "bold",
                      textAlign: "center"
                    }}
                  >
                    {row.num_followers}
                  </Text> */}
                </View>
              </View>
            </View>
            <View
              style={{
                // marginHorizontal: 5,
                marginBottom: 10
              }}
            >
              <Text
                style={{
                  color: "grey",
                  fontSize: 13,
                  // fontWeight: "bold",
                  fontFamily: ConstantFontFamily.defaultFont
                }}
              >
                2 Subtopics{" "}{" "}
                {row.num_followers}{" "}
            Followers
          </Text>
            </View>
            <View
              style={{
                //marginHorizontal: 5,
                marginBottom: 15
              }}
            >
              <Text
                style={{
                  color: "#000",
                  fontSize: 13,
                  fontFamily: ConstantFontFamily.defaultFont
                }}
              >
                {row.description}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  onViewableItemsChanged = ({ viewableItems, changed }) => {
    let perLoadDataCount = 20;
    let halfOfLoadDataCount = perLoadDataCount / 2;
    let lastAddArr =
      this.state.TopicList.length > 0 &&
      this.state.TopicList.slice(-perLoadDataCount);

    try {
      if (
        lastAddArr[halfOfLoadDataCount] &&
        viewableItems.length > 0 &&
        this.props.loginStatus == 1
      ) {
        //FlatList
        if (
          viewableItems.find(
            item =>
              item.item.node.id === lastAddArr[halfOfLoadDataCount].node.id
          )
        ) {
          this._handleLoadMore(0);
        }
      }
    } catch (e) {
      console.log(e, "lastAddArr", lastAddArr[halfOfLoadDataCount]);
    }
  };

  render() {
    return (
      <View style={{ marginTop: 10, marginHorizontal: 'auto' }}>
        <FlatList
          extraData={this.state}
          contentContainerStyle={{
            flexDirection: "column",
            height: Dimensions.get("window").height,
            width: "100%"
          }}
          data={this.state.TopicList}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItem}
          showsVerticalScrollIndicator={false}
          onRefresh={this._handleRefresh}
          refreshing={this.state.refreshing}
          onEndReached={({ distanceFromEnd }) => {
            //this._handleLoadMore(distanceFromEnd);
          }}
          onEndReachedThreshold={0.2}
          initialNumToRender={10}
          ListFooterComponent={this._renderFooter}
          onViewableItemsChanged={this.onViewableItemsChanged}
          viewabilityConfig={this.viewabilityConfig}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  listTrending_cliks: !state.TrendingCliksReducer.getIn(["Trending_cliks_List"])
    ? List()
    : state.TrendingCliksReducer.getIn(["Trending_cliks_List"]),
  link: state.LinkPostReducer.get("link"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  loginStatus: state.UserReducer.get("loginStatus"),
  getUserFollowTopicList: state.LoginUserDetailsReducer.get(
    "userFollowTopicsList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowTopicsList")
    : List(),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List(),
  getUserFollowUserList: state.LoginUserDetailsReducer.get("userFollowUserList")
    ? state.LoginUserDetailsReducer.get("userFollowUserList")
    : List(),
  getUserFollowFeedList: state.LoginUserDetailsReducer.get("userFollowFeedList")
    ? state.LoginUserDetailsReducer.get("userFollowFeedList")
    : List(),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension")
});

const mapDispatchToProps = dispatch => ({
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  topicId: payload => dispatch(getTrendingTopicsProfileDetails(payload)),
  listTopicFeed: payload => dispatch(listTopicFeed(payload)),
  ClikId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  setClikUserRequest: payload => dispatch(listClikUserRequest(payload)),
  setClikMembers: payload => dispatch(listClikMembers(payload)),
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload)),
  setFeedDetails: payload => dispatch(getFeedProfileDetails(payload))
});

const SearchTopicWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(SearchTopic);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  SearchTopicWrapper
);
