import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  TouchableHighlight
} from "react-native";
import ConstantFontFamily from "../constants/FontFamily";
import { Hoverable } from "react-native-web-hooks";
import NavigationService from "../library/NavigationService";
import { connect } from "react-redux";
import { compose } from "recompose";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";

const DiscussByUsersList = ({
  displayCount,
  items,
  keyName,
  title,
  itemPrefix,
  onPress,
  userId
}) => {
  const [isExpaned, setExpend] = useState(false);
  const groupCount = items.length - displayCount;
  const onItemClick = item => {
    // if (onPress) onPress(item);
    userId({
      username: item.username,
      type: "profile"
    });
    NavigationService.navigate("profile", {
      username: item.username,
      type: "profile"
    });
  };
  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "center",
        flexWrap: "wrap",
        width: "100%",
        marginBottom: items.length > 0 ? 10 : 0
      }}
    >
      {title && items.length > 0 ? (
        <Text style={styles.title}>{title}</Text>
      ) : null}
      {items.map((item, index) => {
        if (!isExpaned && index < displayCount)
          return (
            <Hoverable>
              {isHovered => (
                <TouchableOpacity
                  onPress={() => onItemClick(item)}
                  style={{ marginRight: 5 }}
                >
                  <Text
                    style={{
                      color: "#000",
                      fontFamily: ConstantFontFamily.defaultFont,
                      fontSize: 13,
                      fontStyle: "italic",
                      textDecorationLine:
                        isHovered == true ? "underline" : "none"
                    }}
                  >
                    {`${itemPrefix}${item[keyName]}`}
                  </Text>
                </TouchableOpacity>
              )}
            </Hoverable>
          );
        else if (isExpaned) {
          return (
            <Hoverable>
              {isHovered => (
                <TouchableOpacity
                  onPress={() => onItemClick(item)}
                  style={{ marginRight: 5 }}
                >
                  <Text
                    style={{
                      color: "#000",
                      fontFamily: ConstantFontFamily.defaultFont,
                      fontSize: 13,
                      fontStyle: "italic",
                      textDecorationLine:
                        isHovered == true ? "underline" : "none"
                    }}
                  >
                    {`${itemPrefix}${item[keyName]}`}
                  </Text>
                </TouchableOpacity>
              )}
            </Hoverable>
          );
        }
      })}
      {!isExpaned && groupCount > 0 ? (
        <TouchableOpacity
          style={{ marginRight: 5 }}
          onPress={() => setExpend(prevExpend => !prevExpend)}
        >
          <Text style={styles.item}>{`and ${groupCount} others`}</Text>
        </TouchableOpacity>
      ) : null}
    </View>
  );
};
DiscussByUsersList.propTypes = {
  items: PropTypes.array.isRequired,
  keyName: PropTypes.string.isRequired
};
DiscussByUsersList.defaultProps = {
  displayCount: 1,
  items: [],
  itemPrefix: "@"
};

const mapDispatchToProps = dispatch => ({
  userId: payload => dispatch(getCurrentUserProfileDetails(payload))
});

export default compose(connect(null, mapDispatchToProps))(DiscussByUsersList);

const styles = StyleSheet.create({
  title: {
    color: "#000",
    fontFamily: ConstantFontFamily.defaultFont,
    fontSize: 13,
    fontStyle: "italic",
    marginRight: 5
  },
  item: {
    color: "#000",
    fontFamily: ConstantFontFamily.defaultFont,
    fontSize: 13,
    fontStyle: "italic"
  }
});
