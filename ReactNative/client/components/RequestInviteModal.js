import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Button, Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import applloClient from "../client";
import "../components/Firebase";
import ConstantFontFamily from "../constants/FontFamily";
import { ClikJoinMutation } from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { UserQueryMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { ClikJoinVariables } from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { UserDetailsVariables } from "../graphqlSchema/graphqlVariables/UserVariables";
import { SearchUserMutation } from "../graphqlSchema/graphqlMutation/SearchMutation";
import { SearchUserVariables } from "../graphqlSchema/graphqlVariables/SearchVariables";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { graphql } from "react-apollo";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import ButtonStyle from "../constants/ButtonStyle";

class RequestInviteModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      qualification: "",
      username: "",
      showError: false,
      MutipleUserList: [],
      UserList: [],
      status: "Default",
      member_type: "",
      user_msg: ""
    };
  }

  checkUser = async () => {
    let value =
      this.state.username.charAt(0) == "@"
        ? this.state.username.substr(1)
        : this.state.username;
    UserDetailsVariables.variables.id = "User:" + value;
    try {
      await applloClient
        .query({
          query: UserQueryMutation,
          ...UserDetailsVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState({
            MutipleUserList: this.state.MutipleUserList.concat([
              {
                id: res.data.user.id,
                name: res.data.user.username,
                pic: res.data.user.profile_pic
              }
            ])
          });
          this.setState({
            showError: false,
            username: ""
          });
        });
    } catch (e) {
      console.log(e);
      this.setState({
        showError: true,
        username: ""
      });
    }
  };

  deleteUser = async index => {
    let updatedList = this.state.MutipleUserList;
    updatedList.splice(index, 1);
    this.setState({
      MutipleUserList: updatedList
    });
  };

  async componentDidMount() {
    if (this.props.ClikInfo.get("invite_only") == false) {
      await this.requestInvite();
    }
  }

  requestInvite = async () => {
    let permittedValues = this.state.MutipleUserList.map(value => value.id);
    ClikJoinVariables.variables.clik_id = this.props.ClikInfo.get("id");
    ClikJoinVariables.variables.qualification = this.state.qualification;
    ClikJoinVariables.variables.known_members = permittedValues;
    try {
      await applloClient
        .query({
          query: ClikJoinMutation,
          ...ClikJoinVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          if (res.data.clik_join.status.custom_status == "JOINED") {
            let member_type = res.data.clik_join.member_type;
            this.setState({
              status: "Success",
              member_type: member_type
            });
            let resDataLogin = await this.props.Login();
            await this.props.saveLoginUser(resDataLogin.data.login);
          } else if (res.data.clik_join.status.custom_status == "PENDING") {
            this.setState({
              status: "Pending"
            });
          } else {
            let user_msg = res.data.clik_join.status.user_msg;
            this.setState({
              status: "Failure",
              user_msg: user_msg
            });
          }
        });
    } catch (e) {
      console.log(e);
    }
  };

  customRenderUserSuggestion = value => {
    SearchUserVariables.variables.prefix = value;
    applloClient
      .query({
        query: SearchUserMutation,
        ...SearchUserVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        this.setState({ UserList: res.data.search.users });
      });
  };

  checkSelectedUser = async value => {
    UserDetailsVariables.variables.id = "User:" + value;
    try {
      await applloClient
        .query({
          query: UserQueryMutation,
          ...UserDetailsVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState({
            MutipleUserList: this.state.MutipleUserList.concat([
              {
                id: res.data.user.id,
                name: res.data.user.username,
                pic: res.data.user.profile_pic
              }
            ])
          });
          this.setState({
            showError: false,
            username: ""
          });
        });
    } catch (e) {
      console.log(e);
      this.setState({
        showError: true,
        username: ""
      });
    }
  };

  render() {
    return (
      <View
        style={{
          width: "100%"
        }}
      >
        <Hoverable>
          {isHovered => (
            <TouchableOpacity
              style={{
                flexDirection: "row",
                justifyContent: "flex-start",
                flex: 1,
                position: "absolute",
                zIndex: 999999,
                left: 0,
                top: 0
              }}
              onPress={this.props.onClose}
            >
              <Icon
                color={isHovered == true ? "rgba(256,256,256,0.4)" : "#000"}
                iconStyle={{
                  color: "#fff",
                  justifyContent: "center",
                  alignItems: "center"
                }}
                reverse
                name="close"
                type="antdesign"
                size={16}
              />
            </TouchableOpacity>
          )}
        </Hoverable>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            backgroundColor: "#000",
            alignItems: "center",
            height: 50,
            borderTopLeftRadius: 6,
            borderTopRightRadius: 6
          }}
        >
          <Text
            style={{
              color: "#fff",
              fontWeight: "bold",
              fontSize: 18,
              fontFamily: ConstantFontFamily.MontserratBoldFont
            }}
          >
            Join Clik
          </Text>
        </View>
        {this.state.status == "Default" && (
          <View
            style={{
              borderRadius: 4,
              width: "100%",
              backgroundColor: "#fff"
            }}
          >
            <ScrollView
              showsVerticalScrollIndicator={false}
              style={{
                backgroundColor: "#fff",
                padding: 10
              }}
            >
              <Text
                style={{
                  color: "#000",
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  fontSize: 17,
                  fontWeight: "bold",
                  marginVertical: 10
                }}
              >
                Prerequisites to Join Clik
              </Text>
              {this.props.ClikInfo.get("qualifications") == null && (
                <Text
                  style={{
                    textAlign: "center",
                    color: "#000",
                    fontFamily: ConstantFontFamily.defaultFont,
                    fontSize: 16,
                    fontWeight: "bold",
                    margin: 10
                  }}
                >
                  No prerequisites to join this clik
                </Text>
              )}

              {this.props.ClikInfo.get("qualifications") != null &&
                this.props.ClikInfo.get("qualifications").map((item, index) => {
                  return (
                    <Text
                      key={index}
                      style={{
                        color: "#000",
                        fontFamily: ConstantFontFamily.defaultFont,
                        fontSize: 16,
                        fontWeight: "bold",
                        margin: 10
                      }}
                    >
                      {index + 1}
                      {")"} {item}
                    </Text>
                  );
                })}

              <Text
                style={{
                  color: "#000",
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  fontSize: 17,
                  fontWeight: "bold",
                  marginTop: 10
                }}
              >
                Your Qualifications
              </Text>
              <TextInput
                value={this.state.qualification}
                placeholder="Enter your qualification and/or how you know the members of this clik."
                placeholderTextColor="#6D757F"
                multiline={true}
                numberOfLines={5}
                style={{
                  padding: 5,
                  color: "#000",
                  fontSize: 14,
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.defaultFont,
                  width: "100%",
                  borderRadius: 6,
                  borderColor: "#e1e1e1",
                  borderWidth: 1,
                  marginTop: 10,
                  height: Platform.OS == "ios" ? 100 : null
                }}
                onChangeText={qualification => this.setState({ qualification })}
              />
            </ScrollView>
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                marginVertical: 10
              }}
            >
              <Button
                color="#fff"
                title="Join Clik"
                titleStyle={ButtonStyle.titleStyle}
                buttonStyle={ButtonStyle.backgroundStyle}
                containerStyle={ButtonStyle.containerStyle}
                onPress={() => this.requestInvite()}
              />
            </View>
          </View>
        )}

        {this.state.status == "Success" && (
          <View
            style={{
              borderRadius: 4,
              width: "100%",
              backgroundColor: "#fff",
              paddingVertical: 20
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              Congratulations!
            </Text>

            <View
              style={{
                justifyContent: "center",
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  color: "#000",
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  fontSize: 16,
                  fontWeight: "bold",
                  margin: 10
                }}
              >
                You are now a {this.state.member_type.toLowerCase()} of{" "}
              </Text>

              <TouchableOpacity
                //onPress={() => goToProfile(this.props.ClikInfo.get("name"))}
                style={{
                  marginTop: 5,
                  height: 30,
                  alignSelf: "flex-start",
                  padding: 5,
                  backgroundColor: "#E8F5FA",
                  borderRadius: 6
                }}
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        width: "100%",
                        color: "#4169e1",
                        fontSize: 15,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        textDecorationLine:
                          isHovered == true ? "underline" : "none"
                      }}
                    >
                      {" "}
                      #{this.props.ClikInfo.get("name")}{" "}
                    </Text>
                  )}
                </Hoverable>
              </TouchableOpacity>
            </View>

            <View
              style={{
                justifyContent: "space-between",
                alignItems: "center",
                marginVertical: 20,
                flexDirection: "row",
                marginHorizontal: 50
              }}
            >
              <Button
                color="#fff"
                title="Leave"
                titleStyle={ButtonStyle.titleStyle}
                buttonStyle={ButtonStyle.backgroundStyle}
                containerStyle={ButtonStyle.containerStyle}
                onPress={() => this.props.onClose()}
              />

              <Button
                color="#fff"
                title="Continue"
                buttonStyle={{
                  alignSelf: "center",
                  backgroundColor: "#000",
                  borderRadius: 6,
                  paddingHorizontal: 20
                }}
                onPress={() => this.props.onClose()}
              />
            </View>
          </View>
        )}

        {this.state.status == "Failure" && (
          <View
            style={{
              borderRadius: 4,
              width: "100%",
              backgroundColor: "#fff",
              paddingVertical: 20
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              Sorry, you could not join the clik at this time.
            </Text>

            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              {this.state.user_msg}
            </Text>

            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                marginVertical: 20
              }}
            >
              <Button
                color="#fff"
                title="Continue"
                titleStyle={ButtonStyle.titleStyle}
                buttonStyle={ButtonStyle.backgroundStyle}
                containerStyle={ButtonStyle.containerStyle}
                onPress={() => this.props.onClose()}
              />
            </View>
          </View>
        )}

        {this.state.status == "Pending" && (
          <View
            style={{
              borderRadius: 4,
              width: "100%",
              backgroundColor: "#fff",
              paddingVertical: 20
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              An admin of the clik will review your application.
            </Text>

            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                marginVertical: 20
              }}
            >
              <Button
                color="#fff"
                title="Continue"
                titleStyle={ButtonStyle.titleStyle}
                buttonStyle={ButtonStyle.backgroundStyle}
                containerStyle={ButtonStyle.containerStyle}
                onPress={() => this.props.onClose()}
              />
            </View>
          </View>
        )}
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload))
});

const RequestInviteModalWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(RequestInviteModal);

export default compose(connect(null, mapDispatchToProps))(
  RequestInviteModalWrapper
);

export const styles = {
  LogoImageStyle: {
    height: 80,
    width: 235
  },
  LogoContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    margin: 25
  }
};
