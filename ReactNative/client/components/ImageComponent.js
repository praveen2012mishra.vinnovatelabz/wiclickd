import React, { Component } from "react";
import { ActivityIndicator, ImageBackground, View, Dimensions } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

class ImageComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false
    };
  }

  _onLoad = () => {
    this.setState(() => ({ loaded: true }));
  };

  render() {
    return (
      <View>
        <ImageBackground
          style={{
            height: hp("25%"),
            // borderRadius: 9,
            opacity: this.state.loaded == true ? 1 : 0.5
          }}
          imageStyle={{
            borderRadius:  Dimensions.get('window').width <=750 ? 0 : 8
          }}
          source={this.props.bgImage}
          resizeMode={"cover"}
          onLoad={() => this._onLoad()}
        >
          {!this.state.loaded && (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                alignSelf: "center",
                flex: 1
              }}
            >
              <ActivityIndicator animating size="large" color="#000" />
            </View>
          )}
        </ImageBackground>
      </View>
    );
  }
}

export default ImageComponent;
