import "@expo/browser-polyfill";
import _ from "lodash";
import React, { Component } from "react";
import { TouchableOpacity } from "react-native";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setLikeContent } from "../actionCreator/LikeContentAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import applloClient from "../client";
import {
  AdminPostQuery,
  PostQuery
} from "../graphqlSchema/graphqlMutation/PostMutation";

class ChangeIcon extends Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      imageList: [1, 2, 3, 4, 5],
      imageColorList: ["NONE", "RED", "SILVER", "GOLD", "DIAMOND"],
      borderColorList: ["#999", "#f80403", "#8E553A", "#929294", "#A26700"],
      shadowborderColorList: [
        "#999",
        "#feaeae",
        "#e2be9d",
        "#d8d8d8",
        "#ffdc73"
      ],
      BorderColorIndex: 0,
      defultSelect: false,
      score: 0,
      clickEvent: false,
      backgroundColorList: ["#999", "#F3E8E8", "#EAEBEC", "#EAE8BB", "#D6E2F3"],
      heartColorList: ["#969faa", "#de5246", "#b0b0b0", "#ffce44", "#4169e1"]
    };
  }

  async componentDidMount() {
    await this.getHeart(this.props.selectedIconList);
    await this.getBorderIndex(this.props.score);
  }

  componentDidUpdate = async prevProps => {
    if (prevProps.selectedIconList !== this.props.selectedIconList) {
      await this.getHeart(this.props.selectedIconList);
    }
    let data = document.getElementById(this.props.PostId);
    if (data != null) {
      data.addEventListener("keydown", event => {
        if (event.keyCode == 76) {
          this.changeIcon();
        }
      });
    }
  };

  getBorderIndex = async score => {
    let tempIndex = 0;
    if (score == 0) {
      tempIndex = 0;
    } else if (0 < score && score <= 25) {
      tempIndex = 1;
    } else if (25 < score && score <= 50) {
      tempIndex = 2;
    } else if (50 < score && score <= 75) {
      tempIndex = 3;
    } else {
      tempIndex = 4;
    }
    await this.setState({
      score: score,
      BorderColorIndex: tempIndex
    });
  };

  getHeart = async liketype => {
    let tempIndex = 0;
    if (liketype == "RED") {
      tempIndex = 1;
    } else if (liketype == "SILVER") {
      tempIndex = 2;
    } else if (liketype == "GOLD") {
      tempIndex = 3;
    } else if (liketype == "DIAMOND") {
      tempIndex = 4;
    } else {
      tempIndex = 0;
    }
    await this.setState({
      index: tempIndex
    });
  };

  changeIcon = async () => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    this.setState({
      clickEvent: true
    });
    this.props.onOpen(true);
    if (this.state.index + 1 === this.state.imageList.length) {
      this.setState({
        index: 0
      });
    } else {
      this.setState({
        index: this.state.index + 1
      });
    }
    this.changeIconApi(
      this.state.imageColorList[
      this.state.index + 1 === this.state.imageList.length
        ? 0
        : this.state.index + 1
      ]
    );
  };

  changeIconApi = _.debounce(typeIndex => {
    this.props.LikeContent({
      content_id: this.props.PostId,
      like_type: typeIndex
    });
    if (this.props.PostId.startsWith("Post") == true && typeIndex != "NONE") {
      this.updateDiscussion(typeIndex);
    }
    this.props.onOpen(false);
  }, 5000);

  updateDiscussion = typeIndex => {
    let index = 0;
    index = this.props.DiscussionHomeFeed.findIndex(
      i => i.node.id == this.props.PostId
    );
    if (index == -1) {
      applloClient
        .query({
          query: this.props.isAdmin == true ? AdminPostQuery : PostQuery,
          variables: {
            id: this.props.PostId,
            first: 3,
            after: null,
            clik_id: null
          },
          fetchPolicy: "no-cache"
        })
        .then(response => {
          let data = { node: {} };
          data.node = response.data.post;
          data.node.user_like_type = typeIndex;
          this.props.setDiscussionHomeFeed([
            data,
            ...this.props.DiscussionHomeFeed
          ]);
        })
        .catch(e => {
          console.log(e);
        });
    }
  };
  render() {
    const { score, BorderColorIndex, defultSelect, clickEvent } = this.state;
    return (
      <TouchableOpacity
        nativeID={"Heart" + this.props.PostId}
        onPress={this.changeIcon}
      >
        <Icon
          color={
            this.state.index > 0
              ? this.state.heartColorList[this.state.index]
              : this.state.heartColorList[0]
          }
          name={this.state.index > 0 ? "heart" : "heart-o"}
          type="font-awesome"
          size={20}
          // {this.props.PostId.startsWith("Post") == true ? 20 : 20}
          iconStyle={{ alignSelf: "center"}}
          containerStyle={{ alignSelf: "center" }}
        />
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  DiscussionHomeFeed: state.HomeFeedReducer.get("DiscussionHomeFeedList")
});

const mapDispatchToProps = dispatch => ({
  LikeContent: payload => dispatch(setLikeContent(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setDiscussionHomeFeed: payload =>
    dispatch({ type: "SET_DISCUSSION_HOME_FEED", payload })
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  ChangeIcon
);