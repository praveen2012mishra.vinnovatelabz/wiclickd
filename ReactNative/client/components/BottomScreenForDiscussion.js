import React, { Component } from "react";
import { View, Text, Dimensions, StyleSheet } from "react-native";
import { Icon, Tooltip } from "react-native-elements";
import { connect } from "react-redux";

class BottomScreenForDiscussion extends Component {
  constructor(props){
    super(props)
    this.state = {
      openCreateCommentStatus: false,
    }
  }

  render() {
    return (
      <View
        style={{
          width: "100%",
          backgroundColor:
            Dimensions.get("window").width <= 750 ? "#fff" : "#000",
          height: 50,
          flexDirection: "row",
          justifyContent: 'flex-start',
          alignItems: "center",
          borderTopWidth: 1,
          borderTopColor: "#c5c5c5",
          paddingHorizontal: 15
        }}
      >
        <Icon
          name={"comment-o"}
          type="font-awesome"
          size={25}
          iconStyle={{color:'gray'}}
          containerStyle={{ alignSelf: "center" }}
          onPress={() => this.setState({ openCreateCommentStatus: !this.state.openCreateCommentStatus }, () => {
            this.props.openCreateComment(this.state.openCreateCommentStatus)
          })

          }
        />
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  openCreateComment: (payload) =>
    dispatch({ type: "OPEN_CREATE_COMMENT", payload }),
});
export default connect(null, mapDispatchToProps)(BottomScreenForDiscussion);

