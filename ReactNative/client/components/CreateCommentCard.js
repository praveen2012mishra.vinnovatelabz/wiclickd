import React, { useEffect, useState, useRef } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Dimensions,
  Image
} from "react-native";
import RNPickerSelect from "react-native-picker-select";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import applloClient from "../client";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import { CreateCommentMutation } from "../graphqlSchema/graphqlMutation/LikeContentMutation";
import { CreateCommentVariables } from "../graphqlSchema/graphqlVariables/LikeContentVariables";
import { capitalizeFirstLetter } from "../library/Helper";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import { Icon, Button } from "react-native-elements";
import ButtonStyle from "../constants/ButtonStyle";
import { EditorState, RichUtils, convertToRaw, convertFromHTML, ContentState } from 'draft-js';
//import 'draft-js/dist/Draft.css';
import { stateToHTML } from 'draft-js-export-html';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
//import '../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
//import '../assets/react-draft.css';
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";

function myBlockStyleFn(contentBlock) {
  const type = contentBlock.getType();
  if (type === 'blockquote') {
    return styles.blockQuote;
  }
}

const CreateCommentCard = props => {
  const inputTxt = useRef(null);
  let textStyle = "#959FAA";
  const inputRefs = {};
  const [title, settitle] = useState("");
  const [item, setitem] = useState([]);
  const [click, setclick] = useState(null);
  const [showtopictooltip, setshowtopictooltip] = useState(false);
  const [showsubmitbutton, setshowsubmitbutton] = useState(true);
  const [getBorderColor, setBorderColor] = useState("#e8e8e8");
  const [editorState, onChange] = useState(EditorState.createEmpty());
  const [placeholderState, setPlaceholderState] = useState(true);
  const [colorState, setColorState] = useState([
    { type: 'UNDERLINE', state: false },
    { type: 'BOLD', state: false },
    { type: 'ITALIC', state: false },
    { type: 'blockquote', state: false },
    { type: 'code-block', state: false },
    { type: 'unordered-list-item', state: false },
  ]);
  const [titleContent, setTitleContent] = useState("characters more");
  const [getSubmitData, setSubmitData] = useState('');

  const onEditorStateChange = (editorState) => {
    if (props.loginStatus == 0) {
      props.setLoginModalStatus(true);
      settitle("")
    }
    else {
      //document.querySelector(".demo-editor").scrollTop = parseInt(document.querySelector(".demo-editor").scrollHeight)+parseInt(document.querySelector(".demo-editor").scrollHeight);
      if (editorState.getCurrentContent().getPlainText('\u0001').length == 0) {
        setPlaceholderState(false)
      }
      onChange(editorState);
      settitle(editorState.getCurrentContent().getPlainText('\u0001'))
      title.length < 140 ? setTitleContent('characters more') : setTitleContent('characters left')
      setSubmitData(draftToHtml(convertToRaw(editorState.getCurrentContent())))
    }
  };

  const handleKeyCommand = (command) => {
    console.log(command, '-------------------------------------------->');
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      onChange(newState);
      settitle(editorState.getCurrentContent().getPlainText('\u0001'))
      return 'handled';
    }
    return 'not-handled';
  }

  const onUnderlineClick = () => {
    onChange(RichUtils.toggleInlineStyle(editorState, 'UNDERLINE'));
    settitle(editorState.getCurrentContent().getPlainText('\u0001'))
  }

  const onBoldClick = () => {
    onChange(RichUtils.toggleInlineStyle(editorState, 'BOLD'))
    settitle(editorState.getCurrentContent().getPlainText('\u0001'))
  }

  const onItalicClick = () => {
    onChange(RichUtils.toggleInlineStyle(editorState, 'ITALIC'))
    settitle(editorState.getCurrentContent().getPlainText('\u0001'))
  }

  const submitComment = () => {
    //console.log(draftToHtml(convertToRaw(editorState.getCurrentContent())));  
    // CreateCommentVariables.variables.text = stateToHTML(editorState.getCurrentContent());
    //.this.setState({ editorState });
    // if (props.loginStatus == 0) {
    //   props.setLoginModalStatus(true);
    //   settitle("")
    // }
    //else {
    { console.log(title, '------------------->') }
    CreateCommentVariables.variables.text = title//submitComment;
    CreateCommentVariables.variables.parent_content_id =
      props.parent_content_id;

    if (props.initial != "main") {
      CreateCommentVariables.variables.clik = props.topComment.clik
        ? props.topComment.clik
        : null;
    } else {
      if (click == null) {
        CreateCommentVariables.variables.clik = null;
      } else if (click == "Everyone" || click == "all") {
        CreateCommentVariables.variables.clik = null;
      } else {
        CreateCommentVariables.variables.clik = click
          .toLowerCase()
          .replace("#", "");
      }
    }
    // setshowsubmitbutton(false);
    applloClient
      .mutate({
        mutation: CreateCommentMutation,
        ...CreateCommentVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        settitle("");
        setclick(null);
        //props.closeModalBySubmit(props.initial, res.data);
        //---------------------------------- create new comment push to comment list ------------------------------
        let prevCommentList = props.PostCommentDetails;
        let parentId = props.parent_content_id;
        let createData = {
          node: { ...res.data.comment_create.comment },
          comments: {
            edges: []
          }
        };
        if (parentId.startsWith("Comment")) {
          const updateArray = updateNestedArray(
            prevCommentList,
            parentId,
            createData
          );
          props.setPostCommentDetails(updateArray);
        } else {
          //root comments
          props.setPostCommentDetails([createData, ...prevCommentList]);
        }

        //----------------------------------------------------------------------------------------------------------
      })
      .catch(e => {
        console.log(e.message.toLowerCase().replace("graphql error: ", ""));
      });
    // const editorState = EditorState.push(this.state.editorState, ContentState.createFromText(''));
    // onChange(EditorState.push(this.state.editorState, ContentState.createFromText('')));
    setPlaceholderState(true)
    //}
  };

  const toogletopictooltip = () => {
    if (showtopictooltip == false) {
      setshowtopictooltip(true);
    } else {
      setshowtopictooltip(false);
    }
  };

  useEffect(() => {
    document.addEventListener('touchstart', function () { }, { passive: false })
    let data = [
      {
        label: "Everyone",
        value: "all",
        key: -1
      }
    ];
    if (
      props.profileData &&
      props.profileData.getIn(["my_users", "0", "cliks_followed"]).size > 0
    ) {
      props.profileData &&
        props.profileData
          .getIn(["my_users", "0", "cliks_followed"])
          .map((item, index) => {
            if (
              item.getIn(["member_type"]) == "SUPER_ADMIN" ||
              item.getIn(["member_type"]) == "ADMIN" ||
              item.getIn(["member_type"]) == "MEMBER"
            ) {
              data.push({
                label:
                  "#" + capitalizeFirstLetter(item.getIn(["clik", "name"])),
                value: item.getIn(["clik", "name"]),
                key: index,
                color: "#4169e1"
              });
            }
          });
    }

    setitem(data);
    setclick(data.length > 0 ? data[0].label : null);
  }, []);

  useEffect(() => {
    if (props.title) {
      //e.current.focus();
    } else {
      //e.current.focus();
    }
  }, [props.title]);

  const goToUserProfile = async username => {
    props.onClose();
    await props.userId({
      username: username,
      type: "feed"
    });
    await props.navigation.navigate("profile", {
      username: username,
      type: "feed"
    });
  };
  const goToProfile = id => {
    props.clikId({
      id: id
    });
  };
  return (
    <>
      <View
        style={[ButtonStyle.shadowStyle, {
          width: Dimensions.get("window").width <= 750 ? "95%" : '98%',
          backgroundColor: "#fff",
          borderWidth: props.initial == "main" ? 1 : 0,
          borderColor: "#dedede",
          borderRadius: Dimensions.get("window").width <= 750 ? 0 : 10,
          // marginVertical: props.initial == "main" ? 10 : 0,
          marginHorizontal: Dimensions.get("window").width <= 750 ? 10 : 0,
          paddingHorizontal: Dimensions.get("window").width <= 750 ? 10 : 0,
        }]}
      >
        <View
          style={{
            marginTop: props.initial == "main" ? 10 : 0,
            marginBottom: props.initial == "main" ? 10 : 0,
            flexDirection: "row",
            alignSelf: "center"
          }}
        >
          {props.initial == "main" &&
            item != null &&
            item.length > 0 &&
            item[0] != "" && (
              <View
                style={{
                  flexDirection: "row",
                  width: "100%"
                }}
              >
                <Text
                  style={{
                    alignSelf: "center",
                    marginRight: 10,
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    fontSize: 15
                  }}
                >
                  Discuss With:
                </Text>
                {/* <View
                  style={{
                    width: 150,
                    borderRadius: 8,
                    backgroundColor: "#E8F5FA"
                  }}
                > */}
                <RNPickerSelect
                  placeholder={{}}
                  items={item}
                  onValueChange={(itemValue, itemIndex) => {
                    setclick(itemValue);
                  }}
                  onUpArrow={() => {
                    inputRefs.name.focus();
                  }}
                  onDownArrow={() => {
                    inputRefs.picker2.togglePicker();
                  }}
                  style={{
                    ...styles,
                    iconContainer: {
                      top: 10,
                      right: 10
                    }
                    // viewContainer: {
                    //   alignSelf: "center",
                    //   justifyContent: "center",
                    //   alignItems: "center",
                    //   alignContent: "center",
                    //   textAlign: "center",
                    //   textAlignVertical: "center",
                    //   width: 150,
                    //   flex: 1,
                    //   flexDirection: "row"
                    // }
                  }}
                  value={click}
                  ref={el => {
                    inputRefs.picker = el;
                  }}
                // useNativeAndroidPickerStyle={false}
                // Icon={() => {
                //   return (
                //     <Icon
                //       name="caret-down"
                //       size={15}
                //       type="font-awesome"
                //       color="#4169e1"
                //       // iconStyle={{
                //       //   justifyContent: "center",
                //       //   alignItems: "center",
                //       //   alignSelf: "center"
                //       // }}
                //       // containerStyle={{
                //       //   alignSelf: "center"
                //       // }}
                //     />
                //   );
                // }}
                // useNativeAndroidPickerStyle={{
                //   fontFamily: ConstantFontFamily.MontserratBoldFont
                // }}
                // pickerProps={{
                //   itemStyle: {
                //     alignSelf: "center",
                //     justifyContent: "center",
                //     alignItems: "center",
                //     alignContent: "center",
                //     textAlign: "center",
                //     textAlignVertical: "center",
                //     width: 150
                //   }
                // }}
                />
                {/* </View> */}
              </View>
            )}
        </View>
        <Text
          style={{
            color: title.length < 140 ? "#de5246" : '#009B1A',
            fontSize: 13,
            fontFamily: ConstantFontFamily.defaultFont,
            marginTop: 2,
            zIndex: -10,
            alignSelf: 'flex-end'
          }}
        >
          {title ? title.length < 140 ? `${140 - title.length} ${titleContent}` : `${2000 - title.length} ${titleContent}` : ''}
        </Text>
        <View
          style={{
            height: Platform.OS != "web" ? hp("25%") : null
          }}
        >
          <View style={{ flex: 1 }}>
            <View
              onFocus={() => {
                return setBorderColor("black");
              }}
              onBlur={() => {
                return setBorderColor("#c5c5c5");
              }}
              style={{
                borderWidth: 1,
                borderColor: `${getBorderColor}`,
                borderRadius: 6,
                ...Platform.select({
                  web: {
                    marginHorizontal: 10
                  },
                  android: {
                    marginTop: 10
                  },
                  ios: {
                    marginTop: 10
                  }
                })
              }}
            >
              <TextInput
                value={title}
                multiline={true}
                numberOfLines={Platform.OS === "ios" ? null : 4}
                placeholder={placeholderState ? "Write an insightful comment (longer than 140 characters)." : ''}
                style={{
                  padding: 5,
                  color: "#000",
                  fontFamily: ConstantFontFamily.VerdanaFont,
                  fontSize: 13,
                  width: "100%",
                  borderRadius: 6,
                  //minHeight: Platform.OS === "ios" && 4 ? 10 * 4 : null
                  minHeight: 100,
                  //overflowY: title.length > 80 ? 'auto' : 'unset'
                }}
                onChangeText={title => {
                  //debugger
                  console.log(title, '------------------------->');
                  settitle(title)
                  if (title.length == 0) {
                    setPlaceholderState(true)
                  }
                }
                }

              // ref={inputTxt}
              // autoFocus = {true}
              />
              <View >
                {/* <View style={{
                  flexDirection: 'row', justifyContent: 'space-around', padding: 10, borderBottomWidth: 1,
                  borderRadius: 6,
                  backgroundColor: '#f9f9f9',
                  borderColor: `#c8ccd0`,
                }}>
                  <Text
                    style={{
                      color: colorState[0].type == 'UNDERLINE' ? colorState[0].state ? "#4169e1" : "#000" : "#000",
                      //fontWeight: "bold",
                      //fontSize: 17,
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      cursor: "pointer"
                    }}
                    onMouseDown={(e) => {
                      e.preventDefault()
                      onChange(RichUtils.toggleInlineStyle(editorState, 'UNDERLINE'));
                      settitle(editorState.getCurrentContent().getPlainText('\u0001'));
                      let newArray = colorState.map((ele) => {
                        if (ele.type == 'UNDERLINE') {
                          ele.state = !ele.state
                          return ele
                        }
                        else {
                          return ele
                        }
                      })
                      setColorState([...newArray]);
                    }}
                  >
                    <Icon
                      //color={"#000"}
                      color={colorState[0].type == 'UNDERLINE' ? colorState[0].state ? "#4169e1" : "#000" : "#000"}
                      //iconStyle={{ paddingLeft: 15 }}
                      name="underline"
                      type="font-awesome"
                      size={17}
                    />
                  </Text>
                  <Text
                    style={{
                      color: colorState[1].type == 'BOLD' ? colorState[1].state ? "#4169e1" : "#000" : "#000",
                      //fontWeight: "bold",
                      //fontSize: 17,
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      cursor: "pointer"
                    }}
                    onMouseDown={(e) => {
                      e.preventDefault();
                      onChange(RichUtils.toggleInlineStyle(editorState, 'BOLD'));
                      settitle(editorState.getCurrentContent().getPlainText('\u0001'))
                      let newArray = colorState.map((ele) => {
                        if (ele.type == 'BOLD') {
                          ele.state = !ele.state
                          return ele
                        }
                        else {
                          return ele
                        }
                      })
                      setColorState([...newArray]);
                    }}
                  >
                    <Icon
                      color={colorState[1].type == 'BOLD' ? colorState[1].state ? "#4169e1" : "#000" : "#000"}
                      //color={"#4169e1"}
                      name="bold"
                      type="font-awesome"
                      size={15}
                    />
                  </Text>
                  <Text
                    style={{
                      color: colorState[2].type == 'ITALIC' ? colorState[2].state ? "#4169e1" : "#000" : "#000",
                      //fontWeight: "bold",
                      //fontSize: 17,
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      cursor: "pointer"
                    }}
                    onMouseDown={(e) => {
                      e.preventDefault();
                      onChange(RichUtils.toggleInlineStyle(editorState, 'ITALIC'));
                      // onChange(RichUtils.toggleInlineStyle(editorState, 'CODE'));
                      settitle(editorState.getCurrentContent().getPlainText('\u0001'))
                      let newArray = colorState.map((ele) => {
                        if (ele.type == 'ITALIC') {
                          ele.state = !ele.state
                          return ele
                        }
                        else {
                          return ele
                        }
                      })
                      setColorState([...newArray]);
                    }}
                  >
                    <Icon
                      //color={"#000"}
                      color={colorState[2].type == 'ITALIC' ? colorState[2].state ? "#4169e1" : "#000" : "#000"}
                      //iconStyle={{ paddingLeft: 15 }}
                      name="italic"
                      type="font-awesome"
                      size={15}
                    />
                  </Text>
                  <Text
                    style={{
                      color: colorState[3].type == 'blockquote' ? colorState[3].state ? "#4169e1" : "#000" : "#000",
                      //fontWeight: "bold",
                      fontSize: 17,
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      cursor: "pointer"
                    }}
                    onMouseDown={(e) => {
                      e.preventDefault();
                      onChange(RichUtils.toggleBlockType(
                        editorState,
                        'blockquote'
                      ));
                      settitle(editorState.getCurrentContent().getPlainText('\u0001'))
                      let newArray = colorState.map((ele) => {
                        if (ele.type == 'blockquote') {
                          ele.state = !ele.state
                          return ele
                        }
                        else {
                          return ele
                        }
                      })
                      setColorState([...newArray]);
                    }}
                  >

                    <Icon
                      //color={"#000"}
                      color={colorState[3].type == 'blockquote' ? colorState[3].state ? "#4169e1" : "#000" : "#000"}
                      //iconStyle={{ paddingLeft: 15 }}
                      name="quote-right"
                      type="font-awesome"
                      size={10}
                    />
                  </Text>
                  <Text
                    style={{
                      color: colorState[4].type == 'code-block' ? colorState[4].state ? "#4169e1" : "#000" : "#000",
                      //fontWeight: "bold",
                      fontSize: 17,
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      cursor: "pointer"
                    }}
                    onMouseDown={(e) => {
                      e.preventDefault();
                      onChange(RichUtils.toggleBlockType(
                        editorState,
                        'code-block'
                      ));
                      //onChange(RichUtils.toggleInlineStyle(editorState, 'CODE'));
                      settitle(editorState.getCurrentContent().getPlainText('\u0001'))
                      let newArray = colorState.map((ele) => {
                        if (ele.type == 'code-block') {
                          ele.state = !ele.state
                          return ele
                        }
                        else {
                          return ele
                        }
                      })
                      setColorState([...newArray]);
                    }}
                  >
                    <Icon
                      //color={"#000"}
                      color={colorState[4].type == 'code-block' ? colorState[4].state ? "#4169e1" : "#000" : "#000"}
                      //iconStyle={{ paddingLeft: 15 }}
                      name="code"
                      type="font-awesome"
                      size={20}
                    />
                    {/* <Image
                            source={require("../assets/image/code_block.PNG")}
                            style={{
                              width: 15,
                              height: 20,
                              //borderRadius: 30,
                              borderWidth: 1,
                              borderColor: "#000",
                              alignSelf: "center",
                             // marginBottom: 10
                            }}
                          /> 
                  </Text>
                  <Text
                    style={{
                      color: colorState[5].type == 'unordered-list-item' ? colorState[5].state ? "#4169e1" : "#000" : "#000",
                      fontWeight: "bold",
                      fontSize: 17,
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      cursor: "pointer"
                    }}
                    onMouseDown={(e) => {
                      e.preventDefault();
                      onChange(RichUtils.toggleBlockType(
                        editorState,
                        'unordered-list-item'
                      ));
                      settitle(editorState.getCurrentContent().getPlainText('\u0001'))
                      let newArray = colorState.map((ele) => {
                        if (ele.type == 'unordered-list-item') {
                          ele.state = !ele.state
                          return ele
                        }
                        else {
                          return ele
                        }
                      })
                      setColorState([...newArray]);
                    }}
                  >
                    <Icon
                      //color={"#000"}
                      color={colorState[5].type == 'unordered-list-item' ? colorState[5].state ? "#4169e1" : "#000" : "#000"}
                      //iconStyle={{ paddingLeft: 15 }}
                      name="list-ul"
                      type="font-awesome"
                      size={20}
                    />
                  </Text>

                </View> */}

                {/* <View style={{
                  padding: 5,
                  color: title.length > 0 ? "black" : "gray",
                  fontFamily: ConstantFontFamily.VerdanaFont,
                  fontSize: 13,
                  width: "100%",
                  borderRadius: 6,
                  overflow: title.length < 144 ? 'hidden' : 'auto',
                  height: 85
                }}
                  onMouseEnter={() => {
                    onChange(RichUtils.toggleInlineStyle(editorState, 'HIGHLIGHT'))
                  }}
                >
                  <Editor
                    editorState={editorState}
                    handleKeyCommand={() => console.log('=========================================handleKeyCommand')}
                    onChange={(ele) => {
                      console.log(ele);
                      settitle(ele.getCurrentContent().getPlainText('\u0001'))
                      return onChange(ele)
                    }}
                    placeholder="Write an insightful comment."
                    customStyleMap={styleMap}
                    blockStyleFn={myBlockStyleFn}

                  />
                </View> */}
                <View style={{ flexDirection: 'column' }}>
                  {/* <Editor
                    editorState={editorState}
                    wrapperClassName="demo-wrapper"
                    editorClassName="demo-editor"
                    onEditorStateChange={onEditorStateChange}
                    onBlur ={() => {
                      if (editorState.getCurrentContent().getPlainText('\u0001').length == 0) {
                      setPlaceholderState(true)
                    }}}
                    onChange={() => {
                      document.querySelector(".demo-editor").scrollTop = parseInt(document.querySelector(".demo-editor").scrollHeight) + parseInt(document.querySelector(".demo-editor").scrollHeight);
                      //console.log(editorState.getCurrentContent().getPlainText('\u0001').length);
                      if (editorState.getCurrentContent().getPlainText('\u0001').length == 0) {
                        setPlaceholderState(true)
                      }
                    }
                    }
                    editorStyle={{ height: "125px", overflowY: title.length > 0 ? 'auto' : 'unset', fontSize: '13px',color:'#000', fontFamily: ConstantFontFamily.VerdanaFont, paddingLeft: '10px' }}
                    toolbar={
                      {
                        options: ['inline', 'blockType', 'list', 'link'],
                        inline:
                        {
                          options: ['bold', 'italic', 'underline']
                        },
                        blockType: {
                          inDropdown: false,
                          options: ['Blockquote', 'Code'],
                          className: undefined,
                          component: undefined,
                          dropdownClassName: undefined,
                        },
                        list: {
                          options: ['unordered'],
                        },
                        link: {
                          options: ['link'],
                        }
                      }
                    }
                    placeholder={placeholderState ? "Write an insightful comment (longer than 140 characters)." : ''}
                  /> */}
                </View>
              </View>
            </View>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                width: "100%",
                alignItems: "flex-start",
                paddingLeft: 10,
                paddingTop: 5,
                zIndex: -1
              }}
            >
              {/* {props.profileData &&
                  props.profileData.getIn([
                    "my_users",
                    "0",
                    "user",
                    "full_name"
                  ]) != props.profileData &&
                  props.profileData.getIn([
                    "my_users",
                    "0",
                    "user",
                    "username"
                  ]) && (
                    <TouchableOpacity
                      onPress={() =>
                        goToUserProfile(
                          props.profileData &&
                            props.profileData.getIn([
                              "my_users",
                              "0",
                              "user",
                              "username"
                            ])
                        )
                      }
                    >
                      <Text
                        style={{
                          color: "#959FAA",
                          fontWeight: "bold",
                          fontSize: 13,
                          fontFamily: ConstantFontFamily.defaultFont
                        }}
                      >
                        {props.profileData &&
                          props.profileData.getIn([
                            "my_users",
                            "0",
                            "user",
                            "full_name"
                          ])}
                      </Text>
                    </TouchableOpacity>
                  )} */}
              <View style={{ flexDirection: "column" }}>
                <Hoverable>
                  {isHovered => (
                    <TouchableOpacity
                      onPress={() =>
                        goToUserProfile(
                          props.profileData &&
                          props.profileData.getIn([
                            "my_users",
                            "0",
                            "user",
                            "username"
                          ])
                        )
                      }
                    >
                      <Text
                        style={{
                          color: "#6D757F",
                          fontSize: 13,
                          fontFamily: ConstantFontFamily.VerdanaFont,
                          textDecorationLine:
                            isHovered == true ? "underline" : "none"
                        }}
                      >
                        {props.profileData &&
                          "@" +
                          props.profileData.getIn([
                            "my_users",
                            "0",
                            "user",
                            "username"
                          ])}
                      </Text>
                    </TouchableOpacity>
                  )}
                </Hoverable>

              </View>
              {/* <View
              style={{
                flexDirection: "row",
                width: "100%",
                marginVertical: 10,
                paddingHorizontal: 10,
                alignItems: "flex-end"
              }}
            > */}
              <View
                style={{
                  justifyContent: "flex-end",
                  alignItems: "center",
                  alignSelf: "center",
                  //paddingBottom: 10,
                  paddingHorizontal: 10,
                  flexDirection: "row"
                }}
              >
                {props.initial != "main" && (
                  // <TouchableOpacity
                  //   style={[
                  //     ButtonStyle.backgroundStyle,
                  //     { backgroundColor: "#de5246", marginRight: 10 }
                  //   ]}
                  //   // style={{
                  //   //   backgroundColor: "#de5246",
                  //   //   justifyContent: "center",
                  //   //   alignItems: "center",
                  //   //   borderRadius: 6,
                  //   //   borderColor: "#000",
                  //   //   height: 35,
                  //   //   marginRight: 10,
                  //   //   width: "40%"
                  //   // }}
                  //   onPress={() => {
                  //     props.onClose();
                  //   }}
                  // >
                  //   <Text
                  //     style={{
                  //       color: "#fff",
                  //       borderRadius: 6,
                  //       // marginHorizontal: 20,
                  //       marginVertical: 8,
                  //       fontFamily: ConstantFontFamily.MontserratBoldFont
                  //     }}
                  //   >
                  //     Cancel
                  //   </Text>
                  // </TouchableOpacity>
                  <Button
                    title="Cancel"
                    titleStyle={ButtonStyle.cwtitleStyle}
                    buttonStyle={[
                      ButtonStyle.crbackgroundStyle,
                      // {
                      //   backgroundColor: title.length < 140 ? "#e1e1e1" : "#009B1A"
                      // }
                    ]}
                    containerStyle={[
                      ButtonStyle.containerStyle,
                      {
                        marginTop: 0,
                        marginBottom: 5
                      }
                    ]}
                    //containerStyle={ButtonStyle.containerStyle}                  
                    onPress={() => {
                      props.onClose();
                    }}

                  //onClick={() => onChange(EditorState.push(editorState, ContentState.createFromText('')))}
                  />
                )}

                <Button
                  title="Submit"
                  titleStyle={title.length < 140 ? ButtonStyle.cdtitleStyle : ButtonStyle.cwtitleStyle}
                  buttonStyle={title.length < 140 ? ButtonStyle.cdbackgroundStyle : ButtonStyle.cgbackgroundStyle

                    //   [
                    //   ButtonStyle.backgroundStyle,
                    //   {
                    //     backgroundColor: title.length < 140 ? "#e1e1e1" : "#009B1A"
                    //   }
                    // ]
                  }
                  containerStyle={[
                    ButtonStyle.containerStyle,
                    {
                      marginTop: 0,
                      marginBottom: 5
                    }
                  ]}
                  //containerStyle={ButtonStyle.containerStyle}
                  disabled={
                    // (props.initial == "main" &&
                    //   title.replace(/ /g, "").length <= 50) ||
                    //   (props.initial != "main" &&
                    //     title.replace(/ /g, "").length <= 25) ||
                    //   // showsubmitbutton == false ||
                    //   props.loginStatus == 0
                    title.length < 140 ? true : false
                  }
                  onPress={() => {
                    submitComment()
                    //onChange(EditorState.push(editorState, ContentState.createFromText('')))
                  }}

                //onClick={() => onChange(EditorState.push(editorState, ContentState.createFromText('')))}
                />
                {/* <TouchableOpacity
                  style={[
                    ButtonStyle.backgroundStyle,
                    {
                      backgroundColor: title.length < 140 ? "#e1e1e1" : "#fff"
                    }
                  ]}
                  // style={{
                  //   backgroundColor:
                  //     (props.initial == "main" &&
                  //       title.replace(/ /g, "").length <= 50) ||
                  //     (props.initial != "main" &&
                  //       title.replace(/ /g, "").length <= 25)
                  //       ? "#e1e1e1"
                  //       : "#000",
                  //   justifyContent: "center",
                  //   alignItems: "center",
                  //   borderRadius: 6,
                  //   borderColor: "#000",
                  //   height: 35,
                  //   width: "40%"
                  // }}
                  onPress={() => submitComment()}
                  disabled={
                    // (props.initial == "main" &&
                    //   title.replace(/ /g, "").length <= 50) ||
                    //   (props.initial != "main" &&
                    //     title.replace(/ /g, "").length <= 25) ||
                    //   // showsubmitbutton == false ||
                    //   props.loginStatus == 0
                    title.length < 140 ? true : false
                  }
                >
                  <Text
                    style={[
                      ButtonStyle.titleStyle,
                      {
                        // marginHorizontal: 20,
                        marginVertical: 8
                      }
                    ]}
                    // style={{
                    //   color: "#fff",
                    //   borderRadius: 6,
                    //   marginHorizontal: 30,
                    //   marginVertical: 10,
                    //   fontFamily: ConstantFontFamily.MontserratBoldFont
                    // }}
                  >
                    Submit
                  </Text>
                </TouchableOpacity> */}
              </View>
            </View>

            {/* </View> */}
          </View>
        </View>
      </View>
    </>
  );
};
const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  PostCommentDetails: state.PostCommentDetailsReducer.get("PostCommentDetails"),
  loginStatus: state.UserReducer.get("loginStatus")
});

const mapDispatchToProps = dispatch => ({
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  clikId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  CreateCommentCard
);

const styleMap = {
  'CODE': {
    'backgroundColor': 'red',
    'fontFamily': '"Inconsolata", "Menlo", "Consolas", monospace',
    'fontSize': 16,
    'padding': 2,
  },
  'HIGHLIGHT': {
    //'backgroundColor': 'red',
    //'padding': 5,
    fontFamily: ConstantFontFamily.VerdanaFont,
    // fontSize: 13,
    // lineHeight: 1.5,
    //'fontSize': 18,
    'width': "100%",
    'borderRadius': 6,
  },
  'superFancyBlockquote': {
    'color': '#999',
    'font-family': 'Hoefler Text',
    'font-style': 'italic',
    'text-align': 'center',
  }
};

const styles = StyleSheet.create({
  blockQuote: {
    //borderLeft: '5px solid #eee',
    backgroundColor: 'red',
    color: '#666',
    fontFamily: 'Hoefler Text',
    //fontStyle: 'italic',
    marginVertical: 16,
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  container: {
    flex: 1,
    backgroundColor: ConstantColors.customeBackgroundColor,
    padding: Platform.OS == "web" ? 10 : 0
  },
  totalFollowCount: {
    marginTop: 15,
    color: "#ffffff",
    alignItems: "center"
  },
  containerFollow: {
    justifyContent: "flex-end",
    marginRight: 10
  },
  TotalcontainerFollow: {
    flexDirection: "row",
    justifyContent: "flex-end",
    marginRight: 12,
    marginBottom: 10
  },
  profileContainer: {
    marginTop: 10
  },
  totalProfileContainer: {
    alignItems: "center",
    justifyContent: "center"
  },
  image: {
    width: "100%",
    height: hp("40%")
  },
  followbtnStyle: {
    height: 30,
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#1A1819",
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 30,
    paddingRight: 30,
    borderColor: "#fff",
    borderWidth: 2,
    borderRadius: 5
  },
  bgContainerProfile: {
    margin: 0,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  usertext: {
    color: "#fff",
    fontSize: 12,
    fontFamily: ConstantFontFamily.defaultFont
  },
  userblacktext: {
    color: "#fff",
    fontSize: 12,
    fontFamily: ConstantFontFamily.defaultFont
  },
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    flexDirection: "row",
    width: 150,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    textAlign: "center",
    textAlignVertical: "center",
    borderRadius: 4,
    borderWidth: 0,
    backgroundColor: "#E8F5FA",
    color: "#4169e1",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  inputAndroid: {
    paddingVertical: 8,
    paddingRight: 20,
    paddingLeft: 10,
    flexDirection: "row",
    width: 150,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    textAlign: "center",
    textAlignVertical: "center",
    borderRadius: 8,
    borderWidth: 0,
    backgroundColor: "#E8F5FA",
    color: "#4169e1",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },

  TextStyle: {
    textAlign: "center"
  }
});
function updateNestedArray(input, parentId, newData) {
  let key, value, outputValue;
  if (input === null || typeof input !== "object") {
    return input;
  }
  outputValue = Array.isArray(input) ? [] : {};
  for (key in input) {
    value = input[key];
    if (
      value !== null &&
      !!value["node"] &&
      !!value["comments"] &&
      value.node.id === parentId
    ) {
      let clone = { ...value };
      value.comments.edges.unshift(newData);
      outputValue[key] = updateNestedArray(clone, parentId, newData);
    } else {
      outputValue[key] = updateNestedArray(value, parentId, newData);
    }
  }
  return outputValue;
}
