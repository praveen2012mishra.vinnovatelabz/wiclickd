import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Button, Icon, Tooltip } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import applloClient from "../client";
import "../components/Firebase";
import ConstantFontFamily from "../constants/FontFamily";
import { InviteToClikMutation } from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { UserQueryMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { InviteToClikVariables } from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { UserDetailsVariables } from "../graphqlSchema/graphqlVariables/UserVariables";
import { SearchUserMutation } from "../graphqlSchema/graphqlMutation/SearchMutation";
import { SearchUserVariables } from "../graphqlSchema/graphqlVariables/SearchVariables";
import Overlay from "react-native-modal-overlay";
import Modal from "modal-enhanced-react-native-web";
import InviteUserMsgModal from "./InviteUserMsgModal";
import ButtonStyle from "../constants/ButtonStyle";

class InviteUserModal extends React.Component {
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.state = {
      SelectRoleItems: "MEMBER",
      username: "",
      showError: false,
      MutipleUserList: [],
      UpdateMutipleUserList: [],
      showtooltip: false,
      RoleItems: [
        {
          label: "Member",
          value: "MEMBER",
          key: 0
        },
        {
          label: "Admin",
          value: "ADMIN",
          key: 1
        },
        {
          label: "Super Admin",
          value: "SUPER_ADMIN",
          key: 2
        }
      ],
      UserList: [],
      customAddMemberArray: [
        {
          username: "",
          userid_or_email: "",
          pic: "",
          showUserModal: false,
          roleName: "Member",
          member_type: "MEMBER"
        }
      ],
      status: "default",
      user_msg: "",
      showMsgModal: false
    };
  }

  checkUser = async () => {
    let value =
      this.state.username.charAt(0) == "@"
        ? this.state.username.substr(1)
        : this.state.username;
    UserDetailsVariables.variables.id = "User:" + value;
    try {
      await applloClient
        .query({
          query: UserQueryMutation,
          ...UserDetailsVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState({
            MutipleUserList: this.state.MutipleUserList.concat([
              {
                userid_or_email: res.data.user.username,
                member_type: this.state.SelectRoleItems,
                pic: res.data.user.profile_pic
              }
            ])
          });
          this.setState({
            UpdateMutipleUserList: this.state.UpdateMutipleUserList.concat([
              {
                userid_or_email: res.data.user.id,
                member_type: this.state.SelectRoleItems
              }
            ])
          });
          this.setState({
            showError: false,
            username: ""
          });
        });
    } catch (e) {
      console.log(e);
      this.setState({
        showError: true,
        username: ""
      });
    }
  };

  toogletooltip = () => {
    if (this.state.showtooltip == false) {
      this.setState({
        showtooltip: true
      });
    } else {
      this.setState({
        showtooltip: false
      });
    }
  };

  deleteUser = async index => {
    let updatedList1 = this.state.MutipleUserList;
    updatedList1.splice(index, 1);
    this.setState({ MutipleUserList: updatedList1 });
    let updatedList2 = this.state.UpdateMutipleUserList;
    updatedList2.splice(index, 1);
    this.setState({ UpdateMutipleUserList: updatedList2 });
  };

  sendInvite = async () => {
    let MemberArray = [];
    for (let i = 0; i < this.state.customAddMemberArray.length; i++) {
      MemberArray.push({
        userid_or_email: this.state.customAddMemberArray[i].userid_or_email,
        member_type: this.state.customAddMemberArray[i].member_type
      });
    }
    InviteToClikVariables.variables.clik_id = this.props.ClikInfo.get("id");
    InviteToClikVariables.variables.invited_users = MemberArray;
    try {
      await applloClient
        .query({
          query: InviteToClikMutation,
          ...InviteToClikVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          if (res.data.clik_invite.status.success == true) {
            await this.setState({
              customAddMemberArray: [
                {
                  username: "",
                  userid_or_email: "",
                  pic: "",
                  showUserModal: false,
                  roleName: "Member",
                  member_type: "MEMBER"
                }
              ],
              status: "success",
              user_msg:
                "Successfully sent invitation to " +
                MemberArray.length +
                " people.",
              showMsgModal: true
            });
          } else {
            await this.setState({
              status: "failure",
              user_msg: res.data.clik_invite.status.user_msg,
              showMsgModal: true
            });
          }
        });
    } catch (e) {
      console.log(e);
      await this.setState({
        status: "failure",
        user_msg: e.message,
        showMsgModal: true
      });
    }
  };

  customRenderUserSuggestion = (value, index) => {
    let inputArray = [...this.state.customAddMemberArray];
    inputArray[index].username = value;
    inputArray[index].showUserModal = true;

    this.setState({ customAddMemberArray: inputArray });

    SearchUserVariables.variables.prefix = value;
    applloClient
      .query({
        query: SearchUserMutation,
        ...SearchUserVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        this.setState({ UserList: res.data.search.users });
      });
  };

  checkSelectedUser = async (value, index) => {
    UserDetailsVariables.variables.id = "User:" + value;

    try {
      await applloClient
        .query({
          query: UserQueryMutation,
          ...UserDetailsVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState({
            MutipleUserList: this.state.MutipleUserList.concat([
              {
                userid_or_email: res.data.user.username,
                member_type: this.state.SelectRoleItems,
                pic: res.data.user.profile_pic
              }
            ])
          });
          //-----set userdetails on the custom field
          let inputArray = [...this.state.customAddMemberArray];
          inputArray[index].username = res.data.user.username;
          inputArray[index].userid_or_email = res.data.user.id;
          inputArray[index].showUserModal = false;
          inputArray[index].pic = res.data.user.profile_pic;

          this.setState({ customAddMemberArray: inputArray });

          this.setState({
            UpdateMutipleUserList: this.state.UpdateMutipleUserList.concat([
              {
                userid_or_email: res.data.user.id,
                member_type: this.state.SelectRoleItems
              }
            ])
          });
          this.setState({
            showError: false,
            username: ""
          });
        });
    } catch (e) {
      console.log(e);
      this.setState({
        showError: true,
        username: ""
      });
    }
  };

  addcustomMember() {
    this.setState({
      customAddMemberArray: [
        ...this.state.customAddMemberArray,
        {
          username: "",
          userid_or_email: "",
          pic: "",
          showUserModal: false,
          roleName: "Member",
          member_type: "MEMBER"
        }
      ]
    });
  }

  removeCustomMember = index => {
    this.state.customAddMemberArray.splice(index, 1);
    this.setState({ customAddMemberArray: this.state.customAddMemberArray });
  };

  setRoleType = (value, name, index) => {
    let inputArray = [...this.state.customAddMemberArray];
    inputArray[index].roleName = name;
    inputArray[index].member_type = value;
    this.setState({ customAddMemberArray: inputArray });
  };

  onClose = () => {
    this.setState({
      showMsgModal: false
    });
  };
  render() {
    return (
      <View
        style={{
          width: "100%"
        }}
      >
        {this.state.showMsgModal == true ? (
          Platform.OS !== "web" ? (
            <Overlay
              animationType="zoomIn"
              childrenWrapperStyle={{
                margin: 0,
                padding: 0
              }}
              visible={this.state.showMsgModal}
              onClose={this.onClose}
              closeOnTouchOutside
              children={
                <InviteUserMsgModal
                  onClose={this.onClose}
                  status={this.state.status}
                  user_msg={this.state.user_msg}
                />
              }
              childrenWrapperStyle={{
                padding: 0
              }}
            />
          ) : (
            <Modal
              isVisible={this.state.showMsgModal}
              onBackdropPress={this.onClose}
              style={{
                marginHorizontal: "30%"
              }}
            >
              <InviteUserMsgModal
                onClose={this.onClose}
                status={this.state.status}
                user_msg={this.state.user_msg}
              />
            </Modal>
          )
        ) : null}

        {this.props.showHeader != false && (
          <Hoverable>
            {isHovered => (
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  flex: 1,
                  position: "absolute",
                  zIndex: 999999,
                  left: 0,
                  top: 0
                }}
                onPress={this.props.onClose}
              >
                <Icon
                  color={isHovered == true ? "rgba(256,256,256,0.4)" : "#000"}
                  iconStyle={{
                    color: "#fff",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  reverse
                  name="close"
                  type="antdesign"
                  size={16}
                />
              </TouchableOpacity>
            )}
          </Hoverable>
        )}
        {this.props.showHeader != false && (
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              backgroundColor: "#000",
              alignItems: "center",
              height: 50
            }}
          >
            <Image
              source={
                Platform.OS == "web" &&
                this.props.getCurrentDeviceWidthAction > 750
                  ? require("../assets/image/weclickd-logo.png")
                  : Platform.OS == "web"
                  ? require("../assets/image/weclickd-logo.png")
                  : require("../assets/image/weclickd-logo-only-icon.png")
              }
              style={
                Platform.OS == "web" &&
                this.props.getCurrentDeviceWidthAction > 750
                  ? {
                      height: 30,
                      width: Platform.OS == "web" ? 90 : 30,
                      padding: 0,
                      margin: 0,
                      marginVertical: 10
                    }
                  : {
                      height: 30,
                      width: Platform.OS == "web" ? 90 : 30,
                      padding: 0,
                      margin: 0,
                      marginVertical: 10
                    }
              }
            />
          </View>
        )}
        <View
          style={[ButtonStyle.cardShadowStyle, {
            backgroundColor: "#fff",
            borderRadius: 10,
            // borderColor: "#C5C5C5",
            borderWidth: 0,
            // width: "100%",
            marginBottom: 10
          }]}
        >
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={{
              padding: 10
            }}
          >
            <View
              style={{
                width: "100%",
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              <View
                style={{
                  width: "40%",
                  justifyContent: "flex-start",
                  flexDirection: "row"
                }}
              >
                <Text
                  style={{
                    color: "#000",
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    fontSize: 16,
                    fontWeight: "bold"
                  }}
                >
                  Invite Members
                </Text>
              </View>
              <TouchableOpacity
                style={{
                  width: "60%"
                }}
              >
                <Icon
                  name="plus"
                  type="font-awesome"
                  size={18}
                  containerStyle={{
                    marginLeft: "auto",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  onPress={() => this.addcustomMember()}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: "column"
              }}
            >
              {this.state.customAddMemberArray.map((item, index) => {
                return (
                  <View
                    key={index}
                    style={{ flexDirection: "column", marginTop: 5 }}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        width: "100%",
                        justifyContent: "space-between"
                      }}
                    >
                      <View
                        style={{
                          width: "60%",
                          justifyContent: "flex-start",
                          flexDirection: "row"
                        }}
                      >
                        {item.pic == "" || item.pic == null ? (
                          <Image
                            source={require("../assets/image/default-image.png")}
                            style={{
                              width: 40,
                              height: 40,
                              padding: 0,
                              margin: 5,
                              borderRadius: 20
                            }}
                          />
                        ) : (
                          <Image
                            source={{ uri: item.pic }}
                            style={{
                              width: 40,
                              height: 40,
                              padding: 0,
                              margin: 5,
                              borderRadius: 20
                            }}
                          />
                        )}
                        <TextInput
                          value={item.username}
                          placeholder="Enter username or email"
                          placeholderTextColor="#6D757F"
                          style={[this.state.focusInput ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle,
                            {
                              color: "#000",
                              fontSize: 14,
                              fontWeight: "bold",
                              fontFamily: ConstantFontFamily.defaultFont
                            },
                            {
                              width: "100%",
                              borderRadius: 6,
                              borderColor: "#e1e1e1",
                              borderWidth: 0,
                              marginTop: 5,
                              height: 40,
                              padding: 5
                            }
                          ]}
                          onChangeText={username => {
                            this.customRenderUserSuggestion(username, index);
                          }}
                          onFocus = {()=> this.setState({focusInput : true})}
                          onBlur = {()=> this.setState({focusInput : false})}
                        />
                      </View>
                      <View
                        style={[ButtonStyle.shadowStyle,{
                          width: "25%",
                          justifyContent: "flex-end",
                          borderRadius: 6,
                          borderColor: "#e1e1e1",
                          borderWidth: 0,
                          marginHorizontal: 15,
                          height: 40,
                          marginTop: 5,
                          alignSelf: "center"
                        }]}
                      >
                        <RNPickerSelect
                          placeholder={{}}
                          items={this.state.RoleItems}
                          onValueChange={(itemValue, itemIndex) => {
                            this.setRoleType(
                              itemValue,
                              this.state.RoleItems[itemIndex].label,
                              index
                            );
                            this.setState({
                              SelectRoleItems: itemValue
                            });
                          }}
                          onUpArrow={() => {
                            this.inputRefs.name.focus();
                          }}
                          onDownArrow={() => {
                            this.inputRefs.picker2.togglePicker();
                          }}
                          style={{ ...styles }}
                          ref={el => {
                            this.inputRefs.picker = el;
                          }}
                        />
                      </View>
                      <View
                        style={{
                          width: "5%",
                          alignSelf: "center",
                          justifyContent: "flex-end"
                        }}
                      >
                        <Icon
                          color={"#000"}
                          name="trash"
                          type="font-awesome"
                          size={18}
                          iconStyle={{
                            justifyContent: "center",
                            alignItems: "center",
                            marginLeft: "auto"
                          }}
                          onPress={() => this.removeCustomMember()}
                        />
                      </View>
                    </View>
                    {item.showUserModal &&
                      this.state.UserList.map((item, i) => {
                        return (
                          <View
                            key={item.username}
                            style={{
                              backgroundColor: "#FEFEFA",
                              width: "100%",
                              padding: 5,
                              marginLeft: 40
                            }}
                          >
                            <Text
                              style={{
                                color: "#000",
                                fontFamily: ConstantFontFamily.defaultFont,
                                fontWeight: "bold"
                              }}
                              onPress={() =>
                                this.checkSelectedUser(item.username, index)
                              }
                            >
                              {item.username}
                            </Text>
                          </View>
                        );
                      })}
                  </View>
                );
              })}

              {this.state.showError == true && (
                <Text style={{ color: "red", alignSelf: "center" }}>
                  Profile Does not Exist
                </Text>
              )}
            </View>
          </ScrollView>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginVertical: 10
            }}
          >
            <Button
              color="#fff"
              title="Send Invite"
              titleStyle={ButtonStyle.titleStyle}
              buttonStyle={ButtonStyle.backgroundStyle}
              containerStyle={ButtonStyle.containerStyle}
              onPress={() => this.sendInvite()}
            />
          </View>
        </View>
      </View>
    );
  }
}
export default connect(null, null)(InviteUserModal);

export const styles = {
  LogoImageStyle: {
    height: 80,
    width: 235
  },
  LogoContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    margin: 25
  },
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  },
  inputAndroid: {
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#fff",
    borderRadius: 8,
    color: "#000",
    backgroundColor: "white",
    paddingRight: 30,
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  }
};
