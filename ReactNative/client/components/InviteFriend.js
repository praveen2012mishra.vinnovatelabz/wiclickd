import React from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Clipboard,
  TouchableOpacity,
  Image,
  Dimensions
} from "react-native";
import { Button, Icon } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";
import ConstantFontFamily from "../constants/FontFamily";
import RNPickerSelect from "react-native-picker-select";
import ButtonStyle from "../constants/ButtonStyle";
import { ClikGenerateKeyVariables } from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import applloClient from "../client";
import { ClikGenerateKeyMutation } from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { Hoverable } from "react-native-web-hooks";

class InviteFriend extends React.Component {
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.state = {
      showPrivacyPolicyModal: false,
      RoleItems: [
        // {
        //   label: "Select Role",
        //   value: "",
        //   key: -1
        // },
        {
          label: "Member",
          value: "MEMBER",
          key: 0
        },
        {
          label: "Admin",
          value: "ADMIN",
          key: 1
        },
        {
          label: "Super Admin",
          value: "SUPER_ADMIN",
          key: 2
        }
      ],
      SelectRoleItems: {},
      getShowIdUrl: null,
      SelectCliksItem: "",
      itemCliksList: [],
      MutipleUserList: [],
      showError: false,
      title: "",
      showtooltip: false,
      showAddView: false,
      showCopiedText: false,
      showEmailError: false,
      MutipleClikList: [],
      showsubmitText: false,
      key: [],
      getClicks: [],
      getRole: []
    };
  }

  checkUser = async (item, role, id) => {
    //console.log(item,role,id);
    let getRole = role ? role : "MEMBER";
    this.state.getRole.push({ key: id, value: getRole });
    let newRole = Object.assign([], this.state.getRole);

    this.setState({
      MutipleUserList: this.state.MutipleUserList.concat([
        {
          name: this.state.title,
          clik: this.state.SelectCliksItem,
          role: this.state.SelectRoleItems[id]
        }
      ])
    });
    this.setState({
      showError: false,
      title: "",
      SelectRoleItems: Object.assign({}),
      SelectCliksItem: "",
      showAddView: false,
      showsubmitText: true,
      getShowIdUrl: id,
      getRole: newRole
    });

    this.state.getClicks.push({ key: id, value: item.value });
    let newClicks = Object.assign([], this.state.getClicks);

    ClikGenerateKeyVariables.variables.clik_id = `Clik:${item.value.toUpperCase()}`;
    ClikGenerateKeyVariables.variables.member_type = `${getRole.toUpperCase()}`;
    try {
      await applloClient
        .query({
          query: ClikGenerateKeyMutation,
          ...ClikGenerateKeyVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          let temporaryData = this.state.itemCliksList;
          temporaryData[id].inviteKey =
            res.data.clik_create_invite_key.invite_key;
          //console.log(res,'-------------------------->');
          this.setState({
            status: "Success",
            itemCliksList: temporaryData,
            //key: [...this.state.key,[{key:id,value:res.data.clik_create_invite_key.invite_key}]],
            //Object.assign([],this.state.key,[{[id]:res.data.clik_create_invite_key.invite_key}]),
            getClicks: newClicks
          });
          //console.log(res.data.clik_create_invite_key.invite_key);
          this.writeToClipboardInvite(id);
        });
    } catch (e) {
      console.log(e);
    }
  };

  writeToClipboardInvite = async id => {
    //To copy the text to clipboard
    // let {getClicks,key,getRole}=this.state;
    // let newClicks=getClicks.find((item)=>item.key==id);
    // let newKey=await key.find((item,index)=>item[index]?item[index]['key']:null==id);
    // let newRole=getRole.find((item)=>item.key==id);
    //console.log(newClicks,newRole,newKey);
    await Clipboard.setString(
      "https://" +
      window.location.href
        .replace("http://", "")
        .replace("https://", "")
        .replace("www.", "")
        .split(/[/?#]/)[0] +
      "/clik/" +
      this.newClicks(id) +
      //this.props.cliksDetails.getIn(["data", "clik"]).get("name") +
      "/invite?key=" +
      this.state.itemCliksList[id].inviteKey
    );
  };

  deleteUser = async index => {
    let updatedList = this.state.MutipleUserList;
    updatedList.splice(index, 1);
    this.setState({ MutipleUserList: updatedList });
  };

  deleteClik = async index => {
    let updatedList = this.state.MutipleClikList;
    updatedList.splice(index, 1);
    this.setState({ MutipleClikList: updatedList });
  };

  onClose = () => {
    this.setState({
      showPrivacyPolicyModal: false
    });
  };
  onOpen = () => {
    this.setState({
      showPrivacyPolicyModal: true
    });
  };
  toogletooltip = () => {
    if (this.state.showtooltip == false) {
      this.setState({ showtooltip: true });
    } else {
      this.setState({ showtooltip: false });
    }
  };
  componentDidMount() {
    let data = [
      {
        label: "Select Clik",
        value: "",
        key: -1
      }
    ];
    if (
      this.props.profileData.getIn(["my_users", "0", "cliks_followed"]).size > 0
    ) {
      this.props.profileData
        .getIn(["my_users", "0", "cliks_followed"])
        .map((item, index) => {
          if (item.getIn(["member_type"]) == "SUPER_ADMIN") {
            data.push({
              label: "#" + item.getIn(["clik", "name"]),
              value: item.getIn(["clik", "name"]),
              key: index,
              inviteKey: "",
              showCopiedTextInvite: false,
              icon_pic: item.getIn(["clik", "icon_pic"])
            });
          }
        });
    }
    this.setState({
      itemCliksList: data
    });
  }

  writeToClipboard = async () => {
    //To copy the text to clipboard
    await Clipboard.setString(
      // "https://www.weclikd.com/invite/" +
      //   this.props.profileData.getIn(["my_users", "0", "user", "username"])
      "https://" +
      window.location.href
        .replace("http://", "")
        .replace("https://", "")
        .replace("www.", "")
        .split(/[/?#]/)[0] +
      "/invite/" +
      this.props.profileData.getIn(["my_users", "0", "user", "username"])
    );
    this.setState({
      showCopiedText: true
    });
    let timer = setTimeout(() => {
      this.setState({
        showCopiedText: false
      });
    }, 2000);
  };

  checkValue = () => {
    var letters = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.state.title.match(letters)) {
      this.setState({
        showEmailError: false
      });
    } else {
      this.setState({
        showEmailError: true
      });
      return false;
    }
  };

  addElement = () => {
    this.setState({
      MutipleClikList: this.state.MutipleClikList.concat(0)
    });
  };

  newClicks = id => {
    if (this.state.getClicks.length > 0) {
      let { getClicks } = this.state;
      let letClicks = getClicks.find(item => item.key == id);
      let value = letClicks ? letClicks.value : null;
      return value;
    }
  };

  newKey = id => {
    let { key } = this.state;
    let letKey;
    if (key.length > 0) {
      setInterval(function () {
        letKey = key.find((item, index) =>
          item[index] ? item[index]["key"] : null == id
        );
      }, 4000);

      let value = letKey ? letKey.value : null;
      return value;
    }
  };

  newRole = id => {
    let { getRole } = this.state;
    if (getRole.length > 0) {
      let letRole = getRole.find(item => item.key == id);
      let value = letRole ? letRole.value : null;
      return value;
    }
  };

  CheckPost = id => {
    let temporaryData = this.state.itemCliksList;
    temporaryData[id].showCopiedTextInvite = true;
    this.setState({
      itemCliksList: temporaryData
    });
  };

  render() {
    const { itemCliksList, showError } = this.state;
    const textStyle = styles.usertext;

    return (
      <View
        style={{
          backgroundColor: "transparent",
          paddingHorizontal:10
        }}
      >
        {/* <Text
          style={{
            alignSelf: "center",
            textAlign: "center",
            fontFamily: ConstantFontFamily.defaultFont
          }}
        >
          You have 2 invitations left and use them wisely.
        </Text>
        <Text
          style={{
            alignSelf: "center",
            textAlign: "center",
            fontFamily: ConstantFontFamily.defaultFont
          }}
        >
          Only invite those who will contribute to the quality of the
          discussions.
        </Text> */}

        <Text
          style={{
            color: "#000",
            fontSize: 16,
            fontWeight: "bold",
            fontFamily: ConstantFontFamily.MontserratBoldFont,
            marginTop: 20,
            textAlign: "left"
          }}
        >
          Invite Friends via Link
        </Text>
        <View style={{ flexDirection: "row", justifyContent:  'space-between' }}>
          {this.props.profileData != null ? (<Text
            style={{
              color: "#000",
              fontSize: 14,
              fontWeight: "bold",
              fontFamily: ConstantFontFamily.defaultFont,
              marginTop: 20,
              textAlign: "left",
              // border: "1px solid #c5c5c5",
              borderWidth: 1,
              borderColor: '#c5c5c5',
              paddingHorizontal: 10,
              paddingVertical: 5,
              borderRadius: 15
            }}
          >
            {/* {this.props.profileData != null && */}
            {Dimensions.get("window").width > 750 ? "https://" +
              window.location.href
                .replace("http://", "")
                .replace("https://", "")
                .replace("www.", "")
                .split(/[/?#]/)[0] +
              "/invite/" +
              this.props.profileData.getIn([
                "my_users",
                "0",
                "user",
                "username"
              ]) : ("https://" +
                window.location.href
                  .replace("http://", "")
                  .replace("https://", "")
                  .replace("www.", "")
                  .split(/[/?#]/)[0] +
                "/invite/" +
                this.props.profileData.getIn([
                  "my_users",
                  "0",
                  "user",
                  "username"
                ])).substring(0, 25) + '...'}
            {/* } */}
          </Text>): null }
          <Icon
            color={"#000"}
            iconStyle={{
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center"
            }}
            name="clone"
            type="font-awesome"
            size={18}
            containerStyle={{
              marginTop: 20,
              marginLeft: 40,
              alignSelf: "center"
            }}
            onPress={() => this.writeToClipboard()}
          />
        </View>
        <View>
          {this.state.showCopiedText == true ? (
            <Text
              style={{
                color: "#000",
                fontSize: 14,
                fontWeight: "bold",
                fontFamily: ConstantFontFamily.defaultFont,
                marginTop: 20,
                marginLeft: 10,
                textAlign: "left"
              }}
            >
              {"Invitation Link Copied!"}
            </Text>
          ) : null}
        </View>

        <Text
          style={{
            color: "#000",
            fontSize: 16,
            fontWeight: "bold",
            fontFamily: ConstantFontFamily.MontserratBoldFont,
            marginTop: 20,
            textAlign: "left"
          }}
        >
          Invite Friends to clicks
        </Text>

        <View
          style={{
            marginTop: 15
          }}
        >
          {this.state.itemCliksList
            .filter(ele => ele.key != -1)
            .map((item, id) => (
              <View key={id}>
                <View style={{ flexDirection: "row", alignItems: 'flex-end',marginBottom:Dimensions.get("window").width < 750 ? 10 : 0 }}>
                  <View
                    style={{
                     // marginTop: 10,
                      width: "50%",
                      flexDirection: 'row',
                      //alignItems: 'baseline'
                      alignSelf:'center'
                    }}
                  >
                    {/* <TouchableOpacity
                    // onPress={() =>
                    //   this.goToProfile(item.getIn(["clik", "name"]))
                    // }
                    >
                      <Hoverable>
                        {isHovered => (
                          <Text
                            style={{
                              color: "#4169e1",
                              backgroundColor: "#rgb(232, 245, 250)",
                              fontSize: 15,
                              fontWeight: "bold",
                              fontFamily: ConstantFontFamily.MontserratBoldFont,
                              fontSize: 14,
                              //fontWeight: "bold",
                              //fontFamily: ConstantFontFamily.defaultFont,
                              marginTop: 5,
                              textAlign: "left",
                              border: "1px solid #c5c5c5",
                              padding: 8,
                              borderRadius: 10,
                              width: "25%"
                            }}
                          >
                            {item.label}
                          </Text>
                        )}
                      </Hoverable>
                    </TouchableOpacity> */}
                    {Dimensions.get("window").width > 750 ? (<View
                      style={{
                        // width: "20%",                        
                        flexDirection: "row",
                        backgroundColor: "rgba(255,255,255,0.5)"
                      }}
                    >
                      {item.icon_pic ? (
                        <Image
                          source={{
                            uri: item.icon_pic
                          }}
                          style={{
                            width: 30,
                            height: 30,
                            borderRadius: 5
                          }}
                        />
                      ) : (
                          <Image
                            source={{
                              uri:
                                "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAhFBMVEX/pQD/////ngD/oQD/owD/oAD/+fT/nQD///3/7NP/pgD/4r7//vr/+vH/t1D/vF3/+O7/79r/zo//5MP/xnr/16P/2Kj/9OX/rSv/xXf/uVb/zIn/6c3/267/qRP/05v/s0L/wGn/sDb/wW3/tkv/z5T/yYH/qyD/sDf/37b/x4T/ryzcdsqXAAAM+ElEQVR4nN1daXfqOgx0bKUQCIQd2rKWbtz+//93E6ALEI/kLJAw57xP9yXNYFmSpbGtPA6dYDHYdr+WLVUltJZf3e1gEXTY71fwX8PhaKa1MUR0a0oXiL/JGK0/RsMwM8P2RvumetROQcbXUTsLw85A6aqz+wZpGljN1cKwN9KVH72/IKMfew4Mm8/a3PqbnWH0c1PKsE3145fAUNp8vGQYRrWZf+cgHV361QuGgarnAB5gVMAxHDfqOoAHUGOMGY70rT8xN/QIMYz8W39fAfAjO8OnOk/BX5gnG8PoPgjGFKN0hqN7MNED/FEaw3H9ncwv9PiSYdC49VcVikZwzjBU9Y6D5yAVnjG8Gy/zjR9vc2TYvqdJeIBu/2XYrGCVIi+Imn8YPt+bjSYwz78Me/dnowl074fh4z0OYTyIo2+GnfscwngQO0eGg/scwngQB0eGd+hIDyB1YNi2Ztz6CN83ppqFbwZJTIwZRrYP19NObzrtB/N5e7J6e4zWrYRtQvWqn5kDtEkYhlY/ox+8c3T6w8lzNDN7otf81ozQYcxwaDfSS4ZHNKfD1fbQs7nm97rDH8YMR1ZPamd45NlfvL/4lS7/xyFReTPrB3IM9wiD1ZOpLEuaeQqEexHDPfrjqrLUDyoogmFisvPBrIIkdaAW9oTGiWGC6aSr/WqRNAsFUjZnhjHCXeRXiaQZqK39c7IwTEguXhuVMVfaqm7hDGN0xrOK9Fipq77KYBijP6qEtdJMLe3/mothjMVLBXqtSwWEQHkZxgO5vXkAgUKn/AwT0QpVwVgtKIJhjMmyAsaajoIYet5uVlGOhTH0vHY1ORbI0POGVeRYKMPYVpeV8zkFM4x9jqlGovODwhlWTjBXPEPP61VKU1YGQ8+bz6qjiCiHoeeN/aqYalkMvU5VTLU0hnF0bFViGHWq5rYYNB+rIIKkaLPZbB/f38aL9rzfKZjvfFmBYSTab2qI4Wutzcd6O9gFFsF4BmyrMIwnSOjGVP314yQoZIoOqyooP+xV2U76uSmG3Qo32BOafnd1Iat2xLgiccMGMpqiSa6Z2a+Cw8GIWc4GOYayGVXYUr8Rk2z9y06y6pZ6QExyOcjqeoKaKANiki8LvBfQhs5L5SfjEeT722zWuqnBZDwi9juLLBTf6kMxHkgaZEh52rXwN0fEM3I0dabYr4m/OcLojTPH3rJWFJVpOHMMa+NSjzB665rQvdaMYrI31zFA1m/3nDHnOx4ZbGtHkfyPuRPFxxoFxiNIR07T8b1+FJXxnUy1jhSVP3NZd9SSIumBA8XH2rmbBP6HwzDWz6MmIP0mp1jT3YL+Wu5Ur5PdmMNGi2SnRTE5MRl0Vs4pXq6Qhptde7cYr97+bbqfh60W+YnqRynD8AorjdPu2jRYDDb7nRa5/rJ5kVpqr/z1Ylr/MOzvntc6jx6PjDSL65ceFu0d0v44oswsqbESUix9dzLuAfdX60ZGCZDeCCmWXZ5iu9ydXWQykTSfwmXjptyYIenjh+0oi7lSS1jhKDdmCJUKncmnu9KJjKxy3CnVoTrsCho572IjvRO9GuzpyQ8XtUnorgRuTERvXpVI0VFPM1w7chRm4iUm4c6KoaDrpq7Qz5K3NstL3zJoooKu0zjqd8lLy8ttMqm+gk8XjjKKpR1/lFHX1l46LNBlFLslTcXMyj0XeaVoLoYlRcUce9c2clMVedRhOadY5VFfOujytCQubkux03z60mfxMDYEpY1yQkZOBW3/Q/q7a0GOOi/DTnNrhN+FCQD5gpVGGccE5VdBz4XySmrx68Vmq3g7LUDnLZVX0if/rmHxcb8QJfubzOGYDf+q4lPwYrT6c9nKUfPlqU7hzYyCdiP0ZqLfvsEXGcdFUyxsv8WT5MvI8KVi+zksGRlmExqmQNTypBf2PfOCnY1ZTSaL3bA/zc9UtP4xI/Y9RTub/U6LZKsFzaLnxZy/LcMOkS5Ps+lbeecfHm7LaG3GQdadM3MJRX4qlnyGZcxTm+44m9i5L4gatObe0rxC4zSRrb+7iYAOmAoo+uxicXKVDv+epPtISihq9rXXkmjGJD/Grj5WQJE+uJfsrie2IeM/OipI+7y78Vndzcc1dbam8eQmzReEbNZOr3yqM+lXJ47859GMe0fRuRv7Rbrr4nT47IaV+V3/aG7SW4d055119z4X9689iCqRFknFB57gFg0TMW+4ojv9hf8hn47sEGguobjJtgU6v9/Fjh6nbmCD4nUSmwuYlnQY2ZjBOpsbba+Rq2QHzBgQMenSzQ7o9tdCp8ppKw3TkbrdOflEMktl22WaiRibm22RIi2T5XNtCMOoNEsVoTAQKkjfGTvVTFJ/DYGtDX5XRJFZInBF8MUtte5mJlk4cgqLBh7E5k3V/PQhcalMUYkbxNteykFKInVmci9mJt7S16ikISigyKQ2XIH4qmv9S5ASGCqjj9X4FeMSzfRw8hDehEFL3t0w7TKDS4u9ku5RI6P9WXf7/m9/3wXYmsBXIzxvBSkS4adfSzBT8k3092ihzhAcNW8EcRE7Gx+LbIsPiaSf2pdti+lAWdZ7Pp/d4JILYwYPBTMk+xkCC4tWT5CjfsJBZIQ2T4WaqV6jitoqvdTLS4FwxDBb+HCRZkqc+Kz3mvbXiNiYAU69j+FDh/xQXNA3S758P0hz3ny7DN+d6eOTYArzpuaV5eclip6Uv8e3IeBXMr39SUFBn61fHhGkUWSnIs4vcXI6LcZMTy8Qdv1YanFPrdEgGmwDheSmEsnZN9opk4pVWEC9Gi3hs0W09YlcdENpZULWTmFig58uQmDDVthPkeI3+Ao2GghcVyxgpS9Q8ZwgrZDJtW1CxJAxUxxOBSDlqp9JWy74TNy3X4ymuJ5w7kUiE3HTkDKrmOQLF6WwN827Y4gxkVSkJYtcbx4VP5kFRs4xNLItgqdIueGbmKUi9DW4wJ9zIposqsS0vhATMeBttvhnXuUaxOSSS3ekpVLEZLZopUcwacwXEX35OR1/kdbFZgZxh+IaLNfYLxmVoJFNOptmpoST2xAxxL9OnhaNS0b6F6k3mzJtCOuFtirRKqMnYTBlYES7Hy+R6jeYhiAyUzz+4AJHnmGWWJEgdf8O9ssdtNT30ZN5Yr6fRSebIHXFx/xeqOgGE4ZmDoa8mtWC1EnF5CZvwNgMzB1zaMA4tYAV6a09/IOhYgbOa8EtnCzDrHtm/qUyNP/gQyjmwxVmjqwm8ximqxCYig2KF7Bsar92m2foftrxARa7weUCtNKDMT9HwU2yPTcVlnwfx1c4EZEfzrEFI2NaavVujJkihrCWkt2Z4mwJwDYxsDcFdVPcHMjegsLLFjusWQY+UxjVPg16MN11yyhmY2jNFPFSH6Wm0K3n6F5kTGrsft+gyh1KMKHTa2cPF9kmIvBt8ENR2RR29HPk3hI5xSXAT4onImizwJJiHjltJjMF1S9c+AGqWIKZaQ6GWUpRKMXAERGIt3G4yHOeRIbEDeqToZALrNZx7o0FHRjuIRFPe+hqYN6GvDBK2pm7x52ba0xHF69lkX3DlRySmrbUEjN07VxMGEUl6gaiegT0eWj3xVJ9MTbM9Y1Owa1ksNWndDx+GCLzBmtg+lJsZ0O7NNi4LVdYPQKKu3CdA9I26iq+yuEwFfljxmGqC35tOIFBjkFbJdhBJF4JC06tgKeJgEADs6E5yKIGSlAyJuEoslsnFRNgwTII6oVB/8kslEjyLjk07uFTsoiBPhHUTGEFBHDQgYLNyd//kfWoc9mWQFhHBz4Rtj1AlqEflLDKYZbQUpsj4Ql7cBkEym0waNljVLw6UtLuFOnIPoUW4mtvoU9EiekmE0MzihmKC6oxx1SnGk5a8mPLYV0Q1DEgQ/sC0R/GDB3axKRnqzNPEbY3TgddQ4YoriGGdl+imzFDJjM/+0PJ9birdjDtPfT688n7p+vR+mYVPlixA1YaNe3PWa00+V2U+yEM+6ur9f5GjwznzRttB9S3gefsQ9jeM7zV1u/yQQm75L+bbf0uG/v6lYITtebY10wShjfejlkaDv0cheNJrXFoBewZln3O221wrJgcGDbv0J0SNf8wvMHBRKXj+xDGI8O63uRmx89Gn2+GIahz1RGkwjOGzB6y2qHxswz6YVjeJRK3wJ/dq78MvVEt741Mhf9Hu/GH4f14m5PthH8ZisqBNcDpdsIThjW8FTsF/mlv5JRhPW/iPcX5uXFnDL2x241NlQM1zsv/5wy9QNV5Mhp1UQ68YOiFkesFapUB6eiy8XPJME7DxfXdasFQWpMxjaHXfHa/0PDmMPo5Vc2QytDzOqM818VeH2T0yCLuszCMOQ5UbeYjaRpYhTpWhjHam7zXG18BSX06Qk1+xDD2q8P3ma+5I6FuAzrcZvAxGuLN2Jhhgodg8bbtzpaMtOjKaC2/utvBIuBPFPsPHJSoM14DLo8AAAAASUVORK5CYII="
                            }}
                            style={{
                              width: 30,
                              height: 30,
                              borderRadius: 5
                            }}
                          />
                        )}
                    </View>) : null}

                    <TouchableOpacity
                      style={{
                        //marginTop: 5,
                        //height: 30,
                        alignSelf: "flex-start",
                        paddingVertical: 5,
                        backgroundColor: "#E8F5FA",
                        borderRadius: 6,
                        marginLeft: Dimensions.get("window").width > 750 ? '5%' : 0,
                      }}
                    >
                      <Hoverable>
                        {isHovered => (
                          <Text
                            style={{
                              width: "100%",
                              color: "#4169e1",
                              fontSize: 15,
                              fontWeight: "bold",
                              fontFamily: ConstantFontFamily.MontserratBoldFont,
                              textDecorationLine:
                                isHovered == true ? "underline" : "none"
                            }}
                          >
                            {" "}
                            {item.label}{" "}
                          </Text>
                        )}
                      </Hoverable>
                    </TouchableOpacity>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      width: "50%",
                      justifyContent: 'space-between'
                    }}
                  >
                    <View
                      style={{
                        alignSelf: "center",
                        width: Dimensions.get("window").width > 750 ?"35%" : "75%",
                        borderColor: "#e1e1e1",
                        borderWidth: 1,
                        borderRadius: 10,
                        marginRight: 10
                      }}
                    >
                      <RNPickerSelect
                        placeholder={{}}
                        items={this.state.RoleItems}
                        onValueChange={(itemValue, itemIndex) => {
                          //console.log(itemValue, itemIndex);
                          this.setState({
                            SelectRoleItems: { [id]: itemValue }
                          });
                        }}
                        onUpArrow={() => {
                          this.inputRefs.name.focus();
                        }}
                        onDownArrow={() => {
                          this.inputRefs.picker2.togglePicker();
                        }}
                        style={{ ...styles }}
                        value={this.state.SelectRoleItems[id]}
                        ref={el => {
                          this.inputRefs.picker = el;
                        }}
                      />
                    </View>
                    {Dimensions.get("window").width > 750 ? (<Button
                      onPress={() => {
                        this.CheckPost(id);
                        return this.checkUser(
                          item,
                          this.state.SelectRoleItems[id],
                          id
                        );
                      }}
                      color="#fff"
                      title="Generate Link"
                      titleStyle={ButtonStyle.titleStyle}
                      buttonStyle={ButtonStyle.backgroundStyle}
                      containerStyle={ButtonStyle.containerStyle}
                    />): null}
                    {Dimensions.get("window").width < 750 ? (<Icon
                      color={"#000"}
                      iconStyle={{
                        justifyContent: "center",
                        alignItems: "center",
                        alignSelf: "center"
                      }}
                      name="clone"
                      type="font-awesome"
                      size={18}
                      containerStyle={{
                        //marginTop: 20,
                        //marginLeft: 40,
                        alignSelf: "center"
                      }}
                      onPress={() => {
                        this.CheckPost(id);
                        return this.checkUser(
                          item,
                          this.state.SelectRoleItems[id],
                          id
                        );
                      }}
                      //onPress={() => this.writeToClipboard()}
                    />): null}
                  </View>
                </View>
                {this.state.itemCliksList[id].inviteKey != ""  && Dimensions.get("window").width > 750 &&
                  this.newRole(id) ? (
                    <View
                      style={{
                        marginBottom: 5,
                        alignSelf: "center",
                        flexDirection: "row",
                        width: "100%",
                        justifyContent: "center"
                      }}
                    >
                      <View
                        style={{
                          borderWidth: 1,
                          borderColor: "#C5C5C5",
                          borderRadius: 10,
                          paddingHorizontal: 5,
                          paddingVertical: 3,
                          width: "60%"
                        }}
                      >
                        <Text
                          style={{
                            textAlign: "left",
                            color: "#000",
                            fontSize: 14,
                            fontWeight: "bold",
                            fontFamily: ConstantFontFamily.defaultFont
                          }}
                        >
                          {"https://" +
                            window.location.href
                              .replace("http://", "")
                              .replace("https://", "")
                              .replace("www.", "")
                              .split(/[/?#]/)[0] +
                            "/clik/" +
                            this.newClicks(id) +
                            //this.props.cliksDetails.getIn(["data", "clik"]).get("name") +
                            "/invite?key=" +
                            this.state.itemCliksList[id].inviteKey}
                        </Text>
                      </View>
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          justifyContent: "center",
                          alignItems: "center",
                          alignSelf: "center"
                        }}
                        name="clone"
                        type="font-awesome"
                        size={18}
                        containerStyle={{
                          marginLeft: 20,
                          alignSelf: "center"
                        }}
                        onPress={() => this.CheckPost(id)}
                      />
                    </View>
                  ) : null}
                {this.newRole(id) ? (
                  <Text
                    style={{
                      color: "#000",
                      fontSize: 11.4,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.defaultFont,
                      marginVertical: 5,
                      textAlign: "center"
                    }}
                  >
                    {this.state.itemCliksList[id].showCopiedTextInvite &&
                      "Link Copied! "}
                    {`The Link expires after 24 hour. Invited users will be a ${this.newRole(id)
                      ? this.newRole(id).toLowerCase()
                      : "member"
                      }.`}
                  </Text>
                ): null}
              </View>
            ))}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 5,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  },
  inputAndroid: {
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#fff",
    borderRadius: 8,
    color: "#000",
    backgroundColor: "white",
    paddingRight: 5,
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  },
  usertext: {
    color: "#000",
    fontSize: 14,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  }
});

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails")
});

export default compose(connect(mapStateToProps, null))(InviteFriend);
