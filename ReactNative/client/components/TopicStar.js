import "@expo/browser-polyfill";
import _ from "lodash";
import { List } from "immutable";
import React, { Component } from "react";
import { graphql } from "react-apollo";
import { Image, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setLikeContent } from "../actionCreator/LikeContentAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingExternalFeeds } from "../actionCreator/TrendingExternalFeedsAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import { getTrendingUsers } from "../actionCreator/TrendingUsersAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import AppHelper from "../constants/AppHelper";
import {
  TopicFollowMutation,
  TopicUnFollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  TopicFollowVariables,
  TopicUnFollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { getTrendingTopicsProfileDetails } from "../actionCreator/TrendingTopicsProfileAction";

class TopicStar extends Component {
  constructor(props) {
    super(props);
    const gstar = require("../assets/image/gstar.png");
    const ystar = require("../assets/image/ystar.png");
    const wstar = require("../assets/image/wstar.png");

    this.state = {
      index: 0,
      starList: [wstar, gstar, ystar],
      followList: ["TRENDING", "FOLLOW", "FAVORITE"]
    };
  }

  async componentDidMount() {
    await this.getTopicStar(this.props.TopicName);
  }

  componentDidUpdate = async prevProps => {
    if (prevProps.TopicName !== this.props.TopicName) {
      await this.getTopicStar(this.props.TopicName);
    }
    if (
      prevProps.getUserFollowTopicList !== this.props.getUserFollowTopicList
    ) {
      await this.getTopicStar(this.props.TopicName);
    }
  };

  getTopicStar = async TopicName => {
    if (this.props.loginStatus == 0) {
      await this.setState({
        index: 0
      });
      return false;
    }
    let index = 0;
    index = this.props.getUserFollowTopicList.findIndex(
      i =>
        i
          .getIn(["topic", "name"])
          .toLowerCase()
          .replace("topic:", "") == TopicName &&
        TopicName.toLowerCase().replace("topic:", "")
    );
    if (index == -1) {
      await this.setState({
        index: 0
      });
    } else if (
      this.props.getUserFollowTopicList.getIn([
        index,
        "settings",
        "follow_type"
      ]) == "FOLLOW"
    ) {
      await this.setState({
        index: 1
      });
    } else {
      await this.setState({
        index: 2
      });
    }
  };

  changeStar = async () => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    if (this.state.index + 1 == this.state.starList.length) {
      await this.setState({
        index: 0
      });
      await this.changeStarApi(this.state.followList[this.state.index]);
    } else {
      await this.setState({
        index: this.state.index + 1
      });
      await this.changeStarApi(this.state.followList[this.state.index]);
    }
  };

  changeStarApi = _.debounce(typeIndex => {
    typeIndex == "FOLLOW"
      ? this.followTopics(this.props.TopicName)
      : typeIndex == "FAVORITE"
      ? this.favroiteTopics(this.props.TopicName)
      : this.unfollowTopics(this.props.TopicName);
  }, 1000);

  followTopics = async topicId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    TopicFollowVariables.variables.topic_id = topicId;
    TopicFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
        // await this.props.getTrendingTopics({
        //   currentPage: AppHelper.PAGE_LIMIT
        // });
        // await this.props.topicId({
        //   id: topicId,
        //   type: "feed"
        // });
      });
  };

  favroiteTopics = async topicId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    TopicFollowVariables.variables.topic_id = topicId;
    TopicFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
        // await this.props.getTrendingTopics({
        //   currentPage: AppHelper.PAGE_LIMIT
        // });
        // await this.props.topicId({
        //   id: topicId,
        //   type: "feed"
        // });
      });
  };

  unfollowTopics = async topicId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    TopicUnFollowVariables.variables.topic_id = "Topic:" + topicId;
    applloClient
      .query({
        query: TopicUnFollowMutation,
        ...TopicUnFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
        // await this.props.getTrendingTopics({
        //   currentPage: AppHelper.PAGE_LIMIT
        // });
        // await this.props.topicId({
        //   id: topicId,
        //   type: "feed"
        // });
      });
  };

  render() {
    return (
      <TouchableOpacity
        style={this.props.ContainerStyle}
        onPress={() => this.changeStar()}
      >
        <Image
          source={this.state.starList[this.state.index]}
          style={this.props.ImageStyle}
        />
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  DiscussionHomeFeed: state.HomeFeedReducer.get("DiscussionHomeFeedList"),
  getUserFollowTopicList: state.LoginUserDetailsReducer.get(
    "userFollowTopicsList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowTopicsList")
    : List()
});

const mapDispatchToProps = dispatch => ({
  LikeContent: payload => dispatch(setLikeContent(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setDiscussionHomeFeed: payload =>
    dispatch({ type: "SET_DISCUSSION_HOME_FEED", payload }),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  getTrendingUsers: payload => dispatch(getTrendingUsers(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  getTrendingExternalFeeds: payload =>
    dispatch(getTrendingExternalFeeds(payload)),
  topicId: payload => dispatch(getTrendingTopicsProfileDetails(payload))
});

const TopicStarWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(TopicStar);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  TopicStarWrapper
);
