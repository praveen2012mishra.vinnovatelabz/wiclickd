import React, { Component, lazy, Suspense } from "react";
import { Text, View, TextInput, Platform, Clipboard } from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import ButtonStyle from "../constants/ButtonStyle";
import { Button, Icon } from "react-native-elements";
import ConstantFontFamily from "../constants/FontFamily";
import RNPickerSelect from "react-native-picker-select";
import applloClient from "../client";
import { ClikGenerateKeyMutation } from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { ClikGenerateKeyVariables } from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { graphql } from "react-apollo";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import { Hoverable } from "react-native-web-hooks";
import { List } from "immutable";

class InviteLinkPrivate extends Component {
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.state = {
      showCopiedText: false,
      status: "Default",
      key: "",
      SelectRoleItems: "MEMBER",
      RoleItems: [
        {
          label: "Member",
          value: "MEMBER",
          key: 0
        },
        {
          label: "Admin",
          value: "ADMIN",
          key: 1
        },
        {
          label: "Super Admin",
          value: "SUPER_ADMIN",
          key: 2
        }
      ]
    };
  }

  writeToClipboard = async () => {
    //To copy the text to clipboard
    await Clipboard.setString(
      "https://" +
        window.location.href
          .replace("http://", "")
          .replace("https://", "")
          .replace("www.", "")
          .split(/[/?#]/)[0] +
        "/clik/" +
        this.props.cliksDetails.getIn(["data", "clik"]).get("name") +
        "/invite?key=" +
        this.state.key
    );
    this.setState({
      showCopiedText: true
    });
  };

  generateKey = async () => {
    ClikGenerateKeyVariables.variables.clik_id = this.props.cliksDetails
      .getIn(["data", "clik"])
      .get("id");
    ClikGenerateKeyVariables.variables.member_type = this.state.SelectRoleItems;
    try {
      await applloClient
        .query({
          query: ClikGenerateKeyMutation,
          ...ClikGenerateKeyVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState({
            status: "Success",
            key: res.data.clik_create_invite_key.invite_key
          });
          this.writeToClipboard();
        });
    } catch (e) {
      console.log(e);
    }
  };

  componentDidMount = () => {
    if (this.props.InviteType == "Invite") {
      this.generateKey();
    }
  };
  render() {
    return (
      <View
        style={[ButtonStyle.cardShadowStyle, {
          backgroundColor: "#fff",
          borderRadius: 10,
          // borderColor: "#C5C5C5",
          borderWidth: 0,
          // width: "100%",
          marginBottom: 10
        }]}
      >
        <Text
          style={{
            textAlign: "left",
            color: "#000",
            fontSize: 16,
            fontWeight: "bold",
            fontFamily: ConstantFontFamily.MontserratBoldFont,
            marginVertical: 10,
            marginHorizontal: 10
          }}
        >
          Invite Via Link
        </Text>
        {this.state.status == "Default" && (
          <View
            style={{
              marginBottom: 20,
              flexDirection: "row",
              width: "100%"
            }}
          >
            <View
              style={{
                padding: 10,
                width: "65%"
              }}
            >
              <Text
                style={{
                  textAlign: "left",
                  color: "#000",
                  fontSize: 14,
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.defaultFont,
                  marginLeft: 30
                }}
              >
                Invited users will all be:
              </Text>
            </View>
            <View
              style={[ButtonStyle.shadowStyle, {
                justifyContent: "flex-end",
                borderRadius: 6,
                borderColor: "#e1e1e1",
                borderWidth: 0,
                height: 40,
                alignSelf: "center",
                width:'24%'
              }]}
            >
              <RNPickerSelect
                placeholder={{}}
                items={this.state.RoleItems}
                onValueChange={(itemValue, itemIndex) => {
                  this.setState({
                    SelectRoleItems: itemValue
                  });
                }}
                onUpArrow={() => {
                  this.inputRefs.name.focus();
                }}
                onDownArrow={() => {
                  this.inputRefs.picker2.togglePicker();
                }}
                style={{ ...styles }}
                ref={el => {
                  this.inputRefs.picker = el;
                }}
              />
            </View>
          </View>
        )}
        {this.state.status == "Default" && (
          <Button
            onPress={() => this.generateKey()}
            color="#000"
            title="Generate Link"
            titleStyle={ButtonStyle.titleStyle}
            buttonStyle={ButtonStyle.backgroundStyle}
            containerStyle={ButtonStyle.containerStyle}
          />
        )}
        {this.state.status == "Success" && (
          <View
            style={{
              alignSelf: "center",
              flexDirection: "row",
              width: "100%",
              justifyContent: "center"
            }}
          >
            <View
              style={{
                borderWidth: 1,
                borderColor: "#C5C5C5",
                borderRadius: 10,
                padding: 10,
                width: "60%"
              }}
            >
              <Text
                style={{
                  textAlign: "left",
                  color: "#000",
                  fontSize: 14,
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.defaultFont
                }}
              >
                {"https://" +
                  window.location.href
                    .replace("http://", "")
                    .replace("https://", "")
                    .replace("www.", "")
                    .split(/[/?#]/)[0] +
                  "/clik/" +
                  this.props.cliksDetails.getIn(["data", "clik"]).get("name") +
                  "/invite?key=" +
                  this.state.key}
              </Text>
            </View>
            <Icon
              color={"#000"}
              iconStyle={{
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center"
              }}
              name="clone"
              type="font-awesome"
              size={18}
              containerStyle={{
                marginLeft: 40,
                alignSelf: "center"
              }}
              onPress={() => this.writeToClipboard()}
            />
          </View>
        )}
        {this.state.showCopiedText == true && (
          <Text
            style={{
              color: "#000",
              fontSize: 14,
              fontWeight: "bold",
              fontFamily: ConstantFontFamily.defaultFont,
              marginVertical: 10,
              textAlign: "center"
            }}
          >
            {
              "Invitation Link Copied! The Link expires after 24 hour. Invited users will be a member."
            }
          </Text>
        )}
      </View>
    );
  }
}
const mapStateToProps = state => ({
  cliksDetails: state.TrendingCliksProfileReducer.get(
    "getTrendingCliksProfileDetails"
  ),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  listClikUserRequest: !state.ClikUserRequestReducer.get("ClikUserRequestList")
    ? List()
    : state.ClikUserRequestReducer.get("ClikUserRequestList"),
  listClikMembers: !state.ClikMembersReducer.get("clikMemberList")
    ? List()
    : state.ClikMembersReducer.get("clikMemberList"),
  loginStatus: state.UserReducer.get("loginStatus"),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List()
});

const mapDispatchToProps = dispatch => ({});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  InviteLinkPrivate
);

export const styles = {
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  },
  inputAndroid: {
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#fff",
    borderRadius: 8,
    color: "#000",
    backgroundColor: "white",
    paddingRight: 30,
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  }
};