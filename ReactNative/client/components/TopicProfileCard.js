import { List } from "immutable";
import React, { useEffect, useState } from "react";
import { graphql } from "react-apollo";
import {
  Alert,
  Dimensions,
  ImageBackground,
  Image,
  Platform,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { editTopic } from "../actionCreator/TopicEditAction";
import { getTrendingTopicsProfileDetails } from "../actionCreator/TrendingTopicsProfileAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import ConstantFontFamily from "../constants/FontFamily";
import { rootTopics } from "../constants/RootTopics";
import { DeleteTopicMutation } from "../graphqlSchema/graphqlMutation/FeedMutation";
import {
  TopicFollowMutation,
  TopicUnFollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  TopicFollowVariables,
  TopicUnFollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { DeleteTopicVariables } from "../graphqlSchema/graphqlVariables/LikeContentVariables";
import TopicStar from "../components/TopicStar";
import ButtonStyle from "../constants/ButtonStyle";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger,
} from "react-native-popup-menu";

const TopicProfileCard = props => {
  const topic = props.item.getIn(["data", "topic"]);
  const [followBtnActive, setfollowBtnActive] = useState(0);
  let [MenuHover, setMenuHover] = useState(false);
  let banner_pic = topic ? topic.getIn(["banner_pic"]) : "";

  async function followTopics(cliksId) {
    let itemId = props.navigation.getParam("id", "NO-ID");
    if (itemId == "NO-ID") {
      props.navigation.navigate("home");
      return false;
    }
    TopicFollowVariables.variables.topic_id = "Topic:" + cliksId;
    TopicFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await props.Login();
        await props.saveLoginUser(resDataLogin.data.login);
        setfollowBtnActive(1);
        props.topicId({
          id: itemId.replace("%3A", ":"),
          type: "feed"
        });
        props.icon("#E1E1E1");
      });
  }

  async function favroiteTopics(topicId) {
    let itemId = props.navigation.getParam("id", "NO-ID");
    if (itemId == "NO-ID") {
      props.navigation.navigate("home");
      return false;
    }
    TopicFollowVariables.variables.topic_id = "Topic:" + topicId;
    TopicFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await props.Login();
        await props.saveLoginUser(resDataLogin.data.login);
        setfollowBtnActive(2);
        props.topicId({
          id: itemId.replace("%3A", ":"),
          type: "feed"
        });
        props.icon("#FADB4A");
      });
  }

  async function unfollowTopics(cliksId) {
    let itemId = props.navigation.getParam("id", "NO-ID");
    if (itemId == "NO-ID") {
      props.navigation.navigate("home");
      return false;
    }
    TopicUnFollowVariables.variables.topic_id = cliksId;
    applloClient
      .query({
        query: TopicUnFollowMutation,
        ...TopicUnFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await props.Login();
        await props.saveLoginUser(resDataLogin.data.login);
        setfollowBtnActive(0);
        props.topicId({
          id: itemId.replace("%3A", ":"),
          type: "feed"
        });
        props.icon("#fff");
      });
  }

  useEffect(() => {
    const itemId = props.navigation.getParam("title", "NO-ID");
    if (itemId == "NO-ID") {
      props.navigation.navigate("home");
    }
    const index = props.getUserFollowTopicList.findIndex(
      i =>
        i
          .getIn(["topic", "name"])
          .toLowerCase()
          .replace("topic:", "") ==
        itemId
          .replace("%2F", "")
          .toLowerCase()
          .replace("topic:", "")
    );
    if (index != -1) {
      if (
        props.getUserFollowTopicList.getIn([
          index,
          "settings",
          "follow_type"
        ]) == "FAVORITE"
      ) {
        setfollowBtnActive(2);
        props.icon("#FADB4A");
      }
      if (
        props.getUserFollowTopicList.getIn([
          index,
          "settings",
          "follow_type"
        ]) == "FOLLOW"
      ) {
        setfollowBtnActive(1);
        props.icon("#E1E1E1");
      }
    } else {
      setfollowBtnActive(0);
      props.icon("#fff");
    }
  });

  async function goToProfile(id) {
    props.topicId({
      id: id,
      type: "feed"
    });
  }

  async function showAlert() {
    if (Platform.OS === "web") {
      var result = confirm(
        "Are you sure you want to delete /" +
        topic
          .get("name")
          .replace("%2F", "/")
          .toLowerCase()
      );
      if (result == true) {
        DeleteTopicVariables.variables.topic_id = topic.getIn(["id"]);
        applloClient
          .query({
            query: DeleteTopicMutation,
            ...DeleteTopicVariables,
            fetchPolicy: "no-cache"
          })
          .then(async res => {
            let resDataLogin = await props.Login();
            await props.saveLoginUser(resDataLogin.data.login);
          });
      } else {
      }
    } else {
      Alert.alert(
        "Are you sure you want to delete /" +
        topic
          .get("name")
          .replace("%2F", "/")
          .toLowerCase(),
        [
          {
            text: "NO",
            onPress: () => console.warn("NO Pressed"),
            style: "cancel"
          },
          {
            text: "YES",
            onPress: () => {
              DeleteTopicVariables.variables.topic_id = topic.getIn(["id"]);
              applloClient
                .query({
                  query: DeleteTopicMutation,
                  ...DeleteTopicVariables,
                  fetchPolicy: "no-cache"
                })
                .then(async res => {
                  let resDataLogin = await props.Login();
                  await props.saveLoginUser(resDataLogin.data.login);
                });
            }
          }
        ]
      );
    }
  }

  return (

    <View
      style={[
        Dimensions.get('window').width <= 750 ? ButtonStyle.cardBorderStyle : ButtonStyle.borderStyle,
        ButtonStyle.profileShadowStyle,
        {
          overflow: "hidden",
          marginBottom: Dimensions.get("window").width >= 750 ? 10 : 0,
          backgroundColor: "#fff",
          borderWidth: 0,
          maxHeight: Dimensions.get("window").height / 2 - props.feedY,
          marginHorizontal: Dimensions.get("window").width <= 750 ? 0 : 10,
          marginTop: Dimensions.get("window").width <= 750 ? 0 : 10
          // width:Dimensions.get("window").width>=750 ? '100%' : '97%'
        }
      ]}
    >
      {/* <ImageBackground
        style={{
          width: props.ProfileHeight - props.feedY < 0 ? 0 : "100%",
          height: Dimensions.get("window").height / 4 - props.feedY
        }}
        source={{
          uri: banner_pic
        }}
      >
        {props.isAdmin == true && (
          <View
            style={{
              flexDirection: "column",
              justifyContent: "flex-end",
              flex: 1,
              paddingHorizontal: 5
            }}
          >
            <View
              style={{
                flexDirection: "row",
                width: "100%",
                justifyContent: "flex-end"
              }}
            >
              <Hoverable>
                {isHovered => (
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    reverse
                    name="trash"
                    type="font-awesome"
                    size={isHovered == true ? 18 : 16}
                    containerStyle={{
                      alignSelf: "flex-end",
                      justifyContent: "flex-end"
                    }}
                    onPress={() => showAlert()}
                  />
                )}
              </Hoverable>

              <Hoverable>
                {isHovered => (
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    reverse
                    name="edit"
                    type="font-awesome"
                    size={isHovered == true ? 18 : 16}
                    containerStyle={{
                      alignSelf: "flex-end",
                      justifyContent: "flex-end"
                    }}
                    onPress={async () => {
                      await props.editTopic({
                        description: topic.getIn(["description"]),
                        topicName: topic
                          .get("name")
                          .replace("%2F", "/")
                          .toLowerCase(),
                        setBackPic: banner_pic,
                        changeBackPicEnable: banner_pic
                          .split("/")
                        [banner_pic.split("/").length - 1].substring(
                          0,
                          banner_pic
                            .split("/")
                          [banner_pic.split("/").length - 1].indexOf(".")
                        ),
                        topic_id: topic.getIn(["id"]),
                        rtopicName: topic.get("parents")
                          ? [...topic.get("parents")][
                          [...topic.get("parents")].length - 1
                          ]
                          : ""
                      });
                      props.navigation.navigate("edittopic");
                    }}
                  />
                )}
              </Hoverable>
            </View>
          </View>
        )}
      </ImageBackground> */}
      <View style={{ paddingRight: 20, paddingLeft:15}}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: 'space-between',
            marginTop:15,
            marginBottom: 10,
            // marginBottom: 10
          }}
        >
          <TouchableOpacity
            style={{
              // height: 35,
              // alignItems: "center",
              // justifyContent: "center",
              // backgroundColor: "#e3f9d5",
              // alignSelf: "center",
              // borderRadius: 20,              

              // borderTopRightRadius: 6,
              // borderBottomRightRadius: 6,
              // borderTopLeftRadius: 6, //topic && topic.get("parents") ? 0 : 6,
              // borderBottomLeftRadius: 6 //topic && topic.get("parents") ? 0 : 6

                height: 30,
                paddingVertical: 8,
                paddingHorizontal: 3,
                backgroundColor: "#e3f9d5",
                borderRadius: 6,
                alignSelf: "flex-start",
                marginRight: 5,
                justifyContent: "center",
                alignItems: "center",
            }}
            onPress={() =>
              goToProfile(topic && topic.get("name").replace("%2F", "/"))
            }
          >
            <Text
              style={{
                color: "#009B1A",
                fontWeight: "bold",
                fontSize: 15,
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                // paddingHorizontal: 10,
                // paddingVertical: 2
              }}
            >
              /
                {topic &&
                topic
                  .get("name")
                  .replace("%2F", "/")
                  .toLowerCase()}
            </Text>
          </TouchableOpacity>





          {/* <View
            style={{
              // width: "30%",
              justifyContent: "flex-end",
              flexDirection: "row",
              alignSelf: "center",
              alignItems: "center",
              height: 30
            }}
          > */}
            {
              <View style={{ flexDirection: "row", alignItems: 'flex-start', }}>
                {/* {props.isAdmin &&
                <View
                  style={{
                    flexDirection: "row",
                    // width: "100%",
                    justifyContent: "flex-end"
                  }}
                >
                  <Hoverable>
                    {isHovered => (
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          color: "#fff",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                        reverse
                        name="trash"
                        type="font-awesome"
                        size={isHovered == true ? 18 : 16}
                        containerStyle={{
                          alignSelf: "flex-end",
                          justifyContent: "flex-end"
                        }}
                        onPress={() => showAlert()}
                      />
                    )}
                  </Hoverable>

                  <Hoverable>
                    {isHovered => (
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          color: "#fff",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                        reverse
                        name="edit"
                        type="font-awesome"
                        size={isHovered == true ? 18 : 16}
                        containerStyle={{
                          alignSelf: "flex-end",
                          justifyContent: "flex-end"
                        }}
                        onPress={async () => {
                          await props.editTopic({
                            description: topic.getIn(["description"]),
                            topicName: topic
                              .get("name")
                              .replace("%2F", "/")
                              .toLowerCase(),
                            setBackPic: banner_pic,
                            changeBackPicEnable: banner_pic
                              .split("/")
                            [banner_pic.split("/").length - 1].substring(
                              0,
                              banner_pic
                                .split("/")
                              [banner_pic.split("/").length - 1].indexOf(".")
                            ),
                            topic_id: topic.getIn(["id"]),
                            rtopicName: topic.get("parents")
                              ? [...topic.get("parents")][
                              [...topic.get("parents")].length - 1
                              ]
                              : ""
                          });
                          props.navigation.navigate("edittopic");
                        }}
                      />
                    )}
                  </Hoverable>
                </View>
                } */}

                <TouchableOpacity
                  onMouseEnter={() => setMenuHover(true)}
                  onMouseLeave={() => setMenuHover(false)}
                >
                  <Menu>
                    <MenuTrigger>
                      <Image
                        source={require("../assets/image/menu.png")}
                        style={{
                          height: 16,
                          width: 16,
                          // marginTop: 5,
                          marginRight: Dimensions.get('window').width <= 750 ? 10 : 20
                        }}
                      />
                    </MenuTrigger>
                    <MenuOptions
                      optionsContainerStyle={{
                        borderRadius: 6,
                        borderWidth: 1,
                        borderColor: "#e1e1e1",
                        shadowColor: "transparent"
                      }}
                      customStyles={{
                        optionsContainer: {
                          minHeight: 50,
                          width: 150,
                          marginTop: 15,
                        }
                      }}
                    >
                      <MenuOption
                        onSelect={() => {
                          followTopics(props.item.getIn(["data", "topic"]) && props.item.getIn(["data", "topic"]).get("name"));
                        }}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily: ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Follow
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>
                      <MenuOption
                        onSelect={() => {
                          favroiteTopics(props.item.getIn(["data", "topic"]) && props.item.getIn(["data", "topic"]).get("name"));
                        }}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily: ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Favourite
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>
                      <MenuOption
                        onSelect={() => {
                          unfollowTopics(props.item.getIn(["data", "topic"]) && props.item.getIn(["data", "topic"]).get("name"));
                        }}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily: ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Unfollow
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>

                      <MenuOption>
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily: ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Report
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>
                      {props.isAdmin &&
                        <MenuOption
                          onSelect={async () => {
                            await props.editTopic({
                              description: topic.getIn(["description"]),
                              topicName: topic
                                .get("name")
                                .replace("%2F", "/")
                                .toLowerCase(),
                              setBackPic: banner_pic,
                              changeBackPicEnable: banner_pic
                                .split("/")
                              [banner_pic.split("/").length - 1].substring(
                                0,
                                banner_pic
                                  .split("/")
                                [banner_pic.split("/").length - 1].indexOf(".")
                              ),
                              topic_id: topic.getIn(["id"]),
                              rtopicName: topic.get("parents")
                                ? [...topic.get("parents")][
                                [...topic.get("parents")].length - 1
                                ]
                                : ""
                            });
                            props.navigation.navigate("edittopic");
                          }}
                        >
                          <Hoverable>
                            {isHovered => (
                              <Text
                                style={{
                                  textAlign: "center",
                                  color: isHovered == true ? "#009B1A" : "#000",
                                  fontFamily:
                                    ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                Edit
                              </Text>
                            )}
                          </Hoverable>
                        </MenuOption>
                      }
                      {props.isAdmin &&
                        <MenuOption onSelect={() => showAlert()}>
                          <Hoverable>
                            {isHovered => (
                              <Text
                                style={{
                                  textAlign: "center",
                                  color: isHovered == true ? "#009B1A" : "#000",
                                  fontFamily:
                                    ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                Delete
                              </Text>
                            )}
                          </Hoverable>
                        </MenuOption>
                      }
                    </MenuOptions>
                  </Menu>
                </TouchableOpacity>

                <TopicStar
                  TopicName={props.item.getIn(["data", "topic"]) && props.item.getIn(["data", "topic"]).get("name")}
                  ContainerStyle={{}}
                  ImageStyle={{
                    height: 20,
                    width: 20,
                    alignSelf: "center",
                    marginLeft: 5
                  }}
                />
              </View>
            }
          {/* </View> */}



        </View>

        <View
          style={{
            marginBottom: 20,
            paddingLeft: 5,
          }}
        >
          <Text
            style={{
              color: "grey",
              fontSize: 13,
              fontFamily: ConstantFontFamily.defaultFont
            }}
          >
            2 Subtopics{" "}
            {props.item.getIn(["data", "topic"]) && props.item.getIn(["data", "topic"]).getIn(["num_followers"])}{" "}
            Followers
          </Text>
        </View>
        <View
          style={{
            marginBottom: 20,
            paddingLeft: 5
          }}
        >
          <Text
            style={{
              color: "#000",
              fontSize: 13,
              fontFamily: ConstantFontFamily.defaultFont
            }}
          >
            {topic && topic.getIn(["description"])}
          </Text>
        </View>


      </View>



    </View>
  );
};

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  getUserFollowTopicList: state.LoginUserDetailsReducer.get(
    "userFollowTopicsList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowTopicsList")
    : List(),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView")
});

const mapDispatchToProps = dispatch => ({
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  topicId: payload => dispatch(getTrendingTopicsProfileDetails(payload)),
  editTopic: payload => dispatch(editTopic(payload))
});

const TopicProfileCardContainerWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(TopicProfileCard);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  TopicProfileCardContainerWrapper
);
