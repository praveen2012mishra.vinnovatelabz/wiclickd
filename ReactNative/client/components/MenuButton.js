import React from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import HeaderIcon from "./HeaderIcon";

const MenuButton = props => {
  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "space-between"
      }}
    >
      <View
        style={{
          flexDirection: "row"
        }}
      >
        <HeaderIcon />
      </View>
    </View>
  );
};

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus")
});

export default compose(connect(mapStateToProps, null))(MenuButton);
