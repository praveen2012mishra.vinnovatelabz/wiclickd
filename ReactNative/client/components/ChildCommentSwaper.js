import { List } from "immutable";
import React, { lazy, PureComponent, Suspense } from "react";
import { Image, TouchableOpacity, View,Dimensions } from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setFEEDREPORTMODALACTION } from "../actionCreator/FeedReportModalAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import Swiper from "../library/Swiper";
import CreateCommentCard from "./CreateCommentCard";
import ShadowSkeletonComment from "./ShadowSkeletonComment";
import { retry } from "../library/Helper";
import { Hoverable } from "react-native-web-hooks";
import PostDetailsCommentSwaper from "./PostDetailsCommentSwaper";
// const PostDetailsCommentSwaper = lazy(() =>
//   retry(() => import("./PostDetailsCommentSwaper"))
// );

class ChildCommentSwaper extends PureComponent {
  state = {
    modalVisible: false,
    editModalVisible: false,
    outSideClick: false,
    showBug: false,
    parent_content_id: "",
    height: 0,
    pressBug: false,
    nameWidth: 0,
    showEditButton: true,
    setindex: 0,
    setcalHeight: [],
    gesturesEnabled: true,
    disabledSwaper: false,
    gestureStatus: false,
    getShowMoreArray:[],
    setShowArray:[]
  };

  constructor(props) {
    super(props);
    this.commentData = this.props.commentData;
    this.swiperRef = React.createRef();
    this.postDetailsCommentSwaperRef = {};
  }

  onClose = async parentId => {
    await this.setState({
      modalVisible: false,
      outSideClick: true
    });
    this.postDetailsCommentSwaperRef[parentId.replace("Comment:", "")].onOpen(
      parentId
    );
  };

  onSubmit = (position, data) => {
    this.setState({
      modalVisible: false
    });
    this.props.closeModalBySubmit(position, data);
  };

  onOpen(id) {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    let changeData = this.state.modalVisible ? false : true;
    this.setState({
      modalVisible: changeData,
      parent_content_id: id
    });
  }

  getStatus(clik) {
    let index = 0;
    if (clik != null) {
      index = this.props.getUserFollowCliksList.findIndex(
        i =>
          i
            .getIn(["clik", "name"])
            .toLowerCase()
            .replace("clik:", "") == clik.toLowerCase().replace("clik:", "")
      );
    }
    return index;
  }

  loginModalStatusEvent = async status => {
    this.setState({
      modalVisible: status
    });
  };

  goToUserProfile = async username => {
    this.props.userId({
      username: username
    });
    this.props.navigation.navigate("profile", {
      username: username,
      type: "profile"
    });
  };

  onLayout = async event => {
    const { width, height } = event.nativeEvent.layout;
    if (width > 0) {
      this.props.calHeight(height, this.props.index);
      await this.setState({
        height: height
      });
    }
  };

  onLayoutUsername = async event => {
    const { width, height } = event.nativeEvent.layout;
    if (width > 0) {
      await this.setState({
        nameWidth: width
      });
    }
  };

  handleClick = e => {
    this.setState({
      showBug: e
    });
  };

  handleBugClick = e => {
    this.props.setFeedReportModalAction(true);
    this.setState({
      pressBug: true
    });
  };

  calHeightModi = async (height, key) => {
    let data = [];
    data = this.state.setcalHeight;
    data[key] = height;
    this.setState(
      {
        setcalHeight: data
      },
      () => {
        this.swiperRef.current._setHeight(
          this.state.setcalHeight[this.state.setindex]
        );
        this.swiperRef.current.props.containerStyle.height = this.state.setcalHeight[
          this.state.setindex
        ];
        this.swiperRef.current.props.innerContainerStyle.minHeight = this.state.setcalHeight[
          this.state.setindex
        ];
      }
    );
  };

  setModal = async (cvalue, id) => {
    this.setState({
      modalVisible: cvalue,
      parent_content_id: id
    });
  };

  setEditModal = async evalue => {
    await this.setState(
      {
        editModalVisible: evalue
      },
      () => {
        //console.log("editModalVisible", this.state.editModalVisible);
      }
    );
  };

  setGesture = async evalue => {
    await this.setState({
      gesturesEnabled: evalue
    });
  };

  disableSwaper = value => {
    this.setState({ disabledSwaper: value });
  };

  // componentDidMount=()=>{
  //     window.addEventListener('touchstart', this.touchStart);
  //     window.addEventListener('touchmove', this.preventTouch, { passive: false });
  //     //{console.log(this.state.gestureStatus)}

  // }

  // componentWillUnmount() {
  //   window.removeEventListener('touchstart', (e)=>this.touchStart(e));
  //   window.removeEventListener('touchmove', (e)=>this.preventTouch(e), { passive: false });
  // }

  // touchStart=(e)=> {
  //   this.firstClientX = e.touches[0].clientX;
  //   this.firstClientY = e.touches[0].clientY;
  // }

  // preventTouch=(e)=> {
  //   const minValue = 5; // threshold
  //   this.clientX = e.touches[0].clientX
  //   this.clientY = e.touches[0].clientY   
  //   Math.abs(this.clientX) > minValue;
  //  this.setState({gestureStatus:Math.abs(this.clientX) > minValue})   
  // }

  setGestureState = (event) => {
    window.addEventListener('touchstart', this.touchStart);
    window.addEventListener('touchmove', this.preventTouch, { passive: false });
  }

  componentWillUnmount() {
    window.removeEventListener('touchstart', (e) => this.touchStart(e));
    window.removeEventListener('touchmove', (e) => this.preventTouch(e), { passive: false });
  }

  touchStart = (e) => {
    this.firstClientX = e.touches[0].clientX;
    this.firstClientY = e.touches[0].clientY;
  }

  preventTouch = (e) => {
    let _self = this;
    const minValue = 5; // threshold
    this.clientX = e.touches[0].clientX
    this.clientY = e.touches[0].clientY
    // Math.abs(this.clientX) > minValue;
    // if(Math.abs(this.clientX)<215 ){
    //   this.setState({gestureStatus:true})
    // }
    // else{
    //   this.setState({gestureStatus:false})
    // }
    if (Math.abs(this.clientX)) {
      if (!this.props.preventScroll){
        this.stopScrolling(true);
        console.log ("preb false")
      }
      setTimeout(function () {
        // _self.stopScrolling(false);
        if (_self.props.preventScroll){
          _self.stopScrolling(false);
          console.log ("preb true")
        }
      }, 500);
    }
    // console.log(Math.abs(this.clientX),'===============');    
  }

  setGestureStartState = (e) => {
    this.setState({ gestureStatus: true })
  }

  stopScrolling = (data) => {
    this.props.stopScrolling(data)
  }

  getSwiper = () => {
    var swiper = (
      // <Suspense fallback={<ShadowSkeletonComment />}>
      <View style={{ marginTop: 3 }} 
      // onTouchMove={(e) => this.props.commentData.length > 1 && this.setGestureState(e)}
      //  onTouchStart={(e) => this.props.commentData.length > 1 && this.setGestureStartState(e)}
       >

        <Swiper
          ref={this.swiperRef}
          gesturesEnabled={this.props.commentData.length > 1 ? true : false}
          // gesturesEnabled={!this.state.gestureStatus}
          //gesturesEnabled={() => this.state.gesturesEnabled}
          // gesturesEnabled={this.props.gestureStatus}
          controlsEnabled={this.state.disabledSwaper ? false : true}
          controlsProps={{
            dotsTouchable: false,
            dotsPos: false,
            prevPos: "top-left",
            nextPos: "top-right",
            //prevPos: this.state.editModalVisible == false ? "top-left" : "",
            //nextPos: this.state.editModalVisible == false ? "top-right" : "",
            PrevComponent: ({ onPress }) => (
              <Hoverable>
                {isHovered => (
                  <TouchableOpacity
                    style={{
                      height: this.state.setcalHeight[this.state.setindex],
                      justifyContent: "center",
                      alignSelf: "center",
                      position: "absolute",
                      marginTop: this.state.setcalHeight[this.state.setindex] ?
                        this.state.setcalHeight[this.state.setindex] / 2 - 10 : 10
                      // paddingBottom:
                      //   this.state.setcalHeight[this.state.setindex] > 100
                      //     ? 40 +
                      //       this.state.setcalHeight[this.state.setindex] / 2
                      //     : 80 + this.state.setcalHeight[this.state.setindex]
                    }}
                    onPress={onPress}
                  >
                    <Image
                      // source={require(isHovered == true
                      //   ? "../assets/image/left-black.png"
                      //   : "../assets/image/left-light-black.png")}
                      source={
                        isHovered == true
                          ? require("../assets/image/left-black.png")
                          : require("../assets/image/left-light-black.png")
                      }
                      style={{
                        alignSelf: "flex-start",
                        width: 15,
                        height: 15,
                        marginRight: 20
                      }}
                    />
                  </TouchableOpacity>
                )}
              </Hoverable>
            ),
            NextComponent: ({ onPress }) => (
              <Hoverable>
                {isHovered => (
                  <TouchableOpacity
                    style={{
                      // height: this.state.setcalHeight[this.state.setindex],
                      justifyContent: "center",
                      alignSelf: "center",
                      alignItems: "center",
                      position: "absolute",
                      marginTop: this.state.setcalHeight[this.state.setindex] ?
                        this.state.setcalHeight[this.state.setindex] / 2 - 10 : 10
                      // paddingBottom:
                      //   this.state.setcalHeight[this.state.setindex] > 100
                      //     ? 40 +
                      //       this.state.setcalHeight[this.state.setindex] / 2
                      //     : 80 + this.state.setcalHeight[this.state.setindex]
                    }}
                    onPress={onPress}
                  >
                    <Image
                      // source={require(isHovered == true
                      //   ? "../assets/image/right-black.png"
                      //   : "../assets/image/right-light-black.png")}
                      source={
                        isHovered == true
                          ? require("../assets/image/right-black.png")
                          : require("../assets/image/right-light-black.png")
                      }
                      style={{
                        width: 15,
                        height: 15,
                        alignSelf: "flex-end",
                        marginLeft: 20
                      }}
                    />
                  </TouchableOpacity>
                )}
              </Hoverable>
            )
          }}
          containerStyle={{
            height: this.state.setcalHeight[this.state.setindex]
          }}
          innerContainerStyle={{
            minHeight: this.state.setcalHeight[this.state.setindex]
          }}
          onIndexChanged={index => {
            this.setState({
              setindex: index
            }, () => {
              this.stopScrolling(false)
            });
            this.swiperRef.current._setHeight(this.state.setcalHeight[index]);
          }}
          onAnimationStart={index => {
            this.stopScrolling(true);
          }}
          onAnimationEnd={index => {
            this.stopScrolling(false);
          }}
        >
          
          {this.props.commentData.map((item, i) => {
            return (
              <View key={i}>
                {/* <PostDetailsCommentSwaper
                  onRef={el =>
                  (this.postDetailsCommentSwaperRef[
                    item.node.id.replace("Comment:", "")
                  ] = el)
                  }
                  nativeID={item.node.id.replace("Comment:", "")}
                  key={i}
                  index={i}
                  item={item}
                  navigation={this.props.navigation}
                  clickList={item.node.clik ? [item.node.clik] : null}
                  parent_content_id={this.state.parent_content_id}
                  closeModalBySubmit={this.onSubmit}
                  calHeight={this.calHeightModi}
                  margin={this.props.isFirstComment == true ? 0 : 30}
                  modalVisible={this.setModal}
                  editmodalVisible={this.setEditModal}
                  swiperRef={this.swiperRef}
                  modalStatus={this.state.modalVisible}
                  setGesture={this.setGesture}
                  commentHeight={this.state.commentHeight}
                  disableSwaper={this.disableSwaper}
                  contentHeight={this.state.setcalHeight[this.state.setindex]}
                /> */}
              </View>
            );
          })}
        </Swiper>
        {/* <View
            style={{
              height: 1,
              backgroundColor: "#e1e1e1"
            }}
          ></View> */}
      </View>
      // </Suspense>
    );
    return swiper;
  };
  getcomment = () => {
    var comment = <View />;
    if (this.props.commentData.length > 0) {
      comment = this.getSwiper();
    }
    return comment;
  };

  //* showmore
  componentDidUpdate=(prevProps,prevState)=>{
    //console.log(prevProps.index,this.props.index);
    let arr= this.state.setShowArray;
     let newArray=[];
   //console.log('========================',prevProps.index,this.props.index,prevProps.showMoreClickNumber,this.props.showMoreClickNumber);
   if(prevProps.showMoreClickNumber!=this.props.showMoreClickNumber){
     
     newArray=arr.slice(0,3*this.props.showMoreClickNumber)
     this.setState({getShowMoreArray:newArray})
     //console.log(newArray);
   }
   if(this.props.showMoreClickNumber==1 && prevProps.index!=this.props.index){
    newArray=arr.slice(0,3)
    this.setState({getShowMoreArray:newArray})
    //console.log(newArray,'---------------');
   }
   //console.log(newArray,'---------------');
   //console.log(this.state.setShowArray,'-------------------',this.state.getShowMoreArray);
  }

  //* showmore
  setCommentArray=(list)=>{    
    this.setState({setShowArray:list})
  }

  // componentDidMount=()=>{
  //   this.setState({getShowMoreArray:this.state.setShowArray.slice(0,3)})
  // }

 

  
  getrecursiveCommentThread = () => {
    
    var recursiveCommentThread = <View />;
    if (this.props.commentData.length > 0) {
      console.log(this.props.commentData.length>0 && this.props.commentData[this.state.setindex].node.id,this.props.parentCartId,'------------------',this.props.index)
      let getShowArray=this.props.commentData[this.state.setindex].comments.edges.length >2 ?this.setCommentArray(this.props.commentData[this.state.setindex].comments.edges):this.props.commentData[this.state.setindex].comments.edges;
      //console.log(getShowArray,'========================================');
      recursiveCommentThread = (
        <ChildCommentSwaper
          commentData={
            this.props.commentData[this.state.setindex].comments.edges.length >
              0
              ? this.props.commentData[this.state.setindex].comments.edges.length < 3 ? this.props.commentData[this.state.setindex].comments.edges.slice(0,3) : this.props.index >=0 && this.state.getShowMoreArray            
              : []
          }
          currentIndex={this.state.setindex}
          closeModalBySubmit={this.onSubmit}
          navigation={this.props.navigation}
          isFirstComment={false}
          stopScrolling={this.stopScrolling}
          preventScroll = {this.props.preventScroll}
        />
      );
    }
    return recursiveCommentThread;
  };

  render() {
    return (
      <View>
        {this.getcomment()}
        {this.state.modalVisible == false ? (
          this.getrecursiveCommentThread()
        ) : (
            <View style={{ marginLeft: 30, marginTop: 5,marginBottom:  5,marginRight: Dimensions.get("window").width <= 750 ? 5: 0 }}>
              <CreateCommentCard
                onClose={() => this.onClose(this.state.parent_content_id)}
                parent_content_id={this.state.parent_content_id}
                closeModalBySubmit={this.onSubmit}
                clickList={
                  this.props.commentData[this.state.setindex].node.clik
                    ? [...this.props.commentData[this.state.setindex].node.clik]
                    : null
                }
                initial="child"
                topComment={this.props.commentData[this.state.setindex].node}
                navigation={this.props.navigation}
                outSideClick={this.outSideClick}
              />
            </View>
          )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List()
});

const mapDispatchToProps = dispatch => ({
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setFeedReportModalAction: payload =>
    dispatch(setFEEDREPORTMODALACTION(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  ChildCommentSwaper
);
