import React, { useState } from 'react';
import { Image, ImageBackground, Text, TouchableOpacity, View } from 'react-native';
import ConstantFontFamily from '../constants/FontFamily';


const TrendingCard = (props) => {
    const [borderWidth, setborderWidth] = useState(0);
    const [borderColor, setborderColor] = useState(" ");

    const makeActive = (id) => {
        let getborderWidth = !borderWidth ? 2 : 0;
        let getborderColor = !borderColor ? "" : 'green';
        setborderWidth(getborderWidth)
        setborderColor(getborderColor)
    }

    return (
        <View>
            <TouchableOpacity
                style={{ borderRadius: 4, overflow: 'hidden', margin: 5, borderWidth: borderWidth, borderColor: borderColor }}
                onPress={() => makeActive(props.data.node.id)}
            >
                <ImageBackground style={{
                    height: 45,
                }}
                    source={{
                        uri: props.data.node.banner_background.background_pic, cache: 'force-cache'
                    }}
                >
                    <View style={{ flexDirection: 'row' }}>
                        {(props.data.node.profile_pic) ?
                            <Image
                                source={{
                                    uri: props.data.node.profile_pic,
                                    cache: 'force-cache'
                                }}
                                style={{
                                    width: 45,
                                    height: 45,
                                }}
                            />
                            :
                            <Image
                                source={require('../assets/image/default-image.png')}
                                style={{
                                    width: 45,
                                    height: 45,
                                }}
                            />
                        }
                        <Text style={{
                            alignSelf: 'center',
                            textAlign: 'center',
                            fontSize: 18,
                            fontWeight: 'bold',
                            fontFamily: ConstantFontFamily.defaultFont,
                            marginLeft: 7
                        }}>@{props.data.node.first_name} {props.data.node.last_name}</Text>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        </View>
    );
}

export default (TrendingCard);