import React, { Component } from "react";
import { Animated, Dimensions, View } from "react-native";
import ButtonStyle from "../constants/ButtonStyle";

class ShadowSkeletonComment extends React.Component {
  constructor(props) {
    super(props);
    this.Pagescrollview = null;
    this.circleAnimatedValue = new Animated.Value(0);
  }

  componentDidMount() {
    this.circleAnimated();
  }

  circleAnimated = () => {
    this.circleAnimatedValue.setValue(0);
    Animated.timing(this.circleAnimatedValue, {
      toValue: 1,
      duration: 1000
    }).start(() => {
      setTimeout(() => {
        this.circleAnimated();
      }, 1000);
    });
  };

  render() {
    const translateX = this.circleAnimatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [-10, Dimensions.get("window").width]
    });
    const translateTitle = this.circleAnimatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [-10, Dimensions.get("window").width]
    });
    return (
      <View>
        {[0, 1, 2, 3, 4].map((item, index) => {
          return (
            <View
              style={[
                ButtonStyle.borderStyle,
                {
                  height: 100,
                  width: "95%",
                  marginTop: 10,
                  marginLeft: 5,
                  backgroundColor: "rgba(227, 227, 227, 0.3)",
                  overflow: "hidden"
                }
              ]}
              key={index}
            >
              <Animated.View
                style={[
                  ButtonStyle.borderStyle,
                  {
                    width: "50%",
                    height: 60,
                    backgroundColor: "#e3e3e3",
                    transform: [{ translateX: translateTitle }]
                  }
                ]}
              ></Animated.View>
            </View>
          );
        })}
      </View>
    );
  }
}
export default ShadowSkeletonComment;
