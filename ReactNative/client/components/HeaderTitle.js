import React from 'react';
import { Text, View, Platform, TouchableOpacity } from 'react-native';
import { connect } from "react-redux";
import { compose } from 'recompose';
import ConstantFontFamily from '../constants/FontFamily';
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";


const HeaderTitle = (props) => {

	const goToScrollTop = () => {
		props.setHASSCROLLEDACTION(true)
	}

	return (
		<TouchableOpacity
			onPress={() => goToScrollTop()}
			style={{
				alignSelf: 'center',
				alignItems: "center",
				flex: 1
			}}>
			<Text style={{
				color: "#fff",
				fontFamily: ConstantFontFamily.defaultFont,
				fontSize: Platform.OS == 'web' && props.getCurrentDeviceWidthAction > 750 ? 25 : props.customeTitle.length > 10 ? 14 : 20,
				fontWeight: 'bold'
			}}>{props.customeTitle}</Text>

		</TouchableOpacity>
	)
}


const mapStateToProps = state => ({
	loginStatus: state.UserReducer.get('loginStatus'),
	getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get('dimension'),
});


const mapDispatchToProps = dispatch => ({
	setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(HeaderTitle);