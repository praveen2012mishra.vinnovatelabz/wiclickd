import firebase from "firebase/app";
import "firebase/auth";
import React, { Component } from "react";
import {
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  AsyncStorage,
  Platform,
  Image,
  Dimensions
} from "react-native";
import { connect } from "react-redux";
import { setCreateAccount } from "../actionCreator/CreateAccountAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { setVERIFYEMAILMODALACTION } from "../actionCreator/VerifyEmailModalAction";
import ConstantFontFamily from "../constants/FontFamily";
import getEnvVars from "../environment";
import { getLocalStorage, setLocalStorage } from "../library/Helper";
import jwt_decode from "jwt-decode";
import {
  saveUserLoginDaitails,
  setLoginStatus
} from "../actionCreator/UserAction";
import { setAdminStatus } from "../actionCreator/AdminAction";
import { Button, Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { setUSERNAMEMODALACTION } from "../actionCreator/UsernameModalAction";
import SidePanel from "./SidePanel";
import NavigationService from "../library/NavigationService";
import ButtonStyle from "../constants/ButtonStyle";

const apiUrl = getEnvVars();

class SignUpMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: "unsend",
      buttonName: "Creating account...",
      disableBtn: true,
      UserName: ""
    };
  }
  componentDidMount() {
    AsyncStorage.getItem("newUserFirebase").then(r => {
      let data = JSON.parse(r);
      this.setState({
        UserName: data ? data.UserName : ""
      });
    });
    setTimeout(
      () => this.setState({ buttonName: "Continue", disableBtn: false }),
      5000
    );
  }

  formSubmit = async () => {
    let __self = this;
    let actionCodeSettings = {
      url: apiUrl.APPDYNAMICLINK,
      handleCodeInApp: true
    };
    await firebase
      .auth()
      .currentUser.getIdToken(true)
      .then(async function(idToken) {
        return await getLocalStorage("userIdTokenFirebase").then(async res => {
          await setLocalStorage("userIdTokenFirebase", idToken);
          await setLocalStorage(
            "admin",
            jwt_decode(idToken).admin ? "true" : "false"
          );
          await __self.props.changeAdminStatus(
            jwt_decode(idToken).admin ? jwt_decode(idToken).admin : false
          );
          return idToken;
        });
      })
      .then(async res => {
        if (res) {
          await firebase
            .auth()
            .currentUser.sendEmailVerification(actionCodeSettings)
            .then(res => {
              AsyncStorage.removeItem("userIdTokenFirebase");
              __self.props.setCreateAccount({
                username: "",
                email: "",
                password: "",
                first_name: "",
                last_name: ""
              });
              this.setState({
                status: "resend"
              });
              //__self.props.setVerifyEmailModalStatus(false);
            })
            .catch(e => {
              // __self.props.setVerifyEmailModalStatus(false);
              if (e.code == "auth/too-many-requests") {
                alert(e.message);
              } else {
                console.log(e);
              }
            });
        }
      });
  };

  render() {
    return (
      <View
        style={{
          backgroundColor: "#fff",
          borderColor: "#c5c5c5",
          borderRadius: 6,
          // maxHeight: 450,
          maxWidth: 450,
          alignSelf:'center',
          height:500,
        }}
      >
        {/* <View style ={{width:Dimensions.get("window").width - 550}}>
    <SidePanel
      ref={navigatorRef => {
        NavigationService.setTopLevelNavigator(navigatorRef);
      }}
      navigation={NavigationService}
    /></View>   */}
        <View>
          <Hoverable>
            {isHovered => (
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  flex: 1,
                  position: "absolute",
                  zIndex: 999999,
                  left: 0,
                  top: 0
                }}
                onPress={this.props.onClose}
              >
                <Icon
                  color={isHovered == true ? "rgba(256,256,256,0.4)" : "#000"}
                  iconStyle={{
                    color: "#fff",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  reverse
                  name="close"
                  type="antdesign"
                  size={16}
                />
              </TouchableOpacity>
            )}
          </Hoverable>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              backgroundColor: "#000",
              alignItems: "center",
              height: 50,
              borderTopLeftRadius: 6,
              borderTopRightRadius: 6,
              width: "100%"
            }}
          >
            {/* <Text style={[styles.TextHeaderStyle, { color: '#fff' }]}>Sign Up</Text> */}
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Image
                source={require("../assets/image/logo.png")}
                style={{
                  height: 30,
                  width: 30,
                  marginRight: 5
                }}
                resizeMode={"contain"}
              />
              <Text
                style={{
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  fontWeight: "bold",
                  fontSize: 22,
                  textAlign: "center",
                  color: "white"
                }}
              >
                weclikd
                  </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            width: "100%",
            padding: 30,
            flex: 1,
            alignItems: "center"
          }}
        >
          <View
            style={{
              overflow: "visible",
              width: "100%",
              backgroundColor: "#fff",
              alignItems: "center",
              justifyContent: "center"
              // borderBottomLeftRadius: 6,
              // borderBottomRightRadius: 6
            }}
          >
            {/* <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "row"
              }}
            >
              <Image
                source={require("../assets/image/logo.png")}
                style={{
                  height: 60,
                  width: 60
                }}
                resizeMode={"contain"}
              />
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 45,
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
              >
                weclikd
              </Text>
            </View> */}

            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                marginVertical: 20,
                marginHorizontal: 50
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  fontWeight: "bold",
                  fontSize: 18,
                  fontFamily: ConstantFontFamily.defaultFont
                }}
              >
                A message from the founders.
              </Text>
            </View>

            {/* <ScrollView
            style={{
              backgroundColor: "#fff",
              marginTop: 10,
              paddingHorizontal: 20
            }}
          > */}
            <View>
              <Text style={styles.textStyle}>Dear {this.state.UserName},</Text>
              <Text style={styles.textStyle}>
                We are the first dedicated platform solely for intellectual
                discourse. This is the only platform where the most thoughtful
                content, not the most popular gets recognized and paid.
              </Text>
              <Text style={styles.textStyle}>
                To gain the best experience, ensure your posts are sequitur to
                the discussions and invite others to join.
              </Text>
              {/* <Text
                style={{
                  marginTop: 20,
                  textAlign: "left",
                  fontWeight: "bold",
                  fontSize: 15,
                  fontFamily: ConstantFontFamily.defaultFont
                }}
              >
                Our main differential factor is cliks, which is a discussion group. Each clik's discussion is shown together for a given post which helps facilitate diverse, quality discourse.
            </Text> */}
              <Text style={styles.textStyle}>
                Let us build a better discussion platform.
                <Text
                  style={[
                    styles.textStyle,
                    {
                      fontSize: 17,
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }
                  ]}
                >
                  #Weclikd
                </Text>
              </Text>
            </View>
          </View>
          <View style={{ marginTop: 20 }}>
            <TouchableOpacity
              block
              style={[
                ButtonStyle.backgroundStyle,
                ButtonStyle.containerStyle,
                { justifyContent: "center" }
              ]}
              onPress={() => {
                this.props.setUsernameModalStatus(false);
                this.props.setMessageModalStatus(false);
                // window.location.reload(true)
              }}
              disabled={this.state.disableBtn}
            >
              <Text
                style={[
                  ButtonStyle.titleStyle,
                  {
                    textAlign: "center"
                  }
                ]}
              >
                {this.state.buttonName}
              </Text>
            </TouchableOpacity>
          </View>
          {/* </ScrollView> */}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  getCreateAccount: state.CreateAccountReducer.get("setCreateAccountData"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension")
});

const mapDispatchToProps = dispatch => ({
  setVerifyEmailModalStatus: payload =>
    dispatch(setVERIFYEMAILMODALACTION(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setCreateAccount: payload => dispatch(setCreateAccount(payload)),
  changeLoginStatus: payload => dispatch(setLoginStatus(payload)),
  changeAdminStatus: payload => dispatch(setAdminStatus(payload)),
  SignUpFollowModalStatus: payload =>
    dispatch({ type: "SIGNUP_FOLLOW_MODAL", payload }),
  setUsernameModalStatus: payload => dispatch(setUSERNAMEMODALACTION(payload)),
  setMessageModalStatus: payload =>
    dispatch({ type: "MESSAGEMODALSTATUS", payload }),
  setInviteUserDetail: payload =>
    dispatch({ type: "SET_INVITE_USER_DETAIL", payload })
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUpMessage);
export const styles = {
  TextHeaderStyle: {
    fontSize: 23,
    color: "#000",
    textAlign: "center",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  StaticTextStyle: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    fontFamily: ConstantFontFamily.defaultFont
  },
  textStyle: {
    marginTop: 20,
    textAlign: "left",
    fontWeight: "bold",
    fontSize: 15,
    fontFamily: ConstantFontFamily.defaultFont
  }
};
