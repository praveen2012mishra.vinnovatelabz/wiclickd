import React from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import { View, Text, TouchableOpacity, Image } from "react-native";
import ConstantFontFamily from "../constants/FontFamily";

class Publication extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          marginBottom: 10
        }}
      >
        <View
          style={{
            flex: 1,
            width: "100%",
            borderColor: "#525252",
            borderWidth: 1,
            borderRadius: 6
          }}
        >
          <View
            style={{
              justifyContent: "flex-end",
              alignSelf: "flex-end",
              alignContent: "flex-end",
              width: "5%",
              margin: 8
            }}
          >
            <TouchableOpacity>
              <Image
                source={require("../assets/image/edit.png")}
                style={{
                  alignSelf: "flex-end",
                  width: 15,
                  height: 15
                }}
              />
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: "100%",
              flexDirection: "column"
            }}
          >
            <View
              style={{
                width: "100%",
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  width: "95%",
                  flexDirection: "row",
                  paddingLeft: 10
                }}
              >
                <View
                  style={{
                    flexDirection: "row"
                  }}
                >
                  <Text
                    style={{
                      color: "#009B1A",
                      fontSize: 16,
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }}
                  >
                    TITLE{" "}
                  </Text>
                  <Text
                    style={{
                      color: "#3482C3",
                      fontSize: 16,
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }}
                  >
                    @Username
                    {/* {this.props.profileData
                      .getIn(["my_users"])
                      .getIn(["0", "user", "username"])} */}
                  </Text>
                </View>
              </View>
            </View>

            <View
              style={{
                width: "100%",
                flexDirection: "row",
                justifyContent: "flex-start",
                padding: 10,
                paddingLeft: "10%"
              }}
            >
              <Text
                style={{
                  color: "#5C5C5E",
                  fontSize: 14,
                  fontWeight: "800",
                  fontFamily: ConstantFontFamily.defaultFont,
                  fontStyle: "italic"
                }}
              >
                A short paragraph delineating importance of what you wrote. How
                is it important how has it contributed to society, and what did
                you learn in publishing it?
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails")
});

const mapDispatchToProps = dispatch => ({
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  Publication
);
