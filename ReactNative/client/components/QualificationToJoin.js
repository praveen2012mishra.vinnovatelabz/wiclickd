import React, { Component, lazy, Suspense } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  CheckBox,
  StyleSheet,
  Platform,
  TextInput
} from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import ButtonStyle from "../constants/ButtonStyle";
import { Button, Icon } from "react-native-elements";
import ConstantFontFamily from "../constants/FontFamily";
import applloClient from "../client";
import { ClikJoinMutation } from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { ClikJoinVariables } from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { graphql } from "react-apollo";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import { Hoverable } from "react-native-web-hooks";
import { List } from "immutable";
import { editClik } from "../actionCreator/ClikEditAction";

const styles = StyleSheet.create({
  checkbox: {
    alignSelf: "center",
    backgroundColor: "#000"
  }
});

class QualificationToJoin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qualification: "",
      username: "",
      showError: false,
      MutipleUserList: [],
      UserList: [],
      status: "Default",
      member_type: "",
      user_msg: "",
      invite_key: "",
      isSelected: false,
      getCheckboxContent: [],
      otherqualification: "",
      Linkedinqualification: ""
    };
  }

  getQualificationString = () => {
    let value = this.props.type == "Apply" ? true : false;
    let newArray = [];
    this.state.getCheckboxContent.forEach(obj => {
      if (obj.checked == value) {
        if (obj.item == "Add your Linkedin") {
          newArray.push("Linkedin " + this.state.Linkedinqualification);
        } else {
          newArray.push(obj.item);
        }
      }
    });
    if (this.state.otherqualification != "" && this.props.type == "Apply") {
      newArray.push("Other:" + this.state.otherqualification);
    }
    return newArray.join("\n");
  };

  requestInvite = async () => {
    ClikJoinVariables.variables.clik_id = this.props.cliksDetails
      .getIn(["data", "clik"])
      .get("id");
    ClikJoinVariables.variables.qualification = this.getQualificationString();
    ClikJoinVariables.variables.known_members = [];
    ClikJoinVariables.variables.invite_key = this.state.invite_key;
    try {
      await applloClient
        .query({
          query: ClikJoinMutation,
          ...ClikJoinVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          if (res.data.clik_join.status.custom_status == "JOINED") {
            let member_type = res.data.clik_join.member_type;
            this.setState({
              status: "Success",
              member_type: member_type
            });
            let resDataLogin = await this.props.Login();
            await this.props.saveLoginUser(resDataLogin.data.login);
          } else if (res.data.clik_join.status.custom_status == "PENDING") {
            this.setState({
              status: "Pending"
            });
          } else {
            let user_msg = res.data.clik_join.status.user_msg;
            this.setState({
              status: "Failure",
              user_msg: user_msg
            });
          }
        });
    } catch (e) {
      console.log(e);
    }
  };

  componentDidMount() {
    let applyArray = [
      {
        item: "Add your Linkedin",
        checked: false,
        id: 0
      }
    ];
    let newArray = [];
    newArray = this.props.cliksDetails
      .getIn(["data", "clik"])
      .get("qualifications")
      ? this.props.cliksDetails
        .getIn(["data", "clik"])
        .get("qualifications")
        .map((item, index) => {
          return { item: item, checked: false, id: index };
        })
      : [];
    if (this.props.type == "Apply") {
      newArray = newArray.concat(applyArray);
    }
    this.setState({ getCheckboxContent: newArray });
  }

  getClikEdit = () => {
    let value = false;
    if (this.props.loginStatus == 1) {
      const Clikindex = this.props.listClikMembers.findIndex(
        i =>
          i.node.user.id ==
          this.props.profileData.getIn(["my_users", "0", "user", "id"]) &&
          (i.node.type == "SUPER_ADMIN" || i.node.type == "ADMIN")
      );
      if (Clikindex != -1) {
        value = true;
      } else {
        value = false;
      }
    }
    return value;
  };
  render() {
    return (
      <View
        style={[
          ButtonStyle.cardShadowStyle,
          {
            backgroundColor: "#fff",
            borderWidth:0,
            marginVertical: 10
          }
        ]}
      >
        <View style={{ flexDirection: "row", alignItems: 'center' }}>
          <Text
            style={{
              textAlign: "left",
              color: "#000",
              fontSize: 16,
              fontWeight: "bold",
              fontFamily: ConstantFontFamily.MontserratBoldFont,
              marginVertical: 10,
              marginHorizontal: 10
            }}
          >
            Qualifications
          </Text>
          {this.getClikEdit() == true && (
            <Hoverable>
              {isHovered => (
                <Icon
                  iconStyle={{
                    alignSelf: "center"
                  }}
                  name="edit"
                  type="font-awesome"
                  color={"#000"}
                  size={18}
                  onPress={async () => {
                    await this.props.editClik({
                      clikId:
                        this.props.cliksDetails &&
                        this.props.cliksDetails
                          .getIn(["data", "clik"])
                          .get("id"),
                      cliksName:
                        this.props.cliksDetails &&
                        this.props.cliksDetails
                          .getIn(["data", "clik"])
                          .get("name"),
                      banner_pic:
                        this.props.cliksDetails &&
                        this.props.cliksDetails
                          .getIn(["data", "clik"])
                          .get("banner_pic"),
                      changeBackPicEnable:
                        this.props.cliksDetails &&
                        this.props.cliksDetails
                          .getIn(["data", "clik"])
                          .get("banner_pic")
                          .split("/")
                        [
                          this.props.cliksDetails
                            .getIn(["data", "clik"])
                            .get("banner_pic")
                            .split("/").length - 1
                        ].substring(
                          0,

                          this.props.cliksDetails
                            .getIn(["data", "clik"])
                            .get("banner_pic")
                            .split("/")
                          [
                            this.props.cliksDetails
                              .getIn(["data", "clik"])
                              .get("banner_pic")
                              .split("/").length - 1
                          ].indexOf(".")
                        ),
                      description:
                        this.props.cliksDetails &&
                        this.props.cliksDetails
                          .getIn(["data", "clik"])
                          .get("description"),
                      cliksFollowers:
                        this.props.cliksDetails &&
                        this.props.cliksDetails
                          .getIn(["data", "clik"])
                          .get("num_followers"),
                      cliksMembers:
                        this.props.cliksDetails &&
                        this.props.cliksDetails
                          .getIn(["data", "clik"])
                          .get("num_members"),
                      website:
                        this.props.cliksDetails &&
                        this.props.cliksDetails
                          .getIn(["data", "clik"])
                          .get("website"),
                      invite_only:
                        this.props.cliksDetails &&
                        this.props.cliksDetails
                          .getIn(["data", "clik"])
                          .get("invite_only"),
                      qualifications:
                        this.props.cliksDetails &&
                        this.props.cliksDetails
                          .getIn(["data", "clik"])
                          .get("qualifications")
                    });
                    this.props.navigation.navigate("editclik");
                  }}
                />
              )}
            </Hoverable>
          )}
        </View>
        {this.props.cliksDetails
          .getIn(["data", "clik"])
          .get("qualifications") == null && (
            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.defaultFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              No qualifications
            </Text>
          )}

        {this.props.cliksDetails
          .getIn(["data", "clik"])
          .get("qualifications") != null &&
          this.state.getCheckboxContent.map((ele, index) => {
            return (
              <View style={{ flexDirection: "row" }} key={index}>
                <Text
                  style={{
                    color: "#000",
                    fontFamily: ConstantFontFamily.defaultFont,
                    fontSize: 14,
                    fontWeight: "bold",
                    marginBottom: 20,
                    marginLeft: 30
                  }}
                >
                  {this.props.cliksDetails
                    .getIn(["data", "clik"])
                    .get("invite_only") == true &&
                    this.props.type == "Apply" ? (
                      <CheckBox
                        value={ele.checked}
                        onClick={() => {
                          let newArray = this.state.getCheckboxContent.map(
                            (item1, id) => {
                              if (id === index) {
                                item1.checked = !item1.checked;
                                return item1;
                              } else return item1;
                            }
                          );
                          this.setState({ getCheckboxContent: newArray });
                        }}
                        style={styles.checkbox}
                        checked={ele.checked}
                        color={ele.checked ? "#009B1A" : undefined}
                      />
                    ) : (
                      <Text>{"\u25CF"}</Text>
                    )}{" "}
                </Text>
                <Text
                  style={{
                    color: "#000",
                    fontFamily: ConstantFontFamily.defaultFont,
                    fontSize: 14,
                    fontWeight: "bold",
                    marginRight: 10
                  }}
                >
                  {ele.item}{" "}
                </Text>
                {ele.item == "Add your Linkedin" && (
                  <TextInput
                    value={this.state.Linkedinqualification}
                    placeholder={"Copy Link to Linkedin Profile Here"}
                    style={[this.state.focusInput ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle,
                      {
                      alignSelf: "center",
                      padding: 5,
                      color: "#000",
                      fontFamily: ConstantFontFamily.defaultFont,
                      fontSize: 14,
                      borderRadius: 6,
                      // borderColor: "#c5c5c5",
                      borderWidth: 0,
                      marginLeft: 3,
                      marginBottom: 10,
                      width: "50%",
                      position: "relative",
                      bottom: 5
                    }]}
                    onChangeText={text =>
                      this.setState({ Linkedinqualification: text })
                    }
                    onFocus = {()=> this.setState({focusInput : true})}
                    onBlur = {()=> this.setState({focusInput : false})}
                  />
                )}
              </View>
            );
          })}
        {this.props.type == "Apply" && this.props.cliksDetails
          .getIn(["data", "clik"])
          .get("qualifications") != null && (
            <View>
              <TextInput
                value={this.state.otherqualification}
                multiline={true}
                numberOfLines={Platform.OS === "ios" ? null : 4}
                placeholder={
                  "Click on each box that you qualify above. Add any other qualifications you want to share here to #" +
                  this.props.cliksDetails.getIn(["data", "clik"]).get("name") +
                  " admins."
                }
                style={[this.state.focusInputQualification ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle,
                  {
                  padding: 5,
                  color: "#000",
                  fontFamily: ConstantFontFamily.defaultFont,
                  fontSize: 14,
                  borderRadius: 6,
                  // borderColor: "#c5c5c5",
                  borderWidth: 0,
                  marginLeft: 25,
                  width: "95%"
                }]}
                onChangeText={text => this.setState({ otherqualification: text })}
                onFocus = {()=> this.setState({focusInputQualification : true})}
                onBlur = {()=> this.setState({focusInputQualification : false})}
              />
            </View>
          )}
        {this.props.type == "Apply" && this.state.status == "Default" && (
          <Button
            onPress={() => this.requestInvite()}
            color="#000"
            title="Apply"
            titleStyle={ButtonStyle.titleStyle}
            buttonStyle={ButtonStyle.backgroundStyle}
            containerStyle={ButtonStyle.containerStyle}
          />
        )}

        {this.props.cliksDetails.getIn(["data", "clik"]).get("invite_only") ==
          false &&
          this.state.status == "Default" && (
            <Button
              onPress={() => this.requestInvite()}
              color="#000"
              title="Join"
              titleStyle={ButtonStyle.titleStyle}
              buttonStyle={ButtonStyle.backgroundStyle}
              containerStyle={ButtonStyle.containerStyle}
            />
          )}
        {this.state.status == "Success" && (
          <View
            style={{
              width: "100%",
              backgroundColor: "#fff",
              paddingVertical: 20
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              Congratulations!
            </Text>

            <View
              style={{
                justifyContent: "center",
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  color: "#000",
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  fontSize: 16,
                  fontWeight: "bold",
                  margin: 10
                }}
              >
                You are now a {this.state.member_type.toLowerCase()} of{" "}
              </Text>

              <TouchableOpacity
                style={{
                  marginTop: 5,
                  height: 30,
                  alignSelf: "flex-start",
                  padding: 5,
                  backgroundColor: "#E8F5FA",
                  borderRadius: 6
                }}
              >
                <Hoverable>
                  {isHovered => (
                    <Text
                      style={{
                        width: "100%",
                        color: "#4169e1",
                        fontSize: 15,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont,
                        textDecorationLine:
                          isHovered == true ? "underline" : "none"
                      }}
                    >
                      {" "}
                      #
                      {this.props.cliksDetails
                        .getIn(["data", "clik"])
                        .get("name")}{" "}
                    </Text>
                  )}
                </Hoverable>
              </TouchableOpacity>
            </View>
          </View>
        )}

        {this.state.status == "Failure" && (
          <View
            style={{
              width: "100%",
              backgroundColor: "#fff",
              paddingVertical: 20
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              Sorry, you could not join the clik at this time.
            </Text>

            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              {this.state.user_msg}
            </Text>
          </View>
        )}

        {this.state.status == "Pending" && (
          <View
            style={{
              width: "100%",
              backgroundColor: "#fff",
              paddingVertical: 20
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              An admin of the clik will review your application.
            </Text>
          </View>
        )}
      </View>
    );
  }
}
const mapStateToProps = state => ({
  cliksDetails: state.TrendingCliksProfileReducer.get(
    "getTrendingCliksProfileDetails"
  ),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  listClikUserRequest: !state.ClikUserRequestReducer.get("ClikUserRequestList")
    ? List()
    : state.ClikUserRequestReducer.get("ClikUserRequestList"),
  listClikMembers: !state.ClikMembersReducer.get("clikMemberList")
    ? List()
    : state.ClikMembersReducer.get("clikMemberList"),
  loginStatus: state.UserReducer.get("loginStatus"),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List()
});

const mapDispatchToProps = dispatch => ({
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  editClik: payload => dispatch(editClik(payload))
});

const QualificationToJoinWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(QualificationToJoin);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  QualificationToJoinWrapper
);
