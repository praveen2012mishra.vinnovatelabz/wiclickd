import "@expo/browser-polyfill";
import _ from "lodash";
import { List } from "immutable";
import React, { Component } from "react";
import { graphql } from "react-apollo";
import {
  Platform,
  Dimensions,
  Image,
  TouchableOpacity,
  View
} from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setLikeContent } from "../actionCreator/LikeContentAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingExternalFeeds } from "../actionCreator/TrendingExternalFeedsAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import { getTrendingUsers } from "../actionCreator/TrendingUsersAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import AppHelper from "../constants/AppHelper";
import {
  ClikFollowMutation,
  ClikUnfollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  ClikFollowVariables,
  ClikUnfollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { listClikUserRequest } from "../actionCreator/ClikUserRequestAction";
import { listClikMembers } from "../actionCreator/ClikMembersAction";
import LeaveCliksModal from "../components/LeaveCliksModal";
import Modal from "modal-enhanced-react-native-web";
import Overlay from "react-native-modal-overlay";

class ClikStar extends Component {
  constructor(props) {
    super(props);
    const gstar = require("../assets/image/gstar.png");
    const ystar = require("../assets/image/ystar.png");
    const wstar = require("../assets/image/wstar.png");

    this.state = {
      index: 0,
      starList: [wstar, gstar, ystar],
      followList: ["TRENDING", "FOLLOW", "FAVORITE"],
      showLeaveCliksModal: false
    };
  }

  async componentDidMount() {
    await this.getClikStar(this.props.ClikName);
  }

  componentDidUpdate = async prevProps => {
    if (prevProps.ClikName !== this.props.ClikName) {
      await this.getClikStar(this.props.ClikName);
    }
    if (
      prevProps.getUserFollowCliksList !== this.props.getUserFollowCliksList
    ) {
      await this.getClikStar(this.props.ClikName);
    }
  };

  getClikStar = async ClikName => {
    if (this.props.loginStatus == 0) {
      await this.setState({
        index: 0
      });
      return false;
    }
    let index = 0;
    index = this.props.getUserFollowCliksList.findIndex(
      i =>
        i
          .getIn(["clik", "name"])
          .toLowerCase()
          .replace("clik:", "") == ClikName.toLowerCase().replace("clik:", "")
    );
    if (index == -1) {
      await this.setState({
        index: 0
      });
    } else if (
      this.props.getUserFollowCliksList.getIn([
        index,
        "settings",
        "follow_type"
      ]) == "FOLLOW"
    ) {
      await this.setState({
        index: 1
      });
    } else {
      await this.setState({
        index: 2
      });
    }
  };

  changeStar = async () => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    if (this.state.index + 1 === this.state.starList.length) {
      await this.setState({
        index: 0
      });
      await this.changeStarApi(this.state.followList[this.state.index]);
    } else {
      await this.setState({
        index: this.state.index + 1
      });
      await this.changeStarApi(this.state.followList[this.state.index]);
    }
  };

  changeStarApi = _.debounce(typeIndex => {
    typeIndex == "FAVORITE"
      ? this.favroiteCliks(this.props.ClikName)
      : typeIndex == "TRENDING"
      ? this.unfollowCliks(this.props.ClikName)
      : this.followCliks(this.props.ClikName);
  }, 1000);

  followCliks = cliksId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    ClikFollowVariables.variables.clik_id = cliksId;
    ClikFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: ClikFollowMutation,
        ...ClikFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  favroiteCliks = cliksId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    ClikFollowVariables.variables.clik_id = cliksId;
    ClikFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: ClikFollowMutation,
        ...ClikFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowCliks = async cliksId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    if (
      this.props.MemberType == "SUPER_ADMIN" ||
      this.props.MemberType == "ADMIN" ||
      this.props.MemberType == "MEMBER"
    ) {
      this.setState({
        showLeaveCliksModal: true
      });
      return false;
    }

    ClikUnfollowVariables.variables.clik_id = cliksId;
    applloClient
      .query({
        query: ClikUnfollowMutation,
        ...ClikUnfollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  leaveClik = async () => {
    ClikUnfollowVariables.variables.clik_id = this.props.ClikName;
    applloClient
      .query({
        query: ClikUnfollowMutation,
        ...ClikUnfollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
        this.setState({
          showLeaveCliksModal: false,
          index: 0
        });
      });
  };

  onClose = () => {
    this.setState({
      showLeaveCliksModal: false,
      index: 2
    });
  };

  render() {
    return (
      <TouchableOpacity
        style={this.props.ContainerStyle}
        onPress={() => this.changeStar()}
      >
        {this.state.showLeaveCliksModal == true ? (
          Platform.OS !== "web" ? (
            <Overlay
              animationType="zoomIn"
              visible={this.state.showLeaveCliksModal}
              onClose={() => this.onClose()}
              closeOnTouchOutside
              children={
                <LeaveCliksModal
                  onClose={() => this.onClose()}
                  {...this.props}
                />
              }
              childrenWrapperStyle={{
                padding: 0,
                margin: 0
              }}
            />
          ) : (
            <Modal
              isVisible={this.state.showLeaveCliksModal}
              onBackdropPress={() => this.onClose()}
              style={{
                marginHorizontal:
                  Dimensions.get("window").width > 750 ? "30%" : 10,
                padding: 0
              }}
            >
              <LeaveCliksModal
                onClose={() => this.onClose()}
                leaveClik={() => this.leaveClik()}
                {...this.props}
              />
            </Modal>
          )
        ) : null}
        <Image
          source={this.state.starList[this.state.index]}
          style={this.props.ImageStyle}
        />
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  DiscussionHomeFeed: state.HomeFeedReducer.get("DiscussionHomeFeedList"),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List()
});

const mapDispatchToProps = dispatch => ({
  LikeContent: payload => dispatch(setLikeContent(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setDiscussionHomeFeed: payload =>
    dispatch({ type: "SET_DISCUSSION_HOME_FEED", payload }),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  getTrendingUsers: payload => dispatch(getTrendingUsers(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  getTrendingExternalFeeds: payload =>
    dispatch(getTrendingExternalFeeds(payload)),
  userId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  setClikUserRequest: payload => dispatch(listClikUserRequest(payload)),
  setClikMembers: payload => dispatch(listClikMembers(payload))
});

const ClikStarWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(ClikStar);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  ClikStarWrapper
);
