import { openBrowserAsync } from "expo-web-browser";
import React, { useState } from "react";
import {
  Dimensions,
  ImageBackground,
  Platform,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View
} from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import AppHelper from "../constants/AppHelper";
import ConstantFontFamily from "../constants/FontFamily";

const FeedListContent = props => {
  let [MainHeight, setMainHeight] = useState(0);
  let [TextHeight, setTextHeight] = useState(0);
  let [loading, setLoading] = useState(false);
  let singleItem = props.item.item.node;
  let bgImage = {
    uri: singleItem.background.background_pic,
    cache: "force-cache"
  };
  let profileImage = {
    uri: singleItem.author.profile_pic,
    cache: "force-cache"
  };
  let username = singleItem.author.username;
  let title = singleItem.title;
  let link = singleItem.link;
  let gradientColor = "rgba(0,0,0,0.8)";
  let brightnessvalue = 0.5;
  let textStyle = "#fff";
  let cliksNameList = singleItem.cliks;

  const goToPostDetailsScreen = async id => {
    let title = "Home";
    if (props.navigation.state.routeName == "profile") {
      title = "@" + singleItem.author.username;
    } else if (props.navigation.state.routeName == "cliksprofile") {
      title = "#" + cliksNameList[0];
    } else {
      title = "Home";
    }
    await props.setPostDetails({
      title: title,
      id: id
    });
  };
  const loginModalStatusEvent = async status => {
    props.loginModalStatusEventParent(status);
  };
  const openWindow = async link => {
    await openBrowserAsync(link);
  };

  const goToUserProfile = async (id, username) => {
    props.userId({
      id: id
    });
    props.navigation.navigate("profile", {
      id: id,
      username: username
    });
  };

  const goToCliksProfile = id => {
    if (id != null) {
      props.Cliksfeed({
        id: id,
        currentPage: AppHelper.PAGE_LIMIT
      });

      props.cliksUserId({
        id: id
      });
    }
  };

  return (
    <View>
      {singleItem.background.__typename == "PictureBackground" ? (
        <TouchableHighlight
          onPress={() => goToPostDetailsScreen(singleItem.id)}
          style={{
            overflow: "visible"
          }}
        >
          <ImageBackground
            style={{
              height: hp("35%")
            }}
            source={bgImage}
            resizeMode={"cover"}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "column",
                justifyContent: "flex-end",
                padding: 5
              }}
            >
              <TouchableOpacity
                style={{
                  justifyContent: "flex-end",
                  marginRight: 10,
                  flexDirection: "row"
                }}
                onPress={() =>
                  goToCliksProfile(
                    cliksNameList.length > 0 ? "Clik:" + cliksNameList[0] : null
                  )
                }
              >
                <Text
                  style={{
                    color: "#fff",
                    fontSize:
                      Platform.OS == "web"
                        ? 15
                        : Dimensions.get("window").width * 0.037,
                    fontWeight: "bold",
                    fontFamily: ConstantFontFamily.defaultFont
                  }}
                >
                  {cliksNameList.length > 0 ? "#" + cliksNameList[0] : null}
                </Text>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </TouchableHighlight>
      ) : (
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "flex-end",
            padding: 5
          }}
        >
          <TouchableOpacity
            style={{
              justifyContent: "flex-end",
              margin: 10,
              flexDirection: "row"
            }}
            onPress={() =>
              goToCliksProfile(
                cliksNameList.length > 0 ? "Clik:" + cliksNameList[0] : null
              )
            }
          >
            <Text
              style={{
                color: "#000",
                fontSize: 15,
                fontWeight: "bold",
                fontFamily: ConstantFontFamily.defaultFont
              }}
            >
              {cliksNameList.length > 0 ? "#" + cliksNameList[0] : null}
            </Text>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus")
});

const mapDispatchToProps = dispatch => ({
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  cliksUserId: payload => dispatch(getTrendingCliksProfileDetails(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  React.memo(FeedListContent)
);
