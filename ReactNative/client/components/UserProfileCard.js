import { launchImageLibraryAsync } from "expo-image-picker";
import { askAsync, CAMERA_ROLL } from "expo-permissions";
import { List } from "immutable";
import React, { useEffect, useState } from "react";
import { graphql } from "react-apollo";
import {
  Dimensions,
  Image,
  ImageBackground,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  StyleSheet
} from "react-native";
import { Icon, withBadge } from "react-native-elements";
import Slider from "react-native-slider";
import { connect } from "react-redux";
import { compose } from "recompose";
import { saveUserLoginDaitails, setLoginStatus } from "../actionCreator/UserAction";
import applloClient from "../client";
import ConstantFontFamily from "../constants/FontFamily";
import {
  UserFollowMutation,
  UserUnfollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  UserFollowVariables,
  UserUnfollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { getLocalStorage } from "../library/Helper";
import {
  uploadBannerImageAsync,
  uploadProfileImageAsync
} from "../services/UserService";
import { UserEditVariables } from "../graphqlSchema/graphqlVariables/UserVariables";
import { UserEditMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import { getTrendingUsers } from "../actionCreator/TrendingUsersAction";
import AppHelper from "../constants/AppHelper";
import { Hoverable } from "react-native-web-hooks";
import UserStar from "../components/UserStar";
import ButtonStyle from "../constants/ButtonStyle";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger
} from "react-native-popup-menu";
import firebase from "firebase/app";
import "firebase/auth";
import { setAdminStatus } from "../actionCreator/AdminAction";
import { cos } from "react-native-reanimated";
import ConstantColors from "../constants/Colors";
import { Platform } from "react-native";

const UserProfileCard = props => {
  let [Height, setHeight] = useState(0);
  const user = props.item;
  const [followBtnActive, setfollowBtnActive] = useState(0);
  const [currentUserId, setcurrentUserId] = useState("");

  const [Username, setUsername] = useState(false);
  const [FullName, setFullName] = useState(false);
  const [Desc, setDesc] = useState(false);

  let userFollowers = user.getIn(["user", "followers"]);
  let [username, setusername] = useState(user.getIn(["user", "username"]));
  let [fullname, setfullname] = useState(user.getIn(["user", "full_name"]));
  let [description, setdescription] = useState(
    user.getIn(["user", "description"])
  );
  let [profilepic, setprofilepic] = useState(
    user.getIn(["user", "profile_pic"])
  );
  let [bannerpic, setbannerpic] = useState(user.getIn(["user", "banner_pic"]));
  const [value, setvalue] = useState(50);
  const [brightnessvalue, setbrightnessvalue] = useState(0.5);
  const [showSlider, setshowSlider] = useState(false);
  const [showEditProfile, setshowEditProfile] = useState(false);
  let [MenuHover, setMenuHover] = useState(false);
  let [uploading, setuploading] = useState(false);
  let [updatebannerpic, setupdatebannerpic] = useState("");
  let [updateprofilepic, setupdateprofilepic] = useState("");

  useEffect(() => {
    getLocalStorage("UserId").then(res => {
      setcurrentUserId(res);
    });
    const itemId = props.navigation.getParam("username", "NO-ID");
    // if (itemId == "NO-ID") {
    //   props.navigation.navigate("home");
    // }
    const index = props.getUserFollowUserList.findIndex(
      i =>
        i
          .getIn(["user", "username"])
          .toLowerCase()
          .replace("user:", "") ==
        itemId
          .replace("%3A", ":")
          .toLowerCase()
          .replace("user:", "")
    );
    if (index != -1) {
      if (
        props.getUserFollowUserList.getIn([index, "settings", "follow_type"]) ==
        "FAVORITE"
      ) {
        setfollowBtnActive(2);
        props.icon("#FADB4A");
      } else if (
        props.getUserFollowUserList.getIn([index, "settings", "follow_type"]) ==
        "FOLLOW"
      ) {
        setfollowBtnActive(1);
        props.icon("#E1E1E1");
      }
    } else {
      setfollowBtnActive(0);
      props.icon("#fff");
    }
  });

  function changeBrightness(value) {
    setvalue(value);
    setbrightnessvalue(value / 100);
  }

  async function _askPermission(type, failureMessage) {
    const { status, permissions } = await askAsync(type);
    if (status === "denied") {
      alert(failureMessage);
    }
  }

  async function _pickBannerImage() {
    await _askPermission(
      CAMERA_ROLL,
      "We need the camera-roll permission to read pictures from your phone..."
    );
    let pickerResult = await launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    });
    _handleBannerImagePicked(pickerResult);
  }

  async function _handleBannerImagePicked(pickerResult) {
    let uploadResponse, uploadResult;
    try {
      setuploading(true);
      if (!pickerResult.cancelled) {
        uploadResponse = await uploadBannerImageAsync(pickerResult.uri);
        uploadResult = await uploadResponse.json();
        setbannerpic(pickerResult.uri);
        setupdatebannerpic(uploadResult.id);
      }
    } catch (e) {
      alert("Upload failed, sorry :(" + e + ")");
    } finally {
      setuploading(false);
    }
  }

  async function _pickProfileImage() {
    await _askPermission(
      CAMERA_ROLL,
      "We need the camera-roll permission to read pictures from your phone..."
    );
    let pickerResult = await launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    });
    _handleProfileImagePicked(pickerResult);
  }

  async function _handleProfileImagePicked(pickerResult) {
    let uploadResponse, uploadResult;
    try {
      setuploading(true);
      if (!pickerResult.cancelled) {
        uploadResponse = await uploadProfileImageAsync(pickerResult.uri);
        uploadResult = await uploadResponse.json();
        setprofilepic(pickerResult.uri);
        setupdateprofilepic(uploadResult.id);
      }
    } catch (e) {
      alert("Upload failed, sorry :(" + e + ")");
    } finally {
      setuploading(false);
    }
  }

  function setUpdateProfile() {

    const itemId = props.navigation.getParam("id", "NO-ID");
    // if (itemId == "NO-ID") {
    //   props.navigation.navigate("home");
    // }
    UserEditVariables.variables.username = username;
    UserEditVariables.variables.full_name = fullname;
    UserEditVariables.variables.description = description;
    UserEditVariables.variables.banner_pic = updatebannerpic
      ? updatebannerpic
      : null;
    UserEditVariables.variables.profile_pic = updateprofilepic
      ? updateprofilepic
      : null;
    if (
      UserEditVariables.variables.username !=
      user.getIn(["user"]).toJS().username ||
      UserEditVariables.variables.full_name !=
      user.getIn(["user"]).toJS().full_name ||
      UserEditVariables.variables.description !=
      user.getIn(["user"]).toJS().description
    ) {
      updateProfileApi(UserEditVariables, itemId);
    } else if (
      (UserEditVariables.variables.username ==
        user.getIn(["user"]).toJS().username &&
        UserEditVariables.variables.full_name ==
        user.getIn(["user"]).toJS().full_name &&
        UserEditVariables.variables.description ==
        user.getIn(["user"]).toJS().description &&
        UserEditVariables.variables.profile_pic != null) ||
      UserEditVariables.variables.banner_pic != null
    ) {
      updateProfileApi(UserEditVariables, itemId);
    } else {
      setshowEditProfile(false);
      setshowSlider(false);
    }
  }

  function updateProfileApi(UserEditVariables, itemId) {
    applloClient
      .query({
        query: UserEditMutation,
        ...UserEditVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        props.userId({
          username: UserEditVariables.variables.username,
          type: "feed"
        });

        let resDataLogin = await props.Login();
        await props.saveLoginUser(resDataLogin.data.login);
        await props.getTrendingUsers({
          currentPage: AppHelper.PAGE_LIMIT
        });
        setshowEditProfile(false);
        setshowSlider(false);
      })
      .catch(e => console.log(e));
  }

  function setUpdateData() {
    setshowEditProfile(true);
    setfullname(user.getIn(["user", "full_name"]));
    setusername(user.getIn(["user", "username"]));
    setdescription(user.getIn(["user", "description"]));
    setprofilepic(
      user.getIn(["user", "profile_pic"])
        ? user.getIn(["user", "profile_pic"])
        : null
    );
    setbannerpic(
      user.getIn(["user", "banner_pic"])
        ? user.getIn(["user", "banner_pic"])
        : null
    );
  }

  async function logout() {

    await localStorage.clear();
    await firebase.auth().signOut();

    await props.changeLoginStatus(0);
    await props.changeAdminStatus(false);
    if (
      (await props.navigation.getCurrentRoute().routeName) ==
      "notification" ||
      (await props.navigation.getCurrentRoute().routeName) == "settings"
    ) {
      await props.navigation.navigate("home");
    }
    await props.resetLoginUserDetails();
    await props.resetUserProfileDetails();

    if (Platform.OS == "web") {
      extensionLogout();
    }
    var req = indexedDB.deleteDatabase("firebaseLocalStorageDb");
  };


  function extensionLogout() {
    try {
      window.parent.postMessage({ type: "wecklid_logout" }, "*");
    } catch (err) {
      console.log("Extension Logout Error ", err);
    }
  };

  return (
    <View
      style={[
        Dimensions.get('window').width <= 750 ? ButtonStyle.cardBorderStyle : ButtonStyle.borderStyle,
        ButtonStyle.profileShadowStyle,
        {
          overflow: "hidden",
          borderWidth: 0,
          marginBottom: Dimensions.get("window").width >= 750 ? 10 : 0,
          maxHeight: Dimensions.get("window").height / 2,
          marginHorizontal: Dimensions.get("window").width <= 750 ? 0 : 10,
          marginTop: Dimensions.get("window").width <= 750 ? 0 : 10,
          // Dimensions.get("window").width <= 750 ? 0 :
          // Dimensions.get("window").width >= 750 && Dimensions.get("window").width <= 1200 ? 2 : 10,
          paddingBottom: Dimensions.get("window").width <= 750 ? 10 : 0,
          // backgroundColor: '#fff'
          // width: Dimensions.get("window").width <= 750 ? "97%" : '100%',
          // overflow: "hidden",
          // marginBottom: Dimensions.get("window").width >= 750 && 10,
          // backgroundColor: "#fff",
          // borderWidth: 0,
          // maxHeight: Dimensions.get("window").height / 2 ,
          // marginHorizontal: Dimensions.get("window").width <= 750 ? 0 : 5,
          // marginTop: Dimensions.get("window").width <= 750 ? 0 : 10
        }
      ]}
    >
      {showEditProfile == false ? (
        <View style={{
          paddingVertical: 15,
          paddingRight: 20,
          paddingLeft: 15,
          flexDirection: 'column',
        }}>
          <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',

            // alignItems: 'flex-start',

          }}
          >
            <View style={{ flexDirection: 'row', alignItems: 'flex-start',width:'80%' }}>
              {user.getIn(["user", "profile_pic"]) ? (
                <Image
                  source={{
                    uri: user.getIn(["user", "profile_pic"])
                  }}
                  style={{
                    height: 80,
                    width: 80,
                    padding: 0,
                    margin: 0,
                    borderRadius: 60,
                    borderWidth: 1,
                    borderColor: "#fff"
                  }}
                />
              ) : (
                <Image
                  source={require("../assets/image/default-image.png")}
                  style={{
                    height: 80,
                    width: 80,
                    padding: 0,
                    margin: 0,
                    borderRadius: 60,
                    borderWidth: 1,
                    borderColor: "#fff"
                  }}
                />
              )}

              <Text
                style={{
                  // width: Dimensions.get('window').width<=750 && "30%",
                  justifyContent: 'center',
                  color: "#000",
                  fontSize: 16,
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  flex: 1,
                  flexWrap: 'wrap',
                  // marginTop: 15, 
                  marginLeft: 10
                }}
              >
                @{user.getIn(["user", "username"])}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
              <TouchableOpacity
                onMouseEnter={() => setMenuHover(true)}
                onMouseLeave={() => setMenuHover(false)}
              >
                <Menu>
                  <MenuTrigger>
                    <Image
                      source={require("../assets/image/menu.png")}
                      style={{
                        height: 16,
                        width: 16,
                        // marginTop: 15,
                        marginRight: Dimensions.get('window').width <= 750 ? 10 : 20
                      }}
                    />
                  </MenuTrigger>
                  {props.loginStatus == 1 && user.getIn(["user", "id"]) == currentUserId ?
                    <MenuOptions
                      optionsContainerStyle={{
                        borderRadius: 6,
                        borderWidth: 1,
                        borderColor: "#e1e1e1",
                        shadowColor: "transparent",
                      }}
                      customStyles={{
                        optionsContainer: {
                          minHeight: 50,
                          width: 150,
                          marginTop: 30,
                        }
                      }}
                    >
                      <MenuOption
                        onSelect={() => {
                          return props.navigation.navigate("settings");
                        }}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily: ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Settings
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>

                      <MenuOption onSelect={() => props.navigation.navigate("analytics")}>
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily: ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Analytics
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>
                      <MenuOption
                        onSelect={() => setUpdateData()}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Edit Profile
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>
                      <MenuOption
                        onSelect={() => logout()}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Sign Out
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>
                    </MenuOptions>
                    :
                    props.loginStatus == 1 && user.getIn(["user", "id"]) != currentUserId ?
                      <MenuOptions
                        optionsContainerStyle={{
                          borderRadius: 6,
                          borderWidth: 1,
                          borderColor: "#e1e1e1",
                          shadowColor: "transparent"
                        }}
                        customStyles={{
                          optionsContainer: {
                            minHeight: 50,
                            width: 150,
                            marginTop: 30,
                          }
                        }}
                      >
                        <MenuOption >
                          <Hoverable>
                            {isHovered => (
                              <Text
                                style={{
                                  textAlign: "center",
                                  color: isHovered == true ? "#009B1A" : "#000",
                                  fontFamily: ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                Report
                              </Text>
                            )}
                          </Hoverable>
                        </MenuOption>

                        <MenuOption >
                          <Hoverable>
                            {isHovered => (
                              props.isAdmin &&
                              <Text
                                style={{
                                  textAlign: "center",
                                  color: isHovered == true ? "#009B1A" : "#000",
                                  fontFamily: ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                Delete
                              </Text>
                            )}
                          </Hoverable>
                        </MenuOption>


                      </MenuOptions>
                      :
                      props.loginStatus == 0 && (

                        <MenuOptions
                          optionsContainerStyle={{
                            borderRadius: 6,
                            borderWidth: 1,
                            borderColor: "#e1e1e1",
                            shadowColor: "transparent"
                          }}
                          customStyles={{
                            optionsContainer: {
                              minHeight: 50,
                              width: 150,
                              marginTop: 30,
                            }
                          }}>
                          <MenuOption
                            onSelect={() => {
                              return props.navigation.navigate("settings");
                            }}
                          >
                            <Hoverable>
                              {isHovered => (
                                <Text
                                  style={{
                                    textAlign: "center",
                                    color: isHovered == true ? "#009B1A" : "#000",
                                    fontFamily: ConstantFontFamily.MontserratBoldFont
                                  }}
                                >
                                  Settings
                                </Text>
                              )}
                            </Hoverable>
                          </MenuOption>

                          <MenuOption onSelect={() => props.navigation.navigate("analytics")}>
                            <Hoverable>
                              {isHovered => (
                                <Text
                                  style={{
                                    textAlign: "center",
                                    color: isHovered == true ? "#009B1A" : "#000",
                                    fontFamily: ConstantFontFamily.MontserratBoldFont
                                  }}
                                >
                                  Analytics
                                </Text>
                              )}
                            </Hoverable>
                          </MenuOption>
                        </MenuOptions>
                      )
                  }

                </Menu>
              </TouchableOpacity>

              <UserStar
                UserName={props.item.getIn(["user", "username"])}
                UserId={props.item.getIn(["user", "id"])}
                ContainerStyle={{}}
                ImageStyle={{
                  height: 20,
                  width: 20,
                  alignSelf: "center",
                  marginBottom: 5
                }}
              />
            </View>
          </View>
          <Text
            style={{
              color: "#000",
              fontSize: 13,
              fontFamily: ConstantFontFamily.defaultFont,
              marginTop: Dimensions.get('window').width <= 750 ? 5 : 20,
              textAlign: 'left',
              // paddingHorizontal: 20,
            }}
          >
            {user.getIn(["user", "description"])}
          </Text>
        </View>


      ) : (

        <View style={{ width: Dimensions.get('window').width <= 750 ? '90%' : '95%', marginHorizontal: 20 }}>
          <View style={{
            flexDirection: 'row', justifyContent: 'space-between',
            paddingTop: 20,
            // paddingHorizontal: 20,
            // width: props.ProfileHeight - props.feedY < 0 ? 0 : "100%",
            // height: Dimensions.get("window").height / 4 - props.feedY, alignItems: 'flex-start',
            // marginBottom: 15
          }}>
            <View style={{ flexDirection: 'row', width: '50%', marginRight: 10 }}>
              {/* <View
                onLayout={event => {
                  let { x, y, width, height } = event.nativeEvent.layout;
                  setHeight(height);
                }}
              > */}
              {profilepic ? (
                <Image
                  source={{
                    uri: profilepic
                  }}
                  style={{
                    height: 100,
                    width: 100,
                    padding: 0,
                    margin: 0,
                    borderRadius: 50,
                    borderWidth: 1,
                    borderColor: "#fff"
                  }}
                />
              ) : (
                <Image
                  source={require("../assets/image/default-image.png")}
                  style={{
                    height: 100,
                    width: 100,
                    padding: 0,
                    margin: 0,
                    borderRadius: 50,
                    borderWidth: 1,
                    borderColor: "#fff"
                  }}
                />
              )}

              <Icon
                color={"#000"}
                iconStyle={{
                  color: "#fff",
                  justifyContent: "center",
                  alignItems: "center",
                  alignSelf: 'center'
                }}
                reverse
                name="camera"
                type="font-awesome"
                size={16}
                containerStyle={{
                  alignSelf: "center",
                  position: "absolute",
                  paddingHorizontal: 25,
                  paddingBottom: 10
                }}
                onPress={() => _pickProfileImage()}
              />
              {/* </View> */}
              <View
                style={{
                  flexDirection: "column",
                  paddingHorizontal: 10,
                  width: '100%'
                }}
              >
                <TextInput
                  value={fullname}
                  placeholder="Full Name (Optional)"
                  underlineColorAndroid="transparent"
                  style={[FullName ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle, 
                    {
                    width: "100%",
                    height: 40,
                    borderWidth: 0,
                    borderRadius: 5,
                    color: "#000",
                    fontSize: 16,
                    fontWeight: "bold",
                    // backgroundColor: "#000",
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    paddingLeft: 10

                  }]}
                  onChangeText={value => setfullname(value)}
                  onFocus={() => setFullName(true)}
                  onBlur={() => setFullName(false)}
                />
                <View style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop: 10,
                  width: '100%'
                }}>
                  <Text style={{
                    color: "#000",
                    fontSize: 18,
                    fontWeight: "bold",
                    marginRight: '2%',
                    width: Dimensions.get('window').width <= 750 ? '20%' : '10%',
                    fontFamily: ConstantFontFamily.defaultFont,
                  }}> @</Text>
                  <TextInput
                    value={username}
                    underlineColorAndroid="transparent"
                    style={[Username ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle, {
                      width: Dimensions.get('window').width <= 750 ? "80%" : '90%',
                      // marginTop: 15,
                      height: 40,

                      borderWidth: 0,
                      borderRadius: 5,
                      color: "#000",
                      fontSize: 12,
                      fontWeight: "bold",
                      // backgroundColor: "#000",
                      fontFamily: ConstantFontFamily.defaultFont,
                      paddingLeft: 10
                    }]}
                    onChangeText={value => setusername(value)}
                    onFocus={() => setUsername(true)}
                    onBlur={() => setUsername(false)}
                  />
                </View>
              </View>

            </View>
            <View style={{ flexDirection: 'row', width: Dimensions.get('window').width <= 750 ? '18%' : '45%', justifyContent: 'flex-end' }}>
              <Icon
                color={"#000"}
                iconStyle={{
                  color: "#000",
                  // justifyContent: "center",
                  // alignItems: "center",
                  marginRight: 10
                }}
                // reverse
                name="save"
                type="ion-icons"
                size={30}
                onPress={() => {
                  setUpdateProfile();
                }}
              />
              <Icon
                color={"#000"}
                iconStyle={{
                  color: "#000",
                  // justifyContent: "center",
                  // alignItems: "center"
                }}
                // reverse
                name="close"
                type="ion-icons"
                size={30}
                onPress={() => setshowEditProfile(false)}
              />
            </View>
          </View>
          <TextInput
            multiline={true}
            numberOfLines={2}
            value={description}
            style={[Desc ? ButtonStyle.selecttextAreaShadowStyle : ButtonStyle.textAreaShadowStyle, {
              // marginHorizontal: Dimensions.get('window').width<=750 ? 0 : 20,
              marginVertical: 10,
              borderWidth: 0,
              borderRadius: 5,
              color: "#000",
              fontSize: 13,
              fontWeight: "bold",
              fontFamily: ConstantFontFamily.defaultFont,
              paddingTop: 10,
              paddingHorizontal: 10,
              width: '100%',
              height: 65
            }]}
            onChangeText={value => setdescription(value)}
            onFocus={() => setDesc(true)}
            onBlur={() => setDesc(false)}
          />
        </View>
      )
      }
      {showSlider && (
        <View style={{ alignItems: "stretch", justifyContent: "center" }}>
          <Slider
            value={value}
            maximumValue={100}
            minimumValue={0}
            onValueChange={value => changeBrightness(value)}
          />
          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <Text style={{ color: "#000", fontFamily: ConstantFontFamily.defaultFont }}>
              Slide to change the brightness
            </Text>
          </View>
        </View>
      )}
    </View>
  );
};

const mapStateToProps = state => ({
  getUserFollowUserList: state.LoginUserDetailsReducer.get("userFollowUserList")
    ? state.LoginUserDetailsReducer.get("userFollowUserList")
    : List(),
  loginStatus: state.UserReducer.get("loginStatus"),
  isAdmin: state.AdminReducer.get("isAdmin"),
});

const mapDispatchToProps = dispatch => ({
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  getTrendingUsers: payload => dispatch(getTrendingUsers(payload)),
  changeLoginStatus: payload => dispatch(setLoginStatus(payload)),
  changeAdminStatus: payload => dispatch(setAdminStatus(payload)),
  resetLoginUserDetails: payload =>
    dispatch({ type: "LOGIN_USER_DETAILS_RESET", payload }),
  resetUserProfileDetails: payload =>
    dispatch({ type: "USER_PROFILE_DETAILS_RESET", payload }),
});

const UserProfileCardContainerWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(UserProfileCard);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  UserProfileCardContainerWrapper
);
const styles = StyleSheet.create({
  iconsStyle: {
    backgroundColor: '#000',
    borderRadius: 20,
    height: 35,
    width: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10
  }
})