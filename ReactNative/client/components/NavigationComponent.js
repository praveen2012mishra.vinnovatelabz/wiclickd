import React, { useEffect, useState } from "react";
import {
  Dimensions,
  View,
  Platform,
  TouchableOpacity,
  Image,
  Text,
  Animated,
  StyleSheet,
  YellowBox,
  LogBox,
  AsyncStorage
} from "react-native";
import { MenuProvider } from "react-native-popup-menu";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setCURRENTDEVICEWIDTHACTION } from "../actionCreator/CurrentDeviceWidthAction";
import { setFEEDREPORTMODALACTION } from "../actionCreator/FeedReportModalAction";
import { resetHomefeedList } from "../actionCreator/HomeFeedAction";
import { setInviteSIGNUPMODALACTION } from "../actionCreator/InviteSignUpModalAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setRESETPASSWORDMODALACTION } from "../actionCreator/ResetPasswordModalAction";
import { setSHARELINKMODALACTION } from "../actionCreator/ShareLinkModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { setLoginStatus } from "../actionCreator/UserAction";
import { setUSERNAMEMODALACTION } from "../actionCreator/UsernameModalAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import { setVERIFYEMAILMODALACTION } from "../actionCreator/VerifyEmailModalAction";
import BottomFloatingButton from "../components/BottomFloatingButton";
import LeftPanel from "../components/LeftPanel";
import NavigationService from "../library/NavigationService";
import AppNavigator from "../Navigation/AppNavigator";
import SidePanel from "./SidePanel";
import { Icon } from "react-native-elements";
import HeaderRight from "../components/HeaderRight";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RNPickerSelect from "react-native-picker-select";
import { setAdminView } from "../actionCreator/AdminAction";
import ConstantFontFamily from "../constants/FontFamily";
import SearchInputWeb from "../components/SearchInputWeb";
import BottomScreen from "../components/BottomScreen";
import ConstantColors from '../constants/Colors'

const USER_VIEW = "USER_VIEW";
const ADMIN_VIEW = "ADMIN_VIEW";
const roleWiseViewOption = [
  {
    label: Dimensions.get("window").width > 750 ? "User View" : "User",
    value: USER_VIEW,
    key: 0
  },
  {
    label: Dimensions.get("window").width > 750 ? "Admin View" : "Admin",
    value: ADMIN_VIEW,
    key: 1
  }
];

console.ignoredYellowBox = ["Warning: Each", "Warning: Failed"];
console.disableYellowBox = true;
console.ignoredYellowBox = ["Warning: Each", "Warning: Failed"];
YellowBox.ignoreWarnings(["Warning: ReactNative.createElement"]);
console.ignoredYellowBox = ["Warning: ReactNative.createElement"];
console.error;
YellowBox.ignoreWarnings([
  "Warning: componentWillMount is deprecated",
  "Warning: componentWillReceiveProps is deprecated",
  "Warning: componentWillReceiveProps has been renamed, and is not recommended for use."
]);

//let resizeWidth=500;

class NavigationComponent extends React.Component {
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.state = {
      url: ""
    };
  }

  GetResizeStatus = () => {
    AsyncStorage.getItem("getResizeWidth", (error, result) => {
      let getSession = result;
      if (Math.abs(getSession - window.outerWidth) > 50) {
        window.location.reload(true);
      }
    });
    // if (
    //   Math.abs(sessionStorage.getItem("getResizeWidth") - window.outerWidth) >
    //   50
    // ) {
    //   window.location.reload(true);
    // }
    //sessionStorage.setItem("getResizeWidth", window.outerWidth);
    AsyncStorage.setItem("getResizeWidth", window.outerWidth);
  };

  componentDidMount = async () => {
    this.setState({ url: window.location.href });
    YellowBox.ignoreWarnings(["Warning: ReactNative.createElement"]);
    console.ignoredYellowBox = ["Warning: ReactNative.createElement"];
  };

  render() {
    console.ignoredYellowBox = ["Warning: Each", "Warning: Failed"];
    console.disableYellowBox = true;
    console.ignoredYellowBox = ["Warning: Each", "Warning: Failed"];
    YellowBox.ignoreWarnings(["Warning: ReactNative.createElement"]);
    console.ignoredYellowBox = ["Warning: ReactNative.createElement"];
    console.error;
    YellowBox.ignoreWarnings([
      "Warning: componentWillMount is deprecated",
      "Warning: componentWillReceiveProps is deprecated",
      "Warning: componentWillReceiveProps has been renamed, and is not recommended for use."
    ]);
    return (
      <View
        style={{
          height: "100%",
          backgroundColor: ConstantColors.customeBackgroundColor
          // Dimensions.get("window").width >= 750 ? "#f4f4f4" : "#f4f4f4"
        }}
        onResize={this.GetResizeStatus()}
      >
        <MenuProvider>
          {Dimensions.get("window").width >= 750 &&
            /* <View
            // style={{
            //   alignItems: "center",
            //   width: "100%",
            //   justifyContent: "center",
            //   flexDirection: "row",
            //   backgroundColor: "#000",
            //   height: 50,
            //   //paddingRight: Dimensions.get("window").width >= 1200 ? 25 : null,
            //   paddingHorizontal: Dimensions.get("window").width >= 1200 ? wp('3.95%') : null,
            //   display: Dimensions.get("window").width > 750 ? "flex" : "none"
            // }}
            > */
            <View
              style={{
                width: "100%",
                flexDirection: "row",
                height: 50,
                backgroundColor: "#000",
                justifyContent: Dimensions.get("window").width >= 1100 ? "center" : 'space-between',
                alignItems: 'center',
              }}
            >
              {/* <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignSelf: "center",                  
                  // position: "relative",
                  // zIndex: 20
                }}
              > */}
              {/* {Dimensions.get("window").width <= 1200 && (
                  <Icon
                    color={"#fff"}
                    iconStyle={{ paddingLeft: 15 }}
                    onPress={() => NavigationService.toggleDrawer()}
                    name="bars"
                    type="font-awesome"
                    size={35}
                  />
                )} */}
              <TouchableOpacity
                testID="HomeLogo"
                onPress={() => {
                  NavigationService.navigate("home"),
                    window.location.reload();
                }}
                style={{
                  // width: Dimensions.get("window").width >= 1200 ? 150 : "30%",
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  display:
                    Dimensions.get("window").width >= 750 &&
                      Platform.OS == "web"
                      ? "flex"
                      : "none",
                  alignItems: "center",
                  width: Dimensions.get("window").width <= 750 ? '96%' :
                    Dimensions.get("window").width >= 750 && Dimensions.get("window").width <= 1100 ? 450 : 320,
                  paddingLeft: 10
                  // marginHorizontal: Dimensions.get("window").width <= 750 ? '2%' : 0,
                }}
              >
                <Image
                  source={require("../assets/image/logo.png")}
                  style={{
                    justifyContent: "flex-start",
                    // marginLeft: 10,
                    height: 30,
                    width: 30,
                    marginRight: 5
                  }}
                  resizeMode={"contain"}
                />
                {!this.props.getsearchBarStatus &&
                  <Text
                    style={{
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      fontWeight: "bold",
                      fontSize: 20,
                      textAlign: "center",
                      color: "white"
                    }}
                  >
                    weclikd
                  </Text>
                }
              </TouchableOpacity>
              {/* {this.props.isAdmin && Dimensions.get("window").width >= 1200 && (
                  <View
                    style={{
                      alignItems: "center",
                      width: 145
                    }}
                  >
                    <RNPickerSelect
                      value={this.props.isAdminView ? ADMIN_VIEW : USER_VIEW}
                      placeholder={{}}
                      items={roleWiseViewOption}
                      onValueChange={(itemValue, itemIndex) => {
                        this.props.changeAdminView(itemValue === ADMIN_VIEW);
                      }}
                      onUpArrow={() => {
                        this.inputRefs.name.focus();
                      }}
                      onDownArrow={() => {
                        this.inputRefs.picker2.togglePicker();
                      }}
                      style={{
                        ...styles,
                        iconContainer: {
                          top: 10,
                          right: 10
                        }
                      }}
                      ref={el => {
                        this.inputRefs.picker = el;
                      }}
                    />
                  </View>
                )} */}
              {/* </View> */}
              <View
                style={{
                  flexDirection: 'row',
                  width:
                    Dimensions.get("window").width >= 1100
                      ? 950 : 440,
                  justifyContent: "center",
                  marginRight: Dimensions.get("window").width >= 1100 ? 0 : 20
                }}
              >
                {Dimensions.get("window").width >= 1100 &&
                  <View style={{ width: 450, alignSelf: 'center', marginRight: 10 }}>

                    <SearchInputWeb
                      navigation={this.props.navigation}
                      refs={ref => {
                        this.input = ref;
                      }}
                      key={Math.random()}
                      displayType={"web"}
                    />
                  </View>}
                <View style={{ width: 450 }}
                  style={{
                    // flex: 1,
                    flexDirection: "row",
                    justifyContent: "flex-end",
                    width: 450,
                  }}
                >
                  <HeaderRight navigation={NavigationService} />
                </View>
              </View>
            </View>
            /* </View> */
          }

          <View
            style={{
              width: "100%",
              flex: 1,
              flexDirection: "row",
              justifyContent: "center"
            }}
          >
            {Dimensions.get("window").width >= 1200 && (
              <LeftPanel
                ref={navigatorRef => {
                  NavigationService.setTopLevelNavigator(navigatorRef);
                }}
                navigation={NavigationService}
              />
            )}

            <View
              style={{
                backgroundColor: ConstantColors.customeBackgroundColor,
                // Dimensions.get("window").width >= 750 ? "#f4f4f4" : "#f4f4f4",
                width:
                  // 850,
                  Dimensions.get("window").width >= 1200
                    ? 950
                    // : Dimensions.get("window").width >= 750
                    // ? 850
                    // : 
                    :
                    "100%",
                paddingLeft: Dimensions.get("window").width >= 1200 ? 10 : 0,
                justifyContent: "center"
              }}
            >
              <AppNavigator
                ref={navigatorRef => {
                  NavigationService.setTopLevelNavigator(navigatorRef);
                }}
              />
              {/* {// this.props.getUsernameModalStatus == false &&
              Dimensions.get("window").width >= 750 && (
                <View style={{ marginRight: 50, marginBottom: 20 }}>
                  <BottomFloatingButton
                    navigation={this.props.navigation}
                    modalStataus={() => {}}
                  />
                </View>
              )}     */}
              {/* {Dimensions.get("window").width <= 750  &&
                // this.props.loginStatus == 1 && 
                (
                  <BottomScreen key={Math.random()} navigation={NavigationService} />
                )
              } */}
            </View>
          </View>
        </MenuProvider>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getLoginModalStatus: state.LoginModalReducer.get("modalStatus"),
  getSignUpModalStatus: state.SignUpModalReducer.get("modalStatus"),
  getInviteSignUpModalStatus: state.InviteSignUpModalReducer.get("modalStatus"),
  getUsernameModalStatus: state.UsernameModalReducer.get("modalStatus"),
  getResetPasswordModalStatus: state.ResetPasswordModalReducer.get(
    "modalStatus"
  ),
  getVerifyEmailModalStatus: state.VerifyEmailModalReducer.get("modalStatus"),
  getSignupFollowModalStatus: state.VerifyEmailModalReducer.get(
    "signupfollowmodal"
  ),
  getSignupJoinModalStatus: state.VerifyEmailModalReducer.get(
    "signupjoinmodal"
  ),
  getShareLinkModalStatus: state.ShareLinkModalReducer.get("modalStatus"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getFeedReportModalStatus: state.FeedReportModalReducer.get("modalStatus"),
  getScreenLoadingStatus: state.ScreenLoadingReducer.get("modalStatus"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  searchBarStatus: state.AdminReducer.get("searchBarStatus"),
  getsearchBarStatus: state.AdminReducer.get("searchBarOpenStatus"),
});

const mapDispatchToProps = dispatch => ({
  __setCURRENTDEVICEWIDTHACTION: payload =>
    dispatch(setCURRENTDEVICEWIDTHACTION(payload)),
  __resetHomefeedList: payload => dispatch(resetHomefeedList(payload)),
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setUsernameModalStatus: payload => dispatch(setUSERNAMEMODALACTION(payload)),
  setInviteSignUpModalStatus: payload =>
    dispatch(setInviteSIGNUPMODALACTION(payload)),
  setResetPasswordModalStatus: payload =>
    dispatch(setRESETPASSWORDMODALACTION(payload)),
  setVerifyEmailModalStatus: payload =>
    dispatch(setVERIFYEMAILMODALACTION(payload)),
  setShareLinkModalStatus: payload =>
    dispatch(setSHARELINKMODALACTION(payload)),
  setFeedReportModalStatus: payload =>
    dispatch(setFEEDREPORTMODALACTION(payload)),
  changeLoginStatus: payload => dispatch(setLoginStatus(payload)),
  changeAdminView: payload => dispatch(setAdminView(payload)),
  setTerm: payload => dispatch({ type: "SET_TERM", payload }),
  setTermWeb: payload => dispatch({ type: "SET_TERM_WEB", payload })
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  NavigationComponent
);

const styles = StyleSheet.create({
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
    fontSize: 16,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  inputAndroid: {
    width: 145,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#fff",
    borderRadius: 8,
    color: "#000",
    backgroundColor: "white",
    fontSize: 16,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  }
});
