import { logInAsync } from "expo-google-app-auth";
import firebase from "firebase/app";
import "firebase/auth";
import jwt_decode from "jwt-decode";
import React, { Component } from "react";
import { graphql } from "react-apollo";
import {
  AsyncStorage,
  Dimensions,
  Image,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Button, Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { setAdminStatus } from "../actionCreator/AdminAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setInviteSIGNUPMODALACTION } from "../actionCreator/InviteSignUpModalAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setRESETPASSWORDMODALACTION } from "../actionCreator/ResetPasswordModalAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingExternalFeeds } from "../actionCreator/TrendingExternalFeedsAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import { getTrendingUsers } from "../actionCreator/TrendingUsersAction";
import {
  saveUserLoginDaitails,
  setLoginStatus
} from "../actionCreator/UserAction";
import { setUserApproachAction } from "../actionCreator/UserApproachAction";
import { setUSERNAMEMODALACTION } from "../actionCreator/UsernameModalAction";
import { setVERIFYEMAILMODALACTION } from "../actionCreator/VerifyEmailModalAction";
import applloClient from "../client";
import "../components/Firebase";
import LoaderComponent from "../components/LoaderComponent";
import AppHelper from "../constants/AppHelper";
import ConstantFontFamily from "../constants/FontFamily";
import getEnvVars from "../environment";
import {
  AdminHomeFeedMutation,
  HomeFeedMutation
} from "../graphqlSchema/graphqlMutation/FeedMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { setLocalStorage } from "../library/Helper";
import NavigationService from "../library/NavigationService";
import ButtonStyle from "../constants/ButtonStyle";
import { compose } from "recompose";

const apiUrl = getEnvVars();

class LoginModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UserEmail: "",
      UserPassword: "",
      loading: false,
      rememberMe: false,
      rememberMeData: null,
      access_key: "",
      buttonName: "Login",
      errorMsg: ""
    };
    this._isMounted = false;
  }

  async componentDidMount() {
    this._isMounted = true;
    let __self = this;
    let res = JSON.parse(await AsyncStorage.getItem("rememberMe"));
    if (res) {
      __self.setState({
        rememberMe: true,
        UserEmail: res.UserEmail,
        UserPassword: res.UserPassword
      });
    } else {
      __self.setState({
        rememberMe: false
      });
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  userResetModal = async () => {
    this.props.setLoginModalStatus(false);
    this.props.setResetPasswordModalStatus(true);
  };

  onModalClose = async () => {
    // await this.props.setUserApproachAction({ type: "" });
    this.setState({ buttonName: "Logged In!" });
    await this.props.setLoginButtonText("Logged In!");
    setTimeout(() => {
      this.props.setLoginModalStatus(false);
      this.props.setLoginButtonText("Login");
    }, 500);
  };

  onClose = async () => {
    this.props.setLoginModalStatus(false);
    this.props.setLoginButtonText("Login");
  };

  getTrendingFeedList = async () => {
    applloClient
      .query({
        query:
          this.props.isAdmin == true ? AdminHomeFeedMutation : HomeFeedMutation,
        variables: {
          first: 24,
          after: null,
          sort: "TRENDING"
        },
        fetchPolicy: "no-cache"
      })
      .then(response => {
        this.props.setTrendingHomeFeed(response.data.home_feed.posts.edges);
      })
      .catch(e => {
        console.log(e.message);
        this.props.setTrendingHomeFeed([]);
      });
  };

  getNewFeedList = async () => {
    applloClient
      .query({
        query:
          this.props.isAdmin == true ? AdminHomeFeedMutation : HomeFeedMutation,
        variables: {
          first: 24,
          after: null,
          sort: "NEW"
        },
        fetchPolicy: "no-cache"
      })
      .then(response => {
        this.props.setNewHomeFeed(response.data.home_feed.posts.edges);
      })
      .catch(e => {
        console.log(e);
        this.props.setNewHomeFeed([]);
      });
  };

  getDiscussedFeedList = () => {
    applloClient
      .query({
        query:
          this.props.isAdmin == true ? AdminHomeFeedMutation : HomeFeedMutation,
        variables: {
          first: 24,
          after: null,
          sort: "DISCUSSING"
        },
        fetchPolicy: "no-cache"
      })
      .then(response => {
        this.props.setDiscussionHomeFeed(response.data.home_feed.posts.edges);
      })
      .catch(e => {
        console.log(e);
        this.props.setDiscussionHomeFeed([]);
      });
  };

  userLogin = async () => {
    let __self = this;
    this.setState({
      // loading: true,
      buttonName: "Logging in..."
    });
    await this.props.setLoginButtonText("Logging in...");
    //  await this.props.setUserApproachAction({ type: "login" });
    const { UserEmail, UserPassword } = this.state;
    await firebase
      .auth()
      .signInWithEmailAndPassword(UserEmail, UserPassword)
      .then(async res => {
        //if (res.user.emailVerified == true) {
        return res.user.getIdToken(true).then(async function (idToken) {
          await setLocalStorage(
            "admin",
            jwt_decode(idToken).admin ? "true" : "false"
          );
          await __self.props.changeAdminStatus(
            jwt_decode(idToken).admin ? jwt_decode(idToken).admin : false
          );
          if (Platform.OS == "web") {
            __self.setUserNameInExtension = __self.setLoginTokenInExtension(
              idToken
            );
          }
          return await setLocalStorage("userIdTokenFirebase", idToken);
        });
        // }
        // else {
        //   await this.props.setLoginModalStatus(false);
        //   await this.props.setVerifyEmailModalStatus(true);
        //   await this.props.setUsernameModalStatus(true);
        //   // NavigationService.navigate("follow");
        //   return false;
        // }
      })
      .then(async res => {
        if (res) {
          let loginData = await __self.props.Login();
          if (loginData.data.login.status.status == "NOT_FOUND") {
            __self.setState({
              // loading: false
            });
            AsyncStorage.setItem("skipCredentials", "true");
            this.props.setLoginButtonText("Login");
            // this.handleSignButton();
          } else {
            await this.props.saveLoginUser(loginData.data.login);
            await this.props.changeLoginStatus(1);
            await this.props.setAnonymousUser({ "value": false, "token": "" }),
              // await __self.getTrendingFeedList();
              // await __self.getNewFeedList();
              // await __self.getDiscussedFeedList();
              // await __self.props.getHomefeed({
              //   currentPage: AppHelper.PAGE_LIMIT
              // });
              await __self.props.getTrendingUsers({
                currentPage: AppHelper.PAGE_LIMIT
              });
            await __self.props.getTrendingClicks({
              currentPage: AppHelper.PAGE_LIMIT
            });
            await __self.props.getTrendingTopics({
              currentPage: AppHelper.PAGE_LIMIT
            });
            await __self.props.getTrendingExternalFeeds({
              currentPage: AppHelper.PAGE_LIMIT
            });
            await AsyncStorage.setItem(
              "userLoginId",
              loginData.data.login.account.id
            );
            await AsyncStorage.setItem(
              "MyUserUserId",
              loginData.data.login.account.my_users[0].id
            );
            await AsyncStorage.setItem(
              "userIdTokenWeclikd",
              __self.state.access_key
            );
            await AsyncStorage.setItem(
              "UserId",
              loginData.data.login.account.my_users[0].user.id
            );
            await AsyncStorage.setItem(
              "UserName",
              loginData.data.login.account.my_users[0].user.username
            );
            // set user name to extension
            if (Platform.OS == "web") {
              await __self.setUserNameInExtension(
                loginData.data.login.account.my_users[0].user.username
              );
            }
            await this.props.leftPanelModalFunc(false)
            await __self.onModalClose();

            // let users_followed =
            //   loginData.data.login.account.my_users[0].users_followed;
            // let cliks_followed =
            //   loginData.data.login.account.my_users[0].cliks_followed;
            // let topics_followed =
            //   loginData.data.login.account.my_users[0].topics_followed;
            // let feeds_followed =
            //   loginData.data.login.account.my_users[0].feeds_followed;
            // if (
            //   users_followed.length == 0 &&
            //   cliks_followed.length == 0 &&
            //   topics_followed.length == 0 &&
            //   feeds_followed.length == 0
            // ) {
            //   this.props.SignUpFollowModalStatus(true);
            //   this.props.setUsernameModalStatus(true);
            //   // NavigationService.navigate("follow");
            // }
          }
        }
      })

      .catch(error => {
        __self.setState({
          loading: false
        });
        console.log(error);
        if (error.code == "auth/wrong-password") {
          this.setState({
            errorMsg: "Invalid email or password",
            buttonName: "Login"
          });
          this.props.setLoginButtonText("Login");
        } else {
          // this.handleSignButton();
          this.setState({ errorMsg: "User not found", buttonName: "Login" });
          this.props.setLoginButtonText("Login");
          //this.props.setUserApproachAction({ type: "" });
        }
        return false;
      });
  };

  loginWithGoogle = async () => {
    if (Platform.OS == "web") {
      await this.loginWithGoogleWeb();
    } else {
      await this.props.leftPanelModalFunc(false)
      await this.loginWithGoogleMobile();
    }
  };

  loginWithGoogleWeb = async () => {
    this.setState({
      buttonName: "Logging in...",
      UserEmail: "",
      UserPassword: ""
    });
    await this.props.setLoginButtonText("Logging in...");
    await this.props.setGoogleLogin(true);
    await this.props.setUserApproachAction({ type: "login" });
    await this.props.setAnonymousUser({ "value": false, "token": "" });
    const provider = new firebase.auth.GoogleAuthProvider();
    await firebase.auth().signInWithRedirect(provider);
  };

  loginWithGoogleMobile = async () => {
    try {
      const result = await logInAsync({
        androidClientId: apiUrl.ANDROIDCLIENT_ID,
        iosClientId: apiUrl.IOSCLIENT_ID,
        scopes: ["profile", "email"]
      });
      if (result.type === "success") {
        await this.props.setAnonymousUser({ "value": false, "token": "" }),
          await this.onSignIn(result);
      } else {
        return { cancelled: true };
      }
    } catch (e) {
      return { error: true };
    }
  };

  onSignIn = async googleUser => {
    let __self = this;
    __self.onClose();
    let unsubscribe = await firebase
      .auth()
      .onAuthStateChanged(async function (firebaseUser) {
        var credential = firebase.auth.GoogleAuthProvider.credential(
          googleUser.idToken,
          googleUser.accessToken
        );
        await firebase
          .auth()
          .signInWithCredential(credential)
          .then(async function (result) {
            if ((await result.additionalUserInfo.isNewUser) == true) {
              try {
                return await __self.props.Login();
              } catch (e) {
                __self.props.setUsernameModalStatus(true);
              }
            } else {
              await firebase
                .auth()
                .currentUser.getIdToken(true)
                .then(async function (idToken) {
                  await AsyncStorage.setItem("userIdTokenFirebase", idToken);
                  await setLocalStorage(
                    "admin",
                    jwt_decode(idToken).admin ? "true" : "false"
                  );
                  await __self.props.changeAdminStatus(
                    jwt_decode(idToken).admin
                      ? jwt_decode(idToken).admin
                      : false
                  );
                })
                .then(async () => {
                  return await __self.props.Login();
                })
                .then(async res => {
                  __self.setState({
                    loading: false
                  });
                  await AsyncStorage.setItem(
                    "userLoginId",
                    res.data.login.account.id
                  );
                  await AsyncStorage.setItem(
                    "MyUserUserId",
                    res.data.login.account.my_users[0].id
                  );
                  await AsyncStorage.setItem(
                    "userIdTokenWeclikd",
                    __self.state.access_key
                  );
                  await AsyncStorage.setItem(
                    "UserId",
                    res.data.login.account.my_users[0].user.id
                  );
                  await __self.props.saveLoginUser(res.data.login);
                  await __self.props.changeLoginStatus(1);
                  __self.onClose();
                  NavigationService.navigate("home");
                })
                .catch(error => {
                  console.log(error.message);
                  // alert("Invalid email or password");
                  this.setState({ errorMsg: "Invalid email or password" });
                  return false;
                });
            }
          });
      });
    unsubscribe();
  };

  enterPressed = e => {
    var code = e.keyCode || e.which;
    if (code === 13) {
      this.userLogin();
    }
  };

  handleSignButton = async () => {
    await this.props.setLoginModalStatus(false);
    await this.props.setInviteUserDetail({
      clikName: "",
      inviteKey: "",
      userName: ""
    });
    await this.props.setUsernameModalStatus(true);
    // NavigationService.navigate("username");
  };

  //--------------------------- Set Firebase Token in Extension -----------------------------
  setLoginTokenInExtension = idToken => UserName => {
    try {
      window.parent.postMessage(
        { type: "wecklid_login", userIdTokenFirebase: idToken, UserName },
        "*"
      );
    } catch (e) {
      console.log("extension login Error ", e);
    }
  };
  //------------------------------------------------------------------------------------------

  anonymousUserLogin = async () => {
    let __self = this;
    this.setState({
      buttonName: "Logging in..."
    });
    await this.props.setLoginButtonText("Logging in...");
    await setLocalStorage("userIdTokenFirebase", this.props.getAnonymousToken);
    try {
      let loginData = await __self.props.Login();
      if (loginData.data.login.status.status == "NOT_FOUND") {
        __self.setState({
          // loading: false
        });
        AsyncStorage.setItem("skipCredentials", "true");
        this.props.setLoginButtonText("Login");
        // this.handleSignButton();
      } else {
        await this.props.saveLoginUser(loginData.data.login);
        await this.props.changeLoginStatus(1);
        await this.props.setAnonymousUser({ "value": true, "token": this.props.getAnonymousToken }),
          await __self.props.getTrendingUsers({
            currentPage: AppHelper.PAGE_LIMIT
          });
        await __self.props.getTrendingClicks({
          currentPage: AppHelper.PAGE_LIMIT
        });
        await __self.props.getTrendingTopics({
          currentPage: AppHelper.PAGE_LIMIT
        });
        await __self.props.getTrendingExternalFeeds({
          currentPage: AppHelper.PAGE_LIMIT
        });
        await AsyncStorage.setItem(
          "userLoginId",
          loginData.data.login.account.id
        );
        await AsyncStorage.setItem(
          "MyUserUserId",
          loginData.data.login.account.my_users[0].id
        );
        await AsyncStorage.setItem(
          "userIdTokenWeclikd",
          __self.state.access_key
        );
        await AsyncStorage.setItem(
          "UserId",
          loginData.data.login.account.my_users[0].user.id
        );
        await AsyncStorage.setItem(
          "UserName",
          loginData.data.login.account.my_users[0].user.username
        );
        // set user name to extension
        // if (Platform.OS == "web") {
        //   await __self.setUserNameInExtension(
        //     loginData.data.login.account.my_users[0].user.username
        //   );
        // }
        await this.props.leftPanelModalFunc(false)
        await __self.onModalClose();
      }
    } catch (error) {
      __self.setState({
        loading: false
      });
      console.log(error);
      this.props.setAnonymousUser({ "value": false, "token": "" });
      if (error.code == "auth/wrong-password") {
        this.setState({
          errorMsg: "Invalid email or password",
          buttonName: "Login"
        });
        this.props.setLoginButtonText("Login");
      } else {
        this.setState({ errorMsg: "User not found", buttonName: "Login" });
        this.props.setLoginButtonText("Login");
      }
      return false;
    }
  }
  render() {
    return (
      <View
        style={{
          backgroundColor: "#f4f4f4",
          borderColor: "#c5c5c5",
          borderRadius: 6,
          // maxWidth: 450,
          width: Dimensions.get('window').width >= 750 ? 450 :'100%',
          height: Platform.OS != 'web' ? 500 : 450,
                    
          // alignSelf:'center'
        }}
      >
        <View >
          <Hoverable>
            {isHovered => (
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  flex: 1,
                  position: "absolute",
                  zIndex: 999999,
                  left: 0,
                  top: 0
                }}
                onPress={this.onClose}
              >
                <Icon
                  color={isHovered == true ? "rgba(256,256,256,0.4)" : "#000"}
                  iconStyle={{
                    color: "#fff",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  reverse
                  name="close"
                  type="antdesign"
                  size={16}
                />
              </TouchableOpacity>
            )}
          </Hoverable>

          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              backgroundColor: "#000",
              alignItems: "center",
              height: 50,
              borderTopLeftRadius: 6,
              borderTopRightRadius: 6,
              width: "100%"
            }}
          >
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "row",
              }}
            >
              <Image
                source={require("../assets/image/logo.png")}
                style={{
                  height: 30,
                  width: 30,
                  marginRight: 5
                }}
                resizeMode={"contain"}
              />
              <Text
                style={{
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  fontWeight: "bold",
                  fontSize: 22,
                  textAlign: "center",
                  color: "white"
                }}
              >
                weclikd
                  </Text>
            </View>
          </View>
        </View>

        <View
          style={{
            alignItems: "center",
            flex: 1,
            paddingVertical: 20,
            backgroundColor: "#fff",
            borderBottomLeftRadius: 6,
            borderBottomRightRadius: 6,
            borderColor: "#c5c5c5"
          }}
        >
          <ScrollView
            style={{
              backgroundColor: "#fff",
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
              width: '90%'
            }}
          >
            {/* // source={
                //   Platform.OS == "web" &&
                //     this.props.getCurrentDeviceWidthAction > 750
                //     ? require("../assets/image/weclickd-logo.png")
                //     : Platform.OS == "web"
                //       ? require("../assets/image/weclickd-logo.png")
                //       : require("../assets/image/weclickd-logo-only-icon.png")
                // } */}
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "row",
                marginTop: 10
              }}
            >
              {/* <Image
                source={require("../assets/image/logo.png")}
                style={{
                  height: 50,
                  width: 50
                }}
                resizeMode={"contain"}
              />
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 30,
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
              >
                weclikd
              </Text> */}
            </View>
            <View style={{ flex: 1 }}>
              <View
                style={{
                  ...Platform.select({
                    web: {
                      // marginHorizontal: Dimensions.get("window").width / 50,
                      marginTop: 15
                    }
                  })
                }}
              >
                {this.props.getAnonymousLogin &&
                  <Button
                    title="Login Anonymously"
                    titleStyle={[ButtonStyle.titleStyle, { fontSize: 16 }]}
                    buttonStyle={[ButtonStyle.backgroundStyle, { borderWidth: 2, borderRadius: 10 }]}
                    containerStyle={[ButtonStyle.containerStyle, { width: '100%', marginVertical: 0 }]}
                    onPress={this.anonymousUserLogin}
                  />}
                {this.props.getAnonymousLogin &&
                  <View
                    style={{
                      flexDirection: "row",
                      width: "100%",
                      alignItems: "center",
                      justifyContent: "center",
                      height: 50,
                      marginVertical: 10
                    }}
                  >
                    <View
                      style={{
                        borderWidth: 1,
                        borderColor: "#e1e1e1",
                        width: "43%",
                        justifyContent: "flex-start",
                        height: 1,
                        borderRadius: 6,
                        backgroundColor: "#e1e1e1"
                      }}
                    ></View>
                    <Text
                      style={{
                        fontSize: 16,
                        color: "#49525D",
                        textAlign: "center",
                        marginHorizontal: "4%",
                        marginVertical: "10%",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      or
                  </Text>
                    <View
                      style={{
                        borderWidth: 1,
                        borderColor: "#e1e1e1",
                        width: "43%",
                        justifyContent: "flex-end",
                        height: 1,
                        borderRadius: 6,
                        backgroundColor: "#e1e1e1"
                      }}
                    ></View>
                  </View>
                }
                <Button
                  buttonStyle={[
                    styles.GbuttonStyle,
                    { borderColor: "#c5c5c5", marginTop: 0 }
                  ]}
                  title="Login with Google"
                  titleStyle={styles.Gbuttontextstyle}
                  onPress={this.loginWithGoogle}
                  icon={
                    <Image
                      source={require("../assets/image/gLogin.png")}
                      style={styles.Gicon}
                    />
                  }
                />

                <View
                  style={{
                    flexDirection: "row",
                    width: "100%",
                    alignItems: "center",
                    justifyContent: "center",
                    height: 50,
                    marginVertical: 10
                  }}
                >
                  <View
                    style={{
                      borderWidth: 1,
                      borderColor: "#e1e1e1",
                      width: "43%",
                      justifyContent: "flex-start",
                      height: 1,
                      borderRadius: 6,
                      backgroundColor: "#e1e1e1"
                    }}
                  ></View>
                  <Text
                    style={{
                      fontSize: 16,
                      color: "#49525D",
                      textAlign: "center",
                      marginHorizontal: "4%",
                      marginVertical: "10%",
                      fontFamily: ConstantFontFamily.MontserratBoldFont
                    }}
                  >
                    or
                  </Text>
                  <View
                    style={{
                      borderWidth: 1,
                      borderColor: "#e1e1e1",
                      width: "43%",
                      justifyContent: "flex-end",
                      height: 1,
                      borderRadius: 6,
                      backgroundColor: "#e1e1e1"
                    }}
                  ></View>
                </View>

                <TextInput
                  value={this.state.UserEmail}
                  placeholder="Email"
                  placeholderTextColor="#A9A9A9"
                  onChangeText={UserEmail =>
                    this.setState({ UserEmail, errorMsg: "" })
                  }
                  underlineColorAndroid="transparent"
                  style={[
                    styles.TextInputStyleClass,
                    { borderColor: "#c5c5c5" }
                  ]}
                  testID="UserEmail"
                />
                <TextInput
                  value={this.state.UserPassword}
                  placeholder="Password"
                  placeholderTextColor="#A9A9A9"
                  onChangeText={UserPassword =>
                    this.setState({ UserPassword, errorMsg: "" })
                  }
                  onKeyPress={this.enterPressed}
                  underlineColorAndroid="transparent"
                  style={[
                    styles.TextInputStyleClass,
                    { borderColor: "#c5c5c5" }
                  ]}
                  secureTextEntry={true}
                  testID="UserPassword"
                />
                <View
                  style={{
                    flexDirection: Dimensions.get('window').width > 1100 ? "row" : "column",
                    justifyContent: "space-between"
                  }}
                >
                  <TouchableOpacity onPress={this.userResetModal}>
                    <Text style={[styles.forgotpasswordStyle, { textAlign: Dimensions.get('window').width > 1100 ? 'right' : 'left', paddingHorizontal: 2 }]}>
                      Forgot Password?
                    </Text>
                  </TouchableOpacity>

                  <Text
                    style={{
                      fontSize: 12,
                      color: "#49525D",
                      textAlign: "left",
                      fontFamily: ConstantFontFamily.VerdanaFont,
                      marginTop: 10
                    }}
                  >
                    Don't have an account? &nbsp;
                    <Text
                      style={{
                        textDecorationLine: "underline",
                        color: "#49525D",
                        fontFamily: ConstantFontFamily.VerdanaFont,
                      }}
                      onPress={this.handleSignButton}
                    >
                      Sign Up
                    </Text>
                  </Text>
                </View>
                {this.state.loading == true && <LoaderComponent />}

                <Text
                  style={{
                    color: "#E26A64",
                    textAlign: "left",
                    fontSize: 13,
                    textAlign: "center",
                    marginTop: 10,
                    fontFamily: ConstantFontFamily.defaultFont
                  }}
                >
                  {this.state.errorMsg}
                </Text>
                <View
                  style={{
                    marginVertical: 10,
                    alignSelf: "center"
                  }}
                >
                  {this.props.getGoogleLogin == true ? (
                    <Button
                      title={this.props.getLoginButtonText}
                      testID="UserLogin"
                      titleStyle={[
                        ButtonStyle.titleStyle,
                        {
                          color:
                            this.props.getLoginButtonText == "Logged In!" ||
                              this.props.getLoginButtonText == "Logging in..."
                              ? "#fff"
                              : "#000"
                        }
                      ]}
                      buttonStyle={[
                        ButtonStyle.backgroundStyle,
                        {
                          backgroundColor:
                            this.props.getLoginButtonText == "Logged In!" ||
                              this.props.getLoginButtonText == "Logging in..."
                              ? "#009B1A"
                              : "#fff"
                        }
                      ]}
                      containerStyle={ButtonStyle.containerStyle}
                      onPress={this.userLogin}
                    />
                  ) : (
                      <Button
                        title={this.props.getLoginButtonText}
                        testID="UserLogin"
                        titleStyle={
                          this.props.getLoginButtonText == "Logged In!" ||
                            this.props.getLoginButtonText == "Logging in..."
                            ? ButtonStyle.wtitleStyle
                            : ButtonStyle.titleStyle
                        }
                        buttonStyle={
                          this.props.getLoginButtonText == "Logged In!" ||
                            this.props.getLoginButtonText == "Logging in..."
                            ? ButtonStyle.gbackgroundStyle
                            : ButtonStyle.backgroundStyle
                        }
                        containerStyle={ButtonStyle.containerStyle}
                        disabled={
                          this.state.UserEmail == "" ||
                            this.state.UserPassword == ""
                            ? true
                            : false
                        }
                        onPress={this.userLogin}
                      />
                    )}
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getUserApproach: state.UserApproachReducer.get("setUserApproach"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  getLoginButtonText: state.AdminReducer.get("loginButtonText"),
  getGoogleLogin: state.AdminReducer.get("googleLogin"),
  getAnonymousLogin: state.AdminReducer.get("isAnonymousLogin"),
  getAnonymousToken: state.AdminReducer.get("anonymousToken")

});

const mapDispatchToProps = dispatch => ({
  changeLoginStatus: payload => dispatch(setLoginStatus(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  setUsernameModalStatus: payload => dispatch(setUSERNAMEMODALACTION(payload)),
  setResetPasswordModalStatus: payload =>
    dispatch(setRESETPASSWORDMODALACTION(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setUserApproachAction: payload => dispatch(setUserApproachAction(payload)),
  setVerifyEmailModalStatus: payload =>
    dispatch(setVERIFYEMAILMODALACTION(payload)),
  setInviteSignUpModalStatus: payload =>
    dispatch(setInviteSIGNUPMODALACTION(payload)),
  changeAdminStatus: payload => dispatch(setAdminStatus(payload)),
  SignUpFollowModalStatus: payload =>
    dispatch({ type: "SIGNUP_FOLLOW_MODAL", payload }),
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingUsers: payload => dispatch(getTrendingUsers(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  getTrendingExternalFeeds: payload =>
    dispatch(getTrendingExternalFeeds(payload)),
  setTrendingHomeFeed: payload =>
    dispatch({ type: "SET_TRENDING_HOME_FEED", payload }),
  setNewHomeFeed: payload => dispatch({ type: "SET_NEW_HOME_FEED", payload }),
  setDiscussionHomeFeed: payload =>
    dispatch({ type: "SET_DISCUSSION_HOME_FEED", payload }),
  setLoginButtonText: payload =>
    dispatch({ type: "SET_LOGIN_BUTTON_TEXT", payload }),
  setInviteUserDetail: payload =>
    dispatch({ type: "SET_INVITE_USER_DETAIL", payload }),
  setGoogleLogin: payload => dispatch({ type: "SET_GOOGLE_LOGIN", payload }),
  setAnonymousUser: payload =>
    dispatch({ type: "ANONYMOUS_USER", payload }),
  leftPanelModalFunc: payload => dispatch({ type: 'LEFT_PANEL_OPEN', payload })
});

const LoginModalContainerWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(LoginModal);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginModalContainerWrapper);

export const styles = {
  TextInputStyleClass: {
    marginBottom: 10,
    height: 45,
    paddingLeft: 10,
    borderWidth: 2,
    borderRadius: 10,
    color: "#000",
    backgroundColor: "#fff",
    fontFamily: ConstantFontFamily.defaultFont,
    fontWeight: "bold"
  },
  TextComponentStyle: {
    fontSize: 13,
    color: "#000",
    textAlign: "left",
    fontFamily: ConstantFontFamily.MontserratBoldFont,
    marginTop: 10
  },
  forgotpasswordStyle: {
    fontSize: 12,
    color: "#49525D",
    textAlign: "right",
    fontFamily: ConstantFontFamily.VerdanaFont,
    marginTop: 10
  },
  TextHeaderStyle: {
    fontSize: 23,
    color: "#000",
    textAlign: "center",
    marginBottom: 10,
    marginTop: 20,
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },

  buttonStyle: {
    marginVertical: 25,
    backgroundColor: "#000",
    borderRadius: 6,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
    width: "20%",
    alignSelf: "center"
  },
  GbuttonStyle: {
    backgroundColor: "#FFFFFF",
    borderRadius: 10,
    height: 40,
    alignItems: "center",
    padding: 5,
    borderWidth: 2,
    borderColor: "#000",
    marginTop: 10
  },
  buttontextstyle: {
    fontSize: 16,
    color: "#fff",
    fontFamily: ConstantFontFamily.MontserratBoldFont,
    marginLeft: 5
  },
  Gbuttontextstyle: {
    fontSize: 16,
    color: "#000",
    fontFamily: ConstantFontFamily.MontserratBoldFont,
    marginLeft: 5
  },
  Gicon: {
    height: 25,
    width: 25
  }
};
