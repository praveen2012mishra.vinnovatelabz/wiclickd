import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Button, Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import applloClient from "../client";
import "../components/Firebase";
import ConstantFontFamily from "../constants/FontFamily";
import { ClikJoinMutation } from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { UserQueryMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { ClikJoinVariables } from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { UserDetailsVariables } from "../graphqlSchema/graphqlVariables/UserVariables";
import { SearchUserMutation } from "../graphqlSchema/graphqlMutation/SearchMutation";
import { SearchUserVariables } from "../graphqlSchema/graphqlVariables/SearchVariables";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { graphql } from "react-apollo";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import ButtonStyle from "../constants/ButtonStyle";

class InviteUserMsgModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      qualification: "",
      username: "",
      showError: false,
      MutipleUserList: [],
      UserList: [],
      status: "Default",
      member_type: "",
      user_msg: ""
    };
  }

  checkUser = async () => {
    let value =
      this.state.username.charAt(0) == "@"
        ? this.state.username.substr(1)
        : this.state.username;
    UserDetailsVariables.variables.id = "User:" + value;
    try {
      await applloClient
        .query({
          query: UserQueryMutation,
          ...UserDetailsVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState({
            MutipleUserList: this.state.MutipleUserList.concat([
              {
                id: res.data.user.id,
                name: res.data.user.username,
                pic: res.data.user.profile_pic
              }
            ])
          });
          this.setState({
            showError: false,
            username: ""
          });
        });
    } catch (e) {
      console.log(e);
      this.setState({
        showError: true,
        username: ""
      });
    }
  };

  deleteUser = async index => {
    let updatedList = this.state.MutipleUserList;
    updatedList.splice(index, 1);
    this.setState({
      MutipleUserList: updatedList
    });
  };

  requestInvite = async () => {
    let permittedValues = this.state.MutipleUserList.map(value => value.id);
    ClikJoinVariables.variables.clik_id = this.props.ClikInfo.get("id");
    ClikJoinVariables.variables.qualification = this.state.qualification;
    ClikJoinVariables.variables.known_members = permittedValues;
    try {
      await applloClient
        .query({
          query: ClikJoinMutation,
          ...ClikJoinVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          if (res.data.clik_join.status.custom_status == "JOINED") {
            let member_type = res.data.clik_join.member_type;
            this.setState({
              status: "Success",
              member_type: member_type
            });
            let resDataLogin = await this.props.Login();
            await this.props.saveLoginUser(resDataLogin.data.login);
          } else if (res.data.clik_join.status.custom_status == "PENDING") {
            this.setState({
              status: "Pending"
            });
          } else {
            let user_msg = res.data.clik_join.status.user_msg;
            this.setState({
              status: "Failure",
              user_msg: user_msg
            });
          }
        });
    } catch (e) {
      console.log(e);
    }
  };

  customRenderUserSuggestion = value => {
    SearchUserVariables.variables.prefix = value;
    applloClient
      .query({
        query: SearchUserMutation,
        ...SearchUserVariables,
        fetchPolicy: "no-cache"
      })
      .then(res => {
        this.setState({ UserList: res.data.search.users });
      });
  };

  checkSelectedUser = async value => {
    UserDetailsVariables.variables.id = "User:" + value;
    try {
      await applloClient
        .query({
          query: UserQueryMutation,
          ...UserDetailsVariables,
          fetchPolicy: "no-cache"
        })
        .then(async res => {
          this.setState({
            MutipleUserList: this.state.MutipleUserList.concat([
              {
                id: res.data.user.id,
                name: res.data.user.username,
                pic: res.data.user.profile_pic
              }
            ])
          });
          this.setState({
            showError: false,
            username: ""
          });
        });
    } catch (e) {
      console.log(e);
      this.setState({
        showError: true,
        username: ""
      });
    }
  };

  render() {
    return (
      <View
        style={{
          width: "100%"
        }}
      >
        <Hoverable>
          {isHovered => (
            <TouchableOpacity
              style={{
                flexDirection: "row",
                justifyContent: "flex-start",
                flex: 1,
                position: "absolute",
                zIndex: 999999,
                left: 0,
                top: 0
              }}
              onPress={this.props.onClose}
            >
              <Icon
                color={isHovered == true ? "rgba(256,256,256,0.4)" : "#000"}
                iconStyle={{
                  color: "#fff",
                  justifyContent: "center",
                  alignItems: "center"
                }}
                reverse
                name="close"
                type="antdesign"
                size={16}
              />
            </TouchableOpacity>
          )}
        </Hoverable>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            backgroundColor: "#000",
            alignItems: "center",
            height: 50,
            borderTopLeftRadius: 6,
            borderTopRightRadius: 6
          }}
        >
          <Text
            style={{
              color: "#fff",
              fontWeight: "bold",
              fontSize: 18,
              fontFamily: ConstantFontFamily.MontserratBoldFont
            }}
          >
            Invite Members
          </Text>
        </View>

        {this.props.status == "success" && (
          <View
            style={{
              borderRadius: 4,
              width: "100%",
              backgroundColor: "#fff",
              paddingVertical: 20
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              {this.props.user_msg}
            </Text>

            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                marginVertical: 20
              }}
            >
              <Button
                color="#fff"
                title="Continue"
                titleStyle={ButtonStyle.titleStyle}
                buttonStyle={ButtonStyle.backgroundStyle}
                containerStyle={ButtonStyle.containerStyle}
                onPress={() => this.props.onClose()}
              />
            </View>
          </View>
        )}

        {this.props.status == "failure" && (
          <View
            style={{
              borderRadius: 4,
              width: "100%",
              backgroundColor: "#fff",
              paddingVertical: 20
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              Failed to invite members.
            </Text>

            <Text
              style={{
                textAlign: "center",
                color: "#000",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontSize: 16,
                fontWeight: "bold",
                margin: 10
              }}
            >
              {this.props.user_msg}
            </Text>

            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                marginVertical: 20
              }}
            >
              <Button
                color="#fff"
                title="Continue"
                titleStyle={ButtonStyle.titleStyle}
                buttonStyle={ButtonStyle.backgroundStyle}
                containerStyle={ButtonStyle.containerStyle}
                onPress={() => this.props.onClose()}
              />
            </View>
          </View>
        )}
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload))
});

const InviteUserMsgModalWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(InviteUserMsgModal);

export default compose(connect(null, mapDispatchToProps))(
  InviteUserMsgModalWrapper
);

export const styles = {
  LogoImageStyle: {
    height: 80,
    width: 235
  },
  LogoContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    margin: 25
  }
};
