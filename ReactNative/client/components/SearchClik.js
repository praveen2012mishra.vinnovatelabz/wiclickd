import { openBrowserAsync } from "expo-web-browser";
import { List } from "immutable";
import React from "react";
import { graphql } from "react-apollo";
import {
  ActivityIndicator,
  Dimensions,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  ImageBackground
} from "react-native";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";
import { listClikMembers } from "../actionCreator/ClikMembersAction";
import { listClikUserRequest } from "../actionCreator/ClikUserRequestAction";
import { getFeedProfileDetails } from "../actionCreator/FeedProfileAction";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { listTopicFeed } from "../actionCreator/TopicsFeedAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { getTrendingTopicsProfileDetails } from "../actionCreator/TrendingTopicsProfileAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import applloClient from "../client";
import AppHelper from "../constants/AppHelper";
import ConstantFontFamily from "../constants/FontFamily";
import {
  ClikFollowMutation,
  ClikUnfollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { DefaultClikListMutation } from "../graphqlSchema/graphqlMutation/SearchMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  ClikFollowVariables,
  ClikUnfollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import ClikStar from "./ClikStar";

class SearchTopic extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onEndReachedCalledDuringMomentum = true;
    this.viewabilityConfig = {
      viewAreaCoveragePercentThreshold: 50
    };
    this.state = {
      ClikList: this.props.ClikList,
      term: this.props.term,
      page: 1,
      loading: true,
      loadingMore: false,
      refreshing: false,
      pageInfo: null,
      error: null,
      apiCall: true
    };
  }

  componentDidMount = () => {
    this.getDefaultClikList();
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.ClikList !== this.props.ClikList) {
      this.setState({
        ClikList: this.props.ClikList
      });
    }
    if (prevProps.term !== this.props.term) {
      this.setState({
        term: this.props.term
      });
    }
  };

  goToClikProfile = id => {
    this.props.ClikId({
      id: id,
      type: "feed"
    });
    this.props.setClikUserRequest({
      id: id.replace("%3A", ":"),
      currentPage: AppHelper.PAGE_LIMIT
    });
    this.props.setClikMembers({
      id: id.replace("%3A", ":")
    });
  };

  goToProfile = id => {
    this.props.userId({
      id: id,
      type: "feed"
    });
    this.props.setClikUserRequest({
      id: id,
      currentPage: AppHelper.PAGE_LIMIT
    });
    this.props.setClikMembers({
      id: id
    });
  };

  getClikStar = ClikName => {
    let index = 0;
    index = this.props.getUserFollowCliksList.findIndex(
      i =>
        i
          .getIn(["clik", "name"])
          .toLowerCase()
          .replace("clik:", "") == ClikName.toLowerCase().replace("clik:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowCliksList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  followCliks = cliksId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    ClikFollowVariables.variables.clik_id = cliksId;
    ClikFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: ClikFollowMutation,
        ...ClikFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowCliks = async cliksId => {
    ClikUnfollowVariables.variables.clik_id = cliksId;
    applloClient
      .query({
        query: ClikUnfollowMutation,
        ...ClikUnfollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  favroiteCliks = async cliksId => {
    ClikFollowVariables.variables.clik_id = cliksId;
    ClikFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: ClikFollowMutation,
        ...ClikFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  openWindow = async link => {
    await openBrowserAsync(link);
  };

  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this.getDefaultClikList();
      }
    );
  };

  _handleLoadMore = distanceFromEnd => {
    this.setState(
      (prevState, nextProps) => ({
        //  loadingMore: true
      }),
      () => {
        if (
          0 <= distanceFromEnd &&
          distanceFromEnd <= 5 &&
          this.state.apiCall == true
        ) {
          this.setState({
            apiCall: false
          });
          this.getDefaultClikList();
        }
      }
    );
  };

  _renderFooter = () => {
    if (!this.state.loadingMore) return null;
    return (
      <View>
        <ActivityIndicator animating size="large" color="#000" />
      </View>
    );
  };

  getDefaultClikList = () => {
    let __self = this;
    const { page, pageInfo } = this.state;

    if (pageInfo == null) {
      this.setState({
        loadingMore: true
      });
      applloClient
        .query({
          query: DefaultClikListMutation,
          variables: {
            first: 20,
            after: pageInfo ? pageInfo.endCursor : null
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          __self.setState({
            loading: false,
            ClikList: res.data.clik_list.edges,
            pageInfo: res.data.clik_list.pageInfo,
            page: page + 1,
            apiCall: true,
            loadingMore: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else if (pageInfo != null && pageInfo.hasNextPage == true) {
      this.setState({
        loadingMore: true
      });
      applloClient
        .query({
          query: DefaultClikListMutation,
          variables: {
            first: 10,
            after: pageInfo ? pageInfo.endCursor : null
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          __self.setState({
            loading: false,
            ClikList: __self.state.ClikList.concat(res.data.clik_list.edges),
            pageInfo: res.data.clik_list.pageInfo,
            apiCall: true,
            loadingMore: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    }
  };

  _renderItem = item => {
    var row = item.item.node ? item.item.node : item.item;
    return (
      <View key={item.index}>
        {/* <View
          style={{
            overflow: "hidden",
            marginBottom: 5,
            flexDirection: "row"
          }}
        >
          <View
            style={{
              width: "80%",
              justifyContent: "flex-start",
              alignItems: "center",
              display: "flex",
              flexDirection: "row"
            }}
          >
            <TouchableOpacity
              style={{
                height: 30,
                padding: 5,
                backgroundColor: "#E8F5FA",
                borderRadius: 6,
                alignSelf: "flex-start",
                marginHorizontal: 5
              }}
              onPress={() => this.goToClikProfile(row.name)}
            >
              <Text
                style={{
                  color: "#4169e1",
                  fontWeight: "bold",
                  fontSize: 14,
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
              >
                #{row.name}
              </Text>
            </TouchableOpacity>
            <Text
              numberOfLines={1}
              style={{
                textAlign: "left",
                color: "#000",
                fontSize: 13,
                fontFamily: ConstantFontFamily.defaultFont,
                // whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis",
                paddingHorizontal: 10
              }}
            >
              {row.description}
            </Text>
          </View>

          <View
            style={{
              width: "10%",
              justifyContent: "center",
              flexDirection: "row",
              alignSelf: "center",
              alignItems: "center",
              height: 30
            }}
          >
            <View
              style={{
                flexDirection: "row"
              }}
            >
              <Icon
                name="user-plus"
                type="font-awesome"
                color={"#000"}
                size={20}
              />

              <Text
                style={{
                  marginHorizontal: 5,
                  color: "#000",
                  fontSize: 15,
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  fontWeight: "bold",
                  textAlign: "center"
                }}
              >
                {row.num_members}
              </Text>
            </View>
          </View>

          <View
            style={{
              width: "10%",
              justifyContent: "center",
              flexDirection: "row",
              alignSelf: "center",
              alignItems: "center",
              height: 30
            }}
          >
            <View
              style={{
                flexDirection: "row"
              }}
            >
              <ClikStar
                ClikName={row.name}
                ContainerStyle={{}}
                ImageStyle={{
                  height: 20,
                  width: 20,
                  alignSelf: "center"
                }}
              />

              <Text
                style={{
                  marginHorizontal: 5,
                  color: "#000",
                  fontSize: 15,
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  fontWeight: "bold",
                  textAlign: "center"
                }}
              >
                {row.num_followers}
              </Text>
            </View>
          </View>
        </View> */}

        <TouchableOpacity
          style={{
            borderRadius: 10,
            overflow: "hidden",
            borderWidth: 1,
            borderColor: "#c5c5c5",
            maxHeight: Dimensions.get("window").height / 2,
            marginBottom: 5,
            paddingBottom: 5,
            backgroundColor: "#fff"
          }}
          onPress={() =>
            this.goToClikProfile(
              row.name.replace("Clik%3A", "#").replace("Clik:", "#")
            )
          }
        >
          <View>
            <ImageBackground
              style={{
                height: Dimensions.get("window").height / 4,
                backgroundColor: "#fff"
              }}
              source={{
                uri: row.banner_pic
              }}
            ></ImageBackground>
            <View
              style={{
                flexDirection: "column",
                justifyContent: "flex-end",
                flex: 1,
                backgroundColor: "#fff",
                paddingHorizontal: 20
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  width: "100%",
                  marginVertical: 10,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <View
                  style={{
                    // width: row.invite_only == true ? "65%" : "70%"
                    width: "70%"
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <TouchableOpacity
                      style={{
                        height: 30,
                        padding: 8,
                        backgroundColor: "#E8F5FA",
                        borderRadius: 10,
                        alignSelf: "flex-start",
                        marginRight: 5,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      onPress={() =>
                        this.goToClikProfile(
                          row.name.replace("Clik%3A", "#").replace("Clik:", "#")
                        )
                      }
                    >
                      <Text
                        style={{
                          color: "#4169e1",
                          fontWeight: "bold",
                          fontSize: 18,
                          fontFamily: ConstantFontFamily.MontserratBoldFont
                        }}
                      >
                        #
                        {row.name.replace("Clik%3A", "#").replace("Clik:", "#")}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <View
                  style={{
                    width: "30%",
                    height: 30,
                    justifyContent: "flex-end",
                    flexDirection: "row",
                    alignSelf: "center",
                    alignItems: "center"
                  }}
                >
                  {this.props.loginStatus == 1 &&
                    Dimensions.get("window").width >= 750 && (
                      <View style={{ flexDirection: "row" }}>
                        {/* <View style={{ flexDirection: "row" }}>
                          <Icon
                            name="users"
                            type="font-awesome"
                            color={"#000"}
                            size={20}
                          />
                          <Text
                            style={{
                              marginRight: 10,
                              marginHorizontal: 5,
                              textAlign: "center",
                              color: "#000",
                              fontFamily: ConstantFontFamily.MontserratBoldFont,
                              fontSize: 15,
                              fontWeight: "bold"
                            }}
                          >
                            {row.num_members}
                          </Text>
                        </View> */}
                        <View style={{ flexDirection: "row" }}>
                          <ClikStar
                            MemberType={""}
                            ClikName={row.name}
                            ContainerStyle={{}}
                            ImageStyle={{
                              height: 20,
                              width: 20,
                              alignSelf: "center",
                              marginLeft: 5
                            }}
                          />
                          {/* <Text
                            style={{
                              alignSelf: "center",
                              marginHorizontal: 5,
                              textAlign: "center",
                              color: "#000",
                              fontFamily: ConstantFontFamily.MontserratBoldFont,
                              fontSize: 16,
                              fontWeight: "bold"
                            }}
                          >
                            {row.num_followers}
                          </Text> */}
                        </View>
                      </View>
                    )}
                  {row.invite_only == true && (
                    <Icon
                      name="lock"
                      type="font-awesome"
                      color={"#000"}
                      size={20}
                      iconStyle={{ marginHorizontal: 5 }}
                    />
                  )}
                </View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    color: "grey",
                    fontSize: 13,
                    // fontWeight: 'bold',
                    fontFamily: ConstantFontFamily.defaultFont
                  }}
                  onPress={() => props.showMembers()}
                >
                  {row.num_members} Members {row.num_followers} Followers
                </Text>
                {row.website != "" && row.website != null && (
                  <View style={{ alignSelf: "center", marginLeft: 10 }}>
                    <Icon
                      name="link"
                      type="font-awesome"
                      color="#6D757F"
                      size={15}
                    />
                  </View>
                )}
                <Text
                  onPress={() => this.openWindow(row.website)}
                  style={{
                    marginLeft: 5,
                    color: "grey",
                    fontSize: 13,
                    // fontWeight: 'bold',
                    fontFamily: ConstantFontFamily.defaultFont
                  }}
                >
                  {row.website != "" && row.website != null
                    ? row.website
                    : row.website}
                </Text>
              </View>
              <View style={{ marginTop: 10 }}>
                <Text
                  style={{
                    marginBottom: 10,
                    color: "#000",
                    fontSize: 13,
                    fontFamily: ConstantFontFamily.defaultFont
                  }}
                >
                  {row.description}
                </Text>
              </View>

              {this.props.loginStatus == 1 &&
                Dimensions.get("window").width < 750 && (
                  <View style={{ flexDirection: "row", alignSelf: "center" }}>
                    <View style={{ flexDirection: "row" }}>
                      <Icon
                        name="users"
                        type="font-awesome"
                        color={"#000"}
                        size={20}
                      />
                      <Text
                        style={{
                          marginRight: 10,
                          marginHorizontal: 5,
                          textAlign: "center",
                          color: "#000",
                          fontFamily: ConstantFontFamily.MontserratBoldFont,
                          fontSize: 15,
                          fontWeight: "bold"
                        }}
                      >
                        {row.num_members}
                      </Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                      <ClikStar
                        MemberType={""}
                        ClikName={row.name}
                        ContainerStyle={{}}
                        ImageStyle={{
                          height: 20,
                          width: 20,
                          alignSelf: "center",
                          marginLeft: 5
                        }}
                      />
                      <Text
                        style={{
                          alignSelf: "center",
                          marginHorizontal: 5,
                          textAlign: "center",
                          color: "#000",
                          fontFamily: ConstantFontFamily.MontserratBoldFont,
                          fontSize: 16,
                          fontWeight: "bold"
                        }}
                      >
                        {row.num_followers}
                      </Text>
                    </View>
                  </View>
                )}
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  onViewableItemsChanged = ({ viewableItems, changed }) => {
    let perLoadDataCount = 20;
    let halfOfLoadDataCount = perLoadDataCount / 2;
    let lastAddArr =
      this.state.ClikList.length > 0 &&
      this.state.ClikList.slice(-perLoadDataCount);

    try {
      if (
        lastAddArr[halfOfLoadDataCount] &&
        viewableItems.length > 0 &&
        this.props.loginStatus == 1
      ) {
        //FlatList
        if (
          viewableItems.find(
            item =>
              item.item.node.id === lastAddArr[halfOfLoadDataCount].node.id
          )
        ) {
          this._handleLoadMore(0);
        }
      }
    } catch (e) {
      console.log(e, "lastAddArr", lastAddArr[halfOfLoadDataCount]);
    }
  };

  render() {
    return (
      <View style={{ marginTop: 10, flex: 1, marginHorizontal: "auto" }}>
        <FlatList
          extraData={this.state}
          contentContainerStyle={{
            flexDirection: "column",
            paddingBottom: 10,
            // height: Dimensions.get("window").height -165,
            width: "100%"
          }}
          data={this.state.ClikList}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItem}
          showsVerticalScrollIndicator={false}
          onRefresh={this._handleRefresh}
          refreshing={this.state.refreshing}
          onEndReached={({ distanceFromEnd }) => {
            //this._handleLoadMore(distanceFromEnd);
          }}
          onEndReachedThreshold={0.2}
          initialNumToRender={10}
          ListFooterComponent={this._renderFooter}
          onViewableItemsChanged={this.onViewableItemsChanged}
          viewabilityConfig={this.viewabilityConfig}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  listTrending_cliks: !state.TrendingCliksReducer.getIn(["Trending_cliks_List"])
    ? List()
    : state.TrendingCliksReducer.getIn(["Trending_cliks_List"]),
  link: state.LinkPostReducer.get("link"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  loginStatus: state.UserReducer.get("loginStatus"),
  getUserFollowTopicList: state.LoginUserDetailsReducer.get(
    "userFollowTopicsList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowTopicsList")
    : List(),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List(),
  getUserFollowUserList: state.LoginUserDetailsReducer.get("userFollowUserList")
    ? state.LoginUserDetailsReducer.get("userFollowUserList")
    : List(),
  getUserFollowFeedList: state.LoginUserDetailsReducer.get("userFollowFeedList")
    ? state.LoginUserDetailsReducer.get("userFollowFeedList")
    : List(),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension")
});

const mapDispatchToProps = dispatch => ({
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  topicId: payload => dispatch(getTrendingTopicsProfileDetails(payload)),
  listTopicFeed: payload => dispatch(listTopicFeed(payload)),
  ClikId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  setClikUserRequest: payload => dispatch(listClikUserRequest(payload)),
  setClikMembers: payload => dispatch(listClikMembers(payload)),
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload)),
  setFeedDetails: payload => dispatch(getFeedProfileDetails(payload))
});

const SearchTopicWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(SearchTopic);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  SearchTopicWrapper
);
