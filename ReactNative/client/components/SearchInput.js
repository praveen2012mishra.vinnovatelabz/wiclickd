import React, { Component } from "react";
import ConstantFontFamily from "../constants/FontFamily";
import {
  SearchClikMutation,
  SearchEveryThingMutation,
  SearchFeedMutation,
  SearchTopicMutation,
  SearchUserMutation
} from "../graphqlSchema/graphqlMutation/SearchMutation";
import {
  Animated,
  Dimensions,
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View,
  FlatList
} from "react-native";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";

class SearchInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      term: "",
      keyEvent: ""
    };
  }

  getTopicStar = TopicName => {
    let index = 0;
    index = this.props.getUserFollowTopicList.findIndex(
      i =>
        i
          .getIn(["topic", "name"])
          .toLowerCase()
          .replace("topic:", "") ==
        TopicName.toLowerCase().replace("topic:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowTopicList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  getClikStar = ClikName => {
    let index = 0;
    index = this.props.getUserFollowCliksList.findIndex(
      i =>
        i
          .getIn(["clik", "name"])
          .toLowerCase()
          .replace("clik:", "") == ClikName.toLowerCase().replace("clik:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowCliksList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  favroiteCliks = async cliksId => {
    ClikFollowVariables.variables.clik_id = cliksId;
    ClikFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: ClikFollowMutation,
        ...ClikFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowCliks = async cliksId => {
    ClikUnfollowVariables.variables.clik_id = cliksId;
    applloClient
      .query({
        query: ClikUnfollowMutation,
        ...ClikUnfollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  getUserStar = UserName => {
    let index = 0;
    index = this.props.getUserFollowUserList.findIndex(
      i =>
        i
          .getIn(["user", "username"])
          .toLowerCase()
          .replace("user:", "") == UserName.toLowerCase().replace("user:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowUserList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  getFeedStar = Name => {
    let index = 0;
    index = this.props.getUserFollowFeedList.findIndex(
      i =>
        i
          .getIn(["feed", "name"])
          .toLowerCase()
          .replace("ExternalFeed:", "") ==
        Name.toLowerCase().replace("ExternalFeed:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowFeedList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };

  favroiteFeed = async Id => {
    FeedFollowVariables.variables.feed_id = Id;
    FeedFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: FeedFollowMutation,
        ...FeedFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowFeed = async Id => {
    FeedUnFollowVariables.variables.feed_id = Id;
    applloClient
      .query({
        query: FeedUnFollowMutation,
        ...FeedUnFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  favroiteTopics = async topicId => {
    TopicFollowVariables.variables.topic_id = topicId;
    TopicFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowTopics = async topicId => {
    TopicUnFollowVariables.variables.topic_id = topicId;
    applloClient
      .query({
        query: TopicUnFollowMutation,
        ...TopicUnFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  followTopics = topicId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    TopicFollowVariables.variables.topic_id = topicId;
    TopicFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: TopicFollowMutation,
        ...TopicFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  componentDidMount() {
    this.props.setTerm(this.state.term);
    this.props.setKeyEvent(this.state.keyEvent);
    this.props.setDisplayType('');
  }

  handleEvent = e => {
    //console.log(e,'------------------>');
    var code = e.keyCode || e.which;
    this.props.setKeyEvent(code);
    this.setState({ keyEvent: code });
    this.props.setDisplayType(this.props.displayType);
    //this.props.onkeypress1(e)
  };

  render() {
    //console.log(this.props.term,'============================>');
    return (
      <View>
        <Animated.View
          style={{
            zIndex: 10,
            overflow: "hidden",
            borderRadius: 10,
            width: "100%"
          }}
        >
          <View
            style={{
              width: "100%",
              flexDirection: "row",
              backgroundColor: "#000",
              borderRadius: 10,
              height: 42
            }}
          >
            {Platform.OS != "web" && (
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  alignSelf: "center"
                }}
                onPress={() => {
                  let nav = this.props.navigation.dangerouslyGetParent().state;
                  if (nav.routes.length > 1) {
                    this.props.navigation.goBack();
                    return;
                  } else {
                    this.props.navigation.navigate("home");
                  }
                }}
              >
                <Icon
                  color={"#fff"}
                  iconStyle={{ paddingLeft: 15 }}
                  name="angle-left"
                  type="font-awesome"
                  size={40}
                />
              </TouchableOpacity>
            )}
            <View
              style={{
                width: "100%",
                flex: 1,
                backgroundColor: "#000",
                justifyContent: "center",
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <Icon
                iconStyle={{
                  position: "absolute",
                  left: 20,
                  color:Dimensions.get("window").width <= 750?'gray':'black'
                }}
                name="search"
                size={20}
                type="font-awesome"
              />
              <TextInput
                value={this.props.term && this.props.term.replace(",", "")}
                ref={r => {
                  this.props.refs(r);
                }}
                autoFocus={false}
                placeholder="Search Weclikd"
                onChangeText={term => {
                  //this.props.onchangetext1(term);
                  this.props.setTerm(term);
                  this.setState({ term: term });
                }}
                onKeyPress={this.handleEvent}
                style={{
                  height: 34,
                  padding: 5,
                  width: "95%",
                  borderColor: "#e1e1e1",
                  borderWidth: 1,
                  borderRadius: 10,
                  backgroundColor: "#fff",
                  marginHorizontal: 10,
                  fontFamily: ConstantFontFamily.defaultFont,
                  fontWeight: "bold",
                  paddingLeft: 35,
                  outline: "none",
                  color:Dimensions.get("window").width <= 750?'gray':'black'
                }}
                //   selection={this.props.selection}
                //   onSelectionChange={({ nativeEvent }) => {this.props.onselectionchange1(nativeEvent)}}
              />
            </View>
          </View>
        </Animated.View>
      </View>
    );
  }
}


const mapStateToProps = state =>{
  //console.log(state.ScreenLoadingReducer.get("setTerm"));
  return ({
  term: state.ScreenLoadingReducer.get("setTerm")
})};

const mapDispatchToProps = dispatch => ({
  setTerm: payload => dispatch({ type: "SET_TERM", payload }),
  setKeyEvent: payload => dispatch({ type: "SET_KEY_EVENT", payload }),
  setDisplayType: payload => dispatch({ type: "SET_DISPLAY_TYPE", payload })
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  SearchInput
);

//export default SearchInput;
