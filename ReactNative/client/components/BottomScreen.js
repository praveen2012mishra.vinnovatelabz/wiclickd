import React, { Component } from "react";
import { View, Text, Dimensions, StyleSheet } from "react-native";
import IconMenu from "./IconMenu";
import NavigationService from "../library/NavigationService";
import RNPickerSelect from "react-native-picker-select";
import { connect } from "react-redux";
import { Icon } from "react-native-elements";
import ConstantFontFamily from "../constants/FontFamily";
import { Platform } from "react-native";

const roleWiseViewOption = [
  {
    label: "Trending",
    value: "Trending",
  },
  {
    label: "New",
    value: "New",
  },
  {
    label: "Bookmarks",
    value: "Bookmarks",
  },
  {
    label: "Reported",
    value: "Reported",
  },
];

class BottomScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabType: "Trending",
      openCreateCommentStatus: false,

    };
    this.inputRefs = {};
  }

  render() {
    return (
      <View
        style={{
          width: "100%",
          backgroundColor:
            Dimensions.get("window").width <= 750 ? "#fff" : "#000",
          height: 50,
          flexDirection: "row",
          justifyContent: "space-around",
          alignItems: "center",
          borderTopWidth: 1,
          borderTopColor: "#c5c5c5",
        }}
      >
        {/* {window.location.href.search("analytics") == -1 && ( */}
        <IconMenu navigation={NavigationService} />
        {/* )} */}
           {/* <View style={{ flexDirection: "column", alignItems: "center" }}>
             <Text
              style={{
                fontFamily: ConstantFontFamily.defaultFont,
                marginBottom: 1,
                fontSize: 13,
              }}
            >
              Sort
            </Text>  */}
            <View
              style={{
                borderRadius: 8,
                borderColor: "#c5c5c5",
                borderWidth: 1,
                width: 120,
                alignItems:'center',
                justifyContent:'center',
                marginVertical:2,
              }}
            >
              <RNPickerSelect
                placeholder={{}}
                items={                  
                  roleWiseViewOption
                }
                onValueChange={(itemValue, itemIndex) => {
                  this.setState({ tabType: itemValue }, () => {
                    this.props.setTabView(this.state.tabType);
                  });
                }}                
                value={this.state.tabType}
                useNativeAndroidPickerStyle={Platform.OS != 'web' ? false : true}              
                style={{ ...styles }}                             
              />               
            </View>
           {/* </View> */}

        {/* {NavigationService.getCurrentRoute().routeName == "info" && */}
        {/* {this.props.loginStatus == 1 && */}
        {/* <Icon
                    name={"comment"}
                    type="font-awesome"
                    size={25}
                    iconStyle={{ alignSelf: "center", marginLeft: 5 }}
                    containerStyle={{ alignSelf: "center" }}
                    onPress={() =>this.setState({openCreateCommentStatus : !this.state.openCreateCommentStatus}, ()=>{
                        this.props.openCreateComment(this.state.openCreateCommentStatus)
                    })
                        
                    }
                /> */}
        {/* } */}
        {/* }  */}
      </View>
    );
  }
}
const mapDispatchToProps = (dispatch) => ({
  setTabView: (payload) => dispatch({ type: "SET_TAB_VIEW", payload }),
  openCreateComment: (payload) =>
    dispatch({ type: "OPEN_CREATE_COMMENT", payload }),
});
export default connect(null, mapDispatchToProps)(BottomScreen);

const styles = StyleSheet.create({
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
    fontSize: 16,
    // fontWeight: "bold",
    fontFamily: ConstantFontFamily.VerdanaFont,
    textAlign: "center",
  },
  inputAndroid: {
    //   width: 145,
    paddingHorizontal: 10,
    paddingVertical: 1,
    borderWidth: 1,
    borderColor: "#fff",
    borderRadius: 20,
    color: "#000",
    backgroundColor: "white",
    fontSize: 16,
    // fontWeight: "bold",
    fontFamily: ConstantFontFamily.VerdanaFont,
    textAlign: "center",
  },
  
});

