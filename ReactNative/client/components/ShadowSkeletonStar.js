import React from "react";
import { Animated, Dimensions, View } from "react-native";
import { Icon } from "react-native-elements";

class ShadowSkeletonStar extends React.Component {
  constructor(props) {
    super(props);
    this.Pagescrollview = null;
    this.circleAnimatedValue = new Animated.Value(0);
    this.count = [1, 2, 3, 4, 5];
  }

  componentDidMount() {
    this.circleAnimated();
  }

  circleAnimated = () => {
    this.circleAnimatedValue.setValue(0);
    Animated.timing(this.circleAnimatedValue, {
      toValue: 1,
      duration: 1000
    }).start(() => {
      setTimeout(() => {
        this.circleAnimated();
      }, 1000);
    });
  };

  render() {
    const translateX = this.circleAnimatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [-10, Dimensions.get("window").width]
    });
    const translateTitle = this.circleAnimatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [-10, Dimensions.get("window").width]
    });
    return (
      <View>
        {this.count.map((item, i) => {
          return (
            <View
              key={i}
              style={{
                width: "100%",
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  width: "80%",
                  marginTop: 5,
                  marginLeft: 5,
                  backgroundColor: "#fff",
                  overflow: "hidden",
                  borderRadius: 10,
                  borderWidth: 1,
                  borderColor: "#c5c5c5",
                }}
              >
                <Animated.View
                  style={{
                    width: "50%",
                    height: 30,
                    borderRadius: 10,
                    borderWidth: 1,
                    borderColor: "#c5c5c5",
                    backgroundColor: "#e3e3e3",
                    transform: [{ translateX: translateTitle }]
                  }}
                ></Animated.View>
              </View>

              <View
                style={{
                  width: "20%",
                  marginTop: 5,
                  marginLeft: 5,
                  overflow: "hidden",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Icon
                  color={"#f0f0f0"}
                  name={"star"}
                  type="font-awesome"
                  size={25}
                  iconStyle={{ alignSelf: "center" }}
                  containerStyle={{ alignSelf: "center" }}
                />
              </View>
            </View>
          );
        })}
      </View>
    );
  }
}
export default ShadowSkeletonStar;
