import { openBrowserAsync } from "expo-web-browser";
import { List } from "immutable";
import Modal from "modal-enhanced-react-native-web";
import React, { useEffect, useState } from "react";
import { graphql } from "react-apollo";
import {
  Dimensions,
  ImageBackground,
  Platform,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  Image
} from "react-native";
import { Icon, Button } from "react-native-elements";
import Overlay from "react-native-modal-overlay";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { editClik } from "../actionCreator/ClikEditAction";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import InviteUserModal from "../components/InviteUserModal";
import LeaveCliksModal from "../components/LeaveCliksModal";
import RequestInviteModal from "../components/RequestInviteModal";
import ConstantFontFamily from "../constants/FontFamily";
import { DeleteClikMutation } from "../graphqlSchema/graphqlMutation/TrendingMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import RNPickerSelect from "react-native-picker-select";
import UserStar from "./UserStar";
import ConstantColors from "./../constants/Colors";
import { TabBar, TabView, SceneMap, TabViewPage } from "react-native-tab-view";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

import CliksProfileScreen from "../screens/CliksProfileScreen";
import ClikStar from "./ClikStar";
import ButtonStyle from "../constants/ButtonStyle";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger,
} from "react-native-popup-menu";

const FirstRoute = () => (
  <View style={[styles.scene, { backgroundColor: "#ff4081" }]} />
);

const ThirdRoute = () => (
  <View style={[styles.scene, { backgroundColor: "red" }]} />
);

function truncate(input) {
  if (input.length > 15) {
    return input.substring(0, 15) + "...";
  }
  return input;
}

const CliksProfileCard = props => {
  let [Height, setHeight] = useState(0);
  const cliks = props.item.getIn(["data", "clik"]);
  const [followBtnActive, setfollowBtnActive] = useState(0);
  const [showRequestInviteModal, setshowRequestInviteModal] = useState(false);
  const [showLeaveCliksModal, setshowLeaveCliksModal] = useState(false);
  const [Badge, setBadge] = useState(require("../assets/image/YBadge.png"));
  const [showInviteUserModal, setshowInviteUserModal] = useState(false);
  const [ClikEdit, setClikEdit] = useState(false);
  const [userPermision, setuserPermision] = useState(false);
  const [cliksselectItem, setcliksselectItem] = useState("");
  const [inputRefs, setinputRefs] = useState({});
  const [userApprovePermision, setuserApprovePermision] = useState(false);
  const [index, setIndex] = React.useState(0);
  const [routes, setroutes] = React.useState([
    { key: "first", title: "Feed" },
    { key: "second", title: "Members" },
    { key: "third", title: "Applications" }
  ]);
  let [MenuHover, setMenuHover] = useState(false);

  const [items, setitems] = useState([
    {
      label: "Profile",
      value: "FEED",
      key: 0
    },
    {
      label: "Members",
      value: "USERS",
      key: 1
    },
    {
      label: "Applications",
      value: "APPLICATIONS",
      key: 2
    }
  ]);
  let cliksName = cliks && cliks.getIn(["name"]);
  let banner_pic = cliks && cliks.getIn(["banner_pic"]);
  let description = cliks && cliks.getIn(["description"]);
  let cliksFollowers = cliks && cliks.getIn(["num_followers"]);
  let cliksMembers = cliks && cliks.getIn(["num_members"]);
  let website = cliks && cliks.getIn(["website"]);

  useEffect(() => {
    if (props.loginStatus == 1) {
      getUserPermision();
      getUserApprovePermision();
      Invite(
        cliks && cliks.get("name"),
        Badge == "icon"
          ? cliks && cliks.get("invite_only") == true
            ? "Apply"
            : "Join"
          : "Invite"
      );
      const itemId = props.navigation.getParam("id", "NO-ID");
      if (itemId == "NO-ID") {
        props.navigation.navigate("home");
      }
      const index = props.getUserFollowCliksList.findIndex(
        i =>
          i
            .getIn(["clik", "name"])
            .toLowerCase()
            .replace("clik:", "") ==
          itemId
            .replace("%3A", ":")
            .toLowerCase()
            .replace("clik:", "")
      );
      if (index != -1) {
        if (
          props.getUserFollowCliksList.getIn([
            index,
            "settings",
            "follow_type"
          ]) == "FAVORITE"
        ) {
          setfollowBtnActive(2);
        }
        if (
          props.getUserFollowCliksList.getIn([
            index,
            "settings",
            "follow_type"
          ]) == "FOLLOW"
        ) {
          setfollowBtnActive(1);
        }
        if (
          props.getUserFollowCliksList.getIn([index, "member_type"]) ==
          "SUPER_ADMIN"
        ) {
          setBadge(require("../assets/image/YBadge.png"));
          //props.icon(require("../assets/image/YBadge.png"));
        } else if (
          props.getUserFollowCliksList.getIn([index, "member_type"]) == "ADMIN"
        ) {
          setBadge(require("../assets/image/SBadge.png"));
          //props.icon(require("../assets/image/SBadge.png"));
        } else if (
          props.getUserFollowCliksList.getIn([index, "member_type"]) == "MEMBER"
        ) {
          setBadge(require("../assets/image/badge.png"));
          // props.icon(require("../assets/image/badge.png"));
        } else {
          setBadge("icon");
          getIconColour(index);
        }
      } else {
        setfollowBtnActive(0);
        setBadge("icon");
        getIconColour(index);
      }
      const Clikindex = props.listClikMembers.findIndex(
        i =>
          i.node.user.id ==
          props.profileData.getIn(["my_users", "0", "user", "id"]) &&
          (i.node.type == "SUPER_ADMIN" || i.node.type == "ADMIN")
      );
      if (Clikindex != -1) {
        setClikEdit(true);
      } else {
        setClikEdit(false);
      }
    }
  });

  function getIconColour(index) {
    if (index != -1) {
      if (
        props.getUserFollowCliksList.getIn([
          index,
          "settings",
          "follow_type"
        ]) == "FAVORITE"
      ) {
        // props.icon("#FADB4A");
      }
      if (
        props.getUserFollowCliksList.getIn([
          index,
          "settings",
          "follow_type"
        ]) == "FOLLOW"
      ) {
        //props.icon("#E1E1E1");
      }
    } else {
      // props.icon("#fff");
    }
  }

  function onClose() {
    setshowRequestInviteModal(false);
    setshowLeaveCliksModal(false);
    setshowInviteUserModal(false);
  }

  function Invite(cliksId, type) {
    const index = props.getUserFollowCliksList.findIndex(
      i =>
        i
          .getIn(["clik", "name"])
          .toLowerCase()
          .replace("clik:", "") == cliksId.toLowerCase().replace("clik:", "")
    );
    if (
      index != -1 &&
      (props.getUserFollowCliksList.getIn([index, "member_type"]) ==
        "SUPER_ADMIN" ||
        props.getUserFollowCliksList.getIn([index, "member_type"]) == "ADMIN" ||
        props.getUserFollowCliksList.getIn([index, "member_type"]) == "MEMBER")
    ) {
      props.members();
      props.showInviteLink(
        true,
        type,
        props.getUserFollowCliksList.getIn([index, "member_type"])
      );
    } else {
      props.members();
      props.showInviteLink(
        false,
        type,
        props.getUserFollowCliksList.getIn([index, "member_type"])
      );
      //setshowRequestInviteModal(true);
    }
  }

  function MemberTab(cliksId, type) {
    props.showMembers(type);
  }

  const openWindow = async link => {
    await openBrowserAsync(link);
  };

  const showAlert = () => {
    if (Platform.OS === "web") {
      var result = confirm(
        "Are you sure you want to delete " +
        cliks
          .get("name")
          .replace("Clik%3A", "#")
          .replace("Clik:", "#")
      );
      if (result == true) {
        applloClient
          .query({
            query: DeleteClikMutation,
            variables: {
              clik_id: cliks.get("id")
            },
            fetchPolicy: "no-cache"
          })
          .then(async res => {
            let resDataLogin = await props.Login();
            await props.saveLoginUser(resDataLogin.data.login);
          });
      } else {
      }
    } else {
      Alert.alert(
        "Are you sure you want to delete " +
        cliks
          .get("name")
          .replace("Clik%3A", "#")
          .replace("Clik:", "#")[
        ({
          text: "NO",
          onPress: () => console.warn("NO Pressed"),
          style: "cancel"
        },
        {
          text: "YES",
          onPress: () => {
            applloClient
              .query({
                query: DeleteClikMutation,
                variables: {
                  clik_id: cliks.get("id")
                },
                fetchPolicy: "no-cache"
              })
              .then(async res => {
                let resDataLogin = await props.Login();
                await props.saveLoginUser(resDataLogin.data.login);
              });
          }
        })
        ]
      );
    }
  };
  const getUserPermision = () => {
    const index = props.listClikMembers.findIndex(
      i =>
        i.node.user.id ==
        props.profileData.getIn(["my_users", "0", "user", "id"])
    );
    if (index != -1) {
      setuserPermision(true);
    } else {
      setuserPermision(false);
    }
  };

  const getUserApprovePermision = () => {
    const index = props.listClikMembers.findIndex(
      i =>
        i.node.user.id ==
        props.profileData.getIn(["my_users", "0", "user", "id"]) &&
        (i.node.type == "SUPER_ADMIN" || i.node.type == "ADMIN")
    );
    if (index != -1) {
      setuserApprovePermision(true);
    } else {
      setuserApprovePermision(false);
    }
  };
  const handleIndexChange = index => {
    setIndex(index);
  };
  return (
    <View
      style={[ButtonStyle.profileShadowStyle, {
        overflow: "hidden",
        borderTopLeftRadius: Dimensions.get("window").width >= 750 ? 20 : 0,
        borderTopRightRadius: Dimensions.get("window").width >= 750 ? 20 : 0,

        // borderBottomLeftRadius: Dimensions.get("window").width <= 750 &&  20,
        // borderBottomRightRadius: Dimensions.get("window").width <= 750 && 20,
        borderBottomColor: Dimensions.get("window").width <= 750 ? "#D7D7D7" : 'transparent',

        borderWidth: 0,
        borderTopColor: "#D7D7D7",
        borderLeftColor: "#D7D7D7",
        borderRightColor: "#D7D7D7",
        backgroundColor: "#fff",
        maxHeight: Dimensions.get("window").height / 2,
        marginHorizontal: Dimensions.get("window").width <= 750 ? 0 : 10,
        marginTop: Dimensions.get("window").width <= 750 ? 0 : 10,
        width: Dimensions.get("window").width <= 750  ? '100%': '98%',
        // Dimensions.get("window").width >= 1100 ? 10 : 10,

        // width: Dimensions.get("window").width >= 750 ? "97%" : "100%",
        paddingVertical: 15,
        paddingRight: 20,
        paddingLeft: 15
      }]}
    >
      {showRequestInviteModal == true ? (
        Platform.OS !== "web" && props.getCurrentDeviceWidthAction < 750 ? (
          <Overlay
            animationType="zoomIn"
            visible={showRequestInviteModal}
            onClose={() => onClose()}
            closeOnTouchOutside
            children={
              <RequestInviteModal
                onClose={() => onClose()}
                {...props}
                ClikInfo={cliks}
              />
            }
            childrenWrapperStyle={{
              padding: 0,
              margin: 0
            }}
          />
        ) : (
          <Modal
            isVisible={showRequestInviteModal}
            onBackdropPress={() => onClose()}
            style={{
              marginHorizontal:
                Dimensions.get("window").width > 750 ? "30%" : 10,
              padding: 0
            }}
          >
            <RequestInviteModal
              onClose={() => onClose()}
              {...props}
              ClikInfo={cliks}
            />
          </Modal>
        )
      ) : null}

      {showInviteUserModal == true ? (
        Platform.OS !== "web" && props.getCurrentDeviceWidthAction < 750 ? (
          <Overlay
            animationType="zoomIn"
            visible={showInviteUserModal}
            onClose={() => onClose()}
            closeOnTouchOutside
            children={
              <InviteUserModal
                onClose={() => onClose()}
                {...props}
                ClikInfo={cliks}
              />
            }
            childrenWrapperStyle={{
              padding: 0,
              margin: 0
            }}
          />
        ) : (
          <Modal
            isVisible={showInviteUserModal}
            onBackdropPress={() => onClose()}
            style={{
              marginHorizontal:
                Dimensions.get("window").width > 750 ? "30%" : 10,
              padding: 0
            }}
          >
            <InviteUserModal
              onClose={() => onClose()}
              {...props}
              ClikInfo={cliks}
            />
          </Modal>
        )
      ) : null}

      {/* <View      > */}
      {/* <ImageBackground
          style={{
            width: props.ProfileHeight - props.feedY < 0 ? 0 : "100%",
            height: Dimensions.get("window").height / 4 - props.feedY,
            backgroundColor: "#fff",
          }}
          source={{
            uri: banner_pic
          }}
        >
          <View
            style={{
              flexDirection: "column",
              justifyContent: "flex-end",
              flex: 1,
              padding: 5,
            }}
          >
            <View
              style={Dimensions.get('window').width <= 750 && {
                flexDirection: 'row',
                justifyContent: 'space-around',
                marginHorizontal: 30
              }}>
              <View
                // onLayout={event => {
                //   let { x, y, width, height } = event.nativeEvent.layout;
                //   setHeight(height);
                // }}
                style={{
                  alignItems: "flex-start",
                  backgroundColor: "transparent",
                  bottom: Dimensions.get('window').width <= 750 ? 2 : -110
                }}
              >
                {cliks && cliks.getIn(["icon_pic"]) && (
                  <Image
                    source={{
                      uri: cliks && cliks.getIn(["icon_pic"])
                    }}
                    style={{
                      height: Dimensions.get('window').width <= 750 ? 60 : 120,
                      width: Dimensions.get('window').width <= 750 ? 60 : 120,
                      padding: 0,
                      margin: 0,
                      borderRadius: 10,
                      borderWidth: 1,
                      borderColor: "#fff"
                    }}
                  />
                )}
              </View>
              <View
                style={{
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "flex-end"
                }}
              >
                {props.isAdmin == true && (
                  <Hoverable>
                    {isHovered => (
                      <Icon
                        color={"#000"}
                        iconStyle={{
                          color: "#fff",
                          justifyContent: "center",
                          alignItems: "center",
                          alignSelf: "center"
                        }}
                        reverse
                        name="trash"
                        type="font-awesome"
                        size={isHovered == true ? 18 : 16}
                        onPress={() => showAlert()}
                      />
                    )}
                  </Hoverable>
                )}
                {ClikEdit == true && (
                  <Hoverable>
                    {isHovered => (
                      <Icon
                        iconStyle={{
                          alignSelf: "center"
                        }}
                        reverse
                        name="edit"
                        type="font-awesome"
                        color={"#000"}
                        size={isHovered == true ? 18 : 16center}
                        onPress={async () => {
                          await props.editClik({
                            clikId: cliks && cliks.getIn(["id"]),
                            cliksName: cliks && cliks.getIn(["name"]),
                            banner_pic: cliks && cliks.getIn(["banner_pic"]),
                            changeBackPicEnable:
                              cliks &&
                              cliks
                                .getIn(["banner_pic"])
                                .split("/")
                              [
                                cliks.getIn(["banner_pic"]).split("/").length -
                                1
                              ].substring(
                                0,
                                cliks
                                  .getIn(["banner_pic"])
                                  .split("/")
                                [
                                  cliks.getIn(["banner_pic"]).split("/")
                                    .length - 1
                                ].indexOf(".")
                              ),
                            icon_pic: cliks && cliks.getIn(["icon_pic"]),
                            changeIconPicEnable:
                              cliks &&
                              cliks
                                .getIn(["icon_pic"])
                                .split("/")
                              [
                                cliks.getIn(["icon_pic"]).split("/").length - 1
                              ].substring(
                                0,
                                cliks
                                  .getIn(["icon_pic"])
                                  .split("/")
                                [
                                  cliks.getIn(["icon_pic"]).split("/")
                                    .length - 1
                                ].indexOf(".")
                              ),
                            description: cliks && cliks.getIn(["description"]),
                            cliksFollowers:
                              cliks && cliks.getIn(["num_followers"]),
                            cliksMembers: cliks && cliks.getIn(["num_members"]),
                            website: cliks && cliks.getIn(["website"]),
                            invite_only: cliks && cliks.getIn(["invite_only"]),
                            qualifications:
                              cliks && cliks.getIn(["qualifications"])
                          });
                          props.navigation.navigate("editclik");
                        }}
                      />
                    )}
                  </Hoverable>
                )}
              </View>

            </View>
          </View>


        </ImageBackground> */}

      {/* <View style={{
        flexDirection:  'row',
        width: '100%',
        // height: Dimensions.get("window").width <= 750 ? 40 : 55,
      }}> */}
      {/* <View
            style={{
              // flexDirection: "column",
              paddingRight: 5,
              paddingLeft: 1,
              position: "absolute",
              bottom: Dimensions.get('window').width <= 750 ? 40 : 60,
              // paddingBottom: 30,
              position: 'relative',
              width: Dimensions.get("window").width <= 750 ? 0 : '15%',
              marginLeft: Dimensions.get('window').width <= 750 ? 20 : 10,
            }}
          >

          </View> */}
      {/* <View
          style={{
            flexDirection: "column",
            // justifyContent: "flex-end",
            // flex: 1,
            backgroundColor: "transparent",
            paddingHorizontal: 10,
            paddingLeft: Dimensions.get("window").width <= 750 ? 0 : 10,
            width: "100%",
          }}
        > */}
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

        <View style={{
          // width: Dimensions.get('window').width <= 750 ? '15%' : '5%',
          //  marginLeft: Dimensions.get('window').width <= 750 ? 10 : 20,
          // justifyContent: 'center', 
          flexDirection: 'row',
          alignItems: 'flex-start'
        }}>
          {cliks && cliks.getIn(["icon_pic"]) && (
            <Image
              source={{
                uri: cliks && cliks.getIn(["icon_pic"])
              }}
              style={{
                height: 80,
                width: 80,
                padding: 0,
                margin: 0,
                borderRadius: 10,
                borderWidth: 1,
                // marginTop: 10,
                borderColor: "#fff",
              }}
            />
          )}
          <View style={{marginLeft:10}}>
            <View
              style={{
                height: 30,
                paddingVertical: 8,
                paddingHorizontal: 3,
                backgroundColor: "#E8F5FA",
                borderRadius: 6,
                alignSelf: "flex-start",
                marginRight: 5,
                justifyContent: "center",
                alignItems: "center",

              }}
            >
              <Text
                style={{
                  color: "#4169e1",
                  fontWeight: "bold",
                  fontSize: 15,
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
              >
                #
                    {cliks &&
                  cliks
                    .get("name")
                    .replace("Clik%3A", "#")
                    .replace("Clik:", "#")}
              </Text>
            </View>

            <View
              style={{
                marginBottom: 10,
                marginTop: 10,
                flexDirection: Dimensions.get('window').width<=750 ? 'column' : "row",
                // paddingLeft: 10,
                alignItems: 'flex-end',
                // width: '100%',
                // paddingLeft:135
              }}
            >
              <View style={{flexDirection:'row'}}>
              {cliks && cliks.get("invite_only") == true && (
                <Icon
                  name="lock"
                  type="font-awesome"
                  color={"grey"}
                  size={15}
                  containerStyle={{ alignItems: "center" }}
                  iconStyle={{
                    marginRight: 5,
                    alignSelf: "center"
                  }}
                />
              )}
              <Text
                style={{
                  color: "grey",
                  fontSize: 12,
                  fontFamily: ConstantFontFamily.defaultFont
                }}
                onPress={() => props.showMembers()}
              >
                {cliksMembers} Members{" "}{"/ "}
                {props.cliksDetails
                  .getIn(["data", "clik"])
                  .getIn(["num_followers"])}{" "}
                Followers
              </Text>
              </View>
              <View style={{flexDirection:'row', alignSelf : 'flex-start', paddingTop:5}}> 
              {website != "" && website != null && (
                <View style={{ alignSelf: "center", marginLeft: 5 }}>
                  <Icon
                    name="link"
                    type="font-awesome"
                    color="grey"
                    size={14}
                  />
                </View>
              )}
              <Hoverable>
                {isHovered => (
                  <Text
                    onPress={() => openWindow(website)}
                    numberOfLines={1}
                    style={{
                      flex: 1,
                      marginLeft: 5,
                      color: "grey",
                      fontSize: 12,
                      fontFamily: ConstantFontFamily.defaultFont,
                      textDecorationLine:
                        isHovered == true ? "underline" : "none"
                    }}
                  >
                    {/* {website != "" &&
                      website != null &&
                      "/ " + website} */}
                      {
                             website != "" &&
                             website != null && website
                                .replace("http://", "")
                                .replace("https://", "")
                                .replace("www.", "")
                                .split(/[/?#]/)[0]
                            }
                  </Text>
                )}
              </Hoverable>
              </View>
            </View>
          </View>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'flex-start'}}>
          {props.loginStatus == 1 &&
            <TouchableOpacity
              onMouseEnter={() => setMenuHover(true)}
              onMouseLeave={() => setMenuHover(false)}
            >
              <Menu>
                <MenuTrigger>
                  <Image
                    source={require("../assets/image/menu.png")}
                    style={{
                      height: 16,
                      width: 16,
                      marginTop: Dimensions.get('window').width <= 750 ? 2 : 0,
                      marginRight: Dimensions.get('window').width <= 750 ? 10 : 20
                    }}
                  />
                </MenuTrigger>
                <MenuOptions
                  optionsContainerStyle={{
                    borderRadius: 6,
                    borderWidth: 1,
                    borderColor: "#e1e1e1",
                    shadowColor: "transparent"
                  }}
                  customStyles={{
                    optionsContainer: {
                      minHeight: 50,
                      width: 150,
                      marginTop: 15,
                    }
                  }}
                >
                  <MenuOption
                    onSelect={() => {
                      // return props.navigation.navigate("settings");
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <Text
                          style={{
                            textAlign: "center",
                            color: isHovered == true ? "#009B1A" : "#000",
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          Follow
                        </Text>
                      )}
                    </Hoverable>
                  </MenuOption>
                  <MenuOption
                    onSelect={() => {
                      MemberTab(cliks && cliks.get("name"), Badge == "icon" ?
                        cliks && cliks.get("invite_only") == true
                          ?
                          "Apply" : "Join" : "Invite")
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <Text
                          style={{
                            textAlign: "center",
                            color: isHovered == true ? "#009B1A" : "#000",
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          {Badge == "icon" ?
                            cliks && cliks.get("invite_only") == true
                              ?
                              "Apply" : "Join" : "Invite"}

                        </Text>
                      )}
                    </Hoverable>
                  </MenuOption>
                  <MenuOption
                    onSelect={async () => {
                      await props.editClik({
                        clikId: cliks && cliks.getIn(["id"]),
                        cliksName: cliks && cliks.getIn(["name"]),
                        banner_pic: cliks && cliks.getIn(["banner_pic"]),
                        changeBackPicEnable:
                          cliks &&
                          cliks
                            .getIn(["banner_pic"])
                            .split("/")
                          [
                            cliks.getIn(["banner_pic"]).split("/").length -
                            1
                          ].substring(
                            0,
                            cliks
                              .getIn(["banner_pic"])
                              .split("/")
                            [
                              cliks.getIn(["banner_pic"]).split("/")
                                .length - 1
                            ].indexOf(".")
                          ),
                        icon_pic: cliks && cliks.getIn(["icon_pic"]),
                        changeIconPicEnable:
                          cliks &&
                          cliks
                            .getIn(["icon_pic"])
                            .split("/")
                          [
                            cliks.getIn(["icon_pic"]).split("/").length - 1
                          ].substring(
                            0,
                            cliks
                              .getIn(["icon_pic"])
                              .split("/")
                            [
                              cliks.getIn(["icon_pic"]).split("/")
                                .length - 1
                            ].indexOf(".")
                          ),
                        description: cliks && cliks.getIn(["description"]),
                        cliksFollowers:
                          cliks && cliks.getIn(["num_followers"]),
                        cliksMembers: cliks && cliks.getIn(["num_members"]),
                        website: cliks && cliks.getIn(["website"]),
                        invite_only: cliks && cliks.getIn(["invite_only"]),
                        qualifications:
                          cliks && cliks.getIn(["qualifications"])
                      });
                      props.navigation.navigate("editclik");
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <Text
                          style={{
                            textAlign: "center",
                            color: isHovered == true ? "#009B1A" : "#000",
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          Edit
                        </Text>
                      )}
                    </Hoverable>
                  </MenuOption>
                  <MenuOption >
                    <Hoverable>
                      {isHovered => (
                        <Text
                          style={{
                            textAlign: "center",
                            color: isHovered == true ? "#009B1A" : "#000",
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          Report
                        </Text>
                      )}
                    </Hoverable>
                  </MenuOption>
                  {props.isAdmin == true &&
                    <MenuOption
                      onSelect={() => { showAlert() }}
                    >
                      <Hoverable>
                        {isHovered => (
                          <Text
                            style={{
                              textAlign: "center",
                              color: isHovered == true ? "#009B1A" : "#000",
                              fontFamily: ConstantFontFamily.MontserratBoldFont
                            }}
                          >
                            Delete
                          </Text>
                        )}
                      </Hoverable>
                    </MenuOption>
                  }
                </MenuOptions>
              </Menu>
            </TouchableOpacity>
          }

          <ClikStar
            MemberType={props.MemberType}
            ClikName={
              props.cliksDetails.getIn(["data", "clik"])
                ? props.cliksDetails.getIn(["data", "clik"]).get("name")
                : ""
            }
            ContainerStyle={{ justifyContent: "center" }}
            ImageStyle={{
              height: 20,
              width: 20,
              alignSelf: "center",
              marginLeft: 5
            }}
          />
        </View>


        {/* </View> */}
        {/* <View style={{ width: Dimensions.get('window').width <= 750 ? '80%' : '95%', paddingLeft: 10, marginTop: 5 }}>

          <View
            style={{
              flexDirection: "row",
              // width: "100%",
              marginTop: Dimensions.get('window').width <= 750 ? 10 : 5,
              alignItems: 'flex-start',
              justifyContent: "space-between",
              // paddingLeft: 120,
              paddingRight: Dimensions.get('window').width <= 750 ? 10 : 20,


            }}
          > */}
        {/* <View style={{ flexDirection: "row", marginTop: Dimensions.get('window').width <= 750 ? 0 : 10 }}>
              {props.loginStatus == 1 &&
                props.showIcon.toString().indexOf("YBadge") != -1 &&
                props.showIcon.toString().startsWith("#") == false && (
                  <Image
                    source={props.showIcon}
                    style={{
                      width: 22,
                      height: 22,
                      marginRight: 5,
                      alignSelf: "center"
                    }}
                  />
                )}

              <View
                style={{
                  height: 30,
                  paddingVertical: 8,
                  paddingHorizontal: 3,
                  backgroundColor: "#E8F5FA",
                  borderRadius: 6,
                  alignSelf: "flex-start",
                  marginRight: 5,
                  justifyContent: "center",
                  alignItems: "center",

                }}
              >
                <Text
                  style={{
                    color: "#4169e1",
                    fontWeight: "bold",
                    fontSize: 15,
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  #
                    {cliks &&
                    cliks
                      .get("name")
                      .replace("Clik%3A", "#")
                      .replace("Clik:", "#")}
                </Text>
              </View>
            </View> */}
        {/* {Dimensions.get("window").width <= 750 ? ( */}
        {/* <View
                style={{
                  flexDirection: "row",
                  // width: "30%",
                  justifyContent: "flex-end"
                }}
              >
                <ClikStar
                  MemberType={props.MemberType}
                  ClikName={
                    props.cliksDetails.getIn(["data", "clik"])
                      ? props.cliksDetails.getIn(["data", "clik"]).get("name")
                      : ""
                  }
                  ContainerStyle={{}}
                  ImageStyle={{
                    height: 20,
                    width: 20,
                    alignSelf: "center",
                    marginLeft: 5
                  }}
                />
              </View> */}
        {/* ) : ( */}
        {/* <View
              style={{
                flexDirection: "row",
                // width: "25%",
                // justifyContent: "flex-end"
              }}
            > */}
        {/* <View
                    style={{
                      // flexDirection: "row",
                      // width: "100%",
                      // justifyContent: "flex-end",
                    }}
                  > */}
        {/* {props.isAdmin == true && (
                    <Hoverable>
                      {isHovered => (
                        <Icon
                          color={"#000"}
                          iconStyle={{
                            color: "#fff",
                            justifyContent: "center",
                            alignItems: "center",
                            alignSelf: "center"
                          }}
                          reverse
                          name="trash"
                          type="font-awesome"
                          size={isHovered == true ? 18 : 16}
                          onPress={() => showAlert()}
                        />
                      )}
                    </Hoverable>
                  )} */}
        {/* {ClikEdit == true && (
                    <Hoverable>
                      {isHovered => (
                        <Icon
                          iconStyle={{
                            alignSelf: "center"
                          }}
                          reverse
                          name="edit"
                          type="font-awesome"
                          color={"#000"}
                          size={isHovered == true ? 18 : 16}
                          onPress={async () => {
                            await props.editClik({
                              clikId: cliks && cliks.getIn(["id"]),
                              cliksName: cliks && cliks.getIn(["name"]),
                              banner_pic: cliks && cliks.getIn(["banner_pic"]),
                              changeBackPicEnable:
                                cliks &&
                                cliks
                                  .getIn(["banner_pic"])
                                  .split("/")
                                [
                                  cliks.getIn(["banner_pic"]).split("/").length -
                                  1
                                ].substring(
                                  0,
                                  cliks
                                    .getIn(["banner_pic"])
                                    .split("/")
                                  [
                                    cliks.getIn(["banner_pic"]).split("/")
                                      .length - 1
                                  ].indexOf(".")
                                ),
                              icon_pic: cliks && cliks.getIn(["icon_pic"]),
                              changeIconPicEnable:
                                cliks &&
                                cliks
                                  .getIn(["icon_pic"])
                                  .split("/")
                                [
                                  cliks.getIn(["icon_pic"]).split("/").length - 1
                                ].substring(
                                  0,
                                  cliks
                                    .getIn(["icon_pic"])
                                    .split("/")
                                  [
                                    cliks.getIn(["icon_pic"]).split("/")
                                      .length - 1
                                  ].indexOf(".")
                                ),
                              description: cliks && cliks.getIn(["description"]),
                              cliksFollowers:
                                cliks && cliks.getIn(["num_followers"]),
                              cliksMembers: cliks && cliks.getIn(["num_members"]),
                              website: cliks && cliks.getIn(["website"]),
                              invite_only: cliks && cliks.getIn(["invite_only"]),
                              qualifications:
                                cliks && cliks.getIn(["qualifications"])
                            });
                            props.navigation.navigate("editclik");
                          }}
                        />
                      )}
                    </Hoverable>
                  )} */}
        {/* </View> */}
        {/* {props.loginStatus == 1 && (
                    <View
                      style={{
                        height: 30,
                        flexDirection: "row",
                        alignSelf: "center",
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      {Badge == "icon" ? (
                        <Button
                          title={
                            cliks && cliks.get("invite_only") == true
                              ? "Apply"
                              : "Join"
                          }
                          buttonStyle={{
                            borderRadius: 15,
                            borderColor: "#4169e1",
                            borderWidth: 2,
                            height: 30,
                            padding: 10,
                            backgroundColor: "#fff"
                          }}
                          titleStyle={{
                            paddingTop: 0,
                            color: "#4169e1",
                            fontFamily: ConstantFontFamily.MontserratBoldFont,
                            fontSize: 15,
                            fontWeight: "bold"
                          }}
                          onPress={() =>
                            MemberTab(
                              cliks && cliks.get("name"),
                              cliks && cliks.get("invite_only") == true
                                ? "Apply"
                                : "Join"
                            )
                          }
                        />
                      ) : (
                          <Button
                            title={"Invite"}
                            buttonStyle={{
                              borderRadius: 15,
                              borderColor: "#4169e1",
                              borderWidth: 2,
                              height: 30,
                              padding: 10,
                              backgroundColor: "#fff"
                            }}
                            titleStyle={{
                              paddingTop: 0,
                              color: "#4169e1",
                              fontFamily: ConstantFontFamily.MontserratBoldFont,
                              fontSize: 15,
                              fontWeight: "bold"
                            }}
                            onPress={() =>
                              MemberTab(cliks && cliks.get("name"), "Invite")
                            }
                          />
                        )}
                    </View>
                  )} */}
        {/* {props.loginStatus == 1 &&
                <TouchableOpacity
                  onMouseEnter={() => setMenuHover(true)}
                  onMouseLeave={() => setMenuHover(false)}
                >
                  <Menu>
                    <MenuTrigger>
                      <Image
                        source={require("../assets/image/menu.png")}
                        style={{
                          height: 16,
                          width: 16,
                          marginTop: Dimensions.get('window').width <= 750 ? 2 : 0,
                          marginRight: Dimensions.get('window').width <= 750 ? 10 : 20
                        }}
                      />
                    </MenuTrigger>
                    <MenuOptions
                      optionsContainerStyle={{
                        borderRadius: 6,
                        borderWidth: 1,
                        borderColor: "#e1e1e1",
                        shadowColor: "transparent"
                      }}
                      customStyles={{
                        optionsContainer: {
                          minHeight: 50,
                          width: 150,
                          marginTop: 15,
                        }
                      }}
                    >
                      <MenuOption
                        onSelect={() => {
                          // return props.navigation.navigate("settings");
                        }}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily: ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Follow
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>
                      <MenuOption
                        onSelect={() => {
                          MemberTab(cliks && cliks.get("name"), Badge == "icon" ?
                            cliks && cliks.get("invite_only") == true
                              ?
                              "Apply" : "Join" : "Invite")
                        }}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily: ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              {Badge == "icon" ?
                                cliks && cliks.get("invite_only") == true
                                  ?
                                  "Apply" : "Join" : "Invite"}

                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>
                      <MenuOption
                        onSelect={async () => {
                          await props.editClik({
                            clikId: cliks && cliks.getIn(["id"]),
                            cliksName: cliks && cliks.getIn(["name"]),
                            banner_pic: cliks && cliks.getIn(["banner_pic"]),
                            changeBackPicEnable:
                              cliks &&
                              cliks
                                .getIn(["banner_pic"])
                                .split("/")
                              [
                                cliks.getIn(["banner_pic"]).split("/").length -
                                1
                              ].substring(
                                0,
                                cliks
                                  .getIn(["banner_pic"])
                                  .split("/")
                                [
                                  cliks.getIn(["banner_pic"]).split("/")
                                    .length - 1
                                ].indexOf(".")
                              ),
                            icon_pic: cliks && cliks.getIn(["icon_pic"]),
                            changeIconPicEnable:
                              cliks &&
                              cliks
                                .getIn(["icon_pic"])
                                .split("/")
                              [
                                cliks.getIn(["icon_pic"]).split("/").length - 1
                              ].substring(
                                0,
                                cliks
                                  .getIn(["icon_pic"])
                                  .split("/")
                                [
                                  cliks.getIn(["icon_pic"]).split("/")
                                    .length - 1
                                ].indexOf(".")
                              ),
                            description: cliks && cliks.getIn(["description"]),
                            cliksFollowers:
                              cliks && cliks.getIn(["num_followers"]),
                            cliksMembers: cliks && cliks.getIn(["num_members"]),
                            website: cliks && cliks.getIn(["website"]),
                            invite_only: cliks && cliks.getIn(["invite_only"]),
                            qualifications:
                              cliks && cliks.getIn(["qualifications"])
                          });
                          props.navigation.navigate("editclik");
                        }}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily: ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Edit
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>
                      <MenuOption >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily: ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Report
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>
                      {props.isAdmin == true &&
                        <MenuOption
                          onSelect={() => { showAlert() }}
                        >
                          <Hoverable>
                            {isHovered => (
                              <Text
                                style={{
                                  textAlign: "center",
                                  color: isHovered == true ? "#009B1A" : "#000",
                                  fontFamily: ConstantFontFamily.MontserratBoldFont
                                }}
                              >
                                Delete
                              </Text>
                            )}
                          </Hoverable>
                        </MenuOption>
                      }
                    </MenuOptions>
                  </Menu>
                </TouchableOpacity>
              }

              <ClikStar
                MemberType={props.MemberType}
                ClikName={
                  props.cliksDetails.getIn(["data", "clik"])
                    ? props.cliksDetails.getIn(["data", "clik"]).get("name")
                    : ""
                }
                ContainerStyle={{ justifyContent: "center" }}
                ImageStyle={{
                  height: 20,
                  width: 20,
                  alignSelf: "center",
                  marginLeft: 5
                }}
              />
            </View> */}
        {/* ) */}
        {/* } */}
        {/* </View> */}
        {/* </View>
           </View> */}

        {/* <View
            style={{
              marginBottom: 10,
              marginTop: 10,
              flexDirection: "row",
              paddingLeft: 10,
              alignItems: "center",
              // width: '100%',
              // paddingLeft:135
            }}
          >
            {cliks && cliks.get("invite_only") == true && (
              <Icon
                name="lock"
                type="font-awesome"
                color={"grey"}
                size={20}
                containerStyle={{ alignItems: "center" }}
                iconStyle={{
                  marginRight: 5,
                  alignSelf: "center"
                }}
              />
            )}
            <Text
              style={{
                color: "grey",
                fontSize: 12,
                fontFamily: ConstantFontFamily.defaultFont
              }}
              onPress={() => props.showMembers()}
            >
              {cliksMembers} Members{" "}{"/ "}
              {props.cliksDetails
                .getIn(["data", "clik"])
                .getIn(["num_followers"])}{" "}
            Followers
            </Text>
            {website != "" && website != null && (
              <View style={{ alignSelf: "center", marginLeft: 5 }}>
                <Icon
                  name="link"
                  type="font-awesome"
                  color="grey"
                  size={14}
                />
              </View>
            )}
            <Hoverable>
              {isHovered => (
                <Text
                  onPress={() => openWindow(website)}
                  numberOfLines={1}
                  style={{
                    flex: 1,
                    marginLeft: 5,
                    color: "grey",
                    fontSize: 12,
                    fontFamily: ConstantFontFamily.defaultFont,
                    textDecorationLine:
                      isHovered == true ? "underline" : "none"
                  }}
                >
                  {website != "" &&
                    website != null &&
                    "/ " + website}
                </Text>
              )}
            </Hoverable>
          </View> */}
        {/* </View> */}
        {/* {Dimensions.get('window').width>=750 &&
          <View
            style={{ paddingHorizontal: 10 }}
            onLayout={event => {
              let { x, y, width, height } = event.nativeEvent.layout;
              setHeight(height);
            }}
          >
            <Text
              style={{
                marginBottom: 10,
                color: "#000",
                fontSize: 13,
                fontFamily: ConstantFontFamily.defaultFont
              }}
            >
              {description}
            </Text>
          </View>} */}
        {/* </View> */}
      </View>
      {/* {Dimensions.get('window').width<=750 && */}
      <View
        onLayout={event => {
          let { x, y, width, height } = event.nativeEvent.layout;
          setHeight(height);
        }}
      >
        <Text
          style={{
            marginVertical: 20,
            color: "#000",
            fontSize: 13,
            fontFamily: ConstantFontFamily.defaultFont,
            textAlign: 'left',
            // paddingHorizontal:20
          }}
        >
          {description}
        </Text>
      </View>
      {/* } */}

    </View>
  );
};

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List(),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  listClikMembers: !state.ClikMembersReducer.get("clikMemberList")
    ? List()
    : state.ClikMembersReducer.get("clikMemberList"),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  cliksDetails: state.TrendingCliksProfileReducer.get(
    "getTrendingCliksProfileDetails"
  )
});

const mapDispatchToProps = dispatch => ({
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  userId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  editClik: payload => dispatch(editClik(payload))
});

const CliksProfileCardContainerWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(CliksProfileCard);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  CliksProfileCardContainerWrapper
);

const styles = StyleSheet.create({
  indicator: {
    backgroundColor: "#009B1A",
    height: 4,
    borderRadius: 6
    // marginHorizontal: "2%"
  },
  tab: {
    backgroundColor: "transparent",
    flex: 1,
    width: "90%",
    height: 30,
    marginLeft: "auto",
    marginRight: "auto"
  },
  label: {
    fontSize: 16,
    fontWeight: "500",
    fontFamily: "Montserrat-Medium",
    letterSpacing: 1,
    width: "auto"
  },
  labelStyle: {
    color: "#000",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  }
});
