import React, { Component } from "react";
import { View, Text, Dimensions, StyleSheet } from "react-native";
import IconMenu from "./IconMenu";
import NavigationService from "../library/NavigationService";
import RNPickerSelect from "react-native-picker-select";
import { connect } from "react-redux";
import { Icon } from "react-native-elements";
import ConstantFontFamily from "../constants/FontFamily";

const roleWiseViewOptionAnalytics = [
  {
    label: "Summary",
    value: "Summary",
  },
  {
    label: "Graphs",
    value: "Graphs",
  },
  {
    label: "Top Comments",
    value: "Top Comments",
  },
  {
    label: "Stripe",
    value: "Stripe",
  },
];

class BottomScreenAnalytics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabType: "Summary",
      tabId:0
    };
  }

  componentDidUpdate(prevState){
    if(prevState.tabType != this.state.tabType){      
      }
  }

  render() {
    return (
      <View
        style={{
          width: "100%",
          backgroundColor:
            Dimensions.get("window").width <= 750 ? "#fff" : "#000",
          height: 50,
          flexDirection: "row",
          justifyContent: 'center',
          alignItems: "center",
          borderTopWidth: 1,
          borderTopColor: "#c5c5c5",
        }}
      >
        {Dimensions.get("window").width <= 750 && (
          // <View style={{ flexDirection: "column", alignItems: "center"}}>
          //   <Text
          //     style={{
          //       fontFamily: ConstantFontFamily.defaultFont,
          //       marginBottom: 1,
          //       fontSize: 13,
          //     }}
          //   >
          //     Page
          //   </Text>
            <View
              style={{
                borderRadius: 8,
                borderColor: "#c5c5c5",
                borderWidth: 1,
                marginBottom: 2,
                width: 125
              }}
            >
              <RNPickerSelect
                placeholder={{}}
                items={                  
                  roleWiseViewOptionAnalytics
                }
                onValueChange={(itemValue, itemIndex) => {
                  this.props.call({key:itemIndex,value:itemValue})
                  this.setState({ tabType: itemValue, tabId:itemIndex }, () => {
                    
                  //this.props.key({key:itemIndex,value:itemValue})
                  this.props.setTabViewAnalytics(this.state.tabType);
                  });
                }}
                value={this.state.tabType}
                // useNativeAndroidPickerStyle={false}
                style={{
                  ...styles,
                }}
              />
            </View>
          // </View>
        )}
      </View>
    );
  }
}
const mapDispatchToProps = (dispatch) => ({
  setTabViewAnalytics: (payload) => dispatch({ type: "SET_TAB_VIEW_ANALYTICS", payload }),  
});
export default connect(null, mapDispatchToProps)(BottomScreenAnalytics);

const styles = StyleSheet.create({
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
    fontSize: 16,
    // fontWeight: "bold",
    fontFamily: ConstantFontFamily.VerdanaFont,
    textAlign: "center",
  },
  inputAndroid: {
    //   width: 145,
    paddingHorizontal: 10,
    paddingVertical: 1,
    borderWidth: 1,
    borderColor: "#fff",
    borderRadius: 20,
    color: "#000",
    backgroundColor: "white",
    fontSize: 16,
    // fontWeight: "bold",
    fontFamily: ConstantFontFamily.VerdanaFont,
    textAlign: "center",
  },
});
