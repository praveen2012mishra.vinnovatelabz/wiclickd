import React, { useState } from "react";
import { Platform, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import AppHelper from "../constants/AppHelper";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";

const CreateWiki = props => {
  let textStyle = "#959FAA";
  const inputRefs = {};
  const [title, settitle] = useState("");
  const [item, setitem] = useState([]);
  const [click, setclick] = useState(null);
  const [showtopictooltip, setshowtopictooltip] = useState(false);

  const submitComment = () => {};

  const goToUserProfile = async (username) => {
    props.onClose();
    await props.userId({
      username: username
    });
    await props.navigation.navigate("profile", {
      username: username,
      type: "profile"
    });
  };
  const goToProfile = id => {
    props.clikId({
      id: id
    });
  };

  return (
    <View
      style={{
        borderRadius: 4,
        overflow: "visible",
        width: "100%",
        backgroundColor: "#fff"
      }}
    >
      <View
        style={{
          height: Platform.OS != "web" ? hp("25%") : null,
          backgroundColor: "#fff"
        }}
      >
        <View style={{ flex: 1 }}>
          <View
            style={{
              flexDirection: "row",
              width: "100%",
              margin: 10
            }}
          >
            <View style={{ flexDirection: "row", width: "50%" }}>
              <TextInput
                //value={this.state.topicName}
                placeholder="Enter Definition name."
                placeholderTextColor="#6D757F"
                style={{
                  color: "#000",
                  fontSize: 14,
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.defaultFont,
                  width: "90%",
                  borderRadius: 6,
                  borderColor: "#e1e1e1",
                  borderWidth: 1,
                  marginTop: 5,
                  height: 45,
                  padding: 5
                }}
                // onChangeText={topicName => this.checkTopicname(topicName)}
              />
            </View>

            <View style={{ flexDirection: "row", width: "50%" }}>
              <TextInput
                // value={this.state.rtopicName}
                placeholder="Enter username."
                placeholderTextColor="#6D757F"
                style={{
                  color: "#000",
                  fontSize: 14,
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.defaultFont,
                  width: "90%",
                  borderRadius: 6,
                  borderColor: "#e1e1e1",
                  borderWidth: 1,
                  marginTop: 5,
                  height: 45,
                  padding: 5
                }}
                //onChangeText={rtopicName => this.checkRTopicname(rtopicName)}
              />
            </View>
          </View>

          <View
            style={{
              borderWidth: 1,
              borderColor: "#e8e8e8",
              borderRadius: 6,
              ...Platform.select({
                web: {
                  marginHorizontal: 10
                },
                android: {
                  marginTop: 10
                },
                ios: {
                  marginTop: 10
                }
              })
            }}
          >
            <TextInput
              value={title}
              multiline={true}
              numberOfLines={Platform.OS === "ios" ? null : 4}
              placeholder="Write an wiki."
              style={{
                padding: 5,
                color: "#000",
                fontFamily: ConstantFontFamily.defaultFont,
                fontSize: 14,
                width: "100%",
                borderRadius: 6,
                minHeight: Platform.OS === "ios" && 4 ? 10 * 4 : null
              }}
              onChangeText={title => settitle(title)}
            />
          </View>
          <View
            style={{
              flexDirection: "row",
              width: "100%",
              marginVertical: 10,
              paddingHorizontal: 10,
              alignItems: "center"
            }}
          >
            <View
              style={{
                flexDirection: "row",
                width: "50%",
                justifyContent: "flex-start"
              }}
            >
              <View
                style={{
                  alignItems: "flex-start",
                  justifyContent: "flex-start",
                  padding: 5
                }}
              >
                {props.profileData.getIn([
                  "my_users",
                  "0",
                  "user",
                  "full_name"
                ]) !=
                  props.profileData.getIn([
                    "my_users",
                    "0",
                    "user",
                    "username"
                  ]) && (
                  <TouchableOpacity
                    onPress={() =>
                      goToUserProfile(
                        
                        props.profileData.getIn([
                          "my_users",
                          "0",
                          "user",
                          "username"
                        ])
                      )
                    }
                  >
                    <Text
                      style={{
                        color: "#959FAA",
                        fontWeight: "bold",
                        fontSize: 13,
                        fontFamily: ConstantFontFamily.defaultFont
                      }}
                    >
                      {props.profileData.getIn([
                        "my_users",
                        "0",
                        "user",
                        "full_name"
                      ])}
                    </Text>
                  </TouchableOpacity>
                )}
                <Hoverable>
                  {isHovered => (
                    <TouchableOpacity
                      onPress={() =>
                        goToUserProfile(
                          
                          props.profileData.getIn([
                            "my_users",
                            "0",
                            "user",
                            "username"
                          ])
                        )
                      }
                    >
                      <Text
                        style={{
                          color: "#959FAA",
                          fontSize: 13,
                          fontFamily: ConstantFontFamily.defaultFont,
                          textDecorationLine:
                            isHovered == true ? "underline" : "none"
                        }}
                      >
                        @
                        {props.profileData.getIn([
                          "my_users",
                          "0",
                          "user",
                          "username"
                        ])}
                      </Text>
                    </TouchableOpacity>
                  )}
                </Hoverable>
              </View>
            </View>

            <View
              style={{
                flexDirection: "row",
                width: "50%",
                justifyContent: "flex-end",
                alignItems: "center",
                alignSelf: "center"
              }}
            >
              <TouchableOpacity
                style={{
                  backgroundColor: "#de5246",
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: 6,
                  borderColor: "#000",
                  height: 35,
                  marginRight: 10
                }}
                onPress={() => {
                  props.onWikiClose(false);
                }}
              >
                <Text
                  style={{
                    color: "#fff",
                    borderRadius: 6,
                    marginHorizontal: 30,
                    marginVertical: 10,
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  Cancel
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  backgroundColor:
                    (props.initial == "main" && title.length <= 50) ||
                    (props.initial != "main" && title.length <= 25)
                      ? "#e1e1e1"
                      : "#000",
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: 6,
                  borderColor: "#000",
                  height: 35
                }}
                onPress={() => submitComment()}
                disabled={
                  (props.initial == "main" && title.length <= 50) ||
                  (props.initial != "main" && title.length <= 25)
                    ? true
                    : false
                }
              >
                <Text
                  style={{
                    color: "#fff",
                    borderRadius: 6,
                    marginHorizontal: 30,
                    marginVertical: 10,
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  Submit
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};
const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails")
});

const mapDispatchToProps = dispatch => ({
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  clikId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  CreateWiki
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ConstantColors.customeBackgroundColor,
    padding: Platform.OS == "web" ? 10 : 0
  },
  totalFollowCount: {
    marginTop: 15,
    color: "#ffffff",
    alignItems: "center"
  },
  containerFollow: {
    justifyContent: "flex-end",
    marginRight: 10
  },
  TotalcontainerFollow: {
    flexDirection: "row",
    justifyContent: "flex-end",
    marginRight: 12,
    marginBottom: 10
  },
  profileContainer: {
    marginTop: 10
  },
  totalProfileContainer: {
    alignItems: "center",
    justifyContent: "center"
  },
  image: {
    width: "100%",
    height: hp("40%")
  },
  followbtnStyle: {
    height: 30,
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#1A1819",
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 30,
    paddingRight: 30,
    borderColor: "#fff",
    borderWidth: 2,
    borderRadius: 5
  },
  bgContainerProfile: {
    margin: 0,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  usertext: {
    color: "#fff",
    fontSize: 12,
    fontFamily: ConstantFontFamily.defaultFont
  },
  userblacktext: {
    color: "#fff",
    fontSize: 12,
    fontFamily: ConstantFontFamily.defaultFont
  },
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    //borderWidth: 1,
    //  borderColor: "#4C82B6",
    borderRadius: 4,
    backgroundColor: "#E8F5FA",
    color: "#4169e1",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  inputAndroid: {
    paddingVertical: 8,
    //borderWidth: 1,
    // borderColor: "#4C82B6",
    borderRadius: 8,
    backgroundColor: "#E8F5FA",
    color: "#4169e1",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  TextStyle: {
    textAlign: "center"
  }
});
