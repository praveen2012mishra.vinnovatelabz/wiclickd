import "@expo/browser-polyfill";
import { openBrowserAsync } from "expo-web-browser";
import moment from "moment";
import React, { useEffect, useState } from "react";
import {
  Image,
  ImageBackground,
  Platform,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  Clipboard,
  Dimensions
} from "react-native";
import { Icon } from "react-native-elements";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { listClikMembers } from "../actionCreator/ClikMembersAction";
import { listClikUserRequest } from "../actionCreator/ClikUserRequestAction";
import { setFEEDREPORTMODALACTION } from "../actionCreator/FeedReportModalAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { listTopicFeed } from "../actionCreator/TopicsFeedAction";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { getTrendingTopicsProfileDetails } from "../actionCreator/TrendingTopicsProfileAction";
import ChangeIcon from "../components/ChangeIcon";
import AppHelper from "../constants/AppHelper";
import ConstantFontFamily from "../constants/FontFamily";
import { capitalizeFirstLetter } from "../library/Helper";
import { postEditDetails } from "../actionCreator/LinkPostAction";
import { rootTopics } from "../constants/RootTopics";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger
} from "react-native-popup-menu";
import ButtonStyle from "../constants/ButtonStyle";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import FeedImageDisplayUser from "./FeedImageDisplayUser";
import FeedImageDisplay from "./FeedImageDisplayClick";

const PostDetailscard = props => {
  let singleItem = props.item.post;
  let bgImage = {
    uri: singleItem.thumbnail_pic
  };
  let profileImage = {
    uri: singleItem.author ? singleItem.author.profile_pic : null
  };
  let username = singleItem.author ? singleItem.author.username : null;
  let full_name = singleItem.author ? singleItem.author.full_name : null;
  let text = singleItem.text;
  let summary = singleItem.summary;
  let title = singleItem.title;
  let link = singleItem.link;
  let external_feed = singleItem.external_feed;
  const [modalVisible, setmodalVisible] = useState(false);
  const [outSideClick, setoutSideClick] = useState(false);
  const [parent_content_id, setparent_content_id] = useState(" ");
  let [MainHeight, setMainHeight] = useState(0);
  let cliksNameList = singleItem.cliks;
  let [showBug, setshowBug] = useState(false);
  let [pressBug, setpressBug] = useState(false);
  let [pressComment, setpressComment] = useState(false);
  let [showMenu, setshowMenu] = useState(Platform.OS == "web" ? false : true);
  let [MenuHover, setMenuHover] = useState(false);
  let [openCreateComment, setOpenCreateComment] = useState(false);
  moment.updateLocale("en", {
    relativeTime: {
      future: "in %s",
      past: "%s ago",
      s: "a few seconds",
      ss: "%d seconds",
      m: "1 minutes",
      mm: "%d minutes",
      h: "1 hour",
      hh: "%d hours",
      d: "1 day",
      dd: "%d days",
      w: "1 week",
      ww: "%d weeks",
      M: "1 month",
      MM: "%d months",
      y: "1 year",
      yy: "%d years"
    }
  });
  const onClose = () => {
    setmodalVisible(false);
    setpressComment(false);
  };

  const onSubmit = (position, data) => {
    setmodalVisible(false);
    props.closeModalhandalListMode(position, data);
  };
  async function onOpen(id) {
    //console.log(id,'---------------------------------------------->');
    if (props.loginStatus == 0) {
      props.setLoginModalStatus(true);
      return false;
    }
    setmodalVisible(true);
    setparent_content_id(id);
    setoutSideClick(true);
    props.clickEvent(true);
    setOpenCreateComment(!openCreateComment)
    props.openCreateComment(!openCreateComment)
  }

  function loginModalStatusEventParent(val) {
    setmodalVisible(val);
  }
  async function openWindow(link) {
    let result = await openBrowserAsync(link);
  }

  const handleClick = e => {
    setshowBug(e);
  };

  const goToClikProfile = id => {
    props.clikId({
      id: id,
      type: "feed"
    });
    props.setClikUserRequest({
      id: id.replace("%3A", ":"),
      currentPage: AppHelper.PAGE_LIMIT
    });
    props.setClikMembers({
      id: id.replace("%3A", ":")
    });
  };

  async function goToProfile(id) {
    props.topicId({
      id: id,
      type: "feed"
    });
    await props.listTopicFeed({
      id: id,
      currentPage: AppHelper.PAGE_LIMIT
    });
  }
  const handleBugClick = e => {
    props.setFeedReportModalAction(true);
    setpressBug(true);
  };

  const isUpperCase = string => /^[A-Z-]*$/.test(string);

  const getHeart = liketype => {
    switch (liketype) {
      case "RED":
        return [1, 0, 0, 0];
      case "SILVER":
        return [0, 1, 0, 0];
      case "GOLD":
        return [0, 0, 1, 0];
      case "DIAMOND":
        return [0, 0, 0, 1];
      default:
        return [0, 0, 0, 0];
    }
  };

  const getColour = score => {
    return "#969faa";
    // if (score == 0) {
    //   return "#969faa";
    // } else if (0 < score && score <= 25) {
    //   return "#de5246";
    // } else if (25 < score && score <= 50) {
    //   return "#b0b0b0";
    // } else if (50 < score && score <= 75) {
    //   return "#ebca44";
    // } else {
    //   return "#8bbaf9";
    // }
  };

  useEffect(() => {
    
    let data = document.getElementById(singleItem.id);
    if (data != null) {
      data.addEventListener("keydown", event => {
        if (event.keyCode == 82) {
          props.loginStatus == 1
            ? handleBugClick()
            : props.setSignUpModalStatus(true);
        }
        if (event.keyCode == 67) {
          onOpen(singleItem.id);
        }
      });
    }
    //l=76 r=82 c=67
  },[]);

  useEffect(() => {
    if (props.modalStatus != pressComment) {
      setpressComment(props.modalStatus);
    }
  }, [props.modalStatus]);

  const writeToClipboard = async () => {
    let Domain = window.location.href
      .replace("http://", "")
      .replace("https://", "")
      .replace("www.", "")
      .split(/[/?#]/)[0];
    let PostId = "/post/" + singleItem.id.replace("Post:", "");
    await Clipboard.setString(
      Domain.startsWith("localhost") == true
        ? "http://" + Domain + PostId
        : "https://" + Domain + PostId
    );
  };

  const goToUserProfile = async username => {
    await props.userId({
      username: username,
      type: "feed"
    });
    await props.navigation.navigate("profile", {
      username: username,
      type: "feed"
    });
  };

  return (
    <View
      onMouseEnter={() => setshowMenu(true)}
      onMouseLeave={() =>
        MenuHover == true ? setshowMenu(true) : setshowMenu(false)
      }
      nativeID={singleItem.id}
      style={[
        ButtonStyle.cardBorderStyle, 
        ButtonStyle.profileShadowStyle ,
        {
          backgroundColor: "#fff",
          minHeight: MainHeight,
          marginHorizontal: Dimensions.get('window').width <= 750 ? 0: 10,
          // marginTop: Dimensions.get('window').width <= 750 ? 10 : 0,
          width :  Dimensions.get('window').width <= 750 ? '100%' :'97%', 
          borderRadius :  Dimensions.get('window').width <= 750 ? 0 :20
        }
      ]}
    >
      {/* <TouchableOpacity
        style={{
          borderRadius: 20,
          overflow: Platform.OS == "web" ? "hidden" : "visible",
        }}
      >
        {singleItem.thumbnail_pic != null && (
          <ImageBackground
            style={{
              height: hp("40%")
            }}
            source={bgImage}
          ></ImageBackground>
        )}
      </TouchableOpacity> */}

      <View
        style={{
          flex: 1,
          marginHorizontal: 40,
          marginTop: singleItem.thumbnail_pic == null ? 20 : 10,
          justifyContent: "center"
        }}
      >
        {link == null ||
          (link == "" && (
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignSelf: "center",
                marginVertical: 10
              }}
            >
              <View style={{ flexDirection: "row" }}>
                {singleItem.author && singleItem.author.profile_pic ? (
                  <Image
                    source={profileImage}
                    style={{
                      height: 34,
                      width: 34,
                      padding: 0,
                      margin: 0,
                      borderRadius: 17,
                      borderWidth: 1,
                      borderColor: "#fff"
                    }}
                  />
                ) : (
                    <Image
                      source={require("../assets/image/default-image.png")}
                      style={{
                        height: 34,
                        width: 34,
                        padding: 0,
                        margin: 0,
                        borderRadius: 17,
                        borderWidth: 1,
                        borderColor: "#fff"
                      }}
                    />
                  )}
                <View style={{ padding: 5 }}>
                  {full_name && (
                    <Text
                      style={{
                        color: "#000",
                        fontWeight: "bold",
                        fontSize: 15,
                        fontFamily: ConstantFontFamily.defaultFont
                      }}
                    >
                      {full_name}
                    </Text>
                  )}
                  <Text
                    style={{
                      color: "#000",
                      fontSize: 13,
                      fontFamily: ConstantFontFamily.defaultFont
                    }}
                  >
                    @{username}
                  </Text>
                </View>
              </View>
            </View>
          ))}
      </View>

      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          flexWrap: "wrap",
          width: "100%",
          marginBottom: 10,
          paddingHorizontal: 10
        }}
      >
        {/* <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-start",
            flexWrap: "wrap"
          }}
        >
          {props.PostDetails.post.topics &&
            props.PostDetails.post.topics.map((t, i) => {
              return (
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    // marginLeft: 5,
                    marginTop: 5,
                    padding: 3,
                    backgroundColor: "#e3f9d5",
                    borderRadius: 6
                  }}
                  key={i}
                >
                  <Hoverable>
                    {isHovered => (
                      <TouchableHighlight
                        onPress={() =>
                          goToProfile(capitalizeFirstLetter(t.toLowerCase()))
                        }
                      >
                        <Text
                          style={{
                            color: "#009B1A",

                            fontFamily: ConstantFontFamily.MontserratBoldFont,
                            fontWeight: "bold",
                            fontSize: 14,
                            textDecorationLine:
                              isHovered == true ? "underline" : "none"
                          }}
                        >
                          /{t.toLowerCase()}
                        </Text>
                      </TouchableHighlight>
                    )}
                  </Hoverable>
                </View>
              );
            })}

          {props.PostDetails.post.cliks &&
            props.PostDetails.post.cliks.map((t, i) => {
              return (
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    marginLeft: 5,
                    marginTop: 5,
                    padding: 3,
                    backgroundColor: "#E8F5FA",
                    borderRadius: 6
                  }}
                  key={i}
                >
                  <Hoverable>
                    {isHovered => (
                      <TouchableHighlight onPress={() => goToClikProfile(t)}>
                        <Text
                          style={{
                            color: "#4169e1",
                            fontFamily: ConstantFontFamily.MontserratBoldFont,
                            fontWeight: "bold",
                            fontSize: 14,
                            textDecorationLine:
                              isHovered == true ? "underline" : "none"
                          }}
                        >
                          #{t}
                        </Text>
                      </TouchableHighlight>
                    )}
                  </Hoverable>
                </View>
              );
            })}
        </View> */}
        {/* <View
          style={{
            flexDirection: "row"
          }}
        >
          <TouchableOpacity
            onMouseEnter={() => setMenuHover(true)}
            onMouseLeave={() => setMenuHover(false)}
          >
            <Menu>
              <MenuTrigger>
                <Image
                  source={require("../assets/image/menu.png")}
                  style={{
                    height: 16,
                    width: 16,
                    alignSelf: "flex-end",
                    marginTop: 5,
                    paddingRight: 12,
                    transform: [{ rotate: "90deg" }]
                  }}
                />
              </MenuTrigger>
              <MenuOptions
                optionsContainerStyle={{
                  borderRadius: 6,
                  borderWidth: 1,
                  borderColor: "#e1e1e1",
                  shadowColor: "transparent"
                }}
                customStyles={{
                  optionsContainer: {
                    minHeight: 50,
                    width: 150,
                    marginTop: 15,
                    marginLeft: 5
                  }
                }}
              >
                <MenuOption
                  onSelect={() => {
                    props.loginStatus == 1
                      ? handleBugClick()
                      : props.setLoginModalStatus(true);
                  }}
                >
                  <Hoverable>
                    {isHovered => (
                      <Text
                        style={{
                          textAlign: "center",
                          color: isHovered == true ? "#009B1A" : "#000",
                          fontFamily: ConstantFontFamily.MontserratBoldFont
                        }}
                      >
                        Report
                      </Text>
                    )}
                  </Hoverable>
                </MenuOption>

                <MenuOption onSelect={() => writeToClipboard()}>
                  <Hoverable>
                    {isHovered => (
                      <Text
                        style={{
                          textAlign: "center",
                          color: isHovered == true ? "#009B1A" : "#000",
                          fontFamily: ConstantFontFamily.MontserratBoldFont
                        }}
                      >
                        Copy Link
                      </Text>
                    )}
                  </Hoverable>
                </MenuOption>
              </MenuOptions>
            </Menu>
          </TouchableOpacity>
        </View> */}
      </View>

      <View style={{ flexDirection: "row", width: "100%" }}>
        <View
          style={{
            width:
              singleItem.reasons_shared != null &&
                singleItem.reasons_shared.cliks.length > 0
                ? "20%"
                : "0%"
          }}
        >
          {singleItem.reasons_shared && (
            <FeedImageDisplay
              item={singleItem.reasons_shared.cliks}
            />
          )}
        </View>
        <View style={{
          width:
            singleItem.reasons_shared != null &&
              singleItem.reasons_shared.cliks.length > 0
              ? "80%"
              : "100%",
        }}>
          <Text
            style={{
              // color: "#000",
              // fontWeight: "bold",
              // fontSize: 18,
              // fontFamily: ConstantFontFamily.MontserratBoldFont,
              // textAlign: "center"
              textAlign: "center",
              color: "#000",
              paddingHorizontal: 10,
              fontFamily: ConstantFontFamily.VerdanaFont,
              fontSize: 16,
              // height: titleHeight,              
              overflow: 'hidden'
            }}
          >
            {title}
          </Text>
        </View>
      </View>
      <View style={{ flexDirection: "row", width: "100%" }}>
        <View
          style={{
            width:
              singleItem.reasons_shared != null &&
                singleItem.reasons_shared.users.length > 0
                ? "20%"
                : "0%"
          }}
        >
          {singleItem.reasons_shared && (
            <FeedImageDisplayUser
              item={singleItem.reasons_shared.users}
            />
          )}
        </View>
        <View
          onLayout={event => {
            let { x, y, width, height } = event.nativeEvent.layout;
            setMainHeight(height);
          }}
          style={{
            width:
              singleItem.reasons_shared &&
                singleItem.reasons_shared.users.length > 0
                ? "80%"
                : "100%",
          }}
        >
          <Text
            style={{
              // color: "#000",
              // fontFamily: ConstantFontFamily.defaultFont,
              // fontSize: 14,
              // marginTop: 10,
              // paddingHorizontal: 10
              color: "#000",
              paddingHorizontal: 10,
              fontFamily: ConstantFontFamily.VerdanaFont,
              fontSize: 13,
              lineHeight: 20,
              // height: summaryHeight,
              marginTop: 10,
              overflow: 'hidden'
            }}
          >
            {summary}
          </Text>
        </View>
      </View>
      <Text
        style={{
          color: "#000",
          fontFamily: ConstantFontFamily.defaultFont,
          fontSize: 14,
          marginTop: 10,
          paddingHorizontal: 10
        }}
      >
        {text}
      </Text>

      <View
        style={{
          flexDirection: "row",
          width: "100%",
          paddingHorizontal: 10,
          paddingBottom: 7
        }}
      >
        {showBug == true && props.isAdmin == false ? (
          <View
            style={{
              width: "80%",
              flexDirection: "row",
              justifyContent: "flex-start",
              zIndex: 5
            }}
          >
            <Icon
              color={pressBug == true ? "#449733" : "#969faa"}
              name={"bug"}
              type="font-awesome"
              size={20}
              iconStyle={{ alignSelf: "center" }}
              containerStyle={{ alignSelf: "center" }}
              onPress={() =>
                props.loginStatus == 1
                  ? handleBugClick()
                  : props.setSignUpModalStatus(true)
              }
            />
          </View>
        ) : (
            <View
              style={{
                flexDirection: "row",
                width: props.isAdmin ? "60%" : "80%"
              }}
            >
              {link == null || link == "" ? (
                <View
                  style={{
                    flexDirection: "row"
                  }}
                >
                  <View style={{ padding: 5 }}>
                    <Hoverable>
                      {isHovered => (
                        <Text
                          style={{
                            color: "#959FAA",
                            fontSize: 13,
                            fontFamily: ConstantFontFamily.defaultFont,
                            textDecorationLine:
                              isHovered == true ? "underline" : "none"
                          }}
                          onPress={() => goToUserProfile(username)}
                        >
                          @{username}
                        </Text>
                      )}
                    </Hoverable>
                  </View>
                </View>
              ) : (
                  <Hoverable>
                    {isHovered => (
                      <View
                        style={{
                          flexDirection: "row"
                        }}
                      >
                        {link != null && link != "" ? (
                          external_feed != null &&
                            external_feed.icon_url != null ? (
                              <TouchableOpacity
                                style={{
                                  alignSelf: "center"
                                }}
                                onPress={() =>
                                  goToFeedProfile(
                                    external_feed.id.replace("ExternalFeed:", "")
                                  )
                                }
                              >
                                <Image
                                  source={{
                                    uri: external_feed.icon_url
                                  }}
                                  style={{
                                    width: 20,
                                    height: 20,
                                    borderRadius: 10
                                  }}
                                />
                              </TouchableOpacity>
                            ) : (
                              <Icon
                                name="link"
                                type="font-awesome"
                                color="#6D757F"
                                size={12}
                                iconStyle={{ alignSelf: "center" }}
                                containerStyle={{ alignSelf: "center" }}
                              />
                            )
                        ) : null}
                        <TouchableOpacity
                          style={{
                            alignSelf: "center",
                            marginLeft: 5
                          }}
                          onPress={() => openWindow(link)}
                        >
                          <Text
                            style={{
                              textAlign: "center",
                              alignSelf: "center",
                              color: "#6D757F",
                              fontSize: 13,
                              fontFamily: ConstantFontFamily.defaultFont,
                              textDecorationLine:
                                isHovered == true ? "underline" : "none"
                            }}
                          >
                            {
                              link
                                .replace("http://", "")
                                .replace("https://", "")
                                .replace("www.", "")
                                .split(/[/?#]/)[0]
                            }
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                  </Hoverable>
                )}
              {/* <View style={{ alignSelf: "center", paddingLeft: 15 }}>
              <Text
                style={{
                  color: "#959FAA",
                  fontSize: 12,
                  fontFamily: ConstantFontFamily.defaultFont,
                  alignSelf: "center",
                  textAlign: "center"
                }}
              >
                {moment
                  .utc(props.PostDetails.post.created)
                  .local()
                  .fromNow()}
              </Text>
            </View> */}
            </View>
          )}
        {props.isAdmin ? (
          <View
            style={{
              width: "40%",
              flexDirection: "row",
              //padding: 3,
              justifyContent: "space-between"
            }}
          >
            <TouchableOpacity
              onMouseEnter={() => setMenuHover(true)}
              onMouseLeave={() => setMenuHover(false)}
            >
              <Menu>
                <MenuTrigger>
                  <Image
                    source={require("../assets/image/menu.png")}
                    style={{
                      height: 16,
                      width: 16,
                      alignSelf: "flex-end",
                      marginTop: 5,
                      paddingRight: 12,
                      transform: [{ rotate: "180deg" }]
                    }}
                  />
                </MenuTrigger>
                <MenuOptions
                  optionsContainerStyle={{
                    borderRadius: 6,
                    borderWidth: 1,
                    borderColor: "#e1e1e1",
                    shadowColor: "transparent"
                  }}
                  customStyles={{
                    optionsContainer: {
                      minHeight: 50,
                      width: 150,
                      marginTop: 15,
                      marginLeft: 5
                    }
                  }}
                >
                  <MenuOption
                    onSelect={() => {
                      props.loginStatus == 1
                        ? props.setPostEditDetails(props.PostDetails.post)
                        : props.setLoginModalStatus(true);
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <Text
                          style={{
                            textAlign: "center",
                            color: isHovered == true ? "#009B1A" : "#000",
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          Edit
                        </Text>
                      )}
                    </Hoverable>
                  </MenuOption>

                  <MenuOption
                    onSelect={() => {
                      props.loginStatus == 1
                        ? handleBugClick()
                        : props.setLoginModalStatus(true);
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <Text
                          style={{
                            textAlign: "center",
                            color: isHovered == true ? "#009B1A" : "#000",
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          Report
                        </Text>
                      )}
                    </Hoverable>
                  </MenuOption>

                  <MenuOption onSelect={() => writeToClipboard()}>
                    <Hoverable>
                      {isHovered => (
                        <Text
                          style={{
                            textAlign: "center",
                            color: isHovered == true ? "#009B1A" : "#000",
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          Copy Link
                        </Text>
                      )}
                    </Hoverable>
                  </MenuOption>
                </MenuOptions>
              </Menu>
            </TouchableOpacity>
            {/* <TouchableOpacity>
              <Icon
                name={"pencil-square-o"}
                color={"#969faa"}
                type="font-awesome"
                size={20}
                iconStyle={{ alignSelf: "center", marginTop: 2 }}
                containerStyle={{ alignSelf: "center" }}
                onPress={() => {
                  props.setPostEditDetails(props.PostDetails.post);
                }}
              />
            </TouchableOpacity> */}


            <TouchableOpacity
              style={{
                flexDirection: "row"
              }}
              nativeID={"PostDetailsCardComment"}
              onPress={() => onOpen(singleItem.id)}
            >
              <Icon
                color={getColour(props.item.post.comments_score)}
                name={"comment-o"}
                type="font-awesome"
                size={20}
                iconStyle={{ alignSelf: "center", marginBottom: 2 }}
                containerStyle={{ alignSelf: "center" }}
              />
              <Text
                style={{
                  alignSelf: "center",
                  fontSize: 13,
                  fontFamily: ConstantFontFamily.VerdanaFont,
                  paddingHorizontal: 3,
                  color: "#6D757F",
                }}
              >
                {props.item.post.admin_stats &&
                  props.item.post.admin_stats.num_comments}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity style={{ flexDirection: "row",alignItems:'flex-end' }}>
              <ChangeIcon
                PostId={singleItem.id}
                selectedIconList={props.item.post.user_like_type}
                score={props.item.post.likes_score}
                loginModalStatus={loginModalStatusEventParent}
                onOpen={status => handleClick(status)}
                showStatus={showBug}
                pressBug={pressBug}
              />
              <Text
                style={{
                  //fontWeight: "bold",
                  fontSize: 20,
                  paddingHorizontal: 3,
                  color: "#6D757F",
                  fontSize: 13,
                  fontFamily: ConstantFontFamily.VerdanaFont,
                  alignSelf: 'center'
                }}
              >
                {props.item.post.admin_stats &&
                  props.item.post.admin_stats.num_likes}
              </Text>
            </TouchableOpacity>
          </View>
        ) : (
            <View
              style={{
                width: "20%",
                flexDirection: "row",
                flex: 1,
                justifyContent: "flex-end",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                onPress={() => onOpen(singleItem.id)}
                nativeID={"PostDetailsCardComment"}
                style={{
                  marginRight: 5
                }}
              >
                <Icon
                  color={getColour(props.item.post.comments_score)}
                  name={"comment-o"}
                  type="font-awesome"
                  size={20}
                  iconStyle={{ alignSelf: "center", marginBottom: 2 }}
                  containerStyle={{ alignSelf: "center" }}
                />
              </TouchableOpacity>

              <ChangeIcon
                PostId={singleItem.id}
                selectedIconList={props.item.post.user_like_type}
                score={props.item.post.likes_score}
                loginModalStatus={loginModalStatusEventParent}
                onOpen={status => handleClick(status)}
                showStatus={showBug}
                pressBug={pressBug}
              />
            </View>
          )}
      </View>
    </View>
  );
};

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  PostDetails: state.PostDetailsReducer.get("PostDetails"),
  comment: state.PostDetailsReducer.get("comment"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView")
});

const mapDispatchToProps = dispatch => ({
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setFeedReportModalAction: payload =>
    dispatch(setFEEDREPORTMODALACTION(payload)),
  topicId: payload => dispatch(getTrendingTopicsProfileDetails(payload)),
  listTopicFeed: payload => dispatch(listTopicFeed(payload)),
  clikId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  setClikUserRequest: payload => dispatch(listClikUserRequest(payload)),
  setClikMembers: payload => dispatch(listClikMembers(payload)),
  setPostEditDetails: payload => dispatch(postEditDetails(payload)),
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  openCreateComment: (payload) =>
    dispatch({ type: "OPEN_CREATE_COMMENT", payload }),
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  PostDetailscard
);
