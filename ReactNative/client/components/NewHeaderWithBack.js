import React, { Component } from "react";
import { Image, Platform, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import NavigationService from "../library/NavigationService";
import BackButton from "../components/BackButton";
import { Icon } from "react-native-elements";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import HeaderRight from '../components/HeaderRight';
import ConstantFontFamily from "../constants/FontFamily";


class NewHeaderWithBack extends Component {
  constructor(props) {
    super(props);
  }

  goToScrollTop = () => {
    this.props.setHASSCROLLEDACTION(true);
  };

  render() {
    return (
      <View
        style={{
          flexDirection: "row"
        }}
      >
        <HeaderRight />
        {this.props.getCurrentDeviceWidthAction > 750 ? (
          <View
            style={{
              width: "100%",
              flexDirection: "row"
            }}
          >
            <View
              style={{
                width: "38%",
                height: 60,
                backgroundColor: "#fff",
                justifyContent: "center"
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  marginLeft: '35%'
                }}
              >
                <View
                  style={{
                    flexDirection: "row"
                  }}
                >
                  <View
                    style={{
                      marginLeft: 10,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => NavigationService.navigate("home")}
                    >
                      <Image
                        source={
                          this.props.getCurrentDeviceWidthAction > 750
                            ? require("../assets/image/weclickd-logo-beta-white.png")
                            : Platform.OS == "web"
                            ? require("../assets/image/weclickd-logo.png")
                            : require("../assets/image/weclickd-logo-only-icon.png")
                        }
                        style={
                          this.props.getCurrentDeviceWidthAction > 750
                            ? { height: 30, width: 120 }
                            : {
                                height: 30,
                                width: Platform.OS == "web" ? 90 : 30
                              }
                        }
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
            <View
              style={{
                width: "49%",
                height: 60,
                backgroundColor: "#000",
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  alignSelf: 'center'
                }}
              >
                <Icon
                  color={"#fff"}
                  iconStyle={{ paddingLeft: 10 }}
                  onPress={() => this.props.navigation.goBack()}
                  name="angle-left"
                  type="font-awesome"
                  size={40}
                />
              </View>
              <TouchableOpacity
                onPress={() => this.goToScrollTop()}
                style={{
                  width: "100%",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontWeight: "bold",
                    fontSize: 18,
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  {this.props.customeTitle}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : (
          <View
            style={{
              width: "100%",
              flexDirection: "row",
              backgroundColor: "#000",
              height: 60,
              alignItems: "center"
            }}
          >
            <BackButton navigation={this.props.navigation} />
            <TouchableOpacity
              onPress={() => this.goToScrollTop()}
              style={{
                width: "100%",
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontWeight: "bold",
                  fontSize: 18,
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
              >
                {this.props.customeTitle}
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension")
});

const mapDispatchToProps = dispatch => ({
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  NewHeaderWithBack
);
