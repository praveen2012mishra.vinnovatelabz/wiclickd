import React, { useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image
} from "react-native";
import { Icon } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { connect } from "react-redux";
import { compose } from "recompose";
import applloClient from "../client";
import AppHelper from "../constants/AppHelper";
import ConstantFontFamily from "../constants/FontFamily";
import {
  KickClikMemberMutation,
  PromoteClikMemberMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import {
  KickClikMemberVariables,
  PromoteClikMemberVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { listClikMembers } from "../actionCreator/ClikMembersAction";
import { capitalizeFirstLetter } from "../library/Helper";
import { Dimensions } from "react-native";

const ClikProfileUserList = props => {
  const inputRefs = {};
  const [reqUserMemberChange, setReqUserMemberChange] = useState("MEMBER");
  const [tempType, settempType] = useState("");


  const promoteMember = async id => {    
    if (reqUserMemberChange != "KICK_USER") {
      PromoteClikMemberVariables.variables.clik_id = props.ClikInfo.get("data")
        .get("clik")
        .get("id");
      PromoteClikMemberVariables.variables.user_id = id;
      PromoteClikMemberVariables.variables.member_type = reqUserMemberChange;

      try {
        await applloClient
          .query({
            query: PromoteClikMemberMutation,
            ...PromoteClikMemberVariables,
            fetchPolicy: "no-cache"
          })
          .then(async res => {
            await props.setClikMembers({
              id: props.ClikInfo.get("data")
                .get("clik")
                .get("id")
                .replace("Clik:", "")
            });
          });
          if (props.item.item.node.type == "MEMBER" && reqUserMemberChange == "ADMIN"){
            alert("Promoted "+props.item.item.node.user.username+" to Admin")
          } else if (props.item.item.node.type == "ADMIN"  && reqUserMemberChange == "MEMBER"){
            alert("Demoted "+props.item.item.node.user.username+" to Memebr")
          }  else if ((props.item.item.node.type == "ADMIN" ||props.item.item.node.type == "MEMBER") && reqUserMemberChange == "SUPER_ADMIN"){
            alert("Promoted "+props.item.item.node.user.username+" "+reqUserMemberChange.replace('_', " "))
          }
      } catch (e) {
        console.log(e);
      }
    } else {
      KickClikMemberVariables.variables.clik_id = props.ClikInfo.get("data")
        .get("clik")
        .get("id");
      KickClikMemberVariables.variables.user_id = id;
      try {
        await applloClient
          .query({
            query: KickClikMemberMutation,
            ...KickClikMemberVariables,
            fetchPolicy: "no-cache"
          })
          .then(async res => {
            await props.setClikMembers({
              id: props.ClikInfo.get("data")
                .get("clik")
                .get("id")
                .replace("Clik:", "")
            });
          });
          alert("Kicked "+props.item.item.node.user.username+" to "+ props.ClikInfo.get("data")
          .get("clik")
          .get("name"))
          
      } catch (e) {
        console.log(e);
      }
    }
  };

  return (

    // <View
    //   style={{
    //     // backgroundColor: "#fff",
    //     // // borderRadius: 10,
    //     // // borderColor: "#C5C5C5",
    //     // // borderWidth: 1,
    //     // width: "100%",
    //     // marginBottom: 10,
    //     // justifyContent:'space-between'
    //   }}
    // >
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{
          backgroundColor: "#fff",
          paddingVertical: 10,
          paddingRight:10
        }}
      >
        <View
          style={{
            width: "100%",
            flexDirection: "row",
            alignItems:'center'
            // justifyContent: "space-between"
          }}
        >
          <View
            style={{
              width: "65%",
              flexDirection: "row",
              justifyContent: "flex-start"
            }}
          >
            {props.item.item.node.user.profile_pic == "" ||
              props.item.item.node.user.profile_pic == null ? (
                <Image
                  source={require("../assets/image/default-image.png")}
                  style={{
                    width: 40,
                    height: 40,
                    padding: 0,
                    margin: 5,
                    borderRadius: 20
                  }}
                />
              ) : (
                <Image
                  source={{ uri: props.item.item.node.user.profile_pic }}
                  style={{
                    width: 40,
                    height: 40,
                    padding: 0,
                    margin: 5,
                    borderRadius: 20
                  }}
                />
              )}
            {/* <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                padding: 5
              }}
            > */}
              <Text
                style={{
                  color: "#000",
                  fontSize: 14,
                  fontWeight: "bold",
                  alignSelf:'center',
                  fontFamily: ConstantFontFamily.defaultFont
                }}
              >
                @{props.item.item.node.user.username}
              </Text>
            {/* </View> */}
          </View>
          {props.item.item.node.type != "SUPER_ADMIN" ? (
            props.userApprovePermision == true ? (
              <View
                style={{
                  flexDirection: "row",
                  width: '25%'
                }}
              >
                <View
                  style={{
                    // borderRadius: 10,
                    // backgroundColor: "#C0BFBF",
                    // height: 38,
                    // marginTop: 5,
                    // justifyContent: "center", 
                    width: '100%',
                    justifyContent: "flex-end",
                    borderRadius: 6,
                    borderColor: "#e1e1e1",
                    borderWidth: 1,
                    marginLeft:10,
                    marginRight: Dimensions.get('window').width>1100 ? 45 : 0,
                    height: 40,
                    marginTop: 5,
                    alignSelf: "center"
                  }}
                >
                  <RNPickerSelect
                    // value={props.item.item.node.type}
                    // itemKey={props.item.item.node.type}
                    value = {tempType ? tempType : props.item.item.node.type}
                    placeholder={{}}
                    items={AppHelper.MEMBER_PROMOTE_ROLEITEMS}
                    onValueChange={(itemValue, itemIndex) => {
                      console.log("itemValue", itemValue);
                      setReqUserMemberChange(itemValue);
                      settempType(itemValue)
                    }}
                    onUpArrow={() => {
                      inputRefs.name.focus();
                    }}
                    onDownArrow={() => {
                      inputRefs.picker2.togglePicker();
                    }}
                    style={{ ...styles }}
                    ref={el => {
                      inputRefs.picker = el;
                    }}
                  />
                </View>
                <View>
                  <Icon
                    color={"#000"}
                    iconStyle={{
                      width:'5%',                       
                      color: "#000",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "flex-end",
                    }}
                    // reverse
                    name="save"
                    type="font-awesome"
                    size={18}
                    containerStyle={{
                      // marginLeft: 0,
                      justifyContent: 'center',
                      marginTop: 15,
                    }}
                    onPress={() =>
                      promoteMember(props.item.item.node.user.id)
                    }
                  />
                </View>
              </View>
            ) : (
                <View
                  style={{
                    borderRadius: 10,
                    height: 30,
                    justifyContent: "center",
                    marginHorizontal: 10,
                    width:'25%'
                  }}
                >
                  <Text
                    style={{
                      color: "#000",
                      fontSize: 14,
                      fontWeight: "bold",
                      fontFamily: ConstantFontFamily.defaultFont
                    }}
                  >
                    {props.item.item.node.type &&
                      capitalizeFirstLetter(
                        props.item.item.node.type.toLowerCase()
                      )}
                  </Text>
                </View>
              )
          ) : (
              <View
                style={{
                  height: 30,
                  justifyContent: "center",
                  marginHorizontal: 10,
                  width:'25%'
                }}
              >
                <Text
                  style={{
                    textAlign: "left",
                    alignSelf: "flex-start",
                    color: "#000",
                    fontSize: 14,
                    fontWeight: "bold",
                    fontFamily: ConstantFontFamily.defaultFont
                  }}
                >
                  Super Admin
                  </Text>
              </View>
            )}
        </View>
      </ScrollView>
    // </View>
  );
};

const mapDispatchToProps = dispatch => ({
  setClikMembers: payload => dispatch(listClikMembers(payload))
});

export default compose(connect(null, mapDispatchToProps))(
  React.memo(ClikProfileUserList)
);

const styles = StyleSheet.create({

  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  },
  inputAndroid: {
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "#fff",
    borderRadius: 8,
    color: "#000",
    backgroundColor: "white",
    paddingRight: 30,
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.defaultFont
  }
});