import firebase from "firebase/app";
import "firebase/auth";
import React, { Component } from "react";
import {
  TouchableOpacity,
  AsyncStorage,
  Image,
  Platform,
  Text,
  View,
  StyleSheet,
  Dimensions
} from "react-native";
import { Button } from "react-native-elements";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger
} from "react-native-popup-menu";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setAdminStatus } from "../actionCreator/AdminAction";
import { setFEEDREPORTMODALACTION } from "../actionCreator/FeedReportModalAction";
import { setInviteSIGNUPMODALACTION } from "../actionCreator/InviteSignUpModalAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setRESETPASSWORDMODALACTION } from "../actionCreator/ResetPasswordModalAction";
import { setSHARELINKMODALACTION } from "../actionCreator/ShareLinkModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import {
  setLoginStatus,
  saveUserLoginDaitails
} from "../actionCreator/UserAction";
import { setUSERNAMEMODALACTION } from "../actionCreator/UsernameModalAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import { setVERIFYEMAILMODALACTION } from "../actionCreator/VerifyEmailModalAction";
import ConstantFontFamily from "../constants/FontFamily";
import { Badge, Icon } from "react-native-elements";
import NavigationService from "../library/NavigationService";
import { setUserApproachAction } from "../actionCreator/UserApproachAction";
import { setScreenLoadingModalAction } from "../actionCreator/ScreenLoadingModalAction";
import { setLocalStorage } from "../library/Helper";
import jwt_decode from "jwt-decode";
import { graphql } from "react-apollo";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import AppHelper from "../constants/AppHelper";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingExternalFeeds } from "../actionCreator/TrendingExternalFeedsAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import { getTrendingUsers } from "../actionCreator/TrendingUsersAction";
import applloClient from "../client";
import {
  GetNumUnreadNotificationsMutation,
  MarkNotificationsAsReadMutation
} from "../graphqlSchema/graphqlMutation/Notification";

class IconMenu extends Component {
  constructor(props) {
    super(props);
    const gstar = require("../assets/image/gstar.png");
    const ystar = require("../assets/image/ystar.png");
    const wstar = require("../assets/image/wstar.png");
    this.state = {
      UnreadNotifications: 0,
      showSearchIcon: true,
      index: 0,
      starList: [wstar, gstar, ystar],
      followList: ["TRENDING", "FOLLOW", "FAVORITE"]
    };
  }
  gotoprofile = () => {
    this.props.userId({
      username:
        this.props.profileData &&
        this.props.profileData
          .getIn(["my_users"])
          .getIn(["0", "user", "username"]),
      type: "feed"
    });
    this.props.navigation.navigate("profile", {
      username:
        this.props.profileData &&
        this.props.profileData
          .getIn(["my_users"])
          .getIn(["0", "user", "username"]),
      type: "feed"
    });
  };

  logout = async () => {
    await AsyncStorage.multiRemove([
      "userLoginId",
      "MyUserUserId",
      "userIdTokenWeclikd",
      "UserId",
      "admin",
      "skipCredentials"
    ]).then(async r => {
      await firebase
        .auth()
        .signOut()
        .then(async res => {
          await this.props.changeLoginStatus(0);
          await this.props.changeAdminStatus(false);
          if (
            (await this.props.navigation.getCurrentRoute().routeName) ==
              "notification" ||
            (await this.props.navigation.getCurrentRoute().routeName) ==
              "settings"
          ) {
            await this.props.navigation.navigate("home");
          }
          await this.props.resetLoginUserDetails();
          await this.props.resetUserProfileDetails();

          if (Platform.OS == "web") {
            this.extensionLogout();
          }
        });
    });
  };

  extensionLogout = () => {
    try {
      window.parent.postMessage({ type: "wecklid_logout" }, "*");
    } catch (err) {
      console.log("Extension Logout Error ", err);
    }
  };

  loginHandle = () => {
    this.props.setLoginModalStatus(true);
  };

  inviteSignHandle = async () => {
    await this.props.setInviteUserDetail({
      clikName: "",
      inviteKey: "",
      userName: ""
    });
    await this.props.setUsernameModalStatus(true);

    // this.props.navigation.navigate("username");
  };

  componentDidMount = async () => {
    if (this.props.getUserApproach == "login") {
      await this.props.setUserApproachAction({ type: "" });
      this.userLogin();
    }
    if (this.props.getUserApproach == "signUp") {
      this.props.setSignUpModalStatus(true);
    }
    if (this.props.loginStatus == 1) {
      this.getUnreadNotifications();
    }
  };

  onModalClose = async () => {
    await this.props.setLoginButtonText("Logged In!");
    await this.props.setGoogleLogin(true);
    setTimeout(() => {
      this.props.setLoginModalStatus(false);
      this.props.setLoginButtonText("Login");
      this.props.setGoogleLogin(false);
    }, 2000);
  };

  getUnreadNotifications = () => {
    applloClient
      .query({
        query: GetNumUnreadNotificationsMutation,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        this.setState({
          UnreadNotifications: res.data.account.num_unread_notifications
        });
        let timer = setTimeout(() => {
          if (this.props.loginStatus == 1) {
            this.getUnreadNotifications();
          }
        }, 60000);
      });
  };

  setMarkAsRead = () => {
    applloClient
      .query({
        query: MarkNotificationsAsReadMutation,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        this.setState({
          UnreadNotifications: 0
        });
        this.getUnreadNotifications();
      });
  };

  userLogin = async () => {
    let __self = this;
    await this.props.setLoginButtonText("Logging in...");
    await this.props.setGoogleLogin(true);
    await firebase.auth().onAuthStateChanged(async res => {
      if (res) {
        __self.props.setScreenLoadingModalAction(true);
        return await res
          .getIdToken(true)
          .then(async function(idToken) {
            await setLocalStorage("userIdTokenFirebase", idToken);
            await setLocalStorage(
              "admin",
              jwt_decode(idToken).admin ? "true" : "false"
            );
            await __self.props.changeAdminStatus(
              jwt_decode(idToken).admin ? jwt_decode(idToken).admin : false
            );

            if (Platform.OS == "web") {
              __self.setUserNameInExtension = __self.setLoginTokenInExtension(
                idToken
              );
            }
            return idToken;
          })
          .then(async res => {
            if (res) {
              let loginData = await __self.props.Login();
              if (loginData.data.login.status.status == "NOT_FOUND") {
                // console.log("loginData", loginData);
                await __self.props.changeLoginStatus(0);
                __self.props.setScreenLoadingModalAction(false);
                __self.props.setLoginButtonText("Login");
                __self.props.setGoogleLogin(false);
                alert("Invalid email or password");
              } else {
                await __self.props.saveLoginUser(loginData.data.login);
                await __self.props.changeLoginStatus(1);
                await __self.props.getHomefeed({
                  currentPage: AppHelper.PAGE_LIMIT
                });
                await __self.props.getTrendingUsers({
                  currentPage: AppHelper.PAGE_LIMIT
                });
                await __self.props.getTrendingClicks({
                  currentPage: AppHelper.PAGE_LIMIT
                });
                await __self.props.getTrendingTopics({
                  currentPage: AppHelper.PAGE_LIMIT
                });
                await __self.props.getTrendingExternalFeeds({
                  currentPage: AppHelper.PAGE_LIMIT
                });
                await AsyncStorage.setItem(
                  "userLoginId",
                  loginData.data.login.account.id
                );
                await AsyncStorage.setItem(
                  "MyUserUserId",
                  loginData.data.login.account.my_users[0].id
                );
                await AsyncStorage.setItem(
                  "UserId",
                  loginData.data.login.account.my_users[0].user.id
                );
                await AsyncStorage.setItem(
                  "UserName",
                  loginData.data.login.account.my_users[0].user.username
                );
                await __self.onModalClose();
                // set user name to extension
                if (Platform.OS == "web") {
                  __self.setUserNameInExtension(
                    loginData.data.login.account.my_users[0].user.username
                  );
                }
                await __self.props.setScreenLoadingModalAction(false);
              }
            }
          })
          .catch(error => {
            console.log(error);
            __self.props.setScreenLoadingModalAction(false);
            alert("Invalid email or password");
            this.props.setLoginButtonText("Login");
            this.props.setGoogleLogin(false);
            return false;
          });
      }
      // else {
      //   alert("Google Login is not supported in Incognito mode");
      // }
    });
  };

  setLoginTokenInExtension = idToken => UserName => {
    try {
      window.parent.postMessage(
        { type: "wecklid_login", userIdTokenFirebase: idToken, UserName },
        "*"
      );
    } catch (e) {
      console.log("extension login Error ", e);
    }
  };

  getSettingButtonState = type => {
    //let getSession=sessionStorage.getItem('getSettingButtonState')|| {}
    //console.log(getSession,'----------------------->');
    //getSession=Object.assign({},{type:true});
    AsyncStorage.setItem("getSettingButtonState", false);
    //sessionStorage.setItem("getSettingButtonState", false);
  };

  componentWillUnmount = () => {
    this.setState({ showSearchIcon: true });
  };

  render() {
    return (
      <View>
        {/* {this.props.loginStatus == 1 && ( */}
          <View
            style={{
              alignItems: "center",
              justifyContent: 'center',
              // paddingHorizontal: 15,
              height: 50,
              // flexDirection: "row",
            }}
          >
            {/* <Icon
              size={26}
              name="star"
              type="font-awesome"
              iconStyle={styles.actionButtonIcon}
              color="#fff"
              underlayColor="#000"
              onPress={() => NavigationService.navigate("bookmark")}
            />

            <Icon
              name="search"
              size={26}
              type="font-awesome"
              iconStyle={styles.actionButtonIcon}
              color="#fff"
              underlayColor="#000"
              iconStyle={{
                color: "white",
                alignSelf: "center",
                alignItems: "center",
                justifyContent: "center",
                paddingTop: 2
              }}
              onPress={() => {
                NavigationService.navigate("search");
              }}
            /> */}
            <Icon
              testID="AddLink"
              size={25}
              name="plus"
              type="feather"
              iconStyle={styles.actionButtonIcon}
              color= {Dimensions.get("window").width <= 750 ? "#000" : "#fff"}
              underlayColor="#000"
              onPress={() => this.props.setShareLinkModalStatus(!this.props.getShareLinkModalStatus)}
            />
            {/* <Icon
              name="bell"
              size={25}
              type="font-awesome"
              iconStyle={{
                color: "white",
                alignSelf: "center",
                alignItems: "center",
                justifyContent: "center"
              }}
              underlayColor="#000"
              color="#fff"
              onPress={() => {
                NavigationService.navigate("notification"),
                  this.setMarkAsRead();
              }}
            />

            {this.state.showSearchIcon === true && (
              <Menu>
                <MenuTrigger testID="ProfileIcon">
                  {this.props.profileData &&
                  this.props.profileData
                    .getIn(["my_users"])
                    .getIn(["0", "user", "profile_pic"]) != null ? (
                    <Image
                      source={{
                        uri: this.props.profileData
                          .getIn(["my_users"])
                          .getIn(["0", "user", "profile_pic"])
                      }}
                      style={{
                        height: 35,
                        width: 35,
                        borderRadius: 20,
                        borderWidth: 1,
                        borderColor: "#fff"
                      }}
                      navigation={this.props.navigation}
                    />
                  ) : (
                    <Image
                      source={require("../assets/image/default-image.png")}
                      style={{
                        height: 35,
                        width: 35,
                        borderRadius: 20,
                        borderWidth: 1,
                        borderColor: "#fff"
                      }}
                      navigation={this.props.navigation}
                    />
                  )}
                </MenuTrigger>

                <MenuOptions
                  optionsContainerStyle={{
                    borderRadius: 10,
                    borderWidth: 1,
                    borderColor: "#c5c5c5",
                    shadowColor: "transparent"
                  }}
                  customStyles={{
                    optionsContainer: {
                      marginBottom: 50,
                      marginRight: "auto"
                    }
                  }}
                >
                  <MenuOption onSelect={() => this.gotoprofile()}>
                    <Hoverable>
                      {isHovered => (
                        <Text
                          testID="ProfileDetails"
                          style={{
                            textAlign: "center",
                            color: isHovered == true ? "#009B1A" : "#000",
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          Profile
                        </Text>
                      )}
                    </Hoverable>
                  </MenuOption>

                  <MenuOption
                    onSelect={() => {
                      return this.props.navigation.navigate("settings");
                    }}
                  >
                    <Hoverable>
                      {isHovered => (
                        <Text
                          style={{
                            textAlign: "center",
                            color: isHovered == true ? "#009B1A" : "#000",
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          Settings
                        </Text>
                      )}
                    </Hoverable>
                  </MenuOption>
                  <MenuOption
                    onSelect={() => this.props.navigation.navigate("analytics")}
                  >
                    <Hoverable>
                      {isHovered => (
                        <Text
                          style={{
                            textAlign: "center",
                            color: isHovered == true ? "#009B1A" : "#000",
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          Analytics
                        </Text>
                      )}
                    </Hoverable>
                  </MenuOption>
                  <MenuOption onSelect={() => this.logout()}>
                    <Hoverable>
                      {isHovered => (
                        <Text
                          testID="SignOut"
                          style={{
                            textAlign: "center",
                            color: isHovered == true ? "#009B1A" : "#000",
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          Sign Out
                        </Text>
                      )}
                    </Hoverable>
                  </MenuOption>
                </MenuOptions>
              </Menu>
            )} */}
          </View>
         {/* )} */}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getLoginModalStatus: state.LoginModalReducer.get("modalStatus"),
  getSignUpModalStatus: state.SignUpModalReducer.get("modalStatus"),
  getUsernameModalStatus: state.UsernameModalReducer.get("modalStatus"),
  getResetPasswordModalStatus: state.ResetPasswordModalReducer.get(
    "modalStatus"
  ),
  getVerifyEmailModalStatus: state.VerifyEmailModalReducer.get("modalStatus"),
  getShareLinkModalStatus: state.ShareLinkModalReducer.get("modalStatus"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getFeedReportModalStatus: state.FeedReportModalReducer.get("modalStatus"),
  getScreenLoadingStatus: state.ScreenLoadingReducer.get("modalStatus"),
  getUserApproach: state.UserApproachReducer.get("setUserApproach"),
  searchBarStatus: state.AdminReducer.get("searchBarStatus"),
  getLoginButtonText: state.AdminReducer.get("loginButtonText"),
});

const mapDispatchToProps = dispatch => ({
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setInviteSignUpModalStatus: payload =>
    dispatch(setInviteSIGNUPMODALACTION(payload)),
  setUsernameModalStatus: payload => dispatch(setUSERNAMEMODALACTION(payload)),
  setResetPasswordModalStatus: payload =>
    dispatch(setRESETPASSWORDMODALACTION(payload)),
  setVerifyEmailModalStatus: payload =>
    dispatch(setVERIFYEMAILMODALACTION(payload)),
  setShareLinkModalStatus: payload =>
    dispatch(setSHARELINKMODALACTION(payload)),
  setFeedReportModalStatus: payload =>
    dispatch(setFEEDREPORTMODALACTION(payload)),
  changeLoginStatus: payload => dispatch(setLoginStatus(payload)),
  changeAdminStatus: payload => dispatch(setAdminStatus(payload)),
  setUserApproachAction: payload => dispatch(setUserApproachAction(payload)),
  setScreenLoadingModalAction: payload =>
    dispatch(setScreenLoadingModalAction(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingUsers: payload => dispatch(getTrendingUsers(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  getTrendingExternalFeeds: payload =>
    dispatch(getTrendingExternalFeeds(payload)),
  setSearchBarStatus: payload =>
    dispatch({ type: "SEARCH_BAR_STATUS", payload }),
  resetLoginUserDetails: payload =>
    dispatch({ type: "LOGIN_USER_DETAILS_RESET", payload }),
  resetUserProfileDetails: payload =>
    dispatch({ type: "USER_PROFILE_DETAILS_RESET", payload }),
  setMessageModalStatus: payload =>
    dispatch({ type: "MESSAGEMODALSTATUS", payload }),
  setLoginButtonText: payload =>
    dispatch({ type: "SET_LOGIN_BUTTON_TEXT", payload }),
  setInviteUserDetail: payload =>
    dispatch({ type: "SET_INVITE_USER_DETAIL", payload }),
  setGoogleLogin: payload => dispatch({ type: "SET_GOOGLE_LOGIN", payload })
});

const IconMenuWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(IconMenu);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  IconMenuWrapper
);

const styles = StyleSheet.create({
  actionButtonIcon: {
    backgroundColor: Dimensions.get("window").width <= 750 ? "#fff" : "#000",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center"
    // marginRight: 50
  }
});