import "@expo/browser-polyfill";
import _ from "lodash";
import { List } from "immutable";
import React, { Component } from "react";
import { graphql } from "react-apollo";
import { Image, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setLikeContent } from "../actionCreator/LikeContentAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingExternalFeeds } from "../actionCreator/TrendingExternalFeedsAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import { getTrendingUsers } from "../actionCreator/TrendingUsersAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import AppHelper from "../constants/AppHelper";
import {
  FeedFollowMutation,
  FeedUnFollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  FeedFollowVariables,
  FeedUnFollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import { getFeedProfileDetails } from "../actionCreator/FeedProfileAction";

class ExternalFeedStar extends Component {
  constructor(props) {
    super(props);
    const gstar = require("../assets/image/gstar.png");
    const ystar = require("../assets/image/ystar.png");
    const wstar = require("../assets/image/wstar.png");

    this.state = {
      index: 0,
      starList: [wstar, gstar, ystar],
      followList: ["TRENDING", "FOLLOW", "FAVORITE"]
    };
  }

  async componentDidMount() {
    await this.getFeedStar(this.props.FeedName);
  }

  componentDidUpdate = async prevProps => {
    if (prevProps.FeedName !== this.props.FeedName) {
      await this.getFeedStar(this.props.FeedName);
    }
    if (prevProps.getUserFollowFeedList !== this.props.getUserFollowFeedList) {
      await this.getFeedStar(this.props.FeedName);
    }
  };

  getFeedStar = async Name => {
    if (this.props.loginStatus == 0) {
      await this.setState({
        index: 0
      });
      return false;
    }
    let index = 0;
    index =
      this.props.getUserFollowFeedList &&
      this.props.getUserFollowFeedList.findIndex(
        i =>
          i
            .getIn(["feed", "name"])
            .toLowerCase()
            .replace("ExternalFeed:", "") ==
          Name && Name.toLowerCase().replace("ExternalFeed:", "")
      );
    if (index == -1) {
      await this.setState({
        index: 0
      });
    } else if (
      this.props.getUserFollowFeedList.getIn([
        index,
        "settings",
        "follow_type"
      ]) == "FOLLOW"
    ) {
      await this.setState({
        index: 1
      });
    } else {
      await this.setState({
        index: 2
      });
    }
  };

  changeStar = async () => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    if (this.state.index + 1 === this.state.starList.length) {
      await this.setState({
        index: 0
      });
      await this.changeStarApi(this.state.followList[this.state.index]);
    } else {
      await this.setState({
        index: this.state.index + 1
      });
      await this.changeStarApi(this.state.followList[this.state.index]);
    }
  };

  changeStarApi = _.debounce(typeIndex => {
    typeIndex == "FAVORITE"
      ? this.favroiteFeed(this.props.FeedId)
      : typeIndex == "TRENDING"
      ? this.unfollowFeed(this.props.FeedId)
      : this.followFeed(this.props.FeedId);
  }, 1000);

  followFeed = feedid => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    FeedFollowVariables.variables.feed_id = feedid;
    FeedFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: FeedFollowMutation,
        ...FeedFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowFeed = async Id => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    FeedUnFollowVariables.variables.feed_id = Id;
    applloClient
      .query({
        query: FeedUnFollowMutation,
        ...FeedUnFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  favroiteFeed = async Id => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    FeedFollowVariables.variables.feed_id = Id;
    FeedFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: FeedFollowMutation,
        ...FeedFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  render() {
    return (
      <TouchableOpacity
        style={this.props.ContainerStyle}
        onPress={() => this.changeStar()}
      >
        <Image
          source={this.state.starList[this.state.index]}
          style={this.props.ImageStyle}
        />
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  DiscussionHomeFeed: state.HomeFeedReducer.get("DiscussionHomeFeedList"),
  getUserFollowFeedList: state.LoginUserDetailsReducer.get("userFollowFeedList")
    ? state.LoginUserDetailsReducer.get("userFollowFeedList")
    : List()
});

const mapDispatchToProps = dispatch => ({
  LikeContent: payload => dispatch(setLikeContent(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setDiscussionHomeFeed: payload =>
    dispatch({ type: "SET_DISCUSSION_HOME_FEED", payload }),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  getTrendingUsers: payload => dispatch(getTrendingUsers(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  getTrendingExternalFeeds: payload =>
    dispatch(getTrendingExternalFeeds(payload)),
  setFeedDetails: payload => dispatch(getFeedProfileDetails(payload))
});

const ExternalFeedStarWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(ExternalFeedStar);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  ExternalFeedStarWrapper
);
