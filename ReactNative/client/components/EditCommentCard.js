import React, { useEffect, useState } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Dimensions,
  Image
} from "react-native";
import { Icon, Button } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { connect } from "react-redux";
import { compose } from "recompose";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import applloClient from "../client";
import ConstantColors from "../constants/Colors";
import ConstantFontFamily from "../constants/FontFamily";
import { EditCommentMutation } from "../graphqlSchema/graphqlMutation/LikeContentMutation";
import { EditCommentVariables } from "../graphqlSchema/graphqlVariables/LikeContentVariables";
import { capitalizeFirstLetter } from "../library/Helper";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import getEnvVars from "../environment";
import ButtonStyle from "../constants/ButtonStyle";
import { EditorState, RichUtils, convertToRaw, convertFromHTML } from 'draft-js';
//import 'draft-js/dist/Draft.css';
import { stateToHTML } from 'draft-js-export-html';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
//import '../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
//import '../assets/react-draft.css';

function myBlockStyleFn(contentBlock) {
  const type = contentBlock.getType();
  if (type === 'blockquote') {
    return styles.blockQuote;
  }
}

const EditCommentCard = props => {
  const inputRefs = {};
  const [title, settitle] = useState(
    props.topComment.text ? props.topComment.text : ""
  );
  const [item, setitem] = useState([]);
  const [click, setclick] = useState(null);
  const [showsubmitbutton, setshowsubmitbutton] = useState(true);

  const [placeholderState, setPlaceholderState] = useState(true);
  const [titleContent, setTitleContent] = useState("characters more");
  const [getSubmitData, setSubmitData] = useState('');

  const onEditorStateChange = (editorState) => { 
    document.getElementsByClassName("demo-editor")[1].scrollTop = parseInt(document.getElementsByClassName("demo-editor")[1].scrollHeight)+parseInt(document.getElementsByClassName("demo-editor")[1].scrollHeight);
    if (editorState.getCurrentContent().getPlainText('\u0001').length==0) { 
      setPlaceholderState(false)
    }
    onChange(editorState);
    settitle(editorState.getCurrentContent().getPlainText('\u0001'))    
    title.length<140?setTitleContent('characters more'):setTitleContent('characters left')
    setSubmitData(draftToHtml(convertToRaw(editorState.getCurrentContent())))
  };


  const [editorState, onChange] = useState('');
  // const [editorState, onChange] = useState(EditorState.createWithContent(ContentState.createFromText(props.topComment.text ? props.topComment.text : "")));
  const [colorState, setColorState] = useState([
    { type: 'UNDERLINE', state: false },
    { type: 'BOLD', state: false },
    { type: 'ITALIC', state: false },
    { type: 'blockquote', state: false },
    { type: 'code-block', state: false },
    { type: 'unordered-list-item', state: false },
  ]);

  const handleKeyCommand = (command) => {
    console.log(command, '-------------------------------------------->');
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      onChange(newState);
      settitle(editorState.getCurrentContent().getPlainText('\u0001'))
      return 'handled';
    }
    return 'not-handled';
  }

  const onUnderlineClick = () => {
    onChange(RichUtils.toggleInlineStyle(editorState, 'UNDERLINE'));
    settitle(editorState.getCurrentContent().getPlainText('\u0001'))
  }

  const onBoldClick = () => {
    onChange(RichUtils.toggleInlineStyle(editorState, 'BOLD'))
    settitle(editorState.getCurrentContent().getPlainText('\u0001'))
  }

  const onItalicClick = () => {
    onChange(RichUtils.toggleInlineStyle(editorState, 'ITALIC'))
    settitle(editorState.getCurrentContent().getPlainText('\u0001'))
  }

  const apiUrl = getEnvVars();

  const submitComment = mode => {
    // EditCommentVariables.variables.text = mode == "edit" ? title : "";
    //EditCommentVariables.variables.text = mode == "edit" ? stateToHTML(editorState.getCurrentContent()) : "";
    EditCommentVariables.variables.text = mode == "edit" ? title : "";
    EditCommentVariables.variables.content_id = props.topComment.id;
    setshowsubmitbutton(false);
    applloClient
      .mutate({
        mutation: EditCommentMutation,
        ...EditCommentVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        props.closeModalBySubmit("", "");
        props.onClose();
        //---------------------------------- create new comment push to comment list ------------------------------
        // const itemId = "Post:" + props.navigation.getParam("id", "NO-ID");
        const itemId = props.parentPostId;
        await getCommentList(itemId);
        // let prevCommentList = props.PostCommentDetails;
        // let parentId = props.parent_content_id;
        // let createData = {
        //   id: props.topComment.id,
        //   text: title
        // };
        // console.log(
        //   "============>",
        //   parentId,
        //   createData,
        //   "parent Id",
        //   props.parent_content_id,
        //   "props.topComment",
        //   props.topComment
        // );
        //const updateArray = updateNestedArray(prevCommentList, "Comment:95953478488486", createData, true);
        // props.setPostCommentDetails(updateArray);
        //----------------------------------------------------------------------------------------------------------
      })
      .catch(e => {
        console.log(e.message.toLowerCase().replace("graphql error: ", ""));
      });
      setPlaceholderState(true)
  };

  useEffect(() => {
    let data = [
      {
        label: "Everyone",
        value: "all",
        key: -1
      }
    ];
    if (props.profileData.getIn(["my_users", "0", "cliks_followed"]).size > 0) {
      props.profileData
        .getIn(["my_users", "0", "cliks_followed"])
        .map((item, index) => {
          if (item.getIn(["member_type"]) == "SUPER_ADMIN") {
            data.push({
              label: "#" + capitalizeFirstLetter(item.getIn(["clik", "name"])),
              value: item.getIn(["clik", "name"]),
              key: index
            });
          }
        });
    }

    setitem(data);
    setclick(data.length > 0 ? data[0].label : null);
  }, []);

  const goToUserProfile = async username => {
    props.onClose();
    await props.userId({
      username: username
    });
    await props.navigation.navigate("profile", {
      username: username,
      type: "profile"
    });
  };

  const getCommentList = async id => {
    fetch(apiUrl.API_URL + "v1/comments/" + id.replace("Post:", ""))
      .then(response => response.json())
      .then(async res => {
        let l1 = res.data.comments.edges;
        props.setPostCommentDetails(l1);
      });
  };



  return (
    <View
      style={{
        borderRadius: 10,
        overflow: "visible",
        width: "100%",
        backgroundColor: "#fff"
      }}
    >
      <View
        style={{
          marginVertical: 5,
          flexDirection: "row",
          alignSelf: "center"
        }}
      >
        {props.initial == "main" &&
          item != null &&
          item.length > 0 &&
          item[0] != "" && (
            <View
              style={{
                width: "100%"
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  width: "100%"
                }}
              >
                <Text
                  style={{
                    alignSelf: "center",
                    marginRight: 10,
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    fontSize: 15
                  }}
                >
                  Discuss With:
                </Text>
                <RNPickerSelect
                  placeholder={{}}
                  items={item}
                  onValueChange={(itemValue, itemIndex) => {
                    setclick(itemValue);
                  }}
                  onUpArrow={() => {
                    inputRefs.name.focus();
                  }}
                  onDownArrow={() => {
                    inputRefs.picker2.togglePicker();
                  }}
                  style={{ ...styles }}
                  value={click}
                  ref={el => {
                    inputRefs.picker = el;
                  }}
                  itemStyle={{ textAlign: "center" }}
                />
              </View>
            </View>
          )}
      </View>
      <View
        style={{
          height: Platform.OS != "web" ? hp("25%") : null,
          backgroundColor: "#fff"
        }}
      >
        <View style={{ flex: 1 }}>
          <View
            style={{
              borderWidth: 1,
              borderColor: "#e8e8e8",
              borderRadius: 6,
              ...Platform.select({
                web: {
                  marginHorizontal: 10
                },
                android: {
                  marginTop: 10
                },
                ios: {
                  marginTop: 10
                }
              })
            }}
          >
            <TextInput
              value={title}
              multiline={true}
              numberOfLines={Platform.OS === "ios" ? null : 4}
              placeholder={placeholderState ? "Write an insightful comment (longer than 140 characters)." : ''}
              style={{
                padding: 5,
                  color: "#000",
                  fontFamily: ConstantFontFamily.VerdanaFont,
                  fontSize: 13,
                  width: "100%",
                  borderRadius: 6,
                  //minHeight: Platform.OS === "ios" && 4 ? 10 * 4 : null
                  minHeight: 100,
              }}
              onChangeText={title =>{
                //debugger
                console.log(title,'------------------------->');
                settitle(title)
                if (title.length == 0) {
                  setPlaceholderState(true)
                }
              }
               }
            />

            {/* <View >
              <View style={{
                flexDirection: 'row', 
                justifyContent: 'space-around',
                 padding: 10, 
                 borderBottomWidth: 1,
                borderRadius: 6,
                backgroundColor: '#f9f9f9',
                borderColor: `#c8ccd0`,
              }}>
                <Text
                  style={{
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    cursor: "pointer"
                  }}
                  onMouseDown={(e) => {
                    e.preventDefault()
                    onChange(RichUtils.toggleInlineStyle(editorState, 'UNDERLINE'));
                    settitle(editorState.getCurrentContent().getPlainText('\u0001'));
                    let newArray = colorState.map((ele) => {
                      if (ele.type == 'UNDERLINE') {
                        ele.state = !ele.state
                        return ele
                      }
                      else {
                        return ele
                      }
                    })
                    setColorState([...newArray]);
                    //setColorState({type:'UNDERLINE',state:!colorState.state})                    
                  }}
                >
                  <Icon
                   color={colorState[0].type == 'UNDERLINE' ? colorState[0].state ? "#4169e1" : "#000" : "#000"}
                   //iconStyle={{ paddingLeft: 15 }}
                    name="underline"
                    type="font-awesome"
                    size={16}
                  />
                </Text>
                <Text
                  style={{
                    color: colorState[1].type == 'BOLD' ? colorState[1].state ? "#4169e1" : "#000" : "#000",
                    //fontWeight: "bold",
                    //fontSize: 17,
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    cursor: "pointer"
                  }}
                  onMouseDown={(e) => {
                    onChange(RichUtils.toggleInlineStyle(editorState, 'BOLD'));
                    settitle(editorState.getCurrentContent().getPlainText('\u0001'))
                    let newArray = colorState.map((ele) => {
                      if (ele.type == 'BOLD') {
                        ele.state = !ele.state
                        return ele
                      }
                      else {
                        return ele
                      }
                    })
                    setColorState([...newArray]);
                  }}
                >
                  <Icon
                    color={colorState[1].type == 'BOLD' ? colorState[1].state ? "#4169e1" : "#000" : "#000"}
                    //iconStyle={{ paddingLeft: 15 }}
                    name="bold"
                    type="font-awesome"
                    size={15}
                  />
                </Text>
                <Text
                  style={{
                    color: colorState[2].type == 'ITALIC' ? colorState[2].state ? "#4169e1" : "#000" : "#000",
                    //fontWeight: "bold",
                    fontSize: 17,
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    cursor: "pointer"
                  }}
                  onMouseDown={(e) => {
                    onChange(RichUtils.toggleInlineStyle(editorState, 'ITALIC'));
                    // onChange(RichUtils.toggleInlineStyle(editorState, 'CODE'));
                    settitle(editorState.getCurrentContent().getPlainText('\u0001'))
                    let newArray = colorState.map((ele) => {
                      if (ele.type == 'ITALIC') {
                        ele.state = !ele.state
                        return ele
                      }
                      else {
                        return ele
                      }
                    })
                    setColorState([...newArray]);
                  }}
                >
                  <Icon
                    //color={"#000"}
                    color={colorState[2].type == 'ITALIC' ? colorState[2].state ? "#4169e1" : "#000" : "#000"}
                    //iconStyle={{ paddingLeft: 15 }}
                    name="italic"
                    type="font-awesome"
                    size={15}
                  />
                </Text>
                <Text
                  style={{
                    color: colorState[3].type == 'blockquote' ? colorState[3].state ? "#4169e1" : "#000" : "#000",
                    //fontWeight: "bold",
                    fontSize: 17,
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    cursor: "pointer"
                  }}
                  onMouseDown={(e) => {
                    //onChange(RichUtils.toggleInlineStyle(editorState, 'blockquote'));
                    onChange(RichUtils.toggleBlockType(
                      editorState,
                      'blockquote'
                    ));
                    settitle(editorState.getCurrentContent().getPlainText('\u0001'))
                    let newArray = colorState.map((ele) => {
                      if (ele.type == 'blockquote') {
                        ele.state = !ele.state
                        return ele
                      }
                      else {
                        return ele
                      }
                    })
                    setColorState([...newArray]);
                  }}
                >

                  <Icon
                    //color={"#000"}
                    color={colorState[3].type == 'blockquote' ? colorState[3].state ? "#4169e1" : "#000" : "#000"}
                    //iconStyle={{ paddingLeft: 15 }}
                    name="quote-right"
                    type="font-awesome"
                    size={10}
                  />
                </Text>
                <Text
                  style={{
                    color: colorState[4].type == 'code-block' ? colorState[4].state ? "#4169e1" : "#000" : "#000",
                    //fontWeight: "bold",
                    fontSize: 17,
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    cursor: "pointer"
                  }}
                  onMouseDown={(e) => {
                    onChange(RichUtils.toggleBlockType(
                      editorState,
                      'code-block'
                    ));
                    //onChange(RichUtils.toggleInlineStyle(editorState, 'CODE'));
                    settitle(editorState.getCurrentContent().getPlainText('\u0001'))
                    let newArray = colorState.map((ele) => {
                      if (ele.type == 'code-block') {
                        ele.state = !ele.state
                        return ele
                      }
                      else {
                        return ele
                      }
                    })
                    setColorState([...newArray]);
                  }}
                >
                  <Icon
                    //color={"#000"}
                    color={colorState[4].type == 'code-block' ? colorState[4].state ? "#4169e1" : "#000" : "#000"}
                    //iconStyle={{ paddingLeft: 15 }}
                    name="code"
                    type="font-awesome"
                    size={20}
                  />
                </Text>
                <Text
                  style={{
                    color: colorState[5].type == 'unordered-list-item' ? colorState[5].state ? "#4169e1" : "#000" : "#000",
                    fontWeight: "bold",
                    fontSize: 17,
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    cursor: "pointer"
                  }}
                  onMouseDown={(e) => {
                    onChange(RichUtils.toggleBlockType(
                      editorState,
                      'unordered-list-item'
                    ));
                    settitle(editorState.getCurrentContent().getPlainText('\u0001'))
                    let newArray = colorState.map((ele) => {
                      if (ele.type == 'unordered-list-item') {
                        ele.state = !ele.state
                        return ele
                      }
                      else {
                        return ele
                      }
                    })
                    setColorState([...newArray]);
                  }}
                >
                  <Icon
                    //color={"#000"}
                    color={colorState[5].type == 'unordered-list-item' ? colorState[5].state ? "#4169e1" : "#000" : "#000"}
                    //iconStyle={{ paddingLeft: 15 }}
                    name="list-ul"
                    type="font-awesome"
                    size={20}
                  />
                </Text>

              </View>
              <View style={{
                padding: 5,
                color: title.length > 0 ? "black" : "gray",
                fontFamily: ConstantFontFamily.defaultFont,
                fontSize: 13,
                //lineHeight: 1.5,
                width: "100%",
                borderRadius: 6,
                overflow:title.length <144? 'hidden':'auto',
                height: 85
              }}
                onMouseEnter={() => {
                  onChange(RichUtils.toggleInlineStyle(editorState, 'HIGHLIGHT'))
                }}
              >
                <Editor
                  editorState={editorState}
                  handleKeyCommand={() => console.log('=========================================handleKeyCommand')}
                  onChange={(ele) => {
                    //ele.current.focus();
                    // console.log(ele,ele.current,ele.current.focus());
                    settitle(ele.getCurrentContent().getPlainText('\u0001'))
                    return onChange(ele)
                  }}
                  placeholder="Write an insightful comment."
                  customStyleMap={styleMap}
                  blockStyleFn={myBlockStyleFn}
                />
              </View>

            </View> */}

            <View style={{ flexDirection: 'column' }}>
              {/* <Editor
                //localization={{ locale: 'en', translations: editorLabels }}
                editorState={editorState}
                wrapperClassName="demo-wrapper"
                editorClassName="demo-editor"
                onChange={()=>{
                  document.getElementsByClassName("demo-editor")[1].scrollTop = parseInt(document.getElementsByClassName("demo-editor")[1].scrollHeight)+parseInt(document.getElementsByClassName("demo-editor")[1].scrollHeight);}
                }
                onEditorStateChange={onEditorStateChange}                
                toolbar={
                  {
                    options: ['inline', 'blockType', 'list','link'], // This is where you can specify what options you need in
                    //the toolbar and appears in the same order as specified
                    inline:
                    {
                      options: ['bold', 'italic', 'underline'] // this can be specified as well, toolbar wont have
                      //strikethrough, 'monospace', 'superscript', 'subscript'
                    },
                    blockType: {
                      inDropdown: false,
                      options: ['Blockquote', 'Code'],
                      className: undefined,
                      component: undefined,
                      dropdownClassName: undefined,
                    },
                    list: {
                      options: ['unordered'],
                    },
                    link:{
                      options: ['link'],
                    }
                    // image :
                    // {
                    // alignmentEnabled: false,
                    // //uploadCallback: this.UploadImageCallBack,
                    // alt: { present: true, mandatory: false },
                    // previewImage: true
                    // }
                  }
                }
                placeholder={placeholderState?"Write an insightful comment (longer than 140 characters).":''}
                editorStyle={{ height: "150px",overflowY:title.length > 0?'auto':'unset',fontSize:'13px',fontFamily: ConstantFontFamily.VerdanaFont,paddingLeft:'10px'}}
              /> */}
              {/* {console.log(draftToHtml(convertToRaw(editorState.getCurrentContent())),editorState.getCurrentContent().getPlainText('\u0001'))} */}
              {/* <TextInput
                    //disabled
                    //placeholder="Write an insightful comment."
                    //value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
                  /> */}
            </View>

          </View>
          <View
            style={{
              flexDirection: "row",
              width: "100%",
              paddingHorizontal: 10,
              alignItems: "center",
              justifyContent: "flex-start",
              zIndex:-1
            }}
          >
            <View
              style={{
                flexDirection: "row",
                width: "50%"
                
              }}
            >
              <View
                style={{
                  alignItems: "flex-start",
                  justifyContent: "flex-start"
                }}
              >
                <TouchableOpacity
                  disabled={showsubmitbutton == false ? true : false}
                  onPress={() => submitComment("delete")}
                >
                  <Icon
                    color={"#D14C4B"}
                    iconStyle={{
                      color: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center"
                    }}
                    reverse
                    name="trash"
                    type="font-awesome"
                    size={15}
                    containerStyle={{
                      alignSelf: "center"
                    }}
                  />
                </TouchableOpacity>
              </View>
              <Text
                style={{
                  color: title.length < 140 ? "#de5246" : '#009B1A',
                  fontSize: 13,
                  fontFamily: ConstantFontFamily.defaultFont,
                  marginTop: 5,
                  marginLeft: 15
                }}
              >
                 {title ?title.length <140 ?`${140-title.length} ${titleContent}`:`${2000-title.length} ${titleContent}`:''}
              </Text>
            </View>

            <View
              style={{
                flexDirection: "row",
                width: "50%",
                justifyContent: "flex-end",
                alignItems: "center",
                alignSelf: "center",
                marginTop:5
              }}
            >
              {/* <TouchableOpacity
                style={[
                  ButtonStyle.backgroundStyle,
                  { backgroundColor: "#de5246", marginRight: 10 }
                ]}
                // style={{
                //   backgroundColor: "#de5246",
                //   justifyContent: "center",
                //   alignItems: "center",
                //   borderRadius: 6,
                //   borderColor: "#000",
                //   height: 35,
                //   marginRight: 10
                // }}
                onPress={() => {
                  props.onClose();
                }}
              >
                <Text
                  style={[
                    ButtonStyle.titleStyle,
                    {
                      color: "#fff",
                      // marginHorizontal: 20,
                      marginVertical: 8
                    }
                  ]}
                >
                  Cancel
                </Text>
              </TouchableOpacity> */}
               <Button
                  title="Cancel"
                  titleStyle={ButtonStyle.cwtitleStyle}
                  buttonStyle={[
                    ButtonStyle.crbackgroundStyle,
                    // {
                    //   backgroundColor: title.length < 140 ? "#e1e1e1" : "#009B1A"
                    // }
                  ]}
                  containerStyle={[
                    ButtonStyle.containerStyle,
                    {
                      marginTop: 0,
                      marginBottom:5
                    }
                  ]}
                  //containerStyle={ButtonStyle.containerStyle}
                  
                    onPress={() => {
                      props.onClose();
                    }}

                //onClick={() => onChange(EditorState.push(editorState, ContentState.createFromText('')))}
                />
                <Button
                  title="Submit"
                  titleStyle={title.length < 140 ?ButtonStyle.cdtitleStyle:ButtonStyle.cwtitleStyle}
                  buttonStyle={title.length < 140 ?ButtonStyle.cdbackgroundStyle:ButtonStyle.cgbackgroundStyle

                  //   [
                  //   ButtonStyle.backgroundStyle,
                  //   {
                  //     backgroundColor: title.length < 140 ? "#e1e1e1" : "#009B1A"
                  //   }
                  // ]
                }
                  containerStyle={[
                    ButtonStyle.containerStyle,
                    {
                      marginTop: 0,
                      marginBottom:5
                    }
                  ]}
                  //containerStyle={ButtonStyle.containerStyle}
                  disabled={
                    (props.initial == "main" &&
                      title.replace(/ /g, "").length <= 50) ||
                      (props.initial != "main" &&
                        title.replace(/ /g, "").length <= 25) ||
                      showsubmitbutton == false
                      ? true
                      : false
                  }
                  onPress={
                    () => {
                      //onChange(EditorState.push(editorState, ContentState.createFromText('')))
                      return submitComment("edit")
                    }

                }

                //onClick={() => onChange(EditorState.push(editorState, ContentState.createFromText('')))}
                />
              {/* <TouchableOpacity
                style={ButtonStyle.backgroundStyle}
                // style={{
                //   backgroundColor:
                //     (props.initial == "main" &&
                //       title.replace(/ /g, "").length <= 50) ||
                //     (props.initial != "main" &&
                //       title.replace(/ /g, "").length <= 25)
                //       ? "#e1e1e1"
                //       : "#000",
                //   justifyContent: "center",
                //   alignItems: "center",
                //   borderRadius: 6,
                //   borderColor: "#000",
                //   height: 35
                // }}
                onPress={() => submitComment("edit")}
                disabled={
                  (props.initial == "main" &&
                    title.replace(/ /g, "").length <= 50) ||
                    (props.initial != "main" &&
                      title.replace(/ /g, "").length <= 25) ||
                    showsubmitbutton == false
                    ? true
                    : false
                }
              >
                <Text
                  style={[
                    ButtonStyle.titleStyle,
                    {
                      // marginHorizontal: 20,
                      marginVertical: 8
                    }
                  ]}
                // style={{
                //   color: "#fff",
                //   borderRadius: 6,
                //   marginHorizontal: 30,
                //   marginVertical: 10,
                //   fontFamily: ConstantFontFamily.MontserratBoldFont
                // }}
                >
                  Submit
                </Text>
              </TouchableOpacity> */}
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};
const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  PostCommentDetails: state.PostCommentDetailsReducer.get("PostCommentDetails"),
  parentPostId: state.PostCommentDetailsReducer.get("PostId")
});

const mapDispatchToProps = dispatch => ({
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  clikId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  EditCommentCard
);

const styleMap = {
  'CODE': {
    'backgroundColor': 'red',
    'fontFamily': '"Inconsolata", "Menlo", "Consolas", monospace',
    'fontSize': 16,
    'padding': 2,
  },
  'HIGHLIGHT': {
    //'backgroundColor': 'red',
    //'padding': 5,
    'color': "#000",
    'fontFamily': ConstantFontFamily.defaultFont,
    //'fontSize': 18,
    'width': "100%",
    'borderRadius': 6,
  },
  'superFancyBlockquote': {
    'color': '#999',
    'font-family': 'Hoefler Text',
    'font-style': 'italic',
    'text-align': 'center',
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: ConstantColors.customeBackgroundColor,
    padding: Platform.OS == "web" ? 10 : 0
  },
  totalFollowCount: {
    marginTop: 15,
    color: "#ffffff",
    alignItems: "center"
  },
  containerFollow: {
    justifyContent: "flex-end",
    marginRight: 10
  },
  TotalcontainerFollow: {
    flexDirection: "row",
    justifyContent: "flex-end",
    marginRight: 12,
    marginBottom: 10
  },
  profileContainer: {
    marginTop: 10
  },
  totalProfileContainer: {
    alignItems: "center",
    justifyContent: "center"
  },
  image: {
    width: "100%",
    height: hp("40%")
  },
  followbtnStyle: {
    height: 30,
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#1A1819",
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 30,
    paddingRight: 30,
    borderColor: "#fff",
    borderWidth: 2,
    borderRadius: 5
  },
  bgContainerProfile: {
    margin: 0,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  usertext: {
    color: "#fff",
    fontSize: 12,
    fontFamily: ConstantFontFamily.defaultFont
  },
  userblacktext: {
    color: "#fff",
    fontSize: 12,
    fontFamily: ConstantFontFamily.defaultFont
  },
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderRadius: 4,
    backgroundColor: "#E8F5FA",
    color: "#4169e1",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  inputAndroid: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    borderRadius: 8,
    backgroundColor: "#E8F5FA",
    color: "#4169e1",
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  TextStyle: {
    textAlign: "center"
  }
});
function updateNestedArray(input, parentId, newData, isEdit) {
  let key, value, outputValue;
  if (input === null || typeof input !== "object") {
    return input;
  }
  outputValue = Array.isArray(input) ? [] : {};
  for (key in input) {
    value = input[key];
    if (
      value !== null &&
      !!value["node"] &&
      !!value["comments"] &&
      value.node.id === parentId
    ) {
      let clone = { ...value };
      if (isEdit) {
        alert(newData.text);
        //find index of edit data
        let editCommentIndex = value.comments.edges.findIndex(
          comment => comment.node.id === newData.id
        );
        let dataUpdate = { ...clone.comments.edges[editCommentIndex] };
        dataUpdate.node.text = newData.text;
        // delete previous object
        clone.comments.edges.splice(editCommentIndex, 1);
        // set first of the edages array
        clone.comments.edges.unshift(dataUpdate);

        outputValue[key] = updateNestedArray(clone, parentId, newData, isEdit);
      } else {
        value.comments.edges.unshift(newData);
        outputValue[key] = updateNestedArray(clone, parentId, newData, isEdit);
      }
    } else {
      outputValue[key] = updateNestedArray(value, parentId, newData, isEdit);
    }
  }
  return outputValue;
}
