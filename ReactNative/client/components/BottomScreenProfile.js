import React, { Component } from "react";
import { View, Text, Dimensions, StyleSheet } from "react-native";
import IconMenu from "./IconMenu";
import NavigationService from "../library/NavigationService";
import RNPickerSelect from "react-native-picker-select";
import { connect } from "react-redux";
import { Icon } from "react-native-elements";
import ConstantFontFamily from "../constants/FontFamily";

const roleWiseViewOption = [
  {
    label: "Trending",
    value: "Trending",
  },
  {
    label: "New",
    value: "New",
  },
  {
    label: "Bookmarks",
    value: "Bookmarks",
  },
  {
    label: "Reported",
    value: "Reported",
  },
];

const roleWiseTabs = [
  {
    label: "Profile",
    value: "FEED",
    key: 0
  },
  {
    label: "Members",
    value: "USERS",
    key: 1
  },
  {
    label: "Applications",
    value: "APPLICATIONS",
    key: 2
  }
]

class BottomScreenProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabType: "Trending",
      // tabTypeProfile: "FEED",
      openCreateCommentStatus: false,

    };
    this.inputRefs = {};
  }

  render() {
    return (
      <View
        style={{
          width: "100%",
          backgroundColor:
            Dimensions.get("window").width <= 750 ? "#fff" : "#000",
          height: 50,
          flexDirection: "row",
          justifyContent: 'space-evenly',
          alignItems: "center",
          borderTopWidth: 1,
          borderTopColor: "#c5c5c5",
          // paddingHorizontal:20
        }}

      >
        <IconMenu navigation={NavigationService} />

        <View
          style={{
            borderRadius: 8,
            borderColor: "#c5c5c5",
            borderWidth: 1,
            width: 100
          }}
        >
          <RNPickerSelect
            placeholder={{}}
            items={
              roleWiseTabs
            }
            onValueChange={(itemValue, itemIndex) => {
              this.setState({ tabTypeProfile: itemValue }, () => {
                console.log(this.state.tabTypeProfile, 'bottom', itemValue, roleWiseTabs)
                this.props.SetProfileTabView(this.state.tabTypeProfile);
              });
            }}
            value={this.state.tabTypeProfile}
            // useNativeAndroidPickerStyle={false}              
            style={{ ...styles }}
          />
        </View>
        
        <View
          style={{
            borderRadius: 8,
            borderColor: "#c5c5c5",
            borderWidth: 1,
            width: 104
          }}
        >
          <RNPickerSelect
            placeholder={{}}
            items={
              roleWiseViewOption
            }
            onValueChange={(itemValue, itemIndex) => {
              this.setState({ tabType: itemValue }, () => {
                this.props.setTabView(this.state.tabType);
              });
            }}
            value={this.state.tabType}
            // useNativeAndroidPickerStyle={false}              
            style={{ ...styles }}
          />
        </View>
      </View>
    );
  }
}
const mapDispatchToProps = (dispatch) => ({
  setTabView: (payload) => dispatch({ type: "SET_TAB_VIEW", payload }),
  SetProfileTabView: (payload) => dispatch({ type: "SET_PROFILE_TAB_VIEW", payload }),
  openCreateComment: (payload) =>
    dispatch({ type: "OPEN_CREATE_COMMENT", payload }),
});
export default connect(null, mapDispatchToProps)(BottomScreenProfile);

const styles = StyleSheet.create({
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
    fontSize: 14,
    // fontWeight: "bold",
    fontFamily: ConstantFontFamily.VerdanaFont,
    textAlign: "center",
  },
  inputAndroid: {
    //   width: 145,
    paddingHorizontal: 10,
    paddingVertical: 1,
    borderWidth: 1,
    borderColor: "#fff",
    borderRadius: 20,
    color: "#000",
    backgroundColor: "white",
    fontSize: 14,
    // fontWeight: "bold",
    fontFamily: ConstantFontFamily.VerdanaFont,
    textAlign: "center",
  },

});

