import React from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { Icon } from "react-native-elements";
import ConstantFontFamily from "../constants/FontFamily";

const TataStructure = props => {
  const { item, isRoot, topicName } = props;
  return (
    <View key={item.name} style={{ marginLeft: !isRoot ? 25 : 0 }}>
      {item.name !== "dynamicText" && (
        <TouchableOpacity
          style={{
            marginTop: 10,
            height: 30,
            alignSelf: "flex-start",
            padding: 5,
            backgroundColor: !isRoot ? "#e3f9d5" : "#e3f9d5",            
            borderRadius: 6
          }}
        >
          <Text
            style={{
              width: "100%",
              color: !isRoot ? "#009B1A" : "#009B1A",
              fontSize: 15,
              fontWeight: "bold",
              fontFamily: ConstantFontFamily.MontserratBoldFont
            }}
          >
            /{item.name.toLowerCase()}
          </Text>
        </TouchableOpacity>
      )}
      {item.name === "dynamicText" && topicName ? (
        <TouchableOpacity
          style={{
            marginTop: 10,
            height: 30,
            alignSelf: "flex-start",
            padding: 5,
            backgroundColor: !isRoot ? "#e3f9d5" : "#e3f9d5",
            borderRadius: 6,
            flexDirection: "row"
          }}
        >
          {topicName && (
            <Icon
              color={"#009B1A"}
              iconStyle={{
                padding: 3,
                justifyContent: "center",
                alignItems: "center"
              }}
              name="circle"
              type="font-awesome"
              size={12}
              containerStyle={{
                marginRight: 5
              }}
            />
          )}

          <Text
            style={{
              width: "100%",
              color: !isRoot ? "#009B1A" : "#009B1A",
              fontSize: 15,
              fontWeight: "bold",
              fontFamily: ConstantFontFamily.MontserratBoldFont
            }}
          >
            /{topicName.toLowerCase()}
          </Text>
        </TouchableOpacity>
      ) : null}
      {item.children &&
        item.children.map((el, index) => {
          return <TataStructure item={el} topicName={topicName} />;
        })}
    </View>
  );
};

export default TataStructure;
