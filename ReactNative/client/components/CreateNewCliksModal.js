import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { Button, Icon } from "react-native-elements";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import "../components/Firebase";
import ConstantFontFamily from "../constants/FontFamily";
import ButtonStyle from "../constants/ButtonStyle";

const CreateNewCliksModal = props => {
  return (
    <View
      style={{
        width: "100%"
      }}
    >
      <Hoverable>
        {isHovered => (
          <TouchableOpacity
            style={{
              flexDirection: "row",
              justifyContent: "flex-start",
              flex: 1,
              position: "absolute",
              zIndex: 999999,
              left: 0,
              top: 0
            }}
            onPress={props.onClose}
          >
            <Icon
              color={isHovered == true ? "rgba(256,256,256,0.4)" : "#000"}
              iconStyle={{
                color: "#fff",
                justifyContent: "center",
                alignItems: "center"
              }}
              reverse
              name="close"
              type="antdesign"
              size={16}
            />
          </TouchableOpacity>
        )}
      </Hoverable>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          backgroundColor: "#000",
          alignItems: "center",
          height: 50,
          borderTopLeftRadius: 6,
          borderTopRightRadius: 6
        }}
      >
        <Image
          source={
            Platform.OS == "web" && props.getCurrentDeviceWidthAction > 750
              ? require("../assets/image/weclickd-logo.png")
              : Platform.OS == "web"
              ? require("../assets/image/weclickd-logo.png")
              : require("../assets/image/weclickd-logo-only-icon.png")
          }
          style={
            Platform.OS == "web" && props.getCurrentDeviceWidthAction > 750
              ? {
                  height: 30,
                  width: Platform.OS == "web" ? 90 : 30,
                  padding: 0,
                  margin: 0,
                  marginVertical: 10
                }
              : {
                  height: 30,
                  width: Platform.OS == "web" ? 90 : 30,
                  padding: 0,
                  margin: 0,
                  marginVertical: 10
                }
          }
        />
      </View>

      <View
        style={{
          borderRadius: 6,
          width: "100%",
          backgroundColor: "#fff"
        }}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            backgroundColor: "#fff",
            height: hp("60%"),
            padding: 20
          }}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                color: "#000",
                fontFamily: ConstantFontFamily.defaultFont,
                fontSize: 16,
                fontWeight: "bold",
                marginVertical: 10
              }}
            >
              Cliks are groups to foster a safe place for higher quality
              discussion. A clik should be a group of people that have a high
              level of expertise in a particular subject in an academic field.
            </Text>
            <Text
              style={{
                color: "#000",
                fontFamily: ConstantFontFamily.defaultFont,
                fontSize: 16,
                fontWeight: "bold",
                marginVertical: 10
              }}
            >
              For example, the clik #AstroPhysicsPHD can be a group of people
              who are extremely knowledgble about Astro Physics. Only people who
              can prove their accolades should be invited. #Programmers is
              allowed as well.
            </Text>
            <Text
              style={{
                color: "#000",
                fontFamily: ConstantFontFamily.defaultFont,
                fontSize: 16,
                fontWeight: "bold",
                marginVertical: 10
              }}
            >
              Currently, we do not allow groups like #UCLAAlumni. We may allow
              groups associated with established organizations in the future.
            </Text>
            <Text
              style={{
                color: "#000",
                fontFamily: ConstantFontFamily.defaultFont,
                fontSize: 16,
                fontWeight: "bold",
                marginVertical: 10
              }}
            >
              A clik called #PhysicsLovers will not be approved since the group
              is not restrictive enough. Instead, people interested in physics
              should participate in the public discussion in posts that are
              shared to the topic /Physics.
            </Text>
          </View>
        </ScrollView>
        <View
          style={{
            width: "100%",
            flexDirection: "row",
            alignSelf: "center",
            margin: 10
          }}
        >
          <View
            style={{
              width: "50%",
              justifyContent: "center",
              flexDirection: "row"
            }}
          >
            <Button
              onPress={props.onClose}
              color="#fff"
              title="Back"
              titleStyle={ButtonStyle.titleStyle}
              buttonStyle={ButtonStyle.backgroundStyle}
              containerStyle={ButtonStyle.containerStyle}
            />
          </View>
          <View
            style={{
              width: "50%",
              justifyContent: "center",
              flexDirection: "row"
            }}
          >
            <Button
              onPress={() => {
                props.navigation.navigate("createclik"), props.onClose();
              }}
              color="#fff"
              title="Continue"
              titleStyle={ButtonStyle.titleStyle}
              buttonStyle={ButtonStyle.backgroundStyle}
              containerStyle={ButtonStyle.containerStyle}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default connect(null, null)(CreateNewCliksModal);

export const styles = {
  LogoImageStyle: {
    height: 80,
    width: 235
  },
  LogoContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    margin: 25
  }
};
