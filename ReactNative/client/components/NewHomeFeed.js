import firebase from "firebase/app";
import "firebase/auth";
import React, { createRef, lazy, Suspense } from "react";
import { graphql } from "react-apollo";
import {
  Animated,
  AsyncStorage,
  Dimensions,
  FlatList,
  Platform,
  Text,
  View,
  YellowBox
} from "react-native";
import { Button, Icon } from "react-native-elements";
import { FlatGrid } from "react-native-super-grid";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setCreateAccount } from "../actionCreator/CreateAccountAction";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingTopics } from "../actionCreator/TrendingTopicsAction";
import { getTrendingUsers } from "../actionCreator/TrendingUsersAction";
import {
  saveUserLoginDaitails,
  setLoginStatus
} from "../actionCreator/UserAction";
import { setUserApproachAction } from "../actionCreator/UserApproachAction";
import { setUSERNAMEMODALACTION } from "../actionCreator/UsernameModalAction";
import applloClient from "../client";
import NewHeader from "../components/NewHeader";
import ConstantFontFamily from "../constants/FontFamily";
import {
  AdminExternalFeedMutation,
  AdminHomeFeedMutation,
  ExternalFeedMutation,
  HomeFeedMutation
} from "../graphqlSchema/graphqlMutation/FeedMutation";
import {
  AdminUserFeedMutation,
  UserFeedMutation
} from "../graphqlSchema/graphqlMutation/PostMutation";
import {
  AdminClikFeed,
  AdminTopicFeed,
  ClikFeed,
  TopicFeed
} from "../graphqlSchema/graphqlMutation/TrendingMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { retry } from "../library/Helper";
import ShadowSkeleton from "./ShadowSkeleton";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import AdminCompactView from "../components/AdminCompactView";
import AdminFeedList from "../components/AdminFeedList";
import CompactFeedList from "../components/CompactFeedList";
import FeedList from "../components/FeedList";
import ButtonStyle from "../constants/ButtonStyle";
// const AdminCompactView = lazy(() =>
//   retry(() => import("../components/AdminCompactView"))
// );
// const AdminFeedList = lazy(() =>
//   retry(() => import("../components/AdminFeedList"))
// );
// const CompactFeedList = lazy(() =>
//   retry(() => import("../components/CompactFeedList"))
// );
// const FeedList = lazy(() => retry(() => import("../components/FeedList")));

let feedID = [];
class NewHomeFeed extends React.PureComponent {
  state = {
    modalVisible: false,
    showsVerticalScrollIndicatorView: false,
    currentScreentWidth: 0,
    data: [],
    page: 1,
    loading: true,
    loadingMore: false,
    refreshing: false,
    pageInfo: null,
    error: null,
    apiCall: true,
    routes: [
      { key: "first", title: "Trending" },
      { key: "second", title: "New" },
      { key: "third", title: "Discussing" }
    ],
    index: 0,
    activeFeed: -1,
    HomeFeedError: false,
    showEmptyIcon: false,
    activeId: "",
    activeTitle: "",
    loadSkeleton: false
  };
  flatListRef = createRef();
  constructor(props) {
    super(props);
    this.Pagescrollview = null;
    this.circleAnimatedValue = new Animated.Value(0);
    YellowBox.ignoreWarnings(["VirtualizedLists should never be nested"]);
    this.onEndReachedCalledDuringMomentum = true;
    this.baseState = this.state;
    this.onEndReachedCalledDuringMomentum = true;
    this.viewabilityConfig = {
      viewAreaCoveragePercentThreshold: 50
    };
  }

  static navigationOptions = ({ navigation }) => {
    return {
      header: () => <NewHeader navigation={navigation} customeTitle={"Home"} />
    };
  };

  circleAnimated = () => {
    this.circleAnimatedValue.setValue(0);
    Animated.timing(this.circleAnimatedValue, {
      toValue: 1,
      duration: 350
    }).start(() => {
      setTimeout(() => {
        this.circleAnimated();
      }, 1000);
    });
  };

  componentDidMount = async () => {
    await this.props.doScroll(this.flatListRef, "new");
    if (this.props.listType == "Home") {
      //  if (this.props.prevFeedList.new.data) {
      //  this.setState({ ...this.props.prevFeedList.new });
      //  } else {
      // await firebase.auth().onAuthStateChanged(async user => {
      //   if (user) {
      //this._fetchAllHomeFeeds();
      //   }
      // });
      // }
      this._fetchAllHomeFeeds();
    } else if (this.props.listType == "Clik") {
      this._fetchAllClikFeeds();
    } else if (this.props.listType == "Topic") {
      this._fetchAllTopicFeeds();
    } else if (this.props.listType == "User") {
      this._fetchAllUserFeeds();
    } else if (this.props.listType == "Feed") {
      this._fetchAllExternalFeeds();
    }

    let data = document.getElementById("NewHomeFeed");
    if (data != null) {
      await data.addEventListener("keydown", event => {
        if (event.keyCode == 83 || event.keyCode == 40) {
          this.setState({
            activeFeed: this.state.activeFeed + 1
          });
        }
        if (event.keyCode == 87 || event.keyCode == 38) {
          this.setState({
            activeFeed: this.state.activeFeed - 1
          });
        }
        if (event.keyCode == 39 || event.keyCode == 68) {
          this.setState({
            activeFeed: this.state.activeFeed + 1
          });
        }
        if (event.keyCode == 37 || event.keyCode == 65) {
          this.setState({
            activeFeed: this.state.activeFeed - 1
          });
        }
        if (this.state.activeFeed < this.getUnique(feedID).length) {
          this.setBorderColor();
        }
      });
    }
  };

  componentDidUpdate = async prevProps => {
    if (this.props.listType == "Topic" && prevProps.data != this.props.data) {
      await this.setState(this.baseState);
      this._fetchAllTopicFeeds();
    } else if (
      this.props.listType == "Clik" &&
      prevProps.data != this.props.data
    ) {
      await this.setState(this.baseState);
      this._fetchAllClikFeeds();
    } else if (
      this.props.listType == "User" &&
      prevProps.data != this.props.data
    ) {
      await this.setState(this.baseState);
      this._fetchAllUserFeeds();
    } else if (
      this.props.listType == "Feed" &&
      prevProps.data != this.props.data
    ) {
      await this.setState(this.baseState);
      this._fetchAllExternalFeeds();
    }

    // if (this.props.ActiveTab == "New" || this.props.ActiveTab == "Reported") {
    //   setTimeout(() => {
    //     if (prevProps.trendingHomeFeedId == this.props.trendingHomeFeedId) {
    //       this.props.setPostCommentDetails({
    //         id:
    //           this.state.activeId != ""
    //             ? this.state.activeId
    //             : this.state.data.length > 0
    //             ? "New" + this.state.data[0].node.id
    //             : "",
    //         title:
    //           this.state.activeTitle != ""
    //             ? this.state.activeTitle
    //             : this.state.data.length > 0
    //             ? this.state.data[0].node.title
    //             : ""
    //       });
    //       this.makeHighlight(
    //         this.state.activeId != ""
    //           ? this.state.activeId
    //           : this.state.data.length > 0
    //           ? "New" + this.state.data[0].node.id
    //           : "",
    //         this.state.activeTitle != ""
    //           ? this.state.activeTitle
    //           : this.state.data.length > 0
    //           ? this.state.data[0].node.title
    //           : ""
    //       );
    //     }
    //   }, 2000);
    // }
    setTimeout(() => {
      if (
        this.state.activeId == "" &&
        this.state.data.length > 0 &&
        (this.props.ActiveTab == "New" || this.props.ActiveTab == "Reported")
      ) {
        this.props.setPostCommentDetails({
          id: this.state.data[0].node.id,
          title: this.state.data[0].node.title
        });
        this.makeHighlight(
          this.state.data[0].node.id,
          this.state.data[0].node.title
        );
        this.setBorderColor();
      } else if (this.state.data.length == 0) {
        this.props.setPostCommentReset({
          payload: [],
          postId: "",
          title: "",
          loading: true
        });
      }
    }, 2000);
  };

  setBorderColor = async () => {
    for (let i = 0; i < this.getUnique(feedID).length; i++) {
      let data = document.getElementById(this.getUnique(feedID)[i]);
      if (data != null && Dimensions.get("window").width >= 1000) {
        // document.getElementById(this.getUnique(feedID)[i]).style.borderColor =
        //   i == this.state.activeFeed ? "green" : "#c5c5c5";
        if (i == this.state.activeFeed) {
          document.getElementById(this.getUnique(feedID)[i]).click();
        }
      }
    }
  };

  componentWillUnmount = () => {
    document.removeEventListener("keydown", this.componentDidMount());
  };
  _handleLoadMoreByBtnClick = () => {
    this.setState(
      (prevState, nextProps) => ({}),
      () => {
        if (this.state.apiCall == true) {
          this.setState({
            apiCall: false
          });
          if (this.props.listType == "Home") {
            this._fetchAllHomeFeeds();
          } else if (this.props.listType == "Clik") {
            this._fetchAllClikFeeds();
          } else if (this.props.listType == "Topic") {
            this._fetchAllTopicFeeds();
          } else if (this.props.listType == "User") {
            this._fetchAllUserFeeds();
          } else if (this.props.listType == "Feed") {
            this._fetchAllExternalFeeds();
          }
        }
      }
    );
  };
  _handleLoadMore = distanceFromEnd => {
    this.setState(
      (prevState, nextProps) => ({}),
      () => {
        if (
          0 <= distanceFromEnd &&
          distanceFromEnd <= 5 &&
          this.state.apiCall == true
        ) {
          this.setState({
            apiCall: false
          });
          if (this.props.listType == "Home") {
            this._fetchAllHomeFeeds();
          } else if (this.props.listType == "Clik") {
            this._fetchAllClikFeeds();
          } else if (this.props.listType == "Topic") {
            this._fetchAllTopicFeeds();
          } else if (this.props.listType == "User") {
            this._fetchAllUserFeeds();
          } else if (this.props.listType == "Feed") {
            this._fetchAllExternalFeeds();
          }
        }
      }
    );
  };

  _fetchAllHomeFeeds = () => {
    let __self = this;
    const { page, pageInfo } = this.state;

    if (pageInfo == null) {
      this.setState({
        loadingMore: true,
        loadSkeleton: true
      });
      applloClient
        .query({
          query:
            this.props.isAdmin == true
              ? AdminHomeFeedMutation
              : HomeFeedMutation,
          variables: {
            first: 12,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "NEW"
          },
          fetchPolicy: "no-cache"
        })
        .then(async response => {
          __self.setState({
            loading: false,
            data: Array.from(response.data.home_feed.posts.edges),
            pageInfo: response.data.home_feed.posts.pageInfo,
            page: page + 1,
            apiCall: true,
            loadingMore:
              response.data.home_feed.posts.pageInfo != null &&
              response.data.home_feed.posts.pageInfo.hasNextPage == true
                ? true
                : false,
            HomeFeedError: false,
            showEmptyIcon:
              response.data.home_feed.posts.edges.length == 0 ? true : false,
            loadSkeleton: false
          });
          this.props.setNewHomeFeed(response.data.home_feed.posts.edges);
          // if (
          //   this.props.ActiveTab == "New" ||
          //   this.props.ActiveTab == "Reported"
          // ) {
          //   await this.props.setPostCommentDetails({
          //     id: response.data.home_feed.posts.edges[0].node.id,
          //     title: response.data.home_feed.posts.edges[0].node.title
          //   });
          // }
          __self.props.setPrevList(
            {
              loading: false,
              data: Array.from(response.data.home_feed.posts.edges),
              pageInfo: response.data.home_feed.posts.pageInfo,
              page: page + 1,
              apiCall: true,
              loadingMore:
                response.data.home_feed.posts.pageInfo != null &&
                response.data.home_feed.posts.pageInfo.hasNextPage == true
                  ? true
                  : false,
              HomeFeedError: false
            },
            "new"
          );
          // if (
          //   this.props.ActiveTab == "New" ||
          //   this.props.ActiveTab == "Reported"
          // ) {
          //   setTimeout(() => {
          //     this.makeHighlight(
          //       response.data.home_feed.posts.edges[0].node.id,
          //       response.data.home_feed.posts.edges[0].node.title
          //     );
          //   }, 1000);
          // }
        })
        .catch(e => {
          console.log(e);
          __self.setState({
            data: [],
            loading: false,
            loadingMore: false,
            HomeFeedError: true,
            loadSkeleton: false
          });
          // if (
          //   e.message ==
          //     "Network error: Unexpected token T in JSON at position 0" ||
          //   "Network error: Invalid token specified"
          // ) {
          //   this.getFirebaseToken();
          // }
        });
    } else if (pageInfo != null && pageInfo.hasNextPage == true) {
      this.setState({
        loadingMore: true,
        loading: true
      });
      applloClient
        .query({
          query:
            this.props.isAdmin == true
              ? AdminHomeFeedMutation
              : HomeFeedMutation,
          variables: {
            first: 12,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "NEW"
          },
          fetchPolicy: "no-cache"
        })
        .then(response => {
          __self.setState({
            loading: false,
            data: this.state.data.concat(response.data.home_feed.posts.edges),
            pageInfo: response.data.home_feed.posts.pageInfo,
            apiCall: true,
            loadingMore:
              response.data.home_feed.posts.pageInfo != null &&
              response.data.home_feed.posts.pageInfo.hasNextPage == true
                ? true
                : false,
            HomeFeedError: false,
            showEmptyIcon:
              response.data.home_feed.posts.edges.length == 0 ? true : false
          });
          this.props.setNewHomeFeed(
            this.state.data.concat(response.data.home_feed.posts.edges)
          );
          __self.props.setPrevList(
            {
              loading: false,
              data: this.state.data.concat(response.data.home_feed.posts.edges),
              pageInfo: response.data.home_feed.posts.pageInfo,
              apiCall: true,
              loadingMore:
                response.data.home_feed.posts.pageInfo != null &&
                response.data.home_feed.posts.pageInfo.hasNextPage == true
                  ? true
                  : false,
              HomeFeedError: false
            },
            "new"
          );
          //scroll to top handle
          // if (this.flatListRef.current instanceof FlatGrid)
          //   this.flatListRef.current.flatList.scrollToOffset({
          //     x: 0,
          //     y: 0,
          //     animated: true
          //   });
          // else
          //   this.flatListRef.current &&
          //     this.flatListRef.current.scrollToOffset({
          //       x: 0,
          //       y: 0,
          //       animated: true
          //     });
        })
        .catch(e => {
          console.log(e);
          __self.setState({
            data: [],
            loading: false,
            loadingMore: false,
            HomeFeedError: true
          });
          // if (
          //   e.message ==
          //     "Network error: Unexpected token T in JSON at position 0" ||
          //   "Network error: Invalid token specified"
          // ) {
          //   this.getFirebaseToken();
          // }
        });
    }
  };

  _fetchAllTopicFeeds = () => {
    let __self = this;
    const { page, pageInfo } = this.state;
    if (pageInfo == null) {
      this.setState({
        loadingMore: true,
        loadSkeleton: true
      });
      applloClient
        .query({
          query: this.props.isAdmin == true ? AdminTopicFeed : TopicFeed,
          variables: {
            id: "Topic:" + __self.props.data,
            first: 24,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "NEW"
          },
          fetchPolicy: "no-cache"
        })
        .then(response => {
          __self.setState({
            loading: false,
            data: Array.from(response.data.topic.posts.edges),
            pageInfo: response.data.topic.posts.pageInfo,
            page: page + 1,
            apiCall: true,
            loadingMore:
              response.data.topic.posts.pageInfo != null &&
              response.data.topic.posts.pageInfo.hasNextPage == true
                ? true
                : false,
            showEmptyIcon:
              response.data.topic.posts.edges.length == 0 ? true : false,
            loadSkeleton: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else if (pageInfo != null && pageInfo.hasNextPage == true) {
      this.setState({
        loadingMore: true,
        loading: true
      });
      applloClient
        .query({
          query: this.props.isAdmin == true ? AdminTopicFeed : TopicFeed,
          variables: {
            id: "Topic:" + __self.props.data,
            first: 24,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "NEW"
          },
          fetchPolicy: "no-cache"
        })
        .then(response => {
          __self.setState({
            loading: false,
            data: this.state.data.concat(response.data.topic.posts.edges),
            pageInfo: response.data.topic.posts.pageInfo,
            apiCall: true,
            loadingMore:
              response.data.topic.posts.pageInfo != null &&
              response.data.topic.posts.pageInfo.hasNextPage == true
                ? true
                : false,
            showEmptyIcon:
              response.data.topic.posts.edges.length == 0 ? true : false
          });
          //scroll to top handle
          // if (this.flatListRef.current instanceof FlatGrid)
          //   this.flatListRef.current.flatList.scrollToOffset({
          //     x: 0,
          //     y: 0,
          //     animated: true
          //   });
          // else
          //   this.flatListRef.current &&
          //     this.flatListRef.current.scrollToOffset({
          //       x: 0,
          //       y: 0,
          //       animated: true
          //     });
        })
        .catch(e => {
          console.log(e);
        });
    }
  };
  _fetchAllClikFeeds = () => {
    let __self = this;
    const { page, pageInfo } = this.state;
    if (pageInfo == null) {
      this.setState({
        loadingMore: true,
        loadSkeleton: true
      });
      applloClient
        .query({
          query: this.props.isAdmin == true ? AdminClikFeed : ClikFeed,
          variables: {
            id: "Clik:" + __self.props.data,
            first: 24,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "NEW"
          },
          fetchPolicy: "no-cache"
        })
        .then(response => {
          __self.setState({
            loading: false,
            data: Array.from(response.data.clik.posts.edges),
            pageInfo: response.data.clik.posts.pageInfo,
            page: page + 1,
            apiCall: true,
            loadingMore:
              response.data.clik.posts.pageInfo != null &&
              response.data.clik.posts.pageInfo.hasNextPage == true
                ? true
                : false,
            showEmptyIcon:
              response.data.clik.posts.edges.length == 0 ? true : false,
            loadSkeleton: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else if (pageInfo != null && pageInfo.hasNextPage == true) {
      this.setState({
        loadingMore: true,
        loading: true
      });
      applloClient
        .query({
          query: this.props.isAdmin == true ? AdminClikFeed : ClikFeed,
          variables: {
            id: "Clik:" + __self.props.data,
            first: 24,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "NEW"
          },
          fetchPolicy: "no-cache"
        })
        .then(response => {
          __self.setState({
            loading: false,
            data: this.state.data.concat(response.data.clik.posts.edges),
            pageInfo: response.data.clik.posts.pageInfo,
            apiCall: true,
            loadingMore:
              response.data.clik.posts.pageInfo != null &&
              response.data.clik.posts.pageInfo.hasNextPage == true
                ? true
                : false,
            showEmptyIcon:
              response.data.clik.posts.edges.length == 0 ? true : false
          });
          //scroll to top handle
          // if (this.flatListRef.current instanceof FlatGrid)
          //   this.flatListRef.current.flatList.scrollToOffset({
          //     x: 0,
          //     y: 0,
          //     animated: true
          //   });
          // else
          //   this.flatListRef.current &&
          //     this.flatListRef.current.scrollToOffset({
          //       x: 0,
          //       y: 0,
          //       animated: true
          //     });
        })
        .catch(e => {
          console.log(e);
        });
    }
  };
  _fetchAllUserFeeds = () => {
    let __self = this;
    const { page, pageInfo } = this.state;
    if (pageInfo == null) {
      this.setState({
        loadingMore: true,
        loadSkeleton: true
      });
      applloClient
        .query({
          query:
            this.props.isAdmin == true
              ? AdminUserFeedMutation
              : UserFeedMutation,
          variables: {
            id: "User:" + __self.props.data,
            first: 24,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "NEW"
          },
          fetchPolicy: "no-cache"
        })
        .then(response => {
          __self.setState({
            loading: false,
            data: Array.from(response.data.user.posts.edges),
            pageInfo: response.data.user.posts.pageInfo,
            page: page + 1,
            apiCall: true,
            loadingMore:
              response.data.user.posts.pageInfo != null &&
              response.data.user.posts.pageInfo.hasNextPage == true
                ? true
                : false,
            showEmptyIcon:
              response.data.user.posts.edges.length == 0 ? true : false,
            loadSkeleton: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else if (pageInfo != null && pageInfo.hasNextPage == true) {
      this.setState({
        loadingMore: true,
        loading: true
      });
      applloClient
        .query({
          query:
            this.props.isAdmin == true
              ? AdminUserFeedMutation
              : UserFeedMutation,
          variables: {
            id: "User:" + __self.props.data,
            first: 24,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "NEW"
          },
          fetchPolicy: "no-cache"
        })
        .then(response => {
          __self.setState({
            loading: false,
            data: this.state.data.concat(response.data.user.posts.edges),
            pageInfo: response.data.user.posts.pageInfo,
            apiCall: true,
            loadingMore:
              response.data.user.posts.pageInfo != null &&
              response.data.user.posts.pageInfo.hasNextPage == true
                ? true
                : false,
            showEmptyIcon:
              response.data.user.posts.edges.length == 0 ? true : false
          });
          //scroll to top handle
          // if (this.flatListRef.current instanceof FlatGrid)
          //   this.flatListRef.current.flatList.scrollToOffset({
          //     x: 0,
          //     y: 0,
          //     animated: true
          //   });
          // else
          //   this.flatListRef.current &&
          //     this.flatListRef.current.scrollToOffset({
          //       x: 0,
          //       y: 0,
          //       animated: true
          //     });
        })

        .catch(e => {
          console.log(e);
        });
    }
  };
  _fetchAllExternalFeeds = () => {
    let __self = this;
    const { page, pageInfo } = this.state;
    if (pageInfo == null) {
      this.setState({
        loadingMore: true,
        loadSkeleton: true
      });
      applloClient
        .query({
          query:
            this.props.isAdmin == true
              ? AdminExternalFeedMutation
              : ExternalFeedMutation,
          variables: {
            id: "ExternalFeed:" + __self.props.data,
            first: 24,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "NEW"
          },
          fetchPolicy: "no-cache"
        })
        .then(response => {
          __self.setState({
            loading: false,
            data: Array.from(response.data.external_feed.posts.edges),
            pageInfo: response.data.external_feed.posts.pageInfo,
            page: page + 1,
            apiCall: true,
            loadingMore:
              response.data.external_feed.posts.pageInfo != null &&
              response.data.external_feed.posts.pageInfo.hasNextPage == true
                ? true
                : false,
            showEmptyIcon:
              response.data.external_feed.posts.edges.length == 0
                ? true
                : false,
            loadSkeleton: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else if (pageInfo != null && pageInfo.hasNextPage == true) {
      this.setState({
        loadingMore: true,
        loading: true
      });
      applloClient
        .query({
          query:
            this.props.isAdmin == true
              ? AdminExternalFeedMutation
              : ExternalFeedMutation,
          variables: {
            id: "ExternalFeed:" + __self.props.data,
            first: 24,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "NEW"
          },
          fetchPolicy: "no-cache"
        })
        .then(response => {
          __self.setState({
            loading: false,
            data: this.state.data.concat(
              response.data.external_feed.posts.edges
            ),
            pageInfo: response.data.external_feed.posts.pageInfo,
            apiCall: true,
            loadingMore:
              response.data.external_feed.posts.pageInfo != null &&
              response.data.external_feed.posts.pageInfo.hasNextPage == true
                ? true
                : false,
            showEmptyIcon:
              response.data.external_feed.posts.edges.length == 0 ? true : false
          });
          //scroll to top handle
          // if (this.flatListRef.current instanceof FlatGrid)
          //   this.flatListRef.current.flatList.scrollToOffset({
          //     x: 0,
          //     y: 0,
          //     animated: true
          //   });
          // else
          //   this.flatListRef.current &&
          //     this.flatListRef.current.scrollToOffset({
          //       x: 0,
          //       y: 0,
          //       animated: true
          //     });
        })
        .catch(e => {
          console.log(e);
        });
    }
  };

  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        if (this.props.listType == "Home") {
          this._fetchAllHomeFeeds();
        } else if (this.props.listType == "Clik") {
          this._fetchAllClikFeeds();
        } else if (this.props.listType == "Topic") {
          this._fetchAllTopicFeeds();
        } else if (this.props.listType == "User") {
          this._fetchAllUserFeeds();
        } else if (this.props.listType == "Feed") {
          this._fetchAllExternalFeeds();
        }
      }
    );
  };

  _renderLogoutFooter = () => {
    return (
      this.state.data.length > 0 && (
        <View
          style={{
            flex: 1,
            alignContent: "center",
            alignItems: "center"
          }}
        >
          <Icon
            color={"#000"}
            iconStyle={{
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center"
            }}
            name="arrow-down"
            type="font-awesome"
            size={20}
            containerStyle={{
              alignSelf: "center"
            }}
          />
          <Hoverable>
            {isHovered => (
              <Text
                style={{
                  color: isHovered == true ? "#009B1A" : "#000",
                  fontSize: 12,
                  textAlign: "center",
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
              >
                <Text
                  onPress={() => this.props.setLoginModalStatus(true)}
                  style={{
                    textDecorationLine: "underline",
                    fontSize: 12,
                    textAlign: "center",
                    fontFamily: ConstantFontFamily.MontserratBoldFont
                  }}
                >
                  Login
                </Text>{" "}
                to see more...
              </Text>
            )}
          </Hoverable>
        </View>
      )
    );
  };
  _renderFooter = () => {
    if (!this.state.loadingMore) return null;
    return null;
  };

  onViewableItemsChanged = ({ viewableItems, changed }) => {
    let perLoadDataCount = this.props.listType == "Home" ? 12 : 24;
    let halfOfLoadDataCount = perLoadDataCount / 2;
    let lastAddArr = this.state.data.slice(-perLoadDataCount);

    try {
      if (
        lastAddArr[halfOfLoadDataCount] &&
        viewableItems.length > 0 &&
        this.props.loginStatus == 1
      ) {
        // FlatGrid
        if (Array.isArray(viewableItems[0].item)) {
          if (
            viewableItems[0].item.find(
              item => item.node.id === lastAddArr[halfOfLoadDataCount].node.id
            )
          ) {
            this._handleLoadMore(0);
          }
        }
        //FlatList
        else {
          if (
            viewableItems.find(
              item =>
                item.item.node.id === lastAddArr[halfOfLoadDataCount].node.id
            )
          ) {
            this._handleLoadMore(0);
          }
        }
      }
    } catch (e) {
      console.log(e, "lastAddArr", lastAddArr[halfOfLoadDataCount]);
    }
  };

  _renderItem = item => {
    this.getfeedID("New" + item.item.node.id);
    if (this.props.ViewMode != "Compact") {
      return this.props.isAdmin == true ? (
        // <Suspense fallback={<ShadowSkeleton />}>

        this.state.loadSkeleton == true ? (
          <ShadowSkeleton />
        ) : (
          <FeedList
            loginModalStatusEventParent={this.handleModal}
            item={item}
            navigation={this.props.navigation}
            ViewMode={this.props.ViewMode}
            highlight={this.makeHighlight}
            ActiveTab="New"
            deleteItem={this.deleteItemById}
          />
        )
      ) : // </Suspense>
      // <Suspense fallback={<ShadowSkeleton />}>
      this.state.loadSkeleton == true ? (
        <ShadowSkeleton />
      ) : (
        <FeedList
          loginModalStatusEventParent={this.handleModal}
          item={item}
          navigation={this.props.navigation}
          ViewMode={this.props.ViewMode}
          highlight={this.makeHighlight}
          ActiveTab="New"
          deleteItem={this.deleteItemById}
        />
      );

      // </Suspense>
    } else {
      return this.props.isAdmin == true  ? (
        // <Suspense fallback={<ShadowSkeleton />}>
        <AdminCompactView
          loginModalStatusEventParent={this.handleModal}
          item={item}
          navigation={this.props.navigation}
          ViewMode={this.props.ViewMode}
          highlight={this.makeHighlight}
        />
      ) : (
        // </Suspense>
        // <Suspense fallback={<ShadowSkeleton />}>
        <CompactFeedList
          loginModalStatusEventParent={this.handleModal}
          item={item}
          navigation={this.props.navigation}
          ViewMode={this.props.ViewMode}
          highlight={this.makeHighlight}
        />
        // </Suspense>
      );
    }
  };

  deleteItemById = id => {
    const filteredData = this.state.data.filter(item => item.node.id !== id);
    this.setState({ data: filteredData });
  };
  handleModal = status => {
    this.setState({
      modalVisible: status
    });
  };

  makeHighlight = async (id, title) => {
    let newId = id.search("New") != -1 ? id : "New" + id;
    this.setState({
      activeFeed: this.getUnique(feedID).indexOf(newId),
      activeId: id,
      activeTitle: title
    });
    // this.setBorderColor();
  };

  onClose = () => {
    this.setState({
      modalVisible: false
    });
  };

  getfeedID = async id => {
    let data = feedID;
    await data.push(id);
    feedID = data;
  };

  getUnique = array => {
    var uniqueArray = [];
    for (let i = 0; i < array.length; i++) {
      if (uniqueArray.indexOf(array[i]) === -1) {
        uniqueArray.push(array[i]);
      }
    }
    return uniqueArray;
  };

  getFirebaseToken = async () => {
    if (firebase.auth().currentUser) {
      await firebase
        .auth()
        .currentUser.getIdToken(true)
        .then(async idToken => {
          await AsyncStorage.setItem("userIdTokenFirebase", idToken);
          this._fetchAllHomeFeeds();
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      await firebase.auth().onAuthStateChanged(async user => {
        if (user) {
          await user
            .getIdToken(true)
            .then(async idToken => {
              await AsyncStorage.setItem("userIdTokenFirebase", idToken);
              this._fetchAllHomeFeeds();
            })
            .catch(error => {
              console.log(error);
            });
        }
      });
    }
  };

  render() {
    return (
      <View style={{ flex: 1, marginTop: 10 }} nativeID={"NewHomeFeed"}>
        {this.state.data.length == 0 && this.state.showEmptyIcon == false && (
          <ShadowSkeleton />
        )}
        {// this.state.data.length == 0 &&
        this.state.showEmptyIcon == true && (
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: 20
            }}
          >
            <Icon
              color={"#000"}
              iconStyle={{
                color: "#fff",
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center"
              }}
              reverse
              name="file"
              type="font-awesome"
              size={20}
              containerStyle={{
                alignSelf: "center"
              }}
            />
            <Text
              style={{
                fontSize: 14,
                fontWeight: "bold",
                fontFamily: ConstantFontFamily.defaultFont,
                color: "#000",
                alignSelf: "center"
              }}
            >
              No Posts
            </Text>
          </View>
        )}
        {this.state.HomeFeedError == true && (
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginVertical: 20
            }}
          >
            <Button
              onPress={() => this.getFirebaseToken()}
              title={"Try Again"}
              titleStyle={ButtonStyle.titleStyle}
              buttonStyle={ButtonStyle.backgroundStyle}
              containerStyle={ButtonStyle.containerStyle}
            />
          </View>
        )}
        {(this.props.ViewMode == "Default" ||
          this.props.ViewMode == "Text") && (
          <FlatList
            ref={this.flatListRef}
            contentContainerStyle={
              Platform.OS == "web" && Dimensions.get("window").width >= 750
                ? {
                    flexDirection: "column",
                    height: Dimensions.get("window").height,
                    width: "100%",
                    alignSelf: "center"
                  }
                : {}
            }
            data={this.state.data}
            onScroll={event => {
              this.props.listType != "Home"
                ? this.props.listScroll(event.nativeEvent.contentOffset.y)
                : null;
            }}
            scrollEventThrottle={16}
            // keyExtractor={(item, index) => index.toString()}
            keyExtractor={(item, index) => item.node.id}
            renderItem={this._renderItem}
            showsVerticalScrollIndicator={false}
            onEndReachedThreshold={0.2}
            initialNumToRender={10}
            removeClippedSubviews={true}
            ListFooterComponent={
              this.props.loginStatus == 1
                ? this._renderFooter
                : this._renderLogoutFooter
            }
            onViewableItemsChanged={this.onViewableItemsChanged}
            viewabilityConfig={this.viewabilityConfig}
            // contentContainerStyle = {{ flex: 1 }}
          />
        )}
        {this.props.ViewMode == "Card" && (
          <FlatGrid
            ref={this.flatListRef}
            itemDimension={250}
            items={this.state.data}
            renderItem={this._renderItem}
            showsVerticalScrollIndicator={false}
            scrollEnabled={true}
            onScroll={event => {
              this.props.listType != "Home"
                ? this.props.listScroll(event.nativeEvent.contentOffset.y)
                : null;
            }}
            scrollEventThrottle={16}
            spacing={10}
            style={{
              flexDirection: "column",
              height: Dimensions.get("window").height,
              paddingTop: 0
            }}
            itemContainerStyle={{
              justifyContent: "flex-start"
            }}
            onEndReachedThreshold={0.2}
            initialNumToRender={10}
            removeClippedSubviews={true}
            ListFooterComponent={
              this.props.loginStatus == 1
                ? this._renderFooter
                : this._renderLogoutFooter
            }
          />
        )}
        {this.props.ViewMode == "Compact" && (
          <FlatList
            ref={this.flatListRef}
            contentContainerStyle={{
              flexDirection: "column",
              height: Dimensions.get("window").height,
              width: "100%"
            }}
            onScroll={event => {
              this.props.listType != "Home"
                ? this.props.listScroll(event.nativeEvent.contentOffset.y)
                : null;
            }}
            scrollEventThrottle={16}
            data={this.state.data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this._renderItem}
            showsVerticalScrollIndicator={false}
            onEndReachedThreshold={0.2}
            initialNumToRender={10}
            removeClippedSubviews={true}
            ListFooterComponent={
              this.props.loginStatus == 1
                ? this._renderFooter
                : this._renderLogoutFooter
            }
          />
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  listHomeFeed: state.HomeFeedReducer.get("HomefeedList"),
  homefeedListPagination: state.HomeFeedReducer.get("HomefeedListPagination"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  getCreateAccount: state.CreateAccountReducer.get("setCreateAccountData"),
  getUserApproach: state.UserApproachReducer.get("setUserApproach"),
  loginStatus: state.UserReducer.get("loginStatus"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  NewHomeFeed: state.HomeFeedReducer.get("NewHomeFeedList"),
  trendingHomeFeedId: state.PostCommentDetailsReducer.get("PostId")
});

const mapDispatchToProps = dispatch => ({
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingUsers: payload => dispatch(getTrendingUsers(payload)),
  getTrendingTopics: payload => dispatch(getTrendingTopics(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setUsernameModalStatus: payload => dispatch(setUSERNAMEMODALACTION(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  changeLoginStatus: payload => dispatch(setLoginStatus(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  setCreateAccount: payload => dispatch(setCreateAccount(payload)),
  setUserApproachAction: payload => dispatch(setUserApproachAction(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setNewHomeFeed: payload => dispatch({ type: "SET_NEW_HOME_FEED", payload }),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload)),
  setPostCommentReset: payload =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload })
});

const NewHomeFeedWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(NewHomeFeed);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  NewHomeFeedWrapper
);
