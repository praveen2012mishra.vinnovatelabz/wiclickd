import React from "react";
import { View, StatusBar, StyleSheet, Dimensions, Text } from "react-native";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph
} from "react-native-chart-kit";
import RNPickerSelect from "react-native-picker-select";
import ConstantFontFamily from "../constants/FontFamily";
import ButtonStyle from "../constants/ButtonStyle";

const data = {
  labels: ["January", "February", "March", "April", "May", "June"],
  datasets: [
    {
      data: [40, 45, 28, 80, 99, 43]
    }
  ]
};
const chartConfig = {
  fillShadowGradient: "#009B1A",
  fillShadowGradientOpacity: 1,
  backgroundGradientFrom: "#fff",
  backgroundGradientTo: "#fff",
  color: (opacity = 1) => `rgba(${0}, ${0}, ${0}, ${opacity})`
};
export default class AnalyticsGraph extends React.Component {
  constructor(props) {
    super(props);
    this.inputRefs = {};
    this.Pagescrollview = null;
    this.state = {
      showsVerticalScrollIndicatorView: false,
      currentScreentWidth: 0,
      durationDropdownItems: [
        {
          label: "Earnings per Month",
          value: "Earnings per Month"
        }
      ]
    };
  }
  render() {
    return (
      <View
        style={[
          ButtonStyle.shadowStyle,
          {
            marginTop: 10,
            marginBottom:Dimensions.get('window').width > 756 ? 10 : 0,
            paddingVertical: 5,
            backgroundColor: "#fff",
             borderRadius:5,
             marginHorizontal:Dimensions.get('window').width > 756 && 10,
            paddingHorizontal: 5,
            width:Dimensions.get('window').width > 756 ? '95%': '100%',
          }
        ]}
      >
        <View
          style={{
            marginVertical: 10,
            width: 300,
            alignSelf: "center"
          }}
        >
          <RNPickerSelect
            placeholder={{}}
            items={this.state.durationDropdownItems}
            value="Earnings per Month"
            style={{ ...styles }}
            onValueChange={(itemValue, itemIndex) => {
            }}
          />
        </View>

        <BarChart
          data={data}
          //width={800}
          width={Dimensions.get('window').width > 756 ? 800:Dimensions.get('window').width-35}
          height={400}
          yAxisLabel="$"
          chartConfig={chartConfig}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputIOS: {
    paddingTop: 13,
    paddingHorizontal: 5,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
    fontSize: 16,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  },
  inputAndroid: {
    paddingHorizontal: 3,
    paddingVertical: 5,
    borderWidth: 0.5,
    borderColor: "grey",
    borderRadius: 8,
    color: "#000",
    backgroundColor: "white",
    paddingRight: 10,
    fontSize: 16,
    fontWeight: "bold",
    fontFamily: ConstantFontFamily.MontserratBoldFont
  }
});
