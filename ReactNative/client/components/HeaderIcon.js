import React, { Component } from "react";
import {
  Image,
  View,
  TouchableOpacity,
  Platform,
  Dimensions
} from "react-native";
import NavigationService from "../library/NavigationService";
import { connect } from "react-redux";
import { compose } from "recompose";

class HeaderIcon extends Component {
  render() {
    return (
      <View
        style={{
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <TouchableOpacity onPress={() => NavigationService.navigate("home")}>
          <Image
            source={
              this.props.getCurrentDeviceWidthAction > 750
                ? require("../assets/image/weclickd-logo-beta-white.png")
                : require("../assets/image/weclickd-logo-only-icon.png")
            }
            style={
              this.props.getCurrentDeviceWidthAction > 750
                ? { height: 30, width: 120 }
                : {
                    height: 30,
                    width:
                      this.props.getCurrentDeviceWidthAction > 750 ? 90 : 30
                  }
            }
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension")
});

export default compose(connect(mapStateToProps, null))(HeaderIcon);
