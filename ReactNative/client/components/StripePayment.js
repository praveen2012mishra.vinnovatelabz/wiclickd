import { ElementsConsumer } from "@stripe/react-stripe-js";
import Modal from "modal-enhanced-react-native-web";
import React from "react";
import { graphql } from "react-apollo";
import {
  Dimensions,
  Linking,
  Platform,
  Text,
  TouchableOpacity,
  AsyncStorage,
  View
} from "react-native";
import { Button } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import applloClient from "../client";
import ChangePaymentModal from "../components/ChangePaymentModal";
import ConstantFontFamily from "../constants/FontFamily";
import getEnvVars from "../environment";
import {
  GetPaymentInfoOuery,
  IsAccountHaveOuery
} from "../graphqlSchema/graphqlMutation/StripeMutation";
import {
  ChangeAccountSettingsMutation,
  UserLoginMutation
} from "../graphqlSchema/graphqlMutation/UserMutation";
import { ChangeAccountSettingsVariables } from "../graphqlSchema/graphqlVariables/UserVariables";
import {
  getFirebaseToken,
  getMyUserId,
  getWeclikdToken
} from "../library/Helper";
import ButtonStyle from "../constants/ButtonStyle";
import { Icon } from "react-native-elements";
import NavigationService from "../library/NavigationService";

const apiUrlMain = getEnvVars();

class StripePayment extends React.Component {
  state = {
    switchValue1:
      this.props.profileData &&
      this.props.profileData.getIn([
        "settings",
        "email_notifications",
        "monthly_earnings"
      ]),
    switchValue2:
      this.props.profileData &&
      this.props.profileData.getIn([
        "settings",
        "email_notifications",
        "weclikd_updates"
      ]),
    switchValue3:
      this.props.profileData &&
      this.props.profileData.getIn([
        "settings",
        "email_notifications",
        "clik_notifications"
      ]),
    Email:
      this.props.profileData &&
      this.props.profileData.getIn([
        "settings",
        "email_notifications",
        "notification_email"
      ]),
    showTextInput: false,
    cardInfoShow: false,
    infoCard: null,
    isAccountStripe: false,
    loading: false,
    showSubcriptionModal: false,
    conformModal: ""
  };

  componentDidMount() {
    let __self = this;
    applloClient
      .query({
        query: IsAccountHaveOuery,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        if (res) {
          __self.setState({
            isAccountStripe: res.data.account.payout_info.has_stripe_account
          });
        }
      });
  }
  getinfo = () => {
    let __self = this;
    applloClient
      .query({
        query: IsAccountHaveOuery,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        if (res) {
          __self.setState({
            isAccountStripe: res.data.account.payout_info.has_stripe_account
          });
        }
      });
  };

  ChangeSubscription = () => {
    ChangeAccountSettingsVariables.variables.privacy.dm_settings = "EVERYONE";
    ChangeAccountSettingsVariables.variables.email_notification.notification_email = this.state.Email;
    ChangeAccountSettingsVariables.variables.email_notification.monthly_earnings = this.state.switchValue1;
    ChangeAccountSettingsVariables.variables.email_notification.clik_notifications = this.state.switchValue3;
    ChangeAccountSettingsVariables.variables.email_notification.weclikd_updates = this.state.switchValue2;
    applloClient
      .query({
        query: ChangeAccountSettingsMutation,
        ...ChangeAccountSettingsVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  getCardInfo = () => {
    let __self = this;
    this.setState({
      loading: true
    });
    applloClient
      .query({
        query: GetPaymentInfoOuery,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        __self.setState({
          infoCard: {
            last4: res.data.account.payment_info.last4,
            brand: res.data.account.payment_info.brand,
            exp_month:
              res.data.account.payment_info.exp_month.toString().length == 1
                ? "0" + res.data.account.payment_info.exp_month
                : res.data.account.payment_info.exp_month,
            exp_year: res.data.account.payment_info.exp_year,
            exp_year: res.data.account.payment_info.exp_year
              .toString()
              .substring(
                res.data.account.payment_info.exp_year.toString().length - 2
              )
          },
          cardInfoShow: !__self.state.cardInfoShow,
          loading: false
        });
      })
      .catch(e => {
        console.log(e);
        __self.setState({
          loading: false
        });
      });
  };

  onClose = () => {
    this.getCardInfo();
    this.setState({
      showSubcriptionModal: false
    });
  };

  auth = async () => {
    const firebaseToken = await getFirebaseToken();
    const tempToken = await getWeclikdToken();
    const tempCurrentUserId = await getMyUserId();
    let weclikdToken = tempToken ? tempToken : null;
    let CurrentUserId = tempCurrentUserId ? tempCurrentUserId : null;
    return await {
      firebaseToken,
      weclikdToken,
      CurrentUserId
    };
  };

  openWindow = async () => {
    let apiUrl = apiUrlMain.API_URL + "plutus/payout/authorize";
    let apiUrlHeader = await this.auth();
    let headersSet = {
      "Weclikd-Authorization": encodeURI("Bearer " + apiUrlHeader.weclikdToken),
      Authorization: encodeURI("Custom " + apiUrlHeader.firebaseToken)
    };
    let str = "";
    for (let key in headersSet) {
      if (str != "") {
        str += "&";
      }
      str += key + "=" + headersSet[key];
    }
    const uri = apiUrl + "?" + str;
    Linking.openURL(uri);
  };

  openWindowDashboard = async () => {
    let apiUrl = apiUrlMain.API_URL + "plutus/payout/dashboard";
    let apiUrlHeader = await this.auth();
    let headersSet = {
      "Weclikd-Authorization": encodeURI("Bearer " + apiUrlHeader.weclikdToken),
      Authorization: encodeURI("Custom " + apiUrlHeader.firebaseToken)
    };
    let str = "";
    for (let key in headersSet) {
      if (str != "") {
        str += "&";
      }
      str += key + "=" + headersSet[key];
    }
    const uri = apiUrl + "?" + str;
    Linking.openURL(uri);
  };

  render() {
    const {
      cardInfoShow,
      conformModal,
      isAccountStripe,
      loading,
      showSubcriptionModal
    } = this.state;
    return (
      <>
        <View
          style={[
            ButtonStyle.shadowStyle,
            {
              borderRadius: 5,
              marginTop: 10,
              paddingVertical: 10,
              backgroundColor: "#fff",
              marginHorizontal: Dimensions.get("window").width >= 750 && 10,
              width: Dimensions.get("window").width <= 750 && '100%'
            }
          ]}
        >
          <View style={{ margin: 5, paddingHorizontal: 20 }}>
            <Text
              style={{
                fontSize: 16,
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontWeight: "bold",
                textAlign: "center",
                paddingHorizontal: 20
              }}
            >
              Stripe Payouts
            </Text>
          </View>
          {this.state.isAccountStripe == false ? (
            <View style={{ padding: 10, flexDirection: Dimensions.get("window").width <= 750 ? "column" : 'row', width: "100%", justifyContent: 'space-between' }}>
              <View style={{
                width: Dimensions.get("window").width <= 750 && "100%",
                justifyContent: 'center'
              }}>
                <Text
                  style={{
                    //fontWeight: "bold",
                    fontSize: 14,
                    fontFamily: ConstantFontFamily.defaultFont
                  }}
                >
                  Set up a Stripe account to receive money from Weclikd.
                </Text>
                <Text
                  style={{
                    //fontWeight: "bold",
                    fontSize: 14,
                    fontFamily: ConstantFontFamily.defaultFont
                  }}
                >
                  Learn more about getting paid.
                </Text>
              </View>
              <View
                style={{
                  justifyContent: "center",
                  width: Dimensions.get("window").width <= 750 && "100%",
                  alignItems: "flex-end"
                }}
              >
                <Button
                  onPress={this.openWindow}
                  color="#fff"
                  title="Setup Stripe"
                  titleStyle={ButtonStyle.titleStyle}
                  buttonStyle={ButtonStyle.backgroundStyle}
                  containerStyle={[ButtonStyle.containerStyle, { 
                    // marginBottom: Dimensions.get("window").width <= 750 && 0 
                  }]}
                />
              </View>
            </View>
          ) : (
              <View style={{ padding: 10, justifyContent: 'space-between', flexDirection: "row", width: "100%" }}>
                <Text
                  style={{
                    width: Dimensions.get("window").width <= 750 && "70%",
                    fontSize: 14,
                    fontFamily: ConstantFontFamily.defaultFont,
                    //fontWeight: "bold",
                    justifyContent: 'center'
                  }}
                >
                  Go to your Stripe dashboard to view payouts and change Stripe
                  settings. Weclikd pays into your stripe account every month if
                  your balance is over five dollars.
              </Text>

                <View
                  style={{
                    justifyContent: "center",
                    width: Dimensions.get("window").width <= 750 && "30%",
                    alignItems: "flex-end"
                  }}
                >
                  <Button
                    onPress={this.openWindowDashboard}
                    color="#fff"
                    title="Go to Stripe"
                    titleStyle={ButtonStyle.titleStyle}
                    buttonStyle={ButtonStyle.backgroundStyle}
                    containerStyle={[ButtonStyle.containerStyle, {
                      //  marginBottom: Dimensions.get("window").width <= 750 && 0 
                      }]}
                  />
                </View>
              </View>
            )}
        </View>

        <View
          style={[
            //ButtonStyle.borderStyle,
            ButtonStyle.shadowStyle,
            {
              //marginLeft: 5,
              borderRadius: 5,
              marginTop: 10,
              paddingVertical: 10,
              backgroundColor: "#fff",
              marginHorizontal: Dimensions.get("window").width >= 750 && 10,
              width: Dimensions.get("window").width <= 750 && '100%'
            }
          ]}
        >
          <View style={{ margin: 5, paddingHorizontal: 20 }}>
            <Text
              style={{
                fontSize: 16,
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                fontWeight: "bold",
                textAlign: "center",
                paddingHorizontal: 20
              }}
            >
              Weclikd Subscription
            </Text>
          </View>

          {this.props.profileData &&
            this.props.profileData.getIn(["settings", "subscription"]) ==
            "BASIC" && (
              <View style={{ padding: 10, justifyContent: 'space-between', flexDirection: Dimensions.get("window").width <= 750 ? "column" : 'row', width: "100%" }}>
                <Text
                  style={{
                    width: Dimensions.get("window").width <= 750 && "100%",
                    fontSize: 14,
                    fontFamily: ConstantFontFamily.defaultFont,
                    //fontWeight: "bold",
                    justifyContent: 'center'
                  }}
                >
                  To increase the amount Weclikd pays you at the beginning of
                  each month, consider getting a paid subscription. If other
                  subscribers are supporting your commentary, then we
                  incentivize you to do the same.
                </Text>

                <View
                  style={{
                    justifyContent: "center",
                    width: Dimensions.get("window").width <= 750 && "100%",
                    alignItems: "flex-end"
                  }}
                >
                  <Button
                    onPress={async () => { 
                      await AsyncStorage.setItem('getSettingActiveSection', JSON.stringify('Upgrade'))
                      NavigationService.navigate("settings") 
                    }}
                    color="#fff"
                    title="Upgrade"
                    titleStyle={ButtonStyle.titleStyle}
                    buttonStyle={ButtonStyle.backgroundStyle}
                    containerStyle={[ButtonStyle.containerStyle, { 
                      // marginBottom: Dimensions.get("window").width <= 750 && 0 
                    }]}
                  />
                </View>
              </View>
            )}
        </View>
      </>
    );
  }
}

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails")
});

const mapDispatchToProps = dispatch => ({
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload))
});

const StripePaymentContainerWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(StripePayment);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  StripePaymentContainerWrapper
);
