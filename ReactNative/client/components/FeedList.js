import "@expo/browser-polyfill";
import { openBrowserAsync } from "expo-web-browser";
import moment from "moment";
import React, { useEffect, useRef, useState } from "react";
import {
  TextInput,
  Image,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  Dimensions,
  Platform,
  Clipboard,
  TouchableWithoutFeedback,
  AsyncStorage
} from "react-native";
import { Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { getFeedProfileDetails } from "../actionCreator/FeedProfileAction";
import { setFEEDREPORTMODALACTION } from "../actionCreator/FeedReportModalAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { listTopicFeed } from "../actionCreator/TopicsFeedAction";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { getTrendingTopicsProfileDetails } from "../actionCreator/TrendingTopicsProfileAction";
import ChangeIcon from "../components/ChangeIcon";
import ConstantFontFamily from "../constants/FontFamily";
import { rootTopics } from "../constants/RootTopics";
import { capitalizeFirstLetter } from "../library/Helper";
import DiscussByUsersList from "./DiscussByUsersList";
import ImageComponent from "./ImageComponent";
import Overlay from "react-native-modal-overlay";
import FeedShareModal from "../components/FeedShareModal";
import Modal from "modal-enhanced-react-native-web";
import FeedReportModal from "../components/FeedReportModal";
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger
} from "react-native-popup-menu";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import { DeleteContentMutation } from "../graphqlSchema/graphqlMutation/FeedMutation";
import applloClient from "../client";
import { postEditDetails } from "../actionCreator/LinkPostAction";
import NavigationService from "../library/NavigationService";
import FeedImageDisplay from "./FeedImageDisplayClick";
import FeedImageDisplayUser from "./FeedImageDisplayUser";
import ButtonStyle from "../constants/ButtonStyle";


let activeID = "";
let lastTap = null;
const FeedList = props => {
  const swiperRef = useRef(null);
  let bgImage = {
    uri: props.item.item.node.thumbnail_pic
  };
  let [loaded, setloaded] = useState(false);
  let [summaryHeight, setsummaryHeight] = useState(10);
  let [titleHeight, settitleHeight] = useState(10);

  moment.updateLocale("en", {
    relativeTime: {
      future: "in %s",
      past: "%s ago",
      s: "a few secs",
      ss: "%d secs",
      m: "1 minute",
      mm: "%d minutes",
      h: "1 hour",
      hh: "%d hours",
      d: "1 day",
      dd: "%d days",
      w: "1 week",
      ww: "%d weeks",
      M: "1 month",
      MM: "%d months",
      y: "1 year",
      yy: "%d years"
    }
  });

  useEffect(() => {
    if (props.isFocused == false && swiperRef != null) {
      swiperRef.current.goTo(0);
    }
    let data = document.getElementById(props.item.item.node.id);
    if (data != null) {
      data.addEventListener("keydown", event => {
        if (event.keyCode == 82) {
          props.loginStatus == 1
            ? handleBugClick()
            : props.setLoginModalStatus(true);
        }
        if (event.keyCode == 67) {
          props.setPostDetails({
            title: "Home",
            id: "Post:" + activeID.replace("Post:", ""),
            comment: true
          });
          props.setPostCommentDetails({
            id: activeID.replace("Post:", "")
          });
        }
        if (event.keyCode == 13) {
          event.stopImmediatePropagation();
          goToPostDetailsScreen(activeID.replace("Post:", ""));
        }
      });

      data.addEventListener("click", event => {
        if (data.style.borderColor == "red") {
          activeID = data.id;
          data.scrollIntoView();
        }
      });
    }
    //l=76, r=82, c=67,Enter =13
  });
  let [showBug, setshowBug] = useState(false);
  let [pressBug, setpressBug] = useState(false);
  let [pressComment, setpressComment] = useState(false);
  let [pressShare, setpressShare] = useState(false);
  let [TopicList, setTopicList] = useState(
    props.item.item.node.topics && props.item.item.node.topics
  );
  let [ClikList, setClikList] = useState(
    props.item.item.node.cliks && props.item.item.node.cliks
  );
  let [showMenu, setshowMenu] = useState(Platform.OS == "web" ? false : true);
  let [MenuHover, setMenuHover] = useState(false);
  let [showSummary, setShowSummary] = useState(false);
  let [borderWidth, setborderWidth] = useState(false);
  let [borderHighlightId, setBorderHighlightId] = useState(props.item.item.node.id.replace("Post:", ""));

  const openWindow = async link => {
    await openBrowserAsync(link);
  };
  const loginModalStatusEvent = async status => {
    props.loginModalStatusEventParent(status);
  };
  const goToPostDetailsScreen = async (id, navigate) => {
    props.setPostCommentReset({
      payload: [],
      postId: props.item.item.node.id
        .replace("Trending", "")
        .replace("New", "")
        .replace("Discussion", "")
        .replace("Search", ""),
      title: props.item.item.node.title,
      loading: props.item.item.node.comments_score > 0 ? true : false
    });
    // console.log(props.ActiveTab + props.item.item.node.id,
    //   props.item.item.node.title);
    props.highlight(
      props.ActiveTab + props.item.item.node.id,
      props.item.item.node.title
    );
    let title = "Home";
    props.setPostCommentDetails({
      id: id,
      title: props.item.item.node.title,
      loading: props.item.item.node.comments_score > 0 ? true : false
    });
    if (navigate == true) {
      props.setPostDetails({
        title: title,
        id: "Post:" + id,
        navigate: navigate
      });
    }
    props.setComment(false);
  };

  const goToClikProfile = id => {
    if (id != null) {
      props.cliksUserId({
        id: id,
        type: "feed"
      });
    }
  };

  async function goToProfile(id) {
    props.topicId({
      id: id,
      type: "feed"
    });
  }

  const handleClick = e => {
    setshowBug(e);
  };

  const handleBugClick = e => {
    props.setFeedReportModalAction(true);
    setpressBug(true);
  };

  const getHeart = liketype => {
    switch (liketype) {
      case "RED":
        return [1, 0, 0, 0];
      case "SILVER":
        return [0, 1, 0, 0];
      case "GOLD":
        return [0, 0, 1, 0];
      case "DIAMOND":
        return [0, 0, 0, 1];
      default:
        return [0, 0, 0, 0];
    }
  };

  const getColour = score => {
    return "#969faa";
    // if (score == 0) {
    //   return "#969faa";
    // } else if (0 < score && score <= 25) {
    //   return "#de5246";
    // } else if (25 < score && score <= 50) {
    //   return "#b0b0b0";
    // } else if (50 < score && score <= 75) {
    //   return "#ebca44";
    // } else {
    //   return "#8bbaf9";
    // }
  };

  const goToFeedProfile = async id => {
    await props.setFeedDetails({
      id: id,
      type: "feed"
    });
  };

  const handleShareClick = e => {
    props.setFeedShareModal(true);
    setpressShare(true);
  };
  const onClose = () => {
    props.setFeedShareModal(false);
    setpressShare(false);
    props.setFeedReportModalAction(false);
    setpressBug(false);
  };
  const getTopic = List => {
    if (List.length > 0) {
      let prevList = TopicList;
      let newList = [...prevList, ...List];
      setTopicList(newList);
    }
  };
  const getClik = List => {
    if (List.length > 0) {
      let prevList = ClikList;
      let newList = [...prevList, ...List];
      setClikList(newList);
    }
  };
  const writeToClipboard = async () => {
    let Domain = window.location.href
      .replace("http://", "")
      .replace("https://", "")
      .replace("www.", "")
      .split(/[/?#]/)[0];
    let PostId = "/post/" + props.item.item.node.id.replace("Post:", "");
    await Clipboard.setString(
      Domain.startsWith("localhost") == true
        ? "http://" + Domain + PostId
        : "https://" + Domain + PostId
    );
  };

  useEffect(() => {
    if (props.item.item.node.id.replace("Post:", "") != borderHighlightId) {
      setBorderHighlightId(props.item.item.node.id.replace("Post:", ""));
      console.log('===============================>');
      AsyncStorage.setItem(
        'PostId',
        JSON.stringify(props.item.item.node.id)
      );
    }
  }, [props.item.item.node.id.replace("Post:", "")]);

  const handleDoubleTap = async () => {
    if (Dimensions.get("window").width >= 750) {
      goToPostDetailsScreen(
        props.item.item.node.id.replace("Post:", ""),
        false
      );
      setShowSummary(true);
      let asyncClickTitle;
      await AsyncStorage.getItem('ClickTitle').then(houses => { asyncClickTitle = JSON.parse(houses); });
      // NavigationService.navigate("info", {
      //   id: props.item.item.node.id.replace("Post:", "")
      // });
      //console.log(asyncClickTitle, props.item.item.node, '==============================');
      let currentId;
      await AsyncStorage.getItem('PostId').then(houses => { currentId = JSON.parse(houses); });
      let postId = props.item.item.node.id.replace("Post:", "");
      AsyncStorage.setItem(
        'PostId',
        JSON.stringify(postId)
      );
      if (props.item.item.node.id.replace("Post:", "") && window.location.href.search('clik') == -1) {
        let id = props.item.item.node.id.replace("Post:", "");
        NavigationService.navigate("feedId", { id: id });
        // AsyncStorage.setItem(
        //   'PostId',
        //   JSON.stringify(props.item.item.node.id)
        // );
        //props.setFeedId(id);
      }
      console.log(window.location.href, window.location.href.search('clik'), window.location.href.search(currentId), currentId, props.item.item.node.id.replace("Post:", ""));
      if (window.location.href.search('clik') != -1) {
        // AsyncStorage.setItem(
        //   'PostId',
        //   JSON.stringify(props.item.item.node.id)
        // );
        let id = props.item.item.node.id.replace("Post:", "");
        NavigationService.navigate("cliksprofile", {
          id: asyncClickTitle.clickTitle,
          type: asyncClickTitle.type,
          postId: id
        });
      }
      let tempData =
        props.trendingHomeFeedId && props.trendingHomeFeedId.includes("Post:")
          ? props.trendingHomeFeedId.replace("Post:", "")
          : props.trendingHomeFeedId;
      let data1 = props.ActiveTab + props.item.item.node.id;
      let data2 = props.ActiveTab + "Post:" + tempData;
      //console.log(data1,'==============================>',data2);
      let data1content = document.getElementById(data1);
      let data2content = document.getElementById(data2);
      if (data2content != null) {
        data2content.style.borderColor = "#c5c5c5";
        //console.log(data1content,'==============================>',data2content);
      }
      if (data1content != null) {
        // data1content.style.borderColor = "green";
        // data1content.style.borderWidth = "3px";
        //console.log(data1content,'==============================>',data2content);
      }
      // if (data1 == data2) {
      //   goToPostDetailsScreen(
      //     props.item.item.node.id.replace("Post:", ""),
      //     true
      //   );
      // } else {
      //   goToPostDetailsScreen(
      //     props.item.item.node.id.replace("Post:", ""),
      //     false
      //   );
      // }
      // goToPostDetailsScreen(
      //   props.item.item.node.id.replace("Post:", ""),
      //   false
      // );
    } else {
      goToPostDetailsScreen(props.item.item.node.id.replace("Post:", ""), true);
    }
  };

  const handleClickComment = () => {
    if (Dimensions.get("window").width >= 1100) {
      let tempData =
        props.trendingHomeFeedId && props.trendingHomeFeedId.includes("Post:")
          ? props.trendingHomeFeedId.replace("Post:", "")
          : props.trendingHomeFeedId;
      let data1 = props.ActiveTab + props.item.item.node.id;
      let data2 = props.ActiveTab + "Post:" + tempData;
      let data1content = document.getElementById(data1);
      let data2content = document.getElementById(data2);
      if (data1content != null) {
        data1content.style.borderColor = "green";
      }
      if (data2content != null) {
        data2content.style.borderColor = "#c5c5c5";
      }
      if (data1 == data2) {
        goToPostDetailsScreen(
          props.item.item.node.id.replace("Post:", ""),
          false
        );
      } else {
        goToPostDetailsScreen(
          props.item.item.node.id.replace("Post:", ""),
          false
        );
      }
    } else {
      goToPostDetailsScreen(props.item.item.node.id.replace("Post:", ""), true);
    }
  };

  const goToUserProfile = async username => {
    await props.userId({
      username: username,
      type: "feed"
    });
    await props.navigation.navigate("profile", {
      username: username,
      type: "feed"
    });
  };

  const handleDeleteFeed = id => {
    applloClient
      .query({
        query: DeleteContentMutation,
        variables: {
          content_id: id
        },
        fetchPolicy: "no-cache"
      })
      .then(response => {
        if (response.data.content_delete.status.success == true) {
          props.deleteItem(id);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  const imageData = {
    reasons_shared: {
      users: [
        {
          id: "User:5379354969857704960",
          username: "chayanme007",
          profile_pic: "https://cdn-dev.weclikd-beta.com/3005793886624940032"
        },
        {
          id: "User:5379354969857704960",
          username: "chayanme007",
          profile_pic: "https://cdn-dev.weclikd-beta.com/3005793886624940032"
        },
        {
          id: "User:5379354969857704960",
          username: "chayanme007",
          profile_pic: "https://cdn-dev.weclikd-beta.com/3005793886624940032"
        }
      ],
      cliks: [
        {
          id: "User:5379354969857704960",
          username: "chayanme007",
          profile_pic: "https://cdn-dev.weclikd-beta.com/3005793886624940032"
        },
        {
          id: "User:5379354969857704960",
          username: "chayanme007",
          profile_pic: "https://cdn-dev.weclikd-beta.com/3005793886624940032"
        },
        {
          id: "User:5379354969857704960",
          username: "chayanme007",
          profile_pic: "https://cdn-dev.weclikd-beta.com/3005793886624940032"
        }
      ]
    }
  };

  return (
    <TouchableOpacity
      style={{ marginHorizontal: Dimensions.get("window").width <= 750 ? 5 : 0 }}
      // onMouseEnter={() => setshowMenu(true)}
      // onMouseLeave={() =>
      //   MenuHover == true ? setshowMenu(true) : setshowMenu(false)
      // }
    >
      <TouchableOpacity
        onPress={() => {
          handleDoubleTap()
        }}
        nativeID={props.ActiveTab + props.item.item.node.id}
        style={[Dimensions.get("window").width >= 750 &&
          props.PostId && props.PostId.replace("Post:", "") == props.item.item.node.id.replace("Post:", "") ? ButtonStyle.selectShadowStyle : ButtonStyle.shadowStyle,
        {
          //  borderColor:Dimensions.get("window").width <= 750 ? 1 :   style={
          //  props.PostId && props.PostId.replace("Post:", "") == props.item.item.node.id.replace("Post:", "") ? 'green' : "#D7D7D7",
          // borderWidth: Dimensions.get("window").width <= 750 ? 1 :
          //   props.PostId && props.PostId.replace("Post:", "") == props.item.item.node.id.replace("Post:", "") ? 3 : 1,
          // shadowColor: props.PostId && props.PostId.replace("Post:", "") == props.item.item.node.id.replace("Post:", "") ? 'green' : "#000",
          // shadowOpacity: props.PostId && props.PostId.replace("Post:", "") == props.item.item.node.id.replace("Post:", "") ? 0.8 : 0.25,
          borderColor: '#D7D7D7',
          // borderWidth: 1,
          borderRadius: 20,
          backgroundColor: "#fff",
          marginHorizontal: 4
        }]}
      >
        {pressBug == true ? (
          Platform.OS !== "web" ? (
            <Overlay
              animationType="zoomIn"
              visible={pressBug}
              onClose={onClose}
              closeOnTouchOutside
              children={
                <FeedReportModal
                  onClose={onClose}
                  PostId={props.item.item.node.id}
                  TopicList={TopicList}
                  ClikList={ClikList}
                />
              }
              childrenWrapperStyle={{
                padding: 0,
                margin: 0,
                borderRadius: 6
              }}
            />
          ) : (
            <Modal
              isVisible={pressBug}
              onBackdropPress={onClose}
              style={{
                marginHorizontal:
                  Dimensions.get("window").width > 750 ? "30%" : 10,
                padding: 0
              }}
            >
              <FeedReportModal
                onClose={onClose}
                PostId={props.item.item.node.id}
                TopicList={TopicList}
                ClikList={ClikList}
              />
            </Modal>
          )
        ) : null}

        {pressShare == true ? (
          Platform.OS !== "web" ? (
            <Overlay
              animationType="zoomIn"
              visible={pressShare}
              onClose={onClose}
              closeOnTouchOutside
              children={
                <FeedShareModal
                  onClose={onClose}
                  PostId={props.item.item.node.id}
                  getTopic={getTopic}
                  getClik={getClik}
                />
              }
              childrenWrapperStyle={{
                padding: 0,
                margin: 0,
                borderRadius: 6
              }}
            />
          ) : (
            <Modal
              isVisible={pressShare}
              onBackdropPress={onClose}
              style={{
                marginHorizontal:
                  Dimensions.get("window").width > 750 ? "30%" : 10,
                padding: 0
              }}
            >
              <FeedShareModal
                onClose={onClose}
                PostId={props.item.item.node.id}
                getTopic={getTopic}
                getClik={getClik}
              />
            </Modal>
          )
        ) : null}
        <View>
          {/* {props.item.item.node.thumbnail_pic != null &&
            props.ViewMode != "Text" && (
              <TouchableOpacity onPress={() => handleDoubleTap()}>
                <View>
                  <ImageComponent bgImage={bgImage} placeholderColor="#000" />
                </View>
              </TouchableOpacity>
            )} */}

          {/* {props.ViewMode != "Text" && (
            <View
              style={{
                flexDirection: "row",
                justifyContent: "flex-start",
                width: "100%",
                marginTop: 5
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  flexWrap: "wrap",
                  width: "95%"
                }}
              >
                {ClikList &&
                  ClikList.map((t, i) => {
                    return (
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "center",
                          alignItems: "center",
                          marginLeft: 5,
                          marginTop: 5,
                          padding: 3,
                          backgroundColor: "#E8F5FA",
                          borderRadius: 6
                        }}
                        key={i}
                      >
                        <Hoverable>
                          {isHovered => (
                            <TouchableHighlight
                              onPress={() => goToClikProfile(t)}
                            >
                              <Text
                                style={{
                                  color: "#4169e1",
                                  fontFamily:
                                    ConstantFontFamily.MontserratBoldFont,
                                  fontWeight: "bold",
                                  fontSize: 14,
                                  textDecorationLine:
                                    isHovered == true ? "underline" : "none"
                                }}
                              >
                                #{t}
                              </Text>
                            </TouchableHighlight>
                          )}
                        </Hoverable>
                      </View>
                    );
                  })}

                {TopicList &&
                  TopicList.map((t, i) => {
                    return (
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "center",
                          alignItems: "center",
                          marginLeft: 5,
                          marginTop: 5,
                          padding: 3,
                          backgroundColor: "#e3f9d5",

                          borderRadius: 6
                        }}
                        key={i}
                      >
                        <Hoverable>
                          {isHovered => (
                            <TouchableHighlight
                              onPress={() =>
                                goToProfile(
                                  capitalizeFirstLetter(t.toLowerCase())
                                )
                              }
                            >
                              <Text
                                style={{
                                  color: "#009B1A",

                                  fontFamily:
                                    ConstantFontFamily.MontserratBoldFont,
                                  fontWeight: "bold",
                                  fontSize: 14,
                                  textDecorationLine:
                                    isHovered == true ? "underline" : "none"
                                }}
                              >
                                /{t.toLowerCase()}
                              </Text>
                            </TouchableHighlight>
                          )}
                        </Hoverable>
                      </View>
                    );
                  })}

                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    marginLeft: 5,
                    marginTop: 5,
                    padding: 10,
                    backgroundColor: "#f0f0f0",
                    borderRadius: 6,
                    height: 30
                  }}
                >
                  <Hoverable>
                    {isHovered => (
                      <TouchableHighlight
                        onPress={() =>
                          props.loginStatus == 1
                            ? handleShareClick()
                            : props.setLoginModalStatus(true)
                        }
                      >
                        <Text
                          style={{
                            color: "#969faa",
                            fontFamily: ConstantFontFamily.MontserratBoldFont,
                            fontWeight: "bold",
                            fontSize: 25,
                            textDecorationLine:
                              isHovered == true ? "underline" : "none"
                          }}
                        >
                          +
                        </Text>
                      </TouchableHighlight>
                    )}
                  </Hoverable>
                </View>
              </View>
            </View>
          )} */}
          <View style={{ flexDirection: "row", width: "100%" }}>
            <View
              style={{
                width:
                  props.item.item.node.reasons_shared != null &&
                    props.item.item.node.reasons_shared.cliks.length > 0
                    ? "15%"
                    : "0%"
              }}
            >

              {props.item.item.node.reasons_shared && (
                <FeedImageDisplay
                  item={props.item.item.node.reasons_shared.cliks}
                />
              )}
            </View>
            <TouchableOpacity
              style={{
                width:
                  props.item.item.node.reasons_shared != null &&
                    props.item.item.node.reasons_shared.cliks.length > 0
                    ? "85%"
                    : "100%",
                paddingLeft: 5
              }}
              onPress={() => {
                setBorderHighlightId(props.item.item.node.id.replace("Post:", ""));
                handleDoubleTap()
              }}
            >
              <View
                style={{
                  marginTop: 10
                }}
              >
                <TextInput
                  value={props.item.item.node.title}
                  editable={false}
                  style={{
                    textAlign: "center",
                    color: "#000",
                    // paddingHorizontal: 10,
                    fontFamily: ConstantFontFamily.VerdanaFont,
                    fontSize: 16,
                    height: titleHeight,
                    paddingVertical: 20,
                    overflow: 'hidden',
                    fontWeight: 'bold'
                  }}
                  onContentSizeChange={e => {
                    settitleHeight(e.nativeEvent.contentSize.height);
                  }}
                  scrollEnabled={false}
                  multiline={true}
                />
              </View>
            </TouchableOpacity>
          </View>

          <View style={{ flexDirection: "row", width: "100%" }}>
            {Dimensions.get('window').width >= 750 &&
              <View
                style={{
                  width:
                    props.item.item.node.reasons_shared != null &&
                      props.item.item.node.reasons_shared.users.length > 0
                      ? "15%"
                      : "0%"
                }}
              >
                {props.item.item.node.reasons_shared && (
                  <FeedImageDisplayUser
                    height={summaryHeight}
                    item={props.item.item.node.reasons_shared.users}
                  />
                )}
              </View>
            }
            <TouchableOpacity
              style={{
                width:
                  props.item.item.node.reasons_shared &&
                    props.item.item.node.reasons_shared.users.length > 0
                    ? "85%"
                    : "100%",
                paddingLeft: 5
              }}
              onPress={() => { handleDoubleTap(); }}
            >
              {/* {showSummary == true && ( */}
              {
                Dimensions.get('window').width >= 750 &&
                props.PostId && props.PostId.replace("Post:", "") == props.item.item.node.id.replace("Post:", "") && (
                  <TextInput
                    value={
                      props.item.item.node.summary != null
                        ? props.item.item.node.summary.length > 300
                          ? props.item.item.node.summary.substring(0, 297) + "..."
                          : props.item.item.node.summary
                        : null
                    }
                    editable={false}
                    style={{
                      color: "#000",
                      paddingHorizontal: 5,
                      fontFamily: ConstantFontFamily.VerdanaFont,
                      fontSize: 13,
                      lineHeight: 20,
                      height: summaryHeight,
                      paddingBottom: 10,
                      overflow: 'hidden'
                    }}
                    scrollEnabled={false}
                    onContentSizeChange={e => {
                      setsummaryHeight(e.nativeEvent.contentSize.height);
                    }}
                    multiline={true}
                  />
                )}
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: "100%",
              flexDirection: "row",
              paddingRight: 5,
              paddingLeft: 7,
              paddingTop: 5,
              paddingBottom: 7
            }}
          >
            <View
              style={{
                width: "60%",
                flexDirection: "row"
              }}
            >
              {props.item.item.node.link == "" ||
                props.item.item.node.link == null ? (
                <View
                  style={{
                    flexDirection: "row"
                  }}
                >
                  {props.item.item.node.author &&
                    props.item.item.node.author.profile_pic ? (
                    <Image
                      source={{
                        uri: props.item.item.node.author.profile_pic
                      }}
                      style={{
                        width: 30,
                        height: 30,
                        borderRadius: 18,
                        borderWidth: 1,
                        borderColor: "#e1e1e1",
                        marginRight: 5
                      }}
                    />
                  ) : (
                    <Image
                      source={require("../assets/image/default-image.png")}
                      style={{
                        width: 30,
                        height: 30,
                        borderRadius: 18,
                        borderWidth: 1,
                        borderColor: "#e1e1e1",
                        marginRight: 5
                      }}
                    />
                  )}
                  <Hoverable>
                    {isHovered => (
                      <TouchableHighlight
                        onPress={() =>
                          goToUserProfile(
                            props.item.item.node.author &&
                            props.item.item.node.author.username
                          )
                        }
                        style={{
                          alignSelf: "center",
                          width: '95%',
                          flexDirection: 'row',
                        }}
                      >
                        <Text
                          style={{
                            justifyContent: "flex-start",
                            alignContent: "flex-start",
                            color: "#6D757F",
                            paddingRight: 10,
                            fontFamily: ConstantFontFamily.VerdanaFont,
                            fontSize: 13,
                            alignSelf: "center",
                            textDecorationLine:
                              isHovered == true ? "underline" : "none",
                            flexWrap: 'wrap',
                            flex: 1
                          }}
                        >
                          @
                          {props.item.item.node.author &&
                            props.item.item.node.author.username}
                        </Text>
                      </TouchableHighlight>
                    )}
                  </Hoverable>
                </View>
              ) : (
                <Hoverable>
                  {isHovered => (
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      {props.item.item.node.link != null &&
                        props.item.item.node.link != "" ? (
                        props.item.item.node.external_feed != null &&
                          props.item.item.node.external_feed.icon_url != null ? (
                          <TouchableOpacity
                            style={{
                              alignSelf: "center"
                            }}
                            onPress={() =>
                              goToFeedProfile(
                                props.item.item.node.external_feed.id.replace(
                                  "ExternalFeed:",
                                  ""
                                )
                              )
                            }
                          >
                            <Image
                              source={{
                                uri: props.item.item.node.external_feed.icon_url
                              }}
                              style={{
                                width: 22,
                                height: 22,
                                borderRadius: 15
                              }}
                            />
                          </TouchableOpacity>
                        ) : (
                          <View style={{ alignSelf: "center" }}>
                            <Icon
                              name="link"
                              type="font-awesome"
                              color="#6D757F"
                              size={18}
                            />
                          </View>
                        )
                      ) : null}
                      <TouchableOpacity
                        style={{
                          alignSelf: "center",
                          // marginLeft: 5,
                          // maxWidth: 300,
                          width: '95%',
                          flexDirection: 'row',
                        }}
                        onPress={() => openWindow(props.item.item.node.link)}
                      >
                        <Text
                          numberOfLines={1}
                          style={{
                            textAlign: "center",
                            alignSelf: "center",
                            color: "#6D757F",
                            fontSize: props.ViewMode == "Card" ? 11 : 13,
                            fontFamily: ConstantFontFamily.VerdanaFont,
                            textDecorationLine:
                              isHovered == true ? "underline" : "none",
                            flexWrap: 'wrap',
                            flex: 1
                          }}
                        >
                          {
                            props.item.item.node.link
                              .replace("http://", "")
                              .replace("https://", "")
                              .replace("www.", "")
                              .split(/[/?#]/)[0]
                          }
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )}
                </Hoverable>
              )}
              {/* <View style={{ alignSelf: "center", marginLeft: 15 }}>
                <Text
                  numberOfLines={1}
                  style={{
                    textAlign: "center",
                    color: "#6D757F",
                    fontSize: props.ViewMode == "Card" ? 10 : 12,
                    fontFamily: ConstantFontFamily.VerdanaFont,
                    alignSelf: "center"
                  }}
                >
                  {props.ViewMode != "Card" && props.ViewMode != "Default"
                    ? moment
                        .utc(props.item.item.node.created)
                        .local()
                        .fromNow()
                    : moment
                        .utc(props.item.item.node.created)
                        .local()
                        .fromNow(true)}
                </Text>
              </View> */}
            </View>

            <View
              style={{
                width: "40%",
                flexDirection: "row",
                flex: 1,
                justifyContent: Dimensions.get('window').width <= 750 ? "space-evenly" : 'space-between',
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                onMouseEnter={() => setMenuHover(true)}
                onMouseLeave={() => setMenuHover(false)}
              >
                <Menu>
                  <MenuTrigger>
                    <Image
                      source={require("../assets/image/menu.png")}
                      style={{
                        height: 16,
                        width: 16,
                        alignSelf: "flex-end",
                        marginRight: 15
                        //transform: [{ rotate: "90deg" }]
                      }}
                    />
                  </MenuTrigger>
                  <MenuOptions
                    optionsContainerStyle={{
                      borderRadius: 6,
                      borderWidth: 1,
                      borderColor: "#e1e1e1",
                      shadowColor: "transparent"
                    }}
                    customStyles={{
                      optionsContainer: {
                        minHeight: 50,
                        width: 150,
                        marginTop: 15,
                        marginLeft: -140,
                        shadowColor: "#000",
                        shadowOffset: {
                          width: 0,
                          height: 0,
                        },
                        shadowOpacity: 0.3,
                        shadowRadius: 3,
                        elevation: 5,
                      }
                    }}
                  >
                    <MenuOption
                      onSelect={() => {
                        props.loginStatus == 1
                          ? handleBugClick()
                          : props.setLoginModalStatus(true);
                      }}
                    >
                      <Hoverable>
                        {isHovered => (
                          <Text
                            style={{
                              textAlign: "center",
                              color: isHovered == true ? "#009B1A" : "#000",
                              fontFamily: ConstantFontFamily.MontserratBoldFont
                            }}
                          >
                            Report
                          </Text>
                        )}
                      </Hoverable>
                    </MenuOption>

                    <MenuOption onSelect={() => writeToClipboard()}>
                      <Hoverable>
                        {isHovered => (
                          <Text
                            style={{
                              textAlign: "center",
                              color: isHovered == true ? "#009B1A" : "#000",
                              fontFamily: ConstantFontFamily.MontserratBoldFont
                            }}
                          >
                            Copy Link
                          </Text>
                        )}
                      </Hoverable>
                    </MenuOption>
                    {props.isAdmin == true && (
                      <MenuOption
                        onSelect={() => {
                          props.loginStatus == 1
                            ? handleDeleteFeed(props.item.item.node.id)
                            : props.setLoginModalStatus(true);
                        }}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Delete
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>
                    )}
                    {props.isAdmin == true && (
                      <MenuOption
                        onSelect={() =>
                          props.setPostEditDetails(props.item.item.node)
                        }
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                textAlign: "center",
                                color: isHovered == true ? "#009B1A" : "#000",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont
                              }}
                            >
                              Edit
                            </Text>
                          )}
                        </Hoverable>
                      </MenuOption>
                    )}
                  </MenuOptions>
                </Menu>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  alignItems: "center"
                }}
                onPress={() =>
                  props.loginStatus == 1
                    ? handleClickComment()
                    : props.setLoginModalStatus(true)
                }
              >
                <Icon
                  color={getColour(props.item.item.node.comments_score)}
                  name={"comment-o"}
                  type="font-awesome"
                  size={20}
                  iconStyle={{ alignSelf: "center", marginBottom: 2 }}
                  containerStyle={{ alignSelf: "center" }}
                />
                <Text
                  style={{
                    marginBottom: 2,
                    textAlign: "center",
                    fontSize: 13,
                    paddingHorizontal: 3,
                    fontFamily: ConstantFontFamily.VerdanaFont,
                    color: "#6D757F"
                  }}
                >
                  {props.item.item.node.comments_score}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <ChangeIcon
                  PostId={props.item.item.node.id}
                  selectedIconList={props.item.item.node.user_like_type}
                  score={props.item.item.node.likes_score}
                  loginModalStatus={loginModalStatusEvent}
                  onOpen={status => handleClick(status)}
                  showStatus={showBug}
                  pressBug={pressBug}
                />
                <Text
                  style={{
                    marginBottom: 2,
                    textAlign: "center",
                    paddingHorizontal: 3,
                    fontSize: 13,
                    fontFamily: ConstantFontFamily.VerdanaFont,
                    color: "#6D757F"
                  }}
                >
                  {props.item.item.node.likes_score}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableOpacity>

      {props.ViewMode != "Card" && (
        <View
          style={{
            marginBottom: 10
          }}
        ></View>
      )}
    </TouchableOpacity>
  );
};

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  trendingHomeFeedId: state.PostCommentDetailsReducer.get("PostId"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView"),
  PostId: state.PostCommentDetailsReducer.get("PostId"),
});

const mapDispatchToProps = dispatch => ({
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  cliksUserId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  topicId: payload => dispatch(getTrendingTopicsProfileDetails(payload)),
  listTopicFeed: payload => dispatch(listTopicFeed(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setFeedReportModalAction: payload =>
    dispatch(setFEEDREPORTMODALACTION(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload)),
  setFeedDetails: payload => dispatch(getFeedProfileDetails(payload)),
  setFeedShareModal: payload => dispatch({ type: "SHAREFEEDSTATUS", payload }),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setComment: payload => dispatch({ type: "SET_COMMENT", payload }),
  setPostCommentReset: payload =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload }),
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  setPostEditDetails: payload => dispatch(postEditDetails(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  React.memo(FeedList)
);
