import { List } from "immutable";
import React from "react";
import { graphql } from "react-apollo";
import {
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  ActivityIndicator,
  Dimensions
} from "react-native";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import applloClient from "../client";
import ConstantFontFamily from "../constants/FontFamily";
import {
  UserFollowMutation,
  UserUnfollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  UserFollowVariables,
  UserUnfollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import NavigationService from "../library/NavigationService";
import { Button, Icon } from "react-native-elements";
import UserStar from "./UserStar";
import { SearchUserMutation } from "../graphqlSchema/graphqlMutation/SearchMutation";
import { SearchUserVariables } from "../graphqlSchema/graphqlVariables/SearchVariables";
import { Trending_Users_Mutation } from "../graphqlSchema/graphqlMutation/TrendingMutation";

class TrendingUsers extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onEndReachedCalledDuringMomentum = true;
    this.viewabilityConfig = {
      viewAreaCoveragePercentThreshold: 50
    };
    this.state = {
      searchedFollowText: this.props.searchedFollowText,
      listTrending_users: [],
      page: 1,
      loading: true,
      loadingMore: false,
      refreshing: false,
      pageInfo: null,
      error: null,
      apiCall: true
    };
  }

  goToProfile = username => {
    this.props.userId({
      username: username,
      type: "feed"
    });
    NavigationService.navigate("profile", {
      username: username,
      type: "feed"
    });
    this.props.setPostCommentReset({
      payload: [],
      postId: "",
      title: "",
      loading: true
    });
    this.props.leftPanelModalFunc(false)
  };

  componentDidUpdate = async prevProps => {
    if (prevProps.searchedFollowText != this.props.searchedFollowText) {
      await this.userSearch(this.props.searchedFollowText);
    } else if (prevProps.searchedFollowText == "") {
      //this.setState({ listTrending_users: this.props.listTrending_users });
    }
  };

  userSearch = search => {
    this.setState({ search });
    let tempData = [];
    let tempArray = [];

    if (search.length > 0) {
      SearchUserVariables.variables.prefix = search;
      applloClient
        .query({
          query: SearchUserMutation,
          ...SearchUserVariables,
          fetchPolicy: "no-cache"
        })
        .then(res => {
          tempArray = res.data.search.users;
          for (let i = 0; i < tempArray.length; i++) {
            tempData.push({ node: tempArray[i] });
          }
          this.setState({
            listTrending_users: tempData
          });
        });
    } else {
      this.setState({ pageInfo: null }, () => {
        this.getTrendingUserList();
      });
      // this.setState({ listTrending_users: this.props.listTrending_users });
    }
  };

  componentDidMount = () => {
    this.getTrendingUserList();
  };

  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this.getTrendingUserList();
      }
    );
  };

  _handleLoadMore = distanceFromEnd => {
    this.setState(
      (prevState, nextProps) => ({
        //  loadingMore: true
      }),
      () => {
        if (
          0 <= distanceFromEnd &&
          distanceFromEnd <= 5 &&
          this.state.apiCall == true
        ) {
          this.setState({
            apiCall: false
          });
          this.getTrendingUserList();
        }
      }
    );
  };

  _renderFooter = () => {
    if (!this.state.loadingMore) return null;
    return (
      <View>
        <ActivityIndicator animating size="large" color="#000" />
      </View>
    );
  };

  getTrendingUserList = () => {
    let __self = this;
    const { page, pageInfo } = this.state;

    if (pageInfo == null) {
      this.setState({
        loadingMore: true
      });
      applloClient
        .query({
          query: Trending_Users_Mutation,
          variables: {
            first: 20,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "TRENDING"
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          __self.setState({
            loading: false,
            listTrending_users: this.props.getUserFollowUserList
              .toJS()
              .concat(this.getUserList(res.data.user_list.edges)),
            pageInfo: res.data.user_list.pageInfo,
            page: page + 1,
            apiCall: true,
            loadingMore: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else if (pageInfo != null && pageInfo.hasNextPage == true) {
      this.setState({
        loadingMore: true
      });
      applloClient
        .query({
          query: Trending_Users_Mutation,
          variables: {
            first: 20,
            after: pageInfo ? pageInfo.endCursor : null,
            sort: "TRENDING"
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          __self.setState({
            loading: false,
            listTrending_users: __self.state.listTrending_users.concat(
              this.getUserList(res.data.user_list.edges)
            ),
            pageInfo: res.data.user_list.pageInfo,
            apiCall: true,
            loadingMore: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    }
  };

  onViewableItemsChanged = ({ viewableItems, changed }) => {
    let perLoadDataCount = 20;
    let halfOfLoadDataCount = perLoadDataCount / 2;
    let lastAddArr =
      this.state.listTrending_users.length > 0 &&
      this.state.listTrending_users.slice(-perLoadDataCount);
    try {
      if (lastAddArr[halfOfLoadDataCount] && viewableItems.length > 0) {
        viewableItems.find(item => {
          let userId = item.item.node ? item.item.node.id : item.item.user.id;
          let LastuserId = lastAddArr[halfOfLoadDataCount].node
            ? lastAddArr[halfOfLoadDataCount].node.id
            : lastAddArr[halfOfLoadDataCount].user.id;
          if (userId === LastuserId) {
            this._handleLoadMore(0);
          }
        });
      }
    } catch (e) {
      console.log(e, "lastAddArr", lastAddArr[halfOfLoadDataCount]);
    }
  };

  getUserList = item => {
    let newArray = [];
    item.forEach(obj => {
      let index = this.props.getUserFollowUserList.findIndex(
        i => i.getIn(["user", "username"]) == obj.node.username
      );
      if (index == -1) {
        newArray.push({ ...obj });
      }
    });
    return newArray;
  };

  _renderItem = item => {
    var row = item.item.node ? item.item.node : item.item.user;
    return (
      <View
        key={item.index}
        style={{
          overflow: "hidden",
          padding: 2,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          width: "100%"
        }}
      >
        <Hoverable>
          {isHovered => (
            <TouchableOpacity
              onPress={() => this.goToProfile(row.username)}
              style={{
                flexDirection: "row",
                borderRadius: 4,
                overflow: "hidden",
                width: "85%"
              }}
            >
              <View
                style={{
                  //width: "20%",
                  margin: 0,
                  flexDirection: "row",
                  backgroundColor: "rgba(255,255,255,0.5)"
                }}
              >
                {row.profile_pic ? (
                  <Image
                    source={{
                      uri: row.profile_pic
                    }}
                    style={{
                      width: 30,
                      height: 30,
                      borderRadius: 18
                    }}
                  />
                ) : (
                  <Image
                    source={require("../assets/image/default-image.png")}
                    style={{
                      width: 30,
                      height: 30,
                      borderRadius: 18
                    }}
                  />
                )}
              </View>
              <View
                style={{
                  //width: "80%",
                  alignSelf: "center",
                  marginLeft: '5%'
                }}
              >
                <Text
                  style={{
                    textAlign: "left",
                    fontSize: 14,
                    fontWeight: "bold",
                    fontFamily: ConstantFontFamily.defaultFont,
                    textDecorationLine: isHovered == true ? "underline" : "none"
                  }}
                >
                  @{row.username}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        </Hoverable>

        <UserStar
          UserName={row.username}
          UserId={row.id}
          ContainerStyle={{
            flex: 1,
            justifyContent: "flex-end",
            width: "20%",
            paddingLeft: 15
          }}
          ImageStyle={{
            height: 20,
            width: 20,
            alignSelf: "flex-end"
          }}
        />
      </View>
    );
    //}
  };

  render() {
    return (
      <View>
        <FlatList
          extraData={this.state}
          contentContainerStyle={{
            flexDirection: "column",
            height: Dimensions.get("window").height,
            width: "100%"
          }}
          data={this.state.listTrending_users}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItem}
          showsVerticalScrollIndicator={false}
          onRefresh={this._handleRefresh}
          refreshing={this.state.refreshing}
          // onEndReached={({ distanceFromEnd }) => {
          //   this._handleLoadMore(distanceFromEnd);
          // }}
          onEndReachedThreshold={0.2}
          initialNumToRender={10}
          ListFooterComponent={this._renderFooter}
          onViewableItemsChanged={this.onViewableItemsChanged}
          viewabilityConfig={this.viewabilityConfig}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  listTrending_users: !state.TrendingUsersReducer.getIn(["Trending_users_List"])
    ? List()
    : state.TrendingUsersReducer.getIn(["Trending_users_List"]),
  getUserFollowUserList: state.LoginUserDetailsReducer.get("userFollowUserList")
    ? state.LoginUserDetailsReducer.get("userFollowUserList")
    : List(),
  loginStatus: state.UserReducer.get("loginStatus")
});

const mapDispatchToProps = dispatch => ({
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setPostCommentReset: payload =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload }),
  leftPanelModalFunc : payload => dispatch ({type : 'LEFT_PANEL_OPEN' , payload})
});

const TrendingUsersWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(TrendingUsers);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  TrendingUsersWrapper
);
