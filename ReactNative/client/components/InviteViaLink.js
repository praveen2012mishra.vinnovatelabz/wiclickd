import React, { Component } from "react";
import { Text, View, Clipboard } from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import ButtonStyle from "../constants/ButtonStyle";
import { Button, Icon } from "react-native-elements";
import ConstantFontFamily from "../constants/FontFamily";
import { List } from "immutable";

class InviteViaLink extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCopiedText: false
    };
  }
  writeToClipboard = async () => {
    //To copy the text to clipboard
    await Clipboard.setString(
      "https://"+window.location.href
        .replace("http://", "")
        .replace("https://", "")
        .replace("www.", "")
        .split(/[/?#]/)[0] +
        "/clik/" +
        this.props.cliksDetails.getIn(["data", "clik"]).get("name") +
        "/join"
    );
    this.setState({
      showCopiedText: true
    });
    let timer = setTimeout(() => {
      this.setState({
        showCopiedText: false
      });
    }, 2000);
  };
  render() {
    return (
      <View
        style={[ButtonStyle.cardShadowStyle, {
          borderWidth: 0,
          borderColor: "#C5C5C5",
          borderRadius: 10,
          backgroundColor: "#fff",
          marginBottom: 10
        }]}
      >
        <Text
          style={{
            textAlign: "left",
            color: "#000",
            fontSize: 16,
            fontWeight: "bold",
            fontFamily: ConstantFontFamily.MontserratBoldFont,
            marginVertical: 10,
            marginHorizontal: 10
          }}
        >
          Invite Via Link
        </Text>
        <View
          style={{
            marginBottom: 20,
            alignSelf: "center",
            flexDirection: "row",
            width: "100%",
            justifyContent: "center"
          }}
        >
          <View
            style={{
              borderWidth: 1,
              borderColor: "#C5C5C5",
              borderRadius: 10,
              padding: 10,
              width: "65%"
            }}
          >
            <Text
              style={{
                textAlign: "left",
                color: "#000",
                fontSize: 14,
                fontWeight: "bold",
                fontFamily: ConstantFontFamily.defaultFont
              }}
            >
              {"https://"+window.location.href
                .replace("http://", "")
                .replace("https://", "")
                .replace("www.", "")
                .split(/[/?#]/)[0] +
                "/clik/" +
                this.props.cliksDetails.getIn(["data", "clik"]).get("name") +
                "/join"}
            </Text>
          </View>
          <Icon
            color={"#000"}
            iconStyle={{
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center"
            }}
            name="clone"
            type="font-awesome"
            size={18}
            containerStyle={{
              marginLeft: 40,
              alignSelf: "center"
            }}
            onPress={() => this.writeToClipboard()}
          />
        </View>
        {this.state.showCopiedText == true && (
          <Text
            style={{
              color: "#000",
              fontSize: 14,
              fontWeight: "bold",
              fontFamily: ConstantFontFamily.defaultFont,
              marginVertical: 10,
              textAlign: "center"
            }}
          >
            {"Invitation Link Copied!"}
          </Text>
        )}
      </View>
    );
  }
}
const mapStateToProps = state => ({
  cliksDetails: state.TrendingCliksProfileReducer.get(
    "getTrendingCliksProfileDetails"
  ),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  listClikUserRequest: !state.ClikUserRequestReducer.get("ClikUserRequestList")
    ? List()
    : state.ClikUserRequestReducer.get("ClikUserRequestList"),
  listClikMembers: !state.ClikMembersReducer.get("clikMemberList")
    ? List()
    : state.ClikMembersReducer.get("clikMemberList"),
  loginStatus: state.UserReducer.get("loginStatus"),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List()
});

const mapDispatchToProps = dispatch => ({});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  InviteViaLink
);
