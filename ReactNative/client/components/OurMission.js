import React, { Component } from "react";
import { Text, View } from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import ConstantFontFamily from "../constants/FontFamily";
import { phrases } from "../constants/WeclikdIntro";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

class OurMission extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: phrases,
      index: 0
    };
  }

  openLoginModal = async () => {
    this.props.setLoginModalStatus(true);
  };

  signHandle = async () => {
    this.props.setSignUpModalStatus(true);
  };

  componentDidMount() {
    this.reCondition();
  }

  reCondition = () => {
    let __self = this;
    this.time = setInterval(() => {
      if (__self.state.index + 1 == __self.state.content.length) {
        __self.setState({
          index: 0
        });
      } else {
        __self.setState({
          index: __self.state.index + 1
        });
      }
    }, 60000);
  };

  componentWillUnmount() {
    if (this.time > 0) {
      clearInterval(this.timer);
    }
  }

  render() {
    return (
      <View>
        <Text
          style={{
            textAlign: "center",
            fontSize: 20,
            //this.state.content[this.state.index].length < 75 ? 35 : 30,
            color: "#000000",
            fontWeight: "bold",
            fontFamily: ConstantFontFamily.MontserratBoldFont
          }}
        >
          {this.state.content[this.state.index]}
        </Text>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus")
});

const mapDispatchToProps = dispatch => ({
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  OurMission
);
