import React, { Component, lazy, Suspense } from "react";
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  TextInput,
  Dimensions
} from "react-native";
import { TabBar, TabView } from "react-native-tab-view";
import ShadowSkeletonStar from "../components/ShadowSkeletonStar";
import ConstantFontFamily from "../constants/FontFamily";
import { retry } from "../library/Helper";
import { Button, Icon } from "react-native-elements";
import NavigationService from "../library/NavigationService";
import { connect } from "react-redux";
import { compose } from "recompose";
import ButtonStyle from "../constants/ButtonStyle";
import { AsyncStorage } from "react-native";

const CliksListDrawerScreens = lazy(() =>
  retry(() => import("../components/CliksListDrawerScreens"))
);
const FeedListDrawerScreen = lazy(() =>
  retry(() => import("../components/FeedListDrawerScreen"))
);
const TopicsListDrawerScreens = lazy(() =>
  retry(() => import("../components/TopicsListDrawerScreens"))
);
const UserListDrawerScreens = lazy(() =>
  retry(() => import("../components/UserListDrawerScreens"))
);

class DrawerScreens extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    index: 0,
    searchedWord: this.props.searchedWord,
    cal: 0,
    FeedListDrawerScreensHeigth: 0,
    UserListDrawerScreensHeigth: 0,
    CliksListDrawerScreensHeigth: 0,
    TopicsListDrawerScreensHeigth: 0,
    routes: [
      { key: "first", title: "Cliks", icon: "users", type: "font-awesome" },
      { key: "second", title: "Topics", icon: "book", type: "font-awesome" },
      { key: "third", title: "Users", icon: "user", type: "font-awesome" },
      { key: "fourth", title: "Feeds", icon: "rss", type: "font-awesome" }
    ],
    loading: false
  };

  changeColor = isHovered => {
    if (isHovered == true) {
      switch (this.state.primaryIndex) {
        case 0:
          return "#009B1A";
        case 1:
          return "#4C82B6";
        case 2:
          return "#FEC236";
        default:
          return "#F34225";
      }
    }
  };

  styleChange = () => {
    if (this.state.index == 0) {
      return this.state.TopicsListDrawerScreensHeigth + 50;
    } else if (this.state.index == 1) {
      return this.state.CliksListDrawerScreensHeigth + 50;
    } else if (this.state.index == 2) {
      return this.state.UserListDrawerScreensHeigth + 50;
    } else if (this.state.index == 3) {
      return this.state.FeedListDrawerScreensHeigth + 50;
    }
  };

  searchTextField = value => {
    this.setState({ searchedWord: value });
  };

  _renderTabBar = props => (    
    <View style={{height: 70}}>
      <TabBar
        {...props}
        indicatorStyle={{
          backgroundColor: "transparent",
          height: 1,
          borderRadius: 6
          // width: "18.75%",
          // marginHorizontal: "3.125%"
        }}
        style={{ backgroundColor: "transparent", shadowColor: "transparent", height:60 }}
        labelStyle={{
          color: "#000",
          fontFamily: ConstantFontFamily.MontserratBoldFont,
          fontSize: 13,
          fontWeight: "bold",
          height:60,
          justifyContent:'center',
          
        }}
        renderIcon={({ route, focused, color }) => (
          //Dimensions.get('window').width >=750 &&
          <Icon
            name={route.icon}
            type={route.type}
            color={focused ? "#009B1A" : "#D3D3D3"}
          />
        )}
        renderLabel={({ route, focused, color }) => (
          <Text
            style={{
              color: focused ? "#009B1A" : "#D3D3D3",
              fontFamily: ConstantFontFamily.MontserratBoldFont
            }}
          >
            {route.title}
          </Text>
        )}
        // renderIndicator={({ route, focused, color }) => null}
      />
      <View
        style={{
          width: "100%",
          height: 40,
          justifyContent: "flex-start",
          flexDirection: "row",
          alignItems: "center",
          // borderColor: "#000",
          // borderWidth: 1,
          // borderRadius: 5,
          paddingHorizontal: "3%",          
          marginTop:  10,
          marginBottom:5
        }}
      >
        <View style={{ width: "10%", marginRight: "auto" }}>
          <Icon name="search" size={18} type="font-awesome" />
        </View>

        <TextInput
          autoFocus={false}
          placeholder={
            this.state.index == 0
              ? "Search Cliks"
              : this.state.index == 1
              ? "Search Topics"
              : this.state.index == 2
              ? "Search Users"
              : this.state.index == 3 && "Search Feeds"
          }
          onChangeText={query => this.searchTextField(query)}
          value={this.state.searchedWord}
          style={{
            height: 40,
            width: "74%",
            paddingHorizontal: 10,
            //border: "none",
            outline: "none",
            position: "absolute",
            left: "13%",
            fontFamily: ConstantFontFamily.defaultFont
          }}
        />
        {this.state.searchedWord ? (
          <TouchableOpacity
            onPress={() => this.setState({ searchedWord: "" })}
            style={{ marginLeft: "auto", width: "10%" }}
          >
            <Icon name="close" size={18} type="font-awesome" />
          </TouchableOpacity>
        ) : null}
      </View>
    </View>
  );

  getSettingButtonState = type => {
    //sessionStorage.setItem("getSettingButtonState", true);
    AsyncStorage.setItem("getSettingButtonState", true);
  };

  _handleIndexChange = index => {
    this.setState({ index, searchedWord: "" }, () => {
      this.props.activeIndex(index);
    });
  };
  componentDidUpdate = async prevProps => {
    if (prevProps.searchedWord !== this.props.searchedWord) {
      await this.setState({ searchedWord: this.props.searchedWord });
    }
  };

  _renderLazyPlaceholder = ({ route }) => <ShadowSkeletonStar />;

  _renderScene = ({ route }) => {
    switch (route.key) {
      case "fourth":
        return (
          <View
            onLayout={event => {
              let { height } = event.nativeEvent.layout;
              this.setState({
                FeedListDrawerScreensHeigth: height
              });
            }}
            style={{ flex: 1, marginTop:50  }}
          >
            <ScrollView showsVerticalScrollIndicator={false}>
              <Suspense fallback={<ShadowSkeletonStar />}>
                <FeedListDrawerScreen
                  current={this.state.index}
                  navigation={this.props.navigation}
                  searchedWord={this.state.searchedWord}
                />
              </Suspense>
            </ScrollView>
            <Button
              onPress={() => {
                this.props.leftPanelModalFunc(false)
                return NavigationService.navigate("addfeed");                
              }}
              color="#fff"
              title="Add Feed"
              titleStyle={ButtonStyle.titleStyle}
              buttonStyle={ButtonStyle.backgroundStyle}
              containerStyle={ButtonStyle.containerStyle}
            />
          </View>
        );
      case "third":
        return (
          <View
            onLayout={event => {
              let { height } = event.nativeEvent.layout;
              this.setState({
                UserListDrawerScreensHeigth: height
              });
            }}
            style={{ flex: 1, marginTop:50  }}
          >
            <ScrollView showsVerticalScrollIndicator={false}>
              <Suspense fallback={<ShadowSkeletonStar />}>
                <UserListDrawerScreens
                  current={this.state.index}
                  navigation={this.props.navigation}
                  searchedWord={this.state.searchedWord}
                />
              </Suspense>
            </ScrollView>
            <Button
              onPress={() => {
                this.getSettingButtonState("Invite Friends");
                this.props.leftPanelModalFunc(false)
                return NavigationService.navigate("settings");                
              }}
              title="Invite Friends"
              titleStyle={ButtonStyle.titleStyle}
              buttonStyle={ButtonStyle.backgroundStyle}
              containerStyle={ButtonStyle.containerStyle}
            />
          </View>
        );
      case "second":
        return (
          <View
            onLayout={event => {
              let { height } = event.nativeEvent.layout;
              this.setState({
                TopicsListDrawerScreensHeigth: height
              });
            }}
            style={{ flex: 1, marginTop:50  }}
          >
            <ScrollView showsVerticalScrollIndicator={false}>
              <Suspense fallback={<ShadowSkeletonStar />}>
                <TopicsListDrawerScreens
                  navigation={this.props.navigation}
                  current={this.state.index}
                  searchedWord={this.state.searchedWord}
                />
              </Suspense>
            </ScrollView>
            {this.props.isAdmin == true && (
              <Button
                onPress={() => {
                  this.props.leftPanelModalFunc(false)
                  return NavigationService.navigate("createtopic");                
                }}
                title="Propose Topic"
                titleStyle={ButtonStyle.titleStyle}
                buttonStyle={ButtonStyle.backgroundStyle}
                containerStyle={ButtonStyle.containerStyle}
              />
            )}

            {this.props.isAdmin == false && (
              <Button
                onPress={() => NavigationService.navigate("topichierarchy")}
                title="Topic Hierarchy"
                titleStyle={ButtonStyle.titleStyle}
                buttonStyle={ButtonStyle.backgroundStyle}
                containerStyle={ButtonStyle.containerStyle}
              />
            )}
          </View>
        );
      default:
        return (
          <View
            onLayout={event => {
              let { height } = event.nativeEvent.layout;
              this.setState({
                CliksListDrawerScreensHeigth: height
              });
            }}
            style={{flex: 1, marginTop:50 }}
          >
            <ScrollView showsVerticalScrollIndicator={false}>
              <Suspense fallback={<ShadowSkeletonStar />}>
                <CliksListDrawerScreens
                  current={this.state.index}
                  navigation={this.props.navigation}
                  searchedWord={this.state.searchedWord}
                />
              </Suspense>
            </ScrollView>
            <Button
              onPress={() => {
                NavigationService.navigate("createclik");
                this.props.leftPanelModalFunc(false)
              }}
              color="#fff"
              title="New Clik"
              titleStyle={ButtonStyle.titleStyle}
              buttonStyle={ButtonStyle.backgroundStyle}
              containerStyle={ButtonStyle.containerStyle}
            />
          </View>
        );
    }
  };

  render() {
    return (
      <TabView
        lazy
        navigationState={this.state}
        renderScene={this._renderScene}
        renderLazyPlaceholder={this._renderLazyPlaceholder}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
        style={{
          height: this.styleChange(),
          marginRight: 1
        }}
      />
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  isAdmin: state.AdminReducer.get("isAdmin"),
  isAdminView: state.AdminReducer.get("isAdminView")
});
const mapDispatchToProps = dispatch => ({
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  leftPanelModalFunc: payload => dispatch({ type: 'LEFT_PANEL_OPEN', payload }),
});
export default compose(connect(mapStateToProps, mapDispatchToProps))(
  DrawerScreens
);
