import React, { Component } from "react";
import {
  Image,
  StyleSheet,
  AsyncStorage,
  TouchableOpacity
} from "react-native";
import ActionButton from "react-native-circular-action-menu";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { compose } from "recompose";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import AppHelper from "../constants/AppHelper";

class TopFloatingButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      indexicon: 0,
      profilePic: ""
    };
  }

  componentDidMount = async () => {
    const user = this.props.profileData;
    let userProfilePic = user
      .getIn(["my_users", "0", "user"])
      .getIn(["profile_pic"]);
    this.setState({
      profilePic: {
        uri: userProfilePic
      }
    });
  };

  getIcon = async () => {
    if (this.state.indexicon == 0) {
      this.setState({
        indexicon: 1
      });
    } else {
      let id = await AsyncStorage.getItem("UserId");
      this.setState({
        indexicon: 0
      });
      this.props.userId({
        id: id
      });
      this.props.navigation.navigate("profile", {
        id: id
      });
    }
  };

  getBack = () => {
    this.setState({
      indexicon: 0
    });
  };

  goToHome = () => {
    this.props.navigation.navigate("home");
    this.setState({
      indexicon: 0
    });
  };

  render() {
    const { indexicon } = this.state;
    return (
      <ActionButton
        position="right"
        radius={70}
        buttonColor={"#fff"}
        btnOutRange={"#fff"}
        bgColor={"transparent"}
        onOverlayPress={this.getBack}
        onPress={this.getIcon}
        degrees={0}
        startDegree={550}
        endDegree={450}
        icon={
          indexicon == 0 &&
          this.props.profileData
            .getIn(["my_users", "0", "user"])
            .getIn(["profile_pic"]) ? (
            <Image
              source={{
                uri: this.props.profileData
                  .getIn(["my_users", "0", "user"])
                  .getIn(["profile_pic"]),
              }}
              style={{
                height: 40,
                width: 40,
                borderRadius: 20
              }}
              color="none"
            />
          ) : (
            <Image
              source={require("../assets/image/default-image.png")}
              style={{
                height: 40,
                width: 40,
                borderRadius: 20
              }}
              color="none"
            />
          )
        }
        size={40}
        zIndex={1000}
      >
        <ActionButton.Item buttonColor="#0440BD">
          <Icon name="mail" style={styles.actionButtonIcon} color="#fff" />
        </ActionButton.Item>
        <ActionButton.Item buttonColor="#159C23">
          <Icon name="search" style={styles.actionButtonIcon} color="#fff" />
        </ActionButton.Item>
        <ActionButton.Item buttonColor="#D8A829">
          <Icon
            name="notifications"
            style={styles.actionButtonIcon}
            color="#fff"
          />
        </ActionButton.Item>
        <ActionButton.Item buttonColor="#DB0030">
          <TouchableOpacity onPress={this.goToHome}>
            <Icon name="home" style={styles.actionButtonIcon} color="#fff" />
          </TouchableOpacity>
        </ActionButton.Item>
      </ActionButton>
    );
  }
}

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails")
});

const mapDispatchToProps = dispatch => ({
  userId: payload => dispatch(getCurrentUserProfileDetails(payload))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  TopFloatingButton
);

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 23,
    color: "white"
  }
});
