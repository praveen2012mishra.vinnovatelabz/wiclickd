import { fromJS, List } from "immutable";
import Modal from "modal-enhanced-react-native-web";
import React from "react";
import {
  Dimensions,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { Button, Icon } from "react-native-elements";
import Overlay from "react-native-modal-overlay";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import {
  saveUserLoginDaitails,
  setLoginStatus
} from "../actionCreator/UserAction";
import applloClient from "../client";
import CreateNewCliksModal from "../components/CreateNewCliksModal";
import LeaveCliksModal from "../components/LeaveCliksModal";
import ConstantFontFamily from "../constants/FontFamily";
import { SearchClikMutation } from "../graphqlSchema/graphqlMutation/SearchMutation";
import { SearchClikVariables } from "../graphqlSchema/graphqlVariables/SearchVariables";
import NavigationService from "../library/NavigationService";
import ClikStar from "./ClikStar";
import TrendingClik from "./TrendingClicks";

class CliksListDrawerScreens extends React.PureComponent {
  constructor(props) {
    super(props);
    this.timer;
    this.state = {
      createNewCliksModal: false,
      showEditIcon: true,
      showSearchIcon: true,
      search: "",
      value: 50,
      cliklistData: this.props.getUserFollowCliksList,
      showTooltip: false,
      showLeaveCliksModal: false,
      listTrending_cliks: this.props.listTrending_cliks
    };
  }

  goToProfile = async id => {
    this.props.userId({
      id: id,
      type: "feed"
    });
    this.props.setPostCommentReset({
      payload: [],
      postId: "",
      title: "",
      loading: true
    })
  };

  updateClikSearch = async search => {
    this.setState({ search });
    let tempData = [];
    let tempArray = [];
    let memberType = "";

    if (search.length > 0 && this.props.current === 0) {
      SearchClikVariables.variables.prefix = search;
      applloClient
        .query({
          query: SearchClikMutation,
          ...SearchClikVariables,
          fetchPolicy: "no-cache"
        })
        .then(res => {
          tempArray = res.data.search.cliks;
          for (let i = 0; i < tempArray.length; i++) {
            memberType = this.getMemberArray(tempArray[i].name);
            tempData.push({ clik: tempArray[i], member_type: memberType });
          }
          this.setState({
            cliklistData: fromJS(tempData),
            listTrending_cliks: []
          });
        });
    } else {
      this.setState({
        cliklistData: this.props.getUserFollowCliksList,
        listTrending_cliks: this.props.listTrending_cliks
      });
    }
  };

  getMemberArray = (itemId, clickArray) => {
    let member_type = "";
    const index = this.props.getUserFollowCliksList.findIndex(
      i =>
        i
          .getIn(["clik", "name"])
          .toLowerCase()
          .replace("clik:", "") ==
        itemId
          .replace("%3A", ":")
          .toLowerCase()
          .replace("clik:", "")
    );
    if (index != -1) {
      if (
        this.props.getUserFollowCliksList.getIn([index, "member_type"]) ==
        "SUPER_ADMIN"
      ) {
        member_type = "SUPER_ADMIN";
      } else if (
        this.props.getUserFollowCliksList.getIn([index, "member_type"]) ==
        "ADMIN"
      ) {
        member_type = "ADMIN";
      } else if (
        this.props.getUserFollowCliksList.getIn([index, "member_type"]) ==
        "MEMBER"
      ) {
        member_type = "MEMBER";
      }
    } else if (index == -1) {
      member_type = "FOLLOWER";
    }
    return member_type;
  };

  componentDidUpdate = async prevProps => {
    if (prevProps.searchedWord != this.props.searchedWord) {
      await this.updateClikSearch(this.props.searchedWord);
    } else if (prevProps.searchedWord.length == 0) {
      this.setState({ cliklistData: this.props.getUserFollowCliksList });
    }
  };

  componentWillUnmount = () => {
    clearTimeout(this.timer);
  };
  toogle = () => {
    if (this.state.showTooltip == false) {
      this.setState({ showTooltip: true });
    } else {
      this.setState({ showTooltip: false });
    }
  };

  onClose = () => {
    this.setState({
      createNewCliksModal: false
    });
    this.setState({
      showLeaveCliksModal: false
    });
  };

  calHeightLeftPannel(height) {
    if (height > 0) {
      if (this.props.current == 0) {
        this.props.calHeightLeftPannel(height);
      }
    }
  }

  loginHandle = () => {
    this.props.setLoginModalStatus(true);
  };

  render() {
    const {
      createNewCliksModal,
      cliklistData,
      showLeaveCliksModal
    } = this.state;
    return (
      <View>
        {this.props.loginStatus == 1 ? (
          <View>
            {createNewCliksModal == true ? (
              Platform.OS !== "web" ? (
                <Overlay
                  animationType="zoomIn"
                  visible={createNewCliksModal}
                  onClose={this.onClose}
                  closeOnTouchOutside
                  children={
                    <CreateNewCliksModal
                      onClose={this.onClose}
                      {...this.props}
                    />
                  }
                  childrenWrapperStyle={{
                    padding: 0,
                    margin: 0
                  }}
                />
              ) : (
                <Modal
                  isVisible={createNewCliksModal}
                  onBackdropPress={this.onClose}
                  style={{
                    marginHorizontal:
                      Dimensions.get("window").width > 750 ? "30%" : 0
                  }}
                >
                  <CreateNewCliksModal onClose={this.onClose} {...this.props} />
                </Modal>
              )
            ) : null}

            {/* {showLeaveCliksModal == true ? (
              Platform.OS !== "web" &&
              this.props.getCurrentDeviceWidthAction < 750 ? (
                <Overlay
                  animationType="zoomIn"
                  visible={showLeaveCliksModal}
                  onClose={() => this.onClose()}
                  closeOnTouchOutside
                  children={
                    <LeaveCliksModal
                      onClose={() => this.onClose()}
                      {...this.props}
                    />
                  }
                  childrenWrapperStyle={{
                    padding: 0,
                    margin: 0
                  }}
                />
              ) : (
                <Modal
                  isVisible={showLeaveCliksModal}
                  onBackdropPress={() => this.onClose()}
                  style={{
                    marginHorizontal:
                      Dimensions.get("window").width > 750 ? "30%" : 10,
                    padding: 0
                  }}
                >
                  <LeaveCliksModal
                    onClose={() => this.onClose()}
                    {...this.props}
                  />
                </Modal>
              )
            ) : null} */}
            {/* <ScrollView showsVerticalScrollIndicator={false}>
              {cliklistData.map((item, i) => {
                if (
                  item.getIn(["member_type"]) == "SUPER_ADMIN" ||
                  item.getIn(["member_type"]) == "ADMIN" ||
                  item.getIn(["member_type"]) == "MEMBER"
                ) {
                  return (
                    <View
                      key={i}
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        width: "100%"
                      }}
                    >
                      <View
                        style={{
                          width: "80%",
                          marginTop: 10,
                          backgroundColor: "#fff",
                          flexDirection: "row"
                        }}
                      >
                        <View
                          style={{
                            alignSelf: "center"
                          }}
                        >
                          {item.getIn(["member_type"]) == "MEMBER" ? (
                            <Image
                              source={require("../assets/image/badge.png")}
                              style={{
                                width: 30,
                                height: 30,
                                alignSelf: "flex-start"
                              }}
                            />
                          ) : item.getIn(["member_type"]) == "ADMIN" ? (
                            <Image
                              source={require("../assets/image/SBadge.png")}
                              style={{
                                width: 30,
                                height: 30,
                                alignSelf: "flex-start"
                              }}
                            />
                          ) : (
                            <Image
                              source={require("../assets/image/YBadge.png")}
                              style={{
                                width: 30,
                                height: 30,
                                alignSelf: "flex-start"
                              }}
                            />
                          )}
                        </View>
                        <TouchableOpacity
                          onPress={() =>
                            this.goToProfile(item.getIn(["clik", "name"]))
                          }
                          style={{
                            marginLeft: 5,
                            alignSelf: "flex-start",
                            padding: 5,
                            backgroundColor: "#E8F5FA",
                            borderRadius: 6,
                            maxWidth: "85%"
                          }}
                        >
                          <Hoverable>
                            {isHovered => (
                              <Text
                                style={{
                                  width: "100%",
                                  textAlign: "left",
                                  color: "#4169e1",
                                  fontSize: 15,
                                  fontWeight: "bold",
                                  fontFamily:
                                    ConstantFontFamily.MontserratBoldFont,
                                  textDecorationLine:
                                    isHovered == true ? "underline" : "none"
                                }}
                              >
                                #{item.getIn(["clik", "name"])}
                              </Text>
                            )}
                          </Hoverable>
                        </TouchableOpacity>
                      </View>

                      <ClikStar
                        ClikName={item.getIn(["clik", "name"])}
                        ContainerStyle={{
                          height: 30,
                          justifyContent: "center",
                          width: "20%",
                          alignItems: "center",
                          marginTop: 10,
                          paddingLeft: 10
                        }}
                        ImageStyle={{
                          height: 20,
                          width: 20,
                          alignSelf: "flex-end",
                          marginRight: 2
                        }}
                      />
                    </View>
                  );
                }
              })}

              {cliklistData.map((item, i) => {
                if (item.getIn(["member_type"]) == "FOLLOWER") {
                  return (
                    <View
                      key={i}
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "center",
                        width: "100%"
                      }}
                    >
                      <View style={{ width: "80%", backgroundColor: "#fff" }}>
                        <TouchableOpacity
                          onPress={() =>
                            this.goToProfile(item.getIn(["clik", "name"]))
                          }
                          style={{
                            marginTop: 10,
                            //marginLeft: 5,
                            height: 30,
                            alignSelf: "flex-start",
                            padding: 5,
                            backgroundColor: "#E8F5FA",
                            borderRadius: 6
                          }}
                        >
                          <Hoverable>
                            {isHovered => (
                              <Text
                                style={{
                                  width: "100%",
                                  color: "#4169e1",
                                  fontSize: 15,
                                  fontWeight: "bold",
                                  fontFamily:
                                    ConstantFontFamily.MontserratBoldFont,
                                  textDecorationLine:
                                    isHovered == true ? "underline" : "none"
                                }}
                              >
                                #{item.getIn(["clik", "name"])}
                              </Text>
                            )}
                          </Hoverable>
                        </TouchableOpacity>
                      </View>

                      <ClikStar
                        ClikName={item.getIn(["clik", "name"])}
                        ContainerStyle={{
                          height: 30,
                          justifyContent: "center",
                          width: "20%",
                          alignItems: "center",
                          marginTop: 10,
                          paddingLeft: 10
                        }}
                        ImageStyle={{
                          height: 20,
                          width: 20,
                          alignSelf: "flex-end",
                          marginRight: 2
                        }}
                      />
                    </View>
                  );
                }
              })}
            </ScrollView> */}
            <TrendingClik
              navigation={this.props.navigation}
              searchedFollowText={this.props.searchedWord}
            />
            {/* {this.state.listTrending_cliks.map((item, i) => {
              let index = this.props.getUserFollowCliksList.findIndex(
                i => i.getIn(["clik", "name"]) == item.node.name
              );
              if (index == -1 && i < 10) {
                return (
                  <View
                    key={i}
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                      width: "100%"
                    }}
                  >
                    <View style={{ width: "80%", backgroundColor: "#fff" }}>
                      <TouchableOpacity
                        onPress={() => this.goToProfile(item.node.name)}
                        style={{
                          marginTop: 10,
                          height: 30,
                          alignSelf: "flex-start",
                          padding: 5,
                          backgroundColor: "#E8F5FA",
                          borderRadius: 6
                        }}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                width: "100%",
                                color: "#4169e1",
                                fontSize: 15,
                                fontWeight: "bold",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                textDecorationLine:
                                  isHovered == true ? "underline" : "none"
                              }}
                            >
                              {" "}
                              #{item.node.name}{" "}
                            </Text>
                          )}
                        </Hoverable>
                      </TouchableOpacity>
                    </View>
                    <ClikStar
                      ClikName={item.node.name}
                      ContainerStyle={{
                        height: 30,
                        justifyContent: "center",
                        width: "20%",
                        alignItems: "center",
                        marginTop: 10,
                        paddingLeft: 10
                      }}
                      ImageStyle={{
                        height: 20,
                        width: 20,
                        alignSelf: "flex-end",
                        marginRight: 2
                      }}
                    />
                  </View>
                );
              }
            })} */}
          </View>
        ) : (
          <View
            style={{
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Icon
              color={"#000"}
              iconStyle={{
                color: "#fff",
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center"
              }}
              reverse
              name="tags"
              type="font-awesome"
              size={20}
              containerStyle={{
                alignSelf: "center"
              }}
            />
            <Text
              style={{
                fontSize: 14,
                fontWeight: "bold",
                fontFamily: ConstantFontFamily.MontserratBoldFont,
                color: "#000",
                alignSelf: "center"
              }}
            >
              <Text
                onPress={() => this.loginHandle()}
                style={{ textDecorationLine: "underline" }}
              >
                Login
              </Text>{" "}
              to follow cliks
            </Text>
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List(),
  listTrending_cliks: !state.TrendingCliksReducer.getIn(["Trending_cliks_List"])
    ? List()
    : state.TrendingCliksReducer.getIn(["Trending_cliks_List"]),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension"),
  loginStatus: state.UserReducer.get("loginStatus")
});

const mapDispatchToProps = dispatch => ({
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  changeLoginStatus: payload => dispatch(setLoginStatus(payload)),
  userId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setPostCommentReset: payload =>
    dispatch({ type: "POSTCOMMENTDETAILS_RESET", payload })
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  CliksListDrawerScreens
);

const styles = StyleSheet.create({
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
    width: "100%"
  }
});
