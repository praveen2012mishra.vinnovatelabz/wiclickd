import { List } from "immutable";
import moment from "moment";
import React, { Component } from "react";
import {
    Image,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import { Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";

class FeedImageDisplayUser extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {

        return (<View style={{
            marginLeft: 5,
            height: this.props.item.length == 1 ? this.props.height : 0,
            justifyContent: this.props.item.length == 1 ? 'center' : 'flex-start',
            alignItems: this.props.item.length == 1 ? 'center' : 'flex-start'
        }}>
            {this.props.item.length > 0 && (
                // this.props.item[0].profile_pic                 
                <Image
                    source={this.props.item[0].profile_pic ? this.props.item[0].profile_pic : require("../assets/image/default-image.png")}
                    style={{
                        width: 30,
                        height: 30,
                        borderRadius: 18,
                        // borderWidth: 1,
                        // borderColor: "#e1e1e1",
                        marginRight: 5,
                        position: 'relative',
                        top: 0,
                        left: 0
                    }}
                />
            )}
            {this.props.item.length > 1 && (<Image
                source={this.props.item[1].profile_pic ? this.props.item[1].profile_pic : require("../assets/image/default-image.png")}
                style={{
                    width: 30,
                    height: 30,
                    borderRadius: 18,
                    // borderWidth: 1,
                    // borderColor: "#e1e1e1",
                    marginRight: 5,
                    position: 'absolute', left: 25, top: 14
                }}
            />)}
            {this.props.item.length > 2 && (<Image
                source={this.props.item[2].profile_pic ? this.props.item[2].profile_pic : require("../assets/image/default-image.png")}
                style={{
                    width: 30,
                    height: 30,
                    borderRadius: 18,
                    // borderWidth: 1,
                    // borderColor: "#e1e1e1",
                    marginRight: 5,
                }}
            />)}
        </View>
        );
    }
}

const mapStateToProps = state => ({
    loginStatus: state.UserReducer.get("loginStatus"),
    isAdmin: state.AdminReducer.get("isAdmin"),
});

const mapDispatchToProps = dispatch => ({
    setActiveId: payload => dispatch({ type: "SET_ACTIVE_ID", payload })
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
    FeedImageDisplayUser
);
