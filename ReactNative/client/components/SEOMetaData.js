import React from "react";
import Helmet from "react-helmet";
import {
  AsyncStorage,
} from "react-native";
const SEOMetaData = props => {
  const title = props.title == "Weclikd" ? props.title : "Weclikd | " + props.title;
  const description = props.description;
  const image = props.image;
  const [getNotificationNumber,setNotificationNumber]=React.useState(5)

  React.useEffect(()=>{
    AsyncStorage.getItem('notificationMessageNumber').then((number)=>{
      console.log(number,'---------------------------------------');
      setNotificationNumber(number)
    })
  },[])

  const getTitleMessage=()=>{
    if(getNotificationNumber>0 && getNotificationNumber < 100){
      return '(' +getNotificationNumber + ')' + title 
    }
    if( getNotificationNumber > 100){
      return '(' +'100' +'+' +')' + title 
    }
    else{
      return title
    }
    
  }

  return (
    <Helmet>
      {/* General tags */}
      <title>{getTitleMessage()}</title>
      <meta name="description" content={description} />

      {/* Twitter Card tags */}
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:image" content={image} />
      <meta name="twitter:card" content="summary_large_image" />
    </Helmet>
  );
};

export default SEOMetaData;
