import { List } from "immutable";
import React from "react";
import { graphql } from "react-apollo";
import {
  ActivityIndicator,
  Dimensions,
  FlatList,
  Image,
  Text,
  View,
  ImageBackground,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import { listClikMembers } from "../actionCreator/ClikMembersAction";
import { listClikUserRequest } from "../actionCreator/ClikUserRequestAction";
import { getFeedProfileDetails } from "../actionCreator/FeedProfileAction";
import { setHASSCROLLEDACTION } from "../actionCreator/HasScrolledAction";
import { getHomefeedList } from "../actionCreator/HomeFeedAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setPostCommentDetails } from "../actionCreator/PostCommentDetailsAction";
import { setPostDetails } from "../actionCreator/PostDetailsAction";
import { listTopicFeed } from "../actionCreator/TopicsFeedAction";
import { getTrendingClicks } from "../actionCreator/TrendingCliksAction";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { getTrendingTopicsProfileDetails } from "../actionCreator/TrendingTopicsProfileAction";
import { saveUserLoginDaitails } from "../actionCreator/UserAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import applloClient from "../client";
import ConstantFontFamily from "../constants/FontFamily";
import {
  UserFollowMutation,
  UserUnfollowMutation
} from "../graphqlSchema/graphqlMutation/FollowandUnFollowMutation";
import { DefaultUserListMutation } from "../graphqlSchema/graphqlMutation/SearchMutation";
import { UserLoginMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import {
  UserFollowVariables,
  UserUnfollowVariables
} from "../graphqlSchema/graphqlVariables/FollowandUnfollowVariables";
import UserStar from "./UserStar";
import { Icon } from "react-native-elements";
import NavigationService from "../library/NavigationService";


class SearchUser extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onEndReachedCalledDuringMomentum = true;
    this.viewabilityConfig = {
      viewAreaCoveragePercentThreshold: 50
    };
    this.state = {
      UserList: this.props.UserList,
      term: this.props.term,
      page: 1,
      loading: true,
      loadingMore: false,
      refreshing: false,
      pageInfo: null,
      error: null,
      apiCall: true
    };
  }

  componentDidMount = () => {
    this.getDefaultUserList();
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.UserList !== this.props.UserList) {
      this.setState({
        UserList: this.props.UserList
      });
    }
    if (prevProps.term !== this.props.term) {
      this.setState({
        term: this.props.term
      });
    }
  };

  getUserStar = UserName => {
    let index = 0;
    index = this.props.getUserFollowUserList.findIndex(
      i =>
        i
          .getIn(["user", "username"])
          .toLowerCase()
          .replace("user:", "") == UserName.toLowerCase().replace("user:", "")
    );
    if (index == -1) {
      return "TRENDING";
    } else {
      return this.props.getUserFollowUserList.getIn([
        index,
        "settings",
        "follow_type"
      ]);
    }
  };
  // goToUserProfile = username => {
  //   this.props.userId({
  //     username: username,
  //     type: "profile"
  //   });
  //   this.props.navigation.navigate("profile", {
  //     username: username,
  //     type: "profile"
  //   });
  // };

  goToProfile = username => {
    this.props.userId({
      username: username,
      type: "feed"
    });
    NavigationService.navigate("profile", {
      username: username,
      type: "feed"
    });
  };

  followUser = userId => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    UserFollowVariables.variables.user_id = userId;
    UserFollowVariables.variables.follow_type = "FOLLOW";
    applloClient
      .query({
        query: UserFollowMutation,
        ...UserFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  unfollowUser = async userId => {
    UserUnfollowVariables.variables.user_id = userId;
    applloClient
      .query({
        query: UserUnfollowMutation,
        ...UserUnfollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  favroiteUser = async userId => {
    UserFollowVariables.variables.user_id = userId;
    UserFollowVariables.variables.follow_type = "FAVORITE";
    applloClient
      .query({
        query: UserFollowMutation,
        ...UserFollowVariables,
        fetchPolicy: "no-cache"
      })
      .then(async res => {
        let resDataLogin = await this.props.Login();
        await this.props.saveLoginUser(resDataLogin.data.login);
      });
  };

  _handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this.getDefaultUserList();
      }
    );
  };

  _handleLoadMore = distanceFromEnd => {
    this.setState(
      (prevState, nextProps) => ({
        //  loadingMore: true
      }),
      () => {
        if (
          0 <= distanceFromEnd &&
          distanceFromEnd <= 5 &&
          this.state.apiCall == true
        ) {
          this.setState({
            apiCall: false
          });
          this.getDefaultUserList();
        }
      }
    );
  };

  _renderFooter = () => {
    if (!this.state.loadingMore) return null;
    return (
      <View>
        <ActivityIndicator animating size="large" color="#000" />
      </View>
    );
  };

  getDefaultUserList = () => {
    let __self = this;
    const { page, pageInfo } = this.state;

    if (pageInfo == null) {
      this.setState({
        loadingMore: true
      });
      applloClient
        .query({
          query: DefaultUserListMutation,
          variables: {
            first: 20,
            after: pageInfo ? pageInfo.endCursor : null
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          __self.setState({
            loading: false,
            UserList: res.data.user_list.edges,
            pageInfo: res.data.user_list.pageInfo,
            page: page + 1,
            apiCall: true,
            loadingMore: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    } else if (pageInfo != null && pageInfo.hasNextPage == true) {
      this.setState({
        loadingMore: true
      });
      applloClient
        .query({
          query: DefaultUserListMutation,
          variables: {
            first: 20,
            after: pageInfo ? pageInfo.endCursor : null
          },
          fetchPolicy: "no-cache"
        })
        .then(res => {
          __self.setState({
            loading: false,
            UserList: __self.state.UserList.concat(res.data.user_list.edges),
            pageInfo: res.data.user_list.pageInfo,
            apiCall: true,
            loadingMore: false
          });
        })
        .catch(e => {
          console.log(e);
        });
    }
  };

  _renderItem = item => {
    var row = item.item.node ? item.item.node : item.item;
    return (
      <View key={item.index}>
        {/* <View
          style={{
            overflow: "hidden",
            marginBottom: 5,
            flexDirection: "row",
            width: "100%"
          }}
        >
          <View
            style={{
              width: "10%"
            }}
          >
            {row.profile_pic ? (
              <Image
                source={{
                  uri: row.profile_pic
                }}
                style={{
                  height: 40,
                  width: 40,
                  padding: 0,
                  margin: 0,
                  borderRadius: 20,
                  borderWidth: 1,
                  borderColor: "#fff"
                }}
              />
            ) : (
              <Image
                source={require("../assets/image/default-image.png")}
                style={{
                  height: 40,
                  width: 40,
                  padding: 0,
                  margin: 0,
                  borderRadius: 20,
                  borderWidth: 1,
                  borderColor: "#fff"
                }}
              />
            )}
          </View>

          <View style={{ width: "30%", alignSelf: "center" }}>
            {row.username ? (
              <Text
                style={{
                  justifyContent: "flex-start",
                  color: "#000",
                  fontSize: 16,
                  fontWeight: "bold",
                  fontFamily: ConstantFontFamily.MontserratBoldFont
                }}
              >
                @ {row.username}
              </Text>
            ) : (
              <Text></Text>
            )}
          </View>

          <View style={{ width: "50%", alignSelf: "center" }}>
            <Text
              style={{
                color: "#000",
                fontSize: 13,
                fontFamily: ConstantFontFamily.defaultFont,
                marginBottom: 5
              }}
            >
              {row.description.length > 50
                ? row.description.substring(0, 50) + "..."
                : row.description}
            </Text>
          </View>

          <UserStar
            UserName={row.username}
            UserId={row.id}
            ContainerStyle={{
              width: "10%",
              alignSelf: "center"
            }}
            ImageStyle={{
              height: 20,
              width: 20,
              alignSelf: "center"
            }}
          />
        </View> */}

        <TouchableOpacity
          style={{
            overflow: "hidden",
            borderRadius: 10,
            borderWidth: 1,
            borderColor: "#c5c5c5",
            backgroundColor: "#fff",
            marginBottom: 5,
            paddingBottom: 10,
            maxHeight: Dimensions.get("window").height / 2
          }}
          onPress={() => this.goToProfile(row.username)}
        >
          <ImageBackground
            style={{
              height: Dimensions.get("window").height / 4
            }}
            source={{
              uri: row.banner_pic ? row.banner_pic : null
            }}
            imageStyle={{ borderRadius: 10 }}
          ></ImageBackground>
          <View
            style={{
              flexDirection: "row",
              flex: 1,
              padding: 5,
              backgroundColor: "#fff",
              width: "100%"
            }}
          >
            <View
              style={{
                flexDirection: "column",
                paddingHorizontal: 5,
                position: "absolute",
                bottom: -1
              }}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "transparent"
                }}
              >
                {row.profile_pic ? (
                  <Image
                    source={{
                      uri: row.profile_pic
                    }}
                    style={{
                      height: 120,
                      width: 120,
                      padding: 0,
                      margin: 0,
                      borderRadius: 60,
                      borderWidth: 1,
                      borderColor: "#fff"
                    }}
                  />
                ) : (
                  <Image
                    source={require("../assets/image/default-image.png")}
                    style={{
                      height: 120,
                      width: 120,
                      padding: 0,
                      margin: 0,
                      borderRadius: 60,
                      borderWidth: 1,
                      borderColor: "#fff"
                    }}
                  />
                )}
              </View>
            </View>
            <View
              style={{
                flexDirection: "column",
                flex: 1,
                paddingLeft: 135
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  width: "100%",
                  paddingRight:20,
                }}
              >
                {row.username ? (
                  <TouchableOpacity
                    style={{
                      flex: 1,
                      justifyContent: "flex-end"
                    }}
                    onPress={() => this.goToProfile(row.username)}
                  >
                    <Text
                      style={{
                        width: "70%",
                        justifyContent: "flex-start",
                        color: "#000",
                        fontSize: 16,
                        fontWeight: "bold",
                        fontFamily: ConstantFontFamily.MontserratBoldFont
                      }}
                    >
                      {row.username}
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <Text
                    style={{
                      width: "70%"
                    }}
                  ></Text>
                )}

                {this.props.loginStatus == 1 && (
                  <UserStar
                    UserName={row.username}
                    UserId={row.id}
                    ContainerStyle={{}}
                    ImageStyle={{
                      height: 20,
                      width: 20,
                      alignSelf: "center",
                      marginLeft: 5,
                      marginBottom: 5
                    }}
                  />
                )}
              </View>
              <View>
                <Text
                  style={{
                    color: "#000",
                    fontSize: 13,
                    fontFamily: ConstantFontFamily.defaultFont,
                    marginBottom: 5
                  }}
                >
                  {row.description}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  onViewableItemsChanged = ({ viewableItems, changed }) => {
    let perLoadDataCount = 20;
    let halfOfLoadDataCount = perLoadDataCount / 2;
    let lastAddArr =
      this.state.UserList.length > 0 &&
      this.state.UserList.slice(-perLoadDataCount);

    try {
      if (
        lastAddArr[halfOfLoadDataCount] &&
        viewableItems.length > 0 &&
        this.props.loginStatus == 1
      ) {
        //FlatList
        if (
          viewableItems.find(
            item =>
              item.item.node.id === lastAddArr[halfOfLoadDataCount].node.id
          )
        ) {
          this._handleLoadMore(0);
        }
      }
    } catch (e) {
      console.log(e, "lastAddArr", lastAddArr[halfOfLoadDataCount]);
    }
  };

  render() {
    return (
      <View style={{ marginTop: 10, marginHorizontal: "auto" }}>
        <FlatList
          extraData={this.state}
          contentContainerStyle={{
            flexDirection: "column",
            height: Dimensions.get("window").height,
            width: "100%"
          }}
          data={this.state.UserList}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItem}
          showsVerticalScrollIndicator={false}
          onRefresh={this._handleRefresh}
          refreshing={this.state.refreshing}
          onEndReached={({ distanceFromEnd }) => {
            //this._handleLoadMore(distanceFromEnd);
          }}
          onEndReachedThreshold={0.2}
          initialNumToRender={10}
          ListFooterComponent={this._renderFooter}
          onViewableItemsChanged={this.onViewableItemsChanged}
          viewabilityConfig={this.viewabilityConfig}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  listTrending_cliks: !state.TrendingCliksReducer.getIn(["Trending_cliks_List"])
    ? List()
    : state.TrendingCliksReducer.getIn(["Trending_cliks_List"]),
  link: state.LinkPostReducer.get("link"),
  getHasScrollTop: state.HasScrolledReducer.get("hasScrollTop"),
  loginStatus: state.UserReducer.get("loginStatus"),
  getUserFollowTopicList: state.LoginUserDetailsReducer.get(
    "userFollowTopicsList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowTopicsList")
    : List(),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List(),
  getUserFollowUserList: state.LoginUserDetailsReducer.get("userFollowUserList")
    ? state.LoginUserDetailsReducer.get("userFollowUserList")
    : List(),
  getUserFollowFeedList: state.LoginUserDetailsReducer.get("userFollowFeedList")
    ? state.LoginUserDetailsReducer.get("userFollowFeedList")
    : List(),
  getCurrentDeviceWidthAction: state.CurrentDeviceWidthReducer.get("dimension")
});

const mapDispatchToProps = dispatch => ({
  getHomefeed: payload => dispatch(getHomefeedList(payload)),
  getTrendingClicks: payload => dispatch(getTrendingClicks(payload)),
  setPostDetails: payload => dispatch(setPostDetails(payload)),
  setHASSCROLLEDACTION: payload => dispatch(setHASSCROLLEDACTION(payload)),
  topicId: payload => dispatch(getTrendingTopicsProfileDetails(payload)),
  listTopicFeed: payload => dispatch(listTopicFeed(payload)),
  ClikId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  setClikUserRequest: payload => dispatch(listClikUserRequest(payload)),
  setClikMembers: payload => dispatch(listClikMembers(payload)),
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  saveLoginUser: payload => dispatch(saveUserLoginDaitails(payload)),
  setPostCommentDetails: payload => dispatch(setPostCommentDetails(payload)),
  setFeedDetails: payload => dispatch(getFeedProfileDetails(payload))
});

const SearchUserWrapper = graphql(UserLoginMutation, {
  name: "Login",
  options: { fetchPolicy: "no-cache" }
})(SearchUser);

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  SearchUserWrapper
);
