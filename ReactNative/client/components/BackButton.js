import React from 'react';
import { View } from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from "react-redux";
import { compose } from 'recompose';
import HeaderIcon from './HeaderIcon';

const BackButton = (props) => {
	return (
		<View style={{
			flexDirection:"row",
			justifyContent: 'space-between',
		}}>
			<Icon
				color={'#fff'}
				iconStyle={{ paddingLeft: 10,  marginRight: 10  }}
				onPress={() => props.navigation.goBack()}
				name="angle-left"
				type='font-awesome'
				size={40}
			/>
			<HeaderIcon />
		</View>
	)
}


const mapStateToProps = state => ({
	loginStatus: state.UserReducer.get('loginStatus'),
});

export default compose(connect(mapStateToProps, null))(BackButton);