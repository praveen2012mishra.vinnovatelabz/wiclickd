import "@expo/browser-polyfill";
import { List } from "immutable";
import React, { Component } from "react";
import { TouchableOpacity, View, Text,AsyncStorage } from "react-native";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setFEEDREPORTMODALACTION } from "../actionCreator/FeedReportModalAction";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setSIGNUPMODALACTION } from "../actionCreator/SignUpModalAction";
import { getTrendingCliksProfileDetails } from "../actionCreator/TrendingCliksProfileAction";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";
import AppHelper from "../constants/AppHelper";
import ChildCommentSwaper from "./ChildCommentSwaper";
import { Hoverable } from "react-native-web-hooks";
import ConstantFontFamily from "../constants/FontFamily";
import { listClikUserRequest } from "../actionCreator/ClikUserRequestAction";
import { listClikMembers } from "../actionCreator/ClikMembersAction";
import { Icon } from "react-native-elements";
import { Dimensions } from "react-native";
import ButtonStyle from "../constants/ButtonStyle";
import {Button} from 'react-native-elements'

class PostDetailsComment extends Component {
  state = {
    modalVisible: false,
    outSideClick: false,
    lockStatus: true,
    setcalHeight: [],
    parent_content_id: "",
    setindex: 0,
    showBug: false,
    pressBug: false,
    showNestedComment: false,
    nameWidth: 0,
    editModalVisible: false,
    blockUi: false,
    listCommentList: [],
    listSubCommentList: [],
    getSubCommentIndex: 3,
    getCommentIndex: 3,
    listSubCommentListLoading: false,
    listCommentListLoading: false,
    activeIndex: 0,
    activeComment: -1,
    commentID: [],
    item:[],
    cartIndex:null,
    showMoreClickNumber:1
  };

  constructor(props) {
    super(props);
  }

  onClose = () => {
    this.setState({
      modalVisible: false,
      outSideClick: true,
      editModalVisible: false,
      blockUi: !this.state.blockUi
    });
  };

  onSubmit = (position, data) => {
    this.setState({
      modalVisible: false
    });
    // this.props.closeModalhandalListMode(position, data);
  };

  calHeightModi = async (height, key) => {
    let data = [];
    data = this.state.setcalHeight;
    data[key] = height;
    this.setState({
      setcalHeight: data
    });
  };

  onOpen = id => {
    if (this.props.loginStatus == 0) {
      this.props.setLoginModalStatus(true);
      return false;
    }
    let changeData = this.state.modalVisible ? false : true;
    this.setState({
      modalVisible: changeData,
      parent_content_id: id,
      blockUi: !this.state.blockUi
    });
  };

  editCommentField = async () => {
    this.setState({
      editModalVisible: !this.state.editModalVisible,
      blockUi: !this.state.blockUi
    });
  };

  getStatus = clik => {
    let index = 0;
    if (clik != null) {
      index = this.props.getUserFollowCliksList.findIndex(
        i =>
          i
            .getIn(["clik", "name"])
            .toLowerCase()
            .replace("clik:", "") == clik.toLowerCase().replace("clik:", "")
      );
    }
    return index;
  };

  checkLockStatus = async clik => {
    if (this.props.loginStatus == 1) {
      if (clik) {
        if (this.props.profileData) {
          if (
            this.props.profileData.getIn(["my_users", "0", "cliks_followed"])
              .size > 0
          ) {
            this.props.profileData
              .getIn(["my_users", "0", "cliks_followed"])
              .map(async (item, index) => {
                if (
                  item.getIn(["clik", "name"]).toLowerCase() ==
                  clik.toLowerCase()
                ) {
                  if (item.getIn(["member_type"]) == "SUPER_ADMIN") {
                    this.setState({
                      lockStatus: false
                    });
                  }
                }
              });
          }
        }
      } else {
        this.setState({
          lockStatus: false
        });
      }
    } else {
      this.setState({
        lockStatus: true
      });
    }
  };

  loginModalStatusEvent = async status => {
    this.setState({
      modalVisible: status
    });
  };

  goToUserProfile = async username => {
    this.props.userId({
      username: username
    });
    this.props.navigation.navigate("profile", {
      username: username,
      type: "profile"
    });
  };

  goToProfile = id => {
    this.props.clikId({
      id: id,
      type: "feed"
    });
    this.props.setClikUserRequest({
      id: id,
      currentPage: AppHelper.PAGE_LIMIT
    });
    this.props.setClikMembers({
      id: id
    });
  };

  handleClick = e => {
    this.setState({
      showBug: e
    });
  };

  handleBugClick = e => {
    this.props.setFeedReportModalAction(true);
    this.setState({
      pressBug: true
    });
  };

  onLayout = async event => {
    const { width, height } = event.nativeEvent.layout;
    if (width > 0) {
      await this.setState({
        nameWidth: width
      });
    }
  };

  setIndex = e => {
    console.log(e);
    this.setState({
      activeIndex: e
    });
  };
  getNestedCommnets = node => {
    if (node.comments.edges.length == 0) return null;
    return (
      <View>
        <ChildCommentSwaper
          commentData={node.comments.edges.length > 0 && node.comments.edges}
          currentIndex={this.state.setindex}
          closeModalBySubmit={this.onSubmit}
          navigation={this.props.navigation}
          activeIndex={this.setIndex}
          stopScrolling = {this.stopScrolling}
        />
        {this.getNestedCommnets(node.comments.edges[this.state.activeIndex])}
      </View>
    );
  };

  // componentDidMount = async () => {
  //   let data = document.getElementById("PostDetails");
  //   if (data != null) {
  //     await data.addEventListener("keydown", event => {
  //       if (event.keyCode == 83 || event.keyCode == 40) {
  //         this.setState({
  //           activeComment: this.state.activeComment + 1
  //         });
  //       }
  //       if (event.keyCode == 87 || event.keyCode == 38) {
  //         this.setState({
  //           activeComment: this.state.activeComment - 1
  //         });
  //       }
  //       if (this.state.activeComment < this.props.item.length) {
  //         this.setBorderColor();
  //       }
  //     });
  //   }

  //   // w=87,s=83,a=65,d=68
  // };

  // setBorderColor = async () => {
  //   for (let i = 0; i < this.props.item.length; i++) {
  //     document.getElementById("PostComment" + i).style.borderColor =
  //       i == this.state.activeComment ? "red" : "#e1e1e1";
  //   }
  // };
  // componentWillUnmount = () => {
  //   let data = document.getElementById("PostDetails");
  //   data.removeEventListener("keydown", this.componentDidMount());
  // };

  componentDidMount(){
    //this.setState({item:this.props.item})
  }

  componentDidUpdate=(prevState)=>{
    if(prevState.item.length !=this.state.item.length){
      if(this.props.item.length>0 && !this.props.getNotificationId){
        let newArray=Object.assign([],this.props.item)
        this.setState({item:newArray})
      }
      if(this.props.item.length>0 && this.props.getNotificationId){
        let newArray=Object.assign([],this.props.item) 
        let newEle;     
        this.props.item.forEach((ele,index)=>{        
          if(ele.node.id==this.props.getNotificationId){
            newEle=ele;
            newArray.splice(index,1);
            newArray.splice(0,0,newEle);
          }
        })
        this.setState({item:newArray})
      }
    }
    // if(prevState.cartIndex ==this.state.cartIndex ){
    //   this.setState({showMoreClickNumber:this.state.showMoreClickNumber+1})
    // }
  }
  stopScrolling = (data) =>{
    this.props.stopScrolling(data)
  }

  render() {
    let commentItem =this.props.right==true ? this.props.item:this.state.item;
    return (
      <View nativeID="PostDetails" style={{zIndex:-1}}>
        {this.props.loginStatus == 0 
          ?  commentItem.map((item, index) => {
            if (index == 0) {
              return (
                <View key={index}>
                  {/* {[item][0].node.clik && (
                      <TouchableOpacity
                        onPress={() => this.goToProfile([item][0].node.clik)}
                        style={{
                          marginTop: 5,
                          //marginLeft: 5,
                          //marginBottom: 5,
                          // height: 30,
                          alignSelf: "center",
                          padding: 5,
                          backgroundColor: "#E8F5FA",
                          borderRadius: 6
                        }}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                width: "100%",
                                color: "#4169e1",
                                fontSize: 15,
                                fontWeight: "bold",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                textDecorationLine:
                                  isHovered == true ? "underline" : "none"
                              }}
                            >
                              {" "}
                              #{[item][0].node.clik}{" "}
                            </Text>
                          )}
                        </Hoverable>
                      </TouchableOpacity>
                    )} */}
                  <View
                    nativeID={"PostComment" + index}
                    key={index}
                    style={{
                      marginBottom: 10,
                      borderRadius: 20,
                      borderWidth: 1,
                      borderColor: [item][0].node.clik
                        ? "#4C82B6"
                        : "#c5c5c5",
                      backgroundColor: [item][0].node.clik
                        ? "#E8F5FA"
                        : "#fff",
                      paddingVertical: 5
                    }}
                  >
                    {[item][0].node.clik && (
                      <TouchableOpacity
                        onPress={() => this.goToProfile([item][0].node.clik)}
                        style={{
                          marginTop: 5,
                          //marginLeft: 5,
                          //marginBottom: 5,
                          // height: 30,
                          alignSelf: "center",
                          padding: 5,
                          backgroundColor: "#E8F5FA",
                          borderRadius: 6
                        }}
                      >
                        <Hoverable>
                          {isHovered => (
                            <Text
                              style={{
                                width: "100%",
                                color: "#4169e1",
                                fontSize: 15,
                                fontWeight: "bold",
                                fontFamily:
                                  ConstantFontFamily.MontserratBoldFont,
                                textDecorationLine:
                                  isHovered == true ? "underline" : "none"
                              }}
                            >
                              {" "}
                                #{[item][0].node.clik}{" "}
                            </Text>
                          )}
                        </Hoverable>
                      </TouchableOpacity>
                    )}

                    <ChildCommentSwaper
                      commentData={[item]}
                      currentIndex={this.state.setindex}
                      closeModalBySubmit={this.onSubmit}
                      navigation={this.props.navigation}
                      isFirstComment={true}
                      stopScrolling = {this.stopScrolling}
                    />
                  </View>

                  <View
                    style={{
                      flex: 1,
                      alignContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Icon
                      color={"#000"}
                      iconStyle={{
                        justifyContent: "center",
                        alignItems: "center",
                        alignSelf: "center"
                      }}
                      name="arrow-down"
                      type="font-awesome"
                      size={20}
                      containerStyle={{
                        alignSelf: "center"
                      }}
                    />
                    <Hoverable>
                      {isHovered => (
                        <Text
                          style={{
                            color: isHovered == true ? "#009B1A" : "#000",
                            fontSize: 12,
                            textAlign: "center",
                            fontFamily: ConstantFontFamily.MontserratBoldFont
                          }}
                        >
                          <Text
                            onPress={() =>
                              this.props.setLoginModalStatus(true)
                            }
                            style={{
                              textDecorationLine: "underline",
                              fontSize: 12,
                              textAlign: "center",
                              fontFamily:
                                ConstantFontFamily.MontserratBoldFont
                            }}
                          >
                            Login
                            </Text>{" "}
                            to see more discussions...
                        </Text>
                      )}
                    </Hoverable>
                  </View>
                </View>
              );
            }
          })
          : commentItem.map((item, index) => {
            return (
              <View key={index}>    
                {/* {[item][0].node.clik && (
                    <TouchableOpacity
                      onPress={() => this.goToProfile([item][0].node.clik)}
                      style={{
                        marginTop: 5,
                        //marginLeft: 5,
                        // marginBottom: 5,
                        //height: 30,
                        alignSelf: "center",
                        padding: 5,
                        backgroundColor: "#E8F5FA",
                        borderRadius: 6
                      }}
                    >
                      <Hoverable>
                        {isHovered => (
                          <Text
                            style={{
                              width: "100%",
                              color: "#4169e1",
                              fontSize: 15,
                              fontWeight: "bold",
                              fontFamily: ConstantFontFamily.MontserratBoldFont,
                              textDecorationLine:
                                isHovered == true ? "underline" : "none"
                            }}
                          >
                            {" "}
                            #{[item][0].node.clik}{" "}
                          </Text>
                        )}
                      </Hoverable>
                    </TouchableOpacity>
                  )} */}
                <View
                  nativeID={"PostComment" + index}
                  key={index}
                  style={[ButtonStyle.shadowStyle,{
                    // marginTop: 10,
                    marginBottom: 10,
                    borderRadius: 20,
                    // borderWidth: 1,
                    // marginLeft:  Dimensions.get('window').width>=750  && 3,                    
                    // Dimensions.get('window').width<=750 ? 0 : 1,
                    borderColor: [item][0].node.clik ? "#4C82B6" : "#c5c5c5",
                    backgroundColor: [item][0].node.clik ? "#E8F5FA" : "#fff",
                    paddingVertical: 5,
                    // width:'99%',
                    marginLeft: 4
                    // marginLeft: Dimensions.get('window').width<=750 ? 5 : 0,
                  }]}
                >
                  {[item][0].node.clik && (
                    <TouchableOpacity
                      onPress={() => this.goToProfile([item][0].node.clik)}
                      style={{
                        // marginTop: 5,
                        //marginLeft: 5,
                        // marginBottom: 5,
                        //height: 30,
                        alignSelf: "center",
                        padding: 5,
                        backgroundColor: "#E8F5FA",
                        borderRadius: 6
                      }}
                    >
                      <Hoverable>
                        {isHovered => (
                          <Text
                            style={{
                              width: "100%",
                              color: "#4169e1",
                              fontSize: 15,
                              fontWeight: "bold",
                              fontFamily:
                                ConstantFontFamily.MontserratBoldFont,
                              textDecorationLine:
                                isHovered == true ? "underline" : "none"
                            }}
                          >
                            {" "}
                              #{[item][0].node.clik}{" "}
                          </Text>
                        )}
                      </Hoverable>
                    </TouchableOpacity>
                  )}

                  {/* //* showmore */}
                  
                  <ChildCommentSwaper
                    commentData={[item]}
                    currentIndex={this.state.setindex}
                    closeModalBySubmit={this.onSubmit}
                    navigation={this.props.navigation}
                    isFirstComment={true}
                    gestureStatus={this.props.gestureStatus}
                    stopScrolling = {this.stopScrolling}
                    index={this.state.cartIndex}
                    showMoreClickNumber={this.state.showMoreClickNumber}
                    parentCartId={item.node.id}
                  />
                 <Button
                    title="Show More"
                    buttonStyle={{
                      backgroundColor:"#fff",
                      // borderColor: "#fff",
                      width:200,
                      borderWidth: 0,
                      height: 30,
                      borderTopRightRadius: 6,
                      borderBottomRightRadius: 6,
                      alignSelf:'center'
                    }}
                    titleStyle={{
                      fontFamily: ConstantFontFamily.MontserratBoldFont,
                      paddingHorizontal: 10,
                      fontSize: 14,
                      color:  "#000"
                    }}
                     onPress={()=>{
                      console.log([item],'==============================================')
                      //console.log(index,'=========================',this.state.cartIndex);
                      //this.setState({cartIndex:index})
                      if(this.state.cartIndex==index){
                        this.setState({showMoreClickNumber:this.state.showMoreClickNumber+1,cartIndex:index})
                      }
                      else{
                        this.setState({showMoreClickNumber:1,cartIndex:index})
                      }
                     }}
                     //onPress={()=> this.props.updateComments(3)}
                    >
                  </Button>


                </View>
                {/* <View
                    style={{
                      height: 1,
                      backgroundColor: "#e1e1e1"
                    }}
                  ></View> */}
              </View>
            );
          })}
      </View>
    );
  }
}
const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getUserFollowCliksList: state.LoginUserDetailsReducer.get(
    "userFollowCliksList"
  )
    ? state.LoginUserDetailsReducer.get("userFollowCliksList")
    : List(),
    getNotificationId: state.PostDetailsReducer.get("getNotificationId"),
});

const mapDispatchToProps = dispatch => ({
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  setSignUpModalStatus: payload => dispatch(setSIGNUPMODALACTION(payload)),
  setFeedReportModalAction: payload =>
    dispatch(setFEEDREPORTMODALACTION(payload)),
  clikId: payload => dispatch(getTrendingCliksProfileDetails(payload)),
  setClikUserRequest: payload => dispatch(listClikUserRequest(payload)),
  setClikMembers: payload => dispatch(listClikMembers(payload)),
  setNotificationId: payload => dispatch({ type: "GET_NOTIFICATION_ID", payload }),
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(
  PostDetailsComment
);
