import React, { Component } from "react";
import {
  Dimensions,
  Image,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Icon } from "react-native-elements";
import { Hoverable } from "react-native-web-hooks";
import { connect } from "react-redux";
import { compose } from "recompose";
import { setLOGINMODALACTION } from "../actionCreator/LoginModalAction";
import { setUSERNAMEMODALACTION } from "../actionCreator/UsernameModalAction";
import ConstantFontFamily from "../constants/FontFamily";
import ConstantColors from '../constants/Colors';
import NavigationService from "../library/NavigationService";
import DrawerScreen from "./DrawerScreens";
import OurMission from "./OurMission";
import TrendingTabs from "./TrendingTabs";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import ButtonStyle from "../constants/ButtonStyle";
import HeaderRight from "./HeaderRight";
import { Button } from "react-native-elements";
import { getCurrentUserProfileDetails } from "../actionCreator/UserProfileDetailsAction";

class LeftPanel extends Component {
  totalHeigth = 0;
  constructor(props) {
    super(props);
    this.state = {
      getHeight: 100,
      randomItemEvent: 0,
      primaryIndex: 0,
      isSearched: false,
      searchedWord: "",
      isActive: false,
      searchedFollowText: ""
    };
  }

  calHeightLeftPannelMian = height => {
    if (height > 0 && height != null) {
      this.totalHeigth = height;
    }
  };

  activeIndexAction = index => {
    this.setState({
      primaryIndex: index,
      searchedWord: ""
    });
  };

  activeTrendingIndexAction = index => {
    this.setState({
      searchedFollowText: ""
    });
  };

  changeColor = isHovered => {
    if (isHovered == true) {
      switch (this.state.primaryIndex) {
        case 0:
          return "#009B1A";
        case 1:
          return "#4C82B6";
        case 2:
          return "#FEC236";
        default:
          return "#F34225";
      }
    }
  };

  getType = () => {
    switch (this.state.primaryIndex) {
      case 0:
        return "TOPICS";
      case 1:
        return "CLIKS";
      case 2:
        return "USERS";
      default:
        return "FEEDS";
    }
  };

  // click on the search icon
  searchClicked = () => {
    this.setState({ isSearched: true });
  };

  searchParaChanged = () => {
    this.setState({ searchedWord: "" });
  };
  // type on the search field

  searchTextField = value => {
    this.setState({ searchedWord: value });
  };

  componentDidMount = async () => {
    var newWidth = window.innerWidth;
    var newHeight = window.innerHeight;
    this.props.setBrowserHeightWidth({ heigh: newHeight, width: newWidth });
    window.addEventListener("resize", event => {
      var newWidth = window.innerWidth;
      var newHeight = window.innerHeight;
      this.props.setBrowserHeightWidth({ heigh: newHeight, width: newWidth });
    });
  };

  loginHandle = () => {
    this.props.leftPanelModalFunc(false)
    this.props.setLoginModalStatus(true);
  };

  inviteSignHandle = async () => {
    await this.props.leftPanelModalFunc(false)
    await this.props.setInviteUserDetail({
      clikName: "",
      inviteKey: "",
      userName: ""
    });
    await this.props.setUsernameModalStatus(true);

    // this.props.navigation.navigate("username");
  };

  gotoprofile = () => {
    this.props.userId({
      username:
        this.props.profileData &&
        this.props.profileData
          .getIn(["my_users"])
          .getIn(["0", "user", "username"]),
      type: "feed"
    });
    NavigationService.navigate("profile", {
      username:
        this.props.profileData &&
        this.props.profileData
          .getIn(["my_users"])
          .getIn(["0", "user", "username"]),
      type: "feed"
    });
    window.location.reload(true)
  };

  goToUserProfile = async username => {
    //props.onClose();
    //console.log(username);
    await this.props.userId({
      username: username,
      type: "feed"
    });
    await NavigationService.navigate("profile", {
      username: username,
      type: "feed"
    });
    this.props.leftPanelModalFunc(false)
    //window.location.reload(true)
  };

  render() {
    return (
      <View
        style={{
          flexDirection: "column",
          height: "100%",
          width: Dimensions.get("window").width <= 750 ? '100%' :
          Dimensions.get("window").width >= 750 && Dimensions.get("window").width <= 1200 ? 450 : 320,
          marginHorizontal: Dimensions.get("window").width <= 750 ? '0%' : 0,
          backgroundColor: ConstantColors.customeBackgroundColor,
          alignSelf:'center'
        }}
      >
        { Dimensions.get("window").width <= 750 &&
          <View style={{ flexDirection: 'row', backgroundColor: '#f8f8f8', height: 30, marginTop: 10, justifyContent: 'space-between' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
              <Image
                source={require("../assets/image/logo.png")}
                style={{
                  height: 30,
                  width: 30,
                  justifyContent: "flex-start",
                  marginLeft: 10,
                  marginRight: 5
                }}
                resizeMode={"contain"}
              />
              <Text
                style={{
                  fontFamily: ConstantFontFamily.MontserratBoldFont,
                  fontWeight: "bold",
                  fontSize: 20,
                  textAlign: "center",
                  color: "#000"
                }}
              >
                weclikd
              </Text>
            </View>
            <View
              style={{
                justifyContent: "center"
              }}
            >
              {/* <ion-icon name="close-outline"></ion-icon> */}
              <Icon
                color={"#000"}
                iconStyle={{ paddingLeft: 15, fontWeight: 400, marginRight: 12 }}
                onPress={() => this.props.leftPanelModalFunc(false)}
                name="close"
                //name="close-outline"
                type="ion-icon"
                size={35}
              />
            </View>
          </View>
        }

        {/* {Dimensions.get("window").width >= 1100 && ( */}
        {this.props.loginStatus == 0 && Dimensions.get("window").width <= 750 && (<View
          style={[
            // Dimensions.get("window").width <= 750 ? null : ButtonStyle.borderStyle,
            {
              height: Dimensions.get("window").width <= 750 ? hp("15%") : hp("22%"),
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "#fff",
              marginTop: 10,
              paddingVertical: 30,
              paddingHorizontal: 20,
              backgroundColor: ConstantColors.customeBackgroundColor
            }
          ]}
        >
          <OurMission />
        </View>)}

        {Dimensions.get("window").width >= 750 && (<View
          style={[
            // Dimensions.get("window").width <= 750 ? null : ButtonStyle.borderStyle,
            {
              height: Dimensions.get("window").width <= 750 ? hp("15%") : hp("18%"),
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "#fff",
              marginTop: 10,
              paddingVertical: 30,
              paddingHorizontal: 20,
              // backgroundColor: ConstantColors.customeBackgroundColor
            }
          ]}
        >
          <OurMission />
        </View>)}

        {this.props.loginStatus == 1 && Dimensions.get("window").width <= 1100 && (
          <Hoverable>
            {isHovered => (
              <TouchableOpacity
                onPress={() =>
                  this.goToUserProfile(
                    this.props.profileData &&
                    this.props.profileData.getIn([
                      "my_users",
                      "0",
                      "user",
                      "username"
                    ])
                  )
                }
                // onPress={() => this.gotoprofile()}
              >
                <View
                  style={[
                    // Dimensions.get("window").width <= 750 ? null : ButtonStyle.borderStyle,
                    {
                      height: hp("15%"),
                      alignItems: "center",
                      justifyContent: "center",
                      backgroundColor: "#fff",
                      marginTop: 10,
                      paddingVertical: 30,
                      paddingHorizontal: 20,
                      backgroundColor: ConstantColors.customeBackgroundColor
                    }
                  ]}
                // onPress={()=>this.gotoprofile()}

                >
                  {this.props.profileData &&
                    this.props.profileData.getIn([
                      "my_users",
                      "0",
                      "user",
                      "profile_pic"
                    ]) ? (
                      <Image
                        source={{
                          uri: this.props.profileData.getIn([
                            "my_users",
                            "0",
                            "user",
                            "profile_pic"
                          ])
                        }}
                        style={{
                          width: 70,
                          height: 70,
                          borderRadius: 35,
                          borderWidth: 1,
                          borderColor: "#e1e1e1",
                          marginRight: 5
                        }}
                      />
                    ) : (
                      <Image
                        source={require("../assets/image/default-image.png")}
                        style={{
                          width: 70,
                          height: 70,
                          borderRadius: 35,
                          borderWidth: 1,
                          borderColor: "#e1e1e1",
                          marginRight: 5
                        }}
                      />
                    )}

                  <Text
                    style={{
                      color: "#000",
                      fontSize: 22,
                      fontFamily: ConstantFontFamily.VerdanaFont,
                      textDecorationLine:
                        isHovered == true ? "underline" : "none",
                      position: "relative",
                      top: 8,
                      fontWeight: 'bold',
                      marginRight: 5
                    }}
                  >
                    {this.props.profileData &&
                      "@" +
                      this.props.profileData.getIn([
                        "my_users",
                        "0",
                        "user",
                        "username"
                      ])}
                  </Text>
                </View>

              </TouchableOpacity>
            )}
          </Hoverable>
        )}

        {this.props.loginStatus != 1 && Dimensions.get("window").width <= 750 &&
          <View style={{ alignItems: 'center' }}>
            <View>
              {this.props.loginStatus == 0 && (
                <View
                  style={{
                    // flex: 1,
                    flexDirection: "row",
                    // marginRight: 10
                  }}
                >

                  <Button
                    title="Log In"
                    titleStyle={{
                      fontSize: 14,
                      fontFamily: ConstantFontFamily.VerdanaFont,
                      //fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#fff",
                    }}
                    buttonStyle={{
                      backgroundColor: "#000",
                      borderColor: Dimensions.get('window').width <= 750 ? "#f4f4f4" : "#fff",
                      borderRadius: 6,
                      borderWidth: 1,
                      alignSelf: "center",
                      width: 120,
                      height: 30,
                      paddingHorizontal: 20,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                    containerStyle={{
                      alignSelf: "center",
                      marginRight: 10,
                    }}
                    testID="welcome"
                    onPress={this.loginHandle}
                  />

                  <Button
                    title="Sign Up"
                    titleStyle={{
                      fontSize: 14,
                      fontFamily: ConstantFontFamily.VerdanaFont,
                      //fontFamily: ConstantFontFamily.MontserratBoldFont,
                      color: "#000",
                    }}
                    buttonStyle={{
                      backgroundColor: "#fff",
                      borderColor: Dimensions.get('window').width <= 750 ? "#000" : "#fff",
                      borderRadius: 6,
                      borderWidth: 1,
                      alignSelf: "center",
                      width: 120,
                      height: 30,
                    }}
                    containerStyle={{
                      alignSelf: "center",

                    }}
                    onPress={() => this.inviteSignHandle()}
                  />
                  
                </View>
              )}
            </View>
          </View>}
        {/* )} */}

        <View
          style={[
            // ButtonStyle.borderStyle,
            {
              paddingHorizontal: 10,
              flex: 1,
              height: hp("50%"),
              // borderWidth: Dimensions.get("window").width >= 1100 ? 1 : 0,
              borderRadius: Dimensions.get("window").width >= 1100 ? 20 : 0,
              backgroundColor: ConstantColors.customeBackgroundColor,
              marginTop: 30
            }
          ]}
        >
          {this.props.loginStatus == 1 ? (
            <DrawerScreen
              calHeightLeftPannelSend={this.calHeightLeftPannelMian}
              navigation={this.props.navigation}
              activeIndex={this.activeIndexAction}
              searchParam={this.searchParaChanged}
              searchedWord={this.state.searchedWord}
            />
          ) : (
            
              <TrendingTabs
                navigation={this.props.navigation}
                randomItemEvent={this.state.randomItemEvent}
                searchedFollowText={this.state.searchedWord}
                activeIndex={this.activeTrendingIndexAction}
                onchangetext={query => this.searchTextField(query)}
                onpress={() => this.setState({ searchedFollowText: "" })}
              />
            )}
        </View>
        <View
          style={{
            marginBottom: 10,
            marginTop: this.props.loginStatus == 0 ? 10 : 0,
            alignSelf: "flex-end",
            alignItems: "flex-end",
            width: "100%",
            flexDirection: "row",
            height: hp("2%"),
            justifyContent: "center"
          }}
        >
          <Hoverable>
            {isHovered => (
              <TouchableOpacity
                onPress={() => {
                  NavigationService.navigate("termsandconditions");
                }}
              >
                <Text
                  style={{
                    fontSize: 10,
                    color: "#706969",
                    textAlign: "center",
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    marginTop: 10,
                    textDecorationLine: isHovered == true ? "underline" : "none"
                  }}
                >
                  Terms of Service
                </Text>
              </TouchableOpacity>
            )}
          </Hoverable>

          <Hoverable>
            {isHovered => (
              <TouchableOpacity
                style={{
                  width: "30%"
                }}
                onPress={() => {
                  NavigationService.navigate("faq");
                }}
              >
                <Text
                  style={{
                    fontSize: 10,
                    color: "#706969",
                    textAlign: "center",
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    marginTop: 10,
                    textDecorationLine: isHovered == true ? "underline" : "none"
                  }}
                >
                  FAQ
                </Text>
              </TouchableOpacity>
            )}
          </Hoverable>

          <Hoverable>
            {isHovered => (
              <TouchableOpacity
                style={{
                  width: "30%"
                }}
                onPress={() => {
                  NavigationService.navigate("privacyPolicy");
                }}
              >
                <Text
                  style={{
                    fontSize: 10,
                    color: "#706969",
                    textAlign: "center",
                    fontFamily: ConstantFontFamily.MontserratBoldFont,
                    marginTop: 10,
                    textDecorationLine: isHovered == true ? "underline" : "none"
                  }}
                >
                  Privacy Policy
                </Text>
              </TouchableOpacity>
            )}
          </Hoverable>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.UserReducer.get("loginStatus"),
  profileData: state.LoginUserDetailsReducer.get("userLoginDetails"),
  getBrowserHeight: state.HasScrolledReducer.get("setBrowserHeightWidth")
});

const mapDispatchToProps = dispatch => ({
  userId: payload => dispatch(getCurrentUserProfileDetails(payload)),
  setLoginModalStatus: payload => dispatch(setLOGINMODALACTION(payload)),
  setBrowserHeightWidth: payload =>
    dispatch({ type: "SET_BROWSER_HEIGHT_WIDTH", payload }),
  leftPanelModalFunc: payload => dispatch({ type: 'LEFT_PANEL_OPEN', payload }),
  setInviteUserDetail: payload =>
    dispatch({ type: "SET_INVITE_USER_DETAIL", payload }),
  setUsernameModalStatus: payload => dispatch(setUSERNAMEMODALACTION(payload)),
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(LeftPanel);
