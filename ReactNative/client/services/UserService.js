import React from 'react';
import getEnvVars from '../environment';
import { dataURItoBlob } from '../library/Helper';
import { Platform } from 'react-native';


const apiUrlMain = getEnvVars();

export const uploadBannerImageAsync = async (uri) => {
    let apiUrl = apiUrlMain.API_URL + 'v1/media/banners';
    let formData = new FormData();
    let headers = {};
    if (Platform.OS != 'web') {
        let fileType = uri[uri.length - 1];
        formData.append('file', {
            uri,
            name: `photo.${fileType}`,
            type: `image/jpeg`,
        });
        headers = {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        };
    } else {
        let blob = await dataURItoBlob(uri);
        formData.append("file", blob, new Date() + ".jpeg");
    }
    let options = {
        method: 'POST',
        body: formData,
        headers: { ...headers }
    };
    return fetch(apiUrl, options);
};

export const uploadProfileImageAsync = async (uri) => {
    let apiUrl = apiUrlMain.API_URL + 'v1/media/profiles';
    let formData = new FormData();
    let headers = {};
    if (Platform.OS != 'web') {
        let fileType = uri[uri.length - 1];
        formData.append('file', {
            uri,
            name: `photo.${fileType}`,
            type: `image/jpeg`,
        });
        headers = {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        };
    } else {
        let blob = await dataURItoBlob(uri);
        formData.append("file", blob, new Date() + ".jpeg");
    }
    let options = {
        method: 'POST',
        body: formData,
        headers: { ...headers }
    };
    return fetch(apiUrl, options);
};



export const uploadPostImageAsync = async (uri) => {
    let apiUrl = apiUrlMain.API_URL + 'v1/media/contents';
    let formData = new FormData();
    let headers = {};
    if (Platform.OS != 'web') {
        let fileType = uri[uri.length - 1];
        formData.append('file', {
            uri,
            name: `photo.${fileType}`,
            type: `image/jpeg`,
        });
        headers = {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        };
    } else {
        let blob = await dataURItoBlob(uri);
        formData.append("file", blob, new Date() + ".jpeg");
    }
    let options = {
        method: 'POST',
        body: formData,
        headers: { ...headers }
    };
    return fetch(apiUrl, options);
};