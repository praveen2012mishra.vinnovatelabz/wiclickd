import { fromJS } from 'immutable';
import {
    EDITTOPIC_SUCCESS,
    EDITTOPIC_FAILURE,
} from '../constants/Action';


const initialState = fromJS({
    topic: "",
});

export default function EditTopicReducer(state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
        case EDITTOPIC_SUCCESS:
            return state
                .set('topic', fromJS(payload));
        case EDITTOPIC_FAILURE:
            return initialState;
    }
    return state;
}