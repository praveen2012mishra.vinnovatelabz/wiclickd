import { fromJS } from 'immutable';
import {
    LOGINMODALACTION_SUCCESS, LOGINMODALACTION_FAILURE
} from '../constants/Action';


const initialState = fromJS({
    modalStatus: false,
});

export default function LoginModalReducer(state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
        case LOGINMODALACTION_SUCCESS:
            return state
                .set('modalStatus', fromJS(payload));
        case LOGINMODALACTION_FAILURE:
            return initialState;
    }
    return state;
}
