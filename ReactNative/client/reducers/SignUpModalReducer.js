import { fromJS } from 'immutable';
import {
    SIGNUPMODALACTION_SUCCESS, SIGNUPMODALACTION_FAILURE
} from '../constants/Action';


const initialState = fromJS({
    modalStatus: false,
});

export default function SignUpModalReducer(state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
        case SIGNUPMODALACTION_SUCCESS:
            return state
                .set('modalStatus', fromJS(payload));
        case SIGNUPMODALACTION_FAILURE:
            return initialState;
    }
    return state;
}
