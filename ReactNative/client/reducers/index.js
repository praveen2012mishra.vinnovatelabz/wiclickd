import { combineReducers } from "redux";
import CliksFeedReducer from "./CliksFeedReducer";
import CreateAccountReducer from "./CreateAccountReducer";
import CurrentDeviceWidthReducer from "./CurrentDeviceWidthReducer";
import HasScrolledReducer from "./HasScrolledReducer";
import HomeFeedReducer from "./HomeFeedReducer";
import LinkPostReducer from "./LinkPostReducer";
import LoginModalReducer from "./LoginModalReducer";
import LoginUserDetailsReducer from "./LoginUserDetailsReducer";
import PostDetailsReducer from "./PostDetailsReducer";
import ResetPasswordModalReducer from "./ResetPasswordModalReducer";
import ShareLinkModalReducer from "./ShareLinkModalReducer";
import SignUpModalReducer from "./SignUpModalReducer";
import TrendingCliksProfileReducer from "./TrendingCliksProfileReducer";
import TrendingCliksReducer from "./TrendingCliksReducer";
import TrendingUsersReducer from "./TrendingUsersReducer";
import UserApproachReducer from "./UserApproachReducer";
import UserFeedReducer from "./UserFeedReducer";
import UsernameModalReducer from "./UsernameModalReducer";
import UserProfileDetailsReducer from "./UserProfileDetailsReducer";
import UserReducer from "./UserReducer";
import VerifyEmailModalReducer from "./VerifyEmailModalReducer";
import FeedReportModalReducer from "./FeedReportModalReducer";
import TrendingTopicsReducer from "./TrendingTopicsReducer";
import TopicsFeedReducer from "./TopicsFeedReducer";
import TrendingTopicsProfileReducer from "./TrendingTopicsProfileReducer";
import ClikUserRequestReducer from "./ClikUserRequestReducer";
import ClikMembersReducer from "./ClikMembersReducer";
import PostCommentDetailsReducer from "./PostCommentDetailsReducer";
import FeedProfileReducer from "./FeedProfileReducer";
import InviteSignUpModalReducer from "./InviteSignUpModalReducer";
import ScreenLoadingReducer from "./ScreenLoadingReducer";
import TrendingExternalFeedsReducer from "./TrendingExternalFeedsReducer";
import AdminReducer from "./AdminReducer";
import EditTopicReducer from "./EditTopicReducer";
import EditFeedReducer from "./EditFeedReducer";
import EditClikReducer from "./EditClikReducer";

export default function createReducer() {
  return combineReducers({
    UserReducer,
    LoginUserDetailsReducer,
    HomeFeedReducer,
    UserProfileDetailsReducer,
    TrendingUsersReducer,
    TrendingCliksReducer,
    UserFeedReducer,
    PostDetailsReducer,
    TrendingCliksProfileReducer,
    CliksFeedReducer,
    LinkPostReducer,
    LoginModalReducer,
    ResetPasswordModalReducer,
    SignUpModalReducer,
    UsernameModalReducer,
    VerifyEmailModalReducer,
    HasScrolledReducer,
    CurrentDeviceWidthReducer,
    ShareLinkModalReducer,
    CreateAccountReducer,
    UserApproachReducer,
    FeedReportModalReducer,
    TrendingTopicsReducer,
    TopicsFeedReducer,
    TrendingTopicsProfileReducer,
    ClikUserRequestReducer,
    ClikMembersReducer,
    PostCommentDetailsReducer,
    FeedProfileReducer,
    InviteSignUpModalReducer,
    ScreenLoadingReducer,
    TrendingExternalFeedsReducer,
    AdminReducer,
    EditTopicReducer,
    EditFeedReducer,
    EditClikReducer
  });
}
