import { fromJS } from "immutable";
import {
  ADMIN_STATUS_SUCCESS,
  ADMIN_STATUS_FAILURE,
  ADMIN_VIEW_SUCCESS
} from "../constants/Action";

const initialState = fromJS({
  isAdmin: false,
  isAdminView: false,
  searchBarStatus: false,
  // setTerm: '',

  setKeyEvent: "",

  setDisplayType: "",
  inviteUserDetails: {},
  messageModalStatus: false,
  loginButtonText: "Login",
  googleLogin: false,
  earningModalStatus: false,
  isAnonomyousLogin: false,
  anonymousToken: "",
  tabType:"",
  openCreateCommentModal: false,
  leftPanelOpenStatus: false,
  searchBarOpenStatus: false,
  analyticsTabType: "",
  profileTabType: ""
});

export default function AdminReducer(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case ADMIN_STATUS_SUCCESS:
      return state.set("isAdmin", fromJS(payload));
    case ADMIN_VIEW_SUCCESS:
      return state.set("isAdminView", fromJS(payload));
    case "SEARCH_BAR_STATUS":
      return state.set("searchBarStatus", payload);
    case ADMIN_STATUS_FAILURE:
      return initialState;
    // case "SET_TERM":
    // return state.set("setTerm", fromJS(payload));

    case "SET_KEY_EVENT":
      return state.set("setKeyEvent", fromJS(payload));
    case "SET_DISPLAY_TYPE":
      return state.set("setDisplayType", fromJS(payload));

    case "SET_INVITE_USER_DETAIL":
      return state.set("inviteUserDetails", fromJS(payload));

    case "MESSAGEMODALSTATUS":
      return state.set("messageModalStatus", fromJS(payload));

    case "SET_LOGIN_BUTTON_TEXT":
      return state.set("loginButtonText", fromJS(payload));

    case "SET_GOOGLE_LOGIN":
      return state.set("googleLogin", fromJS(payload));

    case "EARNINGMODALSTATUS":
      return state.set("earningModalStatus", fromJS(payload));

    case "ANONYMOUS_USER":
      return state
        .set("isAnonymousLogin", fromJS(payload.value))
        .set("anonymousToken", fromJS(payload.token));

    case "SET_TAB_VIEW":
      return state.set("tabType", fromJS(payload));
     
    case "OPEN_CREATE_COMMENT" :
      return state.set("openCreateCommentModal", fromJS(payload));

    case "LEFT_PANEL_OPEN" :
      return state.set("leftPanelOpenStatus", fromJS(payload));

    case "SEARCHBAR_STATUS" :
    return state.set("searchBarOpenStatus", fromJS(payload));

    case "SET_TAB_VIEW_ANALYTICS" : 
    return state.set("analyticsTabType", fromJS(payload));

    case "SET_PROFILE_TAB_VIEW" : 
    return state.set("profileTabType", fromJS(payload));
  }
  return state;
}
