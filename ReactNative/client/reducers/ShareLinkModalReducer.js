import { fromJS } from 'immutable';
import {
    SHARELINKMODALACTION_SUCCESS, SHARELINKMODALACTION_FAILURE
} from '../constants/Action';


const initialState = fromJS({
    modalStatus: false,
});

export default function ShareLinkModalReducer(state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
        case SHARELINKMODALACTION_SUCCESS:
            return state
                .set('modalStatus', fromJS(payload));
        case SHARELINKMODALACTION_FAILURE:
            return initialState;
    }
    return state;
}
