import { createAppContainer } from "react-navigation";
import TabScreen from "./Tabs";
import Stack from "./Stack";

export default createAppContainer(Stack);
