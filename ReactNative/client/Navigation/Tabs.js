import React from "react";
import { Icon } from "react-native-elements";
import { createBottomTabNavigator } from "react-navigation-tabs";
import BookMarksScreen from "../screens/BookMarksScreen";
import CreatePostScreen from "../screens/CreatePostScreen";
import Stack from "./Stack";
import NotFound from "../screens/NotFound";


const TabScreen = createBottomTabNavigator(
  {
    Home: Stack,
    Bookmarks: BookMarksScreen,
    Add: CreatePostScreen,
    tabprofile: CreatePostScreen,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === "Home") {
          iconName = `home`;
        } else if (routeName === "Bookmarks") {
          iconName = `bookmark`;
        } else if (routeName === "Add") {
          iconName = `plus`;
        } else {
          iconName = `user`;
        }
        return (
          <Icon
            name={iconName}
            type="font-awesome"
            size={25}
            color={tintColor}
          />
        );
      }
    }),
    tabBarOptions: {
      activeTintColor: "white",
      inactiveTintColor: "gray",
      style: { backgroundColor: "#000" },
      showLabel: false
    }
  }
);

export default TabScreen;
