import { createAction } from 'redux-actions';
import { TOPICS_PROFILE_DETAILS } from "../constants/Action";
export const getTrendingTopicsProfileDetails = createAction(TOPICS_PROFILE_DETAILS);