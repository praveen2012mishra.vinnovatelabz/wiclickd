import { createAction } from "redux-actions";
import { POSTCOMMENTDETAILS } from "../constants/Action";
export const setPostCommentDetails = createAction(POSTCOMMENTDETAILS);