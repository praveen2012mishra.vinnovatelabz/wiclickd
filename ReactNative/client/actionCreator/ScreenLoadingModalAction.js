import { createAction } from 'redux-actions';
import { SCREENLOADINGMODALACTION } from "../constants/Action";
export const setScreenLoadingModalAction = createAction(SCREENLOADINGMODALACTION);