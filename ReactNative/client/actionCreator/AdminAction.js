import { createAction } from 'redux-actions';
import { ADMIN_STATUS,ADMIN_VIEW} from "../constants/Action";

export const setAdminStatus = createAction(ADMIN_STATUS);
export const setAdminView = createAction(ADMIN_VIEW);