import { createAction } from 'redux-actions';
import { HOMEFEED,HOMEFEED_FAILURE } from "../constants/Action";
export const getHomefeedList = createAction(HOMEFEED);
export const resetHomefeedList = createAction(HOMEFEED_FAILURE);
