import { createAction } from 'redux-actions';
import { EDITTOPIC } from "../constants/Action";
export const editTopic = createAction(EDITTOPIC);