import { createAction } from 'redux-actions';
import { CREATEACCOUNTACTION } from "../constants/Action";
export const setCreateAccount = createAction(CREATEACCOUNTACTION);