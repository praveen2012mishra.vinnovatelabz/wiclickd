import { createAction } from 'redux-actions';
import { POSTDETAILS } from "../constants/Action";
export const setPostDetails = createAction(POSTDETAILS);