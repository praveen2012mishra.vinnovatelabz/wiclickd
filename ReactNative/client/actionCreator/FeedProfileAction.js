import { createAction } from 'redux-actions';
import { FEED_PROFILE_DETAILS } from "../constants/Action";
export const getFeedProfileDetails = createAction(FEED_PROFILE_DETAILS);