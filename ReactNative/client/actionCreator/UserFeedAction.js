import { createAction } from 'redux-actions';
import { USERFEED } from "../constants/Action";
export const getUserFeedList = createAction(USERFEED);