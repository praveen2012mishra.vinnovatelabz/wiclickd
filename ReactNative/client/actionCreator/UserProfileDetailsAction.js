import { createAction } from 'redux-actions';
import { USER_PROFILE_DETAILS } from "../constants/Action";
export const getCurrentUserProfileDetails = createAction(USER_PROFILE_DETAILS);