import { createAction } from 'redux-actions';
import { POSTLINK,POST_EDIT_DETAILS } from "../constants/Action";
export const postLink = createAction(POSTLINK);
export const postEditDetails = createAction(POST_EDIT_DETAILS);