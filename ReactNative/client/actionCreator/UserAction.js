import { createAction } from 'redux-actions';
import { LOGIN_STATUS, LOGIN_USER_DETAILS } from "../constants/Action";

export const setLoginStatus = createAction(LOGIN_STATUS);
export const saveUserLoginDaitails = createAction(LOGIN_USER_DETAILS);
