import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import { AppLoading } from "expo";
import { loadAsync, FontDisplay } from "expo-font";
import React, { useEffect, useState } from "react";
import { ApolloProvider } from "react-apollo";
import {
  AsyncStorage,
  Platform,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  View
} from "react-native";
import ErrorBoundary from "react-native-error-boundary";
import { MenuProvider } from "react-native-popup-menu";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import applloClient from "./client";
import BottomFloatingButton from "./components/BottomFloatingButton";
import Layout from "./components/Layout";
import LeftPanel from "./components/LeftPanel";
import NavigationComponent from "./components/NavigationComponent";
import CrashlyticsErrorBoundary from "./CrashlyticsErrorBoundary";
import configureStore from "./initializeStore";
import NavigationService from "./library/NavigationService";
import AppNavigator from "./Navigation/AppNavigator";
import BottomScreen from "./components/BottomScreen";

const configuretore = configureStore();
const stripePromise = loadStripe("pk_test_hFxQCiSLrnSGOUsxfRi9Phxc0031td3fGw");

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  const [width, setWidth] = useState(0);

  //------------------------------------------ Set Token in Extension ---------------------------------------------
  const handleExtensionToken = async event => {
    try {
      if (await AsyncStorage.getItem("userIdTokenFirebase")) {
        window.parent.postMessage(
          {
            type: "wecklid_login",
            userIdTokenFirebase: await AsyncStorage.getItem(
              "userIdTokenFirebase"
            ),
            UserName: await AsyncStorage.getItem("UserName")
          },
          "*"
        );
      } else window.parent.postMessage({ type: "wecklid_logout" }, "*");
    } catch (err) {
      console.log("Extension Token Set Error (App.js 41 line) ", err);
    }
  };

  useEffect(() => {
    if (Platform.OS == "web") {
      handleExtensionToken();
    }
    return () => {
      // window.removeEventListener('DOMContentLoaded', handleExtensionToken);
    };
  }, []);
  //------------------------------------------------------------------------------------------------------------

  if (!isLoadingComplete) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return (
      <SafeAreaView style={styles.container}>
        <ApolloProvider client={applloClient}>
          <Provider store={configuretore.store}>
            <PersistGate persistor={configuretore.persistor} loading={null}>
              {<StatusBar barStyle="light-content" />}
              <ErrorBoundary FallbackComponent={CrashlyticsErrorBoundary}>
                <Layout ref={navigatorRef => {
                  NavigationService.setTopLevelNavigator(navigatorRef);                  
                }}
                  navigation={props.navigation} />
                {Platform.OS == "web" ? (
                  <Elements stripe={stripePromise}>
                    <NavigationComponent
                      ref={navigatorRef => {
                        NavigationService.setTopLevelNavigator(navigatorRef);
                      }}
                      navigation={NavigationService}
                    />
                  </Elements>
                ) : (
                    <View
                      onLayout={event => {
                        let { width, height } = event.nativeEvent.layout;
                        setWidth(width);
                      }}
                      style={{
                        width: "100%",
                        flex: 1,
                        flexDirection: "row",
                        justifyContent: "center"
                      }}
                    >
                      {Platform.OS == "web" && width >= 750 ? (
                        <LeftPanel
                          ref={navigatorRef => {
                            NavigationService.setTopLevelNavigator(navigatorRef);
                          }}
                          navigation={props.navigation}
                        />
                      ) : null}
                      <View
                        style={{
                          width:
                            Platform.OS == "web" && width >= 750 ? "50%" : "100%",
                          paddingLeft:
                            Platform.OS == "web" && width >= 750 ? 10 : 0
                        }}
                      >
                        <MenuProvider>
                          <AppNavigator
                            ref={navigatorRef => {
                              NavigationService.setTopLevelNavigator(
                                navigatorRef
                              );
                            }}
                          />
                          {/* <BottomScreen navigation={NavigationService} /> */}
                        </MenuProvider>
                      </View>
                      {/* <BottomFloatingButton
                      navigation={props.navigation}
                      modalStataus={() => {}}
                    /> */}
                    </View>
                  )}
              </ErrorBoundary>
            </PersistGate>
          </Provider>
        </ApolloProvider>
      </SafeAreaView>
    );
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    loadAsync({
      // Charter: {
      //   uri: require("./assets/fonts/Charter.ttf"),
      //   display: FontDisplay.SWAP
      // },
      // Montserrat: {
      //   uri: require("./assets/fonts/Montserrat-Regular.ttf"),
      //   display: FontDisplay.SWAP
      // },
      HeaderBoldFont: {
        uri: require("./assets/fonts/Montserrat-ExtraBold.ttf"),
        display: FontDisplay.SWAP
      },
      Verdana: {
        uri: require("./assets/fonts/verdana.ttf"),
        display: FontDisplay.SWAP
      },
      VerdanaBold: {
        uri: require("./assets/fonts/Verdana-Bold.ttf"),
        display: FontDisplay.SWAP
      }
    })
  ]);
}

function handleLoadingError(error) {
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  }
});
