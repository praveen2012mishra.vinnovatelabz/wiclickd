import { Platform } from "react-native";
import { call, put, takeEvery, select } from "redux-saga/effects";
import {
  POSTCOMMENTDETAILS,
  POSTCOMMENTDETAILS_FAILURE,
  POSTCOMMENTDETAILS_SUCCESS
} from "../constants/Action";
import getEnvVars from "../environment";

const isWeb = Platform.OS === "web";
const apiUrl = getEnvVars();

const getResponse = req => {
  if (req.id) {
    return fetch(
      apiUrl.API_URL +
        "v1/comments/" +
        req.id
          .replace("Trending", "")
          .replace("New", "")
          .replace("Discussion", "")
          .replace("Search", "")
          .replace("Post:", "")
    )
      .then(response => response.json())
      .then(res => res.data.comments.edges);
  } else {
    return [];
  }
};

export function* PostCommentMiddleware({ payload }) {
  const getPostId = state => state.PostCommentDetailsReducer.get("PostId");
  const PostId = yield select(getPostId);
  const getTitle = state => state.PostCommentDetailsReducer.get("Title");
  const title = yield select(getTitle);
  const getPostCommentDetails = state =>
    state.PostCommentDetailsReducer.get("PostCommentDetails");
  const PostCommentDetails = yield select(getPostCommentDetails);

  try {
    let response;
    if (Array.isArray(payload)) {
      response = payload;
    } else {
      if (PostId != payload.id) {
        yield put({
          type: POSTCOMMENTDETAILS_SUCCESS,
          payload: [],
          postId: payload.id
            .replace("Trending", "")
            .replace("New", "")
            .replace("Discussion", "")
            .replace("Search", ""),
          title: payload.title,
          loading: payload.loading && payload.loading
        });
        response = yield call(getResponse, payload);
      } else {
        response = PostCommentDetails;
      }
    }

    // if (
    //   !Array.isArray(payload) &&
    //   PostId.replace("Post:", "") != payload.id &&
    //   payload.id.replace("Post:", "")
    // ) {
    yield put({
      type: POSTCOMMENTDETAILS_SUCCESS,
      payload: [],
      postId: Array.isArray(payload) ? PostId : payload.id,
      title: Array.isArray(payload) ? title : payload.title,
      loading: payload.loading && payload.loading
    });
    // }

    let cloneResponse = {
      comments: {
        edges: [...response]
      }
    };
    yield put({
      type: POSTCOMMENTDETAILS_SUCCESS,
      payload: setArrowKeys(cloneResponse.comments.edges, cloneResponse),
      postId: Array.isArray(payload) ? PostId : payload.id,
      title: Array.isArray(payload) ? title : payload.title,
      loading: false
    });
  } catch (err) {
    yield put({
      type: POSTCOMMENTDETAILS_FAILURE,
      payload: err.message
    });
    console.log(err);
  }
}

export default function* PostCommentMiddlewareWatcher() {
  yield takeEvery(POSTCOMMENTDETAILS, PostCommentMiddleware);
}
function setArrowKeys(items, parentItem, isRoot = true, nextParent = null) {
  let output, key, value;
  output = [];
  for (key in items) {
    value = { ...items[key] };
    value.up =
      parentItem.node && parentItem.node.id ? parentItem.node.id : null;

    if (value.comments.edges[0] && value.comments.edges[0].node.id)
      value.down = value.comments.edges[0].node.id;
    else if (isRoot && items[parseInt(key) + 1])
      value.down = items[parseInt(key) + 1].node.id;
    else if (nextParent) value.down = nextParent.node.id;
    else value.down = null;

    value.right =
      !isRoot && items[parseInt(key) + 1] && items[parseInt(key) + 1].node.id
        ? items[parseInt(key) + 1].node.id
        : null;

    value.left =
      !isRoot && items[parseInt(key) - 1] && items[parseInt(key) - 1].node.id
        ? items[parseInt(key) - 1].node.id
        : null;
    value.comments.edges = setArrowKeys(
      value.comments.edges,
      value,
      false,
      nextParent ? nextParent : items[parseInt(key + 1)]
    );
    output[key] = value;
  }
  return output;
}
