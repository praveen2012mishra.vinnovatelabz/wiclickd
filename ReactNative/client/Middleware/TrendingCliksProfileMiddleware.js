import { call, put, takeEvery } from "redux-saga/effects";
import applloClient from "../client";
import {
  CLIKS_PROFILE_DETAILS,
  CLIKS_PROFILE_DETAILS_FAILURE,
  CLIKS_PROFILE_DETAILS_SUCCESS
} from "../constants/Action";
import { ClikQuery } from "../graphqlSchema/graphqlMutation/TrendingMutation";
import NavigationService from "../library/NavigationService";
import {
  AsyncStorage
} from "react-native";
const getResponse = async req => {
  return await applloClient
    .query({
      query: ClikQuery,
      variables: {
        id: "Clik:" + req.id
      },
      fetchPolicy: "no-cache"
    })
    .then(res => res);
};

const getPostId = async () => {
  let asyncId;
  await AsyncStorage.getItem('PostId').then(houses => { asyncId = JSON.parse(houses); });
  //console.log(asyncId.replace("Post:", ""));
  return asyncId.replace("Post:", "")
}

export function* TrendingCliksProfileMiddleware({ payload }) {
  console.log(payload, 'payload===========')
  //const getLoginStatus = state => state.UserReducer.get("loginStatus");
  //const LoginStatus = yield select(getLoginStatus);
  try {
    const response = yield call(getResponse, payload);

    if (response.data.clik.id == "Clik:None") {
      yield put({
        type: CLIKS_PROFILE_DETAILS_FAILURE,
        payload: {}
      });
      NavigationService.navigate("404");
    } else {
      // if (LoginStatus == 0) {
      //   NavigationService.navigate("username");
      // }
      if (
        payload.type == "feed" ||
        payload.type == "wiki" ||
        payload.type == "users" ||
        payload.type == "join" ||
        payload.type == "invite"
      ) {
        yield put({
          type: CLIKS_PROFILE_DETAILS_SUCCESS,
          payload: response
        });
        //let asyncId;
        //yield AsyncStorage.getItem('PostId').then(houses => { asyncId = JSON.parse(houses); });
        //console.log(payload.type + '/' +asyncId.replace("Post:", ""));
        //let postId=asyncId.replace("Post:", "")
        AsyncStorage.setItem(
          'ClickTitle',
          JSON.stringify({type:payload.type,clickTitle:payload.id})
        );
        let postId;
        yield AsyncStorage.getItem('PostId').then(houses => { postId = JSON.parse(houses); });
        console.log(postId,payload.id, '-------------------------------->');
        // NavigationService.navigate("cliksprofileId", {
        //   id: payload.id,
        //   type: payload.type,
        //   postId:postId
        // });
        NavigationService.navigate("cliksprofile", {
          id: payload.id,
          type: payload.type,
          postId:' '
          //,
          //postId:postId
        });
        // NavigationService.navigate("cliksprofileId", {
        //   id: payload.id,
        //   type: payload.type,
        //   postId:postId
        // });
      } else {
        yield put({
          type: CLIKS_PROFILE_DETAILS_FAILURE,
          payload: {}
        });
        NavigationService.navigate("404");
      }
    }
  } catch (err) {
    yield put({
      type: CLIKS_PROFILE_DETAILS_FAILURE,
      payload: err.message
    });
    console.log(err);
  }
}

export default function* TrendingCliksProfileMiddlewarewareWatcher() {
  yield takeEvery(CLIKS_PROFILE_DETAILS, TrendingCliksProfileMiddleware);
}
