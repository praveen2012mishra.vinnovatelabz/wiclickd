import { call, put, takeEvery } from "redux-saga/effects";
import applloClient from "../client";
import {
  TRENDINGEXTERNALFEEDS,
  TRENDINGEXTERNALFEEDS_FAILURE,
  TRENDINGEXTERNALFEEDS_SUCCESS
} from "../constants/Action";
import { Trending_ExternalFeeds_Mutation } from "../graphqlSchema/graphqlMutation/TrendingMutation";

const getResponse = req => {
  return applloClient
    .query({
      query: Trending_ExternalFeeds_Mutation,
      variables: {
        // currentPage: req.currentPage
        first: req.currentPage,
        after: null,
        sort: "TRENDING"
      },
      fetchPolicy: "no-cache"
    })
    .then(res => res);
};

export function* Trending_ExternalFeeds_Middleware({ payload }) {
  try {
    const response = yield call(getResponse, payload);
    yield put({
      type: TRENDINGEXTERNALFEEDS_SUCCESS,
      // payload: response.data.trending_external_feeds.edges
      payload: response.data.external_feed_list.edges
    });
  } catch (err) {
    yield put({
      type: TRENDINGEXTERNALFEEDS_FAILURE,
      payload: err.message
    });
    console.log(err);
  }
}

export default function* Trending_ExternalFeeds_MiddlewareWatcher() {
  yield takeEvery(TRENDINGEXTERNALFEEDS, Trending_ExternalFeeds_Middleware);
}
