import { call, put, takeEvery } from "redux-saga/effects";
import applloClient from "../client";
import {
  TRENDINGCLICKS,
  TRENDINGCLICKS_FAILURE,
  TRENDINGCLICKS_SUCCESS
} from "../constants/Action";
import { Trending_Cliks_Mutation } from "../graphqlSchema/graphqlMutation/TrendingMutation";

const getResponse = req => {
  return applloClient
    .query({
      query: Trending_Cliks_Mutation,
      variables: {
        // currentPage: req.currentPage
        first: req.currentPage,
        after: null,
        sort: "TRENDING"
      },
      fetchPolicy: "no-cache"
    })
    .then(res => res);
};

export function* Trending_Clicks_Middleware({ payload }) {
  try {
    const response = yield call(getResponse, payload);
    yield put({
      type: TRENDINGCLICKS_SUCCESS,
      //   payload: response.data.trending_cliks.edges
      payload: response.data.clik_list.edges
    });
  } catch (err) {
    yield put({
      type: TRENDINGCLICKS_FAILURE,
      payload: err.message
    });
    console.log(err);
  }
}

export default function* Trending_Clicks_MiddlewareWatcher() {
  yield takeEvery(TRENDINGCLICKS, Trending_Clicks_Middleware);
}
