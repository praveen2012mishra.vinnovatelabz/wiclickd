import { call, put, takeEvery } from "redux-saga/effects";
import applloClient from "../client";
import {
  USER_PROFILE_DETAILS,
  USER_PROFILE_DETAILS_FAILURE,
  USER_PROFILE_DETAILS_SUCCESS
} from "../constants/Action";
import { UserQueryMutation } from "../graphqlSchema/graphqlMutation/UserMutation";
import { UserDetailsVariables } from "../graphqlSchema/graphqlVariables/UserVariables";
import NavigationService from "../library/NavigationService";

const getResponse = async req => {
  UserDetailsVariables.variables.id = "User:" + req.username;
  return await applloClient
    .query({
      query: UserQueryMutation,
      ...UserDetailsVariables,
      fetchPolicy: "no-cache"
    })
    .then(res => res);
};

export function* UserProfileDetailsMiddleware({ payload }) {
  try {
    const response = yield call(getResponse, payload);
    if (response.data.user.id == "User:None") {
      yield put({
        type: USER_PROFILE_DETAILS_FAILURE,
        payload: {}
      });
      NavigationService.navigate("404");
    } else {
      if (
        payload.type.toLowerCase() == "feed" ||
        payload.type.toLowerCase() == "profile" ||
        payload.type.toLowerCase() == "comments"
      ) {
        yield put({
          type: USER_PROFILE_DETAILS_SUCCESS,
          payload: response.data
        });
        NavigationService.navigate("profile", {
          username: response.data.user.username,
          type: payload.type.toLowerCase()
        });
      } else {
        yield put({
          type: USER_PROFILE_DETAILS_FAILURE,
          payload: {}
        });
        NavigationService.navigate("404");
      }
    }
  } catch (err) {
    yield put({
      type: USER_PROFILE_DETAILS_FAILURE,
      payload: err.message
    });
    console.log(err);
  }
}

export default function* UserProfileDetailsMiddlewareWatcher() {
  yield takeEvery(USER_PROFILE_DETAILS, UserProfileDetailsMiddleware);
}
