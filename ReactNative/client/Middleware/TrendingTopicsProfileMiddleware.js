import { call, put, takeEvery } from "redux-saga/effects";
import applloClient from "../client";
import {
  TOPICS_PROFILE_DETAILS,
  TOPICS_PROFILE_DETAILS_FAILURE,
  TOPICS_PROFILE_DETAILS_SUCCESS
} from "../constants/Action";
import { TopicQuery } from "../graphqlSchema/graphqlMutation/TrendingMutation";
import NavigationService from "../library/NavigationService";
import { capitalizeFirstLetter } from "../library/Helper";

const getResponse = async req => {
  return await applloClient
    .query({
      query: TopicQuery,
      variables: {
        id: "Topic:" + req.id
      },
      fetchPolicy: "no-cache"
    })
    .then(res => res);
};

export function* TrendingTopicsProfileMiddleware({ payload }) {
  try {
    const response = yield call(getResponse, payload);

    if (response.data.topic.id == "Topic:None") {
      yield put({
        type: TOPICS_PROFILE_DETAILS_FAILURE,
        payload: {}
      });
      NavigationService.navigate("404");
    } else {
      if (payload.type == "feed" || payload.type == "wiki") {
        yield put({
          type: TOPICS_PROFILE_DETAILS_SUCCESS,
          payload: response
        });
        NavigationService.navigate("topicprofile", {
          title: response.data.topic.name,
          type: !payload.type ? "feed" : payload.type
        });
      } else {
        yield put({
          type: TOPICS_PROFILE_DETAILS_FAILURE,
          payload: {}
        });
        NavigationService.navigate("404");
      }
    }
  } catch (err) {
    yield put({
      type: TOPICS_PROFILE_DETAILS_FAILURE,
      payload: err.message
    });
    console.log(err);
  }
}

export default function* TrendingTopicsProfileMiddlewarewareWatcher() {
  yield takeEvery(TOPICS_PROFILE_DETAILS, TrendingTopicsProfileMiddleware);
}
