import { call, put, takeEvery } from "redux-saga/effects";
import applloClient from "../client";
import {
  FEED_PROFILE_DETAILS,
  FEED_PROFILE_DETAILS_FAILURE,
  FEED_PROFILE_DETAILS_SUCCESS
} from "../constants/Action";
import { ExternalFeedProfileQuery } from "../graphqlSchema/graphqlMutation/FeedMutation";
import NavigationService from "../library/NavigationService";
import { capitalizeFirstLetter } from "../library/Helper";

const getResponse = async req => {
  return await applloClient
    .query({
      query: ExternalFeedProfileQuery,
      variables: {
        id: "ExternalFeed:" + req.id
      },
      fetchPolicy: "no-cache"
    })
    .then(res => res);
};

export function* FeedProfileMiddleware({ payload }) {
  try {
    const response = yield call(getResponse, payload);

    if (response.data.external_feed.id == "ExternalFeed:ExternalFeed:None") {
      yield put({
        type: FEED_PROFILE_DETAILS_FAILURE,
        payload: {}
      });
      NavigationService.navigate("404");
    } else {
      if (payload.type == "feed" || payload.type == "wiki") {
        yield put({
          type: FEED_PROFILE_DETAILS_SUCCESS,
          payload: response
        });
        NavigationService.navigate("feedprofile", {
          title: response.data.external_feed.name,
          type: !payload.type ? "feed" : payload.type
        });
      } else {
        yield put({
          type: FEED_PROFILE_DETAILS_FAILURE,
          payload: {}
        });
        NavigationService.navigate("404");
      }
    }
  } catch (err) {
    yield put({
      type: FEED_PROFILE_DETAILS_FAILURE,
      payload: err.message
    });
    console.log(err);
  }
}

export default function* FeedProfileMiddlewareWatcher() {
  yield takeEvery(FEED_PROFILE_DETAILS, FeedProfileMiddleware);
}
