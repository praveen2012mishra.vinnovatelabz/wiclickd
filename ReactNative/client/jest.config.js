module.exports = {
  preset: "jest-expo",
  globals: {
    URL: "http://localhost:19006/"
  },
  verbose: true,
  testTimeout: 30000,
  setupFilesAfterEnv: ["./jest.setup.js"],
  transform: {
    "\\.js$": "<rootDir>/node_modules/react-native/jest/preprocessor.js"
  },
  collectCoverage: true,
  collectCoverageFrom: [
    "**/*.{js,jsx}",
    "!**/coverage/**",
    "!**/web-build/**",
    "!**/node_modules/**",
    "!**/babel.config.js",
    "!**/webpack.config.js"
  ]
};
